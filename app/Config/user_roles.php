<?php

$config = array(
    'users' => array(
        0 => 'my_account',
        1 => 'set_user_password',
        2 => 'details',
        3 => 'change_user_passwaord',
        4 => 'edit_details',
        5 => 'thanks'
    ),
    'enquiries' => array(
        0 => 'send_enquiry',
        1 => 'user_enquiry_index',
        2 => 'user_enquiry_view'
    ),
    'testimonials' => array(
        0 => 'add',
        1 => 'edit',
        2 => 'user_view',
        3 => 'user_testi_index'
    ),
    'orders' => array(
        0 => 'user_order_index',
        1 => 'user_order_view',
    )
);

$config['roles'] = $config;
?>
