Huntington Ingalls Industries, Inc. - Strategy and SWOT Report, is a source of comprehensive company data and information. The report covers the company’s structure, operation, SWOT analysis, product and service offerings and corporate actions, providing a 360˚ view of the company.

Features:

- Detailed information on Huntington Ingalls Industries, Inc. required for business and competitor intelligence needs
- A study of the major internal and external factors affecting Huntington Ingalls Industries, Inc. in the form of a SWOT analysis
- An in-depth view of the business model of Huntington Ingalls Industries, Inc. including a breakdown and examination of key business segments
- Intelligence on Huntington Ingalls Industries, Inc.’s mergers and acquisitions (M&A), strategic partnerships and alliances, capital raising, private equity transactions, and financial and legal advisors
- News about Huntington Ingalls Industries, Inc., such as business expansion, restructuring, and contract wins
- Large number of easy-to-grasp charts and graphs that present important data and key trends

Highlights:

Huntington Ingalls Industries (HII) designs, builds and maintains nuclear and non-nuclear ships for the US Navy and Coast Guard and provides after-market services for military ships around the globe. HII primarily operates in the US, where it is headquartered in Newport News, Virginia, and employs around 38,000 people.  The company recorded revenues of $6,820 million in the financial year ended December 2013 (FY2013), an increase of 1.7% over FY2012. The company's operating profit was $512 million in FY2013, an increase of 43% over FY2012. Its net profit was $261 million in FY2013, an increase of 78.8% over FY2012.

Reasons to Purchase:

- Gain understanding of  Huntington Ingalls Industries, Inc. and the factors that influence its strategies
- Track strategic initiatives of the company and latest corporate news and actions
- Assess Huntington Ingalls Industries, Inc. as a prospective partner, vendor or supplier
- Support sales activities by understanding your customers' businesses better
- Stay up to date on Huntington Ingalls Industries, Inc.’s business structure, strategy and prospects