<?php

$idata['roles'] = array(
    1 => 'Super Admin',
    2 => 'Analyst',
    3 => 'SEO',
    4 => 'Content',
    5 => 'Jr Analyst',
    11 => 'User',
    12 => 'Sales Team' //code added VG-13/03/2017

);
$idata['switch_roles'] = array(
    2 => 'Analyst',
    3 => 'SEO',
    4 => 'Content',
    5 => 'Jr Analyst'
);
$idata['yes_no'] = array(
    1 => 'Yes',
    0 => 'No'
);
$idata['toc'] = array(
    0 => 'Yes',
    1 => 'No'
);
$idata['product_search'] = array(
    'product_name' => 'Product Name',
);
$idata['user_search'] = array(
    'name' => 'User Name',
    'organisation' => 'Organisation'
);
$idata['client_search'] = array(
    'client_name' => 'Client Name',
    'link' => 'Link'
);
$idata['order_search'] = array(
    'name' => 'User Name',
    'token' => 'Token'
);
$idata['limits'] = array(
    10 => 10,
    20 => 20,
    30 => 30,
    40 => 40,
    50 => 50
);
$idata['title'] = array(
    'Mr' => 'Mr',
    'Miss' => 'Miss',
    'Mrs' => 'Mrs',
    'Dr' => 'Dr',
    'Ms' => 'Ms',
    'Prof' => 'Prof'
);
$idata['enquiry_search'] = array(
    'name' => 'User Name',
    'subject' => 'Subject',
    'rating' => 'Rating',
    'status' => 'Status'
);
$idata['main_category'] = array(
    1 => 'Country Database',
    2 => 'Industry Profiles',
    3 => 'Company Database',
    4 => 'Email Databases',
    5 => 'Lead Generation',
    6 => 'Survey'
);
$idata['display_home'] = array(
    1 => 'Research',
    2 => 'Marketing',
);
/*$idata['lead_status'] = array(
    0 => 'No Status',
    1 => 'Junk',
    2 => 'Hot',
    3 => 'Cold',
    4 => 'Lost',
    5 => 'Closed',
    6 => 'Starred'
);
$idata['lead_rating'] = array(
    0 => 'No Rating',
    1 => '1',
    2 => '2',
    3 => '3',
    4 => '4',
    4 => '5',
);*/
//Code modified Start VG-7/03/2017 Start
$idata['lead_status'] = array(
    0 => 'No Status',
    1 => 'Not Responding',
    2 => 'Not Contacted',
    3 => 'Sample Sent',
    4 => 'Requirement Matched',
    5 => 'Negotiation',
    6 => 'Closed Won',
    7 =>  'Closed Lost',
    8 => 'Waiting For Approval', //code added VG-13/03/2017
    9 =>  'Email Reqested',
    10=> 'Follow-up done',

);
$idata['lead_rating'] = array(
    0 => 'No Rating',
    1 => 'Hot',
    2 => 'Warm',
    3 => 'Cold',
    4 => 'Junk',
    //5 => '5',
);
//Code modified End VG-7/03/2017

$idata['curr'] = array(
    0 => 'INR',
    1 => 'USD',
);
$idata['transaction_status'] = array(
    0 => 'Transaction Incomplete',
    1 => 'Transaction complete Waiting For Payment',
    2 => 'Payment Received Waiting For Deliver',
    3 => 'Deliver',
);

$idata['transaction_status_hdfc'] = array(
    0 => 'Order received',
    1 => 'Processing',
    2 => 'Delivered'
);

$idata['payment_status'] = array(
    0 => 'Pending',
    1 => 'Received',
);
$idata['order_ststus'] = array(
    0 => 'Not Deliver',
    1 => 'Deliver',
);

$idata['product_status'] = array(
    2 => 'Ready to Active',
    1 => 'Active',
    0 => 'Deactive',
);

$idata['months'] = array(
    1 => 'January',
    2 => 'February',
    3 => 'March',
    4 => 'April',
    5 => 'May',
    6 => 'June',
    7 => 'July',
    8 => 'August',
    9 => 'September',
    10 => 'October',
    11 => 'November',
    12 => 'December'
);
$idata['ref_page'] = array(
    1 => 'Report Detail',
    2 => 'Speak to Analyst',
    3 => 'Request For TOC',
    4 => 'Buy Sections or Tables only',
    5 => 'Get Custom Research at price of Syndicate',
    'download-sample' => 'Download Sample',
    'covid-19-impact' => 'COVID-19 Impact',
    'speak-to-analyst' => 'Speak To Analyst',
    'get-custom-research' => 'Get Custom Research at price of Syndicate',
    'ask-questions' => 'Ask Questions',
    'buy-now' => 'Buy Now',
    'pre-buy-now' => 'Pre Buy Now',
    'enquiry-before-buying' => 'Enquiry Before Buying',
    'request-sample' => 'Request Sample',
    'request-customization' => 'Request Customization',
    'request-for-special-pricing' => 'Request for Special Pricing',
    'request-free-sample-report' => 'Request Free Sample Report',
    'request-for-discount' => 'Request for Discount'
);
$idata['licence_type'] = array(
    1 => 'Single User License',
    3 => 'Upto 10 Users License',
    2 => 'Enterprise License',
    4 => 'Data Pack'
);
$idata['licence_type_tooltip'] = array(
    1 => 'Single User License allows only one user to access the Report. The report will be sent electronically to the buyer as a PDF File.',
    3 => 'Multi-User License allows 2-10 users to access the Report. The report will be sent electronically to the buyers as a PDF File.',
    2 => 'Enterprise User License allows access of the Report across the whole enterprise. The report will be sent electronically as a PDF File.',
    4 => 'This covers all data tables of the report in the excel format only.'
);

$idata['article_type'] = array(
    'article' => 'Article',
    'analysis' => 'Analysis',
    'press-releases' => 'Press Releases'
);

$idata['position_type'] = array(
    1 => "Content",
    2 => "Software Development",
    3 => "Sales",
    4 => "Research",
    5 => "HR"
);

// Razorpay Payment Gateway LB-1/04/2022
$idata['RAZOR_API_KEY'] = "rzp_live_ltEISKHL7OraYx";

$idata['RAZOR_KEY_SECRET'] = "ypaVKphrp56KcFr4wIADXONi";

$idata['RAZOR_PAYMENT_URL'] = "https://api.razorpay.com/v1/payments/";

$config['idata'] = $idata;
