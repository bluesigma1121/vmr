<?php

$config = array(
    'products' => array(
        0 => 'add',
        1 => 'edit',
        3 => 'view',
        4 => 'index',
        5 => 'add_category',
        6 => 'edit_category',
        7 => 'active',
        8 => 'deactive',
        9 => 'verified',
        10 => 'deverified',
        11 => 'add_product_countries',
        12 => 'download_sample_file',
        13 => 'upload_products_excel',
        14 => 'upload_products_excel',
        15 => 'category_listing_preview',
        16 => 'product_preview',
        17 => 'ready_to_active',
        18 => 'pending_status',
        19 => 'products_ready_active',
        20 => 'temp_edit_proddescription',
        21 => 'add_faq',
        22 => 'edit_faq'                             
    ),
    'users' => array(
        1 => 'change_password',
    ),
    'specifications' => array(
        1 => 'add_product_spc'
    ),
    'product_categories' => array(
        1 => 'delete'
    ),
    'reportdescriptionimages' => array(
        0 => 'index',
        1=> 'add',
        2 => 'edit'
    ),
    'rdupdateimagedata' => array(
        0 => 'index',
        1=> 'add',
        2 => 'edit'
    ),
    'infographics' => array(
        0 =>'index',
        1 => 'add',
        2 => 'edit',
        3 => 'delete'
    ),
    
    'categories' => array(
        0 => 'level_one',
        1 => 'level_two',
        2 => 'level_three',
        3 => 'level_four',
        4 => 'add_level_one',
        5 => 'add_level_two',
        6 => 'add_level_three',
        7 => 'add_level_four',
        8 => 'delete',
        9 => 'view',
        10 => 'edit',
        11 => 'edit_level_one',
        12 => 'edit_level_two',
        13 => 'edit_level_three',
        14 => 'edit_level_four',
        15 => 'tree_view_categories',
        16 => 'ajax_result',
        17 => 'remove',
        18 => 'display',
        19 => 'active',
        20 => 'deactive',
        21 => 'change_status_selected',
        22 => 'add_level_five',
        23 => 'edit_level_five',
        24 => 'level_five',
        25 => 'add_level_six',
        26 => 'edit_level_six',
        27 => 'level_six',
        28 => 'category_publish',
        29 => 'category_listing_preview',
        30 => 'upload_category_excel',
        31 => 'download_sample_file',
        32 => 'tree_view_sub_categories',
        33 => 'display_active'
    ),
);
$config['roles'] = $config;
?>
