<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */

Router::connect('/', array('controller' => 'categories', 'action' => 'home'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
 
Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

// Router::connect(
//         '/industries/:id-:slug-:strPage-:page', array('controller' => 'products', 'action' => 'category_listing'), array('pass' => array('id', 'slug', 'page', 'strPage'), 'id' => '[0-9]+', 'strPage' => 'page', 'page' => '[0-9]+')
// );
Router::connect(
        '/industries/:slug-:strPage-:page', array('controller' => 'products', 'action' => 'category_listing'), array('pass' => array('slug', 'page', 'strPage'), 'strPage' => 'page', 'page' => '[0-9]+')
);

// Router::connect(
//   '/reports/:page', array('controller' => 'products', 'action' => 'reportlist'),array('pass' => array('page'), 'page' => '[0-9]+')
// );


Router::connect(
  '/reports/*', array('controller' => 'products', 'action' => 'reportlist')
);

Router::connect(
  '/products', array('controller' => 'products', 'action' => 'index')
);
Router::connect(
  '/products/add', array('controller' => 'products', 'action' => 'add')
);
Router::connect(
  '/products/products_ready_active', array('controller' => 'products', 'action' => 'products_ready_active')
);
Router::connect(
  '/products/pending_status', array('controller' => 'products', 'action' => 'pending_status')
);

Router::connect(
    '/enquiries', array('controller' => 'enquiries', 'action' => 'index')
);
Router::connect(
    '/users', array('controller' => 'users', 'action' => 'index')
);
Router::connect(
    '/users/add', array('controller' => 'users', 'action' => 'add')
);
Router::connect(
    '/users/logout', array('controller' => 'users', 'action' => 'logout')
);
Router::connect(
    '/categories/level_two', array('controller' => 'categories', 'action' => 'level_two')
);
Router::connect(
    '/categories/add_level_two', array('controller' => 'categories', 'action' => 'add_level_two')
);
Router::connect(
    '/categories/level_three', array('controller' => 'categories', 'action' => 'level_three')
);
Router::connect(
    '/categories/add_level_three', array('controller' => 'categories', 'action' => 'add_level_three')
);
Router::connect(
    '/categories/level_four', array('controller' => 'categories', 'action' => 'level_four')
);
Router::connect(
    '/categories/add_level_four', array('controller' => 'categories', 'action' => 'add_level_four')
);
Router::connect(
    '/categories/level_five', array('controller' => 'categories', 'action' => 'level_five')
);
Router::connect(
    '/categories/add_level_five', array('controller' => 'categories', 'action' => 'add_level_five')
);
Router::connect(
    '/categories/level_six', array('controller' => 'categories', 'action' => 'level_six')
);
Router::connect(
    '/categories/add_level_six', array('controller' => 'categories', 'action' => 'add_level_six')
);

Router::connect(
    '/categories/tree_view_categories', array('controller' => 'categories', 'action' => 'tree_view_categories')
);


Router::connect(
    '/orders', array('controller' => 'orders', 'action' => 'index')
);
Router::connect(
    '/specifications', array('controller' => 'specifications', 'action' => 'index')
);
Router::connect(
    '/specifications/add', array('controller' => 'specifications', 'action' => 'add')
);
Router::connect(
    '/regions', array('controller' => 'regions', 'action' => 'index')
);
Router::connect(
    '/regions/add', array('controller' => 'regions', 'action' => 'add')
);
Router::connect(
    '/countries', array('controller' => 'countries', 'action' => 'index')
);
Router::connect(
    '/countries/add', array('controller' => 'countries', 'action' => 'add')
);
Router::connect(
    '/mis/reports_by_publisher', array('controller' => 'mis', 'action' => 'reports_by_publisher')
);
Router::connect(
    '/mis/vmr_reports_by_category', array('controller' => 'mis', 'action' => 'vmr_reports_by_category')
);
Router::connect(
    '/mis/leads_by_report', array('controller' => 'mis', 'action' => 'leads_by_report')
);
Router::connect(
    '/our_clients', array('controller' => 'our_clients', 'action' => 'index')
);
Router::connect(
    '/our_clients/add', array('controller' => 'our_clients', 'action' => 'add')
);
Router::connect(
    '/testimonials', array('controller' => 'testimonials', 'action' => 'index')
);
Router::connect(
    '/testimonials/add', array('controller' => 'testimonials', 'action' => 'add')
);
Router::connect(
    '/slideshow', array('controller' => 'slideshow', 'action' => 'index')
);
Router::connect(
    '/slideshow/add', array('controller' => 'slideshow', 'action' => 'add')
);
Router::connect(
    '/blogs', array('controller' => 'blogs', 'action' => 'index')
);
Router::connect(
    '/blogs/add', array('controller' => 'blogs', 'action' => 'add')
);
Router::connect(
    '/whyus', array('controller' => 'whyus', 'action' => 'index')
);
Router::connect(
    '/whyus/add', array('controller' => 'whyus', 'action' => 'add')
);
Router::connect(
    '/searchresultnotfound', array('controller' => 'searchresultnotfound', 'action' => 'index')
);
Router::connect(
    '/settings', array('controller' => 'settings', 'action' => 'index')
);
Router::connect(
    '/site_visits/visit_report', array('controller' => 'site_visits', 'action' => 'visit_report')
);
Router::connect(
    '/articles', array('controller' => 'articles', 'action' => 'index')
);
Router::connect(
    '/articles/add', array('controller' => 'articles', 'action' => 'add')
);
Router::connect(
    '/articles_reports', array('controller' => 'articles_reports', 'action' => 'index')
);
Router::connect(
    '/articles_reports/add', array('controller' => 'articles_reports', 'action' => 'add')
);

//latest eighty market reports
Router::connect(
        '/reports/latest-market-reports', array('controller' => 'products', 'action' => 'latest_market_reports')
);

Router::connect(
        '/feeds/feeds', array('controller' => 'products', 'action' => 'rss','ext'=>'xml')
);

Router::connect(
        '/categories/:id-:slug', array('controller' => 'categories', 'action' => 'category_details_list'), array('pass' => array('id', 'slug'), 'id' => '[0-9]+')
);

Router::connect(
        '/industries/:slug', array('controller' => 'products', 'action' => 'category_listing'), array('pass' => array('slug'))
);

// Router::connect(
//         '/industries/:main/:slug', array('controller' => 'products', 'action' => 'category_details'), array('pass' => array('main', 'slug'))
// );

Router::connect(
        '/report/:slug', array('controller' => 'products', 'action' => 'category_details'), array('pass' => array('slug'))
);

// Router::connect(
//         '/industries/:id-:slug', array('controller' => 'products', 'action' => 'category_listing'), array('pass' => array('id', 'slug'), 'id' => '[0-9]+')
// );

// Router::connect(
//         '/:main/:id-:slug', array('controller' => 'products', 'action' => 'category_details'), array('pass' => array('id', 'main', 'slug'), 'id' => '[0-9]+')
// );


/* New route for search result page VG-21/10/2016 start */
Router::connect(
        '/search-results', array('controller' => 'categories', 'action' => 'home_search_form')
);
/* New route for search result page VG-21/10/2016 end */

//Router::connect('/admin/products', array('controller' => 'products', 'action' => 'index'));

Router::connect('/contact-us', array('controller' => 'users', 'action' => 'contactus'));
Router::connect('/custom404', array('controller' => 'users', 'action' => 'page_not_found'));
Router::connect('/about-us', array('controller' => 'users', 'action' => 'aboutus'));
Router::connect('/privacy-policy', array('controller' => 'users', 'action' => 'privacy_policy'));
Router::connect('/terms-and-conditions', array('controller' => 'users', 'action' => 'terms_conditions'));
Router::connect('/refund-policy', array('controller' => 'users', 'action' => 'refundpolicy'));
Router::connect('/how-to-order', array('controller' => 'users', 'action' => 'howtoorder'));
Router::connect('/research-methodology', array('controller' => 'users', 'action' => 'researchmethodology'));
Router::connect('/login', array('controller' => 'users', 'action' => 'login'));
Router::connect('/forgot_password', array('controller' => 'users', 'action' => 'forgot_password'));
Router::connect('/registration', array('controller' => 'users', 'action' => 'registration'));
Router::connect('/category-list', array('controller' => 'categories', 'action' => 'category_list'));
Router::connect('/cart', array('controller' => 'cart_items', 'action' => 'add_to_cart'));
Router::connect('/sitemap', array('controller' => 'sitemap', 'action' => 'generate','ext'=>'xml'));
Router::connect('/careers', array('controller' => 'users', 'action' => 'careers'));
Router::connect('/thank-you/:slug', array('controller' => 'users', 'action' => 'thanks'),array('pass' => array('slug')));
Router::connect('/thank-you', array('controller' => 'users', 'action' => 'thanks'));
Router::connect('/payment-success', array('controller' => 'users', 'action' => 'payment_success'));
Router::connect('/transaction-failed', array('controller' => 'users', 'action' => 'payment_failed'));
Router::connect('/faq', array('controller' => 'users', 'action' => 'faq'));
Router::connect('/ccavresponse', array('controller' => 'enquiries', 'action' => 'ccavresponsehandler'));
Router::connect('/payresponse', array('controller' => 'enquiries', 'action' => 'payresponsehandler'));

Router::connect('/testimonial', array('controller' => 'testimonials', 'action' => 'listing'));

Router::connect('/contact/:slug/:ref_page', array('controller' => 'enquiries', 'action' => 'get_lead_info_form'),
        array('pass'=>array('slug','ref_page','main_cat') ));

// Router::connect('/contact/:ref_page-:id', array('controller' => 'enquiries', 'action' => 'get_lead_info_form'),
//         array('pass'=>array('id','ref_page','main_cat'),'id' => '[0-9]+'));

// code starts here-VG-11/08/2016
// Router::connect('/contact/:main_cat/:ref_page-:id', array('controller' => 'enquiries', 'action' => 'get_lead_info_form'),
//         array('pass'=>array('id','ref_page','main_cat'),'id' => '[0-9]+'));
// code ends here-VG-11/08/2016

// Router::connect('/insights/:section', array('controller' => 'articles', 'action' => 'article_listing'),
//         array('pass'=>array('section')));

// Router::connect('/insights/:folder/:url_slug/:article_id', array('controller' => 'articles', 'action' => 'article_details'),
//         array('pass'=>array('folder','article_id','url_slug'),'article_id'=>'[0-9]+'));

Router::connect('/:section', array('controller' => 'articles', 'action' => 'article_listing'),
        array('pass'=>array('section')));

Router::connect('/:folder/:url_slug', array('controller' => 'articles', 'action' => 'article_details'),
        array('pass'=>array('folder','url_slug') ));


Router::connect(
        '/blog/:url_slug/:blog_id', array('controller' => 'blogs', 'action' => 'blog_details'), array('pass' => array('blog_id', 'url_slug'), 'blog_id' => '[0-9]+')
);

// Router::connect('/blog/:blog_id-:url_slug', array('controller' => 'blogs', 'action' => 'blog_details'),
//         array('pass'=>array('url_slug','blog_id'),'blog_id'=>'[0-9]+'));
Router::connect('/blog', array('controller' => 'blogs', 'action' => 'blog_listing'));



/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
Router::parseExtensions('html','rss');
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
