<?php

$config = array(
    'users' => array(
        0 => 'add',
        1 => 'index',
        2 => 'edit',
        3 => 'view',
        4 => 'dashbord',
        5 => 'delete',
        6 => 'change_password',
        7 => 'active',
        8 => 'deactive',
        9 => 'verified',
        10 => 'deverified',
        11 => 'user_reset_password',
        12 => 'lead_user_index',
        13 => 'change_login',
        14 => 'offer_price'
    ),
    'categories' => array(
        0 => 'level_one',
        1 => 'level_two',
        2 => 'level_three',
        3 => 'level_four',
        4 => 'add_level_one',
        5 => 'add_level_two',
        6 => 'add_level_three',
        7 => 'add_level_four',
        8 => 'delete',
        9 => 'view',
        10 => 'edit',
        11 => 'edit_level_one',
        12 => 'edit_level_two',
        13 => 'edit_level_three',
        14 => 'edit_level_four',
        15 => 'tree_view_categories',
        16 => 'ajax_result',
        17 => 'remove',
        18 => 'display',
        19 => 'active',
        20 => 'deactive',
        21 => 'change_status_selected',
        22 => 'add_level_five',
        23 => 'edit_level_five',
        24 => 'level_five',
        25 => 'add_level_six',
        26 => 'edit_level_six',
        27 => 'level_six',
        28 => 'category_publish',
        29 => 'category_unpublish',
        30 => 'download_sample_file',
        31 => 'upload_category_excel',
        32 => 'set_count',
        33 => 'find_sub_count',
        34 => 'find_count',
        35 => 'tree_view_sub_categories',
        36 => 'display_active'
    ),
    'regions' => array(
        0 => 'add',
        1 => 'edit',
        2 => 'index',
        3 => 'delete',
        4 => 'view'
    ),
    'countries' => array(
        0 => 'add',
        1 => 'edit',
        2 => 'index',
        3 => 'delete',
        4 => 'view',
    ),
    'specifications' => array(
        0 => 'add',
        1 => 'edit',
        2 => 'index',
        3 => 'delete',
        4 => 'view',
        5 => 'edit_using_modal',
        6 => 'add_product_spc'
    ),
    'specification_options' => array(
        0 => 'add',
        1 => 'edit',
        3 => 'delete',
        4 => 'view_options',
        5 => 'add_option',
        6 => 'add_modal_option',
        7 => 'edit_op'
    ),
    'products' => array(
        0 => 'add',
        1 => 'edit',
        2 => 'delete',
        3 => 'view',
        4 => 'index',
        5 => 'add_category',
        6 => 'edit_category',
        7 => 'active',
        8 => 'deactive',
        9 => 'verified',
        10 => 'deverified',
        11 => 'add_product_countries',
        12 => 'download_sample_file',
        13 => 'upload_products_excel',
        14 => 'pending_status',
        15 => 'category_listing_preview',
        16 => 'product_preview',
        17 => 'hide',
        18 => 'unhide',
        19 => 'update_prod_count_admin',
        20 => 'activate_product',
        21 => 'products_ready_active',
        22 => 'upload_company_reports',
        23 => 'transport_index',
        24 => 'update_company_reports'
    ),
    'product_specifications' => array(
        0 => 'delete',
    ),
    'product_categories' => array(
        0 => 'delete',
        1 => 'update_prod_count_admin'
    ),
    'enquiries' => array(
        0 => 'index',
        1 => 'view',
        2 => 'delete',
        3 => 'active',
        4 => 'deactive',
        5 => 'verified',
        6 => 'deverified',
        7 => 'edit_lead',
        8 => 'change_status_selected'
    ),
   /* 'testimonials' => array(
        0 => 'index',
        2 => 'view',
        3 => 'active',
        4 => 'deactive',
        5 => 'verified',
        6 => 'deverified',
    ),*/
    'testimonials' => array(
        0 => 'index',
        1 => 'edit',
        2 => 'view',
        3 => 'active',
        4 => 'deactive',
        5 => 'verified',
        6 => 'deverified',
        7=> 'add',
        8=>'delete'
    ),
    'settings' => array(
        0 => 'index',
        1 => 'add',
        2 => 'edit'
    ),
    'country_regions' => array(
        0 => 'delete'
    ),
    'our_clients' => array(
        0 => 'index',
        1 => 'add',
        2 => 'edit',
        3 => 'view',
        4 => 'active_logo',
        5 => 'deactive_logo',
        6 => 'delete'
    ),
    'orders' => array(
        0 => 'index',
        1 => 'view',
        2 => 'del_order'
    ),
    'cartItems' => array(
        0 => 'index',
        1 => 'view',
        3 => 'offer_price'
    ),
    'site_visits'=>array(
        0 => 'visit_report'
    ),
    'articles'=>array(
        0 => 'index',
        1 => 'add',
        2 => 'edit',
        4 => 'view',
        5 => 'article_listing',
        6 => 'article_details',
        7 => 'delete'
    ),
    'blogs'=>array(
        0 => 'index',
        1 => 'add',
        2 => 'edit',
        4 => 'view',
        // 5 => 'article_listing',
        // 6 => 'article_details',
        7 => 'delete'
    ),
    'slideshow'=>array(
        0 => 'index',
        1 => 'add',
        2 => 'edit',
        4 => 'view',
        5 => 'delete'
    ),
    'whyus'=>array(
        0 => 'index',
        1 => 'add',
        2 => 'edit',
        4 => 'view',
        5 => 'delete'
    ),
    'searchresultnotfound'=>array(
        0 => 'index',
        1 => 'add',
        2 => 'edit',
        4 => 'view',
        5 => 'delete'
    ),
    'articles_reports'=>array(
        0 => 'index',
        1 => 'add',
        2 => 'edit',
        4 => 'view',
        6 => 'delete'
    ),
    'candidates'=>array(
        0 => 'index',
        1 => 'add',
        2 => 'edit',
        3 => 'view'
    ),
    'mis'=>array(
        'mis_index',
        'reports_by_publisher',
        'vmr_reports_by_category',
        'leads_by_report',
        'products_crud',
        'product_view',
        'user_create',
        'user_edit'
    ),
);
$config['roles'] = $config;
?>
