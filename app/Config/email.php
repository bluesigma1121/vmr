<?php

/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 2.0.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * This is email configuration file.
 *
 * Use it to configure email transports of CakePHP.
 *
 * Email configuration class.
 * You can specify multiple configurations for production, development and testing.
 *
 * transport => The name of a supported transport; valid options are as follows:
 *  Mail - Send using PHP mail function
 *  Smtp - Send using SMTP
 *  Debug - Do not send the email, just return the result
 *
 * You can add custom transports (or override existing transports) by adding the
 * appropriate file to app/Network/Email. Transports should be named 'YourTransport.php',
 * where 'Your' is the name of the transport.
 *
 * from =>
 * The origin email. See CakeEmail::from() about the valid values
 *
 */
/*
use no-reply@m01.valuemarketresearch.com
in the from id
SMTP Hostname: smtp.mailgun.org
Default SMTP Login: postmaster@m01.decisiondatabases.com
Default Password: e9f54bc44d4a943a3bfdc274d3f7cfb2
 */
App::import('Controller', 'Configs');
$configs = new ConfigsController;
define('TRANSPORT', $configs->get_config_settings('smtp_transport'));
define('FROM_EMAIL', $configs->get_config_settings('smtp_from_email'));
define('FROM_NAME', $configs->get_config_settings('smtp_from_name'));
define('HOST', $configs->get_config_settings('smtp_host'));
define('PORT', $configs->get_config_settings('smtp_port'));
define('USERNAME', $configs->get_config_settings('smtp_username'));
define('PASSWORD', $configs->get_config_settings('smtp_password'));

class EmailConfig
{

    public $default = array(
        'transport' => 'Mail',
        'from' => 'you@localhost',
        //'charset' => 'utf-8',
        //'headerCharset' => 'utf-8',
    );
    public $smtp = array(
        'transport' => TRANSPORT,
        'from' => array('site@localhost' => 'My Site'),
        'host' => 'localhost',
        'port' => 25,
        'timeout' => 30,
        'username' => 'user',
        'password' => 'secret',
        'client' => null,
        'log' => false,
        //'charset' => 'utf-8',
        //'headerCharset' => 'utf-8',
    );
    public $fast = array(
        'from' => 'you@localhost',
        'sender' => null,
        'to' => null,
        'cc' => null,
        'bcc' => null,
        'replyTo' => null,
        'readReceipt' => null,
        'returnPath' => null,
        'messageId' => true,
        'subject' => null,
        'message' => null,
        'headers' => null,
        'viewRender' => null,
        'template' => false,
        'layout' => false,
        'viewVars' => null,
        'attachments' => null,
        'emailFormat' => null,
        'transport' => TRANSPORT,
        'host' => 'localhost',
        'port' => 25,
        'timeout' => 30,
        'username' => 'user',
        'password' => 'secret',
        'client' => null,
        'log' => true,
        //'charset' => 'utf-8',
        //'headerCharset' => 'utf-8',
    );
    public $forgot_password = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        // 'port' => 25,
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'forgot_password_email',
    );
    public $user_registration = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        // 'port' => 25,
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'user_registration',
        'bcc' => array('vaibhav@valuemarketresearch.com'),
    );
    public $reset_password = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        // 'port' => 25,
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'reset_password_email',
    );
    public $user_registration_enquiery = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        // 'port' => 25,
        'replyTo' => 'vaibhav@valuemarketresearch.com',
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'user_registration_enquiery',
        // 'bcc' => array('vaibhav@valuemarketresearch.com')
    );
    public $user_enquiery = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        'replyTo' => 'sales@valuemarketresearch.com',
        // 'port' => 25,
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'user_enquiery',
        // 'bcc' => array("vaibhav@valuemarketresearch.com")
    );
    /*Code for new sample enquiry mail Start VG-31/12/2016*/
    public $sample_request = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        //  'replyTo' => 'vaibhav@valuemarketresearch.com',
        // 'port' => 25,
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'new_sample_request',
        //    'bcc' => array("vaibhav@valuemarketresearch.com")
    );
    /*Code for new sample enquiry mail Start VG-31/12/2016*/

    public $user_enquiery_toc = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        // 'port' => 25,
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'user_enquiery',
        // 'bcc' => array("vaibhav@valuemarketresearch.com")
    );
    public $user_enquiery_received = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        'replyTo' => 'vaibhav@valuemarketresearch.com',
        // 'port' => 25,
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'user_enquiery_received',
        'bcc' => array("vaibhav@valuemarketresearch.com"),
    );

    public $user_enquiery_txn_fail = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        // 'port' => 25,
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'user_enquiery_txn_fail',
        'bcc' => array("vaibhav@valuemarketresearch.com"),
    );

    public $user_enquiery_txn_success = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        'replyTo' => 'vaibhav@valuemarketresearch.com',
        // 'port' => 25,
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'user_enquiery_txn_success',
        'bcc' => array("vaibhav@valuemarketresearch.com"),
    );

    public $user_enquiery_received_cc = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        // 'port' => 25,
        'port' => PORT,
        'replyTo' => 'vaibhav@valuemarketresearch.com',
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'user_enquiery_received_cc',
        'bcc' => array("vaibhav@valuemarketresearch.com"),
    );
    public $thanking_contact_us = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        // 'port' => 25,
        'port' => PORT,
        'replyTo' => 'vaibhav@valuemarketresearch.com',
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'thanking_contact_us',
        // 'bcc' => array("vaibhav@valuemarketresearch.com")
    );
    public $new_contact_message = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        // 'port' => 25,
        'replyTo' => 'vaibhav@valuemarketresearch.com',
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'new_contact_message',
        // 'bcc' => array("vaibhav@valuemarketresearch.com")
    );
    public $order_placed = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        'replyTo' => 'vaibhav@valuemarketresearch.com',
        // 'port' => 25,
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'order_placed',
        'bcc' => array("vaibhav@valuemarketresearch.com"),
    );

    public $order_placed_thankyou = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        'replyTo' => 'vaibhav@valuemarketresearch.com',
        // 'port' => 25,
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'order_placed_thankyou',
        //'bcc' => array("vaibhav@valuemarketresearch.com")
        'bcc' => array("vaibhav@valuemarketresearch.com"),
    );

    public $order_placed_hdfc = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        // 'port' => 25,
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'order_placed_hdfc',
        //'bcc' => array("vaibhav@valuemarketresearch.com")
        'bcc' => array("vaibhav@valuemarketresearch.com"),
    );

    public $admin_order_notification = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        // 'port' => 25,
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'admin_order_notification',
        //'bcc' => array("vaibhav@valuemarketresearch.com")
        'to' => array("vaibhav@valuemarketresearch.com"),

    );

    public $cron_job = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        // 'replyTo' => 'sales@valuemarketresearch.com',
        // 'port' => 25,
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'cron_html',
        // 'bcc' => array("vaibhav@valuemarketresearch.com")
    );

    public $cron_jobdetails = array(
        'transport' => TRANSPORT,
        'from' => array(FROM_EMAIL => FROM_NAME),
        'host' => HOST,
        // 'replyTo' => 'sales@valuemarketresearch.com',
        // 'port' => 25,
        'port' => PORT,
        'username' => USERNAME,
        'password' => PASSWORD,
        'emailFormat' => 'html',
        'template' => 'cronjobdetails',
        // 'bcc' => array("vaibhav@valuemarketresearch.com")
    );

}
