<?php

$config = array(
    'products' => array(
        1 => 'seo_index',
        2 => 'view',
        3 => 'seo_edit',
        4 => 'add',
        5 => 'view',
        6 => 'add_category',
        8 => 'pending_status',
        9 => 'index',
        10 => 'edit',
        11 => 'edit_category',
        //12 => 'delete',
        13 => 'active',
        14 => 'deactive',
        15 => 'verified',
        16 => 'deverified',
        17 => 'ready_to_active',
        18 => 'products_ready_active',
        19 => 'temp_edit_proddescription', 
        20 => 'add_faq',
        21 => 'edit_faq'                
    ),
    'users' => array(
        1 => 'change_password',
    ),
    // 'categories' => array(
    //     0 => 'tree_view_categories',
    //     1 => 'edit_tree_category',
    //     2 => 'tree_view_sub_categories',
    //     3 => 'display_active'
    // ),
    'categories' => array(
        0 => 'level_one',
        1 => 'level_two',
        2 => 'level_three',
        3 => 'level_four',
        4 => 'add_level_one',
        5 => 'add_level_two',
        6 => 'add_level_three',
        7 => 'add_level_four',
        //8 => 'delete',
        9 => 'view',
        10 => 'edit',
        11 => 'edit_level_one',
        12 => 'edit_level_two',
        13 => 'edit_level_three',
        14 => 'edit_level_four',
        15 => 'tree_view_categories',
        16 => 'ajax_result',
        17 => 'remove',
        18 => 'display',
        19 => 'active',
        20 => 'deactive',
        21 => 'change_status_selected',
        22 => 'add_level_five',
        23 => 'edit_level_five',
        24 => 'level_five',
        25 => 'add_level_six',
        26 => 'edit_level_six',
        27 => 'level_six',
        28 => 'category_publish',
        29 => 'category_unpublish',
        30 => 'download_sample_file',
        31 => 'upload_category_excel',
        32 => 'set_count',
        33 => 'find_sub_count',
        34 => 'find_count',
        35 => 'tree_view_sub_categories',
        36 => 'display_active'
    ),
    'specifications'=> array(
        0 => 'add_product_spc'
    ),
    // 'articles' => array(
    //     0 => 'index',
    //     1 => 'generate',
    // ),
    'articles'=>array(
        0 => 'index',
        1 => 'add',
        2 => 'edit',
        4 => 'view',
        5 => 'article_listing',
        6 => 'article_details',
        //7 => 'delete',
        8 => 'generate'
    ),
);
$config['roles'] = $config;
?>
