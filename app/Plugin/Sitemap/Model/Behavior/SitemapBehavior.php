<?php

class SitemapBehavior extends ModelBehavior {

    /**
     * _CacheKey - Cache Key
     *
     * @var string
     */
    protected $_CacheKey = 'Sitemap.ModelData.';

    /**
     * setup
     *
     * @param  Model  $Model    [description]
     * @param  array  $settings [description]
     * @return [type]           [description]
     */
    public function setup(Model $Model, $settings = array()) {
        if (!isset($this->settings[$Model->alias])) {
            $this->settings[$Model->alias] = array(
                'primaryKey' => 'id',
                'loc' => 'buildUrl',
                'lastmod' => 'modified',
                'changefreq' => 'daily',
                'priority' => '0.9',
                'conditions' => array(),
            );
        }
        $this->settings[$Model->alias] = array_merge($this->settings[$Model->alias], $settings);
    }

    /**
     * buildUrl - basic build URL function for the model behavior, basic URL using action => 'view'
     *
     * @return [type] [description]
     */
    public function buildUrl(Model $Model, $primaryKey) {
        return Router::url(array('plugin' => NULL, 'controller' => Inflector::tableize($Model->name), 'action' => 'view', $primaryKey), TRUE);
    }

/*    public function buildUrlProducts(Model $Model, $primaryKey) {

        $product = $Model->find('first', array(
            'conditions' => array('Product.id' => $primaryKey, 'Product.is_active' => 1),
            'fields' => array('Product.id', 'Product.product_name', 'Product.slug', 'Category.category_name', 'Category.slug'),
            'recursive' => 0
        ));

        $finalUrl = Router::url('/', true) . $product['Category']['slug'] . "/" . $product['Product']['id'] . "-" . $product['Product']['slug'];

        return $finalUrl;
    }
*/
    public function buildUrlProducts(Model $Model, $primaryKey) {

        $product = $Model->find('first', array(
            'conditions' => array('Product.id' => $primaryKey, 'Product.is_active' => 1),
            'fields' => array('Product.id', 'Product.slug'),
            'recursive' => -1
        ));

        $finalUrl = Router::url('/', true) . "ip/" . $product['Product']['id'] . "-" . $product['Product']['slug'];

        return $finalUrl;
    }

    public function buildUrlCategories(Model $Model, $primaryKey) {
        $category = $Model->find('first', array(
            'conditions' => array('Category.id' => $primaryKey, 'Category.is_active' => 1),
            'fields' => array('Category.id', 'Category.category_name'),
            'recursive' => -1
        ));
        if (!empty($category)) {
            //http://localhost/idata/product/1-country-database.html
            $finalUrl = Router::url('/', true) . "reports/" . $category['Category']['id'] . "-" . $this->cleanString($category['Category']['category_name']);
            return $finalUrl;
        }
        return false;
    }

    public function cleanString($string) {
        $string = str_replace(' ', '-', $string);
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $string = preg_replace('/-+/', '-', $string);
        return strtolower($string);
    }

    /**
     * generateSitemapData - generate the sitemap data, attempting to hit the cache for this data
     *
     * @param  Model  $Model [description]
     * @return [type]        [description]
     */
    public function generateSitemapData(Model $Model) {

        //Attempt to hit the Model Cache for data
        /*   $sitemapData = Cache::read($this->_CacheKey . $Model->name);

          if ($sitemapData !== false) {
          return $sitemapData;
          }
         */
        //Load the Model Data
        $modelData = $Model->find('all', array(
            'conditions' => $this->settings[$Model->alias]['conditions'],
            'recursive' => -1,
        ));

        //Build the sitemap elements
        $sitemapData = $this->_buildSitemapElements($Model, $modelData);
        //Write to the Cache
        Cache::write($this->_CacheKey . $Model->name, $sitemapData);
        //debug($sitemapData);die;
        return $sitemapData;
    }

    /**
     * _buildSitemapElements - build the sitemap elements
     *
     * @param  Model  $Model     [description]
     * @param  [type] $modelData [description]
     * @return [type]            [description]
     */
    protected function _buildSitemapElements(Model $Model, $modelData) {
        $sitemapData = array();

        //Loop through the Model data and create the array of elements for the sitemap
        foreach ($modelData as $key => $data) {
            $sitemapData[$key] = array();

            $sitemapData[$key]['loc'] = call_user_func(array($Model, $this->settings[$Model->alias]['loc']), $data[$Model->alias][$this->settings[$Model->alias]['primaryKey']]);

            if ($this->settings[$Model->alias]['lastmod'] !== FALSE) {
                $sitemapData[$key]['lastmod'] = $data[$Model->alias][$this->settings[$Model->alias]['lastmod']];
            }

            if ($this->settings[$Model->alias]['changefreq'] !== FALSE) {
                $sitemapData[$key]['changefreq'] = $this->settings[$Model->alias]['changefreq'];
            }

            if ($this->settings[$Model->alias]['priority'] !== FALSE) {
                $sitemapData[$key]['priority'] = $this->settings[$Model->alias]['priority'];
            }
        }
        return $sitemapData;
    }

}