<?php
App::uses('Whyus', 'Model');

/**
 * Whyus Test Case
 *
 */
class WhyusTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.whyus'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Whyus = ClassRegistry::init('Whyus');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Whyus);

		parent::tearDown();
	}

}
