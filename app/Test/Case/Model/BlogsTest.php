<?php
App::uses('Blogs', 'Model');

/**
 * Blogs Test Case
 *
 */
class BlogsTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.blogs'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Blogs = ClassRegistry::init('Blogs');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Blogs);

		parent::tearDown();
	}

}
