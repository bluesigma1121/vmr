<div class="row">
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Total Visits</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><?php echo $visit_count ?></h1>
            </div>
        </div>
    </div>
    
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Unique Visits</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><?php echo $unique_visits ?></h1>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Visit Report</h5> 
            </div>
            <div class="ibox-content">
                <table class="table table-bordered table-condensed">
                    <tr>
                        <th>IP</th>
                        <th>Hits</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Country</th>
                        <th>Last Visit On</th>
                    </tr>
                    <?php foreach ($stats as $key => $visit): ?>
                        <tr>
                            <td><?php echo $key ?></td>
                            <td><?php echo $visit['count'] ?></td>
                            <td><?php echo $visit['city'] ?></td>
                            <td><?php echo $visit['state'] ?></td>
                            <td><?php echo $visit['country'] ?></td>
                            <td><?php echo date('d-M-Y h:i A', strtotime($visit['date'])); ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</div>