<div class="recentViews view">
<h2><?php echo __('Recent View'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($recentView['RecentView']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($recentView['User']['title'], array('controller' => 'users', 'action' => 'view', $recentView['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($recentView['Product']['id'], array('controller' => 'products', 'action' => 'view', $recentView['Product']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($recentView['RecentView']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($recentView['RecentView']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Recent View'), array('action' => 'edit', $recentView['RecentView']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Recent View'), array('action' => 'delete', $recentView['RecentView']['id']), null, __('Are you sure you want to delete # %s?', $recentView['RecentView']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Recent Views'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Recent View'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
	</ul>
</div>
