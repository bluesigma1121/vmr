<div class="productRegions view">
<h2><?php echo __('Product Region'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($productRegion['ProductRegion']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($productRegion['Product']['id'], array('controller' => 'products', 'action' => 'view', $productRegion['Product']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo $this->Html->link($productRegion['Country']['id'], array('controller' => 'countries', 'action' => 'view', $productRegion['Country']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Region'); ?></dt>
		<dd>
			<?php echo $this->Html->link($productRegion['Region']['id'], array('controller' => 'regions', 'action' => 'view', $productRegion['Region']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($productRegion['ProductRegion']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($productRegion['ProductRegion']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Product Region'), array('action' => 'edit', $productRegion['ProductRegion']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Product Region'), array('action' => 'delete', $productRegion['ProductRegion']['id']), null, __('Are you sure you want to delete # %s?', $productRegion['ProductRegion']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Product Regions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product Region'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Regions'), array('controller' => 'regions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Region'), array('controller' => 'regions', 'action' => 'add')); ?> </li>
	</ul>
</div>
