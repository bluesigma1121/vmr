<script type="text/javascript">
    var base_path = "<?= Router::url('/', true) ?>";
        var selected_cat = null;
        $(document).ready(function() {
            $('#cat_div1').hide();
            $('.child_cat_div').hide();
            $('#add_lvl_two_cat_frm').bValidator();
            var i = 1;
            $('#add_child_option1').click(function() {
                //    $('#add_more_childs1').append('<tr id="display_child' + i + '"><td><input name="data[option_name][' + i + ']" class="form-control opt_name' + i + '" data-bvalidator="required" placeholder="Enter Child Name " type="text" id="option_name' + i + '" required="required"></td><td><input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger ml  " value="Remove" onclick=" del_row(' + i + ');" </td> </tr>');
                $('#add_more_childs1').append('<tr id="display_child' + i + '"><td><div class="col-md-4"><div class="input text required"><input name="data[option_name][' + i + '][category_name]" id="prnt_of_chld1" placeholder="Category Name " data-bvalidator="required" class="form-control" type="text" required="required"></div></div><div class="col-md-6"><div class="input textarea"><textarea name="data[option_name][' + i + '][short_desc]" id="prnt_of_chld1" placeholder="Short Description" data-bvalidator="required" class="form-control" cols="30" rows="6"></textarea></div></div><div class="col-md-2"><input type="button" name="minus' + i + '" id="minus ' + i + '" class="col-lg-1 btn btn-danger ml  " value="-" onclick=" del_row(' + i + ');" </div></td></tr>');
                i++;
            });
        });
        function del_row(i) {
            $("#display_child" + i).remove();
        }
</script>
<style>
    .ml{
        margin-left: 15px;
    }
</style>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Add Level-4 Category </h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('Category', array('id' => 'add_user', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-2">
            <div class="form-group">
                <label class="col-md-3 control-label">Select Level-3 Category Name:</label>
                <div class="col-md-4">
                    <?php echo $this->Form->input('parent_category_id1', array('id' => 'prnt_of_chld1', 'options' => $categories, 'empty' => 'Select', 'data-bvalidator' => 'required', 'class' => 'form-control', 'label' => false)); ?>
                </div>
                <div class="form-inline" id="cat_div1">
                    <div class="col-md-2 label_container">                        
                        <label id="parent_cat_type1"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Enter Level-4 Category Name :</label>
                <div id="plusminus_id" class="plusminus" style="margin-left: 15px;">
                    <a href="#" id="add_child_option1" class="btn btn-success"><i class="fa fa-plus">Add Level-4 Categories</i></a>
                </div>
            </div>
            <table class="row" id="add_child_option">
                <tbody role="alert" id="add_more_childs1" aria-live="polite" aria-relevant="all">
                    <?php /* <td>
                      <div class="col-md-3">
                      <?php echo $this->Form->input('category_name', array('id' => 'prnt_of_chld1', 'placeholder' => 'Category Name ', 'type' => 'text', 'data-bvalidator' => 'required', 'class' => 'form-control', 'label' => false)); ?>
                      </div>
                      <div class="col-md-3">
                      <?php echo $this->Form->input('short_desc', array('id' => 'prnt_of_chld1', 'type' => 'textarea', 'placeholder' => 'Short Description', 'data-bvalidator' => 'required', 'class' => 'form-control', 'label' => false)); ?>
                      </div>
                      <div class="col-md-4">
                      <?php echo $this->Form->input('long_desc', array('id' => 'prnt_of_chld1', 'type' => 'textarea', 'placeholder' => 'Long Description', 'data-bvalidator' => 'required', 'class' => 'form-control', 'label' => false)); ?>
                      </div>
                      <div class="col-md-2">
                      <?php echo $this->Form->button('remove', array('id' => 'prnt_of_chld1', 'class' => 'btn btn-danger ml', 'label' => false)); ?>
                      </div>
                      </td> */ ?>
                </tbody>
            </table>
            <div class="form-group"><label class="col-lg-3 control-label">Meta Title</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('meta_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Meta Name', 'label' => false,'')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-3 control-label">Meta Description</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('meta_desc', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Meta Description', 'label' => false,'')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-3 control-label">Meta Keywords</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('long_desc', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Meta Keywords', 'label' => false,'')); ?>
                </div>
            </div>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-primary btn-sm', 'div' => false)); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div> 

