<script type="text/javascript">
    $(document).ready(function() {
        $('#level_two').bValidator();
        $('#selecctall').click(function(event) {  //on click 
            if (this.checked) { // check select status
                $('.checkbox1').each(function() { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"               
                });
            } else {
                $('.checkbox1').each(function() { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                });
            }
        });

        $('.btn_bulk_active').click(function() {
            var usr_str = '';
            $("#category_index_tbl tr").each(function(i, row) {
                if ($(this).find("td:first").find('input:checked').prop('checked') == true) {
                    var val = $(this).find("td:first").find('input:checked').val();
                    if (usr_str != '') {
                        usr_str = usr_str + "," + val;
                    }
                    else {
                        usr_str = val;
                    }
                }
            });
            if (usr_str == '') {
                alert('Please select checkbox to perform action.');
                return false;
            }
            else {
                $("#selected_usr_field").val(usr_str);
            }
        });
    });
</script>
<style>
    .mtp{
        margin-top: 10px;
    }
    .search_mtp{
        margin-top: 3px;
    }
</style>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Manage Level-2 Categories</h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('Category', array('id' => 'level_two', 'class' => 'form-inline', 'role' => 'form')); ?>
        <div class="form-group">
            <?php
            echo $this->Form->input('search_by', array(
                'options' => array('category_name' => 'Level-2', 'parent_category_name' => 'Level-1'),
                'class' => 'form-control', 'label' => false));
            ?>
        </div>
        <?php
        if ($this->Session->check('lvl_3_cat_filter')) {
            $cat_filter = $this->Session->read('lvl_3_cat_filter');
            ?>            
            <div class="form-group">
                <?php echo $this->Form->input('search_text', array('type' => 'text', 'value' => $cat_filter['search_text'], 'placeholder' => 'Enter text to be search', 'class' => 'form-control', 'label' => false)); ?>
            </div>
        <?php } else { ?>
            <div class="form-group">
                <?php echo $this->Form->input('search_text', array('type' => 'text', 'autocomplete' => 'off', 'placeholder' => 'Enter text to be search', 'class' => 'form-control', 'label' => false)); ?>
            </div>
        <?php } ?>
        <div class="form-group">
            <?php echo $this->Form->button(' Search ', array('type' => 'submit', 'class' => 'btn btn-sm btn-warning fa fa-search', 'title' => 'Search')); ?>
        </div>
        <div class="form-group col-md-offset-1">
            <?php echo $this->Html->link(__('Clear Field'), array('action' => 'session_del', 3), array('class' => 'btn btn-sm btn-primary', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Clear Search',)); ?>
        </div>
        <div class="form-group col-md-offset-1">
            <?php echo $this->Html->link('Add Level-2 Category', array('action' => 'add_level_three'), array('class' => 'btn btn-sm btn-success', 'title' => 'Add Level-2 Category')) ?>                 
        </div>
        <?php echo $this->Form->end(); ?>            
        <?php if (AuthComponent::user('role') == 1): ?>
            <div class="row mtp">
                <div class="col-lg-12">
                    <?php echo $this->Form->create('Category', array('action' => 'change_status_selected', 'class' => 'form-inline', 'id' => 'bulk_actions')); ?>
                    <div class="col-lg-2">Bulk Action:
                        <?php echo $this->Form->input('selected_products', array('type' => 'hidden', 'id' => 'selected_usr_field')); ?>
                    </div>
                    <div class="col-lg-2">
                        <?php echo $this->Form->submit('Activate Selected', array('name' => 'activate', 'class' => 'btn   btn-outline btn-sm btn-primary dim tooltip-f btn_bulk_active', 'title' => 'Activate Selected')); ?>
                    </div>
                    <div class="col-lg-2">
                        <?php echo $this->Form->submit('Deactivate Selected', array('name' => 'deactivate', 'class' => 'btn   btn-outline btn-sm btn-warning dim tooltip-f btn_bulk_active', 'title' => 'Deactivate Selected')); ?>
                    </div>
                    <?php /*   <div class="col-lg-2">
                      <?php echo $this->Form->submit('Delete Selected', array('name' => 'delete', 'class' => 'btn   btn-outline btn-sm btn-danger dim tooltip-f btn_bulk_active', 'title' => 'Delete Selected')); ?>
                      </div> */ ?>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-striped" id="category_index_tbl">
            <thead>
                <tr> 
                    <?php if (AuthComponent::user('role') == 1): ?>
                        <th style="width: 10px;">
                            <input type="checkbox" id="selecctall"> All
                        </th>
                    <?php endif; ?>
                    <th><?php echo $this->Paginator->sort('category_name', 'Level Two'); ?></th> 
                    <th><?php echo $this->Paginator->sort('parent_category_id', 'Level One'); ?></th>
                    <th><?php echo $this->Paginator->sort('cat_disp_home_page', 'Display On Home'); ?></th>
                    <th><?php echo $this->Paginator->sort('is_active'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
            </thead>
            <?php
            foreach ($categories as $cate1) {
                ?>
                <tbody>
                    <?php if (AuthComponent::user('role') == 1): ?>
                    <td><input type="checkbox" class="checkbox1" value="<?= $cate1['Category']['id'] ?>" ></td>
                <?php endif; ?>
                <td><?php echo h($cate1['Category']['category_name']); ?>&nbsp;</td>
                <td><?php echo h($cate1['ParentCategory']['category_name']); ?>&nbsp;</td>
                <td><?php echo h($yes_no[$cate1['Category']['cat_disp_home_page']]); ?>&nbsp;</td>
                <td><?php echo h($yes_no[$cate1['Category']['is_active']]); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link(__('view'), array('action' => 'view', $cate1['Category']['id']), array('class' => 'btn btn-outline btn-sm btn-warning dim', 'title' => 'View')); ?>
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit_level_three', $cate1['Category']['id']), array('class' => 'btn btn-outline btn-sm btn-info dim', 'title' => 'Edit')); ?>                               
                    <?php echo $this->Html->link(__('Add Icon'), array('action' => 'font_awsome_icon', $cate1['Category']['id'], 3), array('class' => 'btn btn-outline btn-sm btn-success dim', 'title' => 'Add Icon')); ?>                               
                    <?php if (AuthComponent::user('role') == 1): ?>                  
                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $cate1['Category']['id']), array('class' => 'btn btn-outline btn-sm btn-danger dim', 'title' => 'Delete'), __('Are you sure you want to delete # %s?', $cate1['Category']['id'])); ?>
                    <?php endif; ?>
                    <?php
                    if (AuthComponent::user('role') == 1) {
                        if ($cate1['Category']['cat_disp_home_page'] == 1) {
                            echo $this->Html->link(__('Remove From Home'), array('action' => 'remove', $cate1['Category']['id'], 3), array('class' => 'btn btn-outline btn-sm btn-info', 'title' => 'Category Remove From Home'));
                        } else {
                            echo $this->Html->link(__('Display On Home'), array('action' => 'display', $cate1['Category']['id'], 3), array('class' => 'btn btn-outline btn-sm btn-info', 'title' => 'Category Display On Home'));
                        }
                    }
                    ?>
                    <?php
                    if (AuthComponent::user('role') == 1) {
                        if ($cate1['Category']['is_active'] == 1) {
                            echo $this->Html->link(__('Deactivate'), array('action' => 'deactive', $cate1['Category']['id']), array('class' => 'btn   btn-outline btn-sm btn-primary', 'title' => 'DeActive'));
                        } else {
                            echo $this->Html->link(__('Activate'), array('action' => 'active', $cate1['Category']['id']), array('class' => 'btn   btn-outline btn-sm btn-primary', 'title' => 'Active'));
                        }
                    }
                    ?>
                </td>
                </tbody>
            <?php } ?>
        </table>
        <div class="row col-md-12">
            <div class="dataTables_paginate paging_bootstrap">
                <p>
                    <?php
                    echo $this->Paginator->counter(array(
                        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                    ));
                    ?> </p>
                <ul class="pagination" style="visibility: visible;">
                    <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
                    <li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
                    <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
