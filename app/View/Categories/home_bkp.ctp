<!--<div class="slider-banner" id="main-slider-banner">
  <div class="slider-wrapper">
    <?php foreach ($slideshow as $key => $slides) { ?>
      <div class="slide-banner" data-image="<?= $this->webroot.'img/slideshow/' . $slides['Slideshow']['image']?>"></div>
    <?php } ?>
  </div>
</div>-->


<div class="slideshow">
    <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
        <!-- Carousel items -->
        <div class="carousel-inner carousel-inner-slider carousel-zoom">
            <?php foreach ($slideshow as $key => $slides) { ?>
                <div class="<?php if($key == 1){?>active<?php } ?> item"><img class="img-responsive" src="<?= $this->webroot.'img/slideshow/' . $slides['Slideshow']['image']?>">
                </div>
            <?php } ?>
            
       </div>
    </div>
</div>


<!-- start Search on home page -->
<div class="container">
  <div class="row search" style="margin:0px;">
    <div class="search-bar">
      <!--<div class="col-md-4" >-->
      <!--  <h4>Find Top Market Research Report here</h4>-->
      <!--</div>-->
      <div class="col-md-10 col-md-push-1">
        <div class="custom-search-form">
            <?php echo $this->Form->create('Category', array('type' => 'get','action'=>'home_search_form')); ?>
                <?php echo $this->Form->input('q', array('class' => 'form-control', 'label' =>false,'required','autocomplete'=>'off','id'=>'search_text')); ?>
                <div class="col-md-11 pull-left suggest-div" id="suggest"></div>
                <div class="col-md-1 col-sm-1 input-group-btn pull-right search-btn">
                  <button class="btn btn-primary" type="submit">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>
                </div>
            <?php  echo $this->Form->end(); ?>
        </div>
      </div>

      <div class="col-lg-12 text-center hidden-xs hidden-sm">
        <ul>
          <?php foreach ($research_cat as $rckey => $res_cat): ?>
            <li class="search-menu">
                  <a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($res_cat['Category']['category_name']))); ?>"><?php echo $res_cat['Category']['category_name']; ?></a>
            </li>
          <?php endforeach; ?>
          <li class="search-menu search-menu-all"><a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'category_list')); ?>">View all</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- End Search on home page -->

<!-- CLIENT LOGO SECTION START -->
<section id="services" class="services-section">
    <div class="container bgwhite">
        <div class="row">
            <div class="col-lg-12">
                <!--Item slider text-->
                <div class="">
                  <div class="row" id="slider-text">
                    <div class="header-section text-center">
                      <h2 class="">Our Clients</h2>
                      <hr class="bottom-line">
                    </div>
                  </div>
                </div>

                <!-- Item slider-->
                <div class="container-fluid row">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="carousel carousel-showmanymoveone slide" id="itemslider">
                        <div class="carousel-inner carousel-inner-pay">
                          <?php foreach ($ourclients as $key => $clients) { ?>

                            <div class="item <?= $key == 0?'active':''?> payotn">
                              <div class="col-xs-12 col-sm-6 col-md-2">
                                <?php if (!empty($clients['OurClient']['logo'])) { ?>
                                    <a href="http://<?php echo $clients['OurClient']['link']; ?>" target="blank">
                                      <?= $this->Html->image('client_images/' . $clients['OurClient']['logo'], array('class' => '')); ?>
                                    </a>
                                <?php } else {
                                        echo $this->Html->image('noimage.png', array('class' => ''));
                                      } ?>
                              </div>
                            </div>
                          <?php } ?>
                        </div>

                        <div id="slider-control">
                        <a class="left carousel-control" href="#itemslider" data-slide="prev">
                          <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                        </a>
                        <a class="right carousel-control" href="#itemslider" data-slide="next">
                          <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
                        </a>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Item slider end-->
            </div>
        </div>
    </div>
</section>
<!-- CLIENT LOGO SECTION END -->

<!-- WHY CHOSSE US SECTION START -->
<section id="testimonial" class="section-padding">
  <div class="bg-color">
  <div class="container">
    <div class="row">
      <div class="header-section text-center padd-botm">
        <h2 class="white">Why Choose Us</h2>
        <hr class="bottom-line">
      </div>
      <?php foreach ($whyus as $key => $why) { ?>
        <div class="col-md-3">
          <div class="bg-box">
            <?php switch ($key) {
              case 0:
                echo '<i class="fa fa-bullseye" aria-hidden="true"></i>';
                break;
              case 1:
                echo '<i class="fa fa-cogs" aria-hidden="true"></i>';
                break;
              case 2:
                echo '<i class="fa fa-address-card-o" aria-hidden="true"></i>';
                break;
              case 3:
                echo '<i class="fa fa-globe" aria-hidden="true"></i>';
                break;
            }?>

            <h3><?=$why['Whyus']['title']?></h3>
            <p><?=$why['Whyus']['description'] ?></p>
          </div>
        </div>
      <?php } ?>

    </div>
  </div>
</div>
</section>
<!-- WHY CHOSSE US SECTION END -->


<!-- TESTIMONIALS SECTION START -->
<section class="testibg">
<div class="container">
  <div class="row">
    <div class="col-md-12 col-sm-12">
      <div class="price-table testimonial-marg-botm">
        <div class="header-section text-center">
          <h2 class="">Testimonials</h2>
          <hr class="bottom-line">
        </div>
          <!-- Plan  -->
        <div class="pricing-head">
          <div id="myCarousel" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel">
            <!-- Carousel items -->
            <div class="carousel-inner">
              <?php foreach ($testimonial as $key => $testimonials) { ?>
                <div class="item <?= $key == 0?'active':''?>">
                    <div class="row">
                         <div class="col-xs-12 col-sm-12 col-md-12 testimonial-text">
                            <p> <i class="fa fa-quote-left fa-lg" aria-hidden="true"></i><?php echo " \" "; echo  $testimonials['Testimonial']['testimonial_description'] ;echo" \" "; ?></p>
                            <h5><i class="fa fa-user-circle fa-lg" aria-hidden="true"></i> <?php echo $testimonials['Testimonial']['testimonial_title'];?></h5>
                            <!-- <h6>- Position</h6> -->
                        </div>
                    </div>
                    <!--/row-fluid-->
                  </div>
                <?php } ?>
                <!--/item-->
            </div>
            <div class="row">
                <!-- <div class="col-md-4">
                    <span data-slide="prev" class="btn-vertical-slider glyphicon glyphicon-circle-arrow-down"
                        style="color: Black; font-size: 30px"></span>
                </div> -->
            </div>
          </div>
          <div class="view-more-btn">
            <a href="<?php echo Router::url(array('controller' => 'testimonials', 'action' => 'listing' )); ?>">View More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<!-- TESTIMONIALS SECTION END -->


<!--Pricing-->
<section id ="pricing" class="section-padding">
  <div class="container">
    <div class="row">

      <div class="col-md-8 col-sm-8">
        <div class="price-table">
          <div class="price-in mart-15">
            <a href="#." class="btn btn-bg green btn-block"><i class="fa fa-address-card-o fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;FEATURED REPORTS</a>
          </div>
          <!-- Plan  -->

          <?php foreach ($home_products as $home_product): ?>
            <div class="pricing-head">
                <a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details','slug' => $home_product['Product']['slug'] ));?>">
                    <h5 class="recent-rpt">
                      <i class="fa fa-line-chart" aria-hidden="true"></i>                
                      <p>
                        <?= $this->Text->truncate($home_product['Product']['product_name'],180,array(
                                                                                'ellipsis' => '...','exact' => false)) ?>
                      </p>
                    </h5>
                </a>
              <p>
                <?php echo $this->Text->truncate(strip_tags($home_product['Product']['product_description']), 150, array(
                                                                            'ellipsis' => '...','exact' => false)
                                                                            );?>
              </p>
              <!-- <h6>26 January, 2018</h6> -->
            </div>
          <?php endforeach; ?>
        </div>
      </div>
      <div class="col-md-4 col-sm-4">
        <div class="price-table">
          <div class="price-in mart-15">
            <a href="#." class="btn btn-bg green btn-block"><i class="fa fa-newspaper-o fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp; PRESS RELEASES</a>
          </div>
          <!-- Plan  -->
          <?php foreach ($press_releases as $key => $press_relese): ?>
            <div class="pricing-head">
              <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'article_details', 'folder' => 'press-releases',  'url_slug' => $this->Link->cleanString($press_relese['Article']['slug']))); ?>">
                <p><?php echo $press_relese['Article']['headline'] ?></p>
              </a>
              <!-- <h6>26 January, 2018</h6> -->
            </div>
          <?php endforeach; ?>
        </div>
      </div>      
      <!--<div class="col-md-4 col-sm-4">
        <div class="price-table">
          <div class="price-in mart-15">
            <a href="#." class="btn btn-bg green btn-block"><i class="fa fa-newspaper-o fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;ARTICLES</a>
          </div>
          
          <?php foreach ($articles as $key => $article): ?>
            <div class="pricing-head">
              <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'article_details', 'folder' => 'analysis', 'article_id' => $article['Article']['id'], 'url_slug' => $this->Link->cleanString($article['Article']['slug']))); ?>">
                <p><?php echo $article['Article']['headline'] ?></p>
              </a>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
      -->
      
    </div>
  </div>
</section>
<!--/ Pricing-->



<!-- RESEARCH INDUSTRIES SECTION START -->
<!--<section id="research">
<div class="section-padding bg-grey">
  <div class="container">
    <aside class="leftside col-lg-3 col-md-3 col-sm-4 col-xs-12">
      <div class="header-section text-center">
          <h2><i class="fa fa-line-chart" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;Research Industries</h2>
      </div>
      <nav id='cssmenu'>
        <div id="head-mobile"></div>
        <div class="button"></div>

          <ul>
              
            <?php foreach ($research_ind['Level2'] as $key => $value): ?>
              <?php if($key == 5){
                break;
              }?>
              <li><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $value['id'], 'slug' => $this->Link->cleanString($value['category_name']))); ?>"><?= $value['category_name'] ?></a>
                <?php if(!empty($value['Level3'])){?>
                   <ul>

                     <?php foreach ($value['Level3'] as $subkey => $subvalue): ?>
                        <li><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $subvalue['id'], 'slug' => $this->Link->cleanString($subvalue['category_name']))); ?>"><?= $subvalue['category_name']?></a>
                          <?php if(!empty($subvalue['Level4'])){?>
                           <ul>
                              <?php foreach ($subvalue['Level4'] as $lev3key => $lev3value): ?>
                                <li><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $lev3value['id'], 'slug' => $this->Link->cleanString($lev3value['category_name']))); ?>"><?= $lev3value['category_name'] ?></a>
                                  <?php if(!empty($lev3value['Level5'])){?>
                                   <ul>
                                      <?php foreach ($lev3value['Level5'] as $lev4key => $lev4value): ?>
                                        <li><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $lev4value['id'], 'slug' => $this->Link->cleanString($lev4value['category_name']))); ?>"><?= $lev4value['category_name'] ?></a>
                                          <?php if(!empty($lev4value['Level6'])){?>
                                           <ul>
                                              <?php foreach ($lev4value['Level6'] as $lev5key => $lev5value): ?>
                                                <li><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $lev5value['id'], 'slug' => $this->Link->cleanString($lev5value['category_name']))); ?>"><?= $lev5value['category_name'] ?></a>
                                                  <?php if(!empty($lev5value['Level7'])){?>
                                                   <ul>
                                                      <?php foreach ($lev5value['Level7'] as $lev6key => $lev6value): ?>
                                                        <li><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $lev6value['id'], 'slug' => $this->Link->cleanString($lev6value['category_name']))); ?>"><?= $lev6value['category_name'] ?></a></li>
                                                      <?php endforeach; ?>
                                                   </ul>
                                                 <?php } ?>
                                                </li>
                                              <?php endforeach; ?>
                                           </ul>
                                         <?php } ?>
                                        </li>
                                      <?php endforeach; ?>
                                   </ul>
                                 <?php } ?>
                                </li>
                              <?php endforeach; ?>
                           </ul>
                         <?php } ?>
                        </li>
                      <?php endforeach; ?>
                   </ul>
                <?php } ?>
              </li>
            <?php endforeach; ?>
          </ul>
        </nav>
      </aside>-->
      <!-- <aside class="leftside col-lg-3 col-md-3 col-sm-4 col-xs-12">
        <div class="header-section text-center">
            <h2><i class="fa fa-line-chart" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;Research Industries</h2>
       </div>
      <button class="btn visible-xs-block btn-raised btn-default btn-block" type="button" data-toggle="collapse" data-target="#mobilkat" aria-expanded="false" aria-controls="mobilkat">
      Click to View all Categories
      </button>
      <div class="collapse navbar-collapse research-dropdown" id="mobilkat">
        <ul class="nav navbar-nav navbar-dikey">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Healthcare<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Biotechnology</a></li>
              <li><a href="#">Healthcare IT</a></li>
              <li><a href="#">Medical Devices</a></li>
              <li><a href="#">Pharmaceuticals</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Information &amp; Communication Technology<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Cloud Computing</a></li>
              <li><a href="#">Computer Hardware &amp; Networking</a></li>
              <li><a href="#">Data Center</a></li>
              <li><a href="#">Enterprise Computing</a></li>
              <li><a href="#">E-commerce &amp; IT Outsourcing</a></li>
              <li><a href="#">Information Technology</a></li>
              <li><a href="#">Software and Services</a></li>
              <li><a href="#">Technology &amp; Media</a></li>
              <li><a href="#">Telecommunication &amp; Wireless</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Semiconductors<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Display Technologies</a></li>
              <li><a href="#">Electronics</a></li>
              <li><a href="#">Electronic Systems &amp; Devices</a></li>
              <li><a href="#">Emerging &amp; Next Generation Technologies</a></li>
              <li><a href="#">Robotics &amp; Automation Technologies</a></li>
              <li><a href="#">Sensors &amp; Controls</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Chemicals &amp; Materials<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Sub Menu 1</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Energy &amp; Power<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Sub Menu 1</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Food &amp; Beverage<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Sub Menu 1</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Mining, Minerals and Metals<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Sub Menu 1</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Aerospace and Defense<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Sub Menu 1</a></li>
            </ul>
          </li>
            </ul>
            </div>
      </aside> -->
      <!--<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 padd-right">
        <div class="rightside leftside">
          
          <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title hidden-xs">Market Research Reports</h3>
                    <span class="pull-right">
                        
                        <ul class="nav panel-tabs">
                          <?php $cnt=0;?>
                          <?php foreach ($research_cat as $rckey => $res_cat): ?>
                            <?php App::import('Model', 'Category');App::import('Model', 'ProductCategory');?>
                            <?php $this->Category = new Category();$all_cate = $this->Category->find_sub_cat_ids($res_cat['Category']['id']); ?>
                            <?php $this->Product = new Product();
                            //   $product = $this->Product->find('all', array('conditions' => array('Product.category_id' => $all_cate), array('limit' => 5), 'fields' => array('Product.id', 'Product.product_name','Product.id','Product.product_description','Product.category_id','Product.slug')));
                            $product = $this->Product->query('SELECT Product.id, Product.product_name,Product.product_description,Product.category_id,Product.slug FROM `products` as Product INNER JOIN product_categories ON Product.id = product_categories.product_id WHERE product_categories.category_id='.$res_cat['Category']['id'].' LIMIT 6');
                              foreach ($product as $pr_key => $pro) {
                                  $main_cat = $this->Product->find_main_cat($pro['Product']['category_id']);
                                  $product[$pr_key]['main_cat'] = $main_cat['Category'];
                              }
                            ?>
                            <?php if(!empty($res_cat)){?>
                              <li class="<?=$rckey == $cnt ? 'active':''?>"><a href="#tab<?=$rckey+1?>" data-toggle="tab"><?php echo $res_cat['Category']['category_name']; ?></a></li>
                            <?php }else{
                              $cnt++;
                            } ?>

                          <?php endforeach; ?>
                          <li class="view-btn"><a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'category_list')); ?>">View all</a></li>
                        </ul>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                      <?php $cnt=0;?>
                      <?php foreach ($research_cat as $rckey => $res_cat): ?>
                        <?php App::import('Model', 'Category');App::import('Model', 'ProductCategory');?>
                        <?php $this->Category = new Category();$all_cate = $this->Category->find_sub_cat_ids($res_cat['Category']['id']); ?>
                        <?php $this->Product = new Product();
                          //$product = $this->Product->find('all', array('conditions' => array('Product.category_id' => $all_cate), array('limit' => 5), 'fields' => array('Product.id', 'Product.product_name','Product.id','Product.product_description','Product.category_id','Product.slug'))); 
                          $product = $this->Product->query('SELECT Product.id, Product.product_name,Product.product_description,Product.category_id,Product.slug FROM `products` as Product INNER JOIN product_categories ON Product.id = product_categories.product_id WHERE product_categories.category_id='.$res_cat['Category']['id'].' LIMIT 6');
                          ?>
                          <?php foreach ($product as $pr_key => $pro) {
                              $main_cat = $this->Product->find_main_cat($pro['Product']['category_id']);
                              $product[$pr_key]['main_cat'] = $main_cat['Category'];
                          }?>
                          <?php if(!empty($product)){?>
                            <div class="tab-pane <?=$rckey == $cnt ? 'active':''?>" id="tab<?=$rckey+1?>">
                              <div class="feature-info">

                                <?php foreach ($product as $key => $products): ?>
                                  <div class="fea">
                                    <div class="col-md-4">
                                      <div class="heading pull-right">
                                        <i class="fa fa-line-chart" aria-hidden="true"></i>
                                        <h4 class="research-rpt">
                                          <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($products['main_cat']['slug']), 'id' => $products['Product']['id'], 'slug' => $products['Product']['slug'])); ?> " >
                                            <?= $this->Text->truncate($products['Product']['product_name'],50,array(
                                                                            'ellipsis' => '...','exact' => false)) ?>
                                          </a>
                                        </h4>
                                        <p><?php echo $this->Text->truncate(strip_tags($products['Product']['product_description']), 150, array(
                                                                            'ellipsis' => '...','exact' => false)
                                                                            );?>
                                        </p>
                                      </div>
                                    </div>
                                  </div>
                                  <?php if($key == 5){
                                    break;
                                  }?>
                                <?php endforeach; ?>
                            </div>
                          </div>
                        <?php }else{
                          $cnt++;
                        } ?>
                      <?php endforeach; ?>

                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

-->


<script>
(function($) {
$.fn.menumaker = function(options) {
 var cssmenu = $(this), settings = $.extend({
   format: "dropdown",
   sticky: false
 }, options);
 return this.each(function() {
   $(this).find(".button").on('click', function(){
     $(this).toggleClass('menu-opened');
     var mainmenu = $(this).next('ul');
     if (mainmenu.hasClass('open')) {
       mainmenu.slideToggle().removeClass('open');
     }
     else {
       mainmenu.slideToggle().addClass('open');
       if (settings.format === "dropdown") {
         mainmenu.find('ul').show();
       }
     }
   });
   cssmenu.find('li ul').parent().addClass('has-sub');
multiTg = function() {
     cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
     cssmenu.find('.submenu-button').on('click', function() {
       $(this).toggleClass('submenu-opened');
       if ($(this).siblings('ul').hasClass('open')) {
         $(this).siblings('ul').removeClass('open').slideToggle();
       }
       else {
         $(this).siblings('ul').addClass('open').slideToggle();
       }
     });
   };
   if (settings.format === 'multitoggle') multiTg();
   else cssmenu.addClass('dropdown');
   if (settings.sticky === true) cssmenu.css('position', 'fixed');
resizeFix = function() {
  var mediasize = 1000;
     if ($( window ).width() > mediasize) {
       cssmenu.find('ul').show();
     }
     if ($(window).width() <= mediasize) {
       cssmenu.find('ul').hide().removeClass('open');
     }
   };
   resizeFix();
   return $(window).on('resize', resizeFix);
 });
  };
})(jQuery);

(function($){
$(document).ready(function(){
$("#cssmenu").menumaker({
   format: "multitoggle"
});
});
})(jQuery);
</script>
