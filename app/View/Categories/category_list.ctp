  <style>
    .search-block {
        padding: 0;
        text-align: center;
        position: relative;
        margin-bottom: 20px;
        /* background: url(../img/front/bg/7.jpg) 50% 0 repeat fixed; */
    }
</style>
<script>
    $(document).ready(function () {
        $('#collapse-One292').collapse('show');
    });
</script>

<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main bg-color-inner-report">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>Industries</h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url(array('/',true)); ?>">Home</a></li>
              <li><a>Industries</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->
<style>.page-heading{color: #666;font-size: 14px;text-transform: uppercase;font-weight: 700;}</style>
<section class="about-inner report-inner-bg">
  <div class="container">
    
    <div class="row">
        <!-- Begin Content -->
        <div class="row">
          <div class="col-md-8 col-sm-8 ">
            <h1 class="page-heading">Industries</h1>
          </div>
        </div>
        <div class="blog-inner report-page report-details-page col-md-8 col-sm-8">
            <!-- Accordion v1 -->
            <div class="panel-group acc-v1" id="accordion-1">
                <?php foreach ($cat_data['Level2'] as $key1 => $cat): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <?php if ($cat['id'] == $id) { ?>
                                    <a class="accordion-toggle" data-toggle="collapse" aria-expanded="true"  data-parent="#accordion-1" href="#collapse-One<?php echo $cat['id'] ?>">
                                        <?php echo $cat['category_name'] ?>(<?php echo $cat['no_of_product'] ?>)<br>

                                    </a>
                                <?php } else { ?>
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-One<?php echo $cat['id'] ?>">
                                        <?php echo $cat['category_name'] ?> (<?php echo $cat['no_of_product']; ?>)
                                    </a>
                                <?php } ?>
                            </h4>
                        </div>

                        <?php if ($cat['id'] == $id) { ?>
                            <div id="collapse-One<?php echo $cat['id'] ?>" class="panel-collapse collapse in" style="height: 0px;">
                        <?php } else { ?>
                            <div id="collapse-One<?php echo $cat['id'] ?>" class="panel-collapse collapse" style="height: 0px;">
                        <?php } ?>

                        <div class="panel-body">
                          <div class="row">
                            <div class="col-md-10">
                              <ul class="list-unstyled">
                                <li>
                                  <i class="fa fa-angle-double-right color-green"></i>
                                  <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat['category_name']))); ?> "><?php echo $cat['category_name'] ?> (<?php echo $cat['no_of_product']; ?>)</a>

                                </li>
                                <?php if (!empty($cat['Level3'])) { ?>
                                  <ul>
                                    <?php foreach ($cat['Level3'] as $key2 => $cat2): ?>
                                    <li>
                                      <i class="fa fa-angle-double-right color-green"></i>
                    
                                      <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat2['category_name']))); ?> "><?php echo $cat2['category_name'] ?> (<?php echo $cat2['no_of_product']; ?>)</a>
                                      <?php if (!empty($cat2['Level4'])) { ?>
                                      <ul>
                                        <?php foreach ($cat2['Level4'] as $key3 => $cat3): ?>
                                          <li><i class="fa fa-angle-double-right color-green"></i>   
                                            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat3['category_name']))); ?>"><?php echo $cat3['category_name'] ?> (<?php echo $cat3['no_of_product']; ?>)</a>
                                          <?php if (!empty($cat3['Level5'])) { ?>
                                            <ul>
                                              <?php foreach ($cat3['Level5'] as $key4 => $cat4): ?>
                                                <li class="list-unstyled">
                                                  <i class="fa fa-angle-double-right color-green"></i>
                                                  <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat4['category_name']))); ?>"><?php echo $cat4['category_name'] ?>(<?php echo $cat4['no_of_product']; ?>)</a>    
                                                  <?php if (!empty($cat4['Level6'])) { ?>
                                                    <ul>
                                                      <?php foreach ($cat4['Level6'] as $key5 => $cat5): ?>
                                                        <li class="list-unstyled">
                                                          <i class="fa fa-angle-double-right color-green"></i>
                                                          <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat5['category_name']))); ?>"><?php echo $cat5['category_name'] ?>(<?php echo $cat5['no_of_product']; ?>)</a>
                                                        </li>
                                                      <?php endforeach; ?>
                                                    </ul>
                                                  <?php } ?>
                                                </li>
                                              <?php endforeach; ?>
                                            </ul>
                                          <?php } ?>
                                        </li>
                                      <?php endforeach; ?>
                                    </ul>
                                  <?php } ?>
                                <?php endforeach; ?>
                              </li>
                            </ul>
                          <?php } ?>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <hr>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
        <!--<div class="col-md-3 section-block">
          <div class="headline"><h4>Recently Viewed</h4></div>
          <ul>
            <?php foreach ($recent_view as $key => $rec_view) { ?>
              <li><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($rec_view['Category']['category_name']), 'id' => $rec_view['Product']['id'], 'slug' => $rec_view['Product']['slug'])); ?> "><h5><?php echo $rec_view['Product']['product_name']; ?></h5></a></li>
            <?php } ?>
          </ul>
          <div class="clearfix"></div>
        </div>-->
        
        <div class="col-md-4 col-xs-12 col-sm-4 padd-right">
        <div class="report-page relat-reprt">

          <h5>Latest Reports</h5>
          <?php if (!empty($home_products) && isset($home_products)): ?>
            <?php foreach ($home_products as $key => $product): ?>
              <?php if (!empty($product['Product']['id'])): ?>
                
                  <div class="pricing-head">
                      <a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details','slug' => $product['Product']['slug'] ));?>">
                          <h5 class="recent-rpt recent-rpt-list">
                            <i class="fa fa-line-chart" aria-hidden="true"></i>                
                            <p>
                              <?= $product['Product']['alias'] ?>
                            </p>
                          </h5>
                      </a>
                  </div>                    
              <?php endif; ?>
            <?php endforeach; ?>
          <?php endif; ?>
          
          <!-- <h5>Recently Viewed</h5>
          <?php if (!empty($recent_view) && isset($recent_view)): ?>
            <?php foreach ($recent_view as $key => $rec_view): ?>
              <?php if (!empty($rec_view['Product']['id'])): ?>
                    
                          <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'slug' => $rec_view['Product']['slug'])); ?>">
                          <h3><?php echo $rec_view['Product']['alias']; ?></h3>
                          <p></p>
                        </a>
                    
              <?php endif; ?>
            <?php endforeach; ?>
          <?php endif; ?> -->
        </div>
      </div>
        
        
      </div>
    </div>
  </div>
</section>
