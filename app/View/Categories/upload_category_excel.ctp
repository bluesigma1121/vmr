<script>
    $(document).ready(function() {
        $('#upload_pro').bValidator();

        $("#parent1").multiselect({
            multiple: false,
            noneSelectedText: "Select Category",
            selectedList: 1}).multiselectfilter({});
    });
</script> 
<style>
    .err_msg{
        color: red;
    }
</style>
<div class="page-content">
    <div class="widget-box top_margin">
        <div class="widget-header widget-header-blue widget-header-flat">
            <h4>Upload Categories</h4>           
        </div>
        <div class="widget-body">
            <div class="portlet-body form">
                <?php echo $this->Form->create('Category', array('id' => 'upload_pro', 'class' => 'form-horizontal', 'type' => 'file', 'role' => 'form')); ?>
                <div class="form-body top_margin">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Choose  Category:</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('category_id', array('id' => 'parent1', 'options' => $all_categories, 'empty' => 'Select', 'class' => 'form-control', 'label' => false, 'data-bvalidator' => 'required')); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputFile" class="col-md-3 control-label">Category Excel:</label>
                    <div class="col-md-4">
                        <?php echo $this->Form->input('category_upload', array('type' => 'file', 'id' => 'exampleInputFile', 'class' => 'form-control', 'data-bvalidator' => 'required,extension[xls]', 'data-bvalidator-msg' => 'Please select file of type .xls', 'placeholder' => 'Select File', 'label' => false)); ?>
                    </div>
                    <div class="col-md-4">
                        <a href="<?= Router::url(array('controller' => 'categories', 'action' => 'download_sample_file')); ?>" class="btn btn-sm btn-primary color-black tooltips" data-toggle="tooltip" data-placement="top" title="Download Excel file">Download Sample File</a>  
                    </div>
                </div>
                <div class="form-actions text-center">
                    <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <button onclick="goBack();" class="btn btn-warning btn-sm">Back</button>                   
                    <?php //echo $this->Html->link(__('Back'), array('action' => 'tr'), array('class' => 'btn btn-warning btn-sm', 'div' => false)); ?>
                </div>
                <?php echo $this->Form->end(); ?> 
            </div>
        </div>
    </div>

    <hr>
    <div class="row">
        <p class="err_msg">
            * All failed Categories are exported into Excel
        </p>
    </div>
</div>