<?php

$this->Xls->setHeader('Failed Category Excel');

$this->Xls->addXmlHeader();
$this->Xls->setWorkSheetName('Failed Category Excel');

$this->Xls->openRow();
$this->Xls->writeString('Category Name');
$this->Xls->writeString('Category Description');
$this->Xls->writeString('Page Title');
$this->Xls->writeString('Page Description');
$this->Xls->writeString('Reasons');
$this->Xls->closeRow();
$this->Xls->openRow();
$this->Xls->closeRow();

foreach ($category_fails as $key => $data):
    $this->Xls->openRow();
    $this->Xls->writeString($data['category_name']);
    $this->Xls->writeString($data['short_desc']);
    $this->Xls->writeString($data['page_title']);
    $this->Xls->writeString($data['page_desc']);
    $this->Xls->writeString($data['error']);
    $this->Xls->closeRow();
endforeach;

$this->Xls->addXmlFooter();
exit();
?>