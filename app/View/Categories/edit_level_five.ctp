<script type="text/javascript">
    var base_path = "<?= Router::url('/', true) ?>";
    $(document).ready(function() {
        $('#edit_lvl_four_parent_frm').bValidator();
    });
</script>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Edit <?= ucwords($category_name['Category']['category_name']);?> </h5>
    </div>
    <div class="ibox-content"> 
        <?php echo $this->Form->create('Category', array('id' => 'edit_lvl_four_parent_frm', 'class' => 'form-horizontal', 'role' => 'form')); ?>
        <div class="form-group">
            <label class="col-md-3 control-label">Select Level-3 Category Name:</label>
            <div class="col-md-4">
                <?php echo $this->Form->input('parent_category_id', array('id' => 'prnt_of_chld1', 'options' => $categories, 'class' => 'form-control', 'label' => false, 'data-bvalidator' => 'required')); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Level-4 Category Name:</label>
            <div class="col-md-4">
                <?php echo $this->Form->input('category_name', array('type' => 'text', 'class' => 'form-control', 'label' => false, 'data-bvalidator' => 'required')); ?>
            </div>
        </div>

        <div class="form-group"><label class="col-lg-3 control-label">Description</label>
            <div class="col-lg-4">
                <?php echo $this->Form->input('short_desc', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Enter Description', 'label' => false)); ?>
            </div>
        </div>
        <div class="form-group"><label class="col-lg-3 control-label">Meta Title</label>
            <div class="col-lg-4">
                <?php echo $this->Form->input('meta_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Meta Name', 'label' => false,'')); ?>
            </div>
        </div>
        <div class="form-group"><label class="col-lg-3 control-label">Meta Description</label>
            <div class="col-lg-4">
                <?php echo $this->Form->input('meta_desc', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Meta Description', 'label' => false,'')); ?>
            </div>
        </div>
        <div class="form-group"><label class="col-lg-3 control-label">Meta Keywords</label>
            <div class="col-lg-4">
                <?php echo $this->Form->input('long_desc', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Meta Keywords', 'label' => false,'')); ?>
            </div>
        </div>
        <div class="text-center">
            <?php echo $this->Form->button('Submit', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
        </div>
        <?php echo $this->Form->end(); ?> 
    </div>
</div>







