<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Category Details of  <?= ucwords($category['Category']['category_name']); ?> </h5>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="form-group">
                <p class="col-md-10">
                    <?php if ($category['Category']['level'] == 1): ?>       
                    <?php echo $this->Html->link('Edit', array('action' => 'edit_level_one', $category['Category']['id']), array('class' => 'btn btn-success fa fa-pencil color-black tooltips')); ?>
                    <?php echo $this->Html->link(' Add Level-2 Category', array('controller' => 'categories', 'action' => 'add_level_one'), array('class' => 'btn btn-info fa fa-plus color-black tooltips', 'div' => false)); ?>                              
                    <?php elseif ($category['Category']['level'] == 2): ?>
                    <?php echo $this->Html->link('Edit', array('action' => 'edit_level_two', $category['Category']['id']), array('class' => 'btn btn-success fa fa-pencil color-black tooltips')); ?>
                    <?php echo $this->Html->link(' Add Level-3 Category', array('controller' => 'categories', 'action' => 'add_level_two'), array('class' => 'btn btn-info fa fa-plus color-black tooltips', 'div' => false)); ?>                            
                    <?php elseif ($category['Category']['level'] == 3): ?>
                    <?php echo $this->Html->link('Edit', array('action' => 'edit_level_three', $category['Category']['id']), array('class' => 'btn btn-success fa fa-pencil color-black tooltips')); ?>
                    <?php echo $this->Html->link(' Add Level-4 Category', array('controller' => 'categories', 'action' => 'add_level_three'), array('class' => 'btn btn-info fa fa-plus color-black tooltips', 'div' => FALSE)); ?>                                 
                    <?php elseif ($category['Category']['level'] == 4): ?>
                    <?php echo $this->Html->link('Edit', array('action' => 'edit_level_four', $category['Category']['id']), array('class' => 'btn btn-success fa fa-pencil color-black tooltips')); ?>
                    <?php endif; ?> 
                </p>                               
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">                    
                <table class="table table-bordered">
                    <tr>
                        <td><label class="control-label"><strong>
                                    <?php if ($category['Category']['level'] == '1'): ?>Level-1 
                                    <?php elseif ($category['Category']['level'] == '2'): ?>
                                    Level-2
                                    <?php elseif ($category['Category']['level'] == '3'): ?>
                                    Level-3
                                    <?php elseif ($category['Category']['level'] == '4'): ?>
                                    Level-4
                                    <?php endif; ?>
                                    Category Name </strong>
                            </label>
                        </td>
                        <td><?php echo (ucwords($category['Category']['category_name'])); ?></td>
                    </tr>
                    <?php if ($category['Category']['parent_category_id'] != 0): ?>
                    <tr>
                        <td><label class="control-label">
                                <?php if ($category['Category']['level'] == '2'): ?>
                                <strong>Level-1 Category Name</strong>
                                <?php elseif ($category['Category']['level'] == '3'): ?>
                                <strong>Level-2 Category Name</strong>
                                <?php else: ?>
                                <strong>Level-3 Category Name</strong>
                                <?php endif; ?>
                            </label>
                        </td>
                        <td><?php echo (ucwords($category['ParentCategory']['category_name'])); ?></td>
                    </tr>
                    <?php endif; ?>
                </table>                                        
            </div>                     
        </div> 
        <?php if (!empty($sub_cate)): ?>
        <div class="row">
            <div class="col-md-10">
                <table class="table table-bordered">
                    <thead><h4><b>
                            <?php if ($category['Category']['level'] == 1): ?>
                            Level 2
                            <?php elseif ($category['Category']['level'] == 2): ?>
                            Level 3
                            <?php elseif ($category['Category']['level'] == 3): ?>
                            Level 4
                            <?php endif; ?>
                            Categories of <?= $category['Category']['category_name']; ?>
                        </b>
                    </h4>
                    </thead>
                    <tr>
                        <th>Category Name</th>

                        <th>Actions</th>
                    </tr>
                    <?php foreach ($sub_cate as $sub): ?>                            
                    <tr>
                        <td><a href="<?= Router::url(array('controller' => 'categories', 'action' => 'view', $sub['Category']['id'])); ?>" data-toggle="" title="view"><?= ucwords($sub['Category']['category_name']); ?></a></td>
                        </td>
                        <td>
                            <?php echo $this->Html->link(__(''), array('action' => 'view', $sub['Category']['id']), array('class' => 'btn btn-sm btn-primary fa fa-eye color-black tooltips', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'View')); ?>
                            <?php if ($sub['Category']['level'] == 2): ?>
                            <?php echo $this->Html->link('', array('controller' => 'categories', 'action' => 'add_level_three'), array('class' => 'btn btn-sm btn-info fa fa-plus color-black tooltips', 'div' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Add Level-3 Category')); ?>                              

                            <?php echo $this->Html->link(__(''), array('action' => 'edit_level_two', $sub['Category']['id']), array('class' => 'btn btn-sm btn-primary fa fa-pencil color-black tooltips', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Edit')); ?>
                            <?php elseif ($sub['Category']['level'] == 3): ?>
                            <?php echo $this->Html->link('', array('controller' => 'categories', 'action' => 'add_level_four'), array('class' => 'btn btn-sm btn-success fa fa-plus color-black tooltips', 'div' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Add Level-4 Category')); ?>                              

                            <?php echo $this->Html->link(__(''), array('action' => 'edit_level_three', $sub['Category']['id']), array('class' => 'btn btn-sm btn-info fa fa-pencil color-black tooltips', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Edit')); ?>
                            <?php elseif ($sub['Category']['level'] == 4): ?>
                            <?php echo $this->Html->link(__(''), array('action' => 'edit_level_four', $sub['Category']['id']), array('class' => 'btn btn-sm btn-info fa fa-pencil color-black tooltips', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Edit')); ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>   
            </div>
        </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-7 pull-right">
                <div class="form-group">
                    <button class="btn-primary" onclick="goBack();">Back</button>      
                </div>
            </div>
        </div>
    </div>
</div> 


