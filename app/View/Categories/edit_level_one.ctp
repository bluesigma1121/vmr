<script type="text/javascript">
    $(document).ready(function() {
        $('#parentcategory_edit_frm').bValidator();
    });
</script>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Edit <?= ucwords($category_name['Category']['category_name']);?> </h5>
    </div>
    <div class="ibox-content"> 
        <?php echo $this->Form->create('Category', array('id' => 'parentcategory_edit_frm', 'class' => 'form-horizontal', 'role' => 'form')); ?>
        <div class="form-group">
            <label class="col-md-3 control-label">Level-1 Category Name:</label>
            <div class="col-md-4">
                <?php echo $this->Form->input('category_name', array('type' => 'text', 'class' => 'form-control', 'label' => false, 'data-bvalidator' => 'required')); ?>
            </div>
        </div>
        <div class="form-group"><label class="col-lg-3 control-label">Description</label>
            <div class="col-lg-4">
                <?php echo $this->Form->input('short_desc', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Enter Short Description', 'label' => false)); ?>
            </div>
        </div>
        <div class="form-group"><label class="col-lg-3 control-label">Display In </label>
            <div class="col-lg-4">
                <?php echo $this->Form->input('display_home', array('class' => 'form-control', 'options' => $display_home, 'label' => false, 'data-bvalidator' => 'required')); ?>
            </div>
        </div>
        <div class="form-group"><label class="col-lg-3 control-label">Slug</label>
            <div class="col-lg-4">
                <?php echo $this->Form->input('slug', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Slug Name', 'label' => false)); ?>
            </div>
        </div>
        <div class="form-group"><label class="col-lg-3 control-label">Sequence No</label>
            <div class="col-lg-1">
                <?php echo $this->Form->input('seq_no', array('class' => 'form-control', 'type' => 'select', 'options' => array_combine(range(1, 6), range(1, 6)), 'label' => false)); ?>
            </div>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-primary btn-sm', 'div' => false)); ?>
        </div>
        <?php echo $this->Form->end(); ?> 
    </div>
</div>




