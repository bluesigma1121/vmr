<style>
    /*.table-heading{*/
    /*    box-sizing: border-box;*/
    /*    color: rgb(104, 112, 116);*/
    /*    display: block;*/
    /*    font-family: 'Open Sans', sans-serif;*/
    /*    font-size: 14px;*/
    /*    font-weight: normal;*/
    /*    height: 20px;*/
    /*    line-height: 20px;*/
    /*    margin-bottom: 10px;*/
    /*    margin-top: 5px;*/
    /*    text-align: center;*/
    /*    text-shadow: none;*/
    /*}*/
    /*.fa-dollar{*/
    /*    color: #3498db;*/
    /*}*/
    /*.fa-inr{*/
    /*    color: #3498db;*/
    /*}*/
</style>
<?php echo $this->element('listing_search'); ?>


<section class="about-inner report-inner-bg">
    <div class="container">
    <h2>Search Results</h2>
        <div class="">
            <?php if (!empty($search_results)) { ?>
            <?php foreach ($search_results as $key => $product): ?>
            
            <div class="blog-inner report-page">
              <div class="col-md-2 report-left-img">
                <?php if (!empty($product['Product']['product_image'])) { ?>
                    <?php
                    echo $this->Html->image('product_images/' . $product['Product']['product_image'], array('class' => 'img-thumbnail image_size_home'));
                } else {
                    echo $this->Html->image('reports/report1.png', array('class' => 'img-thumbnail image_size_home'));
                }
                ?>
              </div>
              <div class="col-md-8">
                  <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'slug' => $product['Product']['slug'])); ?>">
                        <h5><?=  $product['Product']['product_name']; ?></h5>
                        <p>
                          <?php echo $this->Text->truncate(strip_tags($product['Product']['product_description']), 150, array(
                                                            'ellipsis' => '...','exact' => false)
                                                            );?>
                        </p>
                      </a>
              </div>
              <div class="col-md-2 text-center">
                <h5>$ <?=$product['Product']['price'];?></h5>

              </div>
              <div class="col-md-8 col-sm-12 col-xs-12">
                <div class="pull-left date-left">
                   <h6 class=""><i class="fa fa-calendar"></i>
                     <?php if (!empty($product['Product']['pub_date']) && $product['Product']['pub_date'] != "0000-00-00" && $product['Product']['pub_date'] != "1970-01-01"): ?>
                         <?php echo Date('F Y', strtotime($product['Product']['pub_date'])); ?>
                     <?php else: ?>
                         <?php echo Date('F Y', strtotime("+1 month")); ?>
                     <?php endif; ?>
                   </h6>
                 </div>
                 <div class="pull-right enq-btn">
                   <?php
                      echo $this->Html->link('Enquiry Before Buying', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','slug' => $product['Product']['slug'], 'ref_page' => 'enquiry-before-buying'), array("target" => "_blank"));
                   ?>
                   <!-- <a href="enquiry-before-buying.php">Enquiry Before Buying</a> -->
                 </div>

              </div>
              <div class="col-md-2 pull-left req-btn">
                <?php
                   echo $this->Html->link('Request Sample', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','slug' => $product['Product']['slug'], 'ref_page' => 'request-sample'), array("target" => "_blank"));
                ?>
                   <!-- <a href="request-customization.php">Request Sample</a> -->
             </div>
          </div>

        <?php endforeach; ?>
       </div>
        <?php }else{ ?>
            <div class="blog-inner report-page">
              <div class="col-md-12 text-center">
                    <div class="alert alert-warning text-center">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <strong>Products Not Found </strong>
                    </div>
                    <h3> Didn't find what you are looking for? </h3> 
                    <h4>Send us an email at <a href='mailto:sales@valuemarketresearch.com'><span style="color: #0074bd;font-weight: normal;font-family: 'Roboto', sans-serif;font-size:20px;line-height: 22px;text-align: justify;">sales@valuemarketresearch.com</span></a> and we will soon revert back to you.</h4>
                    
                   <div class="enq-btn">
                    <a class="btn-u" onclick="goBack()" href="#">Go Back</a>
                    </div>
              </div>
            </div>
        <?php } ?>
    </div>
</section>
