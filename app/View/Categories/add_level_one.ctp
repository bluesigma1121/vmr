<script type="text/javascript">
    $(document).ready(function() {
        $('#add_level_one').bValidator();
    });
</script>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Add Level-1 Category </h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('Category', array('id' => 'add_user', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-2">
            <div class="form-group"><label class="col-lg-3 control-label">Category Name</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('category_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Parent Category Name', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-3 control-label">Description</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('short_desc', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Enter Short Description', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>

            <div class="form-group"><label class="col-lg-3 control-label">Display In </label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('display_home', array('class' => 'form-control', 'options' => $display_home, 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div> 
            <div class="form-group"><label class="col-lg-3 control-label">Sequence No</label>
                <div class="col-lg-1">
                    <?php echo $this->Form->input('seq_no', array('class' => 'form-control', 'type' => 'select', 'options' => array_combine(range(1, 6), range(1, 6)), 'label' => false)); ?>
                </div>
            </div>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
            <button onclick="goBack();" class="btn btn-sm btn-info">Back</button>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>

