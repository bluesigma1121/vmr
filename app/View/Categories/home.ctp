<div class="slider-banner home_padding" id="main-slider-banner" >
  <div class="slider-wrapper">
      <div class="slide-banner" data-image="<?= $this->webroot.'img/slideshow/slider.webp'; ?>"></div>
  
      <div class="container">
  <div class="row search">
    <div class="search-bar">    
      <div class="col-md-10 col-md-push-1">
        <div class="custom-search-form">
            <?php echo $this->Form->create('Category', array('type' => 'get','action'=>'home_search_form')); ?>
                <?php echo $this->Form->input('q', array('class' => 'form-control', 'label' =>false,'required','autocomplete'=>'off','id'=>'search_text')); ?>
                <div class="col-md-11 pull-left suggest-div" id="suggest"></div>
                <div class="col-md-1 col-sm-1 input-group-btn pull-right search-btn">
                  <button class="btn btn-primary" type="submit" style="height: 34px;">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>
                </div>
            <?php  echo $this->Form->end(); ?>
        </div>
      </div>
      <div class="col-lg-12 text-center">
        <ul style="margin-top:-12px;margin-bottom: -6px;">
          <?php foreach ($research_cat as $rckey => $res_cat): ?>
            <li class="search-menu">
              <a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($res_cat['Category']['category_name']))); ?>"><?php echo $res_cat['Category']['category_name']; ?></a>
            </li>
          <?php endforeach; ?>
          <li class="search-menu search-menu-all"><a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'category_list')); ?>">View all</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
  </div>
  
</div>


<!-- <style>
.dd_covid{background:#0074bd;box-shadow:0 14px 28px rgba(0,0,0,.25),0 10px 10px rgba(0,0,0,.22);overflow:hidden;padding:30px;margin:auto auto 60px auto;position:relative;width:100%;background-position:bottom center;background-repeat:no-repeat;background-size:cover}.dd_covid .card .title{font-size:40px;font-weight:900;color:#fff}.dd_covid .card .title small{color:#fff;font-size:32px;font-weight:900}.dd_covid .card p{font-size:20px;color:#fff!important;letter-spacing:1px}.btn-covid{display:inline-block;padding:5px 24px;color:#fff;transition:all .33s ease;text-decoration:none;font-size:16px;border:2px solid #fff;margin-top:14px;letter-spacing:2px}.btn-covid:focus,.btn-covid:hover{background:#fff;color:#0074bd}
</style>
<section class="dd_covid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h2 class="title">COVID -19 : <small>Impact Analysis</small></h2>
                    <p>Access reports understanding the COVID 19 impact on major industries and markets</p>
                    <?php echo $this->Html->link('Access Reports', array('controller' => 'products', 'action' => 'reportlist'), array("class" => "btn-covid", "target" => "_blank",'escape' => false));?>
                </div>
            </div>
        </div>
    </div>
</section> -->

<!-- <section id="services" class="services-section">
    <div class="container bgwhite">
        <div class="row">
            <div class="col-lg-12">
                <div class="">
                  <div class="row" id="slider-text">
                    <div class="header-section text-center">
                      <h2 class="">Our Clients</h2>
                      <hr class="bottom-line">
                    </div>
                  </div>
                </div>

                <div class="container-fluid row">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="carousel carousel-showmanymoveone slide" id="itemslider">
                        <div class="carousel-inner carousel-inner-pay">
                          <?php foreach ($ourclients as $key => $clients) { ?>

                            <div class="item <?= $key == 0?'active':''?> payotn">
                              <div class="col-sm-2 col-md-2">
                                <?php if (!empty($clients['OurClient']['logo'])) { ?>
                                      <?= $this->Html->image('client_images/' . $clients['OurClient']['logo'], array('class' => '','alt' => 'Client Logos')); ?>
                                <?php } else {
                                        echo $this->Html->image('noimage.png', array('class' => '','alt' => 'Client Logos'));
                                      } ?>
                              </div>
                            </div>
                          <?php } ?>
                        </div>

                        <div id="slider-control">
                        <a class="left carousel-control" href="#itemslider" data-slide="prev">
                          <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                        </a>
                        <a class="right carousel-control" href="#itemslider" data-slide="next">
                          <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
                        </a>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
<br><br>
<style>#testimonial {background: url(<?= $this->webroot.'img/banner/banner1.webp'; ?>) no-repeat center top / cover !important;margin-top: -48px;}</style>
<section id="testimonial" class="section-padding">
  <div class="bg-color">
  <div class="container">
    <div class="row">
      <div class="header-section text-center padd-botm">
        <h2 class="white">Why Choose Us</h2>
        <hr class="bottom-line">
      </div>
      <?php foreach ($whyus as $key => $why) { ?>
        <div class="col-md-3 col-sm-6">
          <div class="bg-box">
            <?php switch ($key) {
              case 0:
                echo '<i class="fa fa-user" aria-hidden="true"></i>';
                break;
              case 1:
                echo '<i class="fa fa-certificate" aria-hidden="true"></i>';
                break;
              case 2:
                echo '<i class="fa fa-handshake-o" aria-hidden="true"></i>';
                break;
              case 3:
                echo '<i class="fa fa-envelope" aria-hidden="true"></i>';
                break;
            }?>

            <h3><?=$why['Whyus']['title']?></h3>
          </div>
        </div>
      <?php } ?>

    </div>
  </div>
</div>
</section>

<br><br>

<section id="services" class="services-section">
    <div class="container bgwhite">
        <div class="row">
            <div class="col-lg-12">
                <div class="">
                  <div class="row" id="slider-text">
                    <div class="header-section text-center">
                      <h2 class="">Our Clients</h2>
                      <hr class="bottom-line">
                    </div>
                  </div>
                </div>

                <div class="container-fluid row">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="carousel carousel-showmanymoveone slide" id="itemslider">
                        <div class="carousel-inner carousel-inner-pay">
                        <?php foreach ($ourclients as $key => $clients) { ?>

<div class="item <?= $key == 0?'active':''?> payotn">
  <div class="col-sm-2 col-md-2">
    <?php if (!empty($clients['OurClient']['logo'])) { ?>
          <?= $this->Html->image('client_images/' . $clients['OurClient']['logo'], array('class' => '','alt' => 'Client Logos')); ?>
    <?php } else {
            echo $this->Html->image('noimage.png', array('class' => '','alt' => 'Client Logos'));
          } ?>
  </div>
</div>
<?php } ?>
</div>

<div id="slider-control">
<a class="left carousel-control" href="#itemslider" data-slide="prev">
<i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
</a>
<a class="right carousel-control" href="#itemslider" data-slide="next">
<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<!-- <style>
.dd_covid{background:#0074bd;box-shadow:0 14px 28px rgba(0,0,0,.25),0 10px 10px rgba(0,0,0,.22);overflow:hidden;padding:30px;margin:auto auto 60px auto;position:relative;width:100%;background-position:bottom center;background-repeat:no-repeat;background-size:cover}.dd_covid .card .title{font-size:40px;font-weight:900;color:#fff}.dd_covid .card .title small{color:#fff;font-size:32px;font-weight:900}.dd_covid .card p{font-size:20px;color:#fff!important;letter-spacing:1px}.btn-covid{display:inline-block;padding:5px 24px;color:#fff;transition:all .33s ease;text-decoration:none;font-size:16px;border:2px solid #fff;margin-top:14px;letter-spacing:2px}.btn-covid:focus,.btn-covid:hover{background:#fff;color:#0074bd}
</style> -->
<style>
    @media only screen and (max-width: 767px) {
      .dd_covid .card .title {
                        font-size: 22px !important;
                        font-weight: 900;
                        color: #fff;
                        margin-left: 0px !important;
                        margin-right: 0px !important;
                        margin-top: 10px;
                        margin-bottom: 0px;
                        line-height: 24px;
                        padding: 19px !important;
                        text-align: center !important;;
                    }
                    h2 {
                        font-size: 24px;
                        padding-bottom: 15px;
                    }
                    .dd_covid {
                        background: #0074bd;
                        box-shadow: 0 14px 28px rgba(0, 0, 0, .25), 0 10px 10px rgba(0, 0, 0, .22);
                        overflow: hidden;
                        padding: -76px;
                        margin: auto auto 60px auto;
                        position: relative;
                        width: 100%;
                        background-position: bottom center;
                        background-repeat: no-repeat;
                        background-size: cover;
                    }
                    .dd_covid .card .title small {
                        color: #fff;
                        font-size: 22px !important;
                        font-weight: 900;
                    }
                    /* .btn-covid {
                    display: inline-block;
                    padding: 5px 24px;
                    color: #fff;
                    transition: all .33s ease;
                    text-decoration: none;
                    font-size: 12px !important;
                    border: 2px solid #fff;
                    margin-top: 20px;
                    letter-spacing: 2px;
                    margin-left: 0px;
                } */
                .btn-covid{   margin-left: 0 !important;margin-top: 10px !important;margin-bottom: 0px !important;}
                
    }
.dd_covid{background:#0074bd;box-shadow:0 14px 28px rgba(0,0,0,.25),0 10px 10px rgba(0,0,0,.22);overflow:hidden;padding:-76px;margin:auto auto 60px auto;position:relative;width:100%;background-position:bottom center;background-repeat:no-repeat;background-size:cover}.dd_covid .card .title{font-size:40px;font-weight:900;color:#fff;margin-left: 77px;margin-right: 77px;margin-top: 10px;margin-bottom: 0px;line-height: 10px;}.dd_covid .card .title small{color:#fff;font-size:32px;font-weight:900}.dd_covid .card p{font-size:20px;color:#fff!important;letter-spacing:1px;margin-left: 77px;}.btn-covid{display:inline-block;padding:5px 24px;color:#fff;transition:all .33s ease;text-decoration:none;font-size:16px;border:2px solid #fff;margin-top:14px;letter-spacing:2px;margin-left: 77px;}.btn-covid:focus,.btn-covid:hover{background:#fff;color:#0074bd}
</style>
<section class="dd_covid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h2 class="title">COVID -19 : <small>Impact Analysis</small> <?php echo $this->Html->link('Access Reports', array('controller' => 'products', 'action' => 'reportlist'), array("class" => "btn-covid", "target" => "_blank",'escape' => false));?></h2>
                    <!-- <p>Access reports understanding the COVID 19 impact on major industries and markets
                   
                    </p> -->
                  </div>
            </div>
        </div>
    </div>
</section>

<section class="testibg">
<div class="container">
  <div class="row">
    <div class="col-md-12 col-sm-12">
      <div class="price-table testimonial-marg-botm">
        <div class="header-section text-center">
          <h2 class="">Testimonials</h2>
          <hr class="bottom-line">
        </div>
        <div class="pricing-head">
          <div id="myCarousel" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel">
            <div class="carousel-inner">
              <?php foreach ($testimonial as $key => $testimonials) { ?>
                <div class="item <?= $key == 0?'active':''?>">
                    <div class="row">
                         <div class="col-xs-12 col-sm-12 col-md-12 testimonial-text">
                            <p> <i class="fa fa-quote-left fa-lg" aria-hidden="true"></i><?php echo " \" "; echo  $testimonials['Testimonial']['testimonial_description'] ;echo" \" "; ?></p>
                            <h5><i class="fa fa-user-circle fa-lg" aria-hidden="true"></i> <?php echo $testimonials['Testimonial']['testimonial_title'];?></h5>
                        </div>
                    </div>
                  </div>
                <?php } ?>
            </div>
            
          </div>
          <div class="view-more-btn">
            <a href="<?php echo Router::url(array('controller' => 'testimonials', 'action' => 'listing' )); ?>">View More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>


<section id ="pricing" class="section-padding">
  <div class="container">
    <div class="row">

      <div class="col-md-8 col-sm-8">
        <div class="price-table">
          <div class="price-in mart-15">
            <h1 style="margin-top:0px; margin-bottom:0px;"><span class="btn-bg green btn-block"><i class="fa fa-address-card-o fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;LATEST MARKET RESEARCH REPORTS</span></h1>
          </div>

          <?php foreach ($home_products as $home_product): ?>
            <div class="pricing-head">
                <a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details','slug' => $home_product['Product']['slug'] ));?>">
                    <h5 class="recent-rpt">
                      <i class="fa fa-line-chart" aria-hidden="true"></i>                
                      <p>
                        <?= $this->Text->truncate($home_product['Product']['product_name'],180,array(
                                                                                'ellipsis' => '...','exact' => false)) ?>
                      </p>
                    </h5>
                </a>
              <p>
                <?php echo $this->Text->truncate(strip_tags($home_product['Product']['product_description']), 150, array(
                                                                            'ellipsis' => '...','exact' => false)
                                                                            );?>
              </p>
            </div>
          <?php endforeach; ?>
          <div class="text-right pricing-head"><a href="<?php echo Router::url(array('controller'=>'products','action'=>'reportlist'));?>">Read More</a></div>
        </div>
      </div>
      <div class="col-md-4 col-sm-4">
        <div class="price-table">
          <div class="price-in mart-15">
            <span class="btn-bg green btn-block" style="padding:10px 0px;"><i class="fa fa-newspaper-o fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp; PRESS RELEASES</span>
          </div>
          <?php foreach ($press_releases as $key => $press_relese): ?>
            <div class="pricing-head">
              <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'article_details',  'type' => 'pressreleases', 'url_slug' => $this->Link->cleanString($press_relese['Article']['slug']))); ?>">
                <p><?php echo $press_relese['Article']['headline'] ?></p>
              </a>
            </div>
          <?php endforeach; ?>
          <div class="text-right pricing-head"><a href="<?php echo Router::url(array('controller'=>'articles','action'=>'article_listing'));?>">Read More</a></div>
        </div>
        <style>.pt-10{padding-top:10px} </style>
        <div class="price-table pt-10">
          <div class="price-in mart-15">
            <span class="btn-bg green btn-block" style="padding:10px 0px;"><i class="fa fa-newspaper-o fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp; ANALYSIS</span>
          </div>
          <?php foreach ($analysis as $key => $analysis): ?>
            <div class="pricing-head">
              <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'article_details', 'type' => 'analysis',  'url_slug' => $this->Link->cleanString($analysis['Article']['slug']))); ?>">
                <p><?php echo $analysis['Article']['headline'] ?></p>
              </a>
            </div>
          <?php endforeach; ?>
          <div class="text-right pricing-head"><a href="<?php echo Router::url(array('controller'=>'articles','action'=>'analysis_listing'));?>">Read More</a></div>
        </div>
      </div>      
      
    </div>
  </div>
</section>