<?php echo $this->Html->script('back/ckeditor/ckeditor.js'); ?>

<script type="text/javascript">
    var sel_node = '<?php echo $sel_id ?>';
    var main_id = '<?php echo $id ?>';
    $(document).ready(function () {
        $('#edit_category').bValidator();
        $('#add_category').bValidator();

        $('#tt').tree({
            onClick: function (node) {
                var t = $('#tt');
                var node = t.tree('getSelected');
                var id = node.id;
                var target_id = node.target.id;
                if (id != null) {
                    var base_path = "<?= Router::url('/', true) ?>";
                    var urls = base_path + "categories/find_edit_tree_category";
                    $.ajax({
                        url: urls,
                        type: "POST",
                        data: {parent_id: id},
                        dataType: 'html'
                    }).done(function (data) {
                        if (data.length != 0) {
                            var obj = jQuery.parseJSON(data);
                            $('#add_form').hide();
                            $('#edit_form').show();
                            $('#cat_id').val(obj.id);

                            $('#category_name').val(obj.category_name);
                            //  $('#short_desc').html(obj.short_desc);

                            // $('#long_desc').val(obj.long_desc);
                            $('#cat_url').html(obj.cat_url);
                            $('#cat_slug').val(obj.cat_slug);
                            $('#page_title').val(obj.page_title);
                            $('#page_desc').val(obj.page_desc);
                            $('#cat_breadcrub').html(obj.cat_breadcrub);
                            $('#target_id').val(target_id);
                            var active_flg = obj.is_active;
                            var base_path = "<?= Router::url('/', true) ?>";
                            if (active_flg == 0) {
                                $('.unpub').hide();
                                $('.pub').show();
                                var url_publish = base_path + "categories/category_publish_sub/" + main_id + "/" + obj.id + "/" + target_id + "/sub_tree";

                                // var url_publish = base_path + "categories/category_publish/" + obj.id;
                                $('.pub').attr('href', url_publish);
                            } else {
                                $('.pub').hide();
                                $('.unpub').show();
                                var url_unpublish = base_path + "categories/category_unpublish_sub/" + main_id + "/" + obj.id + "/" + target_id + "/" + "sub_tree";
                                $('.unpub').attr('href', url_unpublish);
                            }
                            var url_preview = base_path + "products/category_listing_preview/" + obj.id;
                            $('.preview').attr('href', url_preview);
                            CKEDITOR.instances['short_desc'].setData(obj.short_desc);
                        }
                    });
                } else {
                    alert('Error.. Category Could Not Be Updated');
                }
            }
        });

        if (sel_node != '') {
            var node_sel = $('#tt').tree('find', sel_node);
            $('#tt').tree('select', node_sel.target);
        }

        /*   $('.tree-node').click(function() {
         var t = $('#tt');
         var node = t.tree('getSelected');
         var id = node.id;
         if (id != null) {
         var base_path = "<?= Router::url('/', true) ?>";
         var urls = base_path + "categories/find_edit_tree_category";
         $.ajax({
         url: urls,
         type: "POST",
         data: {parent_id: id},
         dataType: 'html'
         }).done(function(data) {
         if (data.length != 0) {
         var obj = jQuery.parseJSON(data);
         $('#add_form').hide();
         $('#edit_form').show();
         $('#cat_id').val(obj.id);
         $('#category_name').val(obj.category_name);
         $('#short_desc').val(obj.short_desc);
         $('#long_desc').val(obj.long_desc);
         $('#cat_url').val(obj.cat_url);
         $('#page_title').val(obj.page_title);
         $('#page_desc').val(obj.page_desc);
         $('#cat_breadcrub').val(obj.cat_breadcrub);
         
         }
         });
         } else {
         alert('Error.. Category Could Not Be Updated');
         }
         }); */


    });
    function append() {
        var t = $('#tt');
        //   var category = prompt("Please enter Category name");
        var node = t.tree('getSelected');
        var parent_id = node.id;
        var target_id = node.target.id;
        if (parent_id != null) {
            $('#edit_form').hide();
            $('#add_form').show();
            $('#parent_category_id').val(parent_id);
            $('#add_target_id').val(target_id);
        } else {
            alert('error :Category Could not updated');
        }

    }

    function updateit() {
        var t = $('#tt');
        var node = t.tree('getSelected');
        //  var pre_cat = node.text;
        //  var cat_tex = pre_cat.replace(/\(.*?\)/g, '');
        // var category = prompt("Please enter Category name", cat_tex);

        //category = $.trim(category);
//          var node1 = $('#tt').tree('find', 2);
//        $('#tt').tree('select', node.target);
        var id = node.id;
        var target_id = node.target.id;
        if (id != null) {
            var base_path = "<?= Router::url('/', true) ?>";
            var urls = base_path + "categories/find_edit_tree_category";
            $.ajax({
                url: urls,
                type: "POST",
                data: {parent_id: id},
                dataType: 'html'
            }).done(function (data) {
                if (data.length != 0) {
                    var obj = jQuery.parseJSON(data);
                    $('#add_form').hide();
                    $('#edit_form').show();
                    $('#cat_id').val(obj.id);
                    $('#category_name').val(obj.category_name);
                    //  $('#short_desc').val(obj.short_desc);
                    CKEDITOR.instances['short_desc'].setData(obj.short_desc);
                    $('#long_desc').val(obj.long_desc);
                    $('#cat_slug').val(obj.cat_slug);
                    $('#cat_url').html(obj.cat_url);
                    $('#page_title').val(obj.page_title);
                    $('#page_desc').val(obj.page_desc);
                    $('#cat_breadcrub').html(obj.cat_breadcrub);
                    $('#target_id').val(target_id);
                    var active_flg = obj.is_active;
                    var base_path = "<?= Router::url('/', true) ?>";
                    if (active_flg == 0) {
                        $('.unpub').hide();
                        $('.pub').show();
                        var url_publish = base_path + "categories/category_publish_sub/" + main_id + "/" + obj.id + "/" + target_id + "/sub_tree";
                        //    alert(url_publish);
                        $('.pub').attr('href', url_publish);
                    } else {
                        $('.pub').hide();
                        $('.unpub').show();
                        var url_unpublish = base_path + "categories/category_unpublish_sub/" + main_id + "/" + obj.id + "/" + target_id + "/sub_tree";
                        // alert(url_unpublish);
                        $('.unpub').attr('href', url_unpublish);
                    }
                    var url_preview = base_path + "products/category_listing_preview/" + obj.id;
                    $('.preview').attr('href', url_preview);

                }
            });
        } else {
            alert('Error.. Category Could Not Be Updated');
        }
    }

    function filter() {
        var node_filter = $('#tt').tree('getSelected');
        var element = $(node_filter.text); //convert string to JQuery element
        element.find("span").remove(); //remove span elements
        // var newString = element.html(); //get back new string
        //var removes_flg = confirm("Are you really want to delete category : " + newString + "??");
        var id_filter = node_filter.id;
        if (id_filter != null) {
            var base_path = "<?= Router::url('/', true) ?>";
            var redirect_url = base_path + "categories/tree_view_sub_categories/" + id_filter;
            window.location.assign(redirect_url);
        }
        // $('#tt').tree('remove', node.target);
    }

    function removeit() {
        var node_remove = $('#tt').tree('getSelected');

        var element = $(node_remove.text); //convert string to JQuery element
        element.find("span").remove(); //remove span elements
        var newString = element.html(); //get back new string
        var removes_flg = confirm("Are you really want to delete category : " + newString + "??");

        if (removes_flg == true) {
            var id_remove = node_remove.id;
            if (id_remove != null) {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls_remove = base_path + "categories/remove_tree_category";
                $.ajax({
                    url: urls_remove,
                    type: "POST",
                    data: {cat_id: id_remove},
                    dataType: 'html'
                }).done(function (res_data) {
                    if (res_data != 0) {

                        var red_urls_remove = base_path + "categories/tree_view_sub_categories/" + main_id;
                        window.location.assign(red_urls_remove);
                    } else {
                        alert('Error.. Category Used In Product Please Delete Product First');
                    }
                });
            }
        }
        // $('#tt').tree('remove', node.target);
    }

    function activeit() {
        var node_active = $('#tt').tree('getSelected');
        var element = $(node_active.text); //convert string to JQuery element
        element.find("span").remove(); //remove span elements
        var newString = element.html(); //get back new string
        var r = confirm("Are you really want to Active category : " + newString + "??");
        if (r == true) {
            var active_id = node_active.id;
            var active_target_id = node_active.target.id;
            var base_path = "<?= Router::url('/', true) ?>";
            var urls = base_path + "categories/active_tree_category";
            $.ajax({
                url: urls,
                type: "POST",
                data: {cat_id: active_id},
                dataType: 'html'
            }).done(function (res_data) {
                if (res_data == 1) {
                    //  alert(res_data);
                    var red_urls_active = base_path + "categories/tree_view_sub_categories/" + main_id + "/" + active_id + "#" + active_target_id;

                    //window.location = red_urls_active;
                    if (sel_node == active_id) {
                        location.reload();
                    } else {
                        window.location.assign(red_urls_active);
                    }

                    // 
                } else {
                    alert('Error.. Category Could Not be Activated');
                }
            });
        }
        // $('#tt').tree('remove', node.target);
    }

    function deactiveit() {
        var node_de = $('#tt').tree('getSelected');
        var element = $(node_de.text); //convert string to JQuery element
        element.find("span").remove(); //remove span elements
        var newString = element.html(); //get back new string
        var r = confirm("Are you really want to Deactive category : " + newString + "??");
        if (r == true) {
            var id_de = node_de.id;
            var target_id_de = node_de.target.id;
            var base_path = "<?= Router::url('/', true) ?>";
            var urls = base_path + "categories/deactive_tree_category";
            $.ajax({
                url: urls,
                type: "POST",
                data: {cat_id: id_de},
                dataType: 'html'
            }).done(function (res_data) {
                if (res_data == 1) {
                    var red_urls = base_path + "categories/tree_view_sub_categories/" + main_id + "/" + id_de + "#" + target_id_de;
                    if (sel_node == id_de) {
                        location.reload();
                    } else {
                        window.location.assign(red_urls);
                    }
                } else {
                    alert('Error.. Category Could Not Deactivated');
                }
            });
        }
        // $('#tt').tree('remove', node.target);
    }

    function collapse() {
        var node = $('#tt').tree('getSelected');
        $('#tt').tree('collapse', node.target);
    }
    function expand() {
        var node = $('#tt').tree('getSelected');
        $('#tt').tree('expand', node.target);
    }

</script>

<style>
    .tree-title {
        font-size: 16px !important;
        display: inline-block;
        text-decoration: none;
        vertical-align: top;
        white-space: nowrap;
        padding: 0 2px;
        height: 18px;
        line-height: 18px;
    }
    .bg_red{
        color:red !important;
    } 
    .bg_green{
        color:green !important;
    } 
    .form_position{

        right: 15px;
    } 
    .margin_left_14{
        margin-left: 14px;
    }
    .scroll-list_filter {
        float: left;
        width: 225px;
        overflow-y: auto;
        max-height: 1000px;
    }
</style>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Category TreeView </h5>
        <div class="pull-right">
            <?php if (SessionComponent::check('show_active')) : ?>
                <?php echo $this->Html->link(__('Display All Categories'), array('controller' => 'categories', 'action' => 'display_active'), array('class' => 'btn   btn-outline btn-sm btn-info dim', 'title' => 'Display all categories')); ?>
            <?php else: ?>
                <?php echo $this->Html->link(__('Display Active Categories Only'), array('controller' => 'categories', 'action' => 'display_active', 1), array('class' => 'btn   btn-outline btn-sm btn-success dim', 'title' => 'Display Active categories')); ?>
            <?php endif; ?>
            <?php if (AuthComponent::user('role') == 1): ?>                   
                <?php echo $this->Html->link(__('Upload Excel'), array('controller' => 'categories', 'action' => 'upload_category_excel'), array('class' => 'btn   btn-outline btn-sm btn-warning dim', 'title' => 'Category Excel Upload')); ?>
            <?php endif; ?>
            <?php if (AuthComponent::user('role') == 1): ?>                   
                <?php echo $this->Html->link(__('Refresh Count'), array('controller' => 'product_categories', 'action' => 'update_prod_count_admin'), array('class' => 'btn   btn-outline btn-sm btn-default dim', 'title' => 'Product Count Refresh')); ?>
            <?php endif; ?>
            <?php if (AuthComponent::user('role') == 1): ?>                   
                <?php echo $this->Html->link(__('Count'), array('controller' => 'categories', 'action' => 'count_child'), array('class' => 'btn   btn-outline btn-sm btn-default dim', 'title' => 'Product Count Refresh')); ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox-content"> 
        <?php if (!empty($cat_data)): ?>
            <div class="row">

                <div class="col-lg-6">
                    <div class="easyui-panel scroll-list_filter" style="padding:5px">
                        <ul id="tt" class="easyui-tree " data-options="
                            animate: true,
                            lines:true,
                            onContextMenu: function(e,node){
                            e.preventDefault();
                            $(this).tree('select',node.target);
                            $('#mm').menu('show',{
                            left: e.pageX,
                            top: e.pageY
                            });
                            }
                            ">
                                <?php
                                foreach ($cat_data['Level1'] as $key1 => $cat):
                                    ?> 
                                <li data-options="id:<?php echo $cat['id'] ?> ,state:'open'">
                                    <span> <?php if ($cat['is_active'] == 0) { ?>

                                            <span class="bg_red"> <?php echo $cat['category_name'] ?>  (<?php echo $cat['no_of_product'] ?>)(<?php echo $cat['no_of_child'] ?>)</span>
                                        <?php } else { ?>
                                            <span class="bg_green"> <?php echo $cat['category_name'] ?>  (<?php echo $cat['no_of_product'] ?>)(<?php echo $cat['no_of_child'] ?>)</span>
                                        <?php } ?>
                                    </span>
                                    <?php if (!empty($cat['Level2'])) { ?>
                                        <ul data-options="state:'open'"> 
                                            <?php foreach ($cat['Level2'] as $key2 => $cat2): ?>
                                                <li data-options="id:<?php echo $cat2['id'] ?>" attributes ="active" data-state="active">
                                                    <span> <?php if ($cat2['is_active'] == 0) { ?>

                                                            <span class="bg_red"> <?php echo $cat2['category_name'] ?>  (<?php echo $cat2['no_of_product'] ?>)(<?php echo $cat2['no_of_child'] ?>)</span>
                                                        <?php } else { ?>

                                                            <span class="bg_green"> <?php echo $cat2['category_name'] ?>  (<?php echo $cat2['no_of_product'] ?>)  (<?php echo $cat2['no_of_child'] ?>)</span>                                                   
                                                        <?php } ?>
                                                    </span> 

                                                    <?php if (!empty($cat2['Level3'])) { ?>
                                                        <ul> 
                                                            <?php foreach ($cat2['Level3'] as $key3 => $cat3): ?>
                                                                <li data-options="id:<?php echo $cat3['id'] ?>">
                                                                    <span> <?php if ($cat3['is_active'] == 0) { ?>
                                                                            <span class="bg_red"> <?php echo $cat3['category_name'] ?>  (<?php echo $cat3['no_of_product'] ?>)(<?php echo $cat3['no_of_child'] ?>)</span>
                                                                        <?php } else { ?>
                                                                            <span class="bg_green"> <?php echo $cat3['category_name'] ?>  (<?php echo $cat3['no_of_product'] ?>)(<?php echo $cat3['no_of_child'] ?>)</span>
                                                                        <?php } ?>
                                                                    </span>
                                                                    <?php if (!empty($cat3['Level4'])) { ?>
                                                                        <ul> 
                                                                            <?php foreach ($cat3['Level4'] as $key4 => $cat4): ?>
                                                                                <li data-options="id:<?php echo $cat4['id'] ?>">
                                                                                    <span> <?php if ($cat4['is_active'] == 0) { ?>
                                                                                            <span class="bg_red"> <?php echo $cat4['category_name'] ?>  (<?php echo $cat4['no_of_product'] ?>)(<?php echo $cat4['no_of_child'] ?>)</span>
                                                                                        <?php } else { ?>
                                                                                            <span class="bg_green"> <?php echo $cat4['category_name'] ?>  (<?php echo $cat4['no_of_product'] ?>)(<?php echo $cat4['no_of_child'] ?>)</span>
                                                                                        <?php } ?>
                                                                                    </span> 
                                                                                    <?php if (!empty($cat4['Level5'])) { ?>
                                                                                        <ul> 
                                                                                            <?php foreach ($cat4['Level5'] as $key5 => $cat5): ?>
                                                                                                <li data-options="id:<?php echo $cat5['id'] ?>">
                                                                                                    <span> <?php if ($cat5['is_active'] == 0) { ?>
                                                                                                            <span class="bg_red"> <?php echo $cat5['category_name'] ?>  (<?php echo $cat5['no_of_product'] ?>)(<?php echo $cat5['no_of_child'] ?>)</span>
                                                                                                        <?php } else { ?>
                                                                                                            <span class="bg_green"> <?php echo $cat5['category_name'] ?>  (<?php echo $cat5['no_of_product'] ?>)(<?php echo $cat5['no_of_child'] ?>)</span>
                                                                                                        <?php } ?>
                                                                                                    </span>

                                                                                                    <?php if (!empty($cat5['Level6'])) { ?>
                                                                                                        <ul> 
                                                                                                            <?php foreach ($cat5['Level6'] as $key6 => $cat6): ?>
                                                                                                                <li data-options="id:<?php echo $cat6['id'] ?>">
                                                                                                                    <span> <?php if ($cat6['is_active'] == 0) { ?>
                                                                                                                            <span class="bg_red"> <?php echo $cat6['category_name'] ?>  (<?php echo $cat6['no_of_product'] ?>)(<?php echo $cat6['no_of_child'] ?>)</span>
                                                                                                                        <?php } else { ?>
                                                                                                                            <span class="bg_green"> <?php echo $cat6['category_name'] ?>  (<?php echo $cat6['no_of_product'] ?>)(<?php echo $cat6['no_of_child'] ?>)</span>
                                                                                                                        <?php } ?>
                                                                                                                    </span>
                                                                                                                </li>
                                                                                                            <?php endforeach; ?>
                                                                                                        </ul>
                                                                                                    <?php } ?>

                                                                                                </li>
                                                                                            <?php endforeach; ?>
                                                                                        </ul>
                                                                                    <?php } ?>
                                                                                </li>
                                                                            <?php endforeach; ?>
                                                                        </ul>
                                                                    <?php } ?>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    <?php } ?>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php } ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div id="mm" class="easyui-menu" style="width:120px;">

                        <?php if (in_array(AuthComponent::user('role'), array(1, 2))) { ?>
                            <div onclick="append()" data-options="iconCls:'icon-add'">Append</div>
                            <div onclick="removeit()" data-options="iconCls:'icon-remove'">Remove</div>
                            <div onclick="updateit()" data-options="iconCls:'icon-edit'">Edit</div>
                        <?php } ?>
                        <?php if (AuthComponent::user('role') == 1): ?>
                            <div class="menu-sep"></div>
                            <div onclick="activeit()" data-options="iconCls:'icon-ok'">Active</div>
                            <div onclick="deactiveit()" data-options="iconCls:'icon-cancel'">Deactive</div>
                            <div class="menu-sep"></div>
                        <?php endif; ?>
                        <div onclick="filter()">Filter</div>
                        <div onclick="expand()">Expand</div>
                        <div onclick="collapse()">Collapse</div>
                    </div> 
                </div>
                <div class="col-lg-6 form_position" id="edit_form" hidden> 
                    <div class="text-center">
                        <h3> Edit Category </h3>
                    </div>
                    <div class=""> 
                        <?php echo $this->Form->create('Category', array('action' => 'edit_tree_category', 'id' => 'edit_category', 'class' => 'form-horizontal', 'role' => 'form')); ?>
                        <div class="text-center">
                            <?php echo $this->Form->button('Submit', array('type' => 'submit', 'class' => 'btn btn-primary', 'title' => 'Save')); ?>
                            <a id="preview" class="btn btn-success preview" target="_blank" title="Preview">Preview</a>
                            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>

                                <a id="publish" class="btn btn-warning pub" title="Publish">Publish</a>
                                <a id="unpublish" class="btn btn-danger unpub" title="UnPublish">UnPublish</a> 
                            <?php } ?>
                        </div>
                        <?php echo $this->Form->input('id', array('type' => 'hidden', 'id' => 'cat_id')); ?>
                        <?php echo $this->Form->input('main_cat', array('type' => 'hidden', 'id' => 'mat_id', 'value' => $id)); ?>
                        <?php echo $this->Form->input('refpage', array('type' => 'hidden', 'id' => 'refpage', 'value' => 'sub_tree')); ?>
                        <?php echo $this->Form->input('target_id', array('type' => 'hidden', 'id' => 'target_id')); ?>  
                        <div class="form-group">
                            <label class="margin_left_14">Page url</label>
                            <div class="col-lg-12">
                                <label id="cat_url">
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="margin_left_14">Slug</label>
                            <div class="col-lg-12">
                                <?php echo $this->Form->input('cat_slug', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'slug', 'label' => false, 'id' => 'cat_slug')); ?>
                            </div>
                        </div>
                        <?php if (in_array(AuthComponent::user('role'), array(1, 2))) { ?>
                            <div class="form-group">
                                <label class="margin_left_14">Category Name</label>
                                <div class="col-md-12">
                                    <?php echo $this->Form->input('category_name', array('type' => 'text', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Enter Category Name', 'data-bvalidator' => 'required', 'id' => 'category_name')); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="margin_left_14">Description</label>
                                <div class="col-lg-12">
                                    <?php echo $this->Form->input('short_desc', array('class' => 'form-control ckeditor', 'type' => 'textarea', 'placeholder' => 'Enter Description', 'label' => false, 'id' => 'short_desc')); ?>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="form-group">
                            <label class="margin_left_14">Page Title</label>
                            <div class="col-lg-12">
                                <?php echo $this->Form->input('page_title', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Page Title', 'label' => false, 'id' => 'page_title')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="margin_left_14">Page Description</label>
                            <div class="col-lg-12">
                                <?php echo $this->Form->input('page_desc', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Enter Page Description', 'label' => false, 'id' => 'page_desc')); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="margin_left_14">Breadcrumb</label>
                            <div class="col-lg-12">
                                <span id="cat_breadcrub">
                                </span>
                            </div>
                        </div>

                        <div class="text-center">
                            <?php echo $this->Form->button('Submit', array('type' => 'submit', 'class' => 'btn btn-primary', 'title' => 'Save')); ?>
                            <a id="preview" class="btn btn-success preview" target="_blank" title="Preview">Preview</a>
                            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>

                                <a id="publish" class="btn btn-warning pub" title="Publish">Publish</a>
                                <a id="unpublish" class="btn btn-danger unpub" title="UnPublish">UnPublish</a> 

                            <?php } ?>
                        </div>
                        <?php echo $this->Form->end(); ?> 
                    </div>
                </div>
                <div class="col-lg-5 form_position" id="add_form" hidden> 
                    <div class="text-center">
                        <h3> Add Category </h3>
                    </div>
                    <div class=""> 
                        <?php echo $this->Form->create('Category', array('action' => 'add_tree_category', 'id' => 'add_category', 'placeholder' => 'Enter Category Name', 'class' => 'form-horizontal', 'role' => 'form')); ?>
                        <div class="form-group">
                            <label class="margin_left_14">Category Name:</label>
                            <div class="col-md-12">
                                <?php echo $this->Form->input('category_name', array('type' => 'text', 'class' => 'form-control', 'label' => false, 'data-bvalidator' => 'required', 'id' => 'add_category_name')); ?>
                                <?php echo $this->Form->input('parent_category_id', array('type' => 'hidden', 'id' => 'parent_category_id')); ?>
                                <?php echo $this->Form->input('target_id', array('type' => 'hidden', 'id' => 'add_target_id')); ?>
                                <?php echo $this->Form->input('main_cat', array('type' => 'hidden', 'id' => 'mat_id', 'value' => $id)); ?>
                                <?php echo $this->Form->input('refpage', array('type' => 'hidden', 'id' => 'refpage', 'value' => 'sub_tree')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="margin_left_14"> Description</label>
                            <div class="col-lg-12">
                                <?php echo $this->Form->input('short_desc', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Enter Description', 'label' => false, 'id' => 'add_short_desc')); ?>
                            </div>
                        </div> 
                        <?php /* <div class="form-group"><label class="margin_left_14">Breadcrumb</label>
                          <div class="col-lg-12">
                          <?php echo $this->Form->input('cat_breadcrub', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Category Breadcrumb', 'label' => false, 'id' => 'add_cat_breadcrub')); ?>
                          </div>
                          </div>
                         */ ?>

                        <div class="form-group"><label class="margin_left_14">Page Title</label>
                            <div class="col-lg-12">
                                <?php echo $this->Form->input('page_title', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Page Title', 'label' => false, 'id' => 'add_page_title')); ?>
                            </div>
                        </div>
                        <div class="form-group"><label class="margin_left_14">Page Description</label>
                            <div class="col-lg-12">
                                <?php echo $this->Form->input('page_desc', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Enter Page Description', 'label' => false, 'id' => 'add_page_desc')); ?>
                            </div>
                        </div>
                        <div class="text-center">
                            <?php echo $this->Form->button('Submit', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
                        </div>
                        <?php echo $this->Form->end(); ?> 
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>