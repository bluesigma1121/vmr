<style>
    .search-block {
        padding: 0;
        text-align: center;
        position: relative;
        margin-bottom: 20px;
        /* background: url(../img/front/bg/7.jpg) 50% 0 repeat fixed; */
    }

    .hr-width{
        margin-bottom: 0px;
        margin-top: 0px;
        margin: 0px;
        border: 0;
        border-top: 1px solid #eee;
    } 
    .mar_span{
        margin-left: 15px;
        margin-right: 15px;

    } 
    .div_mar_l_r{
        margin-left: 10PX;
        margin-right: 15px;
    } 
    .heg_wid{
        height: 100px;
        width:160px;
    } 
    .top_block{
        min-height: 170px;
        max-height: 170px;
    } 
    .headl{
        margin: 0 0 -2px 0;
        padding-bottom:0px;
        display: inline-block;
        border-bottom: 2px solid #72c02c;
        margin-bottom: 1px;
        margin-left: 14px;
    } 
    .cust_line{
        border-bottom: 2px solid #72c02c;
        margin-right: 10px;
        margin-left: 0px;
        margin-top: 10px;
        margin-bottom: 10px;
    } 
    .breadcrumbs {
        overflow: hidden;
        border-bottom: solid 1px #eee;
        /* background: url(../../img/breadcrumbs.png) repeat; */
    } 
    .mar_top_bot{
        margin-top: -31px;
        margin-bottom: -54px;
    }
    .des_p{
        color: #888;
        font-size: 14px;
        margin-left: 10px;
        margin-right: 10px;
    }
    .mar_bt_h1{
        margin-bottom: 5px;
    }
</style>
<!--=== Search Block ===-->
<?php echo $this->element('listing_search'); ?>    
<!--=== End Search Block ===-->

<?php echo $this->element('front_breadcumb'); ?>

<div class="container content"> 
    <div class="row">             
        <!-- Begin Content -->
        <div class="col-md-9">
            <div class="row">
                <div class="headl"><h1 class="mar_bt_h1"><?php echo $main_cat[0]['Category']['category_name']; ?>(<?php echo $main_cat[0]['Category']['no_of_product']; ?>)</h1></div>
                <div class="div_mar_l_r">
                    <p class="des_p">
                        <?php echo $main_cat[0]['Category']['short_desc']; ?>
                    </p>
                </div>
            </div>
            <hr class="cust_line">
            <div class="row">
                <div class="col-md-12">
                    <div class="" id="">
                        <?php
                        foreach ($cat_data['Level1'] as $key1 => $cat):
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <?php if (!empty($cat['Level2'])) { ?>
                                                <ul class="list-unstyled">
                                                    <?php foreach ($cat['Level2'] as $key2 => $cat2): ?>
                                                        <li><i class="fa fa-angle-double-right color-green"></i>
                                                            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $cat2['id'], 'slug' => $this->Link->cleanString($cat2['category_name']))); ?> "><?php echo $cat2['category_name'] ?>(<?php echo $cat2['no_of_product'] ?> )</a>
                                                            <?php if (!empty($cat2['Level3'])) { ?>
                                                                <ul>
                                                                    <?php foreach ($cat2['Level3'] as $key3 => $cat3): ?>
                                                                        <li><i class="fa fa-angle-double-right color-green"></i>   <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $cat3['id'], 'slug' => $this->Link->cleanString($cat3['category_name']))); ?>"><?php echo $cat3['category_name'] ?>(<?php echo $cat3['no_of_product'] ?>)</a>
                                                                            <?php if (!empty($cat3['Level4'])) { ?>
                                                                                <ul>
                                                                                    <?php foreach ($cat3['Level4'] as $key4 => $cat4): ?>
                                                                                        <li class="list-unstyled">
                                                                                            <i class="fa fa-angle-double-right color-green"></i>
                                                                                            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $cat4['id'], 'slug' => $this->Link->cleanString($cat4['category_name']))); ?>"><?php echo $cat4['category_name'] ?> (<?php echo $cat4['no_of_product'] ?>)</a>
                                                                                        </li>
                                                                                    <?php endforeach; ?>
                                                                                </ul>
                                                                            <?php } ?>
                                                                        </li>
                                                                    <?php endforeach; ?>
                                                                </ul>
                                                            <?php } ?>
                                                        <?php endforeach; ?>
                                                    </li>
                                                </ul>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                            <?php endforeach; ?>
                        </div>
                    </div> 
                </div>
                <?php /*
                  <div class="col-md-6">
                  <div class="row">
                  <?php foreach ($relatet_top_product as $pk => $top_prod) { ?>
                  <div class="col-md-6">
                  <div class="bg-light top_block">
                  <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'id' => $top_prod['Product']['id'], 'cat_id' => $main_cat[0]['Category']['id'], 'slug' => $top_prod['Product']['slug'])); ?>  "><h5>


                  <?php if (!empty($top_prod['Product']['product_image'])) { ?>
                  <?php
                  echo $this->Html->image('product_images/' . $top_prod['Product']['product_image'], array('class' => 'img-thumbnail heg_wid'));
                  } else {
                  echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail heg_wid'));
                  }
                  ?>
                  </h5></a>
                  <p><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'id' => $top_prod['Product']['id'], 'cat_id' => $main_cat[0]['Category']['id'], 'slug' => $top_prod['Product']['slug'])); ?> "><h5><?php echo $this->Text->truncate($top_prod['Product']['product_name'], 40); ?></h5></a> </p>
                  </div>
                  </div>
                  <?php } ?>
                  </div>
                  </div>
                 * 
                 */ ?>
            </div>
        </div>

        <div class="col-md-3 section-block">                    

            <div class="headline"><h4>Recently Viewed</h4></div>
            <ul>
                <?php foreach ($recent_view as $key => $rec_view) {
                    ?>
                    <li><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($rec_view['Category']['category_name']), 'id' => $rec_view['Product']['id'], 'slug' => $rec_view['Product']['slug'])); ?> "><h5><?php echo $rec_view['Product']['product_name']; ?></h5></a></li>
                <?php } ?>

            </ul>
            <div class="clearfix"></div>

        </div>
    </div>          
</div>                     
