<script type="text/javascript">
    $(document).ready(function() {
        $('#add_product').bValidator();
        $('#photo_upload').hide();
        $('#photo_chng').click(function() {
          console.log("hello");
            $('#photo_upload').toggle();
        });
    });
</script>

<style>
    .image_size{
        height: 100px;
        width: 100px;
    }
</style>
<?php echo $this->Html->script('back/ckeditor/ckeditor.js'); ?>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5> Add Product</h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('OurClient', array('id' => 'edit_client', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-1">
            <div class="form-group"><label class="col-lg-3 control-label">Client Name</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('client_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Client Name', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-3 control-label">Client Description</label>
                <div class="col-lg-6">
                    <?php echo $this->Form->input('description', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Product Description', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-3 control-label">Client Link</label>
                <div class="col-lg-6">
                    <?php echo $this->Form->input('link', array('class' => 'ckeditor form-control', 'type' => 'text', 'placeholder' => 'Client Link', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3">client image</label>
                <div class="controls col-sm-4">
                    <?php if (!empty($this->request->data['OurClient']['logo'])) { ?>
                        <?php
                        echo $this->Html->image('client_images/' . $this->request->data['OurClient']['logo'], array('class' => 'img-thumbnail image_size'));
                    } else {
                        echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail image_size'));
                    }
                    ?>
                </div>
            </div>
            <?php echo $this->Form->input('id'); ?>
            <?php echo $this->Form->input('prev_photo', array('type' => 'hidden', 'value' => $this->request->data['OurClient']['logo'])); ?>
            <div class="form-group">
                <label class="control-label col-sm-3"></label>
                <div class="controls col-sm-4">
                    <a id="photo_chng">Change Photo
                    </a> &nbsp;&nbsp;&nbsp;
                    <div hidden id="photo_upload">
                        <?php echo $this->Form->input('logo', array('type' => 'file', 'class' => 'form-control', 'label' => false)); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
