<style>
    .max_heh_with{
        height: 100px;
        width: 100px;
    }
</style>
<div class="row">
    <div class="wrapper wrapper-content animated fadeInUp">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <div class="m-b-md text-center">
                                <h2 class="btn btn-primary btn-rounded btn-block">Client Details</h2>
                            </div>
                        </div>
                        <div class="col-lg-4"> </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <dl class="dl-horizontal">
                            <dt>Client Name:</dt> <dd>  <?php echo h($ourClient['OurClient']['client_name']); ?></dd>
                            <dt>Link:</dt> <dd>    <?php echo h($ourClient['OurClient']['link']); ?></dd>
                            <dt>Client Logo Activated:</dt> <dd>    <?php echo h($yes_no[$ourClient['OurClient']['is_active']]); ?></dd>
                            <dt>Created:</dt> <dd>    <?php echo h(Date('d-M-Y,h:i a', strtotime($ourClient['OurClient']['created']))); ?></dd>
                        </dl>
                    </div>
                    <div class="col-lg-6">
                        <dl class="dl-horizontal">
                            <dt>Logo:</dt> <dd>
                                <?php if (!empty($ourClient['OurClient']['logo'])) { ?>
                                    <?php
                                    echo $this->Html->image('client_images/' . $ourClient['OurClient']['logo'], array('class' => 'img-thumbnail max_heh_with image_size'));
                                } else {

                                    echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail max_heh_with image_size'));
                                }
                                ?>
                            </dd>


                        </dl>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-10">
                        <dl class="dl-horizontal">
                            <dt>Client Description:</dt> <dd>      <?php echo h($ourClient['OurClient']['description']); ?></dd>
                        </dl>
                    </div>
                </div>
                <div class="row">
                    <div class="text-center">
                        <button onclick="goBack();" class="btn btn-info btn-rounded btn-outline">Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>