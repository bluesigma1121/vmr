<style>
    .mtp{
        margin-top: 10px;
    }
    .search_mtp{
        margin-top: 3px;
    } 
    .image_size{
        height: 50px;
        width: 50px;
    }
</style> 

<script type="text/javascript">
    $(document).ready(function() {
        $('#client_index').bValidator();
    });
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="pull-right">
                    <?php echo $this->Html->link(__('Add Client'), array('action' => 'add'), array('class' => 'btn   btn-outline btn-sm btn-warning dim', 'title' => 'Add Client')); ?>
                </div>
                <h5>Client List</h5>
            </div>  
            <div class = "ibox-content">
                <?php echo $this->Form->create('OurClient', array('id' => 'client_index', 'class' => 'form-horizontal'));
                ?>
                <div class="row">
                    <div class="col-lg-3">
                        <?php
                        echo $this->Form->input('search_by', array(
                            'options' => $client_search,
                            'class' => 'form-control', 'label' => false, 'default' => 'name', 'id' => 'enquiry_search'));
                        ?>
                    </div>
                    <div class="col-lg-3" id="div_search_text">
                        <?php echo $this->Form->input('search_text', array('type' => 'text', 'class' => 'form-control', 'id' => 'search_text', 'placeholder' => 'Enter Text For Search', 'data-bvalidator' => 'required', 'label' => false)); ?>
                    </div>
                    <div class="col-lg-2 search_mtp">
                        <?php echo $this->Form->button('Search', array('type' => 'submit', 'class' => 'btn btn-sm btn-primary fa fa-search')); ?>
                    </div>
                </div> 
                <?php echo $this->form->end(); ?> 
                <div class="table-responsive mtp">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('client_name'); ?></th>
                                <th><?php echo $this->Paginator->sort('logo'); ?></th>
                                <th><?php echo $this->Paginator->sort('link'); ?></th>
                                <th><?php echo $this->Paginator->sort('Date'); ?></th>
                                <th><?php echo $this->Paginator->sort('is_active', 'Active'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead> 
                        <?php foreach ($ourClients as $ourClient): ?>
                            <td><?php echo h($ourClient['OurClient']['client_name']); ?>&nbsp;</td>
                            <td>
                                <?php if (!empty($ourClient['OurClient']['logo'])) { ?>
                                    <?php
                                    echo $this->Html->image('client_images/' . $ourClient['OurClient']['logo'], array('class' => 'img-thumbnail image_size'));
                                } else {
                                    echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail image_size'));
                                }
                                ?>
                                &nbsp;</td>
                            <td><?php echo h($ourClient['OurClient']['link']); ?>&nbsp;</td>
                            <td><?php echo h(Date('d-M-Y h:i a', strtotime($ourClient['OurClient']['created']))); ?>&nbsp;</td>
                            <td><?php echo h($yes_no[$ourClient['OurClient']['is_active']]); ?>&nbsp;</td>
                            <td class="actions">
                                <?php echo $this->Html->link(__('view'), array('action' => 'view', $ourClient['OurClient']['id']), array('class' => 'btn btn-outline btn-sm btn-warning dim', 'title' => 'View')); ?>
                                <?php if ($ourClient['OurClient']['is_active'] == 1) { ?>
                                    <?php echo $this->Html->link(__('Deactive'), array('action' => 'deactive_logo', $ourClient['OurClient']['id']), array('class' => 'btn btn-outline btn-sm btn-primary dim', 'title' => 'Deactive')); ?>
                                <?php } else { ?>
                                    <?php echo $this->Html->link(__('Active'), array('action' => 'active_logo', $ourClient['OurClient']['id']), array('class' => 'btn btn-outline btn-sm btn-primary dim', 'title' => 'Active')); ?>
                                <?php } ?>
                                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $ourClient['OurClient']['id']), array('class' => 'btn btn-outline btn-sm btn-info dim', 'title' => 'Edit')); ?>
                                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $ourClient['OurClient']['id']), array('class' => 'btn btn-outline btn-sm btn-danger dim', 'title' => 'Delete'), __('Are you sure you want to delete # %s?', $ourClient['OurClient']['client_name'])); ?>
                            </td>
                            </tbody>
                        <?php endforeach; ?>
                    </table>
                    <div class="row col-md-12">
                        <div class="dataTables_paginate paging_bootstrap">
                            <p>
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?> </p>
                            <ul class="pagination" style="visibility: visible;">
                                <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
                                <li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
                                <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
