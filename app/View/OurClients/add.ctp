<?php echo $this->Html->script('back/ckeditor/ckeditor.js'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#add_client').bValidator();
    });
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5> Add Client</h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('OurClient', array('id' => 'add_client', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-1">
            <div class="form-group"><label class="col-lg-3 control-label">Client Name</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('client_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Client Name', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-3 control-label">Client Description</label>
                <div class="col-lg-6">
                    <?php echo $this->Form->input('description', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Client Description', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Link</label>
                <div class="col-lg-6">
                    <?php echo $this->Form->input('link', array('class' => 'ckeditor form-control', 'type' => 'text', 'placeholder' => 'Client Link', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div> 
            <div class="form-group"><label class="col-lg-3 control-label">Client Logo</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('logo', array('class' => 'form-control', 'type' => 'file', 'placeholder' => 'Client Image', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>