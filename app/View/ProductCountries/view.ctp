<div class="productCountries view">
<h2><?php echo __('Product Country'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($productCountry['ProductCountry']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($productCountry['Product']['id'], array('controller' => 'products', 'action' => 'view', $productCountry['Product']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo $this->Html->link($productCountry['Country']['id'], array('controller' => 'countries', 'action' => 'view', $productCountry['Country']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($productCountry['ProductCountry']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($productCountry['ProductCountry']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Product Country'), array('action' => 'edit', $productCountry['ProductCountry']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Product Country'), array('action' => 'delete', $productCountry['ProductCountry']['id']), null, __('Are you sure you want to delete # %s?', $productCountry['ProductCountry']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Product Countries'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product Country'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
	</ul>
</div>
