<!DOCTYPE HTML>
<html lang="en">
    <head>
        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array(
            'front/bootstrap/css/bootstrap.min.css', 'front/style.css', 'front/line-icons/line-icons.css',
            'front/font-awesome/css/font-awesome.min.css', 'front/owl-carousel/owl-carousel/owl.carousel.css',
            'front/sky-forms/version-2.0.1/css/custom-sky-forms.css', 'front/page_search.css',
            'front/custom.css', 'front/shop.style.css', 'front/plugins/scrollbar/src/perfect-scrollbar.css',
            'front/plugins/noUiSlider/jquery.nouislider.css', 'front/theme-colors/default.css', 'front/page_job_inner1.css',
            'front/page_log_reg_v2.css', 'front/bvalidator.css'
        ));
        ?>
        <?php
        echo $this->Html->script(array('front/plugins/jquery/jquery.min.js', 'front/plugins/jquery/jquery-migrate.min.js',
            'front/plugins/bootstrap/js/bootstrap.min.js', 'front/plugins/back-to-top.js', 'front/plugins/jquery.parallax.js',
            'front/plugins/counter/waypoints.min.js', 'front/plugins/counter/jquery.counterup.min.js',
            'front/plugins/owl-carousel/owl-carousel/owl.carousel.js', 'front/custom.js', 'front/app.js',
            'front/plugins/owl-carousel.js', 'front/jquery.bvalidator-yc.js'));
        ?>
    </head>
    <body> 
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->fetch('content'); ?>
    </body>
</html>