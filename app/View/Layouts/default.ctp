<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
    <head>
        <?= $this->element('front_css_js'); ?>
    </head>
    <body>
        <div class="wrapper">
            <?php echo $this->element('front_header');?>
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('content'); ?>
            <?php echo $this->element('front_footer');?>
        </div>
    </body>
</html>
