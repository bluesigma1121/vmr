<script type="text/javascript">
    $(document).ready(function() {
        $('#add_region').bValidator();
        $('#country').multiselect({
            selectedList: 10
        }).multiselectfilter();
    });
</script>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Add Region</h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('Region', array('id' => 'add_region', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-3">
            <div class="form-group"><label class="col-lg-2 control-label">Region Name</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('region_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Region Name', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
        </div>
        <div class="row col-lg-offset-3">
            <div class="form-group"><label class="col-lg-2 control-label">Add Cities</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('country', array('multiple' => 'multiple', 'options' => $country, 'id' => 'country', 'class' => 'form-control', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
        </div>

        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
            <button onclick="goBack();" class="btn btn-sm btn-info">Back</button>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>


