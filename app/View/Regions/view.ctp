<style>
    .mtp{
        margin-top: 10px;
    }
</style>
<div class="row">
    <div class="wrapper wrapper-content animated fadeInUp">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <div class="m-b-md text-center">
                                <h2 class="btn btn-primary btn-rounded btn-block">Region Details</h2>
                            </div>
                        </div>
                        <div class="col-lg-4"> 
                            <div class="pull-right">
                                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $region['Region']['id']), array('class' => 'btn btn-outline btn-sm btn-warning dim', 'title' => 'Edit Region')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <dl class="dl-horizontal">
                            <dt>Region Name:</dt> <dd> <?php echo h($region['Region']['region_name']); ?></dd>

                        </dl>
                    </div>
                    <div class="col-lg-6">
                        <dl class="dl-horizontal">
                            <dt>Created On:</dt> <dd> <?php echo h($region['Region']['created']); ?></dd>
                        </dl>
                    </div>
                </div>
                <div class="row"> 
                    <div class="col-md-3">
                    </div>
                    <table class="table-bordered col-md-6">
                        <thead >
                            <tr  >
                                <th class="text-center">Country Name</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <?php
                        foreach ($country_list as $region) {
                            ?>
                            <tbody class="text-center">
                            <td><?php echo h($region['Country']['country_name']); ?>&nbsp;</td>
                            <td class="actions">
                                <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'country_regions', 'action' => 'delete', $region['CountryRegion']['id']), array('class' => 'btn btn-outline btn-sm btn-danger dim', 'title' => 'Delete'), __('Are you sure you want to Remove  # %s  ?', $region['Country']['country_name'])); ?>
                            </td>
                            </tbody>
                        <?php } ?>
                    </table>
                    <div class="col-md-3">
                    </div>
                </div>
                <div class="row mtp">
                    <div class="text-center">
                        <button onclick="goBack();" class="btn btn-info btn-rounded btn-outline">Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>