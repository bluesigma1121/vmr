<?php App::uses('CakeTime', 'Utility'); ?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <!--   Domain   -->    
    <url>
        <loc><?php echo $this->Html->url('/',true); ?></loc>
        <changefreq>daily</changefreq>
        <priority>1.0</priority>
    </url>
    <!--   Category   -->
    <?php foreach ($categories as $category): ?>
    <url>
        <loc><?php echo (Router::url('/', true) . "industries/" . $category['Category']['slug']); ?></loc>      
        <lastmod><?php echo $this->Time->toAtom($category['Category']['modified']); ?></lastmod>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
    </url>
    <?php endforeach; ?>
    <!--   Product   -->
    <?php foreach ($products as $product): ?>
    <url>
        <loc><?php echo (Router::url('/', true) . "report/" . $product['Product']['slug']); ?></loc>      
        <lastmod><?php echo $this->Time->toAtom($product['Product']['modified']); ?></lastmod>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
    </url>
    <?php endforeach; ?>

<!--   Page   -->
    <url>
        <loc><?php echo (Router::url('/', true) . "about-us") ;?></loc>      
        <lastmod>2018-07-09</lastmod>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo (Router::url('/', true) . "testimonials") ;?></loc>      
        <lastmod>2018-07-09</lastmod>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo (Router::url('/', true) . "contact-us") ;?></loc>      
        <lastmod>2018-07-09</lastmod>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo (Router::url('/', true) . "how-to-order") ;?></loc>      
        <lastmod>2018-07-09</lastmod>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo (Router::url('/', true) . "research-methodology") ;?></loc>      
        <lastmod>2018-07-09</lastmod>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo (Router::url('/', true) . "pressreleases") ;?></loc>      
        <lastmod>2018-07-09</lastmod>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
    </url>
    
    <?php foreach ($pressrelease as $pressrelease): ?>
    <url>
        <loc><?php echo (Router::url('/', true) . "pressreleases/" . strtolower($pressrelease['Article']['slug'])); ?></loc>      
        <lastmod><?php echo $this->Time->toAtom($pressrelease['Article']['modified']); ?></lastmod>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
    </url>
    <?php endforeach; ?>

    <?php foreach ($analysis as $analysis): ?>
    <url>
        <loc><?php echo (Router::url('/', true) . "analysis/" . $analysis['Article']['slug']); ?></loc>      
        <lastmod><?php echo $this->Time->toAtom($analysis['Article']['modified']); ?></lastmod>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
    </url>
    <?php endforeach; ?>

    <url>
        <loc><?php echo (Router::url('/', true) . "privacy-policy") ;?></loc>      
        <lastmod>2018-07-09</lastmod>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo (Router::url('/', true) . "refund-policy") ;?></loc>      
        <lastmod>2018-07-09</lastmod>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo (Router::url('/', true) . "terms-and-conditions") ;?></loc>      
        <lastmod>2018-07-09</lastmod>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo (Router::url('/', true) . "faq") ;?></loc>      
        <lastmod>2018-07-09</lastmod>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
    </url>

</urlset>