<div class="whyus view">
<h2><?php echo __('Whyus'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($whyus['Whyus']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($whyus['Whyus']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($whyus['Whyus']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($whyus['Whyus']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($whyus['Whyus']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Whyus'), array('action' => 'edit', $whyus['Whyus']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Whyus'), array('action' => 'delete', $whyus['Whyus']['id']), array(), __('Are you sure you want to delete # %s?', $whyus['Whyus']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Whyus'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Whyus'), array('action' => 'add')); ?> </li>
	</ul>
</div>
