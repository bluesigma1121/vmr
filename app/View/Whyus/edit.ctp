<script type="text/javascript">
    $(document).ready(function() {
        $('#whyus').bValidator();
    });
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><?php echo __('Edit Whyus'); ?></h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('Whyus', array('id'=>'whyus', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-2">
            <div  class="form-group">
                <label class="col-md-3 control-label">Title</label>
                <div class="col-md-4">
                    <?php echo $this->Form->input('title',array('type' => 'text', 'data-bvalidator' => 'required', 'class' => 'form-control', 'label' => false)); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Description</label>
                <div class="col-md-4">
                    <?php echo $this->Form->input('description', array('type' => 'textarea', 'data-bvalidator' => 'required', 'class' => 'form-control', 'label' => false)); ?>
                </div>
            </div>
            <?php
            echo $this->Form->input('id');
            ?>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-primary btn-sm', 'div' => false)); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
