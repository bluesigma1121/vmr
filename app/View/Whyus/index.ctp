<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
          <h5><?php echo __('Whyus'); ?></h5>
      </div>
      <div class="ibox-content">
        <div class="table-responsive mtp">
          <table class="table table-bordered table-striped">
						<thead>
							<tr>
									<th><?php echo $this->Paginator->sort('id'); ?></th>
									<th><?php echo $this->Paginator->sort('title'); ?></th>
									<th><?php echo $this->Paginator->sort('description'); ?></th>
									<th><?php echo $this->Paginator->sort('created'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($whyuses as $whyus): ?>
								<tr>
									<td><?php echo h($whyus['Whyus']['id']); ?>&nbsp;</td>
									<td><?php echo h($whyus['Whyus']['title']); ?>&nbsp;</td>
									<td><?php echo h($whyus['Whyus']['description']); ?>&nbsp;</td>
									<td><?php echo h($whyus['Whyus']['created']); ?>&nbsp;</td>
									<td class="actions">
										<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $whyus['Whyus']['id']), array('class' => 'btn btn-outline btn-sm btn-info dim', 'title' => 'Edit')); ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<div class="row col-md-12">
						<div class="dataTables_paginate paging_bootstrap">
							<p>
									<?php
									echo $this->Paginator->counter(array(
											'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
									));
										?>
							</p>
							<ul class="pagination" style="visibility: visible;">
									<li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
									<li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
									<li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
