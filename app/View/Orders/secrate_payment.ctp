<?php echo $this->element('listing_search'); ?> 
<div class="deva_cart">
    <?php echo $this->element('front_breadcumb'); ?>
</div>
<div class="job-description">
    <div class="container">
        <div class="shopping-cart mtp_cart" action="#" novalidate="novalidate">
            <div role="application" class="wizard clearfix" id="steps-uid-0">


                <div class="content clearfix">
                    <section id="steps-uid-0-p-0" role="tabpanel" aria-labelledby="steps-uid-0-h-0" class="body current" aria-hidden="false">
                        <div class="table-responsive">
                            <?php if (!empty($prod)) { ?>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>

                                            <th><b>Product</b></th>
                                            <th><b>Qty</b></th>
                                            <th><b>Price</b></th>

                                        </tr>
                                    </thead>
                                    <tbody> 

                                        <tr>
                                            <td class="product-in-table"> 
                                                <?php if (!empty($prod['Product']['product_image'])) { ?> 
                                                    <?php echo $this->Html->image('product_images/' . $prod['product_image'], array('class' => 'img-responsive hei_whidth_cart')); ?>
                                                    <?php
                                                } else {
                                                    echo $this->Html->image('noimage.png', array('class' => 'img-responsive hei_whidth_cart'));
                                                }
                                                ?>

                                                <div class="product-it-in">
                                                    <h3><?php echo $prod['Product']['product_name']; ?></h3>
                                                </div>   
                                            </td>
                                            <td>
                                                <h4>1</h4>
                                            </td>
                                            <td class="shop-red">
                                                <h4><?php echo $prod['Product']['price'] ?></h4>

                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr class="top_border_cart">
                                            <td></td>
                                            <td><span><h3><b>Total</b></h3></span></td>
                                            <td>
                                                <span>
                                                    <?php if ($cur_curency == 0) {
                                                        ?>
                                                        <b><i class="fa fa-inr"></i>  <?php echo $prod['Product']['price'];
                                                        ?></b>
                                                    <?php } else {
                                                        ?>
                                                        <h3> <b><i class="fa fa-dollar"></i><?php
                                                                echo $prod['Product']['price'];
                                                            }
                                                            ?> </b></h3>
                                                </span>
                                            </td>
                                            <td> 
                                            </td>
                                        </tr> 
                                    </tbody>
                                </table>
                            </div>
                        </section> 
                    </div> 
                    <div class="actions clearfix"> 
                        <div class="pull-right">
                            <a class="btn-u btn-u-green" href="<?php echo Router::url(array('action' => 'new_billing_info', $prod['Product']['id'])) ?>">Proceed</a>
                        </div>
                    </div>

                <?php } else {
                    ?>
                    <div class="content clearfix">
                        <span class = "text-center"><h5>There are no items in this Cart. </h5></span>
                        <div class = "text-center">
                            <a class = "btn btn-warning bold" href = "<?= Router::url(array('controller' => 'categories', 'action' => 'home')); ?>">Continue Buying</a>
                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>
    </div> 
</div>






