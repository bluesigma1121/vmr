<style type="text/css">
    .mar_top_hed{
        margin-top: -40px;
        color: #555;
        text-align: center;
        margin-bottom: 5px;
        border-bottom: solid 1px #eee;
    } 
    .inver{
        color: red !important
    }
    .ver{
        color: green !important
    }
    .bor_green{
        border-bottom: solid 2px #1abc9c;
    }
    .text_color{
        color: orange;
    }
    .max_heh_with{
        height: 80px;
        width: 80px;
    }
    .mtp{
        margin-top: 15px;
    }
</style> 
<script type="text/javascript">

    $(document).ready(function() {
        $('#edit_order').bValidator();
    });
</script>


<div class="col-lg-8">
    <div class="wrapper wrapper-content animated fadeInUp">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-b-md">
                            <h5 ><strong class="bor_green">Order  Details</strong></h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 funny-boxes-img">
                        <div class="col-md-6">
                            <strong> Transaction No:</strong>
                        </div>
                        <div class="col-md-6">
                            <?php echo $orders['Order']['transaction_no']; ?>
                        </div>
                    </div>
                    <div class="col-md-6 funny-boxes-img">
                        <div class="col-md-6">
                            <strong> Token:</strong>
                        </div>
                        <div class="col-md-6">
                            <?php echo $orders['Order']['token']; ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 funny-boxes-img">
                        <div class="col-md-6">
                            <strong> Payer Code:</strong>
                        </div>
                        <div class="col-md-6">
                            <?php echo $orders['Order']['payer_code']; ?>
                        </div>
                    </div>
                    <div class="col-md-6 funny-boxes-img">
                        <div class="col-md-6">
                            <strong> Total Cost :</strong>
                        </div>
                        <div class="col-md-6">
                            <?php echo $orders['Order']['total_cost']; ?>
                        </div>
                    </div>
                </div> 

                <div class="row">
                    <div class="col-md-6 funny-boxes-img">
                        <div class="col-md-6">
                            <strong> User Name:</strong>
                        </div>
                        <div class="col-md-6">
                            <?php echo $orders['User']['name']; ?>
                        </div>
                    </div>
                    <div class="col-md-6 funny-boxes-img">
                        <div class="col-md-6">
                            <strong> Email:</strong> 
                        </div>
                        <div class="col-md-6">
                            <?php echo $orders['User']['email']; ?>
                        </div>
                    </div>
                </div>&nbsp;&nbsp;

                <div class="row">
                    <div class="col-md-6 funny-boxes-img">
                        <div class="col-md-6">
                            <strong> Date:</strong>
                        </div>
                        <div class="col-md-6">
                            <?php echo Date('d-M-Y h:i a', strtotime($orders['Order']['created'])); ?>
                        </div>
                    </div>
                    <div class="col-md-6 funny-boxes-img">
                        <div class="col-md-6">
                            <strong> Transaction Status :</strong>
                        </div>
                        <div class="col-md-6">
                            <?php echo h($transaction_status[$orders['Order']['transaction_status']]); ?>
                        </div>
                    </div>
                </div>
                <hr>
                <div Class="row">
                    <div class="col-md-12">
                        <div class="text-left">
                            <h5><strong class="bor_green">Shipping   Details</strong></h5>
                        </div>
                    </div>
                </div>
                &nbsp;&nbsp;
                <div class="row">
                    <div class="col-md-6 funny-boxes-img">
                        <div class="col-md-4">
                            <strong> Name:</strong> 
                        </div>
                        <div class="col-md-8">
                            <?php echo $orders['Order']['ship_user_name']; ?>
                        </div>
                    </div>
                    <div class="col-md-6 funny-boxes-img">
                        <div class="col-md-4">
                            <strong> Address:</strong> 
                        </div>
                        <div class="col-md-8">
                            <?php echo $orders['Order']['ship_address']; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 funny-boxes-img">
                        <div class="col-md-4">
                            <strong> City :</strong> 
                        </div>
                        <div class="col-md-8">
                            <?php echo $orders['Order']['city']; ?>
                        </div>
                    </div>
                    <div class="col-md-6 funny-boxes-img">
                        <div class="col-md-4">
                            <strong> State:</strong> 
                        </div>
                        <div class="col-md-8">
                            <?php echo $orders['Order']['state']; ?>
                        </div>
                    </div>
                </div>   

                <div class="row">
                    <div class="col-md-6 funny-boxes-img">
                        <div class="col-md-4">
                            <strong> Country:</strong> 
                        </div>
                        <div class="col-md-8">
                            <?php echo $orders['Order']['country']; ?>
                        </div>
                    </div>
                    <div class="col-md-6 funny-boxes-img">
                        <div class="col-md-4">
                            <strong> Pin code: </strong> 
                        </div>
                        <div class="col-md-8">
                            <?php echo $orders['Order']['pincode']; ?>
                        </div>
                    </div>
                </div>   
                <hr>
                <div class="row">
                    <div class="col-lg-2 ml_15"><h5><strong class="bor_green">Status  History</strong></h5></div>
                    <div class="col-lg-9">
                        <?php if (!empty($prev_rem)) { ?>
                            <?php
                            foreach ($prev_rem as $key => $remarks) {
                                ?>
                                <div class="row well"> 
                                    <?php echo $remarks['message'] ?>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <strong>  Status: </strong><?php echo h($transaction_status[$orders['Order']['transaction_status']]); ?>
                                        </div>
                                        <div class="col-lg-4">
                                            <?php echo Date('d-m-Y h:i a', strtotime($remarks['date'])); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <hr>
                <div Class="row">
                    <div class="col-md-12">
                        <div class="text-left">
                            <h5><strong class="bor_green">Product  Details</strong></h5>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="table-responsive mtp center">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Product Name</th>
                                        <th>Product No</th>
                                        <th>Category Name</th>

                                        <th>Price</th>
                                    </tr>
                                </thead> 
                                <?php
                                foreach ($products as $product) {
                                    ?>
                                    <tbody>
                                    <td><?php echo h(ucfirst($product['OrderProduct']['product_name'])); ?>&nbsp;</td>
                                    <td><?php echo h($product['OrderProduct']['product_no']); ?>&nbsp;</td>
                                    <td><?php echo h($product['Category']['category_name']); ?>&nbsp;</td>
                                    <td> <?php echo $product['OrderProduct']['price']; ?>  <?php echo $product['Order']['currency']; ?></td>
                                    </tbody>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 mtp">
                        <div class="text-center">
                            <a href="<?php echo Router::url(array('controller' => 'orders', 'action' => 'index')); ?>" class="btn btn-info"> <i class="fa fa-backward"></i> &nbsp;Back</a>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div> 
</div> 
<div class="col-lg-4">
    <div class="wrapper wrapper-content">
        <div class="col-md-12">
            <div class="">
                <h4><strong class="bor_green center">Change Status </strong></h4>
            </div>
        </div>
        &nbsp;&nbsp;
        <?php echo $this->Form->create('Order', array('id' => 'edit_order', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row">
            <div class="col-lg-offset-1">
                <div class="form-group col-lg-offset-3">
                    <label class="col-lg-2 control-label">Status</label>&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="">
                        <?php echo $this->Form->input('transaction_status', array('class' => 'form-control', 'options' => $transaction_status, 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                &nbsp;&nbsp;
                <div class="form-group col-lg-offset-3"><label class="col-lg-2 control-label">Remark</label>
                    <div class=""style="padding: 0">
                        <?php echo $this->Form->input('new_remark', array('class' => 'form-control', 'type' => 'textarea', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
            </div> 
        </div> 

        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?> 
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>















