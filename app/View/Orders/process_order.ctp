<div class="job-description">
    <div class="container">
        <div class="shopping-cart mtp" action="#" novalidate="novalidate">
            <div role="application" class="wizard clearfix" id="steps-uid-0">
                <h2><?php echo $product['Product']['product_name'] ?></h2>
                <ul class="details list-inline">
                    <li>
                        <span><strong>License Type :</strong>
                            <?php echo $licence_type[$cart_item['CartItem']['licence_type']] ?>
                        </span>
                    </li>
                    <li class="spart_pd">
                        <span>
                            &nbsp;&nbsp;<strong>Price : </strong>
                            <?php if ($cart_item['CartItem']['licence_type'] == 1): ?>
                                $<?php echo $product['Product']['price'] ?>
                            <?php elseif ($cart_item['CartItem']['licence_type'] == 2): ?>
                                $<?php echo $product['Product']['corporate_price'] ?>
                            <?php endif; ?>
                        </span>
                    </li>
                </ul>
                <div class="content clearfix">
                    <section class="billing-info body" id="steps-uid-0-p-1" role="tabpanel" aria-labelledby="steps-uid-0-h-1"  >
                        <div class="row">
                            <div class="col-md-8 col-lg-offset-2">
                                <div class="row">
                                    <h2 class="title-type">Billing Address</h2>
                                    <div class="col-md-12">
                                        <?php echo $this->Form->create('User', array('id' => 'bill_add_cart', 'class' => 'reg-page', 'type' => 'file')); ?>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>First Name <span class="color-red">*</span></label>
                                                <?php echo $this->Form->input('first_name', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'First Name', 'data-bvalidator' => 'alpha,required', 'value' => AuthComponent::user('first_name'))); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Last Name <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('last_name', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Last Name', 'data-bvalidator' => 'alpha,required', 'value' => AuthComponent::user('last_name'))); ?>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <?php if (AuthComponent::user()): ?>
                                                    <?php echo $this->Form->input('email', array('type'=>'hidden','class' => 'form-control margin-bottom-20', 'label' => false, 'placeholder' => 'Email', 'data-bvalidator' => 'email,required', 'value' => AuthComponent::user('email'))); ?>
                                            <div class="col-sm-12">
                                            <?php else: ?>
                                                <div class="col-sm-6">
                                                    <label>Email <span class="color-red">*</span></label>
                                                    <?php echo $this->Form->input('email', array('class' => 'form-control margin-bottom-20', 'label' => false, 'placeholder' => 'Email', 'data-bvalidator' => 'email,required')); ?>
                                                </div>
                                            <div class="col-sm-6">
                                            <?php endif; ?>
                                            
                                                <label>Mobile <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Mobile Number', 'size' => '20', 'maxlength' => '25', 'data-bvalidator' => 'required', 'value' => AuthComponent::user('mobile'))); ?>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Organisation <span class="color-red">*</span></label>
                                                <?php echo $this->Form->input('organisation', array('class' => 'form-control margin-bottom-20', 'label' => false, 'placeholder' => 'Organisation', 'data-bvalidator' => 'required', 'value' => AuthComponent::user('organisation'))); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Designation <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('job_title', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'data-bvalidator' => 'required', 'placeholder' => 'Designation', 'value' => AuthComponent::user('job_title'))); ?>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Address <span class="color-red">*</span></label>
                                                <?php echo $this->Form->input('address', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Address', 'data-bvalidator' => 'required', 'value' => AuthComponent::user('address'))); ?>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>City <span class="color-red">*</span></label>
                                                <?php echo $this->Form->input('city', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'city', 'data-bvalidator' => 'required', 'value' => AuthComponent::user('city'))); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>State <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('state', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'State', 'data-bvalidator' => 'required', 'value' => AuthComponent::user('state'))); ?>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Country <span class="color-red">*</span></label>
                                                <?php echo $this->Form->input('country', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Country', 'data-bvalidator' => 'required', 'value' => AuthComponent::user('country'))); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Pincode <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('pin_code', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Pincode', 'data-bvalidator' => 'required')); ?>
                                            </div>
                                        </div> 

                                        <div class="row">
                                            <div class="col-lg-9 checkbox">
                                                <label>
                                                    <?php echo $this->Form->checkbox('term', array('label' => false, 'data-bvalidator' => 'required')); ?>
                                                    I read <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'terms_conditions')); ?>"  target="_blank" class="color-green">Terms and Conditions</a>
                                                </label>    
                                                <label>* Due to the nature of goods, orders once placed can't be canceled.</label>
                                            </div>
                                            <div class="col-lg-3 text-right">
                                                <?php echo $this->Form->submit('Proceed to payment', array('class' => 'btn-u')) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php /*
                                <div class="actions clearfix mtp"> 
                                    <div class="pull-left">
                                        <a class="btn-u" href="<?php echo Router::url(array('controller' => 'cart_items', 'action' => 'add_to_cart')) ?>">Back</a>
                                    </div>
                                    <div class="pull-right">
                                        <?php echo $this->Form->submit('Proceed', array('class' => 'btn-u')) ?>
                                    </div>
                                </div>
                                 * 
                                 */?>
                                <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                    </section>
                </div> 
            </div>
        </div>
    </div>
</div>


