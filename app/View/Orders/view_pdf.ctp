<?php

App::import('Vendor', 'tcpdf/tcpdf');
$tcpdf = new TCPDF();
$textfont = 'helvetica';

$tcpdf->SetAuthor("valuemarketresearch.com Order Invoice");
$tcpdf->SetAutoPageBreak(true);

$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(false);

$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont, '', 9);

$tcpdf->AddPage();
//$member_application_number = str_pad($order_id, 6, '0', STR_PAD_LEFT);
$member_application_number = $order_details['Order']['invoice_no'];
$tans_no = $order_details['Order']['transaction_no'];
$invoice_to = $order_details['Order']['ship_user_name'];
$address = $order_details['Order']['ship_address'];
$city = $order_details['Order']['city'];
$state = $order_details['Order']['state'];
$country = $order_details['Order']['country'];
$pincode = $order_details['Order']['pincode'];
$mobile = $order_details['Order']['mobile'];
$email = $order_details['Order']['ship_email'];
$created = Date('d-M-Y h:i a', strtotime($order_details['Order']['created']));
$product_text = null;
$invoice_pdf_name = $order_details['Order']['invoice_pdf_name'];
$url_home = Router::url('/', true);
foreach ($order_product as $key => $product):
    if ($product['OrderProduct']['is_offer_set'] == 1) {
        $price = $product['OrderProduct']['offer_price'];
    } else {
        $price = $product['OrderProduct']['price'];
    }
    $product_text .= "<tr><td>" . $product['OrderProduct']['product_no'] . "</td><td>" . $product['OrderProduct']['product_name'] . "</td><td>" . $price . ' ' . $product['Order']['currency'] . "</td></tr>";
endforeach;
$total = $order_details['Order']['total_cost'] . ' ' . $order_details['Order']['currency'];
// create some HTML content
$htmlcontent = <<<EOF
    <style>
    h1 {
        color: navy;
        font-family: times;
        font-size: 20pt;
    }
    p.first {
        color: #003300;
        font-family: helvetica;
        font-size: 12pt;
    }
    p.first span {
        color: #006600;
        font-style: italic;
    }
    p#second {
        color: rgb(00,63,127);
        font-family: times;
        font-size: 12pt;
        text-align: justify;
    }
    p#second > span {
        background-color: #FFFFAA;
    }
    table.first {
        color: #003300;
        font-family: helvetica;
        font-size: 10pt;
    }

    td.photo {
        border: 1px solid black;
        height : 100px;
    }
   .btm_border{
                   border-style: solid;
    border-width: 15px;
         border-color: navy;
                }

</style>

<center>
    <h1 class="title"  align="left">valuemarketresearch.com</h1></center>
        <h3 class="top_margin"  align="left">Invoice</h3>
   <table class="btm_border" cellpadding="4" cellspacing="6">
 <tr class="">
      <td>

        </td>
   </tr>
</table>


        <table class="first" cellpadding="1" cellspacing="4">
 <tr>
    <td><b>Invoice Number</b></td>
    <td>$member_application_number</td>
    <td><b>Transaction Id</b></td>
   <td>$tans_no</td>
    </tr>
       <tr>
    <td><b>Invoice To</b></td>
    <td>$invoice_to</td>
    <td><b>Invoice Date</b></td>
   <td> $created</td>
    </tr>

         <tr>
    <td><b>Contact Number</b></td>
    <td>$mobile</td>
    <td><b>Email</b></td>
   <td> $email</td>
    </tr>

    <tr>
    <td><b>Invoice Address</b></td>
    <td>
        $address,
        $city,
         $state,
         $country,
         $pincode.
      </td>
    </tr>
</table>


EOF;
// output the HTML content
$tcpdf->writeHTML($htmlcontent, true, false, false, false, '');

// create some HTML content
$htmlcontent = <<<EOF
    <style>
    h1 {
        color: navy;
        font-family: times;
        font-size: 16pt;
    }
    p.first {
        color: #003300;
        font-family: helvetica;
        font-size: 12pt;
    }
    p.first span {
        color: #006600;
        font-style: italic;
    }
    p#second {
        color: rgb(00,63,127);
        font-family: times;
        font-size: 12pt;
        text-align: justify;
    }
        .footer{
         color: rgb(00,63,127);
        font-family: times;
        font-size: 8pt;
        text-align: left;
            }
    .bt_border{
                   border-style: solid;
    border-width: 5px;
         border-color: navy;
        margin-top:10px;
                }
        .font_dtl{
              font-family: times;
        font-size: 8pt;
        font-style:normal
   }
        .font_10{
        font-size: 12pt;
            }

</style>
<h1>Product Details</h1>
    <table class="first" cellpadding="4" cellspacing="6">
 <tr>
    <td width="10%"><b>Product Number</b></td>
    <td width="70%"><b>Title</b></td>
     <td width="20%"><b>Price</b></td>
    </tr>

        $product_text

       <tr>
    <td><b></b></td>

    <td class="font_10"><b>Total</b></td>
   <td class="font_10"><b>$total</b></td>
    </tr>

</table>

            <h4 class="font_dtl"  align="left">


Please use following invoice number as reference :  $member_application_number <br>
            </h4>

         <table class="bt_border" cellpadding="4" cellspacing="6">
 <tr class="">
      <td>
                </td>
   </tr>
</table>

          <table class="footer" cellpadding="4" cellspacing="6">
 <tr class="">
      <td>
                </td>
   </tr>
</table>

EOF;
// output the HTML content
$tcpdf->writeHTML($htmlcontent, true, false, false, false, '');
//debug(WEBROOT_DIR);
$tcpdf->Output(APP . DS . WEBROOT_DIR . DS . 'pdf' . DS . $invoice_pdf_name . '.pdf', 'F');
?>
