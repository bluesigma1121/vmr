<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>valuemarketresearch.com</title>
        <!-- Bootstrap core CSS -->
        <?php echo $this->Html->css(array('front/bootstrap/css/bootstrap.min.css', 'front/bvalidator.css')); ?>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .mar_top_hed{
                margin-top: -40px;
                color: #555;
                text-align: center;
                margin-bottom: 5px;
                border-bottom: solid 1px #eee;

            }
            .inver{
                color: red !important
            }
            .ver{
                color: green !important
            }
            .bor_green{
                border-bottom: solid 2px #1abc9c;
            }
        </style>

        <?= $this->Html->script(array('front/plugins/jquery/jquery.min.js',  'front/plugins/bootstrap/js/bootstrap.min.js', 'front/jquery.bvalidator-yc.js')); ?>
    </head>
    <body>
        <div class="container content">
            <div class="row">
                <!-- Begin Sidebar Menu -->
                <div class="col-md-12">
                    <div class="text-center">
                        <h3>Order Details</h3>
                    </div>
                    <div class="funny-boxes funny-boxes-top-sea">

                        <div class="row">
                            <div class="col-md-4 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Token :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['token']; ?>
                                </div>
                            </div>

                            <div class="col-md-4 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Payer Code :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['payer_code']; ?>
                                </div>
                            </div>

                            <div class="col-md-4 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Date :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo Date('d-M-Y h:i a', strtotime($orders['Order']['created'])); ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> User Name :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['User']['name']; ?>
                                </div>
                            </div>

                            <div class="col-md-4 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Total Cost :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['total_cost']; ?>
                                </div>
                            </div>

                            <div class="col-md-4 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Email :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['User']['email']; ?>
                                </div>
                            </div>
                        </div>
                        <div Class="row">
                            <div class="text-left">
                                <h5><strong class="bor_green">Shipping   Details</strong></h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Name :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['ship_user_name']; ?>
                                </div>
                            </div>
                            <div class="col-md-4 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Address :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['ship_address']; ?>
                                </div>
                            </div>
                            <div class="col-md-4 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> City :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['city']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> State :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['state']; ?>
                                </div>
                            </div>
                            <div class="col-md-4 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Country :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['country']; ?>
                                </div>
                            </div>
                            <div class="col-md-4 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Pin code :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['pincode']; ?>
                                </div>
                            </div>
                        </div>
                        <div Class="row">
                            <div class="text-left">
                                <h5 ><strong class="bor_green">Product  Details</strong></h5>
                            </div>
                        </div>
                        <div Class="row">
                            <div class="text-center">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
