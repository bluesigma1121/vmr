<style>
    .mtp{
        margin-top: 10px;
    }
    .search_mtp{
        margin-top: 3px;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Orders</h5> 
            </div>
            <div class="ibox-content">
                <?php echo $this->Form->create('Order', array('id' => 'index', 'class' => 'form-horizontal')); ?>
                <div class="row">
                    <div class="col-lg-3">
                        <?php
                        echo $this->Form->input('search_by', array(
                            'options' => $order_search,
                            'class' => 'form-control', 'label' => false, 'default' => 'vendor_name', 'data-bvalidator' => 'required'));
                        ?>
                    </div>
                    <div class="col-lg-3">
                        <?php echo $this->Form->input('search_text', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Enter Text For Search', 'label' => false)); ?>
                    </div>
                    <div class="col-lg-2 search_mtp">
                        <?php echo $this->Form->button('Search', array('type' => 'submit', 'class' => 'btn btn-outline btn-sm btn-primary fa fa-search')); ?>
                    </div>
                </div>
                <?php echo $this->form->end(); ?>

                <div class="table-responsive mtp">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('user_id'); ?></th>
                                <th><?php echo $this->Paginator->sort('transaction_no'); ?></th>
                                <th><?php echo $this->Paginator->sort('total_cost'); ?></th>
                                <th><?php echo $this->Paginator->sort('currency'); ?></th>
                                <th><?php echo $this->Paginator->sort('created'); ?></th>
                                <th><?php echo $this->Paginator->sort('transaction_status'); ?></th>

                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead> 
                        <?php
                        foreach ($orders as $order) {
                            ?>
                            <tbody>
                            <td><?php echo h(ucfirst($order['User']['name'])); ?>&nbsp;</td>
                            <td><?php echo h($order['Order']['transaction_no']); ?>&nbsp;</td>
                            <td><?php echo h($order['Order']['total_cost']); ?>&nbsp;</td>
                            <td><?php echo h($order['Order']['currency']); ?>&nbsp;</td>
                            <td><?php echo h(Date('d-M-y h:i a', strtotime($order['Order']['created']))) ?></td>
                            <td><?php echo h($transaction_status[$order['Order']['transaction_status']]); ?>&nbsp;</td>

                            <td class="actions">
                                <?php echo $this->Html->link(__('View/Update Order'), array('action' => 'view', $order['Order']['id']), array('class' => 'btn   btn-outline btn-sm btn-warning ', 'title' => 'View')); ?>
                            </td>
                            </tbody>
                        <?php } ?>
                    </table>
                    <div class="row col-md-12">
                        <div class="dataTables_paginate paging_bootstrap">
                            <p>
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?> </p>
                            <ul class="pagination" style="visibility: visible;">
                                <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
                                <li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
                                <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
