<div class="breadcrumbs border_tp">
    <div class="container">
        <h1 class="pull-left">Account</h1>
        <ul class="pull-right breadcrumb">
            <span class="text_color_bread">Your Are Here :</span>
            <li><a href="<?php echo Router::url('/') ?>">Home</a></li>
            <li><a href="#">Your Account</a></li>
        </ul>
    </div>
</div>
<div class="container content">
    <div class="row">
        <div class="col-md-12">
            <!-- Tabs -->
            <div class="tab-v2 margin-bottom-60">
                <?php echo $this->element('my_account_panel'); ?>
                <div class="tab-content">
                    <div id="enquiries" class="inside">
                        <p>
                            Please find below your Orders history with valuemarketresearch.com.
                        </p> <hr>
                        <?php /*if (!empty($user_orders)) { ?>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th> Transaction No</th>
                                            <th> Token Code </th>
                                            <th> Date </th>
                                            <th> Total Cost </th>
                                            <th>Action </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $cnt = 0;
                                        foreach ($user_orders as $key => $order) {
                                            ?>
                                            <tr>
                                                <td><?php echo++$cnt; ?></td>
                                                <td><?php echo $order['Order']['transaction_no']; ?></td>
                                                <td><?php echo $order['Order']['token']; ?></td>

                                                <td><?php echo Date('d-M-Y h:i a', strtotime($order['Order']['created'])); ?></td>
                                                <td><?php echo $order['Order']['total_cost']; ?> <?php echo $order['Order']['currency']; ?></td>
                                                <td> <a href="<?php echo Router::url(array('controller' => 'orders', 'action' => 'user_order_view', $order['Order']['id'])); ?>" class="btn btn-info btn-xs view_products"><i class="fa fa-eye"></i> view</a></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                        <?php } */ ?>



                        <?php if (!empty($transactions)) { ?>
                        <?php //debug($transactions); ?>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th> Txn ID</th>
                                            <th> Product </th>
                                            <th> Date </th>
                                            <th> Cost </th>
                                            <th> Status </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $cnt = 0;
                                        foreach ($transactions as $key => $transaction) {
                                            ?>
                                            <tr>
                                                <td><?php echo ++$cnt; ?></td>
                                                <td><?php echo $transaction['Transaction']['txnid']; ?></td>
                                                <td><?php echo $transaction['Product']['product_name']; ?></td>

                                                <td><?php echo Date('d-M-Y h:i a', strtotime($transaction['Transaction']['created'])); ?></td>
                                                <td><?php echo $transaction['Transaction']['amount']; ?></td>
                                                <td><?php echo $txn_status[$transaction['Transaction']['status']]; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                        <?php } ?>



                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        </div>
    </div>
</div>
