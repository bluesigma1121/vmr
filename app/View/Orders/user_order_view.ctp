<style>
    .cross_line{
        color:red;
        text-decoration:line-through
    }
</style>
<div class="container content">
    <div class="row">
        <div class="col-md-9">
            <!-- Tabs -->
            <div class="tab-v2 margin-bottom-60">
                <?php echo $this->element('my_account_panel'); ?>              
                <div class="tab-content">
                    <div id="enquiries" class="inside"> 
                        <div class="row">
                            <div class="col-md-12 funny-boxes-img">
                                <div class="text-center">
                                    <a href="<?php echo Router::url(array('controller' => 'orders', 'action' => 'user_order_index')); ?>" class="btn btn-warning"> <i class="fa fa-backward"></i> &nbsp;Back To All Orders</a>
                                </div>
                            </div>
                        </div>
                        <div Class="row">
                            <div class="col-md-12">
                                <div class="text-left">
                                    <h5 ><strong class="bor_green_user_order">Order  Details</strong></h5>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Transaction No :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['transaction_no']; ?>
                                </div>
                            </div>
                            <div class="col-md-6 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Token :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['token']; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Date :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo Date('d-M-Y h:i a', strtotime($orders['Order']['created'])); ?>
                                </div>
                            </div>
                            <div class="col-md-6 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> User Name :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['User']['name']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Total Cost :</strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['total_cost']; ?> &nbsp;&nbsp;<?php echo $orders['Order']['currency']; ?>

                                </div>
                            </div>

                            <div class="col-md-6 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Email :</strong> 
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['User']['email']; ?>
                                </div>
                            </div>
                        </div>

                        <div Class="row">
                            <div class="col-md-12">
                                <div class="text-left">
                                    <h5><strong class="bor_green_user_order">Shipping   Details</strong></h5>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Name :</strong> 
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['ship_user_name']; ?>
                                </div>
                            </div>
                            <div class="col-md-6 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Address :</strong> 
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['ship_address']; ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> City :</strong> 
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['city']; ?>
                                </div>
                            </div>
                            <div class="col-md-6 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> State :</strong> 
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['state']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Country :</strong> 
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['country']; ?>
                                </div>
                            </div>
                            <div class="col-md-6 funny-boxes-img">
                                <div class="col-md-4">
                                    <strong> Pin code :</strong> 
                                </div>
                                <div class="col-md-8">
                                    <?php echo $orders['Order']['pincode']; ?>
                                </div>
                            </div>
                        </div> 

                        <div Class="row">
                            <!-- Accordion-->
                            <div class="col-md-12">
                                <h5 ><strong class="bor_green_user_order">Product  Details</strong></h5>
                                <div class="panel-group acc-v1" id="accordion">
                                    <?php foreach ($products as $pkey => $product): ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $product['OrderProduct']['id']; ?>">
                                                        <?php
                                                        echo $this->Text->truncate(
                                                                $product['OrderProduct']['product_name'], 100, array(
                                                            'ellipsis' => '...',
                                                            'exact' => false
                                                                )
                                                        );
                                                        ?>
                                                        <div class="pull-right">
                                                            <?php if ($product['OrderProduct']['is_offer_set'] == 1) { ?>
                                                                <?php echo $product['OrderProduct']['offer_price']; ?>&nbsp;&nbsp;<?php echo $product['Order']['currency']; ?>
                                                            <?php } else { ?>
                                                                <?php echo $product['OrderProduct']['price']; ?>&nbsp;&nbsp;<?php echo $product['Order']['currency']; ?>
                                                            <?php } ?>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div id="collapseOne<?php echo $product['OrderProduct']['id']; ?>" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="row">

                                                        <div class="col-md-6 funny-boxes-img">
                                                            <div class="col-md-4">
                                                                <strong> Product Name :</strong> 
                                                            </div>
                                                            <div class="col-md-8">
                                                                <?php echo $product['OrderProduct']['product_name']; ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 funny-boxes-img">
                                                            <div class="col-md-4">
                                                                <strong> Price :</strong> 
                                                            </div>
                                                            <div class="col-md-8">


                                                                <?php if ($product['OrderProduct']['is_offer_set'] == 1) { ?>
                                                                    <span class="cross_line"><?php echo $product['OrderProduct']['price']; ?></span>&nbsp;&nbsp; <?php echo $product['OrderProduct']['offer_price']; ?>&nbsp;<?php echo $product['Order']['currency']; ?>
                                                                <?php } else { ?>
                                                                    <?php echo $product['OrderProduct']['price']; ?>&nbsp;&nbsp; <?php echo $product['Order']['currency']; ?>
                                                                <?php } ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-md-6 funny-boxes-img">
                                                            <div class="col-md-4">
                                                                <strong> Product No :</strong> 
                                                            </div>
                                                            <div class="col-md-8">
                                                                <?php echo $product['OrderProduct']['product_no']; ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 funny-boxes-img">
                                                            <div class="col-md-4">
                                                                <strong> Category :</strong> 
                                                            </div>
                                                            <div class="col-md-8">
                                                                <?php echo $product['Category']['category_name']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6 funny-boxes-img">
                                                            <div class="col-md-4">
                                                                <strong> Logo :</strong> 
                                                            </div>
                                                            <div class="col-md-6">
                                                                <?php if (!empty($product['Product']['product_image'])) { ?>
                                                                    <?php
                                                                    echo $this->Html->image('product_images/' . $product['Product']['product_image'], array('class' => 'img-thumbnail max_heh_with_user_view image_size'));
                                                                } else {

                                                                    echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail max_heh_with_user_view image_size'));
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6 funny-boxes-img">
                                                            <div class="col-md-4">
                                                                <strong> Licence Type :</strong> 
                                                            </div>
                                                            <div class="col-md-6">
                                                                <?php
                                                                if (($product['OrderProduct']['licence_type'] != 0) && !empty($product['OrderProduct']['licence_type'])) {
                                                                    echo $licence_type[$product['OrderProduct']['licence_type']];
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 funny-boxes-img">
                                                            <div class="col-md-2">
                                                                <strong> Description :</strong> 
                                                            </div>
                                                            <div class="col-md-10">
                                                                <?php echo strip_tags($product['OrderProduct']['product_description']); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 funny-boxes-img">
                                                            <div class="text-center">
                                                                <a href="<?php echo Router::url(array('controller' => 'orders', 'action' => 'user_order_index')); ?>" class="btn btn-warning"> <i class="fa fa-backward"></i> &nbsp;Back To All Orders</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div><!--/acc-v1-->
                            </div><!--/col-md-7-->
                            <!-- End Accordion-->
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
    </div>
</div>          


