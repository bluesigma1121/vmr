<style>
/*404 Error Page v1 
------------------------------------*/
.error-v1 {
	padding-bottom: 30px;
	text-align: center;	
}

.error-v1 p {
	color: #555;
	font-size: 16px;
}

.error-v1 span {
	color: #555;
	display: block;
	font-size: 35px;
	font-weight: 200;
}

.error-v1 span.error-v1-title {
	color: #777;	
	font-size: 180px;
	line-height: 200px;
	padding-bottom: 20px;
}

/*For Mobile Devices*/
@media (max-width: 500px) { 
	.error-v1 p {
		font-size: 12px;
	}	

	.error-v1 span {
		font-size: 25px;
	}

	.error-v1 span.error-v1-title {
		font-size: 140px;
	}
}
</style>

<div class="container content">		
    <!--Error Block-->
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="error-v1 ">
                <span class="error-v1-title"></span>
                <span>Ooopss... </span>
                <p class="text-center">The content you are looking for seems to be moved away. Kindly write to us at <a href="mailto:sales@valuemarketresearch.com">sales@valuemarketresearch.com</a> or continue browsing...<a href="<?php echo Router::url('/') ?>">click here</a> </p>
				<br>
				<div class="enq-btn">
                <a class="btn-u btn-u-green" href="<?php echo Router::url('/') ?>"><i class="fa fa-home fa-lg" aria-hidden="true"></i> Visit Home Page</a>
				</div>
            </div>
        </div>
    </div>
    <!--End Error Block-->
</div>