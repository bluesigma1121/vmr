
<style>
    .image_size{
        /* height: 100px; */
        width: 100px;
    }
</style>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><?php echo __('Edit Slideshow'); ?></h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('Slideshow', array('id' => 'edit_client', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-1">

           <div class="form-group">
                <label class="control-label col-sm-2">Image</label>
								<div class="controls col-sm-4">
									<?php echo $this->Form->input('image', array('type' => 'file', 'class' => 'form-control', 'label' => false)); ?>
								</div>
                <div class="controls col-sm-4">
                    <?php if (!empty($this->request->data['Slideshow']['image'])) { ?>
                        <?php
                        echo $this->Html->image('slideshow/' . $this->request->data['Slideshow']['image'], array('class' => 'img-thumbnail image_size'));
                    } else {
                        echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail image_size'));
                    }
                    ?>
                </div>
            </div>
            <?php echo $this->Form->input('id'); ?>
            <?php echo $this->Form->input('prev_photo', array('type' => 'hidden', 'value' => $this->request->data['Slideshow']['image'])); ?>


        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
