<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo __('Add Slideshow'); ?></title>
        <!-- Bootstrap core CSS -->
        <?php echo $this->Html->css(array('front/bootstrap/css/bootstrap.min.css', 'front/bvalidator.css')); ?>

        <?= $this->Html->script(array('front/plugins/jquery/jquery.min.js',  'front/plugins/bootstrap/js/bootstrap.min.js', 'front/jquery.bvalidator-yc.js')); ?>
				<script type="text/javascript">
				$(document).ready(function() {
					$('#add_slideshow').bValidator();
				});
				</script>
    </head>
    <body>

        <div class="container content">
            <div class="row">
                <!-- Begin Sidebar Menu -->
                <h3><?php echo __('Add Slideshow'); ?></h3>
                <div class="funny-boxes funny-boxes-top-sea">
                    <div class="ibox-content">
											<?php echo $this->Form->create('Slideshow', array('id' => 'add_slideshow','class' => 'form-horizontal', 'type' => 'file')); ?>
                        <div class="row col-lg-offset-1">
													<div class="form-group"><label class="col-lg-3 control-label">Image</label>
														<div class="col-lg-4">
															<?php echo $this->Form->input('image', array('class' => 'form-control', 'type' => 'file', 'placeholder' => 'Client Image', 'label' => false, 'data-bvalidator' => 'required')); ?>
														</div>
													</div>

                        </div>
                        <div class="text-center">
                            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
