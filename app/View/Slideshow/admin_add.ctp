<div class="slideshow form">
<?php echo $this->Form->create('Slideshow'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Slideshow'); ?></legend>
	<?php
		echo $this->Form->input('image');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Slideshow'), array('action' => 'index')); ?></li>
	</ul>
</div>
