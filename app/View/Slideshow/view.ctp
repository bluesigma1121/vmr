<style>
    .max_heh_with{
        /* height: 100px; */
        width: 200px;
    }
</style>
<div class="row">
    <div class="wrapper wrapper-content animated fadeInUp">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <div class="m-b-md text-center">
                                <h2 class="btn btn-primary btn-rounded btn-block"><?php echo __('Slideshow'); ?></h2>
                            </div>
                        </div>
                        <div class="col-lg-4"> </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <dl class="dl-horizontal">
                            <dt>Image:</dt>
                            <dd> <?php if (!empty($slideshow['Slideshow']['image'])) { ?>
                              <?php
                                    echo $this->Html->image('slideshow/' . $slideshow['Slideshow']['image'], array('class' => 'img-thumbnail max_heh_with'));
                                } else {
                                    echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail image_size'));
                                }
                                ?>
                            </dd>
                        </dl>
                    </div>
                </div>
                <div class="row">
                    <div class="text-center">
                        <button onclick="goBack();" class="btn btn-info btn-rounded btn-outline">Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
