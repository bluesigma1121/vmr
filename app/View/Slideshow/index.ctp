
<style>
    .image_size{
        /* height: 50px; */
        width: 50px;
    }
</style>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
       	<div class="pull-right">
          <?php echo $this->Html->link(__('New Slideshow'), array('action' => 'add'), array('class' => 'btn   btn-outline btn-sm btn-warning dim', 'title' => 'Add Slideshow')); ?>
        </div>
        <h5><?php echo __('Slideshow'); ?></h5>
      </div>
      <div class="ibox-content">
          <div class="table-responsive mtp">

						<table class="table table-bordered table-striped">
							<thead>
								<tr>
										<th><?php echo $this->Paginator->sort('id'); ?></th>
										<th><?php echo $this->Paginator->sort('image'); ?></th>
										<th><?php echo $this->Paginator->sort('created'); ?></th>
										<th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($slideshows as $slideshow): ?>
									<tr>
										<td><?php echo h($slideshow['Slideshow']['id']); ?>&nbsp;</td>
										<td>
                      <?php if (!empty($slideshow['Slideshow']['image'])) { ?>
                        <?php
                              echo $this->Html->image('slideshow/' . $slideshow['Slideshow']['image'], array('class' => 'img-thumbnail image_size'));
                          } else {
                              echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail image_size'));
                          }
                          ?>&nbsp;
                    </td>
										<td><?php echo h($slideshow['Slideshow']['created']); ?>&nbsp;</td>
										<td class="actions">
											<?php echo $this->Html->link(__('view'), array('action' => 'view', $slideshow['Slideshow']['id']), array('class' => 'btn btn-outline btn-sm btn-warning dim', 'title' => 'View')); ?>
											<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $slideshow['Slideshow']['id']), array('class' => 'btn btn-outline btn-sm btn-info dim', 'title' => 'Edit')); ?>
											<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $slideshow['Slideshow']['id']), array('class' => 'btn btn-outline btn-sm btn-danger dim', 'title' => 'Delete'), __('Are you sure you want to delete # %s?', $slideshow['Slideshow']['id'])); ?>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<div class="row col-md-12">
							<div class="dataTables_paginate paging_bootstrap">
								<p>
									<?php
									echo $this->Paginator->counter(array(
									'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
									));
									?>
								</p>
								<ul class="pagination" style="visibility: visible;">
										<li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
										<li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
										<li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
