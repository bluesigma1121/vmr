<div class="slideshow view">
<h2><?php echo __('Slideshow'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($slideshow['Slideshow']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image'); ?></dt>
		<dd>
			<?php echo h($slideshow['Slideshow']['image']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($slideshow['Slideshow']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($slideshow['Slideshow']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Slideshow'), array('action' => 'edit', $slideshow['Slideshow']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Slideshow'), array('action' => 'delete', $slideshow['Slideshow']['id']), array(), __('Are you sure you want to delete # %s?', $slideshow['Slideshow']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Slideshow'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Slideshow'), array('action' => 'add')); ?> </li>
	</ul>
</div>
