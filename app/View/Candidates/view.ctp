<div class="candidates view">
<h2><?php echo __('Candidate'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Name'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['last_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mobile'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['mobile']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Position'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['position']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Year Experience'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['year_experience']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Current Employer'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['current_employer']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Current Ctc'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['current_ctc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Expected Ctc'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['expected_ctc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notice Period'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['notice_period']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Buyout'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['is_buyout']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('About You'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['about_you']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Resume'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['resume']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($candidate['Candidate']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Candidate'), array('action' => 'edit', $candidate['Candidate']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Candidate'), array('action' => 'delete', $candidate['Candidate']['id']), null, __('Are you sure you want to delete # %s?', $candidate['Candidate']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Candidates'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Candidate'), array('action' => 'add')); ?> </li>
	</ul>
</div>
