<div class="candidates index">
	<h2><?php echo __('Candidates'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('last_name'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('mobile'); ?></th>
			<th><?php echo $this->Paginator->sort('position'); ?></th>
			<th><?php echo $this->Paginator->sort('year_experience'); ?></th>
			<th><?php echo $this->Paginator->sort('current_employer'); ?></th>
			<th><?php echo $this->Paginator->sort('current_ctc'); ?></th>
			<th><?php echo $this->Paginator->sort('expected_ctc'); ?></th>
			<th><?php echo $this->Paginator->sort('notice_period'); ?></th>
			<th><?php echo $this->Paginator->sort('is_buyout'); ?></th>
			<th><?php echo $this->Paginator->sort('about_you'); ?></th>
			<th><?php echo $this->Paginator->sort('resume'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($candidates as $candidate): ?>
	<tr>
		<td><?php echo h($candidate['Candidate']['id']); ?>&nbsp;</td>
		<td><?php echo h($candidate['Candidate']['last_name']); ?>&nbsp;</td>
		<td><?php echo h($candidate['Candidate']['email']); ?>&nbsp;</td>
		<td><?php echo h($candidate['Candidate']['mobile']); ?>&nbsp;</td>
		<td><?php echo h($candidate['Candidate']['position']); ?>&nbsp;</td>
		<td><?php echo h($candidate['Candidate']['year_experience']); ?>&nbsp;</td>
		<td><?php echo h($candidate['Candidate']['current_employer']); ?>&nbsp;</td>
		<td><?php echo h($candidate['Candidate']['current_ctc']); ?>&nbsp;</td>
		<td><?php echo h($candidate['Candidate']['expected_ctc']); ?>&nbsp;</td>
		<td><?php echo h($candidate['Candidate']['notice_period']); ?>&nbsp;</td>
		<td><?php echo h($candidate['Candidate']['is_buyout']); ?>&nbsp;</td>
		<td><?php echo h($candidate['Candidate']['about_you']); ?>&nbsp;</td>
		<td><?php echo h($candidate['Candidate']['resume']); ?>&nbsp;</td>
		<td><?php echo h($candidate['Candidate']['status']); ?>&nbsp;</td>
		<td><?php echo h($candidate['Candidate']['created']); ?>&nbsp;</td>
		<td><?php echo h($candidate['Candidate']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $candidate['Candidate']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $candidate['Candidate']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $candidate['Candidate']['id']), null, __('Are you sure you want to delete # %s?', $candidate['Candidate']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Candidate'), array('action' => 'add')); ?></li>
	</ul>
</div>
