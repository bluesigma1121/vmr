<div class="container content">
    <div class="row margin-bottom-40">
        <!--register-->
        <div class="container content">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-12">
                    <?php echo $this->Form->create('Candidate', array('id' => 'apply_form', 'class' => 'reg-page', 'type' => 'file')); ?>
                    <legend><?php echo __('Apply Online'); ?></legend>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>First Name <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('first_name', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'First Name', 'data-bvalidator' => 'required,email')); ?>
                        </div>

                        <div class="col-sm-6">
                            <label>Last Name <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('last_name', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Last Name', 'data-bvalidator' => 'required,email')); ?>
                        </div>
                    </div>
                    <div class="row">    
                        <div class="col-sm-6">
                            <label>Email Address <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('email', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Email', 'data-bvalidator' => 'required,email')); ?>
                        </div>

                        <div class="col-sm-6">
                            <label>Contact Number <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Contact Number', 'data-bvalidator' => 'required,email')); ?> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Position you wish to apply for <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('position', array('class' => 'form-control margin-bottom-20', 'options' => $positions, 'label' => false, 'data-bvalidator' => 'required,email')); ?> 
                        </div>

                        <div class="col-sm-6">
                            <label>No. of years of relevant experience <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('year_experience', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'No. of years of relevant experience ', 'data-bvalidator' => 'required')); ?> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Current Employer <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('current_employer', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Current Employer', 'data-bvalidator' => 'required')); ?> 
                        </div>

                        <div class="col-sm-6">
                            <label>Current CTC <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('current_ctc', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Current CTC ', 'data-bvalidator' => 'required')); ?> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Expected CTC <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('expected_ctc', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Expected CTC', 'data-bvalidator' => 'required')); ?> 
                        </div>
                        <div class="col-sm-6">
                            <label>Notice Period <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('notice_period', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Email', 'data-bvalidator' => 'required')); ?> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Is Buy out possible <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('is_buyout', array('class' => 'form-control margin-bottom-20', 'label' => false,'options'=>array('No','Yes')  ,'placeholder' => 'Email', 'data-bvalidator' => 'required')); ?> 
                        </div>

                        <div class="col-sm-6">
                            <label>Resume (.doc,.docx, .pdf formats only) <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('resume', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'file', 'data-bvalidator' => 'required')); ?> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Why we should hire you ?  (Answer to be given in min. 500 words) <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('about_you', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'textarea', 'placeholder' => 'Why should we hire you?', 'data-bvalidator' => 'required')); ?> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <?php echo $this->Form->submit('Submit', array('class' => 'btn-u')) ?>
                        </div>
                    </div>

                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>