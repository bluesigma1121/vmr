<div class="candidates form">
<?php echo $this->Form->create('Candidate'); ?>
	<fieldset>
		<legend><?php echo __('Edit Candidate'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('last_name');
		echo $this->Form->input('email');
		echo $this->Form->input('mobile');
		echo $this->Form->input('position');
		echo $this->Form->input('year_experience');
		echo $this->Form->input('current_employer');
		echo $this->Form->input('current_ctc');
		echo $this->Form->input('expected_ctc');
		echo $this->Form->input('notice_period');
		echo $this->Form->input('is_buyout');
		echo $this->Form->input('about_you');
		echo $this->Form->input('resume');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Candidate.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Candidate.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Candidates'), array('action' => 'index')); ?></li>
	</ul>
</div>
