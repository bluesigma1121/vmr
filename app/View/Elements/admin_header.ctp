<?php if (AuthComponent::user('role') == 1) { ?>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#select_role_id').change(function () {
                var id = $('#select_role_id').val();
                var base_path = "<?= Router::url('/', true) ?>";
                if (id != '') {
                    var urls = base_path + "users/role_users/" + id;
                    $.ajax({
                        url: urls,
                        dataType: 'json'
                    }).done(function (data) {
                        if (data.length != 0) {
                            $("#switch_div2").show();
                            $("#user_id").prop('disabled', false);
                            $("#user_id").empty();
                            $("#user_id").append(
                                    $("<option></option>").text('Select').val('')
                                    );
                            $.each(data, function (i, val) {
                                $("#user_id").append(
                                        $("<option></option>").text(val).val(i)
                                        );
                            });
                        } else {
                            $("#switch_div2").hide();
                            $("#user_id").prop('disabled', true);
                        }
                    });
                }
            });
        });
    </script>
<?php } ?>
<script>
    var base_url = "<?= Router::url('/', true); ?>";
</script>

<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <?php if (AuthComponent::user('role') == 1) { ?>
                    <?php echo $this->Form->create('User', array('controller' => 'User', 'action' => 'change_login', 'id' => 'add_user', 'class' => 'form-inline', 'type' => 'file')); ?>
                    <div class="form-group">
                        <?php echo $this->Form->input('role_id', array('class' => 'form-control', 'id' => 'select_role_id', 'type' => 'select', 'options' => $switch_roles, 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                    <div class="form-group" >
                        <?php echo $this->Form->input('id', array('class' => 'form-control', 'type' => 'select', 'options' => $list_operator, 'id' => 'user_id', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                    <?php echo $this->Form->submit(__('Switch'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
                    <?php echo $this->Form->end(); ?>
                    <?php
                } elseif (in_array(AuthComponent::user('role'), array(2, 3))) {
                    if ($this->Session->check('admin_data')) {
                        ?>
                        <a href="<?= Router::url(array('controller' => 'users', 'action' => 'login_as_admin')) ?>">
                            <i class="fa fa-sign-in"></i> Switch as Admin
                        </a>
                        <?php
                    }
                }
                ?>
            </li>
            <li>
                <span class="m-r-sm text-muted welcome-message">Welcome</span><?php echo ucfirst(AuthComponent::user('first_name')); ?>
            </li>
            <li>
                <a href="<?= Router::url(array('controller' => 'users', 'action' => 'logout')) ?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>