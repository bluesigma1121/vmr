 <!-- FOOTER SECTION START -->
<footer>
  <section class="index-link">
    <div class="container">
      <div class="row">
        <div class="footerboxriben">
          <ul>
            <li>Follow Us</li>
            <li><a href="https://www.facebook.com/valuemarketresearch/" class="facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="https://twitter.com/vmr_reports" class="twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>           
            <li><a href="https://www.linkedin.com/company/value-market-research/" class="linkedin" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="link-area" style="min-height: 301px;">
            <h3>Useful Links</h3>
            <ul>
              <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'aboutus')); ?>"> About Us</a></li>
              <li><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'reportlist')); ?>"> Reports</a></li>
              <li><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'upcomingreport')); ?>"> Upcoming Reports</a></li>
              <!--<li><a href="<?php echo Router::url(array('controller' => 'blogs', 'action' => 'blog_listing')); ?>"> Blog</a></li>-->
              <li><a href="<?php echo Router::url(array('controller' => 'testimonials', 'action' => 'listing' )); ?>"> Testimonials</a></li>
              <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'contactus'))?>"> Contact Us</a></li>
              <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'howtoorder'))?>"> How to Order</a></li>
              <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'researchmethodology'))?>"> Research Methodology</a></li>
              <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'careers'))?>"> Career</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="link-area" style="min-height: 301px;">
            <h3>Find Assistance</h3>
            <ul>
              <li><a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'article_listing')); ?>"> Press Release</a></li>
              <li><a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'analysis_listing')); ?>"> Analysis</a></li>
              <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'privacy_policy')); ?>"> Privacy Policy</a></li>
              <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'refundpolicy')); ?>"> Refund Policy</a></li>
              <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'terms_conditions')); ?>"> Terms & Conditions</a></li>
              <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'faq')); ?>"> FAQ</a></li>
              <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'disclaimers')); ?>"> Disclaimer</a></li>
              <li><a href="<?= Router::url(array('controller' => 'sitemap', 'ext' => 'xml')); ?>" target="_blank"> Sitemap</a></li>
              
            </ul>
          </div>
        </div>
        <div class="col-md-3 address col-sm-3">
          <div class="link-area" style="min-height: 301px;">
              <h3>Contact</h3>
              <p>401/402, TFM, Nagras Road, <br>Aundh, Pune-7.<br> Maharashtra, INDIA<br>
                  <a href="mailto:sales@valuemarketresearch.com" style="color:#fff;">sales@valuemarketresearch.com</a><br>
                  +1-888-294-1147
              </p>
          </div>
        </div>
        <div class="col-md-3 address col-sm-3">
            <div class="link-area" style="min-height: 301px;">
                <h3>Business Hours</h3>
                <p >Monday to Friday : </br>9 A.M IST to 6 P.M IST<br>Saturday-Sunday : Closed</br>Email Support : 24 x 7</p>
                <ul class="pay-option">
                    <li><img src="<?= Router::url('/', true); ?>img/payment/paypal.png" width="52" height="32" alt="paypal"></li>
                    <li><img src="<?= Router::url('/', true); ?>img/payment/visa.png" width="52" height="32" alt="visa"></li>
                    <li><img src="<?= Router::url('/', true); ?>img/payment/mastercard.png" width="52" height="32" alt="mastercard"></li>
                    <li><img src="<?= Router::url('/', true); ?>img/payment/american-express.png" width="52" height="32" alt="american-express"></li>
                </ul>
            </div>
        </div>
      </div>
    </div>
</section>

<section class="index-social">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 index-social-link text-center">
        <p class="copy-c">&copy; <?php echo date('Y') ?>, All Rights Reserved, Value Market Research <!-- Design &amp; Developed By <a href="#">Conversant Technologies</a> --></p>
      </div>
    </div>
  </div>
</section>
</footer>
<!-- FOOTER SECTION END -->
  <?php
  echo $this->Html->script(array(
    // 'front/jquery.min.js',
    'front/jquery.easing.min.js',
    'front/bootstrap.min.js',
    'front/jquery.bvalidator-yc.js',
    'front/custom.js',
  ), ['defer' => 'defer']);
  ?>

<script>
  function acceptCondition(){$("#viewTermsAndCodition").modal("hide")}function notAcceptCondition(){$("#viewTermsAndCodition").modal("hide"),$("#EnquiryTerm").prop("checked",!1)}function openModal(n){$("#EnquiryTerm").is(":checked")&&$.ajax({url:$("#termsandcondition").val()}).done(function(n){$("#viewTermsAndCodition").on("shown.bs.modal",function(){$(this).find(".modal-body").html(n)}).modal("show")})}
</script>

<!-- Google Captcha Code Start here VG-15/2/2015 -->
<?php if(($this->request->params['controller'] == "enquiries" && $this->request->params['action'] == "get_lead_info_form") || ($this->request->params['controller'] == "users" && $this->request->params['action'] == "contactus")){ ?>
  <script defer src='https://www.google.com/recaptcha/api.js'></script>
  <script>
      $("#contact_us").submit(function(e){""===$("#g-recaptcha-response").val()&&(e.preventDefault(),alert("Please check the recaptcha"))});
      $("#enquiry_form").submit(function(e){""===$("#g-recaptcha-response").val()&&(e.preventDefault(),alert("Please check the recaptcha"))});
  </script>
<?php } ?>
<!-- Google Captcha Code end here VG-15/2/2015 -->

<script type="text/javascript">
  $(document).ready(function(){$(".navbar .dropdown").hover(function(){$(this).find(".dropdown-menu").first().stop(!0,!0).slideDown(150)},function(){$(this).find(".dropdown-menu").first().stop(!0,!0).slideUp(105)})});
   $(".carousel-showmanymoveone .item").each(function(){for(var e=$(this),i=1;i<6;i++)(e=e.next()).length||(e=$(this).siblings(":first")),e.children(":first-child").clone().addClass("cloneditem-"+i).appendTo($(this))});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.carousel-showmanymoveone').carousel({
      interval: 500
    })
  });
</script>
<!-- <script type="text/javascript">
  $(document).ready(function(){$(".navbar .dropdown").hover(function(){$(this).find(".dropdown-menu").first().stop(!0,!0).slideDown(150)},function(){$(this).find(".dropdown-menu").first().stop(!0,!0).slideUp(105)})});
   $(".carousel-showmanymoveone .item").each(function(){for(var e=$(this),i=1;i<6;i++)(e=e.next()).length||(e=$(this).siblings(":first")),e.children(":first-child").clone().addClass("cloneditem-"+i).appendTo($(this))});
</script> -->

<!-- New autocomplete search code Start VG-25/11/2016  -->
<script type="text/javascript">
    $(document).ready(function(){var e=null;$("#search_text").on("keyup",function(t){" "!=(e=$(this).val())||e.length>0?38!=t.keyCode&&40!=t.keyCode&&$.ajax({url:"<?php echo Router::url(['controller' => 'categories', 'action' => 'home_search_autofill']);?>",method:"POST",async:!0,data:{query:e},success:function(e){$("#suggest").fadeIn(),$("#suggest").html(e)}}):$("#suggest").fadeOut()}),$(document).on("click","#result",function(){$("#suggest").fadeOut()}),$(document).on("mouseenter","#result",function(){" "!=(e=$("#search_text").val())||e.length>0?($(this).prev().removeClass("selected"),$(this).addClass("selected")):$("#suggest").fadeOut(5e3)}),$(document).on("mouseleave","#result",function(){$(this).removeClass("selected")})});
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5ac844094b401e45400e6c9d/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<!-- <script type='text/javascript' async src='//platform-api.sharethis.com/js/sharethis.js#property=5ac83993003b52001341b5f1&product=inline-share-buttons' async='async'></script> -->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" defer="defer">

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script> -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> -->