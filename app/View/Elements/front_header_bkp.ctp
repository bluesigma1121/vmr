<!--<script type="text/javascript">
    //var filter = "<? // $filters['stream'];                                                             ?>";
    $(document).ready(function() {
        $('#curr').change(function() {
            $('#change_currency').submit();
        });
    });
</script> -->

<script>
    function goBack() {
        window.history.back()
    }
</script>


<div class="wrapper">
<!-- NAVIGATION SECTION START -->
<nav class="navbar navbar-default navbar-fixed-top">
  <!-- HEADER TOP SECTION START -->
<div class="header-top">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-xs-7 text-left"><h6><i class="fa fa-envelope fa-lg" aria-hidden="true"></i> sales@valuemarketresearch.com</h6></div>
      <div class="col-md-6 col-xs-5 text-right"><h6><i class="fa fa-phone-square fa-lg" aria-hidden="true"></i>
     +1 888-452-1505</h6></div>
   </div>
  </div>
</div>

<!-- HEADER TOP SECTION END -->
  <div class="container">
    <div class="navbar-header col-xs-4 hidden-lg hidden-md">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
     <div class="col-md-2 col-xs-8 ">
        <a class="navbar-brand" href="<?= Router::url('/', true); ?>">
          <img src="<?= Router::url('/', true); ?>/img/logos/vmr-logo.png" alt="VMR Logo">
        </a>
     </div>

      <div class="col-md-10 col-xs-12">
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li class="<?= $this->request->params['action'] == 'home'?'active':''?>" ><a href="<?= Router::url(array('controller' => 'categories', 'action' => 'home'))?>"><i class="fa fa-home fa-lg" aria-hidden="true"></i></a></li>
          <li class="<?= $this->request->params['action'] == 'aboutus'?'active':''?>" ><a href="<?= Router::url(array('controller' => 'users', 'action' => 'aboutus')); ?>">About Us</a></li>
          <li class="dropdown <?= $this->request->params['action'] == 'category_listing'?'active':''?>">
              <a href="#." class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Industries <span class="caret"></span></a>
              <ul class="dropdown-menu">

                <?php foreach ($research_ind['Level2'] as $key => $cat): ?>
                    <li>
                        
                            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing',  'slug' => $this->Link->cleanString($cat['category_name']))); ?>" ><?=$cat['category_name']?></a>
                        
                    </li>
                <?php endforeach; ?>
              </ul>
            </li>
      <li class="<?= $this->request->params['action'] == 'reportlist'?'active':''?>" ><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'reportlist')); ?>">Reports</a></li>
      <!--<li class="<?= $this->request->params['action'] == 'blog_listing'?'active':''?>" ><a href="<?php echo Router::url(array('controller' => 'blogs', 'action' => 'blog_listing')); ?>">Blogs</a></li>-->
      <li class="<?= $this->request->params['action'] == 'contactus'?'active':''?>" ><a href="<?= Router::url(array('controller' => 'users', 'action' => 'contactus'))?>">Contact Us</a></li>
      <!-- <li><a href="#" data-target="#login" data-toggle="modal">Sign in</a></li> -->
    </ul>
    </div>
  </div>
    <!--<div class="col-md-3 col-xs-12 padd-right">-->
              <!-- <input type="text" class="form-control">
              <span class="input-group-btn">
              <button class="btn btn-primary" type="button">
              <i class="fa fa-search" aria-hidden="true"></i>
             </button>
             </span> -->

      <!-- <div class="custom-search-form">
         <?php echo $this->Form->create('Category', array('type' => 'get','action'=>'home_search_form')); ?>
             <?php echo $this->Form->input('q', array('class' => 'form-control', 'label' =>false,'required','autocomplete'=>'off','id'=>'search_text')); ?>
             <div class="col-md-11 pull-left suggest-div" id="suggest"></div>
             <div class="col-md-1 col-sm-1 input-group-btn pull-right search-btn">
               <button class="btn btn-primary" type="submit">
                 <i class="fa fa-search" aria-hidden="true"></i>
              </button>
             </div>
         <?php  echo $this->Form->end(); ?>
      </div>-->

    <!--</div>-->
  </div>
</nav>
<!-- NAVIGATION SECTION END -->

<!-- Inner Page -->
<div class="inner-main"></div>
