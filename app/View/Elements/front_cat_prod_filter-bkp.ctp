<script type="text/javascript">
    $(document).ready(function () {
        $('#clearAll').click(function (event) {  //on click
            $('.check_uncheck').each(function () { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"
            });
        });

        $('.check_uncheck').click(function () {
            $('#filter_form').submit();
        });
    });
</script>
<style>
    .filter-by-block .panel-group span a {
        color: #FFF;
        display: block;
        font-size: 14px;
        border-bottom: 1px solid #dedede;
        background-color: #005994;
        padding: 5px;
        text-decoration: none;
    }
</style>
<!--/navbar-collapse-->
<div class="col-md-3 col-sm-4 filter-by-block md-margin-bottom-80">
    <div>
        <!--<button type="button" class="navbar-toggle cust_filter" title="Hide Filter" data-toggle="collapse" data-target=".navbar-responsive-collapse_hide">-->
        <!--    <span class="fa fa-bars"></span>-->
        <!--</button>-->
        <div class="navbar-responsive-collapse_hide">
            <?php echo $this->Form->create('Product', array('class' => 'form-horizontal', 'id' => 'filter_form', 'role' => 'form')); ?>
            <?php if (isset($cat_data['Level2'][0])) {?>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne_cat">
                                    Categories
                                    
                                </a>
                            </span>
                        </div>
                        <div id="collapseOne_cat" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <ul class="list-unstyled checkbox-list">
                                    <div class="accordion-inner">
                                        <?php
                                        foreach ($cat_data['Level2'] as $key1 => $cat):?>
                                          <ul class="list-unstyled cate-list">
                                            <li>
                                                 <a class="text-color" href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat['category_name']))); ?> ">    
                                                    <?php echo $cat['category_name'];?>
                                                </a> 
                                            </li>
                                            <?php
                                            if (!empty($cat['Level3'])) {
                                                ?>
                                                <ul class="list-unstyled" >
                                                    <?php foreach ($cat['Level3'] as $key2 => $cat2): ?>
                                                        <li>
                                                             <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat2['category_name']))); ?> "><?php echo $cat2['category_name'] ?> (<?php echo $cat2['no_of_product'] ?>)</a> 
                                                            <?php if (!empty($cat2['Level4'])) { ?>
                                                                <ul>
                                                                    <?php foreach ($cat2['Level4'] as $key3 => $cat3): ?>
                                                                        <li> 
                                                                             <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat3['category_name']))); ?>"><?php echo $cat3['category_name'] ?> (<?php echo $cat3['no_of_product'] ?>)</a> 
                                                                            <?php if (!empty($cat3['Level5'])) { ?>
                                                                                <ul>
                                                                                    <?php foreach ($cat3['Level5'] as $key4 => $cat4): ?>
                                                                                        <li class="list-unstyled">
                                                                                             <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat4['category_name']))); ?>"><?php echo $cat4['category_name'] ?> (<?php echo $cat4['no_of_product'] ?>)</a> 
                                                                                            <?php if (!empty($cat4['Level6'])) { ?>
                                                                                                <ul>
                                                                                                    <?php foreach ($cat4['Level6'] as $key5 => $cat5): ?>
                                                                                                        <li class="list-unstyled">

                                                                                                             <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat5['category_name']))); ?>"><?php echo $cat5['category_name'] ?> (<?php echo $cat5['no_of_product'] ?>)</a> 
                                                                                                        </li>
                                                                                                    <?php endforeach; ?>
                                                                                                </ul>
                                                                                            <?php } ?>
                                                                                        </li>
                                                                                    <?php endforeach; ?>
                                                                                </ul>
                                                                            <?php } ?>
                                                                        </li>
                                                                    <?php endforeach; ?>
                                                                </ul>
                                                            <?php } ?>
                                                        <?php endforeach; ?>
                                                    </li>
                                                </ul>
                                            <?php } ?>
                                          </ul>
                                        <?php endforeach; ?>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php echo $this->Form->end(); ?>
            <!--/end panel group-->
        </div>
    </div>
</div>
