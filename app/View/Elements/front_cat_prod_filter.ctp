<script type="text/javascript">
    $(document).ready(function () {
        $('#clearAll').click(function (event) {  //on click
            $('.check_uncheck').each(function () { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"
            });
        });

        $('.check_uncheck').click(function () {
            $('#filter_form').submit();
        });
    });
</script>

<!--/navbar-collapse-->
<div class="col-md-3 col-sm-4 filter-by-block md-margin-bottom-80">
    <div>
        <!--<button type="button" class="navbar-toggle cust_filter" title="Hide Filter" data-toggle="collapse" data-target=".navbar-responsive-collapse_hide">-->
        <!--    <span class="fa fa-bars"></span>-->
        <!--</button>-->
        <div class="navbar-responsive-collapse_hide">
            <?php echo $this->Form->create('Product', array('class' => 'form-horizontal', 'id' => 'filter_form', 'role' => 'form')); ?>
            <?php if (isset($cat_data['Level2'][0])) {?>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne_cat">
                                    Categories                                    
                                </a>
                            </span>
                        </div>
                        <div class="panel-group acc-v1 panel-collapse collapse in"  id="collapseOne_cat">
                            <?php foreach ($cat_data['Level2'] as $key1 => $cat): ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">                                        
                                            <?php if ($cat['id'] == $id) { ?>
                                                <a class="accordion-toggle" data-toggle="collapse" aria-expanded="true"  data-parent="#accordion-1" href="#collapse-One<?php echo $cat['id'] ?>">
                                                    <?php echo $cat['category_name'] ?>(<?php echo $cat['no_of_product'] ?>)<br>
                                                </a>
                                            <?php } else { ?>
                                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-One<?php echo $cat['id'] ?>">
                                                    <?php echo $cat['category_name'] ?> (<?php echo $cat['no_of_product']; ?>)
                                                </a>
                                            <?php } ?>
                                        </h4>
                                    </div>

                                    <?php if ($cat['id'] == $id) { ?>
                                        <div id="collapse-One<?php echo $cat['id'] ?>" class="panel-collapse collapse in" style="height: auto;">
                                    <?php } else { ?>
                                        <div id="collapse-One<?php echo $cat['id'] ?>" class="panel-collapse collapse" style="height: auto;">
                                    <?php } ?>

                                    <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                        <ul class="list-unstyled">
                                            <li>
                                            <i class="fa fa-angle-double-right color-green"></i>
                                            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat['category_name']))); ?> "><?php echo $cat['category_name'] ?> (<?php echo $cat['no_of_product']; ?>)</a>

                                            </li>
                                            <?php if (!empty($cat['Level3'])) { ?>
                                            <ul>
                                                <?php foreach ($cat['Level3'] as $key2 => $cat2): ?>
                                                <li>
                                                <i class="fa fa-angle-double-right color-green"></i>
                                
                                                <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat2['category_name']))); ?> "><?php echo $cat2['category_name'] ?> (<?php echo $cat2['no_of_product']; ?>)</a>
                                                <?php if (!empty($cat2['Level4'])) { ?>
                                                <ul>
                                                    <?php foreach ($cat2['Level4'] as $key3 => $cat3): ?>
                                                    <li><i class="fa fa-angle-double-right color-green"></i>   
                                                        <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat3['category_name']))); ?>"><?php echo $cat3['category_name'] ?> (<?php echo $cat3['no_of_product']; ?>)</a>
                                                    <?php if (!empty($cat3['Level5'])) { ?>
                                                        <ul>
                                                        <?php foreach ($cat3['Level5'] as $key4 => $cat4): ?>
                                                            <li class="list-unstyled">
                                                            <i class="fa fa-angle-double-right color-green"></i>
                                                            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat4['category_name']))); ?>"><?php echo $cat4['category_name'] ?>(<?php echo $cat4['no_of_product']; ?>)</a>    
                                                            <?php if (!empty($cat4['Level6'])) { ?>
                                                                <ul>
                                                                <?php foreach ($cat4['Level6'] as $key5 => $cat5): ?>
                                                                    <li class="list-unstyled">
                                                                    <i class="fa fa-angle-double-right color-green"></i>
                                                                    <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat5['category_name']))); ?>"><?php echo $cat5['category_name'] ?>(<?php echo $cat5['no_of_product']; ?>)</a>
                                                                    </li>
                                                                <?php endforeach; ?>
                                                                </ul>
                                                            <?php } ?>
                                                            </li>
                                                        <?php endforeach; ?>
                                                        </ul>
                                                    <?php } ?>
                                                    </li>
                                                <?php endforeach; ?>
                                                </ul>
                                            <?php } ?>
                                            <?php endforeach; ?>
                                        </li>
                                        </ul>
                                    <?php } ?>
                                    </ul>
                                </div>
                                </div>
                            </div>
                            </div>
                            <hr>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    </div>
                </div>
            <?php } ?>

            <?php echo $this->Form->end(); ?>
            <!--/end panel group-->
        </div>
    </div>
</div>
