<style>
    .breadcrumbs {
        overflow: hidden;
        border-bottom: solid 1px #eee;
        /* background: url(../../img/breadcrumbs.png) repeat; */
    }
    .mar_top_bot{
        margin-top: -31px;
        margin-bottom: -10px;
    }
    .breadcrumb>li+li:before {
        padding: 0 5px;
        color: #ccc;
        content: "/\00a0" !important;
    }
</style>

            <li>
                <a href="<?= Router::url(array('controller' => 'categories', 'action' => 'home')); ?>"> Home </a>
            </li>
            <?php
            if (!empty($bredcum_array)) {
                foreach ($bredcum_array as $key => $bred) {
                    ?>
                    <?php if ($bred['Category']['level'] == 1) { ?>
                        <li>
                            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing',  'slug' => $this->Link->cleanString($bred['Category']['category_name']))); ?>"><?php echo $bred['Category']['category_name']; ?></a> </li>
                    <?php } else { ?>
                        <li> 

                            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($bred['Category']['category_name']))); ?> "><?php echo $bred['Category']['category_name']; ?></a>
                        </li>
                        <?php
                    }
                }
            }
            ?>

        </ul>
        <?php if (!empty($cat['Level2'])) { ?>
            <ul class="list-unstyled">
                <?php foreach ($cat['Level2'] as $key2 => $cat2): ?>
                    <li><i class="fa fa-angle-double-right color-green"></i>
                        <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat2['category_name']))); ?> "><?php echo $cat2['category_name'] ?></a>
                        <?php if (!empty($cat2['Level3'])) { ?>
                            <ul>
                                <?php foreach ($cat2['Level3'] as $key3 => $cat3): ?>
                                    <li><i class="fa fa-angle-double-right color-green"></i>   <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat3['category_name']))); ?>"><?php echo $cat3['category_name'] ?></a>
                                        <?php if (!empty($cat3['Level4'])) { ?>
                                            <ul>
                                                <?php foreach ($cat3['Level4'] as $key4 => $cat4): ?>
                                                    <li class="list-unstyled">
                                                        <i class="fa fa-angle-double-right color-green"></i>
                                                        <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat4['category_name']))); ?>"><?php echo $cat4['category_name'] ?></a>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php } ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php } ?>
                    <?php endforeach; ?>
                </li>
            </ul>
        <?php } ?>
