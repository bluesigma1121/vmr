<link rel="shortcut icon" href="ftest.ico">
<?php
echo $this->Html->meta('favicon.ico', '/img/favicon.png', array(
    'type' => 'icon'
));

echo $this->Html->css(array(
    'back/bootstrap.min.css',
    'back/jquery-ui-1.10.4.custom.css',
    'back/font-awesome/css/font-awesome.css',
    'back/plugins/morris/morris-0.4.3.min.css',
    'back/jquery.gritter.css',
    'back/animate.css', 'back/custom_g.css',
    'back/jquery.fancybox.css',
    'back/jquery.multiselect.css',
    'back/jquery.multiselect.filter.css',
    'back/bvalidator.css',
    'back/style.css',
    'back/jquery-ui-1.10.4.custom.css',
    'back/jqtree.css',
    'back/themes/default/easyui.css',
    'back/themes/icon.css',
    'back/custom_g.css',
    'back/plugins/steps/jquery.steps.css',
   'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css',
));
?> 
<?php
echo $this->Html->script(array(
    'back/jquery-2.1.1.js',
    'back/bootstrap.min.js',
    'back/jquery-ui-1.10.4.custom.js',
    'back/plugins/metisMenu/jquery.metisMenu.js',
    'back/plugins/slimscroll/jquery.slimscroll.min.js',
    'back/inspinia.js',
    'back/plugins/flot/jquery.flot.js',
    'back/plugins/flot/jquery.flot.tooltip.min.js',
    'back/jquery.bvalidator-yc.js',
    'back/jquery.multiselect.js',
    'back/jquery.multiselect.filter.js',
    'back/jquery-ui-1.10.4.custom.js',
    'back/jquery.fancybox.js',
    'back/tree.jquery.js',
    'back/jquery.cookie.js',
    'back/jquery.easyui.min.js',
    'back/moment.min.js',
    'back/combodate.js',
    'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js',
    //'back/pace.min.js'
));
?> 
<script>
    function goBack() {
        window.history.back()
    }
</script> 
<script>
    $(document).ready(function () {
        $(".btn").tooltip();
    });
    $(document).ready(function() {
        $('.select2').select2();
    });
</script> 
<style>
.select2-container--default .select2-selection--single {
    background-color: #FFFFFF;
    background-image: none;
    border: 1px solid #e5e6e7;
    border-radius: 1px;
    color: inherit;
    display: block;
    padding: 6px 12px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    width: 100%;
    height: 34px;
    font-size: 14px;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 20px;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 32px; 
}
.select2-results__option {
    font-size: 14px;
}
.select2-search--dropdown .select2-search__field {
    padding: 8px;
    font-size:14px;
}
    .pace {
        -webkit-pointer-events: none;
        pointer-events: none;

        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;

        -webkit-perspective: 12rem;
        -moz-perspective: 12rem;
        -ms-perspective: 12rem;
        -o-perspective: 12rem;
        perspective: 12rem;

        z-index: 2000;
        position: fixed;
        height: 6rem;
        width: 6rem;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    .pace.pace-inactive .pace-progress {
        display: none;
    }

    .pace .pace-progress {
        position: fixed;
        z-index: 2000;
        display: block;
        position: absolute;
        left: 0;
        top: 0;
        height: 6rem;
        width: 6rem !important;
        line-height: 6rem;
        font-size: 2rem;
        border-radius: 50%;
        background: rgba(34, 153, 221, 0.8);
        color: #fff;
        font-family: "Helvetica Neue", sans-serif;
        font-weight: 100;
        text-align: center;

        -webkit-animation: pace-theme-center-circle-spin linear infinite 2s;
        -moz-animation: pace-theme-center-circle-spin linear infinite 2s;
        -ms-animation: pace-theme-center-circle-spin linear infinite 2s;
        -o-animation: pace-theme-center-circle-spin linear infinite 2s;
        animation: pace-theme-center-circle-spin linear infinite 2s;

        -webkit-transform-style: preserve-3d;
        -moz-transform-style: preserve-3d;
        -ms-transform-style: preserve-3d;
        -o-transform-style: preserve-3d;
        transform-style: preserve-3d;
    }

    .pace .pace-progress:after {
        content: attr(data-progress-text);
        display: block;
    }

    @-webkit-keyframes pace-theme-center-circle-spin {
        from { -webkit-transform: rotateY(0deg) }
        to { -webkit-transform: rotateY(360deg) }
    }

    @-moz-keyframes pace-theme-center-circle-spin {
        from { -moz-transform: rotateY(0deg) }
        to { -moz-transform: rotateY(360deg) }
    }

    @-ms-keyframes pace-theme-center-circle-spin {
        from { -ms-transform: rotateY(0deg) }
        to { -ms-transform: rotateY(360deg) }
    }

    @-o-keyframes pace-theme-center-circle-spin {
        from { -o-transform: rotateY(0deg) }
        to { -o-transform: rotateY(360deg) }
    }

    @keyframes pace-theme-center-circle-spin {
        from { transform: rotateY(0deg) }
        to { transform: rotateY(360deg) }
    }
</style>