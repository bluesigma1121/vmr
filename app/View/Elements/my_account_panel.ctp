<style>
    .deactive{ 
        margin-left: 3px;
        margin-right:  3px;
        color: black;
        border: none;
        background: #ccc;
    }
</style>
<?php
$account = '';
$details = '';
$enquiry = '';
$order = '';
$logout = '';
$controller = $this->request->controller;
$action = $this->request->action;
if (($controller == 'users') && ($action == 'my_account')) {
    $account = 'active';
    $details = 'deactive';
    $enquiry = 'deactive';
    $order = 'deactive';
    $logout = 'deactive';
    $testi = 'deactive';
} elseif (($controller == 'orders') && ($action == 'user_order_index' || $action == 'user_order_view' )) {
    $account = 'deactive';
    $details = 'deactive';
    $enquiry = 'deactive';
    $order = 'active';
    $logout = 'deactive';
    $testi = 'deactive';
} elseif (($controller == 'users') && ($action == 'details')) {
    $account = 'deactive';
    $details = 'active';
    $enquiry = 'deactive';
    $order = 'deactive';
    $logout = 'deactive';
    $testi = 'deactive';
} elseif (($controller == 'enquiries') && ($action == 'user_enquiry_index')) {
    $account = 'deactive';
    $details = 'deactive';
    $enquiry = 'active';
    $order = 'deactive';
    $logout = 'deactive';
    $testi = 'deactive';
} elseif (($controller == 'testimonials') && ($action == 'user_testi_index')) {
    $account = 'deactive';
    $details = 'deactive';
    $enquiry = 'deactive';
    $order = 'deactive';
    $logout = 'deactive';
    $testi = 'active';
} else {
    $account = '';
    $details = '';
    $enquiry = '';
    $order = '';
    $logout = '';
    $testi = '';
}
?>
<?php $user_id = AuthComponent::user('id'); ?>
<ul class="nav nav-tabs">
    <?php if (in_array(AuthComponent::user('role'), array(11))) { ?>
        <li data-target="#step2" class="<?php echo $account; ?>">
            <a href="<?= Router::url(array('controller' => 'users', 'action' => 'my_account',$user_id)); ?>">Account</a>
        </li>
    <?php } ?>

    <?php /* if (in_array(AuthComponent::user('role'), array(11))) { ?>
      <li data-target="#step2" class="<?= $details; ?>">
      <a href="<?= Router::url(array('controller' => 'users', 'action' => 'details',$user_id)); ?>">Details</a>
      </li>
      <?php } */ ?>
    <?php if (in_array(AuthComponent::user('role'), array(11))) { ?>
        <li data-target="#step2" class="<?= $enquiry; ?>">
            <a href="<?= Router::url(array('controller' => 'enquiries', 'action' => 'user_enquiry_index',$user_id)); ?>">Inquiries</a>
        </li>
    <?php } ?>
    <?php /* if (in_array(AuthComponent::user('role'), array(11))) { ?>
      <li data-target="#step2" class="<?= $testi; ?>">
      <a href="<?= Router::url(array('controller' => 'testimonials', 'action' => 'user_testi_index',$user_id)); ?>">Testimonials</a>
      </li>
      <?php } */ ?>
    <?php if (in_array(AuthComponent::user('role'), array(11))) { ?>
        <li data-target="#step2" class="<?= $order; ?>">
            <a href="<?= Router::url(array('controller' => 'orders', 'action' => 'user_order_index',$user_id)); ?>">Orders</a>
        </li>
    <?php } ?>
</ul>


