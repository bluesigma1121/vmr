<style>
    .search-block {
        padding: 10px 0px;
        text-align: center;
        position: relative;
        margin-bottom: 20px;
        /* //background: url(../img/front/bg/7.jpg) 50% 0 repeat fixed; */
    }
</style>



<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main bg-color-inner-report">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>Industries</h3>-->
        <!--</div>-->
        <div class="col-md-10 col-md-push-1 col-xs-12 col-lg-10 col-lg-push-1">
            <div class="custom-search-form">
           <?php echo $this->Form->create('Category', array('type' => 'get','action'=>'home_search_form')); ?>
                  <?php  
                  $arr= explode("?",$_SERVER['REQUEST_URI']); //print_r($arr);//echo $this->base.'/search-results';
                  if($arr[0] != $this->base.'/search-results')
                      { 
                          echo $this->Form->input('q', array('class' => 'form-control', 'label' =>false,'required','autocomplete'=>'off','id'=>'search_text'));
                        //   echo $this->Form->input('q', array('class' => 'form-control text_box_height_list', 'id'=>'search_text', 'label' => false,'required','autocomplete'=>'off'));
                      }
                  else
                      {
                          echo $this->Form->input('q', array('class' => 'form-control', 'label' =>false,'required','autocomplete'=>'off','id'=>'search_text','value'=>$_GET['q']));
                        //   echo $this->Form->input('q', array('class' => 'form-control text_box_height_list', 'id'=>'search_text', 'label' => false,'required','autocomplete'=>'off','value'=>$_GET['q']));     
                      }

                   ?>
                   <div class="col-md-11 pull-left suggest-div" id="suggest"></div>
                    <div class="col-md-1 col-sm-1 input-group-btn pull-right search-btn">
                      <button class="btn btn-primary" type="submit">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                    </div>
                    <!-- <div class="col-md-1"  style=" margin-top: -49px;float: right;display: block;">-->
                    <!--        <?php echo $this->Form->submit('Search',array('class' => 'btn btn-default ','style'=>' height: 38px; width: 120%;border-radius: 0px;font-weight: 500;background: #666666; color: #ffffff;')); ?>-->
                    <!-- </div>-->
                    <!--<div class="col-md-12 pull-left" id="suggest" style=" margin-top: -9px; clear:both; margin-left: -14px;overflow-x: hidden;position: absolute; z-index: 2;"></div>-->

                      <?php  echo $this->Form->end(); ?>
           </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->
<!--
<div class="search-block">
    <div class="container">
        <div class="col-md-12">
           
            
              <?php echo $this->Form->create('Category', array('type' => 'get','action'=>'home_search_form')); ?>
                  <?php  
                  $arr= explode("?",$_SERVER['REQUEST_URI']); //print_r($arr);//echo $this->base.'/search-results';
                  if($arr[0] != $this->base.'/search-results')
                      { 
                          echo $this->Form->input('q', array('class' => 'form-control text_box_height_list', 'id'=>'search_text', 'label' => false,'required','autocomplete'=>'off'));
                      }
                  else
                      {
                           echo $this->Form->input('q', array('class' => 'form-control text_box_height_list', 'id'=>'search_text', 'label' => false,'required','autocomplete'=>'off','value'=>$_GET['q']));     
                      }

                   ?>
                     <div class="col-md-1"  style=" margin-top: -49px;float: right;display: block;">
                            <?php echo $this->Form->submit('Search',array('class' => 'btn btn-default ','style'=>' height: 38px; width: 120%;border-radius: 0px;font-weight: 500;background: #666666; color: #ffffff;')); ?>
                     </div>
                    <div class="col-md-12 pull-left" id="suggest" style=" margin-top: -9px; clear:both; margin-left: -14px;overflow-x: hidden;position: absolute; z-index: 2;"></div>

                      <?php  echo $this->Form->end(); ?>
                
            
        </div>
    </div>
</div>-->