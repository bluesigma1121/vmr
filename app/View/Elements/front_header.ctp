


<script>
    function goBack() {
        window.history.back()
    }
</script>

<?php 
// Menu-Research Industries (Category and subCategory) code start here
App::import('Model', 'Category');
$this->Category = new Category();

$catMenu = $this->Category->find('all', array(
                'fields' => array('Category.category_name', 'Category.id'),
                'conditions' => array('Category.parent_category_id' => 2,'Category.is_active' => 1), 
                'recursive' => -1,
                'cache' => 'categoryMenuList', 
                'cacheConfig' => 'long'
              ));
// Menu-Research Industries (Category and subCategory) code end here
?>

<!-- <div class="wrapper"> -->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="header-top">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-xs-7 text-left"><h6><i class="fa fa-envelope fa-lg" aria-hidden="true"></i> <a style="color:#fff" href="mailto:sales@valuemarketresearch.com">sales@valuemarketresearch.com</a></h6></div>
          <div class="col-md-6 col-xs-5 text-right"><h6><i class="fa fa-phone-square fa-lg" aria-hidden="true"></i>
          +1-888-294-1147</h6></div>
      </div>
      </div>
    </div>

    <div class="container">
      <div class="navbar-header col-xs-4 hidden-lg hidden-md hidden-sm" style="min-height:0px !important;">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="col-md-2 col-xs-8 col-sm-3">
          <a class="navbar-brand" href="<?= Router::url('/', true); ?>">
            <img src="<?= Router::url('/', true); ?>img/logos/vmr-logo.png" width="176" height="65" alt="Value Market Research">
          </a>
      </div>
      <div class="col-md-8 col-xs-12 col-sm-9">
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right">
             <li class="<?= $this->request->params['action'] == 'home'?'active':''?>" ><a href="<?= Router::url(array('controller' => 'categories', 'action' => 'home'))?>"><i class="fa fa-home fa-lg" aria-hidden="true"></i></a></li>
            <li class="dropdown <?= $this->request->params['action'] == 'category_listing'?'active':''?>">
              <a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'category_list')); ?>" >Industries <span class="caret"></span></a>
             
             
              <ul class="dropdown-menu">

                <?php foreach ($catMenu as $key => $cat){ ?>
                    <li>
                        
                      <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing',  'slug' => $this->Link->cleanString($cat['Category']['category_name']))); ?>" ><?=$cat['Category']['category_name']?></a>
                        
                    </li>
                <?php } ?>
              </ul>
            </li>
          <li class="<?= $this->request->params['action'] == 'reportlist'?'active':''?>" ><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'reportlist')); ?>">Reports</a></li>
          <!-- <li class="<?= $this->request->params['action'] == 'infographicsview'?'active':''?>" ><a href="<?= Router::url(array('controller' => 'infographics', 'action' => 'infographicsview'))?>">Infographics</a></li> -->
            <!--<li class="<?= $this->request->params['action'] == 'upcomingreport'?'active':''?>" ><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'upcomingreport')); ?>">Upcoming Reports</a></li>-->
          <li class="<?= $this->request->params['action'] == 'article_listing'?'active':''?>" ><a href="<?php echo Router::url(array('controller'=>'articles','action'=>'article_listing'));?>">Press Releases</a></li>
          <li class="<?= $this->request->params['action'] == 'aboutus'?'active':''?>" ><a href="<?= Router::url(array('controller' => 'users', 'action' => 'aboutus')); ?>">About Us</a></li>
           <li class="<?= $this->request->params['action'] == 'contactus'?'active':''?>" ><a href="<?= Router::url(array('controller' => 'users', 'action' => 'contactus'))?>">Contact Us</a></li>
            </ul>
        </div>
      </div>
      <?php
      
            if( $this->request->params['action'] == 'home')
            {}
            else
            {
            ?>
            <div class=" col-md-2 col-xs-8 col-sm-3 custom-search-form">
                <?php echo $this->Form->create('Category', array('type' => 'get','action'=>'home_search_form')); ?>
                    <?php echo $this->Form->input('q', array('class' => 'form-control', 'label' =>false,'required','autocomplete'=>'off','id'=>'search_text')); ?>
                    <div class="col-md-11 pull-left suggest-div" id="suggest"></div>
                    <div class="col-md-1 col-sm-1 input-group-btn pull-right search-btn" style="margin-right:-5px;">
                      <button class="btn btn-primary" type="submit">
                        <i class="fa fa-search" style="font-size: 18px;" aria-hidden="true"></i>
                    </button>
                    </div>
                <?php  echo $this->Form->end(); ?>
                </div>
                <?php } ?>
    </div>

  </nav>
  <?php //echo $this->request->params['action'] == 'aboutus'
  ?>

 
  <?php
  if($this->request->params['action'] == 'home')
  { ?>
    <div class="inner-main home_padding" style="padding-top:0px;"></div>
    <?php }
   else
   {
    ?>
    <div class="inner-main"></div>
    <?php }
  ?>
  <!-- <div class="inner-main"></div> -->

  
