<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php 
// @header('Cache-Control: no-store');
// @header('Cache-Control: post-check=0,pre-check=0',false);

?>
<meta name="msvalidate.01" content="02E68A5E1AD1BB527D849ECE2A716647" />


<?php if (isset($meta_name) && !empty($meta_name)): ?>
    <title><?php echo $meta_name ?></title>
<?php else: ?>
    <title>Value Market Research:Market Research Reports ,Industry Analysis, Market Insights</title>
<?php endif; ?>

<?php if (isset($meta_keyword) && !empty($meta_keyword)): ?>
    <meta name="keywords" content="<?php echo $meta_keyword ?>">
<?php else: ?>
    <meta name="keywords" content="reports">
<?php endif; ?>

<?php if (isset($meta_desc) && !empty($meta_desc)): ?>
    <meta name="description" content="<?php echo strip_tags($meta_desc) ?>">
<?php else: ?>
    <meta name="description" content="VMR">
<?php endif; ?>

<?php if(!empty($product['Product']['product_name'])){ ?>
    <meta property="og:title" content="<?php echo $product['Product']['alias'] ?>">
    <meta property="og:description" content="<?php //echo substr($product['Product']['meta_desc'],0,200);
    echo $text = strip_tags($product['Product']['meta_desc']);
    ?>">
   
    <meta property="og:image" content="<?= Router::url('/', true); ?>img/reports/report1.webp">
    <meta property="og:url" content="<?= Router::url(null, true); ?>">
<?php } ?>
<!-- Favicon -->


<!-- <link rel="stylesheet" type="text/css" href="<?= Router::url('/', true); ?>css/front/bootstrap.min.css"> -->
<link href="<?= Router::url('/', true); ?>css/front/bootstrap.min.css" rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript><link rel="stylesheet" href="<?= Router::url('/', true); ?>css/front/bootstrap.min.css"></noscript>


<!-- <link rel="stylesheet" type="text/css" href="<?= Router::url('/', true); ?>css/front/style.min.css"> -->
<link href="<?= Router::url('/', true); ?>css/front/style.min.css" rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript><link rel="stylesheet" href="<?= Router::url('/', true); ?>css/front/style.min.css"></noscript>

<!-- <link rel="stylesheet" type="text/css" href="<?= Router::url('/', true); ?>css/front/bvalidator.min.css"> -->
<link href="<?= Router::url('/', true); ?>css/front/bvalidator.min.css" rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript><link rel="stylesheet" href="<?= Router::url('/', true); ?>css/front/bvalidator.min.css"></noscript>



<?php echo $this->Html->script(array('front/jquery.min.js',)); ?>

<?php
echo $this->Html->meta('favicon.ico', '/img/favicon.ico', array(
    'type' => 'icon'
));
// echo $this->Html->css(array('front/m_front.min.css','front/elastislide.min.css'));
?>
<script>
    var base_url = "<?php echo Router::url('/', true) ?>";
</script>

<?php
echo $this->fetch('meta');
// echo $this->fetch('css');
// echo $this->fetch('script');
?>
<?php /*
  <script id="_webengage_script_tag" type="text/javascript">
  var _weq = _weq || {};
  _weq['webengage.licenseCode'] = '~10a5cbc6b';
  _weq['webengage.widgetVersion'] = "4.0";

  (function(d) {
  var _we = d.createElement('script');
  _we.type = 'text/javascript';
  _we.async = true;
  _we.src = (d.location.protocol == 'https:' ? "https://ssl.widgets.webengage.com" : "http://cdn.widgets.webengage.com") + "/js/widget/webengage-min-v-4.0.js";
  var _sName": "What is the total market value of global deep hole drilling machines market report?",
        ode = d.getElementById('_webengage_script_tag');
  _sNode.parentNode.insertBefore(_we, _sNode);
  })(document);
  </script>
 *
 */ ?>

<?php //Home Page Start
if(Router::url('/', true) == Router::url(null, true)) { ?>
<?php //========================= Organization============== 
$logo = $this->Html->image('logos/vmr-logo.png', array('class' => 'img-thumbnail image_size_home','fullBase' => true));
preg_match_all('~<img.*?src=["\']+(.*?)["\']+~', $logo, $logo_url);
$logo_img_url = $logo_url[1];
?>         
<script type="application/ld+json">
    {
    "@context": "https://schema.org",
    "@type": "Organization",
    "url": "<?php echo Router::url('/', true) ?>",
    "logo": "<?php echo $logo_img_url[0]; ?>"
    }
</script>
<?php //=======================Search Action================ ?> 
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "WebSite",
        "url": "<?php echo Router::url('/', true) ?>",
        "potentialAction": {
            "@type": "SearchAction",
            "target": {
            "@type": "EntryPoint",
            "urlTemplate": "<?php echo Router::url('/', true) ?>search?q={search_term_string}"
            },
            "query-input": "required name=search_term_string"
        }
    }
</script> 
<?php } ?>
<?php //Home Page End ?>
<?php //About Us Page Start
if(Router::url(null, true) == Router::url(array('controller' => 'users', 'action' => 'aboutus'),true)) { ?>
<?php //========================= Organization============== 
$logo = $this->Html->image('logos/vmr-logo.png', array('class' => 'img-thumbnail image_size_home','fullBase' => true));
preg_match_all('~<img.*?src=["\']+(.*?)["\']+~', $logo, $logo_url);
$logo_img_url = $logo_url[1];
?> 
<script type="application/ld+json">
    {
    "@context": "https://schema.org",
    "@type": "Organization",
    "url": "<?php echo Router::url('/', true) ?>",
    "logo": "<?php echo $logo_img_url[0]; ?>"
    }
</script>
<?php //=======================Search Action================ ?> 
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "WebSite",
    "url": "<?php echo Router::url('/', true) ?>",
    "potentialAction": {
        "@type": "SearchAction",
        "target": {
        "@type": "EntryPoint",
        "urlTemplate": "<?php echo Router::url('/', true) ?>search?q={search_term_string}"
        },
        "query-input": "required name=search_term_string"
    }
}
</script> 
<?php } ?>
<?php //About Us Page End ?>
<?php //Category Page Start
App::import('Model', 'Category');  
$this->Category = new Category();     
$catMenu = $this->Category->find('all', array(
    'fields' => array('Category.category_name', 'Category.id'),
    'conditions' => array('Category.parent_category_id' => 2,'Category.is_active' => 1), 
    'recursive' => -1,
    'cache' => 'categoryMenuList', 
    'cacheConfig' => 'long'
));   
foreach ($catMenu as $key => $cat) {         
$category_url = Router::url(array('controller' => 'products', 'action' => 'category_listing',  'slug' => $this->Link->cleanString($cat['Category']['category_name'])),true);        
if(Router::url(null, true) == $category_url) { ?>
<?php //=========================Breadcrumb List============== ?> 
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [{
        "@type": "ListItem",
        "position": 1,
        "item": {
            "@id": "<?php echo Router::url('/', true) ?>",
            "name": "Home"
        }
        },{
        "@type": "ListItem",
        "position": 2,
        "item": {
            "@id": "<?php echo Router::url(array('controller' => 'categories', 'action' => 'category_list'),true); ?>",
            "name": "INDUSTRIES"
        }
        },{
        "@type": "ListItem",
        "position": 3,
        "item": {
            "@id": "<?php echo $category_url; ?>",
            "name": "<?php echo $cat['Category']['category_name']; ?>"
        }
        }]
    }               
</script>
<?php //=======================Search Action================ ?> 
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "WebSite",
    "url": "<?php echo Router::url('/', true) ?>",
    "potentialAction": {
        "@type": "SearchAction",
        "target": {
        "@type": "EntryPoint",
        "urlTemplate": "<?php echo Router::url('/', true) ?>search?q={search_term_string}"
        },
        "query-input": "required name=search_term_string"
    }
}
</script> 
<?php } } ?>
<?php //Category Page End ?>
<?php //RD Page Start ?>
<?php if(!empty($product['Product']['product_name'])) { 
$i = 1;  
$json_string = json_decode($product['Product']['product_faq'],true); 
$len = count($json_string);  
$report_url = Router::url(null, true); 
$description = substr(strip_tags($product['Product']['product_description']),0,400);
$schema_data = (!empty($product['Product']['schema_data'])) ? json_decode($product['Product']['schema_data'],TRUE) : '';        
$string = htmlentities($description, null, 'utf-8');
$content = str_replace("&nbsp;", "", $description);
$content = html_entity_decode($content); 
$content = preg_replace('/\s+/', ' ', $content); 
if (!empty($product['Product']['product_image'])) {
$image = $this->Html->image('product_images/' . $product['Product']['product_image'], array('class' => 'img-thumbnail image_size_home','fullBase' => true));
} else {
$image = $this->Html->image('reports/report1.webp', array('class' => 'img-thumbnail image_size_home','fullBase' => true));
}    
preg_match_all('~<img.*?src=["\']+(.*?)["\']+~', $image, $urls);
$image_url = $urls[1];         
App::import('Model', 'ProductCategory');       
$this->ProductCategory = new ProductCategory();              
$select_prod_category = $this->ProductCategory->find('all', array('conditions' => array('ProductCategory.product_id' => $product['Product']['id']), 'fields' => array('ProductCategory.*', 'Category.category_name')));
$industry_url = Router::url(array('controller' => 'products', 'action' => 'category_listing',  'slug' => $this->Link->cleanString($select_prod_category[0]['Category']['category_name'])),true);
if(!empty($json_string)) { ?>
<?php //====================Breadcrumb List================ ?>        
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [{
        "@type": "ListItem",
        "position": 1,
        "item": {
            "@id": "<?php echo Router::url('/', true) ?>",
            "name": "Home"
        }
        },{
        "@type": "ListItem",
        "position": 2,
        "item": {
            "@id": "<?php echo $industry_url; ?>",
            "name": "<?php echo $select_prod_category[0]['Category']['category_name']; ?>"
        }
        },{
        "@type": "ListItem",
        "position": 3,
        "item": {
            "@id": "<?php echo $report_url; ?>",
            "name": "<?php echo $product['Product']['alias']; ?>"
        }
        }]
    }              
</script>
<?php //=======================Search Action================ ?>  
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "WebSite",
        "url": "<?php echo Router::url('/', true) ?>",
        "potentialAction": {
            "@type": "SearchAction",
            "target": {
            "@type": "EntryPoint",
            "urlTemplate": "<?php echo Router::url('/', true) ?>search?q={search_term_string}"
            },
            "query-input": "required name=search_term_string"
        }
    }
</script>
<?php //===============================Product==============================  ?>        
<script type="application/ld+json">
    {
        "@context": "https://schema.org/",
        "@type": "Product",
        "name": "<?php echo $product['Product']['alias']; ?>", 
        "image": [
        "<?php echo $image_url[0]; ?>"
        ],       
        "description": "<?php echo $content; ?>",
        "sku" : "<?php echo (!empty($select_prod_category)) ? $select_prod_category[0]['Category']['category_name'] : ''; ?>",
        "mpn": "<?php echo $product['Product']['product_no']; ?>",
        "brand": 
        {
            "@type": "Brand",
            "name": "Value Market Research"
        },
        "review": {
        "@type": "Review",
        "reviewRating": {
        "@type": "Rating",
        "ratingValue": "<?php echo $schema_data['rating']; ?>",
        "bestRating": "5"
        },
        "author": {
        "@type": "Organization",
        "name": "Value Market Research"
        }
        },
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "<?php echo $schema_data['aggregate_rating']; ?>",
            "reviewCount": "<?php echo $schema_data['reviewCount']; ?>"
        },
        "offers": {
            "@type": "Offer",
            "url": "<?php echo $report_url; ?>",
            "priceCurrency": "USD",
            "price": "<?php echo $product['Product']['price']; ?>",
            "priceValidUntil": "<?php echo date('Y-m-d', strtotime('+3 months')); ?>",
            "itemCondition": "PDF",
            "availability": "https://schema.org/OnlineOnly"
        }                 
    }
</script>
<?php //====================FAQ================ ?>        
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "FAQPage",
        "mainEntity": [<?php foreach($json_string as $question => $answer) { ?> {
            "@type": "Question",
            "name": "<?php echo $question; ?>",
            "acceptedAnswer": {
            "@type": "Answer",        
            "text": "<?php echo $answer; ?>&lt;a href='<?php echo $report_url; ?>'&gt;&lt;/a&gt;"
            }       
        }<?php if($i < $len){echo ",";} $i++; } ?>]      
    }    
</script>
<?php } else { ?>
<?php //====================Breadcrumb List================ ?> 
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [{
        "@type": "ListItem",
        "position": 1,
        "item": {
            "@id": "<?php echo Router::url('/', true) ?>",
            "name": "Home"
        }
        },{
        "@type": "ListItem",
        "position": 2,
        "item": {
            "@id": "<?php echo $industry_url; ?>",
            "name": "<?php echo $select_prod_category[0]['Category']['category_name']; ?>"
        }
        },{
        "@type": "ListItem",
        "position": 3,
        "item": {
            "@id": "<?php echo $report_url; ?>",
            "name": "<?php echo $product['Product']['alias']; ?>"
        }
        }]
    }              
</script>
<?php //=======================Search Action================ ?> 
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "WebSite",
        "url": "<?php echo Router::url('/', true) ?>",
        "potentialAction": {
            "@type": "SearchAction",
            "target": {
            "@type": "EntryPoint",
            "urlTemplate": "<?php echo Router::url('/', true) ?>search?q={search_term_string}"
            },
            "query-input": "required name=search_term_string"
        }
    }
</script>
<?php //===============================Product============================== ?>        
<script type="application/ld+json">
    {
        "@context": "https://schema.org/",
        "@type": "Product",
        "name": "<?php echo $product['Product']['alias']; ?>", 
        "image": [
        "<?php echo $image_url[0]; ?>"
        ],       
        "description": "<?php echo $content; ?>",
        "sku" : "<?php echo (!empty($select_prod_category)) ? $select_prod_category[0]['Category']['category_name'] : ''; ?>",
        "mpn": "<?php echo $product['Product']['product_no']; ?>",
        "brand": 
        {
            "@type": "Brand",
            "name": "Value Market Research"
        },
        "review": {
        "@type": "Review",
        "reviewRating": {
        "@type": "Rating",
        "ratingValue": "<?php echo $schema_data['rating']; ?>",
        "bestRating": "5"
        },
        "author": {
        "@type": "Organization",
        "name": "Value Market Research"
        }
        },
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "<?php echo $schema_data['aggregate_rating']; ?>",
            "reviewCount": "<?php echo $schema_data['reviewCount']; ?>"
        },
        "offers": {
            "@type": "Offer",
            "url": "<?php echo $report_url; ?>",
            "priceCurrency": "USD",
            "price": "<?php echo $product['Product']['price']; ?>",
            "priceValidUntil": "<?php echo date('Y-m-d', strtotime('+3 months')); ?>",
            "itemCondition": "PDF",
            "availability": "https://schema.org/OnlineOnly"
        }                 
    }
</script> 
<?php } } ?>
<?php //RD Page End ?>
<?php //Press Releases List Page Start                       
$pr_url = Router::url(array('controller'=>'articles','action'=>'article_listing'),true);        
if(Router::url(null, true) == $pr_url) { ?>
<?php //=========================Breadcrumb List============== ?>                    
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [{
        "@type": "ListItem",
            "position": 1,
            "item": {
            "@id": "<?php echo Router::url('/', true) ?>",
            "name": "Home"
            }
        },{
        "@type": "ListItem",
        "position": 2,
        "item": {
        "@id": "<?php echo $pr_url; ?>",
        "name": "Press Releases"
        }
        }]
    }  
</script>
<?php //=======================Search Action================ ?> 
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "WebSite",
        "url": "<?php echo Router::url('/', true) ?>",
        "potentialAction": {
            "@type": "SearchAction",
            "target": {
            "@type": "EntryPoint",
            "urlTemplate": "<?php echo Router::url('/', true) ?>search?q={search_term_string}"
            },
            "query-input": "required name=search_term_string"
        }
    }
</script> 
<?php } ?>
<?php //Press Releases List Page End ?>
<?php //Press Releases Page Start                
App::import('Model', 'Article');        
$this->Article = new Article();                
$pr_url = Router::url(array('controller'=>'articles','action'=>'article_listing'),true);
$press_releases = $this->Article->find('all',array('conditions' => array('Article.article_type' => 'press-releases')));                    
foreach($press_releases as $pr) { 
$pr_item_url = Router::url(array('controller'=>'articles','action'=>'article_details', 'type' => 'pressreleases', 'url_slug'=>$this->Link->cleanString($pr['Article']['slug'])),true);
if(Router::url(null, true) == $pr_item_url) { ?>
<?php //=========================Breadcrumb List============== ?>                    
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [{
        "@type": "ListItem",
            "position": 1,
            "item": {
            "@id": "<?php echo Router::url('/', true) ?>",
            "name": "Home"
            }
        },{
        "@type": "ListItem",
        "position": 2,
        "item": {
        "@id": "<?php echo $pr_url; ?>",
        "name": "Press Releases"
        }
        },{
        "@type": "ListItem",
        "position": 3,
        "item": {
        "@id": "<?php echo $pr_item_url; ?>",
        "name": "<?php echo $pr['Article']['headline']; ?>"
        }
        }]
    }  
</script>
<?php //=======================Search Action================ ?> 
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "WebSite",
        "url": "<?php echo Router::url('/', true) ?>",
        "potentialAction": {
            "@type": "SearchAction",
            "target": {
            "@type": "EntryPoint",
            "urlTemplate": "<?php echo Router::url('/', true) ?>search?q={search_term_string}"
            },
            "query-input": "required name=search_term_string"
        }
    }
</script> 
<?php } } ?>
<?php //Press Releases Page End ?>
<?php //Analysis List Page Start                       
$as_url = Router::url(array('controller' => 'articles', 'action' => 'analysis_listing'),true);        
if(Router::url(null, true) == $as_url) { ?>
<?php //=========================Breadcrumb List============== ?>                 
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [{
        "@type": "ListItem",
            "position": 1,
            "item": {
            "@id": "<?php echo Router::url('/', true) ?>",
            "name": "Home"
            }
        },{
        "@type": "ListItem",
        "position": 2,
        "item": {
        "@id": "<?php echo $as_url; ?>",
        "name": "Analysis"
        }
        }]
    }  
</script>
<?php //=======================Search Action================ ?> 
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "WebSite",
        "url": "<?php echo Router::url('/', true) ?>",
        "potentialAction": {
            "@type": "SearchAction",
            "target": {
            "@type": "EntryPoint",
            "urlTemplate": "<?php echo Router::url('/', true) ?>search?q={search_term_string}"
            },
            "query-input": "required name=search_term_string"
        }
    }
</script> 
<?php } ?>
<?php //Analysis List Page End ?>
<?php //Analysis Page Start                
App::import('Model', 'Article');        
$this->Article = new Article();                
$as_url = Router::url(array('controller' => 'articles', 'action' => 'analysis_listing'),true);           
$analyis = $this->Article->find('all',array('conditions' => array('Article.article_type' => 'analysis')));             
foreach($analyis as $as) { 
$as_item_url = Router::url(array('controller'=>'articles','action'=>'article_details','type' => 'analysis','url_slug'=>$this->Link->cleanString($as['Article']['slug'])),true);
if(Router::url(null, true) == $as_item_url) { ?>
<?php //=========================Breadcrumb List============== ?>                  
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [{
        "@type": "ListItem",
            "position": 1,
            "item": {
            "@id": "<?php echo Router::url('/', true) ?>",
            "name": "Home"
            }
        },{
        "@type": "ListItem",
        "position": 2,
        "item": {
        "@id": "<?php echo $as_url; ?>",
        "name": "Analysis"
        }
        },{
        "@type": "ListItem",
        "position": 3,
        "item": {
        "@id": "<?php echo $as_item_url; ?>",
        "name": "<?php echo $as['Article']['headline']; ?>"
        }
        }]
    }  
</script>
<?php //=======================Search Action================ ?> 
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "WebSite",
        "url": "<?php echo Router::url('/', true) ?>",
        "potentialAction": {
            "@type": "SearchAction",
            "target": {
            "@type": "EntryPoint",
            "urlTemplate": "<?php echo Router::url('/', true) ?>search?q={search_term_string}"
            },
            "query-input": "required name=search_term_string"
        }
    }
</script> 
<?php } } ?>
<?php //Analysis Page End ?>
<?php //Reports Page Start                       
$re_url = Router::url(array('controller' => 'products', 'action' => 'reportlist'),true);        
if(Router::url(null, true) == $re_url) { ?>
<?php //=========================Breadcrumb List============== ?> 
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [{
        "@type": "ListItem",
            "position": 1,
            "item": {
            "@id": "<?php echo Router::url('/', true) ?>",
            "name": "Home"
            }
        },{
        "@type": "ListItem",
        "position": 2,
        "item": {
        "@id": "<?php echo $re_url; ?>",
        "name": "Reports"
        }
        }]
    }  
</script>
<?php //=======================Search Action================ ?> 
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "WebSite",
        "url": "<?php echo Router::url('/', true) ?>",
        "potentialAction": {
            "@type": "SearchAction",
            "target": {
            "@type": "EntryPoint",
            "urlTemplate": "<?php echo Router::url('/', true) ?>search?q={search_term_string}"
            },
            "query-input": "required name=search_term_string"
        }
    }
</script>
<?php } ?>
<?php //Reports Page End ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-122020282-1"></script>
<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-122020282-1', 'auto');
ga('send', 'pageview');
</script>