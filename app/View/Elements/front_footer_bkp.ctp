 <!-- FOOTER SECTION START -->
<footer>
<section class="index-link">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="link-area">
                    <h3>About Us</h3>
                    <P>Building Consensus among your Senior leaders to leverage your digital strengths and work on gaps which are hindering your growth Lorem ipsum dolor sit amet, consectetuer.</P>
                </div>
            </div>
            <div class="col-md-3">
                <div class="link-area">
                    <h3>Useful Links</h3>
                    <ul>
                    <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'aboutus')); ?>"> About Us</a></li>
                    <li><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'reportlist')); ?>"> Reports</a></li>
                    <!--<li><a href="<?php echo Router::url(array('controller' => 'blogs', 'action' => 'blog_listing')); ?>"> Blog</a></li>-->
                    <li><a href="<?php echo Router::url(array('controller' => 'testimonials', 'action' => 'listing' )); ?>"> Testimonials</a></li>
                    <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'contactus'))?>"> Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="link-area">
                    <h3>Find Assistance</h3>
                    <ul>
                    <li><a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'article_listing','section'=>'press-releases')); ?>"> Press release</a></li>
                    <!--<li><a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'article_listing','section'=>'analysis')); ?>"> Analysis</a></li>-->
                    <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'privacy_policy')); ?>"> Privacy Policy</a></li>
                    <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'terms_conditions')); ?>"> Terms & Conditions</a></li>
                    <li><a href="<?= Router::url(array('controller' => 'sitemap', 'ext' => 'xml')); ?>" target="_blank"> Sitemap</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 address">
                <div class="link-area">
                    <h3>Contact</h3>
                    <p>3<sup>rd</sup> Floor, Fountain chambers, Nanabhai Lane Fort, Mumbai-1. Maharashtra, INDIA.
                       sales@valuemarketresearch.com
                   </p>
                   <ul class="pay-option">
                       <li><img src="<?= Router::url('/', true); ?>/img/payment/paypal.png" alt="paypal"></li>
                       <li><img src="<?= Router::url('/', true); ?>/img/payment/visa.png" alt="visa"></li>
                       <li><img src="<?= Router::url('/', true); ?>/img/payment/mastercard.png" alt="mastercard"></li>
                       <li><img src="<?= Router::url('/', true); ?>/img/payment/american-express.png" alt="american-express"></li>
                   </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="index-social">
  <div class="container">
    <div class="row">
    <div class="col-md-7 col-xs-12 index-social-link text-left">
      <p class="copy-c">&copy; 2018, All Rights Reserved, Value Market Research <!-- Design &amp; Developed By <a href="#">Conversant Technologies</a> --></p>
    </div>
    <div class="col-md-5 col-xs-12 footer-social text-right">
      <ul>
        <li><a href="#."><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#."><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="#."><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
      </ul>
    </div>
  </div>
</div>
</section>
</footer>
<!-- FOOTER SECTION END -->

  <?php
  echo $this->Html->script(array(
    'front/jquery.min.js',
    'front/jquery.easing.min.js',
    'front/bootstrap.min.js',
    'front/jquery.bvalidator-yc.js',
    'front/custom.js',
  ));
  ?>

<!-- Google Captcha Code Start here VG-15/2/2015 -->
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
    $("#contact_us").submit(function(event) {
    
        var recaptcha = $("#g-recaptcha-response").val();
        if (recaptcha === "") {
            event.preventDefault();
            alert("Please check the recaptcha");
        }
    });
    
    $("#enquiry_form").submit(function(event) {
        var recaptcha = $("#g-recaptcha-response").val();
        if (recaptcha === "") {
            event.preventDefault();
            alert("Please check the recaptcha");
        }
    });
</script>
<!-- Google Captcha Code end here VG-15/2/2015 -->

  <script type="text/javascript">
    $(document).ready(function () {
      $('.navbar .dropdown').hover(function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
      }, function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(105)
      });
    });
  </script>


<!-- New autocomplete search code Start VG-25/11/2016  -->
<script type="text/javascript">
    $(document).ready(function(){
        var query=null;
        var oldquery=null;
        $('#search_text').on('keyup',function(e){

              query=$(this).val();

                if(query != ' ' || query.length > 0)
                {
                    if(e.keyCode !=38 && e.keyCode != 40)
                    {
                        $.ajax({
                               url: "<?php echo Router::url(['controller' => 'Categories', 'action' => 'home_search_autofill']);?>",
                                method:"POST",
                                async: true,
                                data:{query:query},
                                success:function(data)
                                {
                                    $('#suggest').fadeIn();
                                    $('#suggest').html(data);
                                }

                        });
                  }
                }
                else{

                    $('#suggest').fadeOut();
                }
        });

                $(document).on('click', '#result', function(){
                //$('#search_text').val($(this).text());

                $('#suggest').fadeOut();
            });

           $(document).on('mouseenter','#result',function(){
                  query=$("#search_text").val();
                  if(query != ' ' || query.length > 0)
                  {
                       $(this).prev().removeClass('selected');
                        $(this).addClass('selected');
                  }
                  else
                  {
                    $('#suggest').fadeOut(5000);
                  }
            });
            $(document).on('mouseleave','#result',function(){
                $(this).removeClass('selected');

            });
    });
</script>

<script type="text/javascript">
(function(){

  $('#itemslider').carousel({ interval: 5000 });
}());

(function(){
  $('.carousel-showmanymoveone .item').each(function(){
    var itemToClone = $(this);

    for (var i=1;i<6;i++) {
      itemToClone = itemToClone.next();


      if (!itemToClone.length) {
        itemToClone = $(this).siblings(':first');
      }


      itemToClone.children(':first-child').clone()
        .addClass("cloneditem-"+(i))
        .appendTo($(this));
    }
  });
}());

</script>

<!-- New autocomplete search code End VG-25/11/2016  -->
<script type="text/javascript">
    // $('#carousel').elastislide();
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5ac844094b401e45400e6c9d/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
