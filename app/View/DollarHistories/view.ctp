<div class="dollarHistories view">
<h2><?php echo __('Dollar History'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($dollarHistory['DollarHistory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Setting Id'); ?></dt>
		<dd>
			<?php echo h($dollarHistory['DollarHistory']['setting_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prev Rate'); ?></dt>
		<dd>
			<?php echo h($dollarHistory['DollarHistory']['prev_rate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Curr Rate'); ?></dt>
		<dd>
			<?php echo h($dollarHistory['DollarHistory']['curr_rate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated By'); ?></dt>
		<dd>
			<?php echo h($dollarHistory['DollarHistory']['updated_by']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($dollarHistory['DollarHistory']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($dollarHistory['DollarHistory']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Dollar History'), array('action' => 'edit', $dollarHistory['DollarHistory']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Dollar History'), array('action' => 'delete', $dollarHistory['DollarHistory']['id']), null, __('Are you sure you want to delete # %s?', $dollarHistory['DollarHistory']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Dollar Histories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Dollar History'), array('action' => 'add')); ?> </li>
	</ul>
</div>
