<div class="dollarHistories form">
<?php echo $this->Form->create('DollarHistory'); ?>
	<fieldset>
		<legend><?php echo __('Edit Dollar History'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('setting_id');
		echo $this->Form->input('prev_rate');
		echo $this->Form->input('curr_rate');
		echo $this->Form->input('updated_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('DollarHistory.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('DollarHistory.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Dollar Histories'), array('action' => 'index')); ?></li>
	</ul>
</div>
