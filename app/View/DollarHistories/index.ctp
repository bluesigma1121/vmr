<div class="dollarHistories index">
	<h2><?php echo __('Dollar Histories'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('setting_id'); ?></th>
			<th><?php echo $this->Paginator->sort('prev_rate'); ?></th>
			<th><?php echo $this->Paginator->sort('curr_rate'); ?></th>
			<th><?php echo $this->Paginator->sort('updated_by'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($dollarHistories as $dollarHistory): ?>
	<tr>
		<td><?php echo h($dollarHistory['DollarHistory']['id']); ?>&nbsp;</td>
		<td><?php echo h($dollarHistory['DollarHistory']['setting_id']); ?>&nbsp;</td>
		<td><?php echo h($dollarHistory['DollarHistory']['prev_rate']); ?>&nbsp;</td>
		<td><?php echo h($dollarHistory['DollarHistory']['curr_rate']); ?>&nbsp;</td>
		<td><?php echo h($dollarHistory['DollarHistory']['updated_by']); ?>&nbsp;</td>
		<td><?php echo h($dollarHistory['DollarHistory']['created']); ?>&nbsp;</td>
		<td><?php echo h($dollarHistory['DollarHistory']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $dollarHistory['DollarHistory']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $dollarHistory['DollarHistory']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $dollarHistory['DollarHistory']['id']), null, __('Are you sure you want to delete # %s?', $dollarHistory['DollarHistory']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Dollar History'), array('action' => 'add')); ?></li>
	</ul>
</div>
