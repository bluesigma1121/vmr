<style>
    .mtp{
        margin-top: 10px;
    }
    .search_mtp{
        margin-top: 3px;
    }
</style> 

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Lead Users Information </h5>
            </div>
            <div class="ibox-content">
                <?php echo $this->Form->create('User', array('id' => 'index', 'class' => 'form-horizontal')); ?>
                <div class="row">
                    <div class="col-lg-3">
                        <?php
                        echo $this->Form->input('search_by', array(
                            'options' => $user_search,
                            'class' => 'form-control', 'label' => false, 'default' => 'name', 'data-bvalidator' => 'required'));
                        ?>
                    </div>
                    <div class="col-lg-3">
                        <?php echo $this->Form->input('search_text', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Enter Text For Search', 'label' => false)); ?>
                    </div>
                    <div class="col-lg-2 search_mtp">
                        <?php echo $this->Form->button('Search', array('type' => 'submit', 'class' => 'btn btn-sm btn-primary fa fa-search')); ?>
                    </div>
                    <?php echo $this->form->end(); ?>
                </div>
                <div class="table-responsive mtp">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('name'); ?></th>
                                <th><?php echo $this->Paginator->sort('email'); ?></th>
                                <th><?php echo $this->Paginator->sort('role'); ?></th>
                                <th><?php echo $this->Paginator->sort('organisation'); ?></th>
                                <th><?php echo $this->Paginator->sort('job_title'); ?></th>
                                <th><?php echo $this->Paginator->sort('last_login'); ?></th>
                                <th><?php echo $this->Paginator->sort('visited_ip'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead> 
                        <?php foreach ($users as $user) { ?>
                            <tbody>
                            <td><?php echo h($user['User']['name']); ?>&nbsp;</td>
                            <td><?php echo h($user['User']['email']); ?>&nbsp;</td>
                            <td><?php echo h($roles[$user['User']['role']]); ?>&nbsp;</td>
                            <td><?php echo h($user['User']['organisation']); ?>&nbsp;</td>
                            <td><?php echo h($user['User']['job_title']); ?>&nbsp;</td>
                            <td>  <?php if (!empty($user['User']['last_login'])) { ?>
                                    <?php echo date('d-M-y h:i a', strtotime($user['User']['last_login'])); ?>
                                <?php } ?>
                            </td>
                            <td><?php if (!empty($user['User']['visited_ip'])) { ?>
                                    <?php echo $user['User']['visited_ip']; ?>
                                <?php } ?></td>
                            <td class="actions">
                                <?php echo $this->Html->link(__('view'), array('action' => 'view', $user['User']['id']), array('class' => 'btn btn-outline btn-sm btn-warning dim', 'title' => 'View')); ?>
                                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id']), array('class' => 'btn btn-outline btn-sm btn-info dim', 'title' => 'Edit')); ?>       
                                <?php
                                if (AuthComponent::user('role') == 1) {
                                    if ($user['User']['is_active'] == 1) {
                                        echo $this->Html->link(__('Deactive'), array('action' => 'deactive', $user['User']['id']), array('class' => 'btn btn-outline btn-sm btn-info ', 'id' => 'test', 'title' => 'DeActive'));
                                    } else {
                                        echo $this->Html->link(__('Active'), array('action' => 'active', $user['User']['id']), array('class' => 'btn btn-outline btn-sm btn-info', 'id' => 'test', 'title' => 'Active'));
                                    }
                                }
                                ?>
                                <?php
                                if (AuthComponent::user('role') == 1) {
                                    if ($user['User']['is_verified'] == 1) {
                                        echo $this->Html->link(__('DeVerified'), array('action' => 'deverified', $user['User']['id']), array('class' => 'btn btn-outline btn-sm btn-warning', 'title' => 'DeVerified'));
                                    } else {
                                        echo $this->Html->link(__('Verified'), array('action' => 'verified', $user['User']['id']), array('class' => 'btn btn-outline btn-sm btn-warning', 'title' => 'Verified'));
                                    }
                                }
                                ?>
                                <?php
                                if (AuthComponent::user('role') == 1) {
                                    echo $this->Html->link(__('Reset'), array('action' => 'user_reset_password', $user['User']['id']), array('class' => 'btn btn-outline btn-sm btn-default', 'title' => 'Reset Password'));
                                }
                                ?>
                                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id'], 'lead_user_index'), array('class' => 'btn btn-outline btn-sm btn-danger dim', 'title' => 'Delete'), __('Are you sure you want to delete # %s?', $user['User']['name'])); ?>
                            </td>
                            </tbody>
                        <?php } ?>
                    </table>
                    <div class="row col-md-12">
                        <div class="dataTables_paginate paging_bootstrap">
                            <p>
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?> </p>
                            <ul class="pagination" style="visibility: visible;">
                                <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
                                <li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
                                <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 



