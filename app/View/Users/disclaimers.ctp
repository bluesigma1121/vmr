<style>
  .blog-inner a {
      color: #0074BD;
      transition: all .5s ease-in-out;
      outline: none;
      text-decoration: none;
  }
</style>
<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>Terms and Conditions</h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a>Disclaimer</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->
<style>.page-heading{color: #666;font-size: 14px;text-transform: uppercase;font-weight: 700;}</style>

<!-- ABOUT INNER SECTION START -->
<section class="about-inner">

      <div class="container">
      
        <h1 class="page-heading">Disclaimer</h1>
        <div class="">
          <div class="blog-inner blog-details-inner">
            <p>Value Market Research is not responsible for any inaccurate information provided in our reports. As this information is collected based on interviews fluctuations and diversion of data is possible, though we ensure regular checks and data validation through secondary sources.</p>
            <p>Reports and data purchased through us is for personal or company use only. Buyer is not authorized to disclose our data analysis to third party or any other publications.</p>
            <p>Disclosing, lending, circulating, reselling, recording, photocopying, mechanical/electronically reproduction of any table/ section/ chapter of the Value Market Research report or services without written permission are prohibited.</p>
            <p>Decisions made by the clients based on the report analysis are solely their own responsibility. Value Market Research holds no liability for those decisions.</p>
            <p>For permissions or any other query please contact us at <a href="mailto:sales@valuemarketresearch.com">sales@valuemarketresearch.com.</a></p>
            
        </div>
       </div>
    </div>
</section>

<!-- ABOUT INNER SECTION END -->
