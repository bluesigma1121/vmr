<script type="text/javascript">
    $(document).ready(function() {
        $('#edit_user').bValidator();
    });
</script>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Edit Add</h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('User', array('id' => 'edit_user', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-3">
            <div class="form-group"><label class="col-lg-2 control-label">Title</label>
                <div class="col-lg-2">
                    <?php echo $this->Form->input('title', array('class' => 'form-control', 'type' => 'select', 'options' => $title, 'placeholder' => 'Title', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    <?php echo $this->Form->input('id'); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">First Name</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('first_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'First Name', 'label' => false, 'data-bvalidator' => 'required,alpha')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">Last Name</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('last_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Last Name', 'label' => false, 'data-bvalidator' => 'required,alpha')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">Email</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('email', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Email', 'label' => false, 'data-bvalidator' => 'required,email')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">Password</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('password', array('class' => 'form-control', 'type' => 'password', 'id'=>'password_show', 'placeholder' => 'Password', 'label' => false, 'data-bvalidator' => 'required,minlength[6]')); ?>
                    <input type="checkbox" onclick="myFunction()"> Show Password
                </div>
            </div>

            <div class="form-group"><label class="col-lg-2 control-label">Role</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('role', array('class' => 'form-control', 'options' => $roles, 'placeholder' => 'Role', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">Organisation</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('organisation', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'organisation', 'label' => false)); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">Job Title</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('job_title', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'job title', 'label' => false)); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">Address</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('address', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'address', 'label' => false)); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">City</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('city', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'City', 'label' => false)); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">State</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('state', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'state', 'label' => false)); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">Country</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('country', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'country', 'label' => false)); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">pin code</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('pin_code', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'pin code', 'label' => false, 'data-bvalidator' => 'digit')); ?>
                </div>
            </div>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<script>
function myFunction() {
  var x = document.getElementById("password_show");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>


