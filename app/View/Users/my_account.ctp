<div class="breadcrumbs border_tp">
    <div class="container">
        <h1 class="pull-left">Account</h1>
        <ul class="pull-right breadcrumb"> 
            <span class="text_color_bread">Your Are Here :</span>
            <li><a href="<?php echo Router::url('/') ?>">Home</a></li>
            <li><a href="#">Your Account</a></li>
        </ul>
    </div>
</div>
<div class="container content">
    <div class="row">
        <div class="col-md-9">
            <!-- Tabs -->
            <div class="tab-v2 margin-bottom-60">
                <?php echo $this->element('my_account_panel'); ?>  
                <div class="tab-content">
                    <div id="home" class="accountTab" style="display: block;">
                        <h2>
                            Hello <?php echo ucfirst(AuthComponent::user('first_name')); ?>,
                            <br>
                            Welcome to Your Account
                        </h2> 
                        <p>There are a number of features available to you:</p>

                        <p class="accountBlurb">
                            <a id="enquiries-blurb-link" href="<?php echo Router::url(array('controller' => 'enquiries', 'action' => 'user_enquiry_index', AuthComponent::user('id'))); ?>">Inquiries<span class="rightArrow"> &gt;</span></a>
                            See your previous and current inquiries placed with our Customer Service Team from the Inquiries Page and submit new inquiries to us.
                        </p>
                        <p class="accountBlurb">
                            <a id="orders-blurb-link" href="#">Order History<span class="rightArrow"> &gt;</span></a>
                            See your orders by visiting the Order History page.
                        </p>
                        <p class="accountBlurb">
                            <a href="<?php echo Router::url(array('action' => 'logout')); ?>" class="logoutLink">Log Out<span class="rightArrow"> &gt;</span></a>
                            Click the Log Out button to log out of Your Account.
                        </p>
                    </div>
                </div>
                <div class="tab-content"> 
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-edit">
                                        </span> Contact Details</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="panel-body">                                                      
                                        <?php echo $this->Form->create('User', array('action' => 'edit_details', 'id' => 'edit_details', 'class' => 'form-horizontal', 'type' => 'file')); ?>
                                        <div class="row">
                                            <div class="form-group"><label class="col-lg-3 control-label">Title</label>
                                                <div class="col-lg-2">
                                                    <?php echo $this->Form->input('title', array('class' => 'form-control', 'type' => 'select', 'options' => $title, 'placeholder' => 'Title', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                                    <?php echo $this->Form->input('id'); ?>
                                                </div>
                                            </div> 
                                            <div class="form-group"><label class="col-lg-3 control-label">First Name</label>
                                                <div class="col-lg-6">
                                                    <?php echo $this->Form->input('first_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'First Name', 'label' => false, 'data-bvalidator' => 'required,alpha')); ?>
                                                </div>
                                            </div> 

                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Last Name</label>
                                                <div class="col-lg-6">
                                                    <?php echo $this->Form->input('last_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Last Name', 'label' => false, 'data-bvalidator' => 'required,alpha')); ?>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-lg-3 control-label">Organisation</label>
                                                <div class="col-lg-6">
                                                    <?php echo $this->Form->input('organisation', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                                </div>
                                            </div> 
                                            <div class="form-group"><label class="col-lg-3 control-label">Job Title</label>
                                                <div class="col-lg-6">
                                                    <?php echo $this->Form->input('job_title', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                                </div>
                                            </div> 

                                            <div class="form-group"><label class="col-lg-3 control-label">Address</label>
                                                <div class="col-lg-6">
                                                    <?php echo $this->Form->input('address', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                                </div>
                                            </div> 

                                            <div class="form-group"><label class="col-lg-3 control-label">City</label>
                                                <div class="col-lg-6">
                                                    <?php echo $this->Form->input('city', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                                </div>
                                            </div> 

                                            <div class="form-group"><label class="col-lg-3 control-label">Pin Code</label>
                                                <div class="col-lg-6">
                                                    <?php echo $this->Form->input('pin_code', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                                </div>
                                            </div> 

                                            <div class="form-group"><label class="col-lg-3 control-label">State</label>
                                                <div class="col-lg-6">
                                                    <?php echo $this->Form->input('state', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                                </div>
                                            </div> 

                                            <div class="form-group"><label class="col-lg-3 control-label">Country</label>
                                                <div class="col-lg-6">
                                                    <?php echo $this->Form->input('country', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                                </div>
                                            </div> 
                                            <?php /*<div class="form-group"><label class="col-lg-3 control-label">Mobile</label>
                                                <div class="col-lg-4">
                                                    <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => '+_(___)___-__-__', 'id' => 'customer_phone', 'size' => '20', 'maxlength' => '25', 'data-bvalidator' => 'required')); ?>
                                                    <input type="checkbox" id="phone_mask" checked=""> <label id="descr" for="phone_mask"></label>
                                                </div>
                                            </div> */?>
                                            
                                            <div class="form-group"><label class="col-lg-3 control-label">Mobile</label>
                                                <div class="col-lg-4">
                                                    <?php echo $this->Form->input('mobile', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                                </div>
                                            </div> 
                                            <div class="form-group"><label class="col-lg-3 control-label">Landline</label>

                                                <div class="col-lg-4">
                                                    <?php echo $this->Form->input('landline', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-lg-3 control-label">Fax No</label>
                                                <div class="col-lg-4">
                                                    <?php echo $this->Form->input('fax_no', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <?php echo $this->Form->submit(__('Update'), array('class' => 'btn btn-u btn-sm', 'div' => false)); ?>
                                        </div>
                                        <?php echo $this->Form->end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-lock">
                                        </span> Change Password</a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="panel-body">
                                        <?php echo $this->Form->create('User', array('action' => 'change_user_passwaord', 'id' => 'Change_password_user', 'class' => 'form-horizontal', 'type' => 'file')); ?>
                                        <div class="row col-lg-offset-2">

                                            <div class="form-group"><label class="col-lg-3 control-label">Current Password</label>
                                                <div class="col-lg-4">
                                                    <?php echo $this->Form->input('cuurent_pwd', array('type' => 'password', 'class' => 'form-control hastip', 'placeholder' => 'Enter Current Password', 'label' => false, 'title' => 'Enter Current Password', 'data-bvalidator' => 'required')); ?>
                                                </div>
                                            </div>

                                            <div class="form-group"><label class="col-lg-3 control-label">New Password</label>
                                                <div class="col-lg-4">
                                                    <?php unset($this->request->data['User']['password']) ?>
                                                    <?php echo $this->Form->input('password', array('type' => 'password', 'class' => 'form-control hastip', 'placeholder' => 'Enter New password ', 'label' => false, 'title' => 'Enter New password', 'data-bvalidator' => 'required')); ?>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-lg-3 control-label">Confirm Password</label>
                                                <div class="col-lg-4">
                                                    <?php echo $this->Form->input('Confirm_password', array('type' => 'password', 'class' => 'form-control hastip', 'placeholder' => 'Enter Confirm Password', 'label' => false, 'title' => 'Enter Confirm Password', 'data-bvalidator' => 'required')); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <?php echo $this->Form->submit(__('Change'), array('class' => 'btn btn-u btn-sm', 'div' => false)); ?>
                                        </div>
                                        <?php echo $this->Form->end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-3">
        </div>
    </div>
</div>
