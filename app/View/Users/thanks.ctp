<style>
/*404 Error Page v1 
------------------------------------*/
.error-v1 {
	padding-bottom: 30px;
	text-align: center;	
}

.error-v1 p {
	color: #555;
	font-size: 16px;
}

.error-v1 span {
	color: #555;
	display: block;
	font-size: 35px;
	font-weight: 200;
}

.error-v1 span.error-v1-title {
	color: #777;	
	font-size: 180px;
	line-height: 200px;
	padding-bottom: 20px;
}

/*For Mobile Devices*/
@media (max-width: 500px) { 
	.error-v1 p {
		font-size: 12px;
	}	

	.error-v1 span {
		font-size: 25px;
	}

	.error-v1 span.error-v1-title {
		font-size: 140px;
	}
}
</style>

<div class="container content">		
    <!--Error Block-->
    <div class="row" style="padding: 37px 10px 38px 10px;">
        <div class="col-md-12">
            <div class="error-v1">
                <span><?php echo $line1; ?></span>
                <p class="text-center"><?php echo $line2; ?></p>
               <!--  <?php /*
               <a class="btn-u btn-bordered" href="<?php echo Router::url(array('controller' => 'orders', 'action' => 'user_order_index')) ?>">My Orders</a>
               */ ?> -->

                 <!-- code starts here-VG-10/08/2016 -->
                <?php if(!empty($line4)) { ?>
                    <?php  echo $this->Form->create('Enquiry', array('id' => 'enquiry_form', 'class' => 'reg-page', 'type' => 'file')); ?>
                    <a class="btn-u btn-u-green btn-xs" target="_blank" href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($line4))); ?>">View Related Reports
                        </a> </h2>
                <?php } else {?>
                    <?php  echo $this->Form->create('Enquiry', array('id' => 'enquiry_form', 'class' => 'reg-page', 'type' => 'file')); ?>
                   <a class="btn-u btn-u-green btn-xs" target="_blank" href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'category_list' )); ?>">View Related Reports
                        </a> </h2>
                <?php } ?>       
                 <!-- code starts here-VG-10/08/2016 --> 
                
            </div>
        </div>
    </div>
    <!--End Error Block-->
</div>