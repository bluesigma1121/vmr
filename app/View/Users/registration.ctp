<div class="container content">
    <div class="row margin-bottom-40">
        <!--register-->
        <div class="container content">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <?php echo $this->Form->create('User', array('id' => 'registration_user', 'class' => 'reg-page', 'type' => 'file')); ?>
                    <div class="reg-header">
                        <h2>Register a new account</h2>
                        <p>Already Signed Up? Click <a href="<?php echo Router::url(array('action' => 'login')); ?>" class="color-green">Sign In</a> to login your account.</p>                    
                    </div>
                    <label>First Name<span class="color-red">*</span></label> 
                    <?php echo $this->Form->input('first_name', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'First Name', 'data-bvalidator' => 'required,alpha')); ?>
                    <label>Last Name<span class="color-red">*</span></label>
                    <?php echo $this->Form->input('last_name', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Last Name', 'data-bvalidator' => 'required,alpha')); ?>
                    <label>Email Address <span class="color-red">*</span></label>
                    <?php echo $this->Form->input('email', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Email', 'data-bvalidator' => 'required,email')); ?>
                    <?php /*    <div class="row">
                      <div class="col-sm-6">
                      <label>Mobile <span class="color-red">*</span></label>
                      <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => '+_(___)___-__-__', 'id' => 'customer_phone', 'size' => '20', 'maxlength' => '25', 'data-bvalidator' => 'required')); ?>
                      </div>
                      <div class="col-sm-6">
                      <div class="mar_phone">
                      <input type="checkbox" id="phone_mask" checked=""> <label id="descr" for="phone_mask"></label>
                      </div>
                      </div>
                      </div> */ ?>
                    <label>Mobile<span class="color-red">*</span></label> 
                    <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Mobile', 'data-bvalidator' => 'required')); ?>




                    <!--                    <div class="demo">
                                            <input type="text" id="customer_phone" value="" size="25" maxlength="32" placeholder="+_(___)___-__-__" class=""><br>
                                            <input type="checkbox" id="phone_mask" checked=""><label id="descr" for="phone_mask">Kazakhstan</label>
                                        </div>-->



                    <label>Organisation <span class="color-red">*</span></label>
                    <?php echo $this->Form->input('organisation', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Organisation', 'data-bvalidator' => 'required')); ?>
                    <label>Designation <span class="color-red">*</span></label>
                    <?php echo $this->Form->input('job_title', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Designation', 'data-bvalidator' => 'required,alpha')); ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Password <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('password', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'password', 'placeholder' => 'Password', 'data-bvalidator' => 'required,minlength[6]')); ?>
                        </div>
                        <div class="col-sm-6">
                            <label>Confirm Password <span class="color-red">*</span></label> 
                            <?php echo $this->Form->input('confirm_password', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'password', 'placeholder' => 'Confirm Password', 'data-bvalidator' => 'required,minlength[6]')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="capcha" class="control-label">Captcha :</label>
                        <div class="span3">
                            <?php echo $this->Html->image($this->Html->url(array('controller' => 'users', 'action' => 'captcha'), true), array('label' => false, 'width' => '150', 'height' => '40', 'alt' => 'captcha')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="capcha" class="control-label">Enter Above Code <span class="color-red">*</span></label>
                        <div class="span3">
                            <?php
                            echo $this->Form->input('captcha', array('data-bvalidator' => 'required', 'class' => 'input-large form-control hastip', 'data-placement' => 'right', 'type' => 'text', 'placeholder' => 'Enter Captcha Code',
                                'title' => 'Enter the characters you see in the image.', 'label' => false));
                            ?>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-6 checkbox">
                            <label>
                                <?php echo $this->Form->checkbox('term', array('label' => false, 'data-bvalidator' => 'required')); ?>
                                I read <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'terms_conditions')); ?>"  target="_blank" class="color-green">Terms and Conditions</a>
                            </label>                        
                        </div>
                        <div class="col-lg-6 text-right">
                            <?php echo $this->Form->submit('Register', array('class' => 'btn-u')) ?>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
        <!-- Reg end -->        
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2"> 
            <h3>Already have an account?</h3>
            <a class="btn btn-block btn-bitbucket-inversed rounded" href="<?php echo Router::url(array('action' => 'login')) ?>">  <i class="fa fa-lock"></i> Get Back To Login
            </a>
        </div>
    </div><!--/row-->
</div> 


