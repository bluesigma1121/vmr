<script type="text/javascript">
    $(function() {
        $('#forgot_password_form').bValidator();

    });
</script>
<div class="container">
    <!--Reg Block-->
    <div class="reg-block">
        <?php echo $this->Form->create('User', array('id' => 'forgot_password_form', 'type' => 'file')); ?>
        <div class="reg-block-header">
            <?php echo $this->Session->flash(); ?>
            <h2>Reset your Password</h2>
        </div> 
        <div class="input-group margin-bottom-20">
            Changing your password is simple.
        </div>
        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            <?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => false, 'type' => 'email', 'data-bvalidator' => 'email,required', 'placeholder' => 'Enter Registered Email.')); ?>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <?php echo $this->Form->submit('Reset', array('class' => 'btn-u btn-block')) ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
    <!--End Reg Block-->
</div><!--/container-->
