<script type="text/javascript">
    $(document).ready(function() {
        $('#set_password_form').bValidator();
    });
</script>
<div class="container">
    <!--Reg Block-->
    <div class="reg-block">
        <?php echo $this->Form->create('User', array('id' => 'set_password_form', 'type' => 'file')); ?>
        <div class="reg-block-header">
            <?php echo $this->Session->flash(); ?>
            <h2>Set your Password</h2>
        </div> 
        <div class="input-group margin-bottom-20">
            Set your password is simple.
        </div>
        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-key"></i></span>
            <?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => false, 'type' => 'password', 'data-bvalidator' => 'required', 'placeholder' => 'Enter  Password.')); ?>
        </div>
        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-key"></i></span>
            <?php echo $this->Form->input('Confirm_password', array('class' => 'form-control', 'label' => false, 'type' => 'password', 'data-bvalidator' => 'required', 'placeholder' => 'Enter Confirm Password.')); ?>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <?php echo $this->Form->submit('Set Password', array('class' => 'btn-u btn-block')) ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
    <!--End Reg Block-->
</div><!--/container-->
