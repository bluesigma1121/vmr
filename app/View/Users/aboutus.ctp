<style>
  .client_image:after { content: attr(alt); }
</style>
<script type="text/javascript">$(document).ready(function () { 
  $(".client_image").each(function(){
      $(this).attr("title", $(this).attr("alt"));
  });
});
$(function () { $('[data-toggle="tooltip"]').tooltip()});
</script>
<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>About Us</h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a>About Us</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->
<style>.page-heading{color: #666;font-size: 14px;text-transform: uppercase;font-weight: 700;}</style>

<div class="container content about-inner-page">
    <div class="row margin-bottom-40">
        <div class="col-md-12 md-margin-bottom-40">
            <div class="headline"><h1 class="page-heading">About Us</h1></div>
  <!--          <p>-->
  <!--              Value Market Research is a global business research reports provider, enriching decision makers and strategists with qualitative statistics.-->
		<!--Value Market Research is proficient in providing syndicated research report, customized research reports, company profiles and industry databases-->
		<!--across multiple domains. Our expert research analysts have been trained to map client’s research requirements to the correct research resource-->
		<!--leading to a distinctive edge over its competitors. We provide intellectual, precise and meaningful data at a lightning speed.-->
  <!--          </p>-->
            <p>Value Market Research was established with the vision to ease decision making and empower the strategists by providing them with holistic market information.</p>
            <p>We facilitate clients with syndicate research reports and customized research reports on 25+ industries with global as well as regional coverage.</p>
            <p>We have 200+ happy customers who have experienced our services and are our frequent buyers.</p>

            <!--<p><b>Why buy research through us : </b></p>-->

            <!--<ul class="list-unstyled about-inner-list">-->
            <!--    <li><i class="fa fa-check color-green"></i>&nbsp;We deliver research from expert analysts spread across the globe</li>-->
            <!--    <li><i class="fa fa-check color-green"></i>&nbsp;We offer flexibility to buy only certain sections and tables without buying the entire report</li>-->
            <!--    <li><i class="fa fa-check color-green"></i>&nbsp;Flexibility to increase or decrease the scope of report </li>-->
            <!--    <li><i class="fa fa-check color-green"></i>&nbsp;We offer custom research if the proposed TOC does not meet your business requirement </li>-->
            <!--    <li><i class="fa fa-check color-green"></i>&nbsp;24x7 email support </li>-->
            <!--    <li><i class="fa fa-check color-green"></i>&nbsp;We offer broad as well as niche industry reports</li>-->
            <!--</ul>-->
            <p>
                For more information, write to us at <a href="mailto:sales@valuemarketresearch.com" target="_blank">sales@valuemarketresearch.com</a>
            </p>
        </div>
    </div><!--/row-->
</div><!--/container-->

<!-- CLIENT LOGO SECTION START -->
<?php if (!empty($ourclients)) { ?>
  <section id="services" class="services-section">
      <div class="container bgwhite">
          <div class="row">
              <div class="col-lg-12">
                  <!--Item slider text-->
                  <div class="">
                    <div class="row" id="slider-text">
                      <div class="header-section text-center">
                        <h2 class="">Our Clients</h2>
                        <hr class="bottom-line">
                      </div>
                    </div>
                  </div>
                  <!-- Item slider-->
                  <div class="container-fluid row">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="carousel carousel-showmanymoveone slide" id="itemslider">
                        <div class="carousel-inner carousel-inner-pay">
						<!----------------------->
                          <?php foreach ($ourclients as $key => $clients) { ?>
                            <div class="item <?= $key == 0?'active':''?> payotn">
                              <div class="col-xs-12 col-sm-2 col-md-2">
                                <?php if (!empty($clients['OurClient']['logo'])) { ?>
                                    <!--<a href="http://<?php echo $clients['OurClient']['link']; ?>" target="blank">-->
                                      <?= $this->Html->image('client_images/' . $clients['OurClient']['logo'],  array('class' => 'client_image','data-toggle'=>'tooltip','title'=>$clients['OurClient']['client_name'],'data-placement'=>'right','alt'=>$clients['OurClient']['client_name'])); ?>
                                    <!--</a>-->
                                <?php } else {
                                        echo $this->Html->image('noimage.png', array('class' => 'client_image'));
                                      } ?>
                              </div>
                            </div>
                          <?php } ?>
						  <!----------------------->
                        </div>
                        <div id="slider-control">
                        <a class="left carousel-control" href="#itemslider" data-slide="prev">
                          <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                        </a>
                        <a class="right carousel-control" href="#itemslider" data-slide="next">
                          <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
                        </a>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
                  <!-- Item slider end-->
              </div>
          </div>
      </div>
  </section>
<?php } ?>
<!-- CLIENT LOGO SECTION END -->
