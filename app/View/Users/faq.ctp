<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main bg-color-inner-report">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>Industries</h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url(array('/',true)); ?>">Home</a></li>
              <li><a>FAQ</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->

<style>.page-heading{color: #666;font-size: 14px;text-transform: uppercase;font-weight: 700;}</style>

<section class="about-inner faq-bg">
  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1 class="page-heading">FAQ</h1>
        </div>
      </div>
    <div class="row">
      <div class="col-md-12 col-xs-12 col-sm-12">
      <div class="faq-main">
        <h4>Order and Delivery Related Queries</h4>
        <h5><b>Q.1 : How can I purchase the report?</b></h5>
        <ul>
          <li>A : You can purchase the report by Selecting the report you wish to purchase. Choose from the various Licenses. Choose the mode of payment and make the payment. Once we receive the payment, we will initiate the delivery of the report. Report will be delivered within 24-48 working hours post receipt of payment.</li>
        </ul>
        <h5><b>Q.2 : How do I receive the report?</b></h5>
        <ul>
        <li>A : After receiving the payment we deliver the report within 24 -48 working hours. The report is delivered as a PDF through e-mail. We can also send the hard copy of the report at additional charges.</li>
        </ul>
        <h5><b>Q.3 : How can I order the report?</b></h5>
        <ul>
        <li>A : You can order the report by - buying directly from the site using the BUY NOW Option Or connect with our sales team <a href="mailto:sales@valuemarketresearch.com">(sales@valuemarketresearch.com)</a> who will assist you in the same.</li>
        </ul>
      </div>
    </div>

      <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="faq-main">
          <h4>Licenses and Purchase Related Queries</h4>
            <h5><b>Q.1 : What are the types of License options available for me?</b></h5>
            <ul>
              <li>A : You can opt for any of the below Licenses as per your requirement - </li>
              <li>Single User License - Single User License allows only one user to access the Report. The report will be sent electronically to the buyer as a PDF File. </li>
              <li>Multi-User License - Multi-User License allows 2-10 users to access the Report. The report will be sent electronically to the buyers as a PDF File.</li>
              <li>Enterprise User License - Enterprise User License allows access of the Report across the whole enterprise. The report will be sent electronically as a PDF File.</li>
              <li>Data Pack - This covers all data tables of the report in the excel format only.</li>
            </ul>
            <h5><b>Q.2 : Can I purchase a specific chapter/ specific region or specific table from your market research reports?</b></h5>
            <ul>
              <li>A : Yes, we can provide you specific chapters / specific regions and/or specific tables as per your research requirements. You just need to drop us a mail at <a href="mailto:sales@valuemarketresearch.com">sales@valuemarketresearch.com</a> specifying your requirements and we will assist you with your customized requirement.</li>
            </ul>
        </div>
      </div>

      <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="faq-main">
          <h4>Payment related Queries</h4>
            <h5><b>Q.1 : How can I make the payment?</b></h5>
            <ul>
              <li>A : We accept Credit Card payment, PayPal and Wire Transfer. You can get the details for the payment by dropping us a mail on <a href="mailto:sales@valuemarketresearch.com">sales@valuemarketresearch.com</a></li>
            </ul>
            <h5><b>Q.2 : What are your Terms of Payment?</b></h5>
            <ul>
              <li>A : We work on advance payment terms. As our product is a digital commodity, it is considered to be consumed the moment it is delivered. We follow a NO REFUND policy. It is therefore suggested to review the samples and mentioned TOC of the report thoroughly before purchasing the report.</li>
            </ul>
            <h5><b>Q.3 : I am unable to make the payment. What to do?</b></h5>
            <ul>
              <li>A : At times you may face issues like ‘Payment Decline’ and ‘Payment Gateway Failure ‘while making the payment. It might be due to Mismatch of billing address and payment information (Recheck the information shared)
              Credit limit issues (You may need to contact your bank to increase the credit limit) Transaction blocked by your card issuing bank (You may need to contact your bank to get the transaction approved) In case of any other payment related issues, please get in touch with us at: <a href="mailto:sales@valuemarketresearch.com">sales@valuemarketresearch.com</a></li>
            </ul>
          </div>
      </div>
    </div>
  </div>
</section>