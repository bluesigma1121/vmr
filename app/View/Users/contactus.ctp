<script type="text/javascript">
    $(document).ready(function () {
        $('#enquiry_form').bValidator();
    });
</script>


<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>Contact Us</h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a>Contact Us</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->
<style>.page-heading{color: #666;font-size: 14px;text-transform: uppercase;font-weight: 700;}</style>
<section class="about-inner">
  <div class="container">
    <h1 class="page-heading">Get In Touch</h1>
      <div class="container">
        <div class="row buy-now-form">
        <div class="col-md-6 col-sm-6 padd-left">
          <!-- <form id="contact" action="" method="post"> -->
            <?php echo $this->Form->create('User', array('id' => 'enquiry_form')); ?>
            <h4>Contact us today, and get reply within 24 hours!</h4>
            <fieldset>
              <label>Name <span>*</span></label>
              <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'data-bvalidator' => 'required,alpha')); ?>
              <!-- <input placeholder="Your name" type="text" tabindex="1"> -->
            </fieldset>
            <fieldset>
              <label>Email <span>*</span></label>
              <?php echo $this->Form->input('email', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required,email')); ?>
              <!-- <input placeholder="Your Email Address" type="email" tabindex="2"> -->
            </fieldset>
            <fieldset>
              <label>Mobile <span>*</span></label>
              <?php echo $this->Form->input('mobile', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required')); ?>
              <!-- <input placeholder="Your Phone Number" type="tel" tabindex="3"> -->
            </fieldset>
            <fieldset>
              <label>Message <span>*</span></label>
              <?php echo $this->Form->input('message', array('class' => 'form-control', 'type' => 'textarea', 'label' => false, 'data-bvalidator' => 'required,alphanum')); ?>
              <span class="text-danger">URL not allowed in message.</span>
              <!-- <textarea placeholder="Type your Message Here...." tabindex="4"></textarea> -->
            </fieldset>
            <!--<fieldset>
              <div class="col-sm-4">
                  <label>Captcha<span>*</span></label>
                  <?php echo $this->Html->image($this->Html->url(array('controller' => 'users', 'action' => 'captcha'), true), array('label' => false, 'width' => '150', 'height' => '40', 'alt' => 'captcha')); ?>
              </div>
              <div class="col-sm-4">
                  <label>Enter Captcha Code <span>*</span></label>
                  <?php
                  echo $this->Form->input('captcha', array('data-bvalidator' => 'required', 'class' => 'input-large form-control hastip', 'data-placement' => 'right', 'type' => 'text', 'placeholder' => 'Enter Captcha Code',
                      'title' => 'Enter the characters you see in the image.', 'label' => false,'autocomplete'=>'off'));
                  ?>
              </div>
            </fieldset>-->
            
        <!--Change Captcha VG-26/2/2018 -->
                <!-- <div class="g-recaptcha" data-sitekey="6LekeEYUAAAAAHPe-rEZtnpqMLcQ_TtFdWSsYsP3" data-bvalidator='required'></div> -->
            <div class="row margin-bottom-20">
                <div class="col-md-7 col-md-offset-0">
                    <div class="g-recaptcha" data-sitekey="6LfRUFIUAAAAAAr70oYLT9gPRZV0jVD8jZOTataM"></div>
                </div>
            </div>
        <!--Change Captcha VG-26/2/2018 -->            
            
            <fieldset>
              <div class="col-md-12 col-xs-12 col-sm-12 text-center">
                <br><br>
                <?php echo $this->Form->submit('Submit', array( 'id' =>'contact-submit')) ?>
              </div>
            </fieldset>
          <!-- </form> -->
          <?php $this->Form->end(); ?>
         </div>
         <!--<div class="col-md-2">-->
         <!--</div>-->
         <div class="col-md-6 col-sm-6 row">
          <div class="col-md-12 col-xs-12 col-sm-12 padd-left">
            <div class="bg-grey-contact">
                <p><i class="fa fa-home fa-sm fa-lg"></i> 401/402, TFM, Nagras Road, Aundh, Pune-7.<br> Maharashtra, INDIA.</p>
               <p><i class="fa fa-envelope fa-sm"></i> <a href="mailto:sales@valuemarketresearch.com">sales@valuemarketresearch.com</a></p>
               <p><i class="fa fa-mobile fa-sm"></i> +1-888-294-1147</p>          
              </div>
            </div>

            <!-- <div class="col-md-6 col-xs-12 col-sm-6 padd-right">
              <div class="bg-grey-contact">
              <p><i class="fa fa-home fa-sm fa-lg"></i> 2500 City West Blvd.
              Suite 300 Houston, 77042 USA.</p>
               <p><i class="fa fa-envelope fa-sm"></i> <a href="mailto:sales@valuemarketresearch.com">sales@valuemarketresearch.com</a></p>
               <p><i class="fa fa-mobile fa-sm"></i>+91 9988776655 / 9988776644</p>
              </div>
            </div> -->
              <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10119.185074069741!2d29.031845602028582!3d41.05911758161741!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6477d93f7badc778!2sTRT+World!5e0!3m2!1str!2str!4v1496302367803" width="100%" height="268" frameborder="0" style="border:0" allowfullscreen></iframe> -->
          </div>
       </div>
    </div>
  </div>
</section>
