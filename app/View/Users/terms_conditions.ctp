<style>
    .blog-inner h4{
        font-weight: normal;
        font-size: 16px;
        letter-spacing: 0;
        margin-top: 0;
        color: #333;
        text-transform: capitalize;
        font-family: Roboto;
        padding-bottom: 6px;
    }
    .blog-inner a {
        color: #0074BD;
        transition: all .5s ease-in-out;
        outline: none;
        text-decoration: none;
    }

    .blog-inner h2 {
        font-weight: normal;
        font-size: 18px;
        letter-spacing: 0;
        margin-top: 0;
        color: #337ab7;
        text-transform: capitalize;
        font-family: Roboto;
    }

</style>
<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>Terms and Conditions</h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a>Terms and Conditions</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->
<style>.page-heading{color: #666;font-size: 14px;text-transform: uppercase;font-weight: 700;}</style>

<!-- ABOUT INNER SECTION START -->
<section class="about-inner">

      <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-heading">Terms and Conditions</h1>
            </div>
        </div>
        <div class="">
          <div class="blog-inner blog-details-inner">
            <p>
                Value Market Research provides its services to you subject to the following conditions.
                If you visit or shop at Value Market Research, you accept these conditions. Please read them
                carefully. We reserve the right from time to time to make changes to these Terms and Conditions
                at our absolute discretion. The Terms and Conditions applying to each transaction with us are
                those which are available on the website at the time that you place your order and it is your
                responsibility to check the Terms and Conditions before placing an order.
            </p>
            <h2>Privacy</h2>
            <p>
                Our Privacy Notice is at the foot of these Terms and Conditions and governs your visit to
                Value Market Research. Please read it carefully so that you understand our practices.
            </p>

            <h2>Electronic Communications</h2>
            <p>
                When you visit Value Market Research or send e-mails to us, you are communicating with us
                electronically. You consent to receive communications from us electronically. We will
                communicate with you by e-mail or by posting notices on this site. You agree that all
                agreements, notices, disclosures and other communications that we provide to you electronically
                satisfy any legal requirement that such communications be in writing.
            </p>

            <h2>Copyright</h2>
            <p>
                All content included on this site, such as text, graphics, logos, button icons, images, and
                audio clips, digital downloads, data compilations, and software, is the property of
                Value Market Research or its content suppliers and protected by international copyright laws.
                The compilation of all content on this site is the exclusive property of Value Market Research
                and protected by international copyright laws. All software used on this site is the property
                of Value Market Research or its software suppliers and protected by international copyright laws.
                You agree not to copy, reproduce, duplicate, sell, resell, or exploit for any commercial purposes,
                any portion of the Site, use of the Site, or access to the Site. You may not re-use and/or extract
                part of the content of this website without Value Market Research’ express consent in writing.
                In particular, you may not utilize any data mining, robots or similar search and extraction tools
                for re-use of any substantial part of this website without Value Market Research’ express consent
                in writing. You may not use substantial parts of this website (for example product listing and
                prices) on your own website without our express consent in writing.
            </p>

            <h2>Our Contract</h2>
            <p>
                <!-- Once you place an order for a product listed on our website, payment for that product becomes due
                and owing immediately. Notwithstanding the fact that the product will not be dispatched until
                monies are received in full, you are liable to pay to Value Market Research all monies due on foot
                of the order. Once we receive your order we have to purchase the product from the publisher,
                process the payment and arrange the shipping. Because of this element of work involved, we regret
                that it is not possible for an order to be cancelled once it is submitted. Value Market Research
                will not be responsible for any delay or failure to comply with our obligations under our contract
                with you where such delay or failure arises from any cause which is beyond our reasonable control. Billing and Invoicing will be done in the name of BLUESIGMA BUSINESS CONSULTING SOLUTIONS LLP. -->
                Once you place an order for a product listed on our website, payment for that product becomes due and 
                owing immediately. Notwithstanding the fact that the product will not be dispatched until monies are 
                received in full, you are liable to pay to Value Market Research all monies due on foot of the order. 
                Once we receive your order, we will arrange the shipping. Because of this element of work involved, we 
                regret that it is not possible for an order to be cancelled once it is submitted. Value Market Research 
                will not be responsible for any delay or failure to comply with our obligations under our contract with 
                you where such delay or failure arises from any cause which is beyond our reasonable control. Billing and 
                Invoicing will be done in the name of BLUESIGMA BUSINESS CONSULTING SOLUTIONS LLP.
            </p>
            <h2>License and Site Access</h2>
            <p>
                Value Market Research grants you a limited revocable license to create a link to the home page
                of Value Market Research provided that the link does not portray Value Market Research, its
                associated companies, or its products, in a negative, defamatory, derogatory or misleading way.
                You may not use any Value Market Research logos, graphics or trademarks as part of the link without
                our express consent in writing. Access and make personal use of this site but not to download
                (other than page caching) or modify it, or any portion of it, except with the express written
                consent of Value Market Research. This license does not include any resale or commercial use of
                this site or its contents; any collection and use of any product listings, descriptions, or prices;
                any derivative use of this site or its contents; any downloading or copying of account information
                for the benefit of another merchant; or any use of data mining, robots, or similar data gathering
                and extraction tools. This site or any portion of this site may not be reproduced, duplicated, copied,
                sold, resold, visited, or otherwise exploited for any commercial purpose without express written
                consent of Value Market Research. You may not frame or utilize framing techniques to enclose any
                trademark, logo, or other proprietary information (including images, text, page layout, or form) of
                Value Market Research and our affiliates without express written consent. You may not use any Meta
                tags or any other "hidden text" utilizing Value Market Research’ name or trademarks without the
                express written consent of Value Market Research. Any unauthorized use terminates the permission or
                license granted by Value Market Research.
            </p>
            <h2>Product Descriptions</h2>
            <p>
                Value Market Research, and its affiliates attempt to be as accurate as possible. However,
                Value Market Research does not warrant that product descriptions or other content of this site
                is accurate, complete, reliable, current, or error-free.
            </p>

            <h2>Product Pricing</h2>
            <p>
                We offer you consistently low prices on all our products All prices are listed in DOLLARS. Despite our best
                efforts, a small number of the items in our catalogue may be mispriced. Rest assured, however,
                that we verify prices of products sold and shipped by Value Market Research as part of our shipping
                procedures.
                <br/>
                If an item's correct price is lower than our stated price, we charge the lower amount and ship
                you the item. If an item's correct price is higher than our stated price, we will, at our discretion,
                either contact you for instructions before shipping or cancel your order and notify you of such
                cancellation.
            </p>

            <h2>Shipping</h2>
            <p>
                Value Market Research can ship to virtually any address in the world. The majority of our products
                are dispatched electronically. Hardcopy orders, such as books, binders, newsletters, software or
                CD-ROMS may be dispatched by mail or courier. Hardcopy orders incur an additional shipping charge.
            </p>

            <h2>Delivery Time</h2>
            <p>
                When you place an order with Value Market Research, we endeavor to deliver the product as soon as
                possible. Electronic delivery is always the fastest option; the majority of electronic information
                products are dispatched within 24 hours via Email or Download. However, the exact delivery schedule
                may vary for each product and is based on whether the product is under study, published or needing
                revamp. Exact delivery schedule will be communicated to you during the purchase process or completion
                of the same. Hardcopy products are generally dispatched from the EU or the US, and delivery to some
                markets may be delayed through local carriers, and local customs. Wherever possible we encourage
                clients to order products electronically.
            </p>

            <h2>Bulk Discounts</h2>
            <p>
                We may be able to offer additional discounts on large orders of a single title or on large orders
                of many individual titles.
            </p>

            <h2>Orders/Return Policy</h2>
            <!-- <p>
                The nature of the information being sold means that Value Market Research cannot accept returns
                of products once an order has been placed.
            </p>
            <p>
              The below mentioned Refund policy is applicable to all the products - Market Research Reports, Industry Analysis, Customized Reports sold through our site - <a href="https://www.valuemarketresearch.com" target="_blank">www.valuemarketresearch.com</a>
            </p>
            <p>The nature of the services that are provided by Value Market Research are digital commodity. These services are considered to be consumed as soon as it is delivered to the buyer. The company therefore does not offer a refund on the sold products.</p>
            <p>The company solely keeps the right to determine the validity of a refund request.</p>

            <p>&nbsp;</p>  -->
            <!-- <p>
              
                As per industry standards and the nature of our product, we do not reimburse/ refund or cancel orders post payment receipt. 
Buyers are instructed to read and validate the product details and clarify all their queries over mail through sales@decisiondatabases.com before proceeding for the payment. The company therefore does not offer a refund on the sold products but hereby agrees to resolve actively all the post purchase concerns and queries at the earliest. </p>
<p>
Value Market Research solely keeps the right to determine the validity of a refund request and is not legally liable both civilly and criminally for any chargebacks and fees involved in the transaction. 
Under no circumstances we reimburse the payment, instead provide the customer with an equivalent credit amount which can be used within a year.
            </p> -->
            <p>
            As all products on Value Market Research are digital in nature, we, therefore, do not reimburse, refund, or cancel the orders once placed. Clients are requested to thoroughly go through the table of contents and understand the product specification before placing the orders.</p>
            <p>
We at Value Market Research do ensure an active after-sales query resolution but do not entertain refund requests. Value Market Research holds the sole right to validate a refund request and award the buyer with equivalent credit points, which can be consumed within a year. Value Market Research cannot be held legally liable for any chargeback or fees related to the transaction.
            </p>

            <h2>Links</h2>
            <p>
                <!-- This site may contain links to third party Web sites and material from third-party information and
                content providers.  -->
                As Value Market Research has no control over such sites and resources, you
                acknowledge and agree that Value Market Research is not responsible for the availability or
                otherwise of such external sites or resources, and does not endorse and is not responsible or
                liable for any content, advertising, products, or other materials on or available from such sites
                or resources. You further acknowledge and agree that Value Market Research shall not be responsible
                or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in
                connection with use of or reliance on any such content, goods, or services available on or through
                any such site or resource.
            </p>

            <!-- <h2>Third Party Content</h2>
            <p>
                As a distributor of content published and supplied by third parties, Value Market Research has no
                editorial control over said content. Such content expresses the opinions, the advice, the services,
                the offers, the statements, or other information of the authors, and not of Value Market Research
            </p> -->

            <h2>Indemnity</h2>
            <p>
                You agree to hold Value Market Research, and its subsidiaries, affiliates, officers, agents, co
                branders or other partners, and employees, harmless from any claim or demand, including reasonable
                legal fees, made by any third party due to or arising out of your use of the Site, your connection
                to the Site, your violation of the TOS, or your violation of any rights of another.
            </p>

            <h2>Applicable Law</h2>
            <p>
                By visiting Value Market Research, you agree that the laws of the India, without regard to
                principles of conflict of laws, will govern these Conditions of Use and any dispute of any sort that
                might arise between you and Value Market Research or its affiliates.
            </p>

            <!-- <h2>Disclaimer of Warranties and Limitation of Liability</h2>
            <p>
                THIS SITE IS PROVIDED BY Value Market Research ON AN "AS IS" AND "AS AVAILABLE" BASIS.
                Value Market Research MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, AS TO
                THE OPERATION OF THIS SITE OR THE INFORMATION, CONTENT, MATERIALS, OR PRODUCTS INCLUDED ON THIS SITE.
                YOU EXPRESSLY AGREE THAT YOUR USE OF THIS SITE IS AT YOUR SOLE RISK.
                TO THE FULL EXTENT PERMISSIBLE BY APPLICABLE LAW, VALUE MARKET RESEARCH DISCLAIMS ALL WARRANTIES,
                EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
                FOR A PARTICULAR PURPOSE. VALUE MARKET RESEARCH DOES NOT WARRANT THAT THIS SITE, ITS SERVERS, OR E-MAIL
                SENT FROM VALUE MARKET RESEARCH ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. VALUE MARKET RESEARCH
                WILL NOT BE LIABLE FOR ANY DAMAGES OF ANY KIND ARISING FROM THE USE OF THIS SITE, INCLUDING,
                BUT NOT LIMITED TO DIRECT, INDIRECT, INCIDENTAL, PUNITIVE, AND CONSEQUENTIAL DAMAGES.
                Value Market Research knows that you care how information about you is used and shared, and we
                appreciate your trust that we will do so carefully and sensibly. This notice describes our privacy policy.
                By visiting Value Market Research, you are accepting the practices described in this Privacy Notice.
            </p> -->
            <h2>What personal information about customers does Value Market Research gather?</h2>
            <p>
                The information we learn from customers helps us personalize and continually improve your
                delivery of business information.
            </p>

            <h2>Here are the types of information we gather:</h2>
            <p>
                <em>Information You Give Us:</em> We receive and store any information you enter on our Web site or give us in
                any other way. You can choose not to provide certain information, but then you might not be able to
                take advantage of many of our features. We use the information that you provide for such purposes as
                responding to your requests, customizing future shopping for you, improving our products, and
                communicating with you.
                <br/>
                <br/>
                <em>Automatic Information:</em> We receive and store certain types of information whenever you interact
                with us. For example, like many Web sites, we use "cookies", and we obtain certain types of information
                when your Web browser accesses Value Market Research.
                <br/>
                <strong><em>Note:</em></strong> Your browsing and purchasing patterns for market research information
                may be commercially sensitive. We recognize this. A number of companies offer utilities designed to
                help you visit Web sites anonymously. Although we will not be able to provide you with a personalized
                experience at Value Market Research if we cannot recognize you, we want you to be aware that these
                tools exist.
                <br/>
                <em>E-mail Communications:</em> To help us make e-mails more useful and interesting, we often receive
                a confirmation when you open e-mail from Value Market Research if your computer supports such
                capabilities. We also compare our customer list to lists received from other companies, in an effort
                to avoid sending unnecessary messages to our customers. If you do not want to receive e-mail or other
                mail from us, please adjust your Customer Communication Preferences.
                <br/>
                <em>Information from Other Sources:</em>
                We might receive information about you or your organization from other sources and add it to our
                account information.
            </p>

            <h2>What about Cookies?</h2>
            <p>
                Cookies are alphanumeric identifiers that we transfer to your computer's hard drive through your
                Web browser to enable our systems to recognize your browser and to provide additional features
                such personalized information, and storage of items in your Shopping Cart between visits.
                The Help portion of the toolbar on most browsers will tell you how to prevent your browser
                from accepting new cookies, how to have the browser notify you when you receive a new cookie,
                or how to disable cookies altogether. However, cookies allow you to take full advantage of some
                of Value Market Research’ coolest features, and we recommend that you leave them turned on.
            </p>

            <h2>How secure is information about me?</h2>
            <p>
                We protect the security of your information during transmission by using Secure Sockets Layer
                (SSL) software, which encrypts information you input. It is important for you to protect against
                unauthorized access to your password and to your computer. Be sure to sign off when finished using
                a shared computer.
            </p>

            <!-- <h2>opt-in/opt-out</h2>

            <p>If you do not want to receive e-mail or other mail from us, please visit: </p>
            <a href="#"><h2 style="text-transform:lowercase">http://www.valuemarketresearch.com/unsubscribe</h2></a>

            <p>If you wish to receive e-mail or other mail from us, please visit:</p>
            <a href="<?php echo Router::url('/', true) . 'users' . '/registration'; ?>"><h2 style="text-transform:lowercase">http://www.valuemarketresearch.com/register</h2></a> -->

        </div>
       </div>
    </div>
</section>

<!-- ABOUT INNER SECTION END -->
