<style>
  .blog-inner a {
      color: #0074BD;
      transition: all .5s ease-in-out;
      outline: none;
      text-decoration: none;
  }
</style>
<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>Terms and Conditions</h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a>Refund Policy</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->
<style>.page-heading{color: #666;font-size: 14px;text-transform: uppercase;font-weight: 700;}</style>

<!-- ABOUT INNER SECTION START -->
<section class="about-inner">

      <div class="container">
        <h1 class="page-heading">Refund Policy</h1>
        <!-- <h4>Blog Details</h4> -->
        <div class="">
          <div class="blog-inner blog-details-inner">
            <p>
              <!-- The below mentioned Refund policy is applicable to all the products - Market Research Reports, Industry Analysis, Customized Reports sold through our site - <a href="https://www.valuemarketresearch.com" target="_blank">www.valuemarketresearch.com</a> -->
              <!-- The below mentioned Refund policy is applicable to all the products - Market Research Reports, Industry Analysis, Customized Reports sold through our site - <a href="www.valuemarketresearch.com">www.valuemarketresearch.com</a> -->
                <!-- Value Market Research provides services which are digital commodity. These services are considered to be consumed as soon as it is delivered to the buyer. The company therefore does not offer a refund on the sold products. -->
            </p>
            <!-- <p>&nbsp;</p> -->
            <!-- <p>The nature of the services that are provided by Value Market Research are digital commodity. These services are considered to be consumed as soon as it is delivered to the buyer. The company therefore does not offer a refund on the sold products.</p> -->
            <!-- <p>&nbsp;</p> -->
            <!-- <p>
            As per industry standards and the nature of our product, we do not reimburse/ refund or cancel orders post payment receipt. 
Buyers are instructed to read and validate the product details and clarify all their queries over mail through sales@decisiondatabases.com before proceeding for the payment. The company therefore does not offer a refund on the sold products but hereby agrees to resolve actively all the post purchase concerns and queries at the earliest.</p>
<p> 
Value Market Research solely keeps the right to determine the validity of a refund request and is not legally liable both civilly and criminally for any chargebacks and fees involved in the transaction. 
Under no circumstances we reimburse the payment, instead provide the customer with an equivalent credit amount which can be used within a year.
           </p> -->
           <p>
            As all products on Value Market Research are digital in nature, we, therefore, do not reimburse, refund, or cancel the orders once placed. Clients are requested to thoroughly go through the table of contents and understand the product specification before placing the orders.</p>
            <p>
We at Value Market Research do ensure an active after-sales query resolution but do not entertain refund requests. Value Market Research holds the sole right to validate a refund request and award the buyer with equivalent credit points, which can be consumed within a year. Value Market Research cannot be held legally liable for any chargeback or fees related to the transaction.
            </p>
            <!-- <p>The company solely keeps the right to determine the validity of a refund request.</p> -->
            <!-- <h1>Disclaimer</h1>
            <ul style="list-style: square;">
              <li>
                <p>Value Market Research is not responsible for any inaccurate information provided in our reports. As this information is collected based on interviews fluctuations and diversion of data is possible, though we ensure regular checks and data validation through secondary sources.</p>
              </li>
              <li>
                <p>Reports and data purchased through us is for personal or company use only. Buyer is not authorized to disclose our data analysis to third party or any other publications.</p>
              </li>
              <li>
                <p>Disclosing, lending, circulating, reselling, recording, photocopying, mechanical/electronically reproduction of any table/ section/ chapter of the Value Market Research report or services without written permission are prohibited.</p>
              </li>
              <li>
                <p>Decisions made by the clients based on the report analysis are solely their own responsibility. Value Market Research holds no liability for those decisions.</p>
              </li>                            
            </ul>  
           <p>&nbsp;</p> 
            <p>
              For permissions or any other query please contact us at <a href="mailto:sales@valuemarketresearch.com">sales@valuemarketresearch.com.</a>
            </p> -->
        </div>
       </div>
    </div>
</section>

<!-- ABOUT INNER SECTION END -->
