<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>Terms and Conditions</h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a>Research Methodology</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->

<!-- ABOUT INNER SECTION START -->
<section class="about-inner">

      <div class="container">
        <!-- <h4>Blog Details</h4> -->
        <div class="">
          <div class="blog-inner blog-details-inner">
            <p>
            This report is equipped with in-depth interviews and discussions with a wide range of key industry participants and opinion leaders. This report is produced by gathering information on the basis of primary and secondary research. Furthermore, the report contains analysis of the market on the basis of Porter’s Five Forces Model, it is a strategic tool designed to give global view which helps to understand industry structure and evaluates the competition intensity attractiveness and profitability in the market by considering factors such as bargaining power of buyers and suppliers, the threat from substitute products and the threat from new entrants
            </p><br>
            <h1>TOP-DOWN APPROACH</h1>
            <p>
              In the top-down approach, the global market was further bifurcated to gain insight into market segments on the basis of the percentage share of each segment.
            </p>
            <p>
              This approach helped in arriving at the market size of each segment globally. The segments market size was further broken down in the regional market size of each segment and sub-segments. The sub-segments were further broken down to country level market. The market size arrived using this approach was then cross-checked with the market size arrived by using a bottom-up approach.
            </p><br>
            <h1>BOTTOM-UP APPROACH</h1>
            <p>
              In the bottom-up approach, we arrived at the country market size by identifying the market size and market shares of the key market players. The country market sizes then were added up to arrive at the regional market size, which eventually added up to arrive at the global market size.
            </p>
            <p>
              Research report helps to keep marketer relevant and future-oriented, also improves decision-making capabilities and reduce risk. It is an excellent way to find the best market or target demographic for a product or service. Furthermore, the research report also helps to understand the comprehensive profiles of the key players in the market and in-depth view of the competitive landscape worldwide.  Additionally, the data obtained from the company sources and the primary respondents were validated through secondary sources including government publications and others sources.
            </p>
        </div>
       </div>
    </div>
</section>

<!-- ABOUT INNER SECTION END -->
