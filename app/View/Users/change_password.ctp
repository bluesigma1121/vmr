<script type="text/javascript">
    $(document).ready(function() {
        $('#Change_password').bValidator();
    });
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Change Password</h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('User', array('id' => 'Change_password', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-2">
            <div class="form-group"><label class="col-lg-3 control-label">Current Password</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('cuurent_pwd', array('type' => 'password', 'class' => 'form-control hastip', 'placeholder' => 'Enter Current Password', 'label' => false, 'title' => 'Enter Current Password', 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-3 control-label">New Password</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('new_password', array('type' => 'password', 'class' => 'form-control hastip', 'placeholder' => 'Enter New password ', 'label' => false, 'title' => 'Enter New password', 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-3 control-label">Confirm Password</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('Confirm_password', array('type' => 'password', 'class' => 'form-control hastip', 'placeholder' => 'Enter Confirm Password', 'label' => false, 'title' => 'Enter Confirm Password', 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Change'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
