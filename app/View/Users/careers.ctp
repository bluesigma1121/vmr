<style>
.panel-heading {
    font-size: 18px;
    text-align: center;
    padding: 15px;
}
</style>
<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a>Career</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->

<div class="container content">		
    <div class="row margin-bottom-30">
        <div class="col-md-12 mb-margin-bottom-30">
            <div class="headline text-center">
                <h1>Come Grow With Us!!!</h1><br>
                <p>We at Value Market Research respect commitment and passion. High commitment showcases the enthusiasm one has towards work, and an enthusiastic employee is a treasure to any company. Passion gives an extra push to accomplish difficult tasks, tasks which require more thinking, more effort..</p>
                <p>Thus a team of committed and passionate employees is <b>What We Are Today!!!</b></p>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>Job Opening</h3><hr>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">Market Research Internship</div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">Market Research Executive</div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">Technology Assistant (Freelancer)</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <br>
            <p><strong>Share your CV with us at <a href="mailto:hr@valuemarketresearch.com">hr@valuemarketresearch.com</a></strong></p>
            <br>
            <br>
        </div>
    </div>
</div>