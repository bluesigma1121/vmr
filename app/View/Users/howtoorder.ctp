<style>
.blog-inner a{
  color:#337ab7 !important;
}
.how-order-bg{
  background: #f9f9f9;
}
.how-order-bg p{
  padding-bottom: 10px;
}
.how-order-bg ul {
    padding-left: 45px;
    margin-bottom: 25px;
}
.how-order-bg ul li p {
    color: #333 !important;
    font-size: 13px;
    font-weight: bold;
    padding-bottom: 0px;
}
.blog-details-inner h5 {
    border-bottom: 1px solid #eee;
    padding-bottom: 10px;
}
.blog-inner h5 {
    font-weight: normal;
    font-size: 18px;
    letter-spacing: 0;
    margin-top: 0;
    color: #337ab7;
    text-transform: capitalize;
    font-family: Roboto;
}
</style>
<style>.page-heading{color: #666;font-size: 14px;text-transform: uppercase;font-weight: 700;}</style>

<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>Terms and Conditions</h3>-->
        <!--</div>-->
      
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a>How to Order</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->

<!-- ABOUT INNER SECTION START -->
<section class="about-inner">

      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="page-heading">How to Order</h1>
          </div>
        </div>
        <!-- <h4>Blog Details</h4> -->
        <div class="">
          <div class="blog-inner blog-details-inner how-order-bg">
            <p>
            At Value Market Research we accept orders through website and e-mails sent to <a href="mailto:sales@valuemarketresearch.com">sales@valuemarketresearch.com</a>
            </p>
            <p>Each report page is equipped with a buy now link. Buyer has the option of making the payment via following options -</p>
          
            <ul style="list-style: square;">
              <li>
                <p>Credit Card/PayPal - Payment link is accessible on each report page.We accept payment via Master Card/ VISA/ American Express We can send payment links via e-mail also.</p>
              </li>
              <li>
                <p>Wire Transfer/Against Invoice - We accept payment against invoice also. Please drop a mail to <a href="mailto:sales@valuemarketresearch.com">sales@valuemarketresearch.com</a> and we will share the invoice with our Bank Details.</p>
              </li>
            </ul>  
            
            <p>After we receive the order we can deliver the electronic copy of the report (PDF) within 24 to 48 working hours. In case of customised reports delivery time may vary.</p>
        </div>
       </div>
    </div>
</section>
<!-- <form style="text-align:center;"><script src="https://checkout.razorpay.com/v1/payment-button.js" data-payment_button_id="pl_J1U2fTmpxzq7Rr" async> </script> </form> -->
<form style="text-align:center;"><button><a href="https://pages.razorpay.com/vmrpayment" target="_blank">Make Payment</a></button></form>


<!-- ABOUT INNER SECTION END -->
