<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>Login</h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a>Login</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->

<section class="about-inner">
  <div class="container">
    <!-- <h4>Login</h4> -->
      <div class="container">
        <div class="row buy-now-form">
        <div class="col-md-4 padd-left">
          <?php echo $this->Form->create('User', array('id' => 'enquiry_form', 'class' => 'reg-page', 'type' => 'file')); ?>
            <fieldset>
              <div class="input-group ">
                  <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                  <?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => false, 'type' => 'email', 'placeholder' => 'Email')); ?>
              </div>
            </fieldset>
            <fieldset>
              <div class="input-group ">
                  <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                  <?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => false, 'type' => 'password', 'placeholder' => 'Password')) ?>
              </div>
            </fieldset>
            <fieldset>
              <div class="">
                  <div class="text-center">
                      <a href="<?php echo Router::url(array('action' => 'forgot_password')) ?>">Forgot password?
                      </a>
                  </div>
              </div>
            </fieldset>

            <fieldset>
              <div class="col-md-12 col-xs-12 col-sm-12 text-center">
                <br><br>
                <?php echo $this->Form->submit('Login', array( 'id' =>'contact-submit')) ?>
              </div>
            </fieldset>
          <!-- </form> -->
          <?php $this->Form->end(); ?>
         </div>

         <div class="col-md-8">
          <div class="col-md-12 col-xs-12 col-sm-12 padd-left">
            <div class="bg-grey-contact text-center">
                <h4>Don't have account? Get registered in simple one step.</h4>
                <a class="btn btn-block btn-windows-inversed rounded" href="<?php echo Router::url(array('action' => 'registration')) ?>"> <i class="fa fa-chevron-right"></i> Register Here
                </a>
              </div>
            </div>
          </div>
       </div>
    </div>
  </div>
</section>
