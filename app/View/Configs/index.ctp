<script type="text/javascript">
    $(document).ready(function() {
        $('#config_form').bValidator();
    });
</script>
<style>
    .mtp{
        margin-top: 10px;
    }
    .search_mtp{
        margin-top: 3px;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Config</h5>
            </div>
            <div class="ibox-content">
            <?php echo $this->Form->create('Config', array('id' => 'config_form', 'class' => 'form-horizontal')); ?>
            <div class="row col-lg-offset-2">
                <div class="form-group">
                    <label class="col-md-3 control-label">Smtp Transport:</label>
                    <div class="col-md-4">
                        <?php echo $this->Form->input('smtp_transport', array('type' => 'text', 'data-bvalidator' => 'required', 'class' => 'form-control', 'label' => false, 'value'=> $this->requestAction('/configs/get_config_settings/smtp_transport'))); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Smtp From Email:</label>
                    <div class="col-md-4">
                        <?php echo $this->Form->input('smtp_from_email', array('type' => 'text', 'data-bvalidator' => 'required', 'class' => 'form-control', 'label' => false, 'value'=> $this->requestAction('/configs/get_config_settings/smtp_from_email'))); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Smtp From Name:</label>
                    <div class="col-md-4">
                        <?php echo $this->Form->input('smtp_from_name', array('type' => 'text', 'data-bvalidator' => 'required', 'class' => 'form-control', 'label' => false, 'value'=> $this->requestAction('/configs/get_config_settings/smtp_from_name'))); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Smtp Host:</label>
                    <div class="col-md-4">
                        <?php echo $this->Form->input('smtp_host', array('type' => 'text', 'data-bvalidator' => 'required', 'class' => 'form-control', 'label' => false, 'value'=> $this->requestAction('/configs/get_config_settings/smtp_host'))); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Smtp Port:</label>
                    <div class="col-md-4">
                        <?php echo $this->Form->input('smtp_port', array('type' => 'text', 'data-bvalidator' => 'required,number', 'class' => 'form-control', 'label' => false, 'value'=> $this->requestAction('/configs/get_config_settings/smtp_port'))); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Smtp Username:</label>
                    <div class="col-md-4">
                        <?php echo $this->Form->input('smtp_username', array('type' => 'text', 'data-bvalidator' => 'required', 'class' => 'form-control', 'label' => false, 'value'=> $this->requestAction('/configs/get_config_settings/smtp_username'))); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Smtp Password:</label>                    
                    <div class="col-md-4">
                        <?php echo $this->Form->input('smtp_password', array('type' => 'text', 'data-bvalidator' => 'required', 'class' => 'form-control', 'label' => false, 'value'=> $this->requestAction('/configs/get_config_settings/smtp_password'))); ?>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-primary btn-sm', 'div' => false)); ?>
            </div>
            <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>