<style>
    .mtp{
        margin-top: 10px;
    }
    .search_mtp{
        margin-top: 3px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('#selecctall').click(function (event) {  //on click 
            if (this.checked) { // check select status
                $('.checkbox1').each(function () { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"               
                });
            } else {
                $('.checkbox1').each(function () { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                });
            }
        });
        $('.btn_bulk_active').click(function () {
            var usr_str = '';
            $("#product_index_tbl tr").each(function (i, row) {
                if ($(this).find("td:first").find('input:checked').prop('checked') == true) {
                    var val = $(this).find("td:first").find('input:checked').val();
                    if (usr_str != '') {
                        usr_str = usr_str + "," + val;
                    }
                    else {
                        usr_str = val;
                    }
                }
            });
            if (usr_str == '') {
                alert('Please select checkbox to perform action.');
                return false;
            }
            else {
                $("#selected_usr_field").val(usr_str);
            }
        });


        $('#prnt_of_chld1').multiselect({
            selectedList: 10,
            multiple: false
        }).multiselectfilter();



        var i = 1;
        $('#prnt_of_chld1').change(function () {
            $("#div2").hide();
            $("#prnt_of_chld2").prop('disabled', true);
            $("#div3").hide();
            $("#prnt_of_chld3").prop('disabled', true);
            $("#div4").hide();
            $("#prnt_of_chld4").prop('disabled', true);
            $("#div5").hide();
            $("#prnt_of_chld5").prop('disabled', true);
            $("#div6").hide();
            $("#prnt_of_chld6").prop('disabled', true);
            var id = $('#prnt_of_chld1').val();
            var lname = '';
            var lname = $("#prnt_of_chld1 option:selected").text();
            $('#parent_cat_id_per').val(id);
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "categories/dropdwn_fill/" + id;
                $.ajax({
                    url: urls,
                    dataType: 'json'
                }).done(function (data) {
                    if (data.length != 0) {
                        $("#div2").show();
                        $("#prnt_of_chld2").prop('disabled', false);
                        $("#prnt_of_chld2").empty();
                        $("#prnt_of_chld2").append(
                                $("<option></option>").text('--SubCategory--').val('')
                                );
                        $.each(data, function (i, val) {
                            $("#prnt_of_chld2").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                        $('#prnt_of_chld2').multiselect({
                            selectedList: 10,
                            multiple: false
                        }).multiselectfilter();
                    }
                    else {
                        $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" value="' + id + '" type="hidden" id="cat_hidden' + i + '">');
                        $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
                        i++;
                        $("#div2").hide();
                        $("#prnt_of_chld2").prop('disabled', true);
                        $("#div3").hide();
                        $("#prnt_of_chld3").prop('disabled', true);
                        $("#div4").hide();
                        $("#prnt_of_chld4").prop('disabled', true);
                    }
                });
            }
            else {
                $("#div2").hide();
                $("#prnt_of_chld2").prop('disabled', true);
                $("#prnt_of_chld2").empty();
                $("#prnt_of_chld2").append(
                        $("<option></option>").text('--Select--').val('')
                        );
            }
        });

        $('#prnt_of_chld2').change(function () {
            $("#div3").hide();
            $("#prnt_of_chld3").prop('disabled', true);
            $("#div4").hide();
            $("#prnt_of_chld4").prop('disabled', true);
            $("#div5").hide();
            $("#prnt_of_chld5").prop('disabled', true);
            $("#div6").hide();
            $("#prnt_of_chld6").prop('disabled', true);
            var id = $('#prnt_of_chld2').val();
            var lname = '';
            var lname = $("#prnt_of_chld2 option:selected").text();
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "categories/dropdwn_fill/" + id;
                $.ajax({
                    url: urls,
                    dataType: 'json'
                }).done(function (data) {
                    if (data.length != 0) {
                        $("#div3").show();
                        $("#prnt_of_chld3").prop('disabled', false);
                        $("#prnt_of_chld3").empty();
                        $("#prnt_of_chld3").append(
                                $("<option></option>").text('--SubCategory--').val('')
                                );
                        $.each(data, function (i, val) {
                            $("#prnt_of_chld3").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                        $('#prnt_of_chld3').multiselect({
                            selectedList: 10,
                            multiple: false
                        }).multiselectfilter();

                    }
                    else {
                        $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" value="' + id + '" type="hidden" id="cat_hidden' + i + '">');
                        $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');">&nbsp&nbsp</div>');
                        i++;
                        $("#div3").hide();
                        $("#prnt_of_chld3").prop('disabled', true);
                        $("#div4").hide();
                        $("#prnt_of_chld4").prop('disabled', true);
                    }
                });
            }
            else {
                $("#div3").hide();
                $("#prnt_of_chld3").prop('disabled', true);
                $("#prnt_of_chld3").empty();
            }
        });



        $('#prnt_of_chld3').change(function () {
            $("#div4").hide();
            $("#prnt_of_chld4").prop('disabled', true);
            $("#div5").hide();
            $("#prnt_of_chld5").prop('disabled', true);
            $("#div6").hide();
            $("#prnt_of_chld6").prop('disabled', true);
            var id = $('#prnt_of_chld3').val();
            var lname = '';
            var lname = $("#prnt_of_chld3 option:selected").text();
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "categories/dropdwn_fill/" + id;
                $.ajax({
                    url: urls,
                    dataType: 'json'
                }).done(function (data) {
                    if (data.length != 0) {
                        $("#div4").show();
                        $("#prnt_of_chld4").prop('disabled', false);
                        $("#prnt_of_chld4").empty();
                        $("#prnt_of_chld4").append(
                                $("<option></option>").text('--SubCategory--').val('')
                                );
                        $.each(data, function (i, val) {
                            $("#prnt_of_chld4").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                        $('#prnt_of_chld4').multiselect({
                            selectedList: 10,
                            multiple: false
                        }).multiselectfilter();
                    }
                    else {
                        $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" type="hidden" value="' + id + '" id="cat_hidden' + i + '">');
                        $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
                        i++;
                        $("#div4").hide();
                        $("#prnt_of_chld4").prop('disabled', true);
                    }
                });
            }
            else {
                $("#div4").hide();
                $("#prnt_of_chld4").prop('disabled', true);
                $("#prnt_of_chld4").empty();
                $("#prnt_of_chld4").append(
                        $("<option></option>").text('--Select--').val('')
                        );
            }
        });
        $('#prnt_of_chld4').change(function () {
            $("#div5").hide();
            $("#prnt_of_chld5").prop('disabled', true);
            $("#div6").hide();
            $("#prnt_of_chld6").prop('disabled', true);
            var id = $('#prnt_of_chld4').val();
            var lname = '';
            var lname = $("#prnt_of_chld4 option:selected").text();
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "categories/dropdwn_fill/" + id;
                $.ajax({
                    url: urls,
                    dataType: 'json'
                }).done(function (data) {
                    if (data.length != 0) {
                        $("#div5").show();
                        $("#prnt_of_chld5").prop('disabled', false);
                        $("#prnt_of_chld5").empty();
                        $("#prnt_of_chld5").append(
                                $("<option></option>").text('--SubCategory--').val('')
                                );
                        $.each(data, function (i, val) {
                            $("#prnt_of_chld5").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                        $('#prnt_of_chld5').multiselect({
                            selectedList: 10,
                            multiple: false
                        }).multiselectfilter();
                    }
                    else {
                        $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" type="hidden" value="' + id + '" id="cat_hidden' + i + '">');
                        $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
                        i++;
                        $("#div5").hide();
                        $("#prnt_of_chld5").prop('disabled', true);
                    }
                });
            }
            else {
                $("#div5").hide();
                $("#prnt_of_chld5").prop('disabled', true);
                $("#prnt_of_chld5").empty();
                $("#prnt_of_chld5").append(
                        $("<option></option>").text('--Select--').val('')
                        );
            }
        });
        $('#prnt_of_chld5').change(function () {
            $("#div6").hide();
            $("#prnt_of_chld6").prop('disabled', true);

            var id = $('#prnt_of_chld5').val();
            var lname = '';
            var lname = $("#prnt_of_chld5 option:selected").text();
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "categories/dropdwn_fill/" + id;
                $.ajax({
                    url: urls,
                    dataType: 'json'
                }).done(function (data) {
                    if (data.length != 0) {
                        $("#div6").show();
                        $("#prnt_of_chld6").prop('disabled', false);
                        $("#prnt_of_chld6").empty();
                        $("#prnt_of_chld6").append(
                                $("<option></option>").text('--SubCategory--').val('')
                                );
                        $.each(data, function (i, val) {
                            $("#prnt_of_chld6").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                        $('#prnt_of_chld6').multiselect({
                            selectedList: 10,
                            multiple: false
                        }).multiselectfilter();
                    }
                    else {
                        $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" type="hidden" value="' + id + '" id="cat_hidden' + i + '">');
                        $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
                        i++;
                        $("#div6").hide();
                        $("#prnt_of_chld6").prop('disabled', true);
                    }
                });
            }
            else {
                $("#div6").hide();
                $("#prnt_of_chld6").prop('disabled', true);
                $("#prnt_of_chld6").empty();
                $("#prnt_of_chld6").append(
                        $("<option></option>").text('--Select--').val('')
                        );
            }
        });

        $('#prnt_of_chld6').change(function () {
            var id = $('#prnt_of_chld6').val();
            if (id != '') {
                var lname = '';
                var lname = $("#prnt_of_chld6 option:selected").text();
                $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" type="hidden" value="' + id + '" id="cat_hidden' + i + '">');
                $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
                i++;
            }
        });

    });
</script>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <?php /* <div class="pull-right">
                  <?php echo $this->Html->link(__('Add Product'), array('action' => 'add'), array('class' => 'btn   btn-outline btn-sm btn-warning dim', 'title' => 'Add Product')); ?>
                  <?php echo $this->Html->link(__('Import Excel'), array('action' => 'upload_products_excel'), array('class' => 'btn   btn-outline btn-sm btn-primary dim', 'title' => 'Import Product Excel')); ?>
                  </div> */ ?>
                <h5>Dashboard</h5> 
            </div>
            <div class="ibox-content">
                <?php echo $this->Form->create('Product', array('id' => 'index', 'class' => 'form-horizontal')); ?>
                <?php /*  <div class="row">
                  <div class="col-lg-2">
                  <label for="form-field-8">Product Name</label>
                  </div>
                  <div class="col-lg-2">
                  <?php echo $this->Form->input('search_text', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Enter Text For Search', 'label' => false)); ?>
                  </div>
                  <div class="col-lg-1">
                  <label for="form-field-8">Price</label>
                  </div>
                  <div class="col-lg-2">
                  <?php echo $this->Form->input('price_search', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Price', 'label' => false)); ?>
                  </div>
                  <div class="col-lg-1">
                  <label for="form-field-8">Status</label>
                  </div>
                  <div class="col-lg-2">
                  <?php
                  echo $this->Form->input('product_status', array(
                  'options' => $product_status,
                  'class' => 'form-control', 'label' => false, 'empty' => 'All', 'data-bvalidator' => 'required'));
                  ?>
                  </div>
                  </div>


                  <div class="col-lg-1">
                  <label for="form-field-8">Select Parent Category</label>
                  </div>
                  <div class="col-lg-2">
                  <?php
                  echo $this->Form->input('parent_category_id1', array(
                  'options' => $cat,
                  'empty' => '--Category Name--', 'class' => 'form-control hastip cat', 'id' => 'prnt_of_chld1', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                  ?>
                  </div> */ ?>


                <div class="row">
                    <div class="col-lg-2">
                        <label for="form-field-8">Product Name : </label>
                        <?php echo $this->Form->input('search_text', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Enter Text For Search', 'label' => false)); ?>
                    </div>
                    <div class="col-lg-2">
                        <label for="form-field-8">Price</label>
                        <?php echo $this->Form->input('price_search', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Price', 'label' => false)); ?>
                    </div>
                    <div class="col-lg-2">
                        <label for="form-field-8">Status</label>

                        <?php
                        echo $this->Form->input('product_status', array(
                            'options' => $product_status,
                            'class' => 'form-control', 'label' => false, 'empty' => 'All', 'data-bvalidator' => 'required'));
                        ?>
                    </div>

                    <div class="col-lg-2" id="main_div">
                        <label for="form-field-8">Select Parent Category</label>
                        <?php
                        echo $this->Form->input('parent_category_id1', array(
                            'options' => $cat,
                            'empty' => '--Category Name--', 'class' => 'form-control hastip cat', 'id' => 'prnt_of_chld1', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                        ?>
                    </div>

                </div>


                <div class="row mtp">
                    <div class="col-lg-1 col-lg-offset-2 search_mtp">
                        <?php echo $this->Form->button('Search', array('type' => 'submit', 'class' => 'btn btn-outline btn-sm btn-primary fa fa-search')); ?>
                    </div>
                    <div class="col-lg-1  search_mtp ">
                        <?php echo $this->Html->link(__(' Clear Fliter'), array('action' => 'clear_product_filter_pending'), array('class' => 'btn btn-outline btn-sm btn-warning fa fa-refresh', 'escape' => false, 'title' => 'Clear')); ?>
                    </div>
                </div>
                <?php echo $this->form->end(); ?>
                <?php if (in_array(AuthComponent::user('role'), array(1, 2, 3))) { ?>
                    <div class="row mtp">
                        <div class="col-lg-12">
                            <?php echo $this->Form->create('Product', array('action' => 'change_status_selected', 'class' => 'form-inline', 'id' => 'bulk_actions')); ?>
                            <div class="col-lg-2">Bulk Action:
                                <?php echo $this->Form->input('selected_products', array('type' => 'hidden', 'id' => 'selected_usr_field')); ?>
                            </div>
                            <div class="col-lg-2">
                                <?php echo $this->Form->submit('Activate Selected', array('name' => 'activate', 'class' => 'btn   btn-outline btn-sm btn-primary dim tooltip-f btn_bulk_active', 'title' => 'Activate Selected')); ?>
                            </div>
                            <?php /*   <div class="col-lg-2">
                              <?php echo $this->Form->submit('Delete Selected', array('name' => 'delete', 'class' => 'btn   btn-outline btn-sm btn-danger dim tooltip-f btn_bulk_active', 'title' => 'Delete Selected')); ?>
                              </div> */ ?>
                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                <?php } ?>
                <hr>
                <div class="table-responsive mtp">
                    <table class="table table-bordered table-striped" id="product_index_tbl">
                        <thead>
                            <tr> 
                                <th style="width: 10px;">
                                    <input type="checkbox" id="selecctall"> All
                                </th>
                                <th><?php echo $this->Paginator->sort('product_name'); ?></th>
                                <th><?php echo $this->Paginator->sort('publisher_name'); ?></th>
                                <th><?php echo $this->Paginator->sort('price', 'price ($)'); ?></th>
                                <th><?php echo $this->Paginator->sort('is_active'); ?></th>
                                <th><?php echo $this->Paginator->sort('slug'); ?></th>
                                <th>cat. count</th>
                                <th>Desc.words count</th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead> 
                        <?php
                        foreach ($products as $product) {
                            ?>
                            <tbody>
                            <td><input type="checkbox" class="checkbox1" value="<?= $product['Product']['id'] ?>" ></td>
                            <td><?php echo h($product['Product']['product_name']); ?>&nbsp;</td>
                            <td><?php echo h($product['Product']['publisher_name']); ?>&nbsp;</td>
                            <td><?php echo h($product['Product']['price']); ?>&nbsp;</td>
                            <td><?php echo h($product_status[$product['Product']['is_active']]); ?>&nbsp;</td>
                            <td><?php echo h($product['Product']['slug']); ?>&nbsp;</td>
                            <td><?php echo h(count($product['ProductCategory'])); ?>&nbsp;</td>
                            <td><?php echo str_word_count($product['Product']['product_description']); ?>&nbsp;</td>
                            <td class="actions">
                                <?php echo $this->Html->link(__('View'), array('action' => 'view', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-primary ', 'title' => 'View')); ?>
                                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-warning ', 'title' => 'Edit')); ?>
                                <?php
                                if (AuthComponent::user('role') == 1) {
                                    echo $this->Html->link(__('Pending'), array('action' => 'deactive', $product['Product']['id'], 'products_ready_active'), array('class' => 'btn   btn-outline btn-sm btn-info', 'title' => 'Mark Pending'));
                                    echo $this->Html->link(__('Activate'), array('action' => 'active', $product['Product']['id'], 'products_ready_active'), array('class' => 'btn   btn-outline btn-sm btn-info', 'title' => 'Activate'));
                                }
                                ?>

                            </td>
                            </tbody>
                        <?php } ?>
                    </table>
                    <div class="row col-md-12">
                        <div class="dataTables_paginate paging_bootstrap">
                            <p>
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?> </p>
                            <ul class="pagination" style="visibility: visible;">
                                <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
                                <li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
                                <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>