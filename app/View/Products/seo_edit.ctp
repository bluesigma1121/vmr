<style>
    .row_btn{
        padding: 5px;
    }
</style>

<div class="row">
    <div class="col-lg-9">
        <div class="wrapper wrapper-content animated fadeInUp">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="m-b-md">
                                <h2><?= $product['Product']['product_name'] ?></h2>
                            </div>
                        </div>
                    </div>
                    <?php echo $this->Form->create('Product', array('id' => 'add_product', 'class' => 'form-horizontal', 'type' => 'file')); ?>
                    <div class="row">
                        <dl class="dl-horizontal">
                            <dt>Title:</dt> 
                            <dd> <?php echo $this->Form->input('meta_name', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Meta title ', 'label' => false, 'data-bvalidator' => 'required')); ?></dd>
                        </dl>
                    </div>

                    <div class="row">
                        <dl class="dl-horizontal">
                            <dt>Meta Keywords:</dt> 
                            <dd> <?php echo $this->Form->input('meta_keywords', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Meta Keywords ', 'label' => false, 'data-bvalidator' => 'required')); ?></dd>
                        </dl>
                    </div>
                    <div class="row">
                        <dl class="dl-horizontal">
                            <dt>Meta Description:</dt> 
                            <dd> <?php echo $this->Form->input('meta_desc', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Meta Description ', 'label' => false, 'data-bvalidator' => 'required')); ?></dd>
                        </dl>
                    </div>

                    <div class="row">
                        <dl class="dl-horizontal">
                            <dt>Slug:</dt> 
                            <dd> <?php echo $this->Form->input('slug', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Slug', 'label' => false, 'data-bvalidator' => 'required')); ?></dd>
                        </dl>
                    </div>
                    
                    <?php echo $this->Form->input('id'); ?>
                    <div class="text-center">
                        <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?> 
                    </div>
                    <?php echo $this->Form->end(); ?> 
                    <div class="row">
                        <div class="col-lg-12">
                            <dl class="dl-horizontal">
                                <dt>is Active?:</dt> <dd><span class="label label-primary"><?= $yes_no[$product['Product']['is_active']] ?></span></dd>
                            </dl>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <dl class="dl-horizontal">

                                <dt>Product No. :</dt> <dd><?= $product['Product']['product_no'] ?></dd>
                                <dt>Primary Category :</dt> <dd><?= $product['Category']['category_name'] ?></dd>
                                <dt>Price ($):</dt> <dd><a href="#" class="text-navy"> <?= $product['Product']['price'] ?></a> </dd>
                            </dl>
                        </div>
                        <div class="col-lg-7" id="cluster_info">
                            <dl class="dl-horizontal" >
                                <dt>Last Updated:</dt> <dd><?= date('d-M-Y h:i A', strtotime($product['Product']['modified'])) ?></dd>
                                <dt>Created:</dt> <dd><?= date('d-M-Y h:i A', strtotime($product['Product']['created'])) ?></dd>
                            </dl>
                        </div>
                    </div>
                    <div class="row">
                        <dl class="dl-horizontal">
                            <dt>Description:</dt> 
                            <dd><?= $product['Product']['product_description']; ?></dd>
                        </dl>
                    </div>

                    <div class="row">
                        <dl class="dl-horizontal">
                            <dt>Table Of Content:</dt> 
                            <dd><?= $product['Product']['product_specification']; ?></dd>
                        </dl>
                    </div> 
                </div>
            </div>

        </div>
    </div>
    <div class="col-lg-3">
        <div class="wrapper wrapper-content project-manager">
            <h4>Product Image</h4>
            <?php
            if (!empty($product['Product']['product_image'])) {
                echo $this->Html->image('product_images/' . $product['Product']['product_image'], array('class' => 'img-responsive'));
            } else {
                echo $this->Html->image('noimage.png', array('class' => 'img-responsive'));
            }
            ?>
            <?php if (!empty($product_categories)) { ?>
                <h5>Other Categories</h5>
                <ul class="tag-list" style="padding: 0">
                    <?php foreach ($product_categories as $pc): ?>
                        <li><a href="#"><i class="fa fa-tag"></i> <?= $pc ?></a></li>
                    <?php endforeach; ?>
                </ul>
            <?php } ?>
            <?php if (!empty($product_countries)) { ?>
                <h5>Countries</h5>
                <ul class="tag-list" style="padding: 0">
                    <?php foreach ($product_countries as $pc): ?>
                        <li><a href="#"><i class="fa fa-tag"></i> <?= $pc ?></a></li>
                    <?php endforeach; ?>
                </ul>
            <?php } ?> 
            <?php if (!empty($product_specifications)) { ?>
                <h5>Specifications & Options</h5>
                <ul class="tag-list" style="padding: 0">
                    <?php foreach ($product_specifications as $key => $pc): ?>
                        <li>
                            <a href="#"><i class="fa fa-anchor"></i> <?= $key ?></a>
                            <?php if (is_array($pc) && !empty($pc)): ?>
                                <ul>
                                    <?php foreach ($pc as $op): ?>
                                        <a href="#"><i class="fa fa-arrow-right"></i> <?= $op ?></a>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php } ?>
        </div>
    </div>
</div>