<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Campaign Image Browser</title>

        <!-- jQuery and jQuery UI (REQUIRED) -->
        <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/smoothness/jquery-ui.css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

        <!-- elFinder CSS (REQUIRED) -->
        <?= $this->Html->script('../packages/elfinder/js/elfinder.min') ?>
        <?= $this->Html->css('../packages/elfinder/css/elfinder.min') ?>
        <?= $this->Html->css('../packages/elfinder/css/theme') ?>

        <!-- elFinder initialization (REQUIRED) -->
        <script>
            var base_url = '<?= $this->Html->Url('/',true) ?>';
        </script>
        <script type="text/javascript" charset="utf-8">
            function getUrlParam(paramName) {
                var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i');
                var match = window.location.search.match(reParam);

                return (match && match.length > 1) ? match[1] : '';
            }

            $().ready(function() {
                var funcNum = getUrlParam('CKEditorFuncNum');

                var elf = $('#elfinder').elfinder({
                    url: base_url + 'packages/elfinder/php/connector-custom.php',
                    getFileCallback: function(file) {
                        window.opener.CKEDITOR.tools.callFunction(funcNum, file);
                        window.close();
                    },
                    resizable: false
                }).elfinder('instance');
            });
        </script>

    </head>
    <body>
        <div id="elfinder"></div>
    </body>
</html>