<?php echo $this->Html->css(array('front/product_description.css')); ?>
<?php
$my_cat_color = array(
    1 => 'primary',
    2 => 'success',
    3 => 'info',
    4 => 'warning',
        )
?>
<script type="text/javascript">
       $(document).ready(function () {
        $('#enquiry_form').bValidator();
    });
</script>

<!--=== Search Block ===-->

<!--=== End Search Block ===-->
<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main bg-color-inner-report">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3><?php echo $product['Product']['product_name']; ?></h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
             <?php echo $this->element('front_breadcumb'); ?>
              <!-- <li><a href="index.php">Home</a></li>
              <li>Report Details</li> -->
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->

<section class="about-inner report-inner-bg">
  <div class="container">
      <div class="blog-inner report-page report-details-page col-md-12">
          <!-- <h4>Reports Title</h4> -->
          <div class="col-md-1 col-xs-3 report-left-img">
            <?php if (!empty($product['Product']['product_image'])) { ?>
                <?php
                echo $this->Html->image('product_images/' . $product['Product']['product_image'], array('class' => 'img-thumbnail image_size_home'));
            } else {
                echo $this->Html->image('reports/report1.png', array('class' => 'img-thumbnail image_size_home'));
            }
            ?>
          </div>
          <div class="col-md-7 col-xs-9">
            <h5><?php echo $product['Product']['product_name']; ?></h5>
            <div class="">
            <div class="date-year-info">
              <div class="pull-left date-left col-md-5 col-xs-12 row">
                <h6 class=""><i class="fa fa-calendar"></i>
                  <!--ID:<?php echo $product['Product']['product_no']; ?>-->
                <?php $len = strlen($product['Product']['id']);
                $id = 11210000+$product['Product']['id'];
                $regno = 'VMR'.$id;
                ?>
                  ID:<?php echo $regno; ?>
                </h6>
              </div>
              <div class="pull-left date-left col-md-5 col-xs-12 row">
                <h6 class=""><i class="fa fa-calendar"></i>
                  <?php if (!empty($product['Product']['pub_date']) && $product['Product']['pub_date'] != "0000-00-00" && $product['Product']['pub_date'] != "1970-01-01"): ?>
                      <?php echo Date('F Y', strtotime($product['Product']['pub_date'])); ?>
                  <?php else: ?>
                      <?php echo Date('F Y', strtotime("+1 month")); ?>
                  <?php endif; ?>
                </h6>
              </div>
              <!--<div class="pull-left date-left col-md-5 col-xs-12 row">
                <?php if (!empty($product['Product']['publisher_name'])) { ?>
                    <h6 class=""><i class="fa fa-file-text-o" aria-hidden="true"></i><?php echo $product['Product']['publisher_name']; ?></h6>
                <?php } ?>
              </div>-->
            </div>
          </div>
          
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 row">
            
            <div class="text-left">
              <div class="btns">
                <ul>
                  <!-- <li class="buy-btn"> <?php
                    echo $this->Html->link('Buy Now', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','id' => $product['Product']['id'], 'ref_page' => 'buy-now'), array("class" => "btn-u btn-u-primary", "target" => "_blank"));
                    ?>
                  </li> -->
                  <li class="btn-secondary"><?php
                    echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-download')).' Download Sample', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'download-sample'), array("class" => "btn-u btn-u-green", "target" => "_blank",'escape' => false)
                    );
                    ?>
                  </li>
                  <li class="btn-primary"> <?php
                    echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-question-circle')).' Ask Questions', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'ask-questions'), array("class" => "btn-u btn-u-purple", "target" => "_blank",'escape' => false)
                    );
                    ?>
                  </li>
                </ul>
              </div>
            </div>
            
          </div>

          </div>
          
          <div class="user">
              <div class="col-md-4">
                <div class="btns chooseopt">
                  <h4>Choose your Buying Option</h4>
                  <!-- <ul>
                    <?php foreach($licence_type as $key=>$value): ?>
                      <li><h6><?php echo $value; ?></h6></li>
                    <?php endforeach; ?>
                  </ul> -->
                      <div class="funkyradio">
                          <?php foreach($licence_type as $key=>$value): ?>
                            <div class="funkyradio-warning">
                                <input type="radio" name="radio" id="radio<?= $key?>" value="<?= $key ?>" onclick="localStorage['buyoption'] = $(this).val();" <?php if($key==1){?>checked<?php }?> />
                                <label for="radio<?= $key?>"><?php echo $value; ?></label>
                            </div>
                          <?php endforeach; ?>
                        <!--<div class="funkyradio-warning">-->
                        <!--    <input type="radio" name="radio" id="radio2" />-->
                        <!--    <label for="radio2">Upto 10 Users License: $ 4950.00</label>-->
                        <!--</div>-->
                        <!--<div class="funkyradio-warning">-->
                        <!--    <input type="radio" name="radio" id="radio3" />-->
                        <!--    <label for="radio3">Multi User License: $ 8600.00</label>-->
                        <!--</div>-->
                    </div>
                    <div class="btns buy-button">
                      <ul>
                        <li class="btn-danger"> <?php
                          echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-shopping-basket')). ' Buy Now', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','slug' => $product['Product']['slug'], 'ref_page' => 'buy-now'), array("class" => "btn-u btn-u-primary", "target" => "_blank",'escape' => false));
                          ?>
                        </li>
                      </ul>
                    </div>
                </div>
              </div>
          </div>      
          <!--<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <div class="user">
              <div class="col-md-5">
                <div class="btns">
                  <ul>
                    <?php foreach($licence_type as $key=>$value): ?>
                      <li><h6><?php echo $value; ?></h6></li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-md-7 text-right">
              <div class="btns">
                <ul>
                  <li class="buy-btn"> <?php
                    echo $this->Html->link('Buy Now', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','id' => $product['Product']['id'], 'ref_page' => 'buy-now'), array("class" => "btn-u btn-u-primary", "target" => "_blank"));
                    ?>
                  </li>
                  <li class="download-btn"> <?php
                    echo $this->Html->link('Download Sample', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'id' => $product['Product']['id'], 'ref_page' => 'download-sample'), array("class" => "btn-u btn-u-green", "target" => "_blank")
                    );
                    ?>
                  </li>
                  <li class="reqst-btn"> <?php
                    echo $this->Html->link('Ask Questions', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'id' => $product['Product']['id'], 'ref_page' => 'ask-questions'), array("class" => "btn-u btn-u-purple", "target" => "_blank")
                    );
                    ?>
                  </li>
                </ul>
              </div>
            </div>
          </div>-->

          <div class="tablur col-md-12 col-lg-12">
            <div class="panel with-nav-tabs panel-warning">
                <div class="panel-heading">
                  <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#profile">Description</a></li>

                    <li><a data-toggle="tab" href="#passwordTab">Table Of Content</a></li>

                    <?php if (!empty($product['Product']['related_news'])) { ?>
                        <li><a data-toggle="tab" href="#relatednewsshow">Related News</a></li>
                    <?php } ?>

                    <?php if (!empty($product['Product']['summary'])) { ?>
                        <li><a data-toggle="tab" href="#prod_summary">Summary</a></li>
                    <?php } ?>

                    <?php if (!empty($product['Product']['glossary'])) { ?>
                        <li><a data-toggle="tab" href="#glossary">Glossary</a></li>
                    <?php } ?>

                    <?php if (!empty($product['Product']['customization'])) { ?>
                        <li><a data-toggle="tab" href="#customization">Customizations</a></li>
                    <?php } ?>

                    <?php if (!empty($product['Product']['industry_analysis'])) { ?>
                        <li><a data-toggle="tab" href="#industry_analysis">Industry Analysis</a></li>
                    <?php } ?>

                    <?php if (!empty($product['Product']['latest_dev'])) { ?>
                        <li><a data-toggle="tab" href="#latest_dev">Latest Developments</a></li>
                    <?php } ?>
                  </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                      <div id="profile" class="tab-pane fade in active">
                         <p><?php echo $product['Product']['product_description']; ?></p>
                      </div>
                      <div id="relatednewsshow" class="tab-pane fade">
                        <p>
                        <?php
                          if (!empty($product['Product']['related_news'])) {
                              echo $product['Product']['related_news'];
                          }
                          ?>
                        </p>
                      </div>

                      <div id="prod_summary" class="tab-pane fade">
                        <p>
                          <?php
                          if (!empty($product['Product']['summary'])) {
                              echo $product['Product']['summary'];
                          }
                          ?>
                        </p>
                      </div>

                      <div id="glossary" class="tab-pane fade">
                        <p>
                          <?php
                          if (!empty($product['Product']['glossary'])) {
                              echo $product['Product']['glossary'];
                          }
                          ?>
                        </p>
                      </div>

                      <div id="customization" class="tab-pane fade">
                        <p>
                          <?php
                          if (!empty($product['Product']['customization'])) {
                              echo $product['Product']['customization'];
                          }
                          ?>
                        </p>
                      </div>

                      <div id="industry_analysis" class="tab-pane fade">
                        <p>
                          <?php
                          if (!empty($product['Product']['industry_analysis'])) {
                              echo $product['Product']['industry_analysis'];
                          }
                          ?>
                        </p>
                      </div>

                      <div id="latest_dev" class="tab-pane fade">
                        <p>
                          <?php
                          if (!empty($product['Product']['latest_dev'])) {
                              echo $product['Product']['latest_dev'];
                          }
                          ?>
                        </p>
                      </div>

                      <div id="passwordTab" class="tab-pane fade">
                          <p>
                            <?php if ($product['Product']['is_set_toc'] == 0) { ?>
                                  <?php echo $product['Product']['product_specification']; ?>
                            <?php } else { ?>
                            <br>
                             <center>
                               <h3>To receive Table of Contents, please write to us at <br> <small>  <a href='mailto:sales@valuemarketresearch.com'>sales@valuemarketresearch.com</a></small></h3>
                             </center>
                          <?php } ?>
                        </p>
                      </div>


                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="left-inner section-block">
                    <div class="row">
                        <div class="row row_mar">
                            <div class="col-lg-12 share">
                                <div class="headline"><span class="headline-title">Related Categories</span></div>
                                <?php
                                $i = 0;
                                foreach ($cat_array as $key => $cat_name) {
                                    $i++;
                                    ?>
                                    <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat_name))); ?>" class="btn-u btn-u-xs rounded-4x btn-u-dark mar_cat">
                                        <?php echo $cat_name; ?>
                                    </a>
                                    <?php
                                    if ($i == 4) {
                                        $i = 0;
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px;">
                        <div class="share">
                            <span class='st_linkedin_hcount' displayText='LinkedIn'></span>
                            <span class='st_facebook_hcount' displayText='Facebook'></span>
                            <span class='st_twitter_hcount' displayText='Tweet'></span>
                            <span class='st_sharethis_hcount' displayText='ShareThis'></span>
                            <span class='st_pinterest_hcount' displayText='Pinterest'></span>
                            <span class='st_email_hcount' displayText='Email'></span>
                        </div>
                    </div>

                </div>

            </div>

        </div>
      </div>
      
  </div>
</section>
<!-- ABOUT INNER SECTION END -->


<div class="job-description">
    <div class="container content mgr_tp_pd">
        <div class="row">
            <!-- Left Inner -->
            <div class="col-md-12 text-left">
                <div class="sharethis-inline-share-buttons"></div>    
            </div>
            
            <!-- End Left Inner -->
        </div>
    </div>
</div>  <!-- End Right Inner -->


<!-- Key Quastions -->
 <div class="container">
      <div class="">
        <div class="report-page relat-reprt key-qust">
          <div class="col-md-6">
            <h2>Key questions answered by the report</h2>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                         <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      What was the market size from2014 to 2016?
                    </a>
                  </h4>

                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.le VHS.</div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                         <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      What will be market growth till 2025 and what will be the resultant market forcast in the year?
                    </a>
                  </h4>

                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.</div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                         <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      How will the market drivers, restraints and future opportunities affect the market dynamics and a subsequent analysis of the associated trends?
                    </a>
                  </h4>

                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                         <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                      How will the market drivers, restraints and future opportunities affect the market dynamics and a subsequent analysis of the associated trends?
                    </a>
                  </h4>

                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                        <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFive">
                         <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                      What will be market growth till 2025 and what will be the resultant market forcast in the year?
                    </a>
                  </h4>

                    </div>
                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                        <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.</div>
                    </div>
                </div>
                <div class="">
                  <div class="pull-left req-free-btn">
                   <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Request Free Sample Report</button>
               </div>
                 
                </div>
            </div>
            </div>
            <div class="col-md-6">
              <h2>Need specific market information?</h2>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <h5><a href="#.">Contact us at sales@valuemarketresearch.com</a></h5>
                  <ul class="mrkt-info">
                    <li><a href="#."><i class="fa fa-user"></i><span>Ask for free product review call with the author</span></a></li>
                    <li><a href="#."><i class="fa fa-users"></i><span>Share your specific research requirments for a customized report</span></a></li>
                    <li><a href="#."><i class="fa fa-share-alt"></i><span>Request for due diligence and consumer centric studies</span></a></li>
                    <li><a href="#."><i class="fa fa-recycle"></i>Request for study updates, segment specific and country level reports</a></li>
                  </ul>
                  <div class="pull-left req-free-btn">
                   <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Request for Customization</button>
               </div>
                </div>
            </div>
        </div>
      </div>
    </div>
      <!-- Key Quastions -->

<!--<script type="text/javascript">var switchTo5x = true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>

<script type="text/javascript">stLight.options({publisher: "fdd4a6a8-7a36-4540-a998-72dc1e815377", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>-->
<!--<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5ac83993003b52001341b5f1&product=inline-share-buttons"></script>-->
<style>
    .stMainServices{
        min-height: 22px;
    }
    .stButton_gradient{
        min-height: 22px;
    }
    .small_space{
        margin: 10px
    }
    .related_products{
        font-size: 14px;
    }
</style>
