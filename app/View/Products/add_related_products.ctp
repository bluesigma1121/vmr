<div class="ibox float-e-margins"> 
    <div id="form" action="#" class="wizard-big wizard clearfix" role="application" novalidate="novalidate">
        <div class="bread">
            <ol class="breadcrumb">
                <li>
                    <a href="#">Products</a>
                </li>
                <?php if (!empty($product['Category']['category_name'])) : ?>
                    <li class="active">
                        <strong><?php echo $product['Category']['category_name']; ?></strong>
                    </li>
                <?php endif; ?>
            </ol>
        </div>
        <div class="steps clearfix"><ul role="tablist">
                <li  class="first current" aria-disabled="false" aria-selected="true">
                    <a  href="#">
                        <span class="number">1.</span> Edit Product</a></li>
                <li  class="first current" ><a  href="#" ><span class="number">2.</span> Edit Categories</a></li>
                <li  class="first current" ><a  href="#" ><span class="number">3.</span> Edit Specification</a></li>
                <li  class="first current"  title="Active"><a  href="#" ><span class="number">4.</span> Edit Countries  <span class="badge badge-warning">Active</span></a></li>
            </ul>
        </div>
    </div>
    <!--    <div class="ibox-title">
            <h5>Add Countries</h5>
        </div>-->
    <div class="ibox-content">
        <div class="row">
            <div class="form-group">
                <label class="col-lg-2 control-label">Product Name</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('product_name', array('class' => 'form-control', 'id' => 'product_name', 'type' => 'text', 'label' => false)); ?>
                    <?php echo $this->Form->input('hid_prod_id', array('id' => 'prod_id')); ?>
                </div>
            </div>
        </div>
        <div class = "row">
            <div class = "col-md-8" id = "rel_products">
            </div>
            <div class = "col-md-4" id = "added_products">
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("#product_name").keypress(function () {
            var rel_str = $(this).val();
            var product_id = $("#prod_id").val();
            /* if (rel_str.length < 3) {
             return false;
             } */
            $.ajax({
                method: "POST",
                url: base_url + "products/display_related_products",
                data: {name: rel_str, product_id: product_id}
            }).done(function (returnHtml) {
                console.log(returnHtml);
                $("#rel_products").html("");
                $("#rel_products").html(returnHtml);
            });
        });
    });
</script>