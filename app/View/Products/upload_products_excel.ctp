<script>
    $(document).ready(function() {
        //$('#upload_pro').bValidator();
    });
</script> 
<style>
    .err_msg{
        color: red;
    }
</style>
<div class="page-content">
    <div class="widget-box top_margin">
        <div class="widget-header widget-header-blue widget-header-flat">
            <h4>Upload Products</h4>           
        </div>
        <div class="widget-body">
            <div class="portlet-body form">
                <?php echo $this->Form->create('Product', array('id' => 'upload_pro', 'class' => 'form-horizontal', 'type' => 'file', 'role' => 'form')); ?>


                <div class="form-group">
                    <label for="exampleInputFile" class="col-md-3 control-label">Product Excel:</label>
                    <div class="col-md-4">
                        <?php echo $this->Form->input('product_upload', array('type' => 'file', 'id' => 'exampleInputFile', 'class' => 'form-control', 'data-bvalidator' => 'required', 'data-bvalidator-msg' => 'Please select file of type .xls', 'placeholder' => 'Select File', 'label' => false)); ?>
                    </div>
                </div>
                <div class="form-actions text-center">
                    <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php echo $this->Html->link(__('Back'), array('action' => 'index'), array('class' => 'btn btn-warning btn-sm', 'div' => false)); ?>
                </div>
                <?php echo $this->Form->end(); ?> 
            </div>
        </div>
    </div>
    <hr>
</div>