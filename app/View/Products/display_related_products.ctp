<?php echo $this->Form->create('',array('action'=>'add_related_products')); ?>
<table class="table table-bordered">
    <tr>
        <th>#</th>
        <th>Product Name</th>
    </tr>
    <?php foreach($products as $key=>$product): ?>
    <tr>
        <td><input type="checkbox" name="product" value="<?php echo $key ?>"></td>
        <td><?php echo $product ?></td>
    </tr>
    <?php endforeach; ?>
</table>
<?php echo $this->Form->submit("Submit",array('class'=>'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>