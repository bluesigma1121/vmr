<style>
    .err_msg{
        color: red;
    }
</style>
<div class="page-content">
    <div class="widget-box top_margin">
        <div class="widget-header widget-header-blue widget-header-flat">
            <h4>Upload Products</h4>           
        </div>
        <div class="widget-body">
            <div class="portlet-body form">
                <?php echo $this->Form->create('Product', array('id' => 'upload_pro', 'class' => 'form-horizontal', 'type' => 'file', 'role' => 'form')); ?>

                <div class="form-body top_margin">
                    <div class="form-group">
                        <label class="col-md-3 control-label">&nbsp;</label>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('category_id', array('type'=>'hidden','value'=>3,'class' => 'form-control', 'label' => false)); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputFile" class="col-md-3 control-label">Product Excel:</label>
                    <div class="col-md-4">
                        <?php echo $this->Form->input('product_upload', array('type' => 'file', 'id' => 'exampleInputFile', 'class' => 'form-control', 'data-bvalidator' => 'required', 'data-bvalidator-msg' => 'Please select file of type .xls', 'placeholder' => 'Select File', 'label' => false)); ?>
                    </div>
                    
                </div>
                <div class="form-actions text-center">
                    <?php echo $this->Form->submit(__('Upload'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php echo $this->Html->link(__('Back'), array('action' => 'index'), array('class' => 'btn btn-warning btn-sm', 'div' => false)); ?>
                </div>
                <?php echo $this->Form->end(); ?> 
            </div>
        </div>
    </div>

    <hr>
    <div class="row">
        <p class="err_msg">
            * All failed product are exported into Excel
        </p>
    </div>
</div>