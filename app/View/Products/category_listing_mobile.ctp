<style>
.fa-dollar{
        color: #3498db;
    }
</style>
<?php echo $this->element('listing_search'); ?>

<?php echo $this->element('front_breadcumb'); ?>
<!--=== Content ===-->
<div class="content container">
    <div class="row">
        <?php echo $this->element('front_cat_prod_filter'); ?>
        <div class="col-md-9">
            <!--/end pagination-->
            <h2><?php echo $selected_cat['Category']['category_name']; ?></h2>
            <div class="msg1">
                <p> <?php echo $this->Text->truncate($selected_cat['Category']['short_desc'], 350); ?>
                    <?php if (strlen($selected_cat['Category']['short_desc']) >= 350) { ?> <a id="more">Read More</a> <?php } ?></p>


            </div>
            <div class="msg">
                <p><?php echo $selected_cat['Category']['short_desc'] ?><a id="less">Read Less</a></p>
            </div>
            <?php if (!empty($products)) { ?>

                <div class = "filter-results" style = "padding-top:0px;">
                    <div class = "row">
                        <div class="col-sm-4 margin_top_search">
                            <?php echo $this->Form->create('Product', array('id' => 'search_products', 'class' => 'form-horizontal'));
                            ?>
                            <div class="col-sm-5">
                                <?php
                                echo $this->Form->input('limit_of_records', array(
                                    'options' => $limits,
                                    'class' => 'form-control search-query',
                                    'id' => 'limit_search', 'label' => false,
                                    'selected' => $limit_per_page,
                                ));
                                ?>
                            </div>
                            Records Per Page
                            <?php echo $this->Form->end(); ?>
                        </div>
                        <?php
                        if (isset($this->params['paging']['ProductCategory']['pageCount'])) {
                            $page_cnt = $this->params['paging']['ProductCategory']['pageCount'];
                            $current = $this->params['paging']['ProductCategory']['page'];
                        } elseif (isset($this->params['paging']['Product']['pageCount'])) {
                            $page_cnt = $this->params['paging']['Product']['pageCount'];
                            $current = $this->params['paging']['ProductCategory']['page'];
                        }
                        ?>

                        <div class="col-sm-8">
                            <?php if ($page_cnt > 1) { ?>
                                <div class="text-center mar_tb ">
                                    <ul class="pagination pagination-v2 pagination-sm" style="visibility: visible;"> 
                                        <li>
                                            <span class="prev" style="min-width: 100px;">
                                                <?php if ($current > 2): ?>
                                                    <a href="<?php echo Router::url("/", true); ?>reports/<?php echo $paginator_url ?>-page-<?php echo $current - 1 ?>">&lt; &nbsp; Previous</a>
                                                <?php elseif ($current == 2): ?>
                                                    <a href="<?php echo Router::url("/", true); ?>reports/<?php echo $paginator_url ?>">&lt; &nbsp; Previous</a>
                                                <?php else: ?>
                                                    &lt; &nbsp; Previous &nbsp;
                                                <?php endif; ?>
                                            </span>
                                        </li>

                                        <li>
                                            <span class="next" style="min-width: 70px;">
                                                <?php if ($page_cnt != $current): ?>
                                                    <a href="<?php echo Router::url("/", true); ?>reports/<?php echo $paginator_url ?>-page-<?php echo $current + 1 ?>">Next &nbsp; &gt;</a>
                                                <?php else: ?>
                                                    Next &nbsp; &gt;
                                                <?php endif; ?>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            <?php } ?>
                        </div>

                    </div>
                    <div>
                        <p>
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?> 
                        </p> 
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="text-center">
                                    <h5>  PRODUCT TITLE </h5>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <?php
                                echo $this->Html->link(__('<h5>PUBLISH DATE</h5>'), array('controller' => 'products', 'action' => 'publish_session'), array('escape' => false));
                                ?>
                            </div>
                            <div class="col-sm-2">
                                <?php
                                echo $this->Html->link(__('<h5>PRICE </h5>'), array('controller' => 'products', 'action' => 'price_session'), array('escape' => false));
                                ?>
                            </div>
                        </div>
                        <?php foreach ($products as $key => $product) {
                            ?>
                            <div class="list-product-description product-description-brd margin-bottom-30 m_top">
                                <div class="row ">
                                    <div class="col-sm-2">
                                        <?php /*
                                          <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'id' => $product['Product']['id'], 'slug' => $this->Link->cleanString($product['Product']['product_name']))); ?> ">
                                         */ ?>
                                        <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($product['main_cat']['slug']), 'id' => $product['Product']['id'], 'slug' => $product['Product']['slug'])); ?> ">
                                            <?php if (!empty($product['Product']['product_image'])) { ?>
                                                <?php
                                                echo $this->Html->image('product_images/' . $product['Product']['product_image'], array('class' => 'img-thumbnail image_size'));
                                            } else {
                                                echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail image_size'));
                                            }
                                            ?>
                                        </a>
                                    </div> 

                                    <?php /*
                                      <div class="col-sm-6">
                                      <div class="row">
                                      <div style="padding-right: 10px;">
                                      <ul class="list-inline overflow-h">
                                      <li><h4 class="title-price h4_fnt"><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($product['main_cat']['slug']), 'id' => $product['Product']['id'], 'slug' => $product['Product']['slug'])); ?> "> <?php echo $product['Product']['product_name']; ?></a></h4></li>
                                      </ul>
                                      </div>
                                      </div>
                                      <div class="row">
                                      <div style="padding-right: 10px;">
                                      <p>
                                      <?php
                                      echo $this->Text->truncate(
                                      strip_tags($product['Product']['product_description']), 150, array(
                                      'ellipsis' => '...',
                                      'exact' => false
                                      )
                                      );
                                      ?>
                                      </p>
                                      </div>
                                      </div>
                                      </div>
                                      <div class="col-sm-2 price_div">
                                      <div class="row">
                                      <div class="margin_in min_hei_pub">
                                      <span class="text-center">
                                      OCTOMBER 2014
                                      </span>
                                      </div>
                                      </div>
                                      <div class="row">
                                      <div class="margin_in">
                                      <?php
                                      echo $this->Html->link(__('<i class="fa fa-bell-o"></i> Send Inquiry'), array('controller' => 'enquiries', 'action' => 'send_enquiry', $product['Product']['id'], $selected_cat['Category']['id'], 'category_listing'), array('class' => 'btn-u btn-u-green send_enquery', 'escape' => false, 'title' => 'Send Inquiry'));
                                      ?>
                                      </div>
                                      </div>
                                      </div>
                                      <div class="col-sm-2 price_div">
                                      <div class="row">
                                      <div class="margin_in min_hei_pub">
                                      <span class="title-price margin-right-10 font_clr">
                                      <?php $final_price = round(($product['Product']['price'] * $dollar_rate), 2); ?>
                                      <?php if ($cur_curency == 0): ?>
                                      <b><i class="fa fa-inr"></i> </b><?php echo $final_price; ?>
                                      <?php else: ?>
                                      <b><i class="fa fa-dollar"></i> </b>
                                      <?php echo number_format($final_price, 2); ?>
                                      <?php endif; ?>
                                      </span>
                                      </div>
                                      </div>
                                      <div class="row">
                                      <div class="margin_in">
                                      <?php echo $this->Form->create('CartItem', array('controller' => 'cart_items', 'action' => 'cart', 'class' => '', 'id' => 'cart_frm')) ?>
                                      <?php echo $this->Form->input('product_id', array('type' => 'hidden', 'value' => $product['Product']['id'])) ?>
                                      <?php echo $this->Form->button('<i class="fa fa-shopping-cart"></i> Add to Cart', array('class' => 'btn-u btn-u-blue', 'escape' => false)); ?>
                                      <?php echo $this->Form->end(); ?>
                                      </div>
                                      </div>
                                      </div>

                                     */ ?>
                                    <div class="col-sm-10">
                                        <div class="overflow-h margin-bottom-5" style="margin-bottom:0px;">
                                            <ul class="list-inline overflow-h">
                                                <li><h4 class="title-price h4_fnt"><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($product['main_cat']['slug']), 'id' => $product['Product']['id'], 'slug' => $product['Product']['slug'])); ?> "> <?php echo $product['Product']['product_name']; ?></a></h4></li>
                                            </ul>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="margin-bottom-10d">
                                                        <span class="title-price margin-right-10 font_clr">
                                                            <?php $final_price = round(($product['Product']['price'] * $dollar_rate), 2); ?>
                                                            <?php if ($cur_curency == 0): ?>
                                                                <b><i class="fa fa-inr"></i> </b><?php echo $final_price; ?>
                                                            <?php else: ?>
                                                                <b><i class="fa fa-dollar"></i> </b>
                                                                <?php echo number_format($final_price, 2); ?>
                                                            <?php endif; ?>
                                                        </span>
                                                        <span class="title-price margin-right-10 font_clr text-center">
                                                            <?php
                                                            if (!empty($product['Product']['pub_date']) &&
                                                                    $product['Product']['pub_date'] != "0000-00-00" && $product['Product']['pub_date'] != "1970-01-01"):
                                                                ?>
                                                                <?php echo Date('F Y', strtotime($product['Product']['pub_date'])); ?>
                                                            <?php else: ?>
                                                                <?php echo Date('F Y', strtotime("+1 month")); ?>
        <?php endif; ?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-5">
                                                    <div class="pull-right">
                                                        <?php /*
                                                          $url_gen = Router::url('/', true) . 'enquiries/send_enquiry/' . $product['Product']['id'] . '/' . $selected_cat['Category']['id'] . '/category_listing';
                                                          ?>
                                                          <span
                                                          id="call_me_<?php echo $product['Product']['id']; ?>"
                                                          class="btn-u btn-u-green"
                                                          rel="<?php echo $product['Product']['id']; ?>"
                                                          onclick="getPopupForm(this.id, '<?php echo $url_gen; ?>');" >
                                                          Send Inquiry
                                                          </span> */ ?>
                                                    </div>
                                                    <?php /* echo $this->Form->create('CartItem', array('controller' => 'cart_items', 'action' => 'cart', 'class' => '', 'id' => 'cart_frm')) ?>
                                                      <?php echo $this->Form->input('product_id', array('type' => 'hidden', 'value' => $product['Product']['id'])) ?>
                                                      <?php echo $this->Form->button('<i class="fa fa-shopping-cart"></i> Add to Cart', array('class' => 'btn-u btn-u-blue', 'escape' => false)); ?>
                                                      <?php echo $this->Form->end(); */ ?>
                                                </div>
                                                <div class="col-lg-3"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p class="margin-bottom-20d" style="margin-bottom:0px;">
                                                        <?php
                                                        echo $this->Text->truncate(
                                                                strip_tags($product['Product']['product_description']), 150, array(
                                                            'ellipsis' => '...',
                                                            'exact' => false
                                                                )
                                                        );
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
    <?php } ?>
                    </div>
                </div>
                    <?php if ($page_cnt > 1) { ?>
                    <div class="text-center mar_tb ">
                        <ul class="pagination pagination-v2 pagination-sm" style="visibility: visible;"> 
                            <li>
                                <span class="prev" style="min-width: 100px;">
                                    <?php if ($current > 2): ?>
                                        <a href="<?php echo Router::url("/", true); ?>reports/<?php echo $paginator_url ?>-page-<?php echo $current - 1 ?>">&lt; &nbsp; Previous</a>
                                    <?php elseif ($current == 2): ?>
                                        <a href="<?php echo Router::url("/", true); ?>reports/<?php echo $paginator_url ?>">&lt; &nbsp; Previous</a>
                                    <?php else: ?>
                                        &lt; &nbsp; Previous &nbsp;
                                    <?php endif; ?>
                                </span>
                            </li>

                            <li>
                                <span class="next" style="min-width: 70px;">
                                    <?php if ($page_cnt != $current): ?>
                                        <a href="<?php echo Router::url("/", true); ?>reports/<?php echo $paginator_url ?>-page-<?php echo $current + 1 ?>">Next &nbsp; &gt;</a>
                                    <?php else: ?>
                                        Next &nbsp; &gt;
                                    <?php endif; ?>
                                </span>
                            </li>
                        </ul>
                    </div>
    <?php } ?>
                <div class="text-center m_top"> 
                    <p>
                        <?php
                        echo $this->Paginator->counter(array(
                            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                        ));
                        ?> 
                    </p>
                </div> 
            <?php } else {
                ?>
                <div class = "filter-results" style = "padding-top:0px;">
                    <div class = "row">
                        <div class="col-sm-12  contex-bg">
                            <div class="alert alert-warning text-center">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <strong>Products Not Found </strong>
                            </div>
                        </div>
                        <div class="col-sm-12 m_top">
                            <div class="text-center">
                                <button class="btn-u"onclick="goBack()">Go Back</button>
                            </div>
                        </div>

                    </div>
                </div>
            <?php }
            ?>

        </div>
    </div><!--/end row-->
</div>
