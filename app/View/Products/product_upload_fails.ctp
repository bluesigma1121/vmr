<?php

$this->Xls->setHeader('Upload Product Excel');

$this->Xls->addXmlHeader();
$this->Xls->setWorkSheetName('Upload Product Excel');

$this->Xls->openRow();
$this->Xls->writeString('Product Name');
$this->Xls->writeString('Product Description');
$this->Xls->writeString('Product Specification');
$this->Xls->writeString('price');
$this->Xls->writeString('Published Date');
$this->Xls->writeString('Meta Name');
$this->Xls->writeString('Meta Description');
$this->Xls->writeString('Reasons');
$this->Xls->closeRow();
$this->Xls->openRow();
$this->Xls->closeRow();

foreach ($product_fails as $key => $data):
    $this->Xls->openRow();
    $this->Xls->writeString($data['product_name']);
    $this->Xls->writeString($data['product_description']);
    $this->Xls->writeString($data['product_specification']);
    $this->Xls->writeString($data['price']);
    $this->Xls->writeString($data['published']);
    $this->Xls->writeString($data['meta_name']);
    $this->Xls->writeString($data['meta_description']);
    $this->Xls->writeString($data['error']);
    $this->Xls->closeRow();
endforeach;

$this->Xls->addXmlFooter();
exit();
?>