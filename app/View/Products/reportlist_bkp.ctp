<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main bg-color-inner-report">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>Reports</h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a>Reports</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->

<!-- ABOUT INNER SECTION START -->
<section class="about-inner report-inner-bg">
      <div class="container">
        <div class="">
          <?php foreach ($products as $key => $product): ?>
            <div class="blog-inner report-page">
              <div class="col-md-2 report-left-img">
                <?php if (!empty($product['Product']['product_image'])) { ?>
                    <?php
                    echo $this->Html->image('product_images/' . $product['Product']['product_image'], array('class' => 'img-thumbnail image_size_home'));
                } else {
                    echo $this->Html->image('reports/report1.png', array('class' => 'img-thumbnail image_size_home'));
                }
                ?>
              </div>
              <div class="col-md-8">
                  <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'slug' => $product['Product']['slug'])); ?>">
                        <h5><?=  $product['Product']['product_name']; ?></h5>
                        <p>
                          <?php echo $this->Text->truncate(strip_tags($product['Product']['product_description']), 150, array(
                                                            'ellipsis' => '...','exact' => false)
                                                            );?>
                        </p>
                      </a>
              </div>
              <div class="col-md-2 text-center">
                <h5>$ <?=$product['Product']['price'];?></h5>

              </div>
              <div class="col-md-8 col-sm-12 col-xs-12">
                <div class="pull-left date-left">
                   <h6 class=""><i class="fa fa-calendar"></i>
                     <?php if (!empty($product['Product']['pub_date']) && $product['Product']['pub_date'] != "0000-00-00" && $product['Product']['pub_date'] != "1970-01-01"): ?>
                         <?php echo Date('F Y', strtotime($product['Product']['pub_date'])); ?>
                     <?php else: ?>
                         <?php echo Date('F Y', strtotime("+1 month")); ?>
                     <?php endif; ?>
                   </h6>
                 </div>
                 <div class="pull-right enq-btn">
                   <?php
                      echo $this->Html->link('Enquiry Before Buying', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','slug' => $product['Product']['slug'], 'ref_page' => 'enquiry-before-buying'), array("target" => "_blank"));
                   ?>
                   <!-- <a href="enquiry-before-buying.php">Enquiry Before Buying</a> -->
                 </div>

              </div>
              <div class="col-md-2 pull-left req-btn">
                <?php
                   echo $this->Html->link('Request Sample', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','slug' => $product['Product']['slug'], 'ref_page' => 'request-sample'), array("target" => "_blank"));
                ?>
                   <!-- <a href="request-customization.php">Request Sample</a> -->
             </div>
          </div>

        <?php endforeach; ?>
       </div>

        <ul class="col-md-12 pagination text-center">
          <?php if (isset($this->params['paging']['Product']['pageCount'])) {
               $page_cnt = $this->params['paging']['Product']['pageCount'];
               $current = $this->params['paging']['Product']['page'];
           } elseif (isset($this->params['paging']['Product']['pageCount'])) {
               $page_cnt = $this->params['paging']['Product']['pageCount'];
               $current = $this->params['paging']['Product']['page'];
           }
          ?>
           <?php if ($current > 2): ?>
               <a href="<?php echo Router::url(array('controller' => 'products','action' => 'reportlist','page' => $current - 1)); ?>">&lt; &nbsp; Previous</a>
           <?php elseif ($current == 2): ?>
               <a href="<?php echo Router::url(array('controller' => 'products','action' => 'reportlist','page' => $current - 1)); ?>">&lt; &nbsp; Previous</a>
           <?php else: ?>
               &lt; &nbsp; Previous &nbsp;
           <?php endif; ?>
           &nbsp;&nbsp;|&nbsp;&nbsp;
           <?php if ($page_cnt != $current && $page_cnt > 0): ?>
               <a href="<?php echo Router::url(array('controller' => 'products','action' => 'reportlist','page' => $current + 1)); ?>">Next &nbsp; &gt;</a>
           <?php else: ?>
               Next &nbsp; &gt;
           <?php endif; ?>
           <br>
           <small><?php
           echo $this->Paginator->counter(array(
               'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
           ));
           ?></small>

          <?php
            // $this->Paginator->options(array('url' => $this->passedArgs));
            //
        		// echo $this->Paginator->first('< ' . __('First'), array('tag' => 'li' ), null, array('class' => 'prev disabled','tag' => 'li'));
        		// echo $this->Paginator->prev('<< ' . __(''), array('tag' => 'li' ), null, array('class' => 'prev disabled','tag' => 'li'));
        		// echo $this->Paginator->numbers(array('tag' => 'li','separator' => ''));
        		// echo $this->Paginator->next(__('') . ' >>', array('tag' => 'li'), null, array('class' => 'next disabled'));
        		// echo $this->Paginator->last(__('Last') . ' >', array('tag' => 'li'), null, array('class' => 'next disabled'));
        	?>
              <!-- <li class="disabled"><a href="#">«</a></li>
              <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">»</a></li> -->
            </ul>
    </div>
</section>

<!-- ABOUT INNER SECTION END -->
