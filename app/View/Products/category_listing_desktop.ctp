<style>
.background-white {
    padding-right: 0;
}
.about-inner h1 {
    color: #333;
    border-bottom: 1px solid #eee;
    padding-bottom: 15px;
    font-size: 20px;
    font-weight: normal;
}
.image_size:after { content: attr(alt); }
</style>
<script type="text/javascript">$(document).ready(function () {  
  $(".image_size").each(function(){
        $(this).attr("title", $(this).attr("alt"));
    });
});</script>
<!--BANNER SECTION START -->

<div class="banner">
  <div class="bg-color bg-color-inner-main bg-color-inner-report">
    <div class="container">
      <div class="row">
        <!-- <div class="col-md-10 col-md-push-1 col-xs-12 col-lg-10 col-lg-push-1">
            <div class="custom-search-form">
                <?php echo $this->Form->create('Category', array('type' => 'get','action'=>'home_search_form')); ?>
                  <?php  
                    $arr= explode("?",$_SERVER['REQUEST_URI']); //print_r($arr);//echo $this->base.'/search-results';
                    if($arr[0] != $this->base.'/search-results'){ 
                            echo $this->Form->input('q', array('class' => 'form-control', 'label' =>false,'required','autocomplete'=>'off','id'=>'search_text'));
                    }else{
                            echo $this->Form->input('q', array('class' => 'form-control', 'label' =>false,'required','autocomplete'=>'off','id'=>'search_text','value'=>$_GET['q']));
                    }
                  ?>
                   <div class="col-md-11 pull-left suggest-div" id="suggest"></div>
                    <div class="col-md-1 col-sm-1 input-group-btn pull-right search-btn">
                      <button class="btn btn-primary" type="submit">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                    </div>
                <?php  echo $this->Form->end(); ?>
           </div>
        </div> -->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
             <?php echo $this->element('front_breadcumb'); ?>
              <!-- <li><a href="index.php">Home</a></li>
              <li>Reports</li> -->
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->

<!-- ABOUT INNER SECTION START -->
<section class="about-inner report-inner-bg">
  <div class="container">
    <div style="background-color:#fff;overflow:hidden">
        <!-- <div class="col-md-4"> -->
        <?php
         $request = $this->request;
         ?>
          <?php if(!$request->is('mobile')){ ?>
          <div class="hidden-xs hidden-sm">
              <?php echo $this->element('front_cat_prod_filter'); ?>
          </div>
          <?php } ?>
        <!-- </div> -->
        
        <div class="<?php if($cat_data['Level2'][0]['no_of_product']>0){ echo 'col-md-9 col-sm-8'; }else{ echo 'col-md-12'; }?> background-white">
          <div class="rpt-heading">
            <div class="col-md-12 col-xs-12 col-sm-12 healthcare-report">
              <h1><?php echo $selected_cat['Category']['category_name']; ?></h1>
              <div class="msg1">
                  <p> <?php echo $this->Text->truncate($selected_cat['Category']['short_desc'], 350); ?>
                      <?php if (strlen($selected_cat['Category']['short_desc']) >= 350) { ?> <a href="#." id="more">Read More</a> <?php } ?></p>
              </div>
              <div class="msg">
                  <p><?php echo $selected_cat['Category']['short_desc'] ?><a href="#." id="less">&nbsp;Read Less</a></p>
              </div>
            </div>
          </div>

          <?php if (!empty($products)) { ?>
            <?php foreach ($products as $key => $product) { ?>
              <div class="blog-inner report-page">
                <div class="col-md-2 col-sm-2 report-left-img">
                    <?php if (!empty($product['Product']['product_image'])) {
                          echo $this->Html->image('product_images/' . $product['Product']['product_image'], array('class' => 'img-thumbnail image_size','alt'=>'Market Research Report'));
                      } else {
                          echo $this->Html->image('reports/report1.webp', array('class' => 'img-thumbnail image_size','alt'=>'Market Research Report'));
                      }
                    ?>                  
                </div>
                <div class="col-md-8 col-sm-8">
                  <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details',  'slug' => $product['Product']['slug'])); ?> ">
                        <h5><?php echo $product['Product']['product_name']; ?></h5>
                        <p>
                          <?php echo $this->Text->truncate(
                                      strip_tags($product['Product']['product_description']), 150,
                                                  array('ellipsis' => '...','exact' => false)
                                                );?>
                        </p>
                    </a>
                </div>
                <div class="col-md-2 col-sm-2 text-center">
                  <h5>
                    <?php $final_price = round(($product['Product']['price'] * $dollar_rate), 2); ?>
                    <?php if ($cur_curency == 0): ?>
                        <b><i class="fa fa-inr"></i></b><?php echo $final_price; ?>
                    <?php else: ?>
                        <b><i class="fa fa-dollar"></i> </b>
                        <?php echo number_format($final_price, 2); ?>
                    <?php endif; ?>
                  </h5>

                </div>
                <div class="clearfix"></div>
                <div class="col-md-7 col-sm-8 col-xs-12">
                  <div class="pull-left date-left">
                     <h6 class=""><i class="fa fa-calendar"></i>
                       <?php if (!empty($product['Product']['pub_date']) && $product['Product']['pub_date']!= "0000-00-00" && $product['Product']['pub_date']!= "1970-01-01"): ?>
                           <?php echo Date('F Y', strtotime($product['Product']['pub_date'])); ?>
                       <?php else: ?>
                           <?php echo Date('F Y', strtotime("+1 month")); ?>
                       <?php endif; ?>
                      </h6>
                   </div>
                   <div class="pull-right enq-btn">
                     <?php
                       echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-download')).' Download Sample', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'download-sample'), array("class" => "", "target" => "_blank",'escape' => false)
                      );
                     ?>
                   </div>

                </div>
                <div class="col-md-3 col-sm-4 text-right pull-left req-btn">
                  <?php
                   echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-question-circle')).' Ask Questions', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'ask-questions'), array("class" => "", "target" => "_blank",'escape' => false)
                  );
                ?>
               </div>
             </div>
           <?php } ?>
         <?php } ?>

       </div>

       <div class="<?php if($cat_data['Level2'][0]['no_of_product']>0){ echo 'col-md-12'; }else{ echo 'col-md-12'; }?> text-center pagination-box">

         
         <ul class="pagination">
          <?php if (isset($this->params['paging']['ProductCategory']['pageCount'])) {
               $page_cnt = $this->params['paging']['ProductCategory']['pageCount'];
               $current = $this->params['paging']['ProductCategory']['page'];
           } elseif (isset($this->params['paging']['ProductCategory']['pageCount'])) {
               $page_cnt = $this->params['paging']['ProductCategory']['pageCount'];
               $current = $this->params['paging']['ProductCategory']['page'];
           }
          ?>
          
           <?php if ($current > 2): ?>
               <a href="<?php echo Router::url("/", true); ?>industries/<?php echo $paginator_url ?>?page=<?php echo $current - 1 ?>">&lt; &nbsp; Previous</a>
           <?php elseif ($current == 2): ?>
               <a href="<?php echo Router::url("/", true); ?>industries/<?php echo $paginator_url ?>">&lt; &nbsp; Previous</a>
              
           <?php else: ?>
               &lt; &nbsp; Previous &nbsp;
           <?php endif; ?>
          
           &nbsp;&nbsp;|&nbsp;&nbsp;
           <?php if ($page_cnt != $current && $page_cnt > 0): ?>
           <?php //$this->Paginator->options(array('convertKeys' => array('page')));?>
               <!-- <a href="<?php //echo Router::url("/", true); ?>industries/<?php //echo $paginator_url ?>-page=<?php //echo $current + 1 ?>">Next &nbsp; &gt;</a> -->
             <?php 
              //$url = array('?' => ['page' => $current + 1]);
             ?>
               <?php //echo $this->Paginator->options['url']['?'] = 'page' 
              // print_r($value1);
              //echo $_GET['value1'];
               //echo $this->request['url']['value1'];
               ?>
               <!-- <a href="<?php //echo Router::url("/", true); ?>industries/<?php //echo $paginator_url; ?>?page=<?php //echo $current + 1 ?>" >Next &nbsp; &gt;</a> -->

               <a href="<?php echo Router::url("/", true); ?>industries/<?php echo $paginator_url; ?>?page=<?php echo $current + 1 ?>" >Next &nbsp; &gt;</a>

           <?php else: ?>
               Next &nbsp; &gt;
           <?php endif; ?>
           <br>
          <small>
        
          <?php
           echo $this->Paginator->counter(array(
               'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
           ));

           
           ?></small>
            
            <!-- <?= $this->Paginator->numbers() ?>

// Shows the next and previous links
<?= $this->Paginator->prev('« Previous') ?>
<?= $this->Paginator->next('Next »') ?>

// Prints X of Y, where X is current page and Y is number of pages
<?= $this->Paginator->counter() ?>
            
<?= $this->Paginator->counter([
    'format' => 'Page {{page}} of {{pages}}, showing {{current}} records out of
             {{count}} total, starting on record {{start}}, ending on {{end}}'
]) ?> -->



<!-- 
            <ul class="pagination">
<?= $this->Paginator->prev("<<") ?>
<?= $this->Paginator->numbers() ?>
<?= $this->Paginator->next(">>") ?>

</ul> -->





            <?php 

// $prev_link = str_replace('page:', '',  $this->Paginator->prev("<<"));
// $prev_link = preg_replace('/\/1"/', '"', $prev_link);
// $next_link = str_replace('page:', '', $this->Paginator->next(">>") );
// echo $prev_link;
// echo $next_link;
    // $this->Paginator->options['url'] = array('controller' => 'products', 'action' => 'category_listing');
    // echo $this->Paginator->prev(' <<' . __('Previous  '), array(), null, array('class' => 'prev disabled'));
    // echo $this->Paginator->numbers();
    // echo $this->Paginator->next(__('  Next') . '>> ', array(), null, array('class' => 'next disabled'));

            // $prev_link = str_replace('page:', 'page=', $this->Paginator->prev('&laquo;', array( 'tag' => 'li', 'escape' => false), null, array('class' => 'prev disabled prv' ,'tag' => 'li', 'escape' => false)));
            // $prev_link = preg_replace('/\/1"/', '"', $prev_link);
              
            // $next_link = str_replace('page:', 'page=',  $this->Paginator->next('&raquo;', array( 'tag' => 'li', 'escape' => false), null, array('class' => 'next disabled nxt' ,'tag' => 'li', 'escape' => false)));
            // echo $prev_link;
            // echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li' ,'currentClass' => 'active', 'currentTag' => 'a' , 'escape' => false));
           
            // echo $next_link;
          
              // echo $this->Paginator->prev('&laquo;', array( 'tag' => 'li', 'escape' => false), null, array('class' => 'prev disabled prv' ,'tag' => 'li', 'escape' => false));
              // echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li' ,'currentClass' => 'active', 'currentTag' => 'a' , 'escape' => false));
              // echo $this->Paginator->next('&raquo;', array( 'tag' => 'li', 'escape' => false), null, array('class' => 'next disabled nxt' ,'tag' => 'li', 'escape' => false));
            ?>
            <!-- <ul class="pagination" style="visibility: visible;">
                                <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
                                <li>
                                <?= $this->Paginator->numbers([
               'modulus' => 2,
               'url' => [
                '?' => [
                  'page' => $current + 1
        ]
    ]
 ]) ?>
                                <?= $this->Paginator->numbers(array('separator' => '')); ?></li>
                                <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
                            </ul> -->
        </ul>

        
        

         <!-------start pagination----------->
         <?php
  //    $this->Paginator->options([
  //     // 'url' => [
  //     //     '?' => $pagelinks
  //     // ]
  // ]);
         ?>
         <!-- <div class="paginator">
    <ul class="pagination">
            <?= $this->Paginator->first(__('1st')) ?>
            <?= $this->Paginator->prev(__('< ')) ?>
            <?= $this->Paginator->numbers([
               'modulus' => 2,
               'url' => [
                '?' => [
                  'page' => 'a'
        ]
    ]
]) ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div> -->
         <!-- <div class="row col-md-12">
                        <div class="dataTables_paginate paging_bootstrap">
                            <p>
                                <?php
                                //echo $this->Paginator->counter(array(
                                //    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                               // ));
                                ?> </p>
                            <ul class="pagination" style="visibility: visible;">
                                <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
                                <li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
                                <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
                            </ul>
                        </div>
                    </div> -->
        <!-------end pagination----------->
      </div>

      <!-- <div class="hidden-lg hidden-md">
          <?php //echo $this->element('front_cat_prod_filter'); ?>
      </div> -->
    </div>
  </div>

</section>

<!-- ABOUT INNER SECTION END -->

<script type="text/javascript">
$('.msg').hide();
$('#more').click(function () {
    $('.msg').show();
    $('.msg1').hide();
});
$('#less').click(function () {
    $('.msg1').show();
    $('.msg').hide();
});
</script>
