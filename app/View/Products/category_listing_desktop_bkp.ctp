<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main bg-color-inner-report">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>Industries</h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
             <?php echo $this->element('front_breadcumb'); ?>
              <!-- <li><a href="index.php">Home</a></li>
              <li>Reports</li> -->
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->

<!-- ABOUT INNER SECTION START -->
<section class="about-inner report-inner-bg">
      <div class="container">
        <!-- <div class="col-md-2"> -->
          <?php echo $this->element('front_cat_prod_filter'); ?>
        <!-- </div> -->
        
        <div class="<?php if($cat_data['Level2'][0]['no_of_product']>0){ echo 'col-md-9'; }else{ echo 'col-md-12'; }?> background-white">
          <div class="rpt-heading">
            <div class="col-md-12 col-xs-12 col-sm-12 healthcare-report">
              <h4><?php echo $selected_cat['Category']['category_name']; ?></h4>
              <div class="msg1">
                  <p> <?php echo $this->Text->truncate($selected_cat['Category']['short_desc'], 350); ?>
                      <?php if (strlen($selected_cat['Category']['short_desc']) >= 350) { ?> <a href="#." id="more">Read More</a> <?php } ?></p>
              </div>
              <div class="msg">
                  <p><?php echo $selected_cat['Category']['short_desc'] ?><a href="#." id="less">&nbsp;Read Less</a></p>
              </div>
            </div>
          </div>

          <?php if (!empty($products)) { ?>
            <?php foreach ($products as $key => $product) { ?>
              <div class="blog-inner report-page">
                <div class="col-md-2 report-left-img">
                    <?php if (!empty($product['Product']['product_image'])) {
                          echo $this->Html->image('product_images/' . $product['Product']['product_image'], array('class' => 'img-thumbnail image_size'));
                      } else {
                          echo $this->Html->image('reports/report1.png', array('class' => 'img-thumbnail image_size'));
                      }
                    ?>

                  <!-- <img src="img/reports/report1.png" alt="report1"> -->
                </div>
                <div class="col-md-8">
                  <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details',  'slug' => $product['Product']['slug'])); ?> ">
                        <h5><?php echo $product['Product']['product_name']; ?></h5>
                        <p>
                          <?php echo $this->Text->truncate(
                                      strip_tags($product['Product']['product_description']), 150,
                                                  array('ellipsis' => '...','exact' => false)
                                                );?>
                        </p>
                    </a>
                </div>
                <div class="col-md-2 text-center">
                  <h5>
                    <?php $final_price = round(($product['Product']['price'] * $dollar_rate), 2); ?>
                    <?php if ($cur_curency == 0): ?>
                        <b><i class="fa fa-inr"></i></b><?php echo $final_price; ?>
                    <?php else: ?>
                        <b><i class="fa fa-dollar"></i> </b>
                        <?php echo number_format($final_price, 2); ?>
                    <?php endif; ?>
                  </h5>

                </div>
                <div class="col-md-7 col-sm-12 col-xs-12">
                  <div class="pull-left date-left">
                     <h6 class=""><i class="fa fa-calendar"></i>
                       <?php if (!empty($product['Product']['pub_date']) && $product['Product']['pub_date']!= "0000-00-00" && $product['Product']['pub_date']!= "1970-01-01"): ?>
                           <?php echo Date('F Y', strtotime($product['Product']['pub_date'])); ?>
                       <?php else: ?>
                           <?php echo Date('F Y', strtotime("+1 month")); ?>
                       <?php endif; ?>
                      </h6>
                   </div>
                   <div class="pull-right enq-btn">
                     <?php
                        echo $this->Html->link('Enquiry Before Buying', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','slug' => $product['Product']['slug'], 'ref_page' => 'enquiry-before-buying'), array("target" => "_blank"));
                     ?>
                   </div>

                </div>
                <div class="col-md-3 pull-left req-btn">
                  <?php
                     echo $this->Html->link('Request Sample', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','slug' => $product['Product']['slug'], 'ref_page' => 'request-sample'), array("target" => "_blank"));
                  ?>
               </div>
             </div>
           <?php } ?>
         <?php } ?>

       </div>

       <div class="<?php if($cat_data['Level2'][0]['no_of_product']>0){ echo 'col-md-12'; }else{ echo 'col-md-12'; }?> text-center">

         <ul class="pagination">
          <?php if (isset($this->params['paging']['ProductCategory']['pageCount'])) {
               $page_cnt = $this->params['paging']['ProductCategory']['pageCount'];
               $current = $this->params['paging']['ProductCategory']['page'];
           } elseif (isset($this->params['paging']['ProductCategory']['pageCount'])) {
               $page_cnt = $this->params['paging']['ProductCategory']['pageCount'];
               $current = $this->params['paging']['ProductCategory']['page'];
           }
          ?>
           <?php if ($current > 2): ?>
               <a href="<?php echo Router::url("/", true); ?>industries/<?php echo $paginator_url ?>-page-<?php echo $current - 1 ?>">&lt; &nbsp; Previous</a>
           <?php elseif ($current == 2): ?>
               <a href="<?php echo Router::url("/", true); ?>industries/<?php echo $paginator_url ?>">&lt; &nbsp; Previous</a>
           <?php else: ?>
               &lt; &nbsp; Previous &nbsp;
           <?php endif; ?>
           &nbsp;&nbsp;|&nbsp;&nbsp;
           <?php if ($page_cnt != $current && $page_cnt > 0): ?>
               <a href="<?php echo Router::url("/", true); ?>industries/<?php echo $paginator_url ?>-page-<?php echo $current + 1 ?>">Next &nbsp; &gt;</a>
           <?php else: ?>
               Next &nbsp; &gt;
           <?php endif; ?>
           <br>
           <small><?php
           echo $this->Paginator->counter(array(
               'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
           ));
           ?></small>

        </ul>
      </div>
  </div>

</section>

<!-- ABOUT INNER SECTION END -->

<script type="text/javascript">
$('.msg').hide();
$('#more').click(function () {
    $('.msg').show();
    $('.msg1').hide();
});
$('#less').click(function () {
    $('.msg1').show();
    $('.msg').hide();
});
</script>
