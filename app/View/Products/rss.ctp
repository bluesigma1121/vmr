<?php echo "<?xml version='1.0' encoding='UTF-8'?>" ?>
<rss version="2.0">
    <channel>
        <title>Latest Market Research Reports</title>
        <link><?php echo Router::url('/', true) . "reports/latest-market-reports"; ?></link>
        <description>Value Market Research XML Feed</description>
        <language>en-us</language>
        <copyright>Copyright valuemarketresearch</copyright>
        <pubDate><?php echo date('Y-m-d H:i:s'); ?></pubDate>
        <lastBuildDate><?php echo date('Y-m-d H:i:s'); ?></lastBuildDate>
        <ttl>300</ttl>
        <?php foreach ($products as $product): ?>
            <?php if (!empty($product['Product']['pub_date']) && $product['Product']['pub_date'] != "0000-00-00" && $product['Product']['pub_date'] != "1970-01-01"): ?>
                <?php $dt = Date('F Y', strtotime($product['Product']['pub_date'])); ?>
            <?php else: ?>
                <?php $dt = Date('F Y', strtotime("+1 month")); ?>
            <?php endif; ?>
            <item>
                <title><?php echo trim($product['Product']['alias']); ?></title>
                <link><?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($product['main_cat']['slug']), 'id' => $product['Product']['id'], 'slug' => $product['Product']['slug']), true); ?></link>
                <description><?php
                    echo $this->Text->truncate(
                            strip_tags($product['Product']['product_description']), 380, array(
                        'ellipsis' => '...',
                        'exact' => false
                            )
                    );
                    ?></description>
                <pubDate><?php echo trim($dt); ?></pubDate>
            </item>
        <?php endforeach; ?>
    </channel>
</rss>
