<script type="text/javascript">
    $(document).ready(function () {
        $('#add_product').bValidator();

        /*  $('#prnt_of_chld1').change(function() {
         $("#div2").hide();
         $("#prnt_of_chld2").prop('disabled', true);
         $("#div3").hide();
         $("#prnt_of_chld3").prop('disabled', true);
         $("#div4").hide();
         $("#prnt_of_chld4").prop('disabled', true);
         var id = $('#prnt_of_chld1').val();
         var lname = '';
         var lname = $("#prnt_of_chld1 option:selected").text();
         $('#parent_cat_id_per').val(id);
         if (id != '') {
         var base_path = "<?= Router::url('/', true) ?>";
         var urls = base_path + "categories/dropdwn_fill_add/" + id;
         $.ajax({
         url: urls,
         dataType: 'json'
         }).done(function(data) {
         if (data.length != 0) {
         $("#div2").show();
         $("#prnt_of_chld2").prop('disabled', false);
         $("#prnt_of_chld2").empty();
         $("#prnt_of_chld2").append(
         $("<option></option>").text('--SubCategory--').val('')
         );
         $.each(data, function(i, val) {
         $("#prnt_of_chld2").append(
         $("<option></option>").text(val).val(i)
         );
         });
         }
         else {
         
         $("#div2").hide();
         $("#prnt_of_chld2").prop('disabled', true);
         $("#div3").hide();
         $("#prnt_of_chld3").prop('disabled', true);
         $("#div4").hide();
         $("#prnt_of_chld4").prop('disabled', true);
         }
         });
         }
         else {
         $("#div2").hide();
         $("#prnt_of_chld2").prop('disabled', true);
         $("#prnt_of_chld2").empty();
         $("#prnt_of_chld2").append(
         $("<option></option>").text('--Select--').val('')
         );
         }
         });
         
         $('#prnt_of_chld2').change(function() {
         $("#div3").hide();
         $("#prnt_of_chld3").prop('disabled', true);
         $("#div4").hide();
         $("#prnt_of_chld4").prop('disabled', true);
         var id = $('#prnt_of_chld2').val();
         var lname = '';
         var lname = $("#prnt_of_chld2 option:selected").text();
         if (id != '') {
         var base_path = "<?= Router::url('/', true) ?>";
         var urls = base_path + "categories/dropdwn_fill_add/" + id;
         $.ajax({
         url: urls,
         dataType: 'json'
         }).done(function(data) {
         if (data.length != 0) {
         $("#div3").show();
         $("#prnt_of_chld3").prop('disabled', false);
         $("#prnt_of_chld3").empty();
         $("#prnt_of_chld3").append(
         $("<option></option>").text('--SubCategory--').val('')
         );
         $.each(data, function(i, val) {
         $("#prnt_of_chld3").append(
         $("<option></option>").text(val).val(i)
         );
         });
         }
         else {
         
         $("#div3").hide();
         $("#prnt_of_chld3").prop('disabled', true);
         $("#div4").hide();
         $("#prnt_of_chld4").prop('disabled', true);
         }
         });
         }
         else {
         $("#div3").hide();
         $("#prnt_of_chld3").prop('disabled', true);
         $("#prnt_of_chld3").empty();
         }
         });
         
         $('#prnt_of_chld3').change(function() {
         $("#div4").hide();
         $("#prnt_of_chld4").prop('disabled', true);
         var id = $('#prnt_of_chld3').val();
         var lname = '';
         var lname = $("#prnt_of_chld3 option:selected").text();
         if (id != '') {
         var base_path = "<?= Router::url('/', true) ?>";
         var urls = base_path + "categories/dropdwn_fill_add/" + id;
         $.ajax({
         url: urls,
         dataType: 'json'
         }).done(function(data) {
         if (data.length != 0) {
         $("#div4").show();
         $("#prnt_of_chld4").prop('disabled', false);
         $("#prnt_of_chld4").empty();
         $("#prnt_of_chld4").append(
         $("<option></option>").text('--SubCategory--').val('')
         );
         $.each(data, function(i, val) {
         $("#prnt_of_chld4").append(
         $("<option></option>").text(val).val(i)
         );
         });
         }
         else {
         
         $("#div4").hide();
         $("#prnt_of_chld4").prop('disabled', true);
         }
         });
         }
         else {
         $("#div4").hide();
         $("#prnt_of_chld4").prop('disabled', true);
         $("#prnt_of_chld4").empty();
         $("#prnt_of_chld4").append(
         $("<option></option>").text('--Select--').val('')
         );
         }
         }); */

    });
</script>
<style>
    .bread{
        margin-bottom: 10px;
    }
</style>
<?php echo $this->Html->script('back/ckeditor/ckeditor.js'); ?>

<div class="ibox float-e-margins"> 
    <div id="form" action="#" class="wizard-big wizard clearfix" role="application" novalidate="novalidate">
        <div class="bread">
            <ol class="breadcrumb">
                <li>
                    <a href="#">Products</a>
                </li>
                <?php if (!empty($prod_dt['Category']['category_name'])) : ?>
                    <li class="active">
                        <strong><?php echo $prod_dt['Category']['category_name']; ?></strong>
                    </li>
                <?php endif; ?>
            </ol>
        </div>
        <div class="steps clearfix"><ul role="tablist">
                <li  class="first current" aria-disabled="false" aria-selected="true">
                    <a  href="#">
                    </span> Add Product  <span class="badge badge-warning">Active</span> </a>
                </li>
                <li  class="disabled" style="width:200px;"><a  href="#" > Add Categories</a></li>
                <li  class="disabled" style="width:200px;"><a  href="#" > Add FAQ</a></li>                
                <li  class="disabled" style="width:200px;"><a  href="#" > Add Specification</a></li>
                <li  class="disabled last" style="width:200px;"><a  href="#" > Add Countries</a></li>
            </ul>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <!--        <div class="ibox-title">
                    <h5> Add Product</h5>
                </div>-->
        <div class="ibox-content">
            <?php echo $this->Form->create('Product', array('id' => 'add_product', 'class' => 'form-horizontal', 'type' => 'file')); ?>
            <div class="row">
                <div class="form-group"><label class="col-lg-2 control-label">Product Name</label>
                    <div class="col-lg-4">
                        <?php echo $this->Form->input('product_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Product Name', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>

                <div class="form-group"><label class="col-lg-2 control-label">Product Alias</label>
                    <div class="col-lg-4">
                        <?php echo $this->Form->input('alias', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Product Alias', 'label' => false)); ?>
                    </div>
                </div>            

                <div class="form-group"><label class="col-lg-2 control-label">Publisher Name</label>
                    <div class="col-lg-4">
                        <?php echo $this->Form->input('publisher_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Publisher Name', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>

                <div class='form-group'>
                    <label class="col-lg-2 control-label">Request TOC</label>
                    <div class="col-lg-6">
                        <?php echo $this->Form->checkbox('is_set_toc', array('value' => 0)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Product TOC</label>
                    <div class="col-lg-10">
                        <?php echo $this->Form->input('product_specification', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Product Specification', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="col-lg-2 control-label">Related News</label>
                    <div class="col-lg-10">
                        <?php echo $this->Form->input('related_news', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Related News', 'label' => false)); ?>
                    </div>
                </div> 
                <div class="form-group"><label class="col-lg-2 control-label">Product Snapshot</label>
                    <div class="col-lg-10">
                        <?php echo $this->Form->input('product_snapshot', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Product Snapshot', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group"><label class="col-lg-2 control-label">Product Description</label>
                    <div class="col-lg-10">
                        <?php echo $this->Form->input('product_description', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Product Description', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">Summary</label>
                    <div class="col-lg-10">
                        <?php echo $this->Form->input('summary', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Related News', 'label' => false)); ?>
                    </div>
                </div> 

                <div class="form-group">
                    <label class="col-lg-2 control-label">Glossary</label>
                    <div class="col-lg-10">
                        <?php echo $this->Form->input('glossary', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Related News', 'label' => false)); ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-lg-2 control-label">Customizations</label>
                    <div class="col-lg-10">
                        <?php echo $this->Form->input('customization', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'label' => false)); ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-lg-2 control-label">Industry Analysis</label>
                    <div class="col-lg-10">
                        <?php echo $this->Form->input('industry_analysis', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'label' => false)); ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-lg-2 control-label">Latest Developments</label>
                    <div class="col-lg-10">
                        <?php echo $this->Form->input('latest_dev', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'label' => false)); ?>
                    </div>
                </div>

                <!--<div class="form-group"  id="main_div">
                    <label class="col-lg-3 control-label">Select Primary Category</label>
                    <div class="col-lg-4">
                        <?php
                        echo $this->Form->input('category_id', array(
                            'options' => $category,
                            'empty' => '--Select--', 'class' => 'form-control hastip', 'id' => 'prnt_of_chld1', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                        ?>
                    </div>
                </div>--> 
                <?php /*  <div class="form-group" id="div2" hidden>
                  <label class="col-lg-3 control-label">Select Category</label>
                  <div class="col-lg-4">
                  <?php
                  echo $this->Form->input('parent_category_id2', array(
                  'options' => '',
                  'empty' => '--Select--', 'class' => 'form-control hastip', 'id' => 'prnt_of_chld2', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                  ?>
                  </div>
                  </div>

                  <div class="form-group" id="div3" hidden>
                  <label class="col-lg-3 control-label">Select Category</label>
                  <div class="col-lg-4">
                  <?php
                  echo $this->Form->input('parent_category_id3', array(
                  'options' => '',
                  'empty' => '--Select--', 'class' => 'form-control hastip', 'id' => 'prnt_of_chld3', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                  ?>
                  </div>
                  </div>

                  <div class="form-group" id="div4" hidden>
                  <label class="col-lg-3 control-label">Select Category</label>
                  <div class="col-lg-4">
                  <?php
                  echo $this->Form->input('parent_category_id4', array(
                  'options' => '',
                  'empty' => '--Select--', 'class' => 'form-control hastip', 'id' => 'prnt_of_chld4', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                  ?>
                  </div>
                  </div> */ ?>
                <div class="form-group"><label class="col-lg-2 control-label">Product Image</label>
                    <div class="col-lg-4">
                        <?php echo $this->Form->input('product_image', array('class' => 'form-control', 'type' => 'file', 'placeholder' => 'Product Image', 'label' => false, 'data-bvalidator' => '')); ?>
                    </div>
                </div>
                <div class="form-group"><label class="col-lg-2 control-label">Single User License(In Dollar)</label>
                    <div class="col-lg-4">
                        <?php echo $this->Form->input('price', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Price', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group"><label class="col-lg-2 control-label">Upto 10 Users License(In Dollar)</label>
                    <div class="col-lg-4">
                        <?php echo $this->Form->input('upto10', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Price', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group"><label class="col-lg-2 control-label">Corporate User License(In Dollar)</label>
                    <div class="col-lg-4">
                        <?php echo $this->Form->input('corporate_price', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Price', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group"><label class="col-lg-2 control-label">Data Pack License(In Dollar)</label>
                    <div class="col-lg-4">
                        <?php echo $this->Form->input('data_pack_price', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Price', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group"><label class="col-lg-2 control-label">Published</label>
                    <div class="col-lg-2">
                        <?php echo $this->Form->input('pub_month', array('class' => 'form-control', 'type' => 'select', 'options' => $months, 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                    <div class="col-lg-2">
                        <?php echo $this->Form->input('pub_year', array('class' => 'form-control', 'type' => 'select', 'options' => array_combine(range(1950, 2050), range(1950, 2050)), 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div> 

                <div class='form-group'>
                    <label class="col-lg-2 control-label">Is Upcoming Report?</label>
                    <div class="col-lg-6">
                        <?php echo $this->Form->checkbox('is_upcoming'); ?>
                    </div>
                </div>
                
                <div class='form-group'>
                    <label class="col-lg-2 control-label">Is Uploaded Without Data?</label>
                    <div class="col-lg-6">
                        <?php echo $this->Form->checkbox('is_data'); ?>
                    </div>
                </div>
                
                <?php /*
                  <div class="form-group"><label class="col-lg-3 control-label">Tags</label>
                  <div class="col-lg-4">
                  <?php echo $this->Form->input('tags', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Tags', 'label' => false, 'data-bvalidator' => 'required')); ?>
                  </div>
                  </div>
                 */ ?>
                <?php /* <div class="form-group">
                  <label class="col-lg-3 control-label">Price</label>
                  <div class="col-lg-5">
                  <div class="col-lg-6">
                  <?php echo $this->Form->input('price', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Price', 'label' => false, 'data-bvalidator' => 'required')); ?>
                  </div>
                  <div class="col-lg-6">
                  <label class="col-lg-3 control-label">Dollar</label>
                  <div class="col-lg-9">
                  <?php echo $this->Form->input('dollar_price', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Price', 'label' => false, 'data-bvalidator' => 'required')); ?>
                  </div>
                  </div>
                  </div>
                  </div> */ ?>


                <div class = "form-group"><label class = "col-lg-2 control-label">Meta Title</label>
                    <div class = "col-lg-4">
                        <?php echo $this->Form->input('meta_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Meta Name', 'label' => false, 'data-bvalidator' => 'required'));
                        ?>
                    </div>
                </div>
                <div class="form-group"><label class="col-lg-2 control-label">Meta Description</label>
                    <div class="col-lg-4">
                        <?php echo $this->Form->input('meta_desc', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Meta Description', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group"><label class="col-lg-2 control-label">Meta Keywords</label>
                    <div class="col-lg-4">
                        <?php echo $this->Form->input('meta_keywords', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Meta Keywords', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class = "form-group"><label class = "col-lg-2 control-label">Rating</label>
                    <div class = "col-lg-4">
                        <?php echo $this->Form->input('schema_data', array('class' => 'form-control', 'type' => 'number', 'placeholder' => 'Rating', 'label' => false, 'data-bvalidator' => 'required','name'=>'schema_data[rating][]','value' => "4.2",'step'=>'0.01','min'=>'3.8','max'=>'4.8'));
                        ?>
                    </div>
                </div>
                <div class = "form-group"><label class = "col-lg-2 control-label">Aggregate Rating</label>
                    <div class = "col-lg-4">
                        <?php echo $this->Form->input('schema_data', array('class' => 'form-control', 'type' => 'number', 'placeholder' => 'Aggregate Rating', 'label' => false, 'data-bvalidator' => 'required','name'=>'schema_data[aggregate_rating][]','value' => "4.2",'step'=>'0.01','min'=>'3.8','max'=>'4.8'));
                        ?>
                    </div>
                </div>
                <div class = "form-group"><label class = "col-lg-2 control-label">Review Count</label>
                    <div class = "col-lg-4">
                        <?php echo $this->Form->input('schema_data', array('class' => 'form-control', 'type' => 'number', 'placeholder' => 'Review Count', 'label' => false, 'data-bvalidator' => 'required','name'=>'schema_data[reviewCount][]','value' => "80",'step'=>"0.01"));
                        ?>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>