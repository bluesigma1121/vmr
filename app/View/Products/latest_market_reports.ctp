<style>
    hr {
        margin: 15px 0;
    }
</style>
<div class="container content">
    <div class="headline"><h2>Latest Market Research Reports</h2></div>
    <?php foreach ($products as $key => $product): ?>
        <div class="row clients-page">
            <div class="col-md-12">
                <span class="title-price h4_fnt">
                    <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($product['main_cat']['slug']), 'id' => $product['Product']['id'], 'slug' => $product['Product']['slug'])); ?> "> <?php echo $product['Product']['alias']; ?></a>
                </span>
                <ul class="list-inline">
                    <li>
                        <i class="fa fa-calendar color-green"></i>&nbsp;&nbsp;
                        <strong>Publish Date: 
                            <?php if (!empty($product['Product']['pub_date']) && $product['Product']['pub_date'] != "0000-00-00" && $product['Product']['pub_date'] != "1970-01-01"): ?>
                                <?php echo Date('F Y', strtotime($product['Product']['pub_date'])); ?>
                            <?php else: ?>
                                <?php echo Date('F Y', strtotime("+1 month")); ?>
                            <?php endif; ?>
                        </strong>
                    </li>
                    <li><i class="fa fa-money color-green"></i>&nbsp;&nbsp;
                        <strong>Price: $<?php echo $product['Product']['price']; ?></strong>
                    </li>
                </ul>
                <p>
                    <?php
                    echo $this->Text->truncate(
                            strip_tags($product['Product']['product_description']), 380, array(
                        'ellipsis' => '&nbsp;',
                        'exact' => false
                            )
                    );
                    ?>
                    <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($product['main_cat']['slug']), 'id' => $product['Product']['id'], 'slug' => $product['Product']['slug'])); ?> ">read more..</a>
                </p>
            </div>
        </div>
        <hr/>
    <?php endforeach; ?>
</div>