<?php
$my_cat_color = array(
    1 => 'primary',
    2 => 'success',
    3 => 'info',
    4 => 'warning',
        )
?> 
<!--=== Search Block ===-->
<?php echo $this->element('listing_search'); ?>    
<!--=== End Search Block ===--> 
<?php echo $this->element('front_breadcumb'); ?>    
<div class="job-description">
    <div class="container content mgr_tp_pd">  
        <div class="row">
            <!-- Left Inner -->
            <div class="col-md-8">
                <div class="left-inner section-block">
                    <div class="row">
                        <div class="col-md-12 margin_bot_home">
                            <a href="#">
                                <?php if (!empty($product['Product']['product_image'])) { ?>
                                    <?php
                                    echo $this->Html->image('product_images/' . $product['Product']['product_image'], array('class' => 'img-thumbnail image_size_home'));
                                } else {
                                    echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail image_size_home'));
                                }
                                ?>
                            </a>
                            <h4><?php echo $product['Product']['product_name']; ?></h4>
                            <div class="overflow-h">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <ul class="details list-inline">
                                            <li id="first">ID:<?php echo $product['Product']['product_no']; ?></li>
                                            <?php /*
                                              if (!empty($pro_country)) {
                                              ?>  <li class="spart_pd">Countries :<?php echo implode(',', $pro_country); ?></li><?php } */ ?>
                                            <?php if (!empty($product['Product']['pub_date'])) { ?>
                                                <li class="spart_pd"><?php echo Date('F Y', strtotime($product['Product']['pub_date'])); ?></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4> <b>  <?php
                                                $final_price = round(($product['Product']['price'] * $dollar_rate), 2);
                                                if ($cur_curency == 0) {
                                                    ?>
                                                    <b><i class="fa fa-inr"></i> </b><?php echo round($final_price, 2); ?>
                                                <?php } else {
                                                    ?>
                                                    <b><i class="fa fa-dollar"></i> </b><?php
                                                    echo number_format($final_price, 2);
                                                }
                                                ?></b></h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5"> 
                                        <?php echo $this->Form->create('CartItem', array('controller' => 'cart_items', 'action' => 'cart', 'class' => '', 'id' => 'cart_frm')) ?>               
                                        <?php echo $this->Form->input('product_id', array('type' => 'hidden', 'value' => $product['Product']['id'])) ?>
                                        <?php echo $this->Form->button('<i class="fa fa-shopping-cart"></i> Add to Cart', array('class' => 'btn-u btn-u-blue', 'escape' => false)); ?>
                                        <?php echo $this->Form->end(); ?>
                                    </div>
                                    <div class="col-md-2"> 
                                    </div>
                                    <div class="col-md-5"> 
                                        <?php //echo $this->Html->link(__(' Send Inquiry'), array('controller' => 'enquiries', 'action' => 'send_enquiry', $product['Product']['id'], $product['ProductCategory'][0]['category_id'], 'category_details'), array('class' => 'fa fa-bell-o btn-u btn-u-green send_enquery_dtl', 'title' => 'Send Inquiry')); ?>                               
                                        <?php
                                        //  echo $this->Html->link(__('<i class="fa fa-bell-o"></i> Send Inquiry'), array('controller' => 'enquiries', 'action' => 'send_enquiry', $product['Product']['id'], $selected_cat['Category']['id'], 'category_listing'), array('class' => 'btn-u btn-u-green send_enquery', 'escape' => false, 'title' => 'Send Inquiry'));
                                        $url_gen = Router::url('/', true) . 'enquiries/send_enquiry/' . $product['Product']['id'] . '/' . $product['ProductCategory'][0]['category_id'] . '/category_details';
                                        ?>
                                        <button 
                                            id="call_me_<?php echo $product['Product']['id']; ?>" 
                                            class="btn-u btn-u-green" 

                                            rel="<?php echo $product['Product']['id']; ?>" 
                                            onclick="getPopupForm(this.id, '<?php echo $url_gen; ?>');" >
                                            <i class="fa fa-mail-forward">   </i> Send Inquiry

                                        </button>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <hr style="margin:10px;">
                        <div class="tab-v1">
                            <ul class="nav nav-justified nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#profile">Description</a></li>
                                <li><a data-toggle="tab" href="#passwordTab">Table Of Content</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="profile" class="profile-edit tab-pane fade in active margin_p_pd">
                                    <p>
                                        <?php echo $product['Product']['product_description']; ?>
                                    </p>
                                </div>



                                <div id="passwordTab" class="profile-edit tab-pane fade margin_p_pd">
                                    <?php if ($product['Product']['is_set_toc'] == 0) { ?>
                                        <p>
                                            <?php echo $product['Product']['product_specification']; ?>
                                        </p>
                                    <?php } else { ?>


                                        <?php echo $this->Form->create('Enquiry', array('id' => 'enquiry_form', 'class' => 'reg-page', 'type' => 'file')); ?>
                                        <div class="reg-header mar_top_hed">
                                            <h2>Request TOC</h2>
                                        </div> 
                                        <?php if (!AuthComponent::user('id')): ?>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>Title<span class="color-red">*</span></label> 
                                                    <?php echo $this->Form->input('title', array('class' => 'form-control', 'type' => 'select', 'options' => $title, 'placeholder' => 'Title', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>First Name<span class="color-red">*</span></label> 
                                                    <?php echo $this->Form->input('first_name', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'First Name', 'data-bvalidator' => 'required,alpha')); ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Last Name<span class="color-red">*</span></label>
                                                    <?php echo $this->Form->input('last_name', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Last Name', 'data-bvalidator' => 'required,alpha')); ?>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="col-sm-6">
                                                    <label>Organisation <span class="color-red">*</span></label> 
                                                    <?php echo $this->Form->input('organisation', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Organisation', 'data-bvalidator' => 'required')); ?>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label>Designation <span class="color-red">*</span></label> 
                                                    <?php echo $this->Form->input('job_title', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Designation', 'data-bvalidator' => 'required')); ?>
                                                </div> 
                                                <div class="col-sm-8">
                                                    <label>Email Address <span class="color-red">*</span></label>
                                                    <?php echo $this->Form->input('email', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Email', 'data-bvalidator' => 'required,email')); ?>
                                                </div>

                                            </div>
                                        <?php endif; ?>
                                        <?php /* <div class="row">
                                          <div class="col-sm-6">
                                          <label>Mobile <span class="color-red">*</span></label>
                                          <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => '+_(___)___-__-__', 'id' => 'customer_phone', 'value' => AuthComponent::user('mobile'), 'size' => '20', 'maxlength' => '25', 'data-bvalidator' => 'required')); ?>
                                          </div>
                                          <div class="col-sm-6">
                                          <div class="mar_phone">
                                          <input type="checkbox" id="phone_mask" checked=""> <label id="descr" for="phone_mask"></label>
                                          </div>
                                          </div>
                                          </div> */ ?>
                                        <div class="row">
                                            <div class="col-sm-6">

                                                <label>Mobile <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Mobile', 'data-bvalidator' => 'required')); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Subject <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('subject', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Subject', 'data-bvalidator' => 'required')); ?>
                                            </div>
                                        </div>
                                        <div class="row"> 
                                            <div class="col-sm-12">
                                                <label>Message <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('message', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'textarea', 'placeholder' => 'Message', 'data-bvalidator' => 'required')); ?>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Captcha   <span class="color-red">*</span></label>
                                                <?php echo $this->Html->image($this->Html->url(array('controller' => 'users', 'action' => 'captcha'), true), array('label' => false, 'width' => '150', 'height' => '40', 'alt' => 'captcha')); ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Enter Code <span class="color-red">*</span></label>
                                                <?php
                                                echo $this->Form->input('captcha', array('data-bvalidator' => 'required', 'class' => 'input-large form-control hastip', 'data-placement' => 'right', 'type' => 'text', 'placeholder' => 'Enter Captcha Code',
                                                    'title' => 'Enter the characters you see in the image.', 'label' => false));
                                                ?>
                                            </div>
                                            <div class='hidden'>
                                                <?php echo $this->Form->input('ref_page', array('value' => 'Request_TOC')) ?>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?php echo $this->Form->submit('Send', array('class' => 'btn btn-lg btn-info')) ?>
                                            </div>
                                        </div>
                                        <?php echo $this->Form->end(); ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                <div class="row row_mar">
                    <div class="col-lg-12">
                        <div class="headline"><h4>Related Categories</h4></div>
                        <?php
                        $i = 0;
                        foreach ($cat_array as $key => $cat_name) {
                            $i++;
                            ?>
                            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $key, 'slug' => $this->Link->cleanString($cat_name))); ?>" class="btn-u btn-u-xs rounded-4x btn-u-dark mar_cat">
                                <?php echo $cat_name; ?>
                            </a>
                            <?php
                            if ($i == 4) {
                                $i = 0;
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Left Inner -->

    <!-- Right Inner -->
    <div class="col-md-4 section-block">                    
        <div class="headline"><h4>Related Product</h4></div>
        <?php
        foreach ($related_product as $key => $prod) {
            if (!empty($prod['Product']['id'])) {
                ?>
                <div class="right-inner_pd">
                    <div class="people-say">
                        <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $prod['main_cat']['slug'], 'id' => $prod['Product']['id'], 'slug' => $prod['Product']['slug'])); ?>">
                            <?php if (!empty($prod['Product']['product_image'])) { ?>
                                <?php
                                echo $this->Html->image('product_images/' . $prod['Product']['product_image'], array('class' => 'img-thumbnail image_size_home'));
                            } else {
                                echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail image_size_home'));
                            }
                            ?>
                        </a>
                        <div class="overflow-h">
                            <span>  <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $prod['main_cat']['slug'], 'id' => $prod['Product']['id'], 'slug' => $prod['Product']['slug'])); ?> "> <?php echo $this->Text->truncate($prod['Product']['product_name'], 50); ?></a> </span><br>
                            <ul class="details list-inline">
                                <li id="first"><h4><b> <?php
                                            $final_price = round(($prod['Product']['price'] * $dollar_rate), 2);
                                            if ($cur_curency == 0) {
                                                ?>
                                                <b><i class="fa fa-inr"></i> </b><?php echo round($final_price, 2); ?>
                                            <?php } else {
                                                ?>
                                                <b><i class="fa fa-dollar"></i> </b><?php
                                                echo number_format($final_price, 2);
                                            }
                                            ?></b></h4></li>
                            </ul>
                            <ul class="details list-inline">
                                <?php if (!empty($prod['Product']['pub_date'])) { ?>
                                    <li class="first"><?php echo Date('F Y', strtotime($prod['Product']['pub_date'])); ?></li>
                                    <?php } ?>
                            </ul>
                        </div>    
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
    <!-- End Right Inner -->
</div>    
</div>   
</div>