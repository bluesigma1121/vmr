<script type="text/javascript">
    $(document).ready(function() {
        $('#add_country').bValidator();
        $('#country').multiselect({
            selectedList: 10
        }).multiselectfilter();
    });
</script> 
<style>
    .bread{
        margin-bottom: 10px;
    }
    .breadcrumb>li+li:before {
        padding: 0 5px;
        color: #ccc;
        content: ">>\00a0" !important;
    }
</style>
<div class="ibox float-e-margins"> 
    <div id="form" action="#" class="wizard-big wizard clearfix" role="application" novalidate="novalidate">
        <div class="bread">
            <ol class="breadcrumb">
                <li>
                    <a href="#">Products</a>
                </li>
                <?php if (!empty($product['Category']['category_name'])) : ?>
                    <li class="active">
                        <strong><?php echo $product['Category']['category_name']; ?></strong>
                    </li>
                <?php endif; ?>
            </ol>
        </div>
        <div class="steps clearfix"><ul role="tablist">
                <li  class="first current" aria-disabled="false" aria-selected="true">
                    <a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'edit', $pro_id)) ?>">
                         Edit Product</a></li>
                <li  class="first current" style="width:200px;"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'edit_category', $pro_id)) ?>" > Edit Categories</a></li>
                <?php  if (!empty($product['Product']['product_faq'])) { ?>
                    <li  class="first current" style="width:200px;"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'edit_faq', $pro_id)) ?>" > Edit FAQ</a></li>
                <?php } else { ?>
                    <li  class="first current" style="width:200px;"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'add_faq', $pro_id)) ?>" > Add FAQ</a></li>
                <?php }  ?>                
                <li  class="first current" style="width:200px;"><a  href="<?php echo Router::url(array('controller' => 'specifications', 'action' => 'add_product_spc', $pro_id)) ?>" > Edit Specification</a></li>
                <?php if ($res == 1) { ?>
                    <li  class="first current"  title="Active" style="width:200px;"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'add_product_countries', $pro_id)) ?>" > Edit Countries  <span class="badge badge-warning">Active</span></a></li>
                    <?php
                } else {
                    ?>
                    <li class = "first current" title = "Active" style="width:200px;"><a href = "<?php echo Router::url(array('controller' => 'products', 'action' => 'add_product_countries', $pro_id)) ?>" > Add Countries  <span class="badge badge-warning">Active</span></a></li>
                <?php }
                ?>
            </ul>
        </div>
    </div>
    <!--    <div class="ibox-title">
            <h5>Add Countries</h5>
        </div>-->
    <div class="ibox-content">
        <?php echo $this->Form->create('Product', array('id' => 'add_country', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-3">
            <div class="form-group"><label class="col-lg-2 control-label">Product Name</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('product_name', array('class' => 'form-control', 'type' => 'text', 'value' => $product['Product']['product_name'], 'readonly' => 'readonly', 'label' => false)); ?>
                </div>
            </div>
        </div>
        <div class="row col-lg-offset-3">
            <div class="form-group"><label class="col-lg-2 control-label">Add Cities</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('country', array('multiple' => 'multiple', 'options' => $country, 'selected' => $select_country, 'id' => 'country', 'class' => 'form-control', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
            <?php
            if ($product['Product']['is_active'] == 0) {
                echo $this->Html->link(__('Activate Product'), array('controller' => 'products', 'action' => 'active', $product['Product']['id'], 'pending_status'), array('class' => 'btn btn-sm btn-success', 'title' => 'Active Product'));
            }
            ?>
            <?php echo $this->Form->end(); ?>
        </div>
        <button onclick="goBack();" class="btn btn-sm btn-info">Back</button>

    </div>
</div>