

<style>
@media (max-width:450px) and (min-width:20px){.tab-content div img{width:265px!important;height:auto!important;margin-left:calc(100% - 262px)}}@media only screen and (mix-width:375px) and (max-width:667px){.tab-content div img{width:300px!important;height:auto!important;margin-left:calc(100% - 304px)}}.key-qust .panel-group{min-height:338px}.key-qst-left li{line-height:31px}.req-free-btn{margin-top:20px;width:100%}.req-free-btn .btn{background:#0074bd;border:none;border-radius:0;padding:5px 10px;color:#fff;font-weight:400;font-size:14px;width:100%}.req-free-btn .btn:hover{background:#333;color:#fff}.carousel-inner>.item{padding-left:10px;padding-right:10px}.report-page h1{color:#0074bd;font-weight:700;font-family:Roboto,sans-serif;font-size:14px;line-height:22px;text-align:justify}.btns ul li{margin-bottom:8px}.date-year-info h6{/*display:contents*/}.item{display:grid;grid-template-rows:1fr min-content;align-items:center;justify-content:center;flex-wrap:wrap}#covid-19{background:0 0;color:#fff;border:none;padding:5px 10px;font-size:16px;outline:0;cursor:pointer;position:relative;transition:.2s ease-in-out}.button-rainbow #covid-19{animation:rainbow 3s infinite,heartbeat .6s infinite;background:#c0392b}.button-rainbow #covid-19 .rainbow{width:120px;height:120px;position:absolute;left:0;right:0;margin:auto;top:-40px;transition:.5s ease;opacity:0;transform:rotate(0)}@keyframes rotate-nonstop{50%{transform:rotate(92deg)}}@keyframes omg-yes{50%{opacity:1}100%{top:-50px}}@keyframes rainbow{0%{background:#1abc9c}10%{background:#2ecc71}20%{background:#3498db}30%{background:#9b59b6}40%{background:#e74c3c}50%{background:#e67e22}60%{background:#f1c40f}70%{background:#2c3e50}80%{background:#9b59b6}}@keyframes heartbeat{50%{transform:scale(1.1)}}.payotn img{height:auto}@media (max-width:450px) and (min-width:20px){.report-page img{width:auto}.carousel-control i{margin-top:40px}}

/* .tab-pane fade in active .colortext .marker a{
  background-color: red;
  color: white;
  padding: 1em 1.5em;
  text-decoration: none;
  text-transform: uppercase;
} */
/* .blog-inner a, .report-details-page p {
    font-weight: 400;
    font-size: 14px;
    text-align: justify;
    color: #d23737;
} */
.marker a, .report-details-page p {
    color: blue;
}
  .report-details-page p a{
    color: blue;
}

.m-faq {margin-bottom:0px;;margin:0 16px;}
  .m-faq .panel-group a.collapsed {
        font-size: 16px;
        color: #340f49;
        font-weight: bold;
        text-decoration: none !important;
    }
  .m-faq .panel-group > div:last-child {
        border-bottom: 0px solid #e1e1e1;
    }

  .m-faq .panel-group .m-faq-ans { 
        display: table !important;font-size:14px;color:#423547;margin-bottom:8px;
    }
  .m-faq .m-faq-ans span {
        padding-right:10px;
        height: 100%;
        display: table-cell;
        position:relative;font-size:0;
    }

  .m-faq2 .m-faq-ans span:before {
        content: "\f0da";font-family: fontawesome;color:#5BCAF4;font-size:16px;
    }

    .m-faq2 .panel-body {padding: 12px 16px;}    
    .m-faq2 .panel-default {padding:16px 0;border-bottom:1px solid #BAACC2;margin: 0 !important; background: #f5fafc;}
    .m-faq2 .panel-group .p-heading {}
    .m-faq2 .panel-group .panel  a {text-decoration: none !important; font-size:1rem;margin:0 16px;color:#351249; font-weight: normal;}
    .m-faq6 .panel-group a::before {
        content: "\f0d7";
        display: block;
        color: #5BCAF4;
        font-size:20px;
        font-family: fontawesome;
        position: absolute;
        right:0px;
        top: -3px;
        transition:all 0.15s ease-in-out;
    }
    .m-faq2 .panel-group a.collapsed::before {
        content: "\f0d7";
        display: block;
        color: #5BCAF4; 
        font-size:16px;
        font-family: fontawesome;
        position: absolute;
        right:0px;left:auto;
        top: 0px;
    }
    .m-faq2 .panel-group a::before {
        content: "\f0d8";
        display: block;
        color: #5BCAF4; 
        font-size:16px;
        font-family: fontawesome;
        position: absolute;
        right:0px;left:auto;
        top: 0px;
    }

    .m-faq .panel-group .p-heading a {
        color: #128ab7;
        display: block;
        text-decoration: none !important;
        font-size: 15px;
        font-weight: bold; 
        position: relative;
    }
    #rprt_summary .m-faq .panel-group .m-faq-ans a {
        text-decoration: underline !important;
        display:unset !important;
        font-size:1rem;
        font-weight:normal;
        margin:0;
    }
    .m-faq .panel-group .m-faq-ans a:before {
        display:none;
    }
    .image_size_home:after { content: attr(alt); }
    .client_image:after { content: attr(alt); }
</style>
<?php $my_cat_color = array(1 => 'primary', 2 => 'success', 3 => 'info', 4 => 'warning',) ?>
<script type="text/javascript">$(document).ready(function () { 
  $('#enquiry_form').bValidator();
  $(".image_size_home").each(function(){
        $(this).attr("title", $(this).attr("alt"));
    });
  $(".client_image").each(function(){
      $(this).attr("title", $(this).attr("alt"));
  });
});</script>

<!--=== Search Block ===-->
<!--=== End Search Block ===-->
<!--BANNER SECTION START -->

<div class="banner">
  <div class="bg-color bg-color-inner-main bg-color-inner-report">
    <div class="container">
      <div class="row">
      <table  class="col-md-12 col-xs-12 col-lg-12">
        <tr>
        <td>
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
             <?php echo $this->element('front_breadcumb'); ?>
           </ol>
        </div>
        </td>
      <!-- <td> -->
        <!-- <div class="col-md-12 col-xs-12 col-lg-12 inner-title"> -->
        <!-- <div class="col-md-12 col-xs-12 col-lg-12 inner-title">
            <div class="col-md-10 col-md-push-1">
                <div class="custom-search-form" style="padding-top:0px;">
                <?php echo $this->Form->create('Category', array('type' => 'get','action'=>'home_search_form')); ?>
                    <?php echo $this->Form->input('q', array('class' => 'form-control', 'label' =>false,'required','autocomplete'=>'off','id'=>'search_text')); ?>
                    <div class="col-md-11 pull-left suggest-div" id="suggest"></div>
                    <div class="col-md-1 col-sm-1 input-group-btn pull-right search-btn">
                      <button class="btn btn-primary" type="submit">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                    </div>
                <?php  echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
        </td> -->
        <!-- <div class="col-md-12 col-xs-12 col-lg-12"> -->
      
        </tr>
      </table>
      </div>
     
      
    </div>
  </div>
</div>
<!--BANNER SECTION END -->

<section class="about-inner report-inner-bg">
  <div class="container">
      <div class="blog-inner report-page report-details-page col-md-12">
          <div class="col-md-9">
              <div class="col-md-2 col-xs-12 col-sm-1 report-left-img">
                <?php if (!empty($product['Product']['product_image'])) { ?>
                    <?php
                    echo $this->Html->image('product_images/' . $product['Product']['product_image'], array('class' => 'img-thumbnail image_size_home','alt'=>'Market Research Report'));
                } else {
                    echo $this->Html->image('reports/report1.webp', array('class' => 'img-thumbnail image_size_home','style'=>'margin-left: -49px;','alt'=>'Market Research Report'));
                }
                ?>
              </div>
              <div class="col-md-10 col-xs-12 col-sm-7 padd-0">
                <h1 style="margin-left: -45px;" class="cat_display"><?php echo $product['Product']['product_name']; ?></h1>
                <div class="">
                <div class="date-year-info" style="margin-left: -45px;">
                  <div class="text-center col-md-3 col-xs-12 col-sm-6 padding-0">
                    <h6 class="report-highlights"><i class="fa fa-calendar"></i>
                      <!--ID:<?php echo $product['Product']['product_no']; ?>-->
                    <?php $len = strlen($product['Product']['id']);
                    $id = 11210000+$product['Product']['id'];
                    $regno = 'VMR'.$id;
                    ?>
                      ID:<?php echo $regno; ?>
                    </h6>
                  </div>
                  <div class="text-center col-md-3 col-xs-12 col-sm-6 padding-0">
                    <h6 class=""><i class="fa fa-calendar"></i>
                      <?php if (!empty($product['Product']['pub_date']) && $product['Product']['pub_date'] != "0000-00-00" && $product['Product']['pub_date'] != "1970-01-01"): ?>
                          <?php echo Date('F Y', strtotime($product['Product']['pub_date'])); ?>
                      <?php else: ?>
                          <?php echo Date('F Y', strtotime("+1 month")); ?>
                      <?php endif; ?>
                    </h6>
                  </div>
                  <div class="text-center col-md-6 col-xs-12">
                    <h6 class="report-highlights1">Report Formats: 
                      <?php echo $this->Html->image('format.webp', array('class' => 'img-thumbnail image_size_home','alt'=>'Report Formats')); ?>
                    </h6>
                
                  </div>
                  <?php if($product['Product']['is_upcoming'] == 1){ ?>
                    <div class="text-center col-md-3 col-xs-12 padding-0">
                      <h6 class="report-highlights1"><b>Status: Upcoming</b>
                      </h6>
                    </div>
                  <?php } ?>
                  
                  <!--<div class="pull-left date-left col-md-5 col-xs-12 row">
                    <?php if (!empty($product['Product']['publisher_name'])) { ?>
                        <h6 class=""><i class="fa fa-file-text-o" aria-hidden="true"></i><?php echo $product['Product']['publisher_name']; ?></h6>
                    <?php } ?>
                  </div>-->
                </div>
              </div>
              
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 row donl-btns">
                
                <div class="text-left">
                  <div class="btns" style="margin-left: -45px;">
                    <ul>
                      
                      <!-- <li class="buy-btn"> <?php
                        echo $this->Html->link('Buy Now', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','id' => $product['Product']['id'], 'ref_page' => 'buy-now'), array("class" => "btn-u btn-u-primary", "target" => "_blank"));
                        ?>
                      </li> -->
                      <li class="btn-secondary"><?php
                        echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-download')).' Download Sample', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'download-sample'), array("class" => "btn-u btn-u-green", "target" => "_blank",'escape' => false)
                        );
                        ?>
                      </li>
                      <li class="btn-primary"> <?php
                        echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-question-circle')).' Ask Questions', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'ask-questions'), array("class" => "btn-u btn-u-purple", "target" => "_blank",'escape' => false)
                        );
                        ?>
                      </li>
                      <li class="btn-primary"> <?php
                        echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-edit')).' Request Customization', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'request-customization'), array("class" => "btn-u btn-warning", "target" => "_blank",'escape' => false)
                        );
                        ?>
                      </li>
                      <li class="btn-success"> <?php
                        echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-edit')).' Request Discount', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'request-for-discount'), array("class" => "btn-u btn-success", "target" => "_blank",'escape' => false)
                        );
                        ?>
                      </li>
                      <li class=""> 
                      <div class="item button-rainbow" style="--bg-color: #ecf0f1">
                        <?php echo $this->Html->link('COVID-19 Impact' . $this->Html->tag('div','',array('class' => 'rainbow')), array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'covid-19-impact'), array("id" => "covid-19", "target" => "_blank",'escape' => false)
                        );
                        ?>
                        <!-- <button id="covid-19">COVID-19 Impact <div class="rainbow"></div></button> -->
                      </div>
                      
                      </li>
                    </ul>
                  </div>
                </div>

               <!-- <div class="user hidden-lg hidden-md">
                  <div class="">
                    <div class="btns chooseopt">
                      <h4>Choose your Buying Option</h4>
                      /* <ul>
                        <?php foreach($licence_type as $key=>$value): ?>
                          <li><h6><?php echo $value; ?></h6></li>
                        <?php endforeach; ?>
                      </ul> */
                          <div class="funkyradio">
                              <?php foreach($licence_type as $key=>$value): ?>
                                <div class="funkyradio-warning">
                                    <input type="radio" name="radiomobile" id="radiomobile<?= $key?>" value="<?= $key ?>" onclick="localStorage['buyoption'] = $(this).val();" <?php if($key==1){?>checked<?php }?> />
                                    <label for="radiomobile<?= $key?>"><?php echo $value; ?></label>
                                </div>
                              <?php endforeach; ?>
                        </div>
                        <div class="btns buy-button">
                          <ul>
                            <li class="btn-danger"> 
                              <?php if($product['Product']['is_upcoming'] == 1){
                                echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-shopping-basket')). ' Pre Buy Now', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','slug' => $product['Product']['slug'], 'ref_page' => 'pre-buy-now'), array("class" => "btn-u btn-u-primary", "target" => "_blank",'escape' => false));  
                              } else{
                                echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-shopping-basket')). ' Buy Now', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','slug' => $product['Product']['slug'], 'ref_page' => 'buy-now'), array("class" => "btn-u btn-u-primary", "target" => "_blank",'escape' => false));  
                              }?>
                            </li>
                          </ul>
                        </div>
                    </div>
                  </div>

              </div>--> 
                
              </div>
    
              </div>
              <div class="tablur col-md-12 col-lg-12 col-sm-12 padd-0" style="margin-left: -16px;">
                <div class="panel with-nav-tabs panel-warning">
                <div class="panel-heading">
                  <ul class="nav nav-tabs">
                    <?php if (!empty($product['Product']['product_snapshot'])) { ?>
                        <li class="active"><a data-toggle="tab" href="#snapshot">Snapshot</a></li>
                    <?php } ?>

                    <?php if (empty($product['Product']['product_snapshot'])) { ?>
                      <li class="active"><a data-toggle="tab" href="#profile">Description</a></li>
                    <?php }else{ ?>
                      <li><a data-toggle="tab" href="#profile">Description</a></li>
                    <?php } ?>

                    <li><a data-toggle="tab" href="#passwordTab">Table Of Content</a></li>

                    <?php if (!empty($product['Product']['related_news'])) { ?>
                        <li><a data-toggle="tab" href="#relatednewsshow">Related News</a></li>
                    <?php } ?>

                    <?php if (!empty($product['Product']['summary'])) { ?>
                        <li><a data-toggle="tab" href="#prod_summary">Summary</a></li>
                    <?php } ?>

                    <?php if (!empty($product['Product']['glossary'])) { ?>
                        <li><a data-toggle="tab" href="#glossary">Glossary</a></li>
                    <?php } ?>

                    <?php if (!empty($product['Product']['customization'])) { ?>
                        <li><a data-toggle="tab" href="#customization">Customizations</a></li>
                    <?php } ?>

                    <?php if (!empty($product['Product']['industry_analysis'])) { ?>
                        <li><a data-toggle="tab" href="#industry_analysis">Industry Analysis</a></li>
                    <?php } ?>

                    <?php if (!empty($product['Product']['latest_dev'])) { ?>
                        <li><a data-toggle="tab" href="#latest_dev">Latest Developments</a></li>
                    <?php } ?>
                  </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                      <?php if (!empty($product['Product']['product_snapshot'])) { ?>
                        <div id="snapshot" class="tab-pane fade in active">
                          <p><?php echo $product['Product']['product_snapshot']; ?></p>
                        </div>
                      <?php } ?>

                      <div id="profile" class="tab-pane fade <?= empty($product['Product']['product_snapshot']) ? 'in active' : '' ?>">
                         <!-- <p><?php //echo $product['Product']['product_description']; ?></p> -->
                         
                       <?php $faqs = json_decode($product['Product']['product_faq'],TRUE); 
                       echo $product['Product']['product_description']; ?>
                        <?php if(!empty($faqs)) { ?>
                          <div class="faq" style="margin-bottom: 6px;">
                            <h2>Frequently Asked Questions (FAQs) about this Report</h2>
                            <div class="report_faq_box">
                              <div class="clearfix">
                                <div class="m-faq m-faq2">
                                <div id="accordion2" class="panel-group">
                                  <?php $cnt = 0; foreach($faqs as $key => $val) { ?>                                        
                                  <div class="panel panel-default">
                                    <div class="p-heading"><a class="accordion-toggle" style="word-break: break-word;" href="#question<?php echo $cnt; ?>" data-parent="#accordion2" data-toggle="collapse" aria-expanded="true" class="collapsed"><?= $key ?></a>
                                    </div>
                                    <div class="panel-collapse collapse" id="question<?php echo $cnt; ?>" aria-expanded="false" style="">
                                      <div class="panel-body">
                                        <p class="m-faq-ans" style="word-break: break-word;"><span>b. </span> <?= $val ?></p>
                                      </div>
                                    </div>
                                  </div> 
                                  <?php $cnt++; } ?>                                       
                                </div>
                              </div>
                              </div>
                            </div>
                          </div>
                        <?php } ?>                                             
                      </div>                    
                      <div id="relatednewsshow" class="tab-pane fade">
                        <p>
                        <?php
                          if (!empty($product['Product']['related_news'])) {
                              echo $product['Product']['related_news'];
                          }
                          ?>
                        </p>
                      </div>

                      <div id="prod_summary" class="tab-pane fade">
                        <p>
                          <?php
                          if (!empty($product['Product']['summary'])) {
                              echo $product['Product']['summary'];
                          }
                          ?>
                        </p>
                      </div>

                      <div id="glossary" class="tab-pane fade">
                        <p>
                          <?php
                          if (!empty($product['Product']['glossary'])) {
                              echo $product['Product']['glossary'];
                          }
                          ?>
                        </p>
                      </div>

                      <div id="customization" class="tab-pane fade">
                        <p>
                          <?php
                          if (!empty($product['Product']['customization'])) {
                              echo $product['Product']['customization'];
                          }
                          ?>
                        </p>
                      </div>

                      <div id="industry_analysis" class="tab-pane fade">
                        <p>
                          <?php
                          if (!empty($product['Product']['industry_analysis'])) {
                              echo $product['Product']['industry_analysis'];
                          }
                          ?>
                        </p>
                      </div>

                      <div id="latest_dev" class="tab-pane fade">
                        <p>
                          <?php
                          if (!empty($product['Product']['latest_dev'])) {
                              echo $product['Product']['latest_dev'];
                          }
                          ?>
                        </p>
                      </div>

                      <div id="passwordTab" class="tab-pane fade">
                          <p>
                            <?php if ($product['Product']['is_set_toc'] == 0) { ?>
                                  <?php echo $product['Product']['product_specification']; ?>
                            <?php } else { ?>
                            <br>
                             <center>
                               <h3>To receive Table of Contents, please write to us at <br> <small>  <a href='mailto:sales@valuemarketresearch.com'>sales@valuemarketresearch.com</a></small></h3>
                             </center>
                          <?php } ?>
                        </p>
                      </div>


                    </div>
                </div>
            </div>
                <div class="col-md-12 col-sm-6">
                <div class="left-inner section-block">
                    <div class="row">
                        <div class="row row_mar">
                            <div class="col-lg-12 share">
                                <div class="headline"><span class="headline-title">Related Categories</span></div>
                                <?php
                                $i = 0;
                                foreach ($cat_array as $key => $cat_name) {
                                    $i++;
                                    ?>
                                    <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($cat_name))); ?>" class="btn-u btn-u-xs rounded-4x btn-u-dark mar_cat">
                                        <?php echo $cat_name; ?>
                                    </a>
                                    <?php
                                    if ($i == 4) {
                                        $i = 0;
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px;">
                        <div class="share">
                            <span class='st_linkedin_hcount' displayText='LinkedIn'></span>
                            <span class='st_facebook_hcount' displayText='Facebook'></span>
                            <span class='st_twitter_hcount' displayText='Tweet'></span>
                            <span class='st_sharethis_hcount' displayText='ShareThis'></span>
                            <span class='st_pinterest_hcount' displayText='Pinterest'></span>
                            <span class='st_email_hcount' displayText='Email'></span>
                        </div>
                    </div>

                </div>

            </div>
              </div>
          </div>
          <div class="col-md-3">
            <div class="user hidden-xs hidden-sm">
              <div class="">
                <div class="btns chooseopt">
                  <h4>Choose your Buying Option</h4>
                  <!-- <ul>
                    <?php foreach($licence_type as $key=>$value): ?>
                      <li><h6><?php echo $value; ?></h6></li>
                    <?php endforeach; ?>
                  </ul> -->
                      <div class="funkyradio">
                          <?php foreach($licence_type as $key=>$value): ?>
                            <div class="funkyradio-warning" data-toggle="tooltip" data-placement="bottom" title="<?=$licence_type_tooltip[$key]?>">
                                <input  type="radio" name="radio" id="radio<?= $key?>" value="<?= $key ?>" onclick="localStorage['buyoption'] = $(this).val();" <?php if($key==1){?>checked<?php }?> />
                                <label for="radio<?= $key?>"><?php echo $value; ?></label>
                            </div>
                          <?php endforeach; ?>
                    </div>
                    <div class="btns buy-button">
                      <ul>
                        <li class="btn-danger"> <?php if($product['Product']['is_upcoming'] == 1){
                                echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-shopping-basket')). ' Pre Buy Now', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','slug' => $product['Product']['slug'], 'ref_page' => 'pre-buy-now'), array("class" => "btn-u btn-u-primary", "target" => "_blank",'escape' => false));  
                              } else{
                                echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-shopping-basket')). ' Buy Now', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','slug' => $product['Product']['slug'], 'ref_page' => 'buy-now'), array("class" => "btn-u btn-u-primary", "target" => "_blank",'escape' => false));  
                                // echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-shopping-basket')). ' Buy Now', "https://pages.razorpay.com/vmrpayment", array("class" => "btn-u btn-u-primary", "target" => "_blank",'escape' => false));  
                              }?>
                        </li>
                      </ul>
                    </div>
                </div>
              </div>

            </div>
            <div class="user">
              <div class="">
                <div class="btns chooseopt">
                  <h4>Buy Chapters or Sections</h4>
                  <div class="why-us-content buy-chap-content">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                      <p>Avail customized purchase options to meet your exact research needs:</p>                  
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 buy-chap">
                      <ul>
                        <li>Buy sections of this report</li>
                        <li>Buy country level reports</li>
                        <li>Request for historical data</li>
                        <li>Request discounts available for Start-Ups & Universities</li>
                      </ul>
                    </div>
                    <div class="btns buy-button">
                      <ul>
                        <li class="btn-danger"> <?php
                          echo $this->Html->link('Request for Special Pricing', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','slug' => $product['Product']['slug'], 'ref_page' => 'request-for-special-pricing'), array("class" => "btn-u btn-u-primary", "target" => "_blank",'escape' => false));
                          ?>
                        </li>
                      </ul>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="user">
              <div class="">
                <div class="btns chooseopt">
                  <h4>Objectives of Study</h4>
                  <div class="why-us-content buy-chap-content">
                    <div class="col-md-12 col-sm-12 col-xs-12 buy-chap">
                      <ul>
                        <li>Define and measure the global market</li>
                        <li>Volume or revenue forecast of the global market and its various sub-segments with respect to main geographies</li>
                        <li>Analyze and identify major market trends along with the factors driving or inhibiting the market growth</li>
                        <li>Study the company profiles of the major market players with their market share</li>
                        <li>Analyze competitive developments</li>
                      </ul>
                    </div>
                    <!-- <div class="btns buy-button">
                      <ul>
                        <li class="btn-danger"> <?php
                          echo $this->Html->link('Request for Special Pricing', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','slug' => $product['Product']['slug'], 'ref_page' => 'request-for-special-pricing'), array("class" => "btn-u btn-u-primary", "target" => "_blank",'escape' => false));
                          ?>
                        </li>
                      </ul>
                    </div> -->
                    
                  </div>
                </div>
              </div>
            </div>

           
            <div class="user">
                <div class="">
                <div class="btns chooseopt">
                  <h4>Why Choose Us</h4>
                  <div class="why-us-content">
                    <?php foreach ($whyus as $key => $why) { ?>
                        <div class="col-md-2 col-xs-2 col-sm-2">
                            <?php switch ($key) {
                              case 0:
                                echo '<i class="fa fa-user" aria-hidden="true"></i>';
                                break;
                              case 1:
                                echo '<i class="fa fa-certificate" aria-hidden="true"></i>';
                                break;
                              case 2:
                                echo '<i class="fa fa-handshake-o" aria-hidden="true"></i>';
                                break;
                              case 3:
                                echo '<i class="fa fa-envelope" aria-hidden="true"></i>';
                                break;
                            }?>
                        </div>
                        
                        <div class="col-md-10 col-xs-10 col-sm-10 whychoosedata">
                          <h5><?=$why['Whyus']['title']?></h5>
                          <!--<p>Get your queries resolved from an industry expert.</p>-->
                        </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="user">
              <div class="">
                <div class="btns chooseopt">
                  <h4>Clients</h4>
                  <div class="why-us-content">
                  <div class="container-fluid row">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="carousel carousel-showmanymoveone1 slide" id="itemslider">
                        <div class="carousel-inner carousel-inner-pay">
                          <?php foreach ($ourclients as $key => $clients) { ?>

                            <div class="item <?= $key == 0?'active':''?> payotn">
                              <div class="col-xs-12 col-sm-12 col-md-12">
                                <?php if (!empty($clients['OurClient']['logo'])) { ?>
                                    <!--<a href="http://<?php echo $clients['OurClient']['link']; ?>" target="blank">-->
                                      <?= $this->Html->image('client_images/' . $clients['OurClient']['logo'], array('class' => 'client_image','data-toggle'=>'tooltip','title'=>$clients['OurClient']['client_name'],'data-placement'=>'right','alt'=>$clients['OurClient']['client_name'])); ?>
                                    <!--</a>-->
                                <?php } else {
                                        echo $this->Html->image('noimage.png', array('class' => 'client_image'));
                                      } ?>
                              </div>
                            </div>
                          <?php } ?>
                        </div>

                        <div id="slider-control">
                        <a class="left carousel-control" href="#itemslider" data-slide="prev">
                          <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                        </a>
                        <a class="right carousel-control" href="#itemslider" data-slide="next">
                          <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
                        </a>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="user">
                <div class="">
                <div class="btns chooseopt">
                  <h4>Testimonials</h4>
                  <div class="why-us-content">
                  <div class="pricing-head">
                    <div id="myCarousel" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel">
                      <!-- Carousel items -->
                      <div class="carousel-inner">
                        <?php foreach ($testimonial as $key => $testimonials) { ?>
                          <div class="item <?= $key == 0?'active':''?>">
                              <div class="row">
                                  <div class="col-xs-12 col-sm-12 col-md-12 testimonial-text">
                                      <p> <i class="fa fa-quote-left fa-lg" aria-hidden="true"></i><?php echo " \" "; echo  $testimonials['Testimonial']['testimonial_description'] ;echo" \" "; ?></p>
                                      <h5><i class="fa fa-user-circle fa-lg" aria-hidden="true"></i> <?php echo $testimonials['Testimonial']['testimonial_title'];?></h5>
                                      <!-- <h6>- Position</h6> -->
                                  </div>
                              </div>
                              <!--/row-fluid-->
                            </div>
                          <?php } ?>
                          <!--/item-->
                      </div>
                    </div>
                    <!-- <div class="view-more-btn">
                      <a href="<?php echo Router::url(array('controller' => 'testimonials', 'action' => 'listing' )); ?>">View More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    </div> -->
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
      
  </div>
</section>
<!-- ABOUT INNER SECTION END -->


<style>
.Buttons{display:-webkit-box;display:flex}.Buttons .Button{padding:10px;box-sizing:border-box;display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center;text-decoration:none;color:#fff;-webkit-box-flex:1;flex:1 0 auto;width:200px}.Buttons .Button .fa{font-size:22px}.Buttons .Button .text{margin-left:10px;text-transform:uppercase;letter-spacing:.16em;font-size:12px;font-weight:600}.Buttons .Button[data-type=facebook]{background:#3b5998;border-bottom-left-radius:5px}.Buttons .Button[data-type=facebook]:hover{background:#4c70ba}.Buttons .Button[data-type=twitter]{background:#00aced}.Buttons .Button[data-type=twitter]:hover{background:#21c2ff}.Buttons .Button[data-type=pinterest]{background:#bd081c}.Buttons .Button[data-type=pinterest]:hover{background:#bd081c}.Buttons .Button[data-type=linkedin]{background:#2867b2;border-bottom-right-radius:5px}.Buttons .Button[data-type=linkedin]:hover{background:#2867b2}.justify-content-center{justify-content:center;display:flex}@media(max-width:767px){.Buttons{display:block;}.Buttons .Button[data-type=facebook] {border-bottom-left-radius: unset;border-top-left-radius: 5px;}}
</style>
<!------social tabs start--------->
<!--<div class="job-description">
    <div class="container content mgr_tp_pd">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
              <div class="Buttons">
                <a href="http://www.facebook.com/sharer.php?u=<?= Router::url(null, true); ?>" target="_blank" class="Button" data-type="facebook"><i class="fa fa-fw fa-facebook"></i><span class="text">facebook</span></a>
                <a href="http://twitter.com/share?url=http://www.example.com&text=<?php echo $product['Product']['alias']?>&url=<?= Router::url(null, true); ?>" target="_blank" class="Button" data-type="twitter"><i class="fa fa-fw fa-twitter"></i><span class="text">twitter</span></a>
                <a href="http://pinterest.com/pin/create/button/?description=<?php echo $product['Product']['alias']?>&media=&url=<?= Router::url(null, true); ?>" target="_blank" class="Button" data-type="pinterest"><i class="fa fa-fw fa-pinterest"></i><span class="text">pinterest</span></a>
                <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?= Router::url(null, true); ?>" target="_blank" class="Button" data-type="linkedin"><i class="fa fa-fw fa-linkedin"></i><span class="text">linkedin</span></a>
              </div>
            </div>
        </div>
    </div>
</div>-->  
<!------social tabs end-087097-------->
<div class="job-description">
    <div class="container content mgr_tp_pd">
        <div class="row justify-content-center">
            <!-- Left Inner -->
            <div class="col-md-8 text-center">
              <div class="Buttons">
              <!-- <a href="" target="_blank" class="Button btn btn-secondary" data-type="" >
                <span class="text" style="margin-left:40%;">Download Sample 
              </a> -->
                        <!-- </br> -->
                        <span class="Button btn-danger" >
              <?php
                        echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-download')).' Download Sample', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'download-sample'), array("class" => "btn-u btn-u-green", "target" => "_blank",'escape' => false, "style"=>'color:#fff;margin-left:40%')
                        );
                        ?>
                      </span>
                  </span>
              </div>
            </div>
            
            <!-- End Left Inner -->
        </div>
    </div>
</div>  <!-- End Right Inner -->



<!-- Key Quastions -->
 <div class="container">
      <div class="">
        <div class="report-page relat-reprt key-qust">
            <div class="col-md-6 col-sm-6 padd-0">
                <h2><center>Key questions answered by the report</center></h2>
                <div class="panel-group faq-quest" id="accordion" role="tablist" aria-multiselectable="true">
                    <ul class="mrkt-info key-qst-left">
                        <li style="box-shadow: 0 0 7px; background-color:#0074bd;"><a  style="color:#fff"><span>What is the current market size and trends? </span></a></li>
                        <li style="box-shadow: 0 0 7px; background-color:#0074bd;"><a style="color:#fff">What will be the market size during the forecast period? </a></li>
                        <li style="box-shadow: 0 0 7px; background-color:#0074bd;"><a style="color:#fff">How various market factors such as a driver, restraints, and opportunity impact the market? </a></li>
                        <li style="box-shadow: 0 0 7px; background-color:#0074bd;"><a style="color:#fff">What are the dominating segment and region in the market <br>and why? </a></li>
                        <!-- <li><a href="#."><span>What was the market size from 2017 to 2024?</span></a></li>
                        <li><a href="#.">What will be market growth till 2024 and what will be the resultant market forcast in the year?</a></li>
                        <li><a href="#.">How will the market drivers, restraints and future opportunities affect the market dynamics and a subsequent analysis of the associated trends? </a></li>
                        <li><a href="#.">What are the segment and region leading the market growth and why? </a></li> -->
                    </ul>
                    <div class="pull-left req-free-btn">
                      <?php echo $this->Html->link('Request Free Sample Report', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'request-free-sample-report'), array("class" => "btn", "target" => "_blank", "style"=>"background-color:#d9534f", "onMouseOver"=>"this.style.background='#ac2925'", "onMouseOut"=>"this.style.background='#d9534f'")); ?>
                        <!-- <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Request Free Sample Report</button> -->
                    </div>
                </div>
            </div>
            
            <div class="col-md-6 col-sm-6 padd-0">
              <h2><center>Need specific market information?</center></h2>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <ul class="mrkt-info">
                    <li style="box-shadow: 0 0 7px; background-color:#0074bd;"><a style="color:#fff"><i class="fa fa-user"></i><span>Ask for free product review call with the author</span></a></li>
                    <li style="box-shadow: 0 0 7px; background-color:#0074bd;"><a style="color:#fff"><i class="fa fa-users"></i><span>Share your specific research requirments for a customized report</span></a></li>
                    <li style="box-shadow: 0 0 7px; background-color:#0074bd;"><a style="color:#fff"><i class="fa fa-share-alt"></i><span>Request for due diligence and consumer centric studies</span></a></li>
                    <li style="box-shadow: 0 0 7px; background-color:#0074bd;"><a style="color:#fff"><i class="fa fa-recycle"></i>Request for study updates, segment specific and country level reports</a></li>
                  </ul>
                  <div class="pull-left req-free-btn">
                    <?php echo $this->Html->link('Request for Customization', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'request-customization'), array("class" => "btn", "target" => "_blank", "style"=>"background-color:#d9534f", "onMouseOver"=>"this.style.background='#ac2925'", "onMouseOut"=>"this.style.background='#d9534f'")); ?>
                   <!-- <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Request for Customization</button> -->
                  </div>
                </div>
            </div>
        </div>
        <!-------->
        <div class="job-description">
    <div class="container content mgr_tp_pd">
        <div class="row justify-content-center">
            <!-- Left Inner -->
            <div class="col-md-8 text-center">
              <div class="Buttons">
                <a href="http://www.facebook.com/sharer.php?u=<?= Router::url(null, true); ?>" target="_blank" class="Button" data-type="facebook"><i class="fa fa-fw fa-facebook"></i><span class="text">facebook</span></a>
                <a href="http://twitter.com/share?url=http://www.example.com&text=<?php echo $product['Product']['alias']?>&url=<?= Router::url(null, true); ?>" target="_blank" class="Button" data-type="twitter"><i class="fa fa-fw fa-twitter"></i><span class="text">twitter</span></a>
                <a href="http://pinterest.com/pin/create/button/?description=<?php echo $product['Product']['alias']?>&media=&url=<?= Router::url(null, true); ?>" target="_blank" class="Button" data-type="pinterest"><i class="fa fa-fw fa-pinterest"></i><span class="text">pinterest</span></a>
                <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?= Router::url(null, true); ?>" target="_blank" class="Button" data-type="linkedin"><i class="fa fa-fw fa-linkedin"></i><span class="text">linkedin</span></a>
              </div>
                <!-- <div class="sharethis-inline-share-buttons"></div>     -->
            </div>
            
            <!-- End Left Inner -->
        </div>
    </div>
</div>  <!-- End Right Inner -->

        <!-------->
      </div>
    </div>
      <!-- Key Quastions -->

<style>
.stMainServices{min-height: 22px;}.stButton_gradient{min-height: 22px;}.small_space{margin: 10px}.related_products{font-size: 14px;}
</style>

<script>
  $(function () { $('[data-toggle="tooltip"]').tooltip()});
</script>

<!-- New Code For Covid-19 Popup Start VG-22/04/2020 -->
<style>
  .blur-it{filter:blur(4px)}.modal-wrapper{width:100%;height:100%;position:fixed;top:0;left:0;background:rgba(0,0,0,.6);visibility:hidden;opacity:0;transition:all .25s ease-in-out;z-index:9999}.modal-wrapper.open{opacity:1;visibility:visible}.covid-popup .modal{width:600px;height:auto;display:block;margin:50% 0 0 -300px;position:relative;top:42%;left:50%;background:#fff;opacity:0;transition:all .5s ease-in-out;border-radius:10px}.modal-wrapper.open .modal{margin-top:-200px;opacity:1}.covid-popup .head{width:100%;height:44px;padding:0 10px;overflow:hidden;background:#3498db}.covid-popup .head h4{float:left;color:#fff;font-size:22px;letter-spacing:1px;font-weight:500}.covid-popup .btn-close{font-size:28px;display:block;float:right;color:#fff}.covid-popup .btn-close:hover{color:#fff}.modal-wrapper.covid-popup .modal .content{padding:5%;background-position:top left;background-repeat:no-repeat;background-size:cover;box-shadow:10000px 0 10000px rgba(255,255,255,.8) inset}.covid-popup .good-job{text-align:justify}.covid-popup .good-job p{letter-spacing:1px;line-height:26px}
</style>
<div class="modal-wrapper covid-popup">
  <div class="modal">
    <div class="head">
        <h4><i class="fa fa-info-circle" aria-hidden="true"></i> Important: Covid19 pandemic market impact</h4>
        <a class="btn-close trigger" href="#">
            <i class="fa fa-times" aria-hidden="true"></i>
        </a>
    </div>
    <div class="content">
        <div class="good-job">
          <p>
          The world is currently experiencing a huge challenge which has affected all the industries and markets. Few markets have experienced a high growth in their demand while others have faced a downfall. The extent of growth or drop in the market statistics of different industries by COVID 19 plays an important role in how the future of businesses will shape up in the near future. We at Value Market Research provide you with an impact analysis of COVID 19 on this market with its latest update in the purchased version of the report.
          </p>
        </div>
    </div>
  </div>
</div>
