<style>
    .mtp{
        margin-top: 10px;
    }
    .search_mtp{
        margin-top: 3px;
    }
    .cat{
        width: 0px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {

        $('#selecctall').click(function (event) {  //on click 
            if (this.checked) { // check select status
                $('.checkbox1').each(function () { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"               
                });
            } else {
                $('.checkbox1').each(function () { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                });
            }
        });

        $('.btn_bulk_active').click(function () {
            var usr_str = '';
            $("#product_index_tbl tr").each(function (i, row) {
                if ($(this).find("td:first").find('input:checked').prop('checked') == true) {
                    var val = $(this).find("td:first").find('input:checked').val();
                    if (usr_str != '') {
                        usr_str = usr_str + "," + val;
                    }
                    else {
                        usr_str = val;
                    }
                }
            });
            if (usr_str == '') {
                alert('Please select checkbox to perform action.');
                return false;
            }
            else {
                $("#selected_usr_field").val(usr_str);
            }
        });

        $('#prnt_of_chld1').multiselect({
            selectedList: 10,
            multiple: false
        }).multiselectfilter();



        var i = 1;
        $('#prnt_of_chld1').change(function () {
            $("#div2").hide();
            $("#prnt_of_chld2").prop('disabled', true);
            $("#div3").hide();
            $("#prnt_of_chld3").prop('disabled', true);
            $("#div4").hide();
            $("#prnt_of_chld4").prop('disabled', true);
            $("#div5").hide();
            $("#prnt_of_chld5").prop('disabled', true);
            $("#div6").hide();
            $("#prnt_of_chld6").prop('disabled', true);
            var id = $('#prnt_of_chld1').val();
            var lname = '';
            var lname = $("#prnt_of_chld1 option:selected").text();
            $('#parent_cat_id_per').val(id);
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "categories/dropdwn_fill/" + id;
                $.ajax({
                    url: urls,
                    dataType: 'json'
                }).done(function (data) {
                    if (data.length != 0) {
                        $("#div2").show();
                        $("#prnt_of_chld2").prop('disabled', false);
                        $("#prnt_of_chld2").empty();
                        $("#prnt_of_chld2").append(
                                $("<option></option>").text('--SubCategory--').val('')
                                );
                        $.each(data, function (i, val) {
                            $("#prnt_of_chld2").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                        $('#prnt_of_chld2').multiselect({
                            selectedList: 10,
                            multiple: false
                        }).multiselectfilter();
                    }
                    else {
                        $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" value="' + id + '" type="hidden" id="cat_hidden' + i + '">');
                        $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
                        i++;
                        $("#div2").hide();
                        $("#prnt_of_chld2").prop('disabled', true);
                        $("#div3").hide();
                        $("#prnt_of_chld3").prop('disabled', true);
                        $("#div4").hide();
                        $("#prnt_of_chld4").prop('disabled', true);
                    }
                });
            }
            else {
                $("#div2").hide();
                $("#prnt_of_chld2").prop('disabled', true);
                $("#prnt_of_chld2").empty();
                $("#prnt_of_chld2").append(
                        $("<option></option>").text('--Select--').val('')
                        );
            }
        });

        $('#prnt_of_chld2').change(function () {
            $("#div3").hide();
            $("#prnt_of_chld3").prop('disabled', true);
            $("#div4").hide();
            $("#prnt_of_chld4").prop('disabled', true);
            $("#div5").hide();
            $("#prnt_of_chld5").prop('disabled', true);
            $("#div6").hide();
            $("#prnt_of_chld6").prop('disabled', true);
            var id = $('#prnt_of_chld2').val();
            var lname = '';
            var lname = $("#prnt_of_chld2 option:selected").text();
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "categories/dropdwn_fill/" + id;
                $.ajax({
                    url: urls,
                    dataType: 'json'
                }).done(function (data) {
                    if (data.length != 0) {
                        $("#div3").show();
                        $("#prnt_of_chld3").prop('disabled', false);
                        $("#prnt_of_chld3").empty();
                        $("#prnt_of_chld3").append(
                                $("<option></option>").text('--SubCategory--').val('')
                                );
                        $.each(data, function (i, val) {
                            $("#prnt_of_chld3").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                        $('#prnt_of_chld3').multiselect({
                            selectedList: 10,
                            multiple: false
                        }).multiselectfilter();

                    }
                    else {
                        $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" value="' + id + '" type="hidden" id="cat_hidden' + i + '">');
                        $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');">&nbsp&nbsp</div>');
                        i++;
                        $("#div3").hide();
                        $("#prnt_of_chld3").prop('disabled', true);
                        $("#div4").hide();
                        $("#prnt_of_chld4").prop('disabled', true);
                    }
                });
            }
            else {
                $("#div3").hide();
                $("#prnt_of_chld3").prop('disabled', true);
                $("#prnt_of_chld3").empty();
            }
        });




        $('#prnt_of_chld3').change(function () {
            $("#div4").hide();
            $("#prnt_of_chld4").prop('disabled', true);
            $("#div5").hide();
            $("#prnt_of_chld5").prop('disabled', true);
            $("#div6").hide();
            $("#prnt_of_chld6").prop('disabled', true);
            var id = $('#prnt_of_chld3').val();
            var lname = '';
            var lname = $("#prnt_of_chld3 option:selected").text();
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "categories/dropdwn_fill/" + id;
                $.ajax({
                    url: urls,
                    dataType: 'json'
                }).done(function (data) {
                    if (data.length != 0) {
                        $("#div4").show();
                        $("#prnt_of_chld4").prop('disabled', false);
                        $("#prnt_of_chld4").empty();
                        $("#prnt_of_chld4").append(
                                $("<option></option>").text('--SubCategory--').val('')
                                );
                        $.each(data, function (i, val) {
                            $("#prnt_of_chld4").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                        $('#prnt_of_chld4').multiselect({
                            selectedList: 10,
                            multiple: false
                        }).multiselectfilter();
                    }
                    else {
                        $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" type="hidden" value="' + id + '" id="cat_hidden' + i + '">');
                        $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
                        i++;
                        $("#div4").hide();
                        $("#prnt_of_chld4").prop('disabled', true);
                    }
                });
            }
            else {
                $("#div4").hide();
                $("#prnt_of_chld4").prop('disabled', true);
                $("#prnt_of_chld4").empty();
                $("#prnt_of_chld4").append(
                        $("<option></option>").text('--Select--').val('')
                        );
            }
        });
        $('#prnt_of_chld4').change(function () {
            $("#div5").hide();
            $("#prnt_of_chld5").prop('disabled', true);
            $("#div6").hide();
            $("#prnt_of_chld6").prop('disabled', true);
            var id = $('#prnt_of_chld4').val();
            var lname = '';
            var lname = $("#prnt_of_chld4 option:selected").text();
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "categories/dropdwn_fill/" + id;
                $.ajax({
                    url: urls,
                    dataType: 'json'
                }).done(function (data) {
                    if (data.length != 0) {
                        $("#div5").show();
                        $("#prnt_of_chld5").prop('disabled', false);
                        $("#prnt_of_chld5").empty();
                        $("#prnt_of_chld5").append(
                                $("<option></option>").text('--SubCategory--').val('')
                                );
                        $.each(data, function (i, val) {
                            $("#prnt_of_chld5").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                        $('#prnt_of_chld5').multiselect({
                            selectedList: 10,
                            multiple: false
                        }).multiselectfilter();
                    }
                    else {
                        $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" type="hidden" value="' + id + '" id="cat_hidden' + i + '">');
                        $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
                        i++;
                        $("#div5").hide();
                        $("#prnt_of_chld5").prop('disabled', true);
                    }
                });
            }
            else {
                $("#div5").hide();
                $("#prnt_of_chld5").prop('disabled', true);
                $("#prnt_of_chld5").empty();
                $("#prnt_of_chld5").append(
                        $("<option></option>").text('--Select--').val('')
                        );
            }
        });
        $('#prnt_of_chld5').change(function () {
            $("#div6").hide();
            $("#prnt_of_chld6").prop('disabled', true);

            var id = $('#prnt_of_chld5').val();
            var lname = '';
            var lname = $("#prnt_of_chld5 option:selected").text();
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "categories/dropdwn_fill/" + id;
                $.ajax({
                    url: urls,
                    dataType: 'json'
                }).done(function (data) {
                    if (data.length != 0) {
                        $("#div6").show();
                        $("#prnt_of_chld6").prop('disabled', false);
                        $("#prnt_of_chld6").empty();
                        $("#prnt_of_chld6").append(
                                $("<option></option>").text('--SubCategory--').val('')
                                );
                        $.each(data, function (i, val) {
                            $("#prnt_of_chld6").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                        $('#prnt_of_chld6').multiselect({
                            selectedList: 10,
                            multiple: false
                        }).multiselectfilter();
                    }
                    else {
                        $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" type="hidden" value="' + id + '" id="cat_hidden' + i + '">');
                        $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
                        i++;
                        $("#div6").hide();
                        $("#prnt_of_chld6").prop('disabled', true);
                    }
                });
            }
            else {
                $("#div6").hide();
                $("#prnt_of_chld6").prop('disabled', true);
                $("#prnt_of_chld6").empty();
                $("#prnt_of_chld6").append(
                        $("<option></option>").text('--Select--').val('')
                        );
            }
        });
        $('#prnt_of_chld6').change(function () {
            var id = $('#prnt_of_chld6').val();
            if (id != '') {
                var lname = '';
                var lname = $("#prnt_of_chld6 option:selected").text();
                $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" type="hidden" value="' + id + '" id="cat_hidden' + i + '">');
                $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
                i++;
            }
        });


        //$('#togg').hide();
        $("#togg1").click(function () {
            $("#togg").toggle();

        });


    });
</script>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="pull-right">
                    <?php echo $this->Html->link(__('Add Product'), array('action' => 'add'), array('class' => 'btn   btn-outline btn-sm btn-warning dim', 'title' => 'Add Product')); ?>
                    <?php echo $this->Html->link(__('Import Excel'), array('action' => 'upload_products_excel'), array('class' => 'btn   btn-outline btn-sm btn-primary dim', 'title' => 'Import Product Excel')); ?>
                    <?php if (AuthComponent::user('role') == 1): ?>                   
                        <?php echo $this->Html->link(__('Refresh Count'), array('controller' => 'product_categories', 'action' => 'update_prod_count_admin'), array('class' => 'btn   btn-outline btn-sm btn-default dim', 'title' => 'Product Count Refresh')); ?>
                    <?php endif; ?>
                 <?php /*   <a id="togg1" class = 'btn btn-outline btn-sm btn-primary dim' title ='search'><?php echo "Search" ?></a> */?>

                </div>
                <h5>Products</h5> 
            </div>

            <div class="ibox-content">
                <div class="serc" id="togg">
                    <?php echo $this->Form->create('Product', array('id' => 'index', 'class' => 'form-horizontal')); ?>
                    <div class="row">
                        <div class="col-lg-2">
                            <label for="form-field-8">Product Name : </label>
                            <?php echo $this->Form->input('search_text', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Enter Text For Search', 'label' => false)); ?>
                        </div>
                        <div class="col-lg-2">
                            <label for="form-field-8">Price</label>
                            <?php echo $this->Form->input('price_search', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Price', 'label' => false)); ?>
                        </div>
                        <div class="col-lg-2">
                            <label for="form-field-8">Status</label>

                            <?php
                            echo $this->Form->input('product_status', array(
                                'options' => $product_status,
                                'class' => 'form-control', 'label' => false, 'empty' => 'All', 'data-bvalidator' => 'required'));
                            ?>
                        </div>

                        <div class="col-lg-2" id="main_div">
                            <label for="form-field-8">Select Parent Category</label>
                            <?php
                            echo $this->Form->input('parent_category_id1', array(
                                'options' => $cat,
                                'empty' => '--Category Name--', 'class' => 'form-control hastip cat', 'id' => 'prnt_of_chld1', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                            ?>
                        </div>
                        <div class="col-lg-1"></div>
                        <div class="col-lg-2" id="div2" hidden>
                            <label for="form-field-8"> </label>
                            <?php
                            echo $this->Form->input('parent_category_id2', array(
                                'options' => '',
                                'empty' => '--Select--', 'class' => 'form-control hastip cat', 'id' => 'prnt_of_chld2', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                            ?>
                        </div>
                    </div> 
                    <div>
                        <div class="row">
                            <div class="form-group">

                                <div class="col-lg-2" id="div3" hidden>
                                    <label for="form-field-8"> </label>
                                    <?php
                                    echo $this->Form->input('parent_category_id3', array(
                                        'options' => '',
                                        'empty' => '--Select--', 'class' => 'form-control cat', 'id' => 'prnt_of_chld3', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                                    ?>
                                </div>
                                <div class="col-lg-1"></div>
                                <div class="col-lg-2" id="div4" hidden>
                                    <label for="form-field-8"> </label>
                                    <?php
                                    echo $this->Form->input('parent_category_id4', array(
                                        'options' => '',
                                        'empty' => '--Select--', 'class' => 'form-control cat', 'id' => 'prnt_of_chld4', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                                    ?>
                                </div>
                                <div class="col-lg-1"></div>
                                <div class="col-lg-2" id="div5" hidden>
                                    <label for="form-field-8"> </label>
                                    <?php
                                    echo $this->Form->input('parent_category_id5', array(
                                        'options' => '',
                                        'empty' => '--Select--', 'class' => 'form-control cat', 'id' => 'prnt_of_chld5', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                                    ?>
                                </div>
                                <div class="col-lg-1"></div>
                                <div class="col-lg-2" id="div6" hidden>
                                    <label for="form-field-8"> </label>
                                    <?php
                                    echo $this->Form->input('parent_category_id6', array(
                                        'options' => '',
                                        'empty' => '--Select--', 'class' => 'form-control cat', 'id' => 'prnt_of_chld6', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 mtp">
                            <?php echo $this->Form->button('Search', array('type' => 'submit', 'class' => 'btn btn-outline btn-sm btn-primary fa fa-search')); ?>
                        </div>
                        <div class="col-lg-1 mtp ">
                            <?php echo $this->Html->link(__(' Clear Fliter'), array('action' => 'clear_product_filter'), array('class' => 'btn btn-outline btn-sm btn-warning fa fa-refresh', 'escape' => false, 'title' => 'Clear')); ?>
                        </div>


                        <?php echo $this->form->end(); ?>
                        <?php if (AuthComponent::user('role') == 1): ?>

                            <div class="col-lg-1"></div>
                            <?php echo $this->Form->create('Product', array('action' => 'change_status_selected', 'class' => 'form-inline', 'id' => 'bulk_actions')); ?>
                            <div class="col-lg-2">Bulk Action:
                                <?php echo $this->Form->input('selected_products', array('type' => 'hidden', 'id' => 'selected_usr_field')); ?>
                            </div>
                            <div class="col-lg-2">
                                <?php echo $this->Form->submit('Activate Selected', array('name' => 'activate', 'class' => 'btn   btn-outline btn-sm btn-primary dim tooltip-f btn_bulk_active', 'title' => 'Activate Selected')); ?>
                            </div>
                            <div class="col-lg-2">
                                <?php echo $this->Form->submit('Deactivate Selected', array('name' => 'deactivate', 'class' => 'btn   btn-outline btn-sm btn-warning dim tooltip-f btn_bulk_active', 'title' => 'Deactivate Selected')); ?>
                            </div>
                            <?php /*   <div class="col-lg-2">
                              <?php echo $this->Form->submit('Delete Selected', array('name' => 'delete', 'class' => 'btn   btn-outline btn-sm btn-danger dim tooltip-f btn_bulk_active', 'title' => 'Delete Selected')); ?>
                              </div> */ ?>
                            <?php echo $this->Form->end(); ?>


                        <?php endif; ?>
                    </div>
                </div> 

                <hr>
                <div class="table-responsive mtp">
                    <table class="table table-bordered table-striped" id="product_index_tbl">
                        <thead>
                            <tr> 
                                <?php if (AuthComponent::user('role') == 1): ?>
                                    <th style="width: 10px;">
                                        <input type="checkbox" id="selecctall"> All
                                    </th>
                                <?php endif; ?>
                                <th><?php echo $this->Paginator->sort('product_name'); ?></th>
                                <th><?php echo $this->Paginator->sort('meta_title'); ?></th>
                                <th><?php echo $this->Paginator->sort('meta_description'); ?></th>
                                <th><?php echo $this->Paginator->sort('meta_keywords'); ?></th>
                                <th><?php echo $this->Paginator->sort('price', 'price ($)'); ?></th>
                                <th><?php echo $this->Paginator->sort('is_active'); ?></th>
                                <th><?php echo $this->Paginator->sort('TOC_written'); ?></th>
                                <th>cat. count</th>
                                <th>spec. count</th>
                                <th>country count</th>
                                <th>Desc.words count</th>

                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead> 
                        <?php
                        foreach ($products as $product) {
                            ?>
                            <tbody>
                                <?php if (AuthComponent::user('role') == 1): ?>
                                <td><input type="checkbox" class="checkbox1" value="<?= $product['Product']['id'] ?>" ></td>
                            <?php endif; ?>
                            <td><?php echo h($product['Product']['product_name']); ?>&nbsp;</td>
                            <td><?php echo h($product['Product']['meta_name']); ?>&nbsp;</td>
                            <td><?php echo $this->Text->truncate(($product['Product']['meta_desc']),100); ?>&nbsp;</td>
                            <td><?php echo h($product['Product']['meta_keywords']); ?>&nbsp;</td>
                            <td><?php echo h($product['Product']['price']); ?>&nbsp;</td>
                            <td><?php echo h($product_status[$product['Product']['is_active']]); ?>&nbsp;</td>
                            <td><?php echo h($toc[$product['Product']['is_set_toc']]); ?>&nbsp;</td>
                            <td><?php echo h(count($product['ProductCategory'])); ?>&nbsp;</td>
                            <td><?php echo h(count($product['ProductSpecification'])); ?>&nbsp;</td>
                            <td><?php echo h(count($product['ProductCountry'])); ?>&nbsp;</td>
                            <td><?php echo str_word_count($product['Product']['product_description']); ?>&nbsp;</td>

                            <td class="actions" style="min-width: 250px; max-width: 250px;">
                                <?php echo $this->Html->link(__('view'), array('action' => 'view', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-warning ', 'title' => 'View')); ?>
                                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $product['Product']['id']), array('class' => 'btn  btn-outline btn-sm btn-info ', 'title' => 'Edit')); ?>       
                                <?php echo $this->Html->link(__('Manage Specs.'), array('controller' => 'specifications', 'action' => 'add_product_spc', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-primary dim', 'title' => 'Edit Specification')); ?>  
                                <?php echo $this->Html->link(__('Manage Categories'), array('controller' => 'products', 'action' => 'edit_category', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-success dim', 'title' => 'Edit Categories')); ?>       
                                <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-danger dim', 'title' => 'Delete'), __('Are you sure you want to delete # %s?', $product['Product']['id'])); ?>
                                <?php } ?>
                                <?php echo $this->Html->link(__('Manage Countries'), array('controller' => 'products', 'action' => 'add_product_countries', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-primary dim', 'title' => 'View countries')); ?>                                     
                                <?php
                                if (AuthComponent::user('role') == 1) {
                                    if ($product['Product']['is_active'] == 1) {
                                        echo $this->Html->link(__('Deactivate'), array('action' => 'deactive', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-info', 'title' => 'DeActive'));
                                    } else {
                                        echo $this->Html->link(__('Activate'), array('action' => 'active', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-info', 'title' => 'Active'));
                                    }
                                }
                                ?>
                                <?php echo $this->Html->link(__('Preview'), array('controller' => 'products', 'action' => 'product_preview', $product['Product']['id']), array('class' => 'btn btn-outline btn-sm btn-warning dim', 'title' => 'Product Preview', 'target' => '_blank')); ?>                                     


                                <?php
                                if (AuthComponent::user('role') == 1) {
                                    if ($product['Product']['is_hide'] == 1) {
                                        echo $this->Html->link(__('Remove From Home '), array('action' => 'unhide', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-info', 'title' => 'Remove'));
                                    } else {
                                        echo $this->Html->link(__('Display on Home'), array('action' => 'hide', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-info', 'title' => 'Display'));
                                    }
                                }
                                ?>
                            </td>
                            </tbody>
                        <?php } ?>
                    </table>
                    <div class="row col-md-12">
                        <div class="dataTables_paginate paging_bootstrap">
                            <p>
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?> </p>
                            <ul class="pagination" style="visibility: visible;">
                                <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
                                <li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
                                <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>