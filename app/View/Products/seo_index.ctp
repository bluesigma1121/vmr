<style>
    .mtp{
        margin-top: 10px;
    }
    .search_mtp{
        margin-top: 3px;
    }
</style>



<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="pull-right">
                    <?php echo $this->Html->link(__('Add Product'), array('action' => 'add'), array('class' => 'btn   btn-outline btn-sm btn-warning dim', 'title' => 'Add Product')); ?>
                </div>
                <h5>Products</h5> 
            </div>
            <div class="ibox-content">
                <?php echo $this->Form->create('Product', array('id' => 'index', 'class' => 'form-horizontal')); ?>
                <div class="row">
                    <div class="col-lg-2">
                        <label for="form-field-8">Product Name</label>
                    </div>
                    <div class="col-lg-2">
                        <?php echo $this->Form->input('search_text', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Enter Text For Search', 'label' => false)); ?>
                    </div>
                    <div class="col-lg-1">
                        <label for="form-field-8">Price</label>
                    </div>
                    <div class="col-lg-2">
                        <?php echo $this->Form->input('price_search', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Price', 'label' => false)); ?>
                    </div>
                    <div class="col-lg-1">
                        <label for="form-field-8">Status</label>
                    </div>
                    <div class="col-lg-2"> 
                        <?php
                        echo $this->Form->input('product_status', array(
                            'options' => $product_status,
                            'class' => 'form-control', 'label' => false, 'empty' => 'All', 'data-bvalidator' => 'required'));
                        ?>
                    </div>
                </div>
                <div class="row mtp">
                    <div class="col-lg-1 col-lg-offset-2 search_mtp">
                        <?php echo $this->Form->button('Search', array('type' => 'submit', 'class' => 'btn btn-outline btn-sm btn-primary fa fa-search')); ?>
                    </div>
                    <div class="col-lg-1  search_mtp ">
                        <?php echo $this->Html->link(__(' Clear Fliter'), array('action' => 'clear_product_filter'), array('class' => 'btn btn-outline btn-sm btn-warning fa fa-refresh', 'escape' => false, 'title' => 'Clear')); ?>
                    </div>
                </div>
                <?php echo $this->form->end(); ?>

                <div class="table-responsive mtp">
                    <table class="table table-bordered table-striped" id="product_index_tbl">
                        <thead>
                        <th><?php echo $this->Paginator->sort('product_name'); ?></th>
                        <th><?php echo $this->Paginator->sort('is_active'); ?></th>
                        <th><?php echo $this->Paginator->sort('meta_name', 'Title'); ?></th>
                        <th><?php echo $this->Paginator->sort('meta_keywords'); ?></th>
                        <th><?php echo $this->Paginator->sort('meta_desc'); ?></th>
                        <th class="actions"><?php echo __('Actions'); ?></th>
                        </tr>
                        </thead> 
                        <?php
                        foreach ($products as $product) {
                            ?>
                            <tbody>
                            <td><?php echo h($product['Product']['product_name']); ?>&nbsp;</td>
                            <td><?php echo h($yes_no[$product['Product']['is_active']]); ?>&nbsp;</td>
                            <td><?php echo $this->Text->truncate($product['Product']['meta_name'], 100); ?>&nbsp;</td>
                            <td><?php echo $this->Text->truncate($product['Product']['meta_keywords'], 100); ?>&nbsp;</td>
                            <td><?php echo $this->Text->truncate($product['Product']['meta_desc'], 100); ?>&nbsp;</td>
                            <td class="actions">
                                <?php echo $this->Html->link(__('Edit'), array('action' => 'seo_edit', $product['Product']['id']), array('class' => 'btn  btn-outline btn-sm btn-info ', 'title' => 'Edit')); ?>       
                            </td>
                            </tbody>
                        <?php } ?>
                    </table>
                    <div class="row col-md-12">
                        <div class="dataTables_paginate paging_bootstrap">
                            <p>
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?> </p>
                            <ul class="pagination" style="visibility: visible;">
                                <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
                                <li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
                                <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>