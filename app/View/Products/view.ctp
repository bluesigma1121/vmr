<style>
    .row_btn{
        padding: 5px;
    }
</style>


<div class="row">
    <div class="col-lg-9">
        <div class="wrapper wrapper-content animated fadeInUp">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="m-b-md">
                                <h2><?= $product['Product']['product_name'] ?></h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <dl class="dl-horizontal">
                                <dt>is Active?:</dt> <dd><span class="label label-primary"><?= $product_status[$product['Product']['is_active']] ?></span></dd>
                            </dl>
                        </div>
                        <div class="col-lg-7">
                            <dl class="dl-horizontal">
                                <dt>Published Date:</dt> 
                                <dd><a href="#" class="text-navy"> <?php if (!empty($product['Product']['pub_date'])) { ?>
                                            <?php echo date('F Y', strtotime($product['Product']['pub_date'])); ?>
                                        <?php } ?></a> </dd>
                            </dl>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <dl class="dl-horizontal">
                                <dt>Product No. :</dt> <dd><?= $product['Product']['product_no'] ?></dd>
                                <dt>Primary Category :</dt> <dd><?= $product['Category']['category_name'] ?></dd>
                                <dt>Single User License($):</dt> <dd><a href="#" class="text-navy"> <?= $product['Product']['price'] ?></a> </dd>
                            </dl>
                        </div>
                        <div class="col-lg-7" id="cluster_info">
                            <dl class="dl-horizontal" >
                                <dt>Last Updated:</dt> <dd><?= date('d-M-Y h:i A', strtotime($product['Product']['modified'])) ?></dd>
                                <dt>Created:</dt> <dd><?= date('d-M-Y h:i A', strtotime($product['Product']['created'])) ?></dd>
                                <dt>Corporate User License($):</dt>
                                <dd><a href="#" class="text-navy"> <?= $product['Product']['corporate_price'] ?></a> </dd>
                            </dl>
                        </div>
                    </div>
                    <div class="row">
                        <dl class="dl-horizontal">
                            <dt>Description:</dt> 
                            <dd><?= $product['Product']['product_description']; ?></dd>
                        </dl>
                    </div>
                    <div class="row">
                        <dl class="dl-horizontal">
                            <dt>Table Of Content:</dt> 
                            <dd><?= $product['Product']['product_specification']; ?></dd>
                        </dl>
                    </div>
                    <div class="row">
                        <dl class="dl-horizontal">
                            <dt>Related News:</dt> 
                            <dd><?= $product['Product']['related_news']; ?></dd>
                        </dl>
                    </div>
                     <div class="row">
                        <dl class="dl-horizontal">
                            <dt>Tags:</dt> 
                            <dd><?= $product['Product']['tags']; ?></dd>
                        </dl>
                    </div>
                    <div class="row">
                        <dl class="dl-horizontal">
                            <dt>Meta Title:</dt> 
                            <dd><?= $product['Product']['meta_name']; ?></dd>
                        </dl>
                    </div>

                    <div class="row">
                        <dl class="dl-horizontal">
                            <dt>Meta Keywords:</dt> 
                            <dd><?= $product['Product']['meta_keywords']; ?></dd>
                        </dl>
                    </div>
                    <div class="row">
                        <dl class="dl-horizontal">
                            <dt>Meta Description:</dt> 
                            <dd><?= $product['Product']['meta_desc']; ?></dd>
                        </dl>
                    </div>
                    <div class="row">
                        <dl class="dl-horizontal">
                            <dt>Publisher Name:</dt> 
                            <dd><?= $product['Product']['publisher_name']; ?></dd>
                        </dl>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="col-lg-3">


        <div class="wrapper wrapper-content animated fadeInUp">

            <?php if (AuthComponent::user('role') != 3): ?>
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row">


                            <div class="col-lg-8 row_btn">
                                <?php echo $this->Html->link(__('Manage Categories'), array('controller' => 'products', 'action' => 'edit_category', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-success dim', 'title' => 'Edit Categories')); ?>       
                            </div>
                            <div class="col-lg-4 row_btn">
                                <?php if (in_array(AuthComponent::user('role'), array(1, 2))) { ?>
                                    <?php echo $this->Html->link(__('Preview'), array('controller' => 'products', 'action' => 'product_preview', $product['Product']['id']), array('class' => 'btn btn-outline btn-sm btn-warning dim', 'title' => 'Product Preview', 'target' => '_blank')); ?>                                     
                                <?php } ?>
                            </div>
                            <div class="col-lg-5 row_btn">
                                <?php
                                if (AuthComponent::user('role') == 1) {
                                    if ($product['Product']['is_active'] == 1) {
                                        echo $this->Html->link(__('Unpublish'), array('action' => 'deactive', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-info', 'title' => 'DeActive'));
                                    } else {
                                        echo $this->Html->link(__('Publish'), array('action' => 'active', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-info', 'title' => 'Active'));
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-lg-7 row_btn">
                                <?php echo $this->Html->link(__('Manage Countries'), array('controller' => 'products', 'action' => 'add_product_countries', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-primary dim', 'title' => 'View countries')); ?>                                     
                            </div>

                            <div class="col-lg-3 row_btn">
                                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $product['Product']['id']), array('class' => 'btn  btn-outline btn-sm btn-info ', 'title' => 'Edit')); ?>       
                            </div>
                            <div class="col-lg-6 row_btn">
                                <?php echo $this->Html->link(__('Manage Specs.'), array('controller' => 'specifications', 'action' => 'add_product_spc', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-primary dim', 'title' => 'Edit Specification')); ?>  
                            </div>
                            <div class="col-lg-2 row_btn">
                                <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $product['Product']['id']), array('class' => 'btn   btn-outline btn-sm btn-danger dim', 'title' => 'Delete'), __('Are you sure you want to delete # %s?', $product['Product']['id'])); ?>
                                <?php } ?>
                            </div>



                        </div> 
                        <div class="row">



                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div> 

        <div class="wrapper wrapper-content project-manager">
            <h4>Product Image</h4>
            <?php
            if (!empty($product['Product']['product_image'])) {
                echo $this->Html->image('product_images/' . $product['Product']['product_image'], array('class' => 'img-responsive'));
            } else {
                echo $this->Html->image('noimage.png', array('class' => 'img-responsive'));
            }
            ?>
            <?php if (!empty($product_categories)) { ?>
                <h5>Other Categories</h5>
                <ul class="tag-list" style="padding: 0">
                    <?php foreach ($product_categories as $pc): ?>
                        <li><a href="#"><i class="fa fa-tag"></i> <?= $pc ?></a></li>
                    <?php endforeach; ?>
                </ul>
            <?php } ?>
            <?php if (!empty($product_countries)) { ?>
                <h5>Countries</h5>
                <ul class="tag-list" style="padding: 0">
                    <?php foreach ($product_countries as $pc): ?>
                        <li><a href="#"><i class="fa fa-tag"></i> <?= $pc ?></a></li>
                    <?php endforeach; ?>
                </ul>
            <?php } ?> 
            <?php if (!empty($product_specifications)) { ?>
                <h5>Specifications & Options</h5>
                <ul class="tag-list" style="padding: 0">
                    <?php foreach ($product_specifications as $key => $pc): ?>
                        <li>
                            <a href="#"><i class="fa fa-anchor"></i> <?= $key ?></a>
                            <?php if (is_array($pc) && !empty($pc)): ?>
                                <ul>
                                    <?php foreach ($pc as $op): ?>
                                        <a href="#"><i class="fa fa-arrow-right"></i> <?= $op ?></a>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php } ?>
        </div>
    </div>
</div>