<script type="text/javascript">
   $(document).ready(function () {   
    var fieldID,fieldNum;               
    var cnt = parseInt($('#cnt').val());//hidden input from form        
    var next = cnt - 1;     
    console.log('next=',next,'cnt=',cnt);
    $(document).on('click','#add-more',function(e) {            
        e.preventDefault();                      
        var addto = "#field" + (next);
        var addRemove = "#field" + (next);         
        next = next + 1;                               
        var newIn = ' <div id="field'+ next +'" name="field'+ next +'"><!-- Text input--><div class="form-group"> <label class="col-md-4 control-label" for="question">Question</label> <div class="col-md-5"> <input maxlength="300" id="question" name="faq[Question][]" type="text" placeholder="" class="form-control input-md" style="margin-bottom:5px;"> </div></div><br><br> <!-- Text input--><div class="form-group"> <label class="col-md-4 control-label" for="answer">Answer</label> <div class="col-md-5"> <input maxlength="300" id="answer" name="faq[Answer][]" type="text" placeholder="" class="form-control input-md" style="margin-bottom:5px;"> </div></div><br><br><!-- File Button -->  <label class="col-md-4 control-label" for="action_json"></label> <div class="col-md-4"></div></div>';       
        var newInput = $(newIn);       
        var removeBtn = '<button id="remove' + (next) + '" class="btn btn-danger remove-me" >Remove</button></div></div><div id="field">';       
        var removeButton = $(removeBtn);        
        $(addto).after(newInput);        
        $(addRemove).after(removeButton);                      
        $("#field" + next).attr('data-source',$(addto).attr('data-source'));       
            $('.remove-me').click(function(e) {                
                e.preventDefault();
                fieldNum = this.id.charAt(this.id.length-1);
                fieldID = "#field" + fieldNum;                                              
                $(this).remove();
                $(fieldID).remove();     
                next = cnt - 1;  
                // if(($(this).length == 1) && ($(fieldID).length == 0)) 
                // { 
                //     next = cnt - 1; 
                //     cnt++;                                   
                // } 
                // else
                // {                    
                //     next = cnt;                 
                //     next++;
                // }                                      
            });                                      
    });                
});
</script>
<style>
    .bg { background: aquamarine ;
          margin-left: 5px;
    }
    .bread{
        margin-bottom: 10px;
    }
    .breadcrumb>li+li:before {
        padding: 0 5px;
        color: #ccc;
        content: ">>\00a0" !important;
    }
    .next_cat_div{
        margin-top: 10px;
    }
</style>
<div class="ibox float-e-margins">
    <div id="form" action="#" class="wizard-big wizard clearfix" role="application" novalidate="novalidate">
        <div class="bread">
            <ol class="breadcrumb">
                <li>
                    <a href="#">Products</a>
                </li>
                <?php if (!empty($prod_name['Category']['category_name'])) : ?>
                    <li class="active">
                        <strong><?php echo $prod_name['Category']['category_name']; ?></strong>
                    </li>
                <?php endif; ?>
            </ol>
        </div>
        <div class="steps clearfix"><ul role="tablist">
                <li  class="first current" aria-disabled="false" aria-selected="true">
                    <a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'edit', $pro_id)) ?>">
                         Edit Product</a></li>
                <li  class="first current"  title="Active" style="width:200px;"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'edit_category', $pro_id)) ?>" > Edit Categories  </a></li>
                <?php if (!empty($prod_data['Product']['product_faq'])) { ?>
                    <li  class="first current" title="Active" style="width:200px;"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'edit_faq', $pro_id)) ?>" > Edit FAQ</a></li>
                <?php } else { ?>
                    <li  class="disabled" title="Active" style="width:200px;"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'add_faq', $pro_id)) ?>" > Add FAQ</a></li>
                <?php } ?>                
                <li  class="first current"  title="Active" style="width:200px;"><a  href="<?php echo Router::url(array('controller' => 'specifications', 'action' => 'add_product_spc', $pro_id)) ?>" > Edit Specification</a></li>
                <?php if ($res == 1) { ?>
                    <li  class="first current"  title="Active" style="width:200px;"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'add_product_countries', $pro_id)) ?>" > Edit Countries</a></li>
                    <?php
                } else {
                    ?>
                    <li class = "first current" title = "Active" style="width:200px;"><a href = "<?php echo Router::url(array('controller' => 'products', 'action' => 'add_product_countries', $pro_id)) ?>" > Add Countries</a></li>
                <?php }
                ?>
            </ul>
        </div>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('Product', array('id' => 'edit_faq')) ?>
        <div class="row">           
            <div class="container">               
                <div class="row">                  
                  <div class="col-xs-12">
                        <div class="col-md-12" >
                                <h3> FAQ's</h3>                                
                                <div id="field">
                                <?php $i = 0; if(!empty($questions_answers_array)) { 
                                    foreach($questions_answers_array as $question => $answer) { ?>                                
                                    <div id="field<?php echo $i; ?>">                                        
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="question">Question</label>  
                                            <div class="col-md-5">
                                                <input maxlength="300" id="question" name="faq[Question][]" type="text" placeholder="" class="form-control input-md" style="margin-bottom:5px;" value="<?php echo $question; ?>">                                        
                                            </div>
                                        </div>
                                        </br></br>                                        
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="answer">Answer</label>  
                                            <div class="col-md-5">
                                                <input maxlength="300" id="answer" name="faq[Answer][]" type="text" placeholder="" class="form-control input-md" style="margin-bottom:5px;" value="<?php echo $answer; ?>">                                        
                                            </div>
                                        </div>
                                        </br></br>                                                                               
                                        <label class="col-md-4 control-label" for="action_json"></label>
                                        <div class="col-md-4">
                                         <input id="prod_id" name="prod_id" type="hidden" placeholder="" class="form-control input-md" style="margin-bottom:5px;" value="<?php echo $pro_id; ?>">                                         
                                        </div>                                       
                                    </div>
                                <?php $i++; } } ?> 
                                <input id="cnt" name="cnt" type="hidden" placeholder="" class="form-control input-md" style="margin-bottom:5px;" value="<?php echo $i; ?>">                                
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <button id="add-more" name="add-more" class="btn btn-primary">Add More</button>
                                    </div>
                                </div>
                                </div>                                                                                                                             
                                <br><br>                            
                        </div>
                    </div>
                </div>                               
             </div>                     
        </div>       
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
            <button onclick="goBack();" class="btn btn-sm btn-info">Back</button>
        </div>
        <?php echo $this->Form->end(); ?>
    </div> 
</div>