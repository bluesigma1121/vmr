<script type="text/javascript">
    $(document).ready(function() {
        $('#add_Country').bValidator();
    });
</script>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Add Country</h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('Country', array('id' => 'add_Country', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-3">
            <div class="form-group"><label class="col-lg-2 control-label">Country Name</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('country_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Country Name', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
        </div>
        <?php echo $this->Form->input('id'); ?>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
            <button onclick="goBack();" class="btn btn-sm btn-info">Back</button>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
