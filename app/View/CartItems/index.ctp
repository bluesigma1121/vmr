<style>
    .mtp{
        margin-top: 10px;
    }
    .search_mtp{
        margin-top: 3px;
    }
</style>


<div class="table-responsive mtp">
    <table class="table table-bordered table-striped" id="product_index_tbl">
        <thead>
            <tr> 



                <th>Name</th>
                <th>Product Name</th>
                <th>Price</th>
                <th>Offer Price</th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
        </thead> 
        <?php
        foreach ($cartItems as $cartItem) {
            ?>
            <tbody>
            <td><?php echo h($cartItem['User']['name']); ?>&nbsp;</td>
            <td><?php echo h($cartItem['Product']['product_name']); ?>&nbsp;</td>
            <td><?php echo h($cartItem['CartItem']['current_price']); ?>&nbsp;</td>
            <td><?php echo h($cartItem['CartItem']['offer_price']); ?>&nbsp;</td>


            <td class="actions" style="min-width: 250px; max-width: 250px;">
                <?php // echo $this->Html->link(__('view'), array('action' => 'view', $cartItem['CartItem']['id']), array('class' => 'btn   btn-outline btn-sm btn-warning ', 'title' => 'View')); ?>
                <?php echo $this->Html->link(__(''), array('action' => 'offer_price', $cartItem['CartItem']['id']), array('class' => 'btn   btn-outline btn-sm btn-warning fa fa-plus', 'title' => 'View')); ?>



            </td>
            </tbody>
        <?php } ?>
    </table>
    <div class="row col-md-12">
        <div class="dataTables_paginate paging_bootstrap">
            <p>
                <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                ));
                ?> </p>
            <ul class="pagination" style="visibility: visible;">
                <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
                <li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
                <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
            </ul>
        </div>
    </div>
</div>






