<div class="job-description">
    <div class="container">
        <div class="shopping-cart mtp" action="#" novalidate="novalidate">
            <div role="application" class="wizard clearfix" id="steps-uid-0">
                <div class="steps clearfix">
                    <ul role="tablist">
                        <li role="tab" class="first current"><a href="<?php echo Router::url(array('controller' => 'cart_items', 'action' => 'add_to_cart')); ?>"><span class="current-info audible">current step: </span><span class="number">1.</span>             
                                <div class="overflow-h">
                                    <h2>Shopping Cart</h2>
                                    <p>Review &amp; edit your product</p>
                                    <i class="rounded-x fa fa-check"></i>
                                </div>    
                            </a>
                        </li>
                        <li role="tab" class="first current" aria-disabled="true">
                            <a id="steps-uid-0-t-1" href="<?php echo Router::url(array('controller' => 'cart_items', 'action' => 'billing_info')); ?>"><span class="number">2.</span> 
                                <div class="overflow-h">
                                    <h2>Billing info</h2>
                                    <p>Shipping and address info</p>
                                    <i class="rounded-x fa fa-home"></i>
                                </div>    
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="content clearfix">
                    <div class="header-tags title" id="steps-uid-0-h-1" tabindex="-1">
                        <div class="overflow-h">
                            <h2>Billing info</h2>
                            <p>Shipping and address info</p>
                            <i class="rounded-x fa fa-home"></i>
                        </div>    
                    </div>
                    <section class="billing-info body" id="steps-uid-0-p-1" role="tabpanel" aria-labelledby="steps-uid-0-h-1"  >
                        <div class="row">
                            <div class="col-md-8 col-lg-offset-2">
                                <div class="row">
                                    <h2 class="title-type">Billing Address</h2>
                                    <div class="col-md-12">
                                        <?php echo $this->Form->create('User', array('id' => 'bill_add_cart', 'class' => 'reg-page', 'type' => 'file')); ?>
                                        <?php if(!AuthComponent::user()): ?>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>First Name <span class="color-red">*</span></label>
                                                <?php echo $this->Form->input('first_name', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'First Name', 'data-bvalidator' => 'alpha,required')); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Last Name <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('last_name', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Last Name', 'data-bvalidator' => 'alpha,required')); ?>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Email <span class="color-red">*</span></label>
                                                <?php echo $this->Form->input('email', array('class' => 'form-control margin-bottom-20', 'label' => false, 'placeholder' => 'Email', 'data-bvalidator' => 'email,required')); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Mobile <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Mobile Number', 'size' => '20', 'maxlength' => '25', 'data-bvalidator' => 'required')); ?>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Organisation <span class="color-red">*</span></label>
                                                <?php echo $this->Form->input('organisation', array('class' => 'form-control margin-bottom-20', 'label' => false, 'placeholder' => 'Organisation', 'data-bvalidator' => 'required')); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Designation <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('job_title', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text','data-bvalidator' => 'required','placeholder'=>'Designation')); ?>
                                            </div>
                                        </div>
                                        
                                        <?php endif; ?>
                                        
                                        
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Address <span class="color-red">*</span></label>
                                                <?php echo $this->Form->input('address', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Address', 'data-bvalidator' => 'required','value'=>  AuthComponent::user('address'))); ?>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>City <span class="color-red">*</span></label>
                                                <?php echo $this->Form->input('city', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'city', 'data-bvalidator' => 'required','value'=>  AuthComponent::user('city'))); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>State <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('state', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'State', 'data-bvalidator' => 'required','value'=>  AuthComponent::user('state'))); ?>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Country <span class="color-red">*</span></label>
                                                <?php echo $this->Form->input('country', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Country', 'data-bvalidator' => 'required','value'=>  AuthComponent::user('country'))); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Pincode <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('pin_code', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Pincode', 'data-bvalidator' => 'required')); ?>
                                            </div>
                                        </div> 
                                    </div>
                                </div>  
                                <div class="actions clearfix mtp"> 
                                    <div class="pull-left">
                                        <a class="btn-u" href="<?php echo Router::url(array('controller' => 'cart_items', 'action' => 'add_to_cart')) ?>">Back</a>
                                    </div>
                                    <div class="pull-right">
                                        <?php echo $this->Form->submit('Proceed', array('class' => 'btn-u')) ?>
                                    </div>
                                </div>
                                <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                    </section>
                </div> 
            </div>
        </div>
    </div>
</div>


