<?php echo $this->element('listing_search'); ?> 
<div class="deva_cart">
    <?php echo $this->element('front_breadcumb'); ?>
</div>
<style>
    .cross_line{
        color:red;
        text-decoration:line-through
    }
</style>
<div class="job-description">
    <div class="container">
        <div class="shopping-cart mtp_cart" action="#" novalidate="novalidate">
            <div role="application" class="wizard clearfix" id="steps-uid-0">
                <div class="steps clearfix">
                    <ul role="tablist">
                        <li role="tab" class="first current"><a href="<?php echo Router::url(array('controller' => 'cart_items', 'action' => 'add_to_cart')); ?>" aria-controls="steps-uid-0-p-0"><span class="current-info audible">current step: </span><span class="number">1.</span>             
                                <div class="overflow-h">
                                    <h2>Shopping Cart</h2>
                                    <p>Review &amp; edit your product</p>
                                    <i class="rounded-x fa fa-check"></i>
                                </div>    
                            </a>
                        </li>
                        <li role="tab" class="disabled" aria-disabled="true">
                            <a id="steps-uid-0-t-1" href="#" aria-controls="steps-uid-0-p-1"><span class="number">2.</span> 
                                <div class="overflow-h">
                                    <h2>Billing info</h2>
                                    <p>Shipping and address info</p>
                                    <i class="rounded-x fa fa-home"></i>
                                </div>    
                            </a>
                        </li>

                    </ul>
                </div>
                <div class="actions clearfix"> 
                    <div class="pull-left">
                        <a class="btn-u btn-u-green" href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'home')) ?>">Continue Buying</a>
                    </div>
                    <div class="pull-right">
                        <?php if (count($result) > 0) { ?>
                            <a class="btn-u btn-u-green" href="<?php echo Router::url(array('controller' => 'cart_items', 'action' => 'billing_info')) ?>">Proceed</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="content clearfix">
                    <section id="steps-uid-0-p-0" role="tabpanel" aria-labelledby="steps-uid-0-h-0" class="body current" aria-hidden="false">
                        <div class="table-responsive">
                            <?php if (!empty($result)) { ?>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th><b>Product</b></th>
                                            <th><b>Price</b></th>
                                            <th><b>Qty</b></th>
                                            <th><b>Total</b></th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                        <?php
                                        foreach ($result as $key => $prod):
                                            ?>
                                            <tr>
                                                <td class="product-in-table"> 
                                                    <?php if (!empty($prod['Product']['product_image'])) { ?> 
                                                        <?php echo $this->Html->image('product_images/' . $prod['Product']['product_image'], array('class' => 'img-responsive hei_whidth_cart')); ?>
                                                        <?php
                                                    } else {
                                                        echo $this->Html->image('noimage.png', array('class' => 'img-responsive hei_whidth_cart'));
                                                    }
                                                    ?>
                                                    <div class="product-it-in">
                                                        <h3><?php echo $prod['Product']['product_name']; ?>   (<?php echo $licence_type[$prod['Product']['licence_type']]; ?>) </h3>
                                                    </div>   
                                                </td>
                                                <td><?php
                                                    $final_price = round(($prod['Product']['subtotal'] * $dollar_rate), 2);
                                                    $final_offer = round(($prod['Product']['offer_price'] * $dollar_rate), 2);
                                                    if ($cur_curency == 0) {
                                                        if ($prod['Product']['is_offer_set'] == 1) {
                                                            ?>
                                                            <span class="cross_line"><b><i class="fa fa-inr"></i> </b><?php echo $final_price; ?> </span><br> <b><i class="fa fa-inr"></i> </b> <?php echo $final_offer; ?>
                                                        <?php } else { ?>
                                                            <b><i class = "fa fa-inr"></i> </b><?php
                                                            echo $final_price;
                                                        }
                                                        ?>

                                                        <?php
                                                    } else {
                                                        if ($prod['Product']['is_offer_set'] == 1) {
                                                            ?>
                                                            <span class="cross_line"> <b><i class="fa fa-dollar"></i> </b><?php echo $final_price; ?> </span><br> <b><i class="fa fa-dollar"></i> </b>  <?php echo $final_offer; ?>
                                                        <?php } else { ?>
                                                            <b><i class = "fa fa-dollar"></i> </b><?php
                                                            echo $final_price;
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    1
                                                </td>
                                                <td class="shop-red"><?php
                                                    $final_price = round(($prod['Product']['subtotal'] * $dollar_rate), 2);
                                                    if ($cur_curency == 0) {
                                                        ?>
                                                        <b><i class="fa fa-inr"></i> </b><?php echo $final_price; ?>
                                                    <?php } else {
                                                        ?>
                                                        <b><i class="fa fa-dollar"></i> </b><?php
                                                        echo number_format($final_price, 2);
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php echo $this->Form->postLink(__(''), array('action' => 'remove_cart_product', $prod['Product']['id'], $prod['Product']['licence_type']), array('class' => 'btn-u btn-u-red hastip fa fa-close', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Remove'), __('Are you sure you want to Remove # %s?', ucfirst($prod['Product']['product_name']))); ?>    
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>

                                        <tr class="top_border_cart">
                                            <td> 
                                            </td>
                                            <td></td>
                                            <td><span><h3><b>Total</b></h3></span></td>
                                            <td style="min-width: 165px;">
                                                <span><h3><b><?php
                                                            $final_price = round(($total * $dollar_rate), 2);
                                                            if ($cur_curency == 0) {
                                                                ?>
                                                                <b><i class="fa fa-inr"></i>  </b><?php echo $final_price; ?>
                                                            <?php } else {
                                                                ?>
                                                                <b><i class="fa fa-dollar"></i> </b><?php
                                                                echo number_format($final_price, 2);
                                                            }
                                                            ?> </b></h3></span>                                                         
                                            </td>

                                            </td>
                                            <td>
                                            </td>
                                        </tr> 
                                    </tbody>
                                </table>
                            </div>
                        </section> 
                    </div> 
                    <div class="actions clearfix"> 
                        <div class="pull-left">
                            <a class="btn-u btn-u-green" href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'home')) ?>">Continue Buying</a>
                        </div>
                        <div class="pull-right">
                            <?php if (count($result) > 0) { ?>
                                <a class="btn-u btn-u-green" href="<?php echo Router::url(array('controller' => 'cart_items', 'action' => 'billing_info')) ?>">Proceed</a>
                            <?php } ?>
                        </div>
                    </div>

                <?php } else {
                    ?>
                    <div class="content clearfix">
                        <span class = "text-center"><h5>There are no items in this Cart. </h5></span>
                        <div class = "text-center">
                            <a class = "btn btn-warning bold" href = "<?= Router::url(array('controller' => 'categories', 'action' => 'home')); ?>">Continue Buying</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div> 
</div>
</div>
</div>




