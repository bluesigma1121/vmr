<div class="productSpecifications form">
<?php echo $this->Form->create('ProductSpecification'); ?>
	<fieldset>
		<legend><?php echo __('Add Product Specification'); ?></legend>
	<?php
		echo $this->Form->input('product_id');
		echo $this->Form->input('specification_id');
		echo $this->Form->input('specification_option_id');
		echo $this->Form->input('is_active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Product Specifications'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Specifications'), array('controller' => 'specifications', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Specification'), array('controller' => 'specifications', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Specification Options'), array('controller' => 'specification_options', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Specification Option'), array('controller' => 'specification_options', 'action' => 'add')); ?> </li>
	</ul>
</div>
