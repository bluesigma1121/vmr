<div class="productSpecifications view">
<h2><?php echo __('Product Specification'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($productSpecification['ProductSpecification']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($productSpecification['Product']['id'], array('controller' => 'products', 'action' => 'view', $productSpecification['Product']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Specification'); ?></dt>
		<dd>
			<?php echo $this->Html->link($productSpecification['Specification']['id'], array('controller' => 'specifications', 'action' => 'view', $productSpecification['Specification']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Specification Option'); ?></dt>
		<dd>
			<?php echo $this->Html->link($productSpecification['SpecificationOption']['id'], array('controller' => 'specification_options', 'action' => 'view', $productSpecification['SpecificationOption']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($productSpecification['ProductSpecification']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($productSpecification['ProductSpecification']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($productSpecification['ProductSpecification']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Product Specification'), array('action' => 'edit', $productSpecification['ProductSpecification']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Product Specification'), array('action' => 'delete', $productSpecification['ProductSpecification']['id']), null, __('Are you sure you want to delete # %s?', $productSpecification['ProductSpecification']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Product Specifications'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product Specification'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Specifications'), array('controller' => 'specifications', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Specification'), array('controller' => 'specifications', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Specification Options'), array('controller' => 'specification_options', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Specification Option'), array('controller' => 'specification_options', 'action' => 'add')); ?> </li>
	</ul>
</div>
