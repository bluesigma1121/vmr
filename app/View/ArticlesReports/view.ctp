<div class="articlesReports view">
<h2><?php echo __('Articles Report'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($articlesReport['ArticlesReport']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Article'); ?></dt>
		<dd>
			<?php echo $this->Html->link($articlesReport['Article']['id'], array('controller' => 'articles', 'action' => 'view', $articlesReport['Article']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($articlesReport['Product']['id'], array('controller' => 'products', 'action' => 'view', $articlesReport['Product']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($articlesReport['ArticlesReport']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($articlesReport['ArticlesReport']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Articles Report'), array('action' => 'edit', $articlesReport['ArticlesReport']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Articles Report'), array('action' => 'delete', $articlesReport['ArticlesReport']['id']), null, __('Are you sure you want to delete # %s?', $articlesReport['ArticlesReport']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Articles Reports'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Articles Report'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Articles'), array('controller' => 'articles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Article'), array('controller' => 'articles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
	</ul>
</div>
