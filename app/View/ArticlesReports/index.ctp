<div class="articlesReports index">
    <h2><?php echo __('Articles Reports'); ?></h2>
    <table class="table table-bordered table-condensed">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('article_id'); ?></th>
            <th><?php echo $this->Paginator->sort('product_id'); ?></th>
            <th><?php echo $this->Paginator->sort('created'); ?></th>
            <th><?php echo $this->Paginator->sort('modified'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        <?php foreach ($articlesReports as $articlesReport): ?>
            <tr>
                <td><?php echo h($articlesReport['ArticlesReport']['id']); ?>&nbsp;</td>
                <td>
                    <?php echo $this->Html->link($articlesReport['Article']['headline'], array('controller' => 'articles', 'action' => 'view', $articlesReport['Article']['id'])); ?>
                </td>
                <td>
                    <?php echo $this->Html->link($articlesReport['Product']['product_name'], array('controller' => 'products', 'action' => 'view', $articlesReport['Product']['id'])); ?>
                </td>
                <td><?php echo h($articlesReport['ArticlesReport']['created']); ?>&nbsp;</td>
                <td><?php echo h($articlesReport['ArticlesReport']['modified']); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link(__('View'), array('action' => 'view', $articlesReport['ArticlesReport']['id'])); ?>
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $articlesReport['ArticlesReport']['id'])); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $articlesReport['ArticlesReport']['id']), null, __('Are you sure you want to delete # %s?', $articlesReport['ArticlesReport']['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('New Articles Report'), array('action' => 'add')); ?></li>
        <li><?php echo $this->Html->link(__('List Articles'), array('controller' => 'articles', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Article'), array('controller' => 'articles', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
    </ul>
</div>
