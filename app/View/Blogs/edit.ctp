<script>
    $(document).ready(function(){
       $('#blogAddForm').bValidator();
    });
</script>
<?php echo $this->Html->script('back/ckeditor/ckeditor.js'); ?>

<div class="ibox float-e-margins">
  <div class="ibox-content">
		<?php echo $this->Form->create('Blog', array('class' => 'form-horizontal','id' =>'blogAddForm')); ?>
			<fieldset>
				<legend><?php echo __('Edit Blog'); ?></legend>
				<div class="row">
					<div class="form-group">
						<label class="col-lg-3 control-label">Title</label>
						<div class="col-lg-8">
								<?php echo $this->Form->input('title',array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Title', 'label' => false, 'data-bvalidator' => 'required')); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3 control-label">Description</label>
						<div class="col-lg-8">
								<?php echo $this->Form->input('description',array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Description', 'label' => false, 'data-bvalidator' => 'required')); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3 control-label">Slug</label>
						<div class="col-lg-8">
								<?php echo $this->Form->input('slug',array('class' => 'ckeditor form-control', 'type' => 'text', 'placeholder' => 'Slug', 'label' => false, 'data-bvalidator' => 'required')); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3 control-label">Meata Title</label>
						<div class="col-lg-8">
								<?php echo $this->Form->input('meta_title',array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Meta Name', 'label' => false, 'data-bvalidator' => 'required')); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Meta Description</label>
						<div class="col-lg-8">
								<?php echo $this->Form->input('meta_desc',array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Meta Description', 'label' => false, 'data-bvalidator' => 'required')); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Keywords</label>
						<div class="col-lg-8">
								<?php echo $this->Form->input('meta_keywords',array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Meta Keyword', 'label' => false, 'data-bvalidator' => 'required')); ?>
						</div>
					</div>
					<?php echo $this->Form->input('id'); ?>
					<div class="form-group">
						<div class="col-lg-12 text-center">
								<?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success', 'div' => false)); ?>
						</div>
					</div>


				</div>
			</fieldset>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
