
<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>Blogs</h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a>Blogs</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->


<!-- ABOUT INNER SECTION START -->
<section class="about-inner">

  <div class="container">
    <div class="">
      <?php foreach ($blog as $blog): ?>
        <div class="blog-inner">
          <a href="<?php echo Router::url(array('controller'=>'blogs','action'=>'blog_details','blog_id'=>$blog['Blog']['id'],'url_slug'=>$this->Link->cleanString($blog['Blog']['slug']))); ?>">
          <h1><?php echo $blog['Blog']['title'] ?></h1>
            <?php
            echo $this->Text->truncate(
                    strip_tags($blog['Blog']['description']), 400, array(
                'ellipsis' => '...',
                'exact' => false
                    )
            );
            ?>
          </a>
          <div class="clearfix">
          </div>
          <div class="pull-left col-md-6 col-xs-12 col-sm-6 row">
              <h6 class="">Posted On <?php echo date('d-F-Y', strtotime($blog['Blog']['created'])) ?></h6>
          </div>

        </div>
      <?php endforeach; ?>

    </div>
  </div>
</section>

<!-- ABOUT INNER SECTION END -->
