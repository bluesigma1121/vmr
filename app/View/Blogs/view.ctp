
<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-xs-12 col-lg-6 inner-title">
          <h3><?php echo h($blog['Blog']['title']); ?></h3>
        </div>
        <div class="col-md-6 col-xs-12 col-lg-6">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a href="#." >Blog</li>
              <li><?php echo h($blog['Blog']['title']); ?></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->


<!-- ABOUT INNER SECTION START -->
<section class="about-inner">

      <div class="container">
        <!-- <h4>Blog Details</h4> -->
        <div class="">
          <div class="blog-inner blog-details-inner">
            <h1><?php echo h($blog['Blog']['title']); ?></h1>
             <div class="pull-left post-on">
                <h6 class="">Posted On <?php echo date('M d Y',strtotime($blog['Blog']['created'])) ?></h6>
            </div>
            <p><?php echo h($blog['Blog']['description']); ?></p>
        </div>
       </div>
    </div>
</section>

<!-- ABOUT INNER SECTION END -->
