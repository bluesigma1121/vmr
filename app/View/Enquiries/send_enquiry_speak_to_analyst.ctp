<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Value Market Research</title>
        <!-- Bootstrap core CSS -->
        <?php
        echo $this->Html->css(array('front/bootstrap/css/bootstrap.min.css', 'front/bvalidator.css'));
        ?>
        <?php
        echo $this->Html->script(array('front/plugins/jquery/jquery.min.js', 'front/plugins/bootstrap/js/bootstrap.min.js', 'front/jquery.bvalidator-yc.js','front/jquery.fancybox.js'
        ));
        ?>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->

        <style type="text/css">
            .mar_top_hed{
                margin-top: -40px;
                color: #555;
                text-align: center;
                margin-bottom: 5px;
                border-bottom: solid 1px #eee;

            }
            .mrtp_enq{
                padding: 5px;
            }
        </style>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#enquiry_form').bValidator();
                // var b_path = base_url + "phone-codes.json";

                //    var maskList = $.masksSort($.masksLoad(b_path), ['#'], /[0-9]|#/, "mask");


//                var maskOpts = {
//                    inputmask: {
//                        definitions: {
//                            '#': {
//                                validator: "[0-9]",
//                                cardinality: 1
//                            }
//                        },
//                        //clearIncomplete: true,
//                        showMaskOnHover: false,
//                        autoUnmask: true
//                    },
//                    match: /[0-9]/,
//                    replace: '#',
//                    list: maskList,
//                    listKey: "mask",
//                    onMaskChange: function(maskObj, determined) {
//                        if (determined) {
//                            var hint = maskObj.name_en;
//                            if (maskObj.desc_en && maskObj.desc_en != "") {
//                                hint += " (" + maskObj.desc_en + ")";
//                            }
//                            $("#descr").html(hint);
//                        } else {
//                            $("#descr").html("Mask of input");
//                        }
//                        $(this).attr("placeholder", $(this).inputmask("getemptymask"));
//                    }
//                };
//                $('#phone_mask').change(function() {
//                    if ($('#phone_mask').is(':checked')) {
//                        $('#customer_phone').inputmasks(maskOpts);
//                    } else {
//                        $('#customer_phone').inputmask("+[####################]", maskOpts.inputmask)
//                                .attr("placeholder", $('#customer_phone').inputmask("getemptymask"));
//                        $("#descr").html("Mask of input");
//                    }
//                });
//
//                $('#phone_mask').change();

            });
        </script>

    </head>
    <body>
        <div class="container content">
            <div class="row">
                <div class="col-sm-8">
                    <?php echo $this->Form->create('Enquiry', array('id' => 'enquiry_form', 'class' => 'reg-page', 'type' => 'file')); ?>
                    <div class="reg-header mar_top_hed">
                        <h2><?php echo $ref_page; ?></h2>
                    </div>
                    <?php if (!AuthComponent::user('id')): ?>
                        <div class="row">
                            <div class="col-sm-2">
                                <label>Title<span class="color-red">*</span></label>
                                <?php echo $this->Form->input('title', array('class' => 'form-control', 'type' => 'select', 'options' => $title, 'placeholder' => 'Title', 'label' => false, 'data-bvalidator' => 'required')); ?>
                            </div>
                            <div class="col-sm-5">
                                <label>First Name<span class="color-red">*</span></label>
                                <?php echo $this->Form->input('first_name', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'First Name', 'data-bvalidator' => 'required,alpha')); ?>
                            </div>
                            <div class="col-sm-5">
                                <label>Last Name<span class="color-red">*</span></label>
                                <?php echo $this->Form->input('last_name', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Last Name', 'data-bvalidator' => 'required,alpha')); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Email Address <span class="color-red">*</span></label>
                                <?php echo $this->Form->input('email', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Email', 'data-bvalidator' => 'required,email')); ?>
                            </div>

                            <div class="col-sm-6">
                                <label>Organisation <span class="color-red">*</span></label>
                                <?php echo $this->Form->input('organisation', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Organisation', 'data-bvalidator' => 'required')); ?>
                            </div>

                            <div class="col-sm-6">
                                <label>Designation <span class="color-red">*</span></label>
                                <?php echo $this->Form->input('job_title', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Designation', 'data-bvalidator' => 'required')); ?>
                            </div>

                            <div class="col-sm-6">
                                <label>Mobile <span class="color-red">*</span></label>
                                <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Mobile', 'id' => 'customer_phone_test', 'value' => AuthComponent::user('mobile'), 'size' => '20', 'maxlength' => '25', 'data-bvalidator' => 'required')); ?>
                            </div>
                        </div>
                    <?php else:
                        ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Mobile <span class="color-red">*</span></label>
                                <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Mobile', 'id' => 'customer_phone_test', 'value' => AuthComponent::user('mobile'), 'size' => '20', 'maxlength' => '25', 'data-bvalidator' => 'required')); ?>
                            </div>
                            <?php /* <div class="col-sm-6">
                              <div class="mar_phone">
                              <input type="checkbox" id="phone_mask" checked=""> <label id="descr" for="phone_mask"></label>
                              </div>
                              </div> */ ?>
                        </div>

                    <?php endif; ?>

                    <div class="row">
                        <div class="col-sm-12">
                            <label>Subject <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('subject', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Subject', 'data-bvalidator' => 'required')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Remarks <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('message', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'textarea', 'placeholder' => 'Remarks', 'data-bvalidator' => 'required')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Captcha       <span class="color-red">*</span></label>
                            <?php echo $this->Html->image($this->Html->url(array('controller' => 'users', 'action' => 'captcha'), true), array('label' => false, 'width' => '150', 'height' => '40', 'alt' => 'captcha')); ?>
                        </div>
                        <div class="col-sm-4">
                            <label>Enter Code <span class="color-red">*</span></label>
                            <?php
                            echo $this->Form->input('captcha', array('data-bvalidator' => 'required', 'class' => 'input-large form-control hastip', 'data-placement' => 'right', 'type' => 'text', 'placeholder' => 'Enter Captcha Code',
                                'title' => 'Enter the characters you see in the image.', 'label' => false));
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="text-center mrtp_enq">
                            <?php echo $this->Form->submit('Send', array('class' => 'btn-u')) ?>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </body>
</html>
