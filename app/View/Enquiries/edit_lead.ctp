
<script type="text/javascript">
    var max_year = "<?= date('Y', strtotime('+1 year')) ?>";
    var min_year = "<?= date('Y', strtotime('-1 year')) ?>";
    $(document).ready(function () {
        $('#edit_enquiery').bValidator();
        $("#next_follow_date").combodate({
            firstItem: 'name',
            minuteStep: 1,
            minYear: min_year,
            maxYear: max_year,
        });
    });
</script>
<style>
    .mtp{
        margin-top: 16px; 
    }
    .ml_15{
        margin-left: 15px;
    }
    .ml_5px{
        margin-left: 3px;
    }
</style>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5> Edit  Inquiry</h5>
    </div> 
    <div class="ibox-content">
        <?php echo $this->Form->create('Enquiry', array('id' => 'edit_enquiery', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <table class="table table-bordered table-condensed">
            <tr>
                <th>Name</th>
                <th>Report Name</th>
            </tr>
            <tr>
                <td>
                    <?php echo ucfirst($this->request->data['User']['name']); ?>
                    <?php echo $this->Form->input('id'); ?>
                </td>
                <td><?php echo ucfirst($this->request->data['Product']['product_name']); ?></td>
            </tr>
            <tr>
                <th>Inquiry Subject</th>
                <th>Inquiry Message</th>
            </tr>
            <tr>
                <td><?php echo ucfirst($this->request->data['Enquiry']['subject']); ?></td>
                <td><?php echo ucfirst($this->request->data['Enquiry']['message']); ?></td>
            </tr>
        </table>

        <div class="row">
            <h3>Previous Remarks</h3>
            <div class="col-lg-7">
                <?php if (!empty($prev_rem)): ?>
                    <table class="table table-bordered table-condensed">
                        <tr>
                            <th style="width:430px;max-width:430px;">Remark</th>
                            <th>Status</th>
                            <th>Rating</th>
                            <th>Added at</th>
                        </tr>
                        <?php foreach ($prev_rem as $key => $remarks): ?>
                            <tr>
                                <td><?php echo $remarks['message'] ?></td>
                                <td><?php echo $status[$remarks['status']]; ?></td>
                                <td><?php echo $rating[$remarks['rating']]; ?></td>
                                <td><?php echo Date('d-M-Y h:i a', strtotime($remarks['date'])); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                <?php endif; ?>
            </div>
            <div class="col-lg-5">
                <div class="form-group ">
                    <label class="control-label">Rating</label>
                    <div>
                        <?php echo $this->Form->input('rating', array('class' => 'form-control', 'options' => $rating, 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Status</label>
                    <div>
                        <?php echo $this->Form->input('status', array('class' => 'form-control', 'options' => $status, 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Remark</label>
                    <div>
                        <?php echo $this->Form->input('new_remark', array('class' => 'form-control', 'type' => 'textarea', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Next Followup Date </label>
                    <div>
                        <?php
                        echo $this->Form->input('next_followup_date', array('id' => 'next_follow_date',
                            'value' => date("Y-m-d h:i:s", strtotime($this->request->data['Enquiry']['next_followup_date'])),
                            'type' => 'text', 'data-format' => 'YYYY-MM-DD H:mm:ss', 'data-template' => 'DD-MMM-YYYY h:mm a',
                            'class' => 'form-control', 'label' => false, 'data-bvalidator' => 'required'));
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?> 
                    <a class="btn btn-warning btn-sm" onclick="goBack()">Back</a>
                </div>
            </div> 
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>