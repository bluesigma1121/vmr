
<style>
    .mtp{
        margin-top: 10px;
    }
    .search_mtp{
        margin-top: 3px;
    }
    hr {
        margin-top: 6px;
        margin-bottom: 6px;
        border: 0;
        border-top: 1px solid #eee;
    }
    .checkbox{
        display: inline-block;
    }
    .checkbox input[type=checkbox]{
        margin-left: 4px;
        margin-right: 4px;
    }
    .checkbox > label {
        width: 80px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('#selecctall').click(function (event) {  //on click
            if (this.checked) { // check select status
                $('.checkbox1').each(function () { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"
                });
            } else {
                $('.checkbox1').each(function () { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"
                });
            }
        });
        $('#btn_bulk_delete').click(function () {
            var usr_str = '';
            $("#enq_index_tbl tr").each(function (i, row) {
                if ($(this).find("td:first").find('input:checked').prop('checked') == true) {
                    var val = $(this).find("td:first").find('input:checked').val();
                    if (usr_str != '') {
                        usr_str = usr_str + "," + val;
                    }
                    else {
                        usr_str = val;
                    }
                }
            });
            if (usr_str == '') {
                alert('Please select checkbox to perform action.');
                return false;
            }
            else {
                $("#selected_usr_field").val(usr_str);
            }
        });


        //////////////////////////////////////////////////////
    });
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Leads List</h5>
                <!-- Code for enquiry status summary start VG-20-05-2017 -->
                    <?php if (AuthComponent::user('role') == 1 || AuthComponent::user('role') == 12){ ?>
                        <span class="pull-right">
                          <span class="badge label label-primary">Sample Sent:&nbsp;&nbsp;<b style="letter-spacing: 1.1px;"><?= $sample;?></b></span>
                          <span class="badge label label-success">Email Requested:&nbsp;&nbsp;<b style="letter-spacing: 1.1px;"><?= $mailrequest;?></b></span>
                          <span class="badge label label-danger">No Status:&nbsp;&nbsp;<b style="letter-spacing: 1.1px;"><?= $nostatus;?></b></span>
                        </span>
                    <?php } ?>
                 <!-- Code for enquiry status summary end VG-20-05-2017 -->
            </div>
            <div class = "ibox-content">
                <?php echo $this->Form->create('Enquiry', array('action' => 'index','id' => 'index', 'class' => 'form-horizontal'));
                ?>


                <div class="row">

                        <div class="row mtp">
                            <div class="col-lg-12">
                              <?php echo $this->Form->create('Enquiry', array('action' => 'change_status_selected', 'class' => 'form-inline', 'id' => 'bulk_actions')); ?>
                                <?php if (AuthComponent::user('role') == 1): ?>
                                <!-- <div class="col-lg-1">


                                </div> -->
                                <?php echo $this->Form->input('selected_enquiries', array('type' => 'hidden', 'id' => 'selected_usr_field')); ?>

                                <div class="col-lg-1">
                                  Bulk Action:
                                    <?php echo $this->Form->submit('Delete Selected', array('name' => 'action', 'id' => 'btn_bulk_delete', 'class' => 'btn btn-outline btn-sm btn-primary dim tooltip-f btn_bulk_active', 'title' => 'Activate Selected')); ?>
                                </div>
                                <?php endif; ?>
                                <?php /*   <div class="col-lg-2">
                                  <?php echo $this->Form->submit('Delete Selected', array('name' => 'delete', 'class' => 'btn   btn-outline btn-sm btn-danger dim tooltip-f btn_bulk_active', 'title' => 'Delete Selected')); ?>
                                  </div> */ ?>

                                <!-- Code added for filters VG-7/03/2017 Start   -->

                                  <div class="col-lg-2">
                                  Filter by Rating:
                                    <?php echo $this->Form->input('rating', array('multiple'=>true,'class'=>'form-control input-sm', 'options' => $rating, 'label' => false)); ?>
                                  </div>

                                  <div class="col-lg-2">
                                    Filter by Status:
                                    <?php echo $this->Form->input('status', array('multiple'=>true, 'class'=>'form-control input-sm','options' => $status, 'label' => false)); ?>
                                  </div>

                                  <div class="col-lg-2">
                                      <label for="form-field-8">Product Name : </label>
                                      <?php echo $this->Form->input('search_text', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Enter Text For Search', 'label' => false)); ?>
                                  </div>

                                  <div class="col-md-4">
                                    <?php echo $this->Form->input('created', array(  'type'=>'date',
                                                                                      'label' => 'Created Date',
                                                                                      'dateFormat' => 'YMD',
                                                                                      'timeFormat' => '24',
                                                                                      'minYear' => date('Y') - 30,
                                                                                      'maxYear' => date('Y'),

                                                                                  )); ?>
                                    <!-- Ignore Created Date Filter on search start VG-12/02/2018 -->
                                    <input type="checkbox" name="isdatefilter" <?php if($isdtfilter==true) {?>checked<?php }?> />&nbsp;Apply Created Date Filter
                                    <!-- Ignore Created Date Filter on search end VG-12/02/2018 -->

                                  </div>

                                    <?php echo $this->Form->submit(__('Search'), array('class' => 'btn btn-success btn-sm btn-outline','style'=>'margin:10px 10px;' ,'div' => false)); ?>

                                <!-- Code added for filters VG-7/03/2017 End-->

                                <?php echo $this->Form->end(); ?>
                            </div>
                        </div>

                </div>


                <div class="table-responsive mtp">
                    <table id="enq_index_tbl" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                              <th><?php echo $this->Paginator->sort('id'); ?></th>
                                <th style="width: 10px;">
                                    <input type="checkbox" id="selecctall"> All
                                </th>
                                <th><?php echo $this->Paginator->sort('user_id', 'Name'); ?></th>
                                <th><?php echo $this->Paginator->sort('ref_page'); ?></th>
                                <th style="min-width: 150px; max-width: 200px; width: 150px;"><?php echo $this->Paginator->sort('email', 'Email'); ?></th>
                                <th><?php echo $this->Paginator->sort('product_id'); ?></th>
                                <th><?php echo $this->Paginator->sort('publisher_name'); ?></th>
                                <th><?php echo $this->Paginator->sort('progress'); ?></th>
                                <th><?php echo $this->Paginator->sort('rating'); ?></th>
                                <th><?php echo $this->Paginator->sort('message'); ?></th>
                                <th style="min-width: 300px; max-width: 300px; width: 300px;"><?php echo $this->Paginator->sort('remark'); ?></th>
                                <th><?php echo $this->Paginator->sort('Country'); ?></th>
                                <th><?php echo $this->Paginator->sort('organisation'); ?></th>
                                <th><?php echo $this->Paginator->sort('job_title', 'Designation'); ?></th>
                                <th><?php echo $this->Paginator->sort('created'); ?></th>
                              <!-- Coloumns added VG-7-03-2017 Start   -->
                              <!-- Coloumns added VG-7/03/2017 End   -->
                            </tr>
                        </thead>
                        <?php  foreach ($enquiries as $enquirie) {  //echo "<pre>"; print_r($enquiries);exit; ?>
                            <td><?php echo h($enquirie['Enquiry']['id']); ?>&nbsp;</td>

                            <td><input type="checkbox" class="checkbox1" value="<?= $enquirie['Enquiry']['id'] ?>" ></td>

                            <td>
                                <?php echo $this->Html->link(__(ucfirst($enquirie['User']['first_name'] . " " . $enquirie['User']['last_name'])), array('controller' => 'users', 'action' => 'view', $enquirie['User']['id']), array('title' => 'View')); ?>
                                <?php echo $this->Html->link(__('view'), array('action' => 'view', $enquirie['Enquiry']['id']), array('class' => 'btn btn-outline btn-sm btn-warning dim', 'title' => 'View')); ?>
                                <?php echo $this->Html->link(__('Update'), array('action' => 'edit_lead', $enquirie['Enquiry']['id']),
                                        array('class' => 'btn btn-outline btn-sm btn-info dim', 'title' => 'Update Lead')); ?>

                                <?php
                                  // if (AuthComponent::user('role') == 1 || AuthComponent::user('role') == 12) {
                                  // if ($enquirie['Enquiry']['is_active'] == 1) {
                                  // echo $this->Html->link(__('Deactive'), array('action' => 'deactive', $enquirie['Enquiry']['id']), array('class' => 'btn btn-outline btn-sm btn-info', 'title' => 'DeActive'));
                                  // } else {
                                  // echo $this->Html->link(__('Active'), array('action' => 'active', $enquirie['Enquiry']['id']), array('class' => 'btn btn-outline btn-sm btn-info', 'title' => 'Active'));
                                  // }
                                  // }
                                  ?>

                                  <?php
                                  // if (AuthComponent::user('role') == 1 || AuthComponent::user('role') == 12) {
                                  // if ($enquirie['Enquiry']['is_verified'] == 1) {
                                  // echo $this->Html->link(__('DeVerified'), array('action' => 'deverified', $enquirie['Enquiry']['id']), array('class' => 'btn btn-outline btn-sm btn-warning', 'title' => 'DeVerified'));
                                  // } else {
                                  // echo $this->Html->link(__('Verified'), array('action' => 'verified', $enquirie['Enquiry']['id']), array('class' => 'btn btn-outline btn-sm btn-warning', 'title' => 'Verified'));
                                  // }
                                  // }
                                  ?>
                                <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                                    <?php echo $this->Form->postLink(__('<i class="fa fa-trash"></i>'), array('action' => 'delete', $enquirie['Enquiry']['id']),
                                            array('class' => 'btn btn-outline btn-sm btn-danger dim', 'title' => 'Delete','escape'=>false), __('Are you sure you want to delete # %s?', $enquirie['Enquiry']['id'])); ?>
                                <?php } ?>
                            </td>
                            <td><?php echo h($enquirie['Enquiry']['ref_page']); ?>&nbsp;</td>
                            <td><?php echo h($enquirie['User']['email']); ?>&nbsp;</td>

                            <td><?php echo h($enquirie['Product']['product_name']); ?>&nbsp;</td>
                            <td><?php echo h($enquirie['Product']['publisher_name']); ?>&nbsp;</td>
                            <!-- Rting and Status Column added VG-7/03/2017 Start -->
                              <td>
                                <?php
                                    if (isset($enquirie['Enquiry']['status']))
                                    {
                                        if ($enquirie['Enquiry']['status'] == 0) {
                                          echo "No Status";
                                        }
                                        elseif ($enquirie['Enquiry']['status'] == 1) {
                                          echo "Not Responding";
                                        }
                                        elseif ($enquirie['Enquiry']['status'] == 2) {
                                          echo"Not Contacted";
                                        }
                                        elseif ($enquirie['Enquiry']['status'] == 3) {
                                          echo "Sample Sent";
                                        }
                                        elseif ($enquirie['Enquiry']['status'] == 4) {
                                          echo "Requirement Matched";
                                        }
                                        elseif ($enquirie['Enquiry']['status'] == 5) {
                                          echo "Negotiation";
                                        }
                                        elseif ($enquirie['Enquiry']['status'] == 6) {
                                          echo "Closed Won";
                                        }
                                        elseif ($enquirie['Enquiry']['status'] == 7) {
                                          echo "Closed Lost";
                                        }
                                        elseif ($enquirie['Enquiry']['status'] == 8) {
                                          echo "Waiting For Approval";
                                        }
                                        elseif ($enquirie['Enquiry']['status'] == 9) {
                                          echo "Email Requested";
                                        }
                                        elseif ($enquirie['Enquiry']['status'] == 10) {
                                          echo "Follow-up done";
                                        }
                                    }
                                    else {
                                      echo "Not Set";
                                    }
                                  echo $this->Form->select('status', $status,array('onchange'=>'$.get("'.$this->webroot.'enquiries/changestatus?value='.'"+$(this).val()+"&id="+"'.$enquirie['Enquiry']['id'].'",function(){location.reload();});','id'=>'stat'));
                                ?>
                              </td>
                              <td>
                                <?php
                                  if (isset($enquirie['Enquiry']['rating'])) {
                                      if ($enquirie['Enquiry']['rating'] == 0) {
                                        echo "No Rating";
                                      }
                                      elseif($enquirie['Enquiry']['rating'] == 1) {
                                        echo "Hot";
                                      }
                                      elseif($enquirie['Enquiry']['rating'] == 2) {
                                        echo "Warm";
                                      }
                                      elseif ($enquirie['Enquiry']['rating'] == 3) {
                                        echo "Cold";
                                      }
                                      elseif ($enquirie['Enquiry']['rating'] == 4) {
                                        echo "Junk";
                                      }
                                      else {
                                        echo "Not set";
                                      }
                                  }
                                  else {
                                    echo "Not Set";
                                  }
                                  echo $this->Form->select('rating', $rating,array('onchange'=>'$.get("'.$this->webroot.'enquiries/changerating?value='.'"+$(this).val()+"&id="+"'.$enquirie['Enquiry']['id'].'",function(){location.reload();});','id'=>'rate'));
                                ?>
                              </td>
                            <!-- Rting and Status Column added VG-7/03/2017 End -->
                            <td><?php echo h($enquirie['Enquiry']['message']); ?>&nbsp;</td>
                            <td>
                                <?php
                                if (!empty($enquirie['Enquiry']['remark'])) {
                                    $remarks = json_decode($enquirie['Enquiry']['remark']);
                                    $count = count($remarks);
                                    echo $remarks[$count - 1]->message;
                                    /* foreach ($remarks as $remark) {
                                      echo "$remark->message<hr/>";
                                      } */
                                }

                                ?>
                            </td>
                            <td><?php echo h($enquirie['User']['country']); ?>&nbsp;</td>
                            <td><?php echo h($enquirie['User']['organisation']); ?>&nbsp;</td>
                            <td><?php echo h($enquirie['User']['job_title']); ?>&nbsp;</td>
                            <td><?php echo h(date('d-M-Y h:i:s a', strtotime($enquirie['Enquiry']['created']))); ?>&nbsp;</td>

                            </tbody>
                        <?php } ?>
                    </table>
                    <div class="row col-md-12">
                        <div class="dataTables_paginate paging_bootstrap">
                            <p>
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?> </p>
                            <ul class="pagination" style="visibility: visible;">
                                <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
                                <li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
                                <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
