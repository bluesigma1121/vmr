<style type="text/css">
    .mrtp_enq{
        padding: 5px;
    }
    .margin-bottom-40{
        margin-top: 15px;
    }
    .reg-header {
        border-bottom: none;
    }
    .mar_top_hed{
        margin-top: -40px;
        color: #555;
        text-align: center;
        border-bottom: solid 1px #eee;
    }
    .paypal-logo{
      margin-right: -60px;
      z-index: 99;
    }
    .logo-margin{
      margin-right: -40px;
    }
    .payment-logo{
      margin-top: -10px;
    }
    @media (max-width: 480px) and (min-width: 320px){
      .input-group-textarea{
        height: 66px;
      }
      .input-group-textarea .input-group-addon{
        padding: 24px 8px 24px 8px;
      }
    }

    .recent-rpt h1 {
      padding-left:10px;
      display:flex;
    }
</style>

<script type="text/javascript">

 /* Ciode Start VG-6-1-2017*/
    var options = {
        validateOn: 'keydown',
        position:{x:'right', y:'top'},
    };
/* Code END VG-6-1-2017*/

    $(document).ready(function () {

        $('#enquiry_form').bValidator(options);

        /*Copy and Paste Prevention Code Start VG-3-1-2017*/
          $('#EnquiryEmail').bind('copy paste cut',function(e) {
         e.preventDefault();
         });

        $('#EnquiryConfirmEmail').bind('copy paste cut',function(e) {
         e.preventDefault();
         });
         /*Copy and Paste Prevention Code End VG-3-1-2017*/

    });
</script>

<?php if (empty($ex_ref_page)){?>
<!-- All form except buy now start here -->
  <section class="about-inner report-inner-bg enquiry-before-form-main">
    <div class="container">
      <div class="blog-inner report-page col-md-12">
        <div class="col-md-6 col-sm-6 recent-rpt oth-form-rpt-name hidden-lg hidden-md" style="display:none !important;">
          <i class="fa fa-line-chart"></i>
          <p>
            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'slug' => $pro_name['Product']['slug'])); ?> " target="_blank"> <?php echo $pro_name['Product']['alias'] ?></a>
          </p>
          
          <small>
            <?php echo $this->Text->truncate(strip_tags($pro_name['Product']['product_description']), 100, array('ellipsis' => '...','exact' => false) );?>
          </small>
        </div>
        <div class="col-md-6 col-sm-6 text-center">
          <!-- <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($cat_main), 'id' => $pro_name['Product']['id'], 'slug' => $pro_name['Product']['slug'])); ?> " target="_blank"> <?php echo $pro_name['Product']['product_name'] ?></a> -->
          
          <div class="enquiry-before-form buy-now-form oth-form">
            <?php  echo $this->Form->create('Enquiry', array('id' => 'enquiry_form', 'class' => 'reg-page', 'type' => 'file')); ?>
              
              <?php if(!empty($related_category_name[0]['Category']['id'])) { ?>
                <div class="row ">
                <h6 class="text-center">Please fill out the form. We will contact you within 24 hours.</h6>
                  <div class="col-md-12">
                    <p class="text-center">
                      <span class="form-header">
                        <?php if($ref_page == "COVID-19 Impact") { ?>
                          REQUEST FOR COVID19 IMPACT ANALYSIS
                        <?php } else { ?>
                          <?php echo $ref_page ?>&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php } ?>
                      </span>
                    </p>
                  </div>
                  <div class="col-md-12 text-right">
                    <div class="enq-btn">
                      <a class="form-btn" target="_blank" href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($related_category_name[0]['Category']['slug']))); ?>">View Related Reports</a>
                    </div>
                  </div>
                </div>

              <?php } else {?>
                <div class="row">
                <h6 class="text-center">Please fill out the form. We will contact you within 24 hours.</h6>
                  <div class="col-md-12">
                    <!-- <p class="text-center"><span class="form-header"><?php echo $ref_page ?>&nbsp;&nbsp;&nbsp;&nbsp;</span></p> -->
                    <p class="text-center">
                      <span class="form-header">
                        <?php if($ref_page == "COVID-19 Impact") { ?>
                          REQUEST FOR COVID19 IMPACT ANALYSIS
                        <?php } else { ?>
                          <?php echo $ref_page ?>&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php } ?>
                      </span>
                    </p>
                  </div>
                  <div class="col-md-12 text-right">
                    <div class="enq-btn">
                        
                      <?php if(!empty($related_category_name['Category']['id'])){?><a class="form-btn" target="_blank" href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($related_category_name['Category']['slug']))); ?>">View Related Reports
                      </a> <?php } ?>
                    </div>
                  </div>
                </div>

              <?php } ?>
              <!-- code ends here-VG-06/08/2016 -->
              <hr>
            <!-- <div class="row form-group"> -->
              <fieldset>

                <div class="col-md-12 col-xs-12 col-sm-12 text-left">
                  <div class="col-md-4">
                    <label>Full Name <span>*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-group ">
                      <span class="input-group-addon"><i class="fa fa-user"></i></span>
                      <?php echo $this->Form->input('first_name', array('class' => 'form-control margin-bottom-20', 'label' => false,
                      'type' => 'text', 'value' => AuthComponent::user('first_name'), 'placeholder' => 'Full Name',
                      'data-bvalidator' => 'required,alpha','autocomplete'=>'off')); ?>
                    </div>
                  </div>
                </div>
              </fieldset>

              <!--<fieldset>
                <div class="col-md-12 col-xs-12 col-sm-12 text-left">
                  <div class="col-md-4">
                    <label>Last Name <span>*</span></label>
                  </div>    
                  <div class="col-md-8">
                    <div class="input-group ">
                      <span class="input-group-addon"><i class="fa fa-user"></i></span>
                      <?php echo $this->Form->input('last_name', array('class' => 'form-control margin-bottom-20', 'label' => false,
                      'type' => 'text', 'value' => AuthComponent::user('last_name'), 'placeholder' => 'Last Name',
                      'data-bvalidator' => 'required,alpha','autocomplete'=>'off')); ?>
                    </div>
                  </div>
                </div>
              </fieldset>-->

              <fieldset>
                <div class="col-md-12 col-xs-12 col-sm-12 text-left">
                  <div class="col-md-4">
                    <label>Organisation <span>*</span></label>
                  </div>    
                  <div class="col-md-8">
                    <div class="input-group ">
                      <span class="input-group-addon"><i class="fa fa-users"></i></span>
                      <?php echo $this->Form->input('organisation', array('class' => 'form-control margin-bottom-20', 'label' => false,
                      'type' => 'text', 'value' => AuthComponent::user('organisation'), 'placeholder' => 'Organisation',
                      'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <div class="col-md-12 col-xs-12 col-sm-12 text-left">
                  <div class="col-md-4">
                    <label>Designation <span>*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-group ">
                      <span class="input-group-addon"><i class="fa fa-address-card"></i></span>
                      <?php echo $this->Form->input('job_title', array('class' => 'form-control margin-bottom-20', 'label' => false,
                      'type' => 'text', 'value' => AuthComponent::user('designation'), 'placeholder' => 'Designation',
                      'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                    </div>
                  </div>
                </div>
              </fieldset>

              <fieldset>
                <div class="col-md-12 col-xs-12 col-sm-12 text-left">
                  <div class="col-md-4">
                    <label>Mobile <span>*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-group ">
                      <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                      <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false,
                      'type' => 'text', 'placeholder' => 'Mobile', 'id' => 'customer_phone_test', 'value' => AuthComponent::user('mobile'),
                      'size' => '20', 'maxlength' => '25', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <?php if (!AuthComponent::user()): ?>
                    <div class="col-md-12 col-xs-12 col-sm-12 text-left">
                      <div class="col-md-4">
                        <label>Corporate Email <span>*</span></label>
                      </div>
                      <div class="col-md-8">
                        <div class="input-group ">
                          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        
                          <?php echo $this->Form->input('email', array('class' => 'form-control margin-bottom-20', 'label' => false,
                          'type' => 'text', 'placeholder' => 'Corporate Email', 'value' => AuthComponent::user('email'),'data-bvalidator' => 'required,email','autocomplete'=>'off')); ?>
                        </div>
                      </div>
                    </div>
                    </fieldset>
                    <fieldset>    
                    <div class="col-md-12 col-xs-12 col-sm-12 text-left">
                      <div class="col-md-4">
                        <label>Confirm Email <span>*</span></label>
                      </div>
                      <div class="col-md-8">
                        <div class="input-group ">
                          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                          <?php echo $this->Form->input('Confirm_email', array('class' => 'form-control margin-bottom-20', 'label' => false,
                          'type' => 'text', 'placeholder' => 'Confirm Email', 'value' => AuthComponent::user('Confirm_email'),'data-bvalidator' => 'required,email,equalto[EnquiryEmail]','autocomplete'=>'off')); ?>
                        </div>
                      </div>
                    </div>
                <?php endif; ?>
              </fieldset>

              <fieldset>

                <div class="col-md-12 col-xs-12 col-sm-12 text-left">
                  <div class="col-md-4">
                    <label>Country <span>*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-group ">
                      <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                      <?php echo $this->Form->input('country', array('class' => 'form-control margin-bottom-20', 'label' => false,
                          'type' => 'text', 'value' => AuthComponent::user('country'), 'placeholder' => 'Country',
                          'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                    </div>
                  </div>
                </div>
              </fieldset>

              <fieldset>
                <div class="col-md-12 col-xs-12 col-sm-12 text-left">
                  <div class="col-md-4">
                    <label><?php if($ref_page == "Request Customization"){echo 'Requirement'; }else if($ref_page == "Ask Questions"){ echo 'Question';}else{ echo 'Any Specific Requirement'; } ?></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-group input-group-textarea">
                      <span class="input-group-addon"><i class="fa fa-info-circle"></i></span>
                      <?php echo $this->Form->input('message', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'textarea', 'placeholder' => 'Specific Requirement from this report.', 'autocomplete'=>'off','rows' => 3,'style' => 'resize:both')); ?>
                    </div>
                      <span class="text-danger"><small>URL not allowed in message.</small></span>
                  </div>
                </div>
              </fieldset>

          <!--Change Captcha VG-26/2/2018 -->
              <div class="margin-bottom-20" style="overflow:hidden">
                  <div class="col-md-12">
                    <div class="g-recaptcha" data-sitekey="6LfRUFIUAAAAAAr70oYLT9gPRZV0jVD8jZOTataM"></div>
                    <!--test <div class="g-recaptcha" data-sitekey="6LdfMVUUAAAAAG_2D6qGpWUDcFNjWIuluiDrubqk" data-bvalidator='required'></div> -->
                  </div>
              </div>
          <!--Change Captcha VG-26/2/2018 -->            

              <br><br>
              <fieldset>
                  <div class="col-md-12 col-xs-12 col-sm-12">
                      <?php echo $this->Form->submit('Submit', array( 'id' =>'contact-submit')) ?>
                  </div>
              </fieldset>

            <?php echo $this->Form->end(); ?>
          </div>
        </div>


        <div class="col-md-6 col-sm-7 recent-rpt oth-form-rpt-name hidden-xs hidden-sm">
          <i class="fa fa-line-chart"></i>
          <h1>
            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'slug' => $pro_name['Product']['slug'])); ?> " target="_blank"> <?php echo $pro_name['Product']['alias'] ?></a>
          </h1>
          
          <small>
            <?php echo $this->Text->truncate(strip_tags($pro_name['Product']['product_description']), 150, array('ellipsis' => '...','exact' => false) );?>
          </small>

          <?php if($ref_page == "COVID-19 Impact") { ?>
            <!-- <div class="col-md-12">
              <div class="why-us-donl-smpl-main"> -->
                <div class="why-us-donl-smpl-main">
                  <h4>Important : Covid-19 Pandemic Market Impact</h4>
                  <!-- <div class="why-us-content"> -->
                    <small>The world is currently experiencing a huge challenge which has affected all the industries and markets. Few markets have experienced a high growth in their demand while others have faced a downfall. The extent of growth or drop in the market statistics of different industries by COVID 19 plays an important role in how the future of businesses will shape up in the near future. We at Value Market Research provide you with an impact analysis of COVID 19 on this market with its latest update in the purchased version of the report.</small>
                  <!-- </div> -->
                </div>
              <!-- </div>
            </div>        -->
          <?php } ?>
        
            
        <div class="user">
          
          <div class="why-us-donl-smpl-main col-md-8 col-md-push-2 col-xs-12 padd-0 hidden-xs">
            <div class="btns chooseopt">
              <h4>Why Choose Us</h4>
              <div class="why-us-content">
                
                <?php foreach ($whyus as $key => $why) { ?>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                      <div class="why-us-donl-smpl">
                        <?php switch ($key) {
                          case 0:
                            echo '<i class="fa fa-user" aria-hidden="true"></i>';
                            break;
                          case 1:
                            echo '<i class="fa fa-certificate" aria-hidden="true"></i>';
                            break;
                          case 2:
                            echo '<i class="fa fa-handshake-o" aria-hidden="true"></i>';
                            break;
                          case 3:
                            echo '<i class="fa fa-envelope" aria-hidden="true"></i>';
                            break;
                        }?>
                        <h5><?=$why['Whyus']['title']?></h5>
                     </div>
                    </div>
                <?php } ?>
                
            
              </div>
            </div>
          </div>    
        </div>
  
  
      </div>
    </div>
  </section>


<!-- All form except buy now end here -->
<?php }else{ ?>
<!-- buy now start here -->
  <section class="about-inner report-inner-bg enquiry-before-form-main">
    <div class="container">
      <div class="blog-inner report-page col-md-12">
        <div class="col-md-12 text-center">
          <h1><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'slug' => $pro_name['Product']['slug'])); ?> " target="_blank"> <?php echo $pro_name['Product']['alias'] ?></a></h1>
          <div class="enquiry-before-form buy-now-form">
            <?php  echo $this->Form->create('Enquiry', array('id' => 'enquiry_form', 'class' => 'reg-page', 'type' => 'file')); ?>
              <?php if(!empty($related_category_name[0]['Category']['id'])) { ?>
                <div class="row ">
                  <div class="col-md-6">
                    <p class="text-right"><span><?php echo $ref_page ?>&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
                  </div>
                  <div class="col-md-6 text-left">
                    <div class="enq-btn">
                      <a class="form-btn" target="_blank" href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($related_category_name[0]['Category']['slug']))); ?>">View Related Reports</a>
                    </div>
                  </div>
                </div>
              <?php } else {?>
                <div class="row">
                  <div class="col-md-6">
                    <p class="text-right"><span><?php echo $ref_page ?>&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
                  </div>
                  <div class="col-md-6 text-left">
                    <div class="enq-btn">
                      <?php if(!empty($related_category_name['Category']['id'])){?><a class="form-btn" target="_blank" href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($related_category_name['Category']['slug']))); ?>">View Related Reports</a> <?php } ?>
                    </div>
                  </div>
                </div>

              <?php } ?>
              <!-- code ends here-VG-06/08/2016 -->
              <hr>


              <input type="hidden" name="pro_name" value="<?php echo $pro_name['Product']['product_name'];?>">
              <fieldset>
                <div class="col-md-2 col-xs-12 col-sm-2 text-left"><label>Select License Type : <span>*</span></label></div>
                <div class="col-md-10 col-xs-12 col-sm-10 text-left licence-type">
                    <?php foreach($licence_type as $key => $value){ ?>
                        <label class="radio"><?= $value ?>
                          <input type="radio" name="data[Enquiry][licence_type]" id="EnquiryLicenceType<?=$key?>" data-bvalidator="required" value="<?=$key?>" >
                          <span class="checkround"></span>
                        </label>&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php } ?>
                    <!--<?php
                    $options = $licence_type;
                    $attributes = array(
                        'legend' => false, 'separator' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                        'data-bvalidator' => 'required', 'value' => 1);
                    ?>
                    <?php echo $this->Form->radio('licence_type', $options, $attributes); ?>-->
                </div>
              </fieldset>

              <fieldset>
                  <div class="col-md-2 col-xs-12 col-sm-2">
                    <div class="form-group text-left">
                      <label>Title <span>*</span></label>
                      <?php echo $this->Form->input('title', array('class' => 'form-control', 'type' => 'select',
                      'options' => $title, 'placeholder' => 'Title', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                  </div>

                <div class="col-md-5 col-xs-12 col-sm-5 text-left">
                  <label>First Name <span>*</span></label>
                  <?php echo $this->Form->input('first_name', array('class' => 'form-control margin-bottom-20', 'label' => false,
                  'type' => 'text', 'value' => AuthComponent::user('first_name'), 'placeholder' => 'First Name',
                  'data-bvalidator' => 'required,alpha','autocomplete'=>'off')); ?>
                </div>
                <div class="col-md-5 col-xs-12 col-sm-5 text-left">
                    <label>Last Name <span>*</span></label>
                    <?php echo $this->Form->input('last_name', array('class' => 'form-control margin-bottom-20', 'label' => false,
                    'type' => 'text', 'value' => AuthComponent::user('last_name'), 'placeholder' => 'Last Name',
                    'data-bvalidator' => 'required,alpha','autocomplete'=>'off')); ?>
                </div>
              </fieldset>
              <!-- </div> -->


              <fieldset>
                <!-- <div class="col-sm-6"> -->
                  <div class="col-md-6 col-xs-12 col-sm-5 text-left">
                    <label>Organisation <span>*</span></label>
                    <?php echo $this->Form->input('organisation', array('class' => 'form-control margin-bottom-20', 'label' => false,
                    'type' => 'text', 'value' => AuthComponent::user('organisation'), 'placeholder' => 'Organisation',
                    'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                  </div>

                  <div class="col-md-6 col-xs-12 col-sm-5 text-left">
                    <label>Designation <span>*</span></label>
                    <?php echo $this->Form->input('job_title', array('class' => 'form-control margin-bottom-20', 'label' => false,
                    'type' => 'text', 'value' => AuthComponent::user('designation'), 'placeholder' => 'Designation',
                    'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                  </div>
                <!-- </div> -->
              </fieldset>

              <fieldset>
              <!-- <div class="row form-group"> -->
                <div class="col-md-4 col-xs-12 col-sm-4 text-left">
                    <label>Mobile <span>*</span></label>
                    <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false,
                    'type' => 'text', 'placeholder' => 'Mobile', 'id' => 'customer_phone_test', 'value' => AuthComponent::user('mobile'),
                    'size' => '20', 'maxlength' => '25', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                </div>

                <?php //if (!AuthComponent::user()): ?>
                  <!-- <div class="row form-group"> -->
                    <div class="col-md-4 col-xs-12 col-sm-4 text-left">
                        <label>Corporate Email <span>*</span></label>
                        <?php echo $this->Form->input('email', array('class' => 'form-control margin-bottom-20', 'label' => false,
                        'type' => 'text', 'placeholder' => 'Corporate Email', 'value' => AuthComponent::user('email'),'data-bvalidator' => 'required,email','autocomplete'=>'off')); ?>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-4 text-left">
                        <label>Confirm Email <span>*</span></label>
                        <?php echo $this->Form->input('Confirm_email', array('class' => 'form-control margin-bottom-20', 'label' => false,
                        'type' => 'text', 'placeholder' => 'Confirm Email', 'value' => AuthComponent::user('Confirm_email'),'data-bvalidator' => 'required,email,equalto[EnquiryEmail]','autocomplete'=>'off')); ?>
                    </div>
                  <!-- </div> -->
                <?php //endif; ?>

              <!-- </div> -->
              </fieldset>

              <fieldset>
                <div class="col-md-12 col-xs-12 col-sm-12 text-left">
                    <label>Address <span>*</span></label>
                    <?php echo $this->Form->input('address', array('class' => 'form-control margin-bottom-20', 'label' => false,
                    'type' => 'text', 'value' => AuthComponent::user('address'), 'placeholder' => 'Address',
                    'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                </div>

              </fieldset>

              <fieldset>
                <div class="col-md-6 col-xs-12 col-sm-6 text-left">
                    <label>City <span>*</span></label>
                    <?php echo $this->Form->input('city', array('class' => 'form-control margin-bottom-20', 'label' => false,
                        'type' => 'text', 'value' => AuthComponent::user('city'), 'placeholder' => 'City', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                </div>
                <div class="col-md-6 col-xs-12 col-sm-6 text-left">
                    <label>State <span>*</span></label>
                    <?php echo $this->Form->input('state', array('class' => 'form-control margin-bottom-20', 'label' => false,
                        'type' => 'text', 'value' => AuthComponent::user('state'), 'placeholder' => 'State', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                </div>

                <div class="col-md-6 col-xs-12 col-sm-6 text-left">
                  <label>Country <span>*</span></label>
                  <?php echo $this->Form->input('country', array('class' => 'form-control margin-bottom-20', 'label' => false,
                      'type' => 'text', 'value' => AuthComponent::user('country'), 'placeholder' => 'Country',
                      'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                </div>
                <div class="col-md-6 col-xs-12 col-sm-6 text-left">
                    <label>Zip code <span>*</span></label>
                    <?php echo $this->Form->input('pin_code', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'zipcode', 'value' => AuthComponent::user('pin_code'), 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                </div>
              </fieldset>

          <!--Change Captcha VG-26/2/2018 -->
              <div class="margin-bottom-20" style="overflow:hidden">
                  <div class="col-md-7 col-md-offset-0">
                      <div class="g-recaptcha" data-sitekey="6LfRUFIUAAAAAAr70oYLT9gPRZV0jVD8jZOTataM"></div>
                      <!--test <div class="g-recaptcha" data-sitekey="6LekeEYUAAAAAHPe-rEZtnpqMLcQ_TtFdWSsYsP3" data-bvalidator='required'></div> -->
                  </div>
              </div>
          <!--Change Captcha VG-26/2/2018 -->            

          <!-- Selecting payment gateway -->
              <?php if (isset($ex_ref_page) && ($ex_ref_page == 'buy-now' || $ex_ref_page == 'pre-buy-now')){ ?>
                <br/>
                <fieldset>
                   <!-- <div class="col-md-3 col-xs-6 col-sm-3 paypal-img text-left">
                      <label>
                          <input type="radio" class="radio-inline" name="payment_option" value="paypal">
                          <?php //echo $this->Html->image('logos/paypal.png', array('alt' => 'Paypal')); ?>
                      </label>
                    </div> -->
                    <!--<div class="col-md-4 col-xs-6 col-sm-4 credit-cards-img text-left">
                      <label>
                          <input type="radio" name="payment_option" value="ccavenue" checked>
                          <?php //echo $this->Html->image('logos/visa_mastercart_amex.png', array('alt' => 'credit-cards')); ?>
                      </label>
                    </div>-->
                    <div class="col-md-4 col-xs-6 col-sm-4 credit-cards-img text-left">
                      <label>
                          <input type="radio" name="payment_option" value="razorpay" checked>
                          <?php echo $this->Html->image('logos/visa_mastercart_amex.png', array('alt' => 'credit-cards')); ?>
                      </label>                      
                    </div>
                </fieldset>
                <br/>
              <?php } ?>
          <!-- *************************** -->
              <?php if (isset($ex_ref_page) && ($ex_ref_page == 'buy-now' || $ex_ref_page == 'pre-buy-now')): ?>
                <fieldset>
                  <div class="col-md-12 col-xs-12 col-sm-12 check text-left">
                    <p>
                      <?php echo $this->Form->checkbox('term', array('label' => false, 'data-bvalidator' => 'required', 'onclick' => 'openModal()')); ?>
                      <input type="hidden" id="termsandcondition" value="<?php echo Router::url(array('controller' => 'users', 'action' => 'accept_terms_conditions')); ?>">

                      I have read <a  href="<?php echo Router::url(array('controller' => 'users', 'action' => 'terms_conditions')); ?>"  target="_blank" class="color-green">Terms and Conditions</a> and I accept it.
                      <br>* Due to the nature of goods, orders once placed can't be cancelled.
                    </p>
                  </div>
                </fieldset>
              <?php endif; ?>
              <br>
              <fieldset>
                  <div class="col-md-12 col-xs-12 col-sm-12">
                      <?php echo $this->Form->submit('Next', array( 'id' =>'contact-submit')) ?>
                  </div>
              </fieldset>

            <?php echo $this->Form->end(); ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- buy now End here -->
<?php } ?>


<div class="modal  fade" id="viewTermsAndCodition" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Terms And Conditions</h4>
      </div>
      <div class="modal-body">
      </div>
    </div>
    
  </div>
</div>

<script>
$(document).ready(function(e){
  var buyoption = localStorage.getItem('buyoption');
  console.log(buyoption);
  if(buyoption == null){
    $('#EnquiryLicenceType1').prop('checked', true);
  }else{
    localStorage.removeItem('buyoption');
    $('#EnquiryLicenceType'+buyoption).prop('checked', true);
  }
  
});

</script>