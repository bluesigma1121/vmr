
<div class="breadcrumbs border_tp">
    <div class="container">
        <h1 class="pull-left">Account</h1>
        <ul class="pull-right breadcrumb">
            <span class="text_color_bread">Your Are Here :</span>
            <li><a href="<?php echo Router::url('/') ?>">Home</a></li>
            <li><a href="#">Your Account</a></li>
        </ul>
    </div>
</div>
<div class="container content">
    <div class="row">
        <div class="col-md-9">
            <!-- Tabs -->
            <div class="tab-v2 margin-bottom-60">
                <?php echo $this->element('my_account_panel'); ?>
                <div class="tab-content">
                    <div id="enquiries" class="inside">
                        <p>
                            Please find below your inquiry history with valuemarketresearch.com.
                        </p><hr>
                        <?php if (!empty($user_enquirys)) { ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th> Date </th>
                                        <th>Subject </th>
                                        <th>Product Name </th>
                                        <th>Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $cnt = 0;
                                    foreach ($user_enquirys as $key => $inquery) {
                                        ?>
                                        <tr>
                                            <td><?php echo++$cnt; ?></td>
                                            <td><?php echo Date('d-M-Y h:i a', strtotime($inquery['Enquiry']['created'])); ?></td>
                                            <td><?php echo $inquery['Enquiry']['subject']; ?></td>
                                            <td><?php echo $inquery['Product']['product_name']; ?></td>
                                            <?php /*
                                            <td><?php echo $yes_no[$inquery['Enquiry']['is_active']]; ?></td>
                                            <td><?php echo $yes_no[$inquery['Enquiry']['is_verified']]; ?></td>
                                             *
                                             */ ?>
                                            <td> <a href="<?php echo Router::url(array('controller' => 'enquiries', 'action' => 'user_enquiry_view', $inquery['Enquiry']['id'])); ?>" class="btn btn-info btn-xs view_enquiry_user"><i class="fa fa-eye"></i> view</a></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                            <hr>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        </div>
    </div>
</div>
