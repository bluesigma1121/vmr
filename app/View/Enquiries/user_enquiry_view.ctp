<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>valuemarketresearch.com</title>
        <!-- Bootstrap core CSS -->
        <?php echo $this->Html->css(array('front/bootstrap/css/bootstrap.min.css', 'front/bvalidator.css')); ?>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->


        <style type="text/css">
            .mar_top_hed{
                margin-top: -40px;
                color: #555;
                text-align: center;
                margin-bottom: 5px;
                border-bottom: solid 1px #eee;

            }
            .inver{
                color: red !important
            }
            .ver{
                color: green !important
            }
        </style>

        <?= $this->Html->script(array('front/plugins/jquery/jquery.min.js',  'front/plugins/bootstrap/js/bootstrap.min.js', 'front/jquery.bvalidator-yc.js')); ?>
    </head>
    <body>
        <div class="container content">
            <div class="row">
                <!-- Begin Sidebar Menu -->
                <div class="col-md-12">
                    <div class="funny-boxes funny-boxes-top-sea">
                        <div class="row">
                            <div class="col-md-3 funny-boxes-img">
                                <?php echo ucfirst($enquery['User']['name']); ?>
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-briefcase"></i>  <?php echo ucfirst($enquery['User']['organisation']); ?></li>
                                    <li><i class="fa fa-map-marker"></i> <?php echo ucfirst($enquery['User']['city']); ?> <?php echo ucfirst($enquery['User']['country']); ?></li>
                                </ul>
                            </div>
                            <div class="col-md-9">
                                <h2><?php echo ucfirst($enquery['Enquiry']['subject']); ?> </h2>
                                <p>
                                    <i class="fa fa-calendar"></i> <?php echo Date('d-M-Y h:i a', strtotime($enquery['Enquiry']['created'])); ?>  <?php if ($enquery['Enquiry']['is_active'] == 1) { ?>
                                        <i class="fa fa-check ver"></i> Active
                                    <?php } else { ?>
                                        <i class="fa fa-times inver"></i> InActive
                                    <?php }
                                    ?>
                                    <?php if ($enquery['Enquiry']['is_verified'] == 1) { ?>
                                        <i class="fa fa-check ver"></i> Verified
                                    <?php } else { ?>
                                        <i class="fa fa-times inver"></i> InVerified
                                    <?php } ?>
                                </p>
                                <hr>
                                <strong>Category Name : </strong> <?php echo $enquery['Category']['category_name'] ?><br>
                                <strong>Product Name : </strong> <?php echo $enquery['Product']['product_name'] ?>
                                <hr>
                                <p>
                                    <?php echo $enquery['Enquiry']['message'] ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
