<style type="text/css">
    .mrtp_enq{
        padding: 5px;
    }
    .margin-bottom-40{
        margin-top: 15px;
    }
    .reg-header {
        border-bottom: none;
    }
    .mar_top_hed{
        margin-top: -40px;
        color: #555;
        text-align: center;
        border-bottom: solid 1px #eee;
    }
    .paypal-logo{
      margin-right: -60px;
      z-index: 99;
    }
    .logo-margin{
      margin-right: -40px;
    }
    .payment-logo{
      margin-top: -10px;
    }
</style>

<script type="text/javascript">

 /* Ciode Start VG-6-1-2017*/
    var options = {
        validateOn: 'keydown',
        position:{x:'center', y:'top'},
    };
/* Code END VG-6-1-2017*/

    $(document).ready(function () {

        $('#enquiry_form').bValidator(options);

        /*Copy and Paste Prevention Code Start VG-3-1-2017*/
          $('#EnquiryEmail').bind('copy paste cut',function(e) {
         e.preventDefault();
         });

        $('#EnquiryConfirmEmail').bind('copy paste cut',function(e) {
         e.preventDefault();
         });
         /*Copy and Paste Prevention Code End VG-3-1-2017*/

    });
</script>
<section class="about-inner report-inner-bg enquiry-before-form-main">
  <div class="container">
    <div class="blog-inner report-page col-md-12">
        <!-- <h4>Reports Title</h4> -->
      <!--<div class="col-md-10 col-md-push-1 text-center">-->
      <div class="col-md-12 text-center">
             <!-- code starts here-VG-06/08/2016 -->

            <?php //echo "<pre>"; print_r($related_category_name['Category']['id']);exit; //echo $this->Form->hidden('cat_main', ['value'=>$cat_main]);?>

        <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($cat_main), 'slug' => $pro_name['Product']['slug'])); ?> " target="_blank"> <?php echo $pro_name['Product']['product_name'] ?></a>
        <div class="enquiry-before-form buy-now-form">
          <?php  echo $this->Form->create('Enquiry', array('id' => 'enquiry_form', 'class' => 'reg-page', 'type' => 'file')); ?>

            <?php if(!empty($related_category_name[0]['Category']['id'])) { ?>
              <div class="row ">

                <div class="col-md-6">
                  <p class="text-right"><span><?php echo $ref_page ?>&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
                </div>
                <div class="col-md-6 text-left">
                  <div class="enq-btn">
                    <a class="form-btn" target="_blank" href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($related_category_name[0]['Category']['cat_slug']))); ?>">View Related Reports</a>
                  </div>
                </div>
              </div>

            <?php } else {?>
              <div class="row">

                <div class="col-md-6">
                  <p class="text-right"><span><?php echo $ref_page ?>&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
                </div>
                <div class="col-md-6 text-left">
                  <div class="enq-btn">
                      
                    <?php if(!empty($related_category_name['Category']['id'])){?><a class="form-btn" target="_blank" href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'slug' => $this->Link->cleanString($related_category_name['Category']['category_name']))); ?>">View Related Reports
                    </a> <?php } ?>
                  </div>
                </div>
              </div>

            <?php } ?>
            <!-- code ends here-VG-06/08/2016 -->
            <hr>

          <!-- <div class="row form-group"> -->
            <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'): ?>

              <input type="hidden" name="pro_name" value="<?php echo $pro_name['Product']['product_name'];?>">
              <!-- <div class="col-md-2 text-left">
                  <label>Select License Type : <span>*</span></label>
              </div> -->
              <fieldset>
                <div class="col-md-3 col-xs-12 col-sm-3 text-left"><label>Select License Type : <span>*</span></label></div>
                <!-- <div class="col-md-4 col-xs-12 col-sm-4"><label><input type="radio" class="radio-inline" name="radios" value=""><span class="outside"><span class="inside"></span></span>Single User License: $ 3100.00</label></div>
                <div class="col-md-4 col-xs-12 col-sm-4"><label><input type="radio" class="radio-inline" name="radios" value=""><span class="outside"><span class="inside"></span></span>Multi User License: $ 6500.00</label></div> -->

                <div class="col-md-9 col-xs-12 col-sm-9 text-left">
                    <?php
                    $options = $licence_type;
                    $attributes = array(
                        'legend' => false, 'separator' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                        'data-bvalidator' => 'required', 'value' => 1);
                    ?>
                    <?php echo $this->Form->radio('licence_type', $options, $attributes); ?>
                </div>
                </fieldset>

              <?php endif; ?>
          <!-- </div> -->

          <!-- <div class="row form-group"> -->
            <fieldset>

              <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now') { ?>
                <div class="col-md-2 col-xs-12 col-sm-2">
                  <div class="form-group text-left">
                    <label>Title <span>*</span></label>
                    <?php echo $this->Form->input('title', array('class' => 'form-control', 'type' => 'select',
                    'options' => $title, 'placeholder' => 'Title', 'label' => false, 'data-bvalidator' => 'required')); ?>
                  </div>
                </div>
              <?php } ?>

              <div class="<?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now') { echo 'col-md-5 col-xs-12 col-sm-5 text-left'; }else{echo 'col-md-6 col-xs-12 col-sm-6 text-left'; }?>">
                <label>First Name <span>*</span></label>
                <?php echo $this->Form->input('first_name', array('class' => 'form-control margin-bottom-20', 'label' => false,
                'type' => 'text', 'value' => AuthComponent::user('first_name'), 'placeholder' => 'First Name',
                'data-bvalidator' => 'required,alpha','autocomplete'=>'off')); ?>
              </div>
              <div class="<?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now') { echo 'col-md-5 col-xs-12 col-sm-5 text-left'; }else{echo 'col-md-6 col-xs-12 col-sm-6 text-left'; }?>">
                  <label>Last Name <span>*</span></label>
                  <?php echo $this->Form->input('last_name', array('class' => 'form-control margin-bottom-20', 'label' => false,
                  'type' => 'text', 'value' => AuthComponent::user('last_name'), 'placeholder' => 'Last Name',
                  'data-bvalidator' => 'required,alpha','autocomplete'=>'off')); ?>
              </div>
            </fieldset>
            <!-- </div> -->


            <fieldset>
              <!-- <div class="col-sm-6"> -->
                <div class="col-md-6 col-xs-12 col-sm-5 text-left">
                  <label>Organisation <span>*</span></label>
                  <?php echo $this->Form->input('organisation', array('class' => 'form-control margin-bottom-20', 'label' => false,
                  'type' => 'text', 'value' => AuthComponent::user('organisation'), 'placeholder' => 'Organisation',
                  'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                </div>

                <div class="col-md-6 col-xs-12 col-sm-5 text-left">
                  <label>Designation <span>*</span></label>
                  <?php echo $this->Form->input('job_title', array('class' => 'form-control margin-bottom-20', 'label' => false,
                  'type' => 'text', 'value' => AuthComponent::user('designation'), 'placeholder' => 'Designation',
                  'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                </div>
              <!-- </div> -->
            </fieldset>

            <fieldset>
            <!-- <div class="row form-group"> -->
              <div class="col-md-4 col-xs-12 col-sm-2 text-left">
                  <label>Mobile <span>*</span></label>
                  <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false,
                  'type' => 'text', 'placeholder' => 'Mobile', 'id' => 'customer_phone_test', 'value' => AuthComponent::user('mobile'),
                  'size' => '20', 'maxlength' => '25', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
              </div>

              <?php if (!AuthComponent::user()): ?>
                <!-- <div class="row form-group"> -->
                  <div class="col-md-4 col-xs-12 col-sm-2 text-left">
                      <label>Corporate Email <span>*</span></label>
                      <?php echo $this->Form->input('email', array('class' => 'form-control margin-bottom-20', 'label' => false,
                      'type' => 'text', 'placeholder' => 'Corporate Email', 'value' => AuthComponent::user('email'),'data-bvalidator' => 'required,email','autocomplete'=>'off')); ?>
                  </div>
                  <div class="col-md-4 col-xs-12 col-sm-2 text-left">
                      <label>Confirm Email <span>*</span></label>
                      <?php echo $this->Form->input('Confirm_email', array('class' => 'form-control margin-bottom-20', 'label' => false,
                      'type' => 'text', 'placeholder' => 'Confirm Email', 'value' => AuthComponent::user('Confirm_email'),'data-bvalidator' => 'required,email,equalto[EnquiryEmail]','autocomplete'=>'off')); ?>
                  </div>
                <!-- </div> -->
              <?php endif; ?>

            <!-- </div> -->
            </fieldset>



            <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'){?>
              <fieldset>
                <div class="col-md-12 col-xs-12 col-sm-12 text-left">
                    <label>Address <span>*</span></label>
                    <?php echo $this->Form->input('address', array('class' => 'form-control margin-bottom-20', 'label' => false,
                    'type' => 'text', 'value' => AuthComponent::user('address'), 'placeholder' => 'Address',
                    'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                </div>

              </fieldset>
            <?php } ?>

            <fieldset>
              <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'){ ?>
                <div class="col-md-6 col-xs-12 col-sm-6 text-left">
                    <label>City <span>*</span></label>
                    <?php echo $this->Form->input('city', array('class' => 'form-control margin-bottom-20', 'label' => false,
                        'type' => 'text', 'value' => AuthComponent::user('city'), 'placeholder' => 'City', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                </div>
              <?php } ?>

              <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'){?>
                <div class="col-md-6 col-xs-12 col-sm-6 text-left">
                    <label>State <span>*</span></label>
                    <?php echo $this->Form->input('state', array('class' => 'form-control margin-bottom-20', 'label' => false,
                        'type' => 'text', 'value' => AuthComponent::user('state'), 'placeholder' => 'State', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                </div>
              <?php } ?>

              <div class="<?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'){ echo 'col-md-6 col-xs-12 col-sm-6'; }else{ echo 'col-md-12 col-xs-12 col-sm-12'; } ?> text-left">
                <label>Country <span>*</span></label>
                <?php echo $this->Form->input('country', array('class' => 'form-control margin-bottom-20', 'label' => false,
                    'type' => 'text', 'value' => AuthComponent::user('country'), 'placeholder' => 'Country',
                    'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
              </div>

              <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'){?>
                <div class="col-md-6 col-xs-12 col-sm-6 text-left">
                    <label>Zip code <span>*</span></label>
                    <?php echo $this->Form->input('pin_code', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'zipcode', 'value' => AuthComponent::user('pin_code'), 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                </div>
              <?php } ?>
            </fieldset>

            <fieldset>
              <?php if (!isset($ex_ref_page)){?>
                  <div class="col-md-12 col-xs-12 col-sm-12 text-left">
                      <label>Specific Requirement from this report <span>*</span></label>
                      <?php echo $this->Form->input('message', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'textarea', 'placeholder' => 'Specific Requirement from this report.', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                  </div>
              <?php } ?>
            </fieldset>


            <!--<fieldset>
                <div class="col-sm-3 text-left">
                    <label>Captcha<span>*</span></label>
                    <?php echo $this->Html->image($this->Html->url(array('controller' => 'users', 'action' => 'captcha'), true), array('label' => false, 'width' => '150', 'height' => '40', 'alt' => 'captcha')); ?>
                </div>
                <div class="col-sm-3 text-left">
                    <label>Enter Captcha Code <span>*</span></label>
                    <?php
                    echo $this->Form->input('captcha', array('data-bvalidator' => 'required', 'class' => 'input-large form-control hastip', 'data-placement' => 'right', 'type' => 'text', 'placeholder' => 'Enter Captcha Code',
                        'title' => 'Enter the characters you see in the image.', 'label' => false,'autocomplete'=>'off'));
                    ?>
                </div>
            </fieldset>-->


        <!--Change Captcha VG-26/2/2018 -->
            <div class="margin-bottom-20" style="overflow:hidden">
                <div class="col-md-7 col-md-offset-0">
                    <div class="g-recaptcha" data-sitekey="6LfRUFIUAAAAAAr70oYLT9gPRZV0jVD8jZOTataM"></div>
                <!-- <div class="g-recaptcha" data-sitekey="6LekeEYUAAAAAHPe-rEZtnpqMLcQ_TtFdWSsYsP3" data-bvalidator='required'></div> -->
                </div>
            </div>
        <!--Change Captcha VG-26/2/2018 -->            



        <!-- Selecting payment gateway -->
            <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'){ ?>
              <br/><br/>
              <fieldset>
                  <div class="col-md-3 col-xs-12 col-sm-3 paypal-img text-left">
                    <label>
                        <input type="radio" class="radio-inline" name="payment_option" value="paypal">
                        <?php echo $this->Html->image('logos/paypal.png', array('alt' => 'Paypal')); ?>
                    </label>
                  <!-- </div>
                  <div class="col-sm-2 "> -->
                  </div>
                  <div class="col-md-4 col-xs-12 col-sm-4 credit-cards-img text-left">
                    <label>
                        <input type="radio" name="payment_option" value="ccavenue" checked>
                        <?php echo $this->Html->image('logos/visa_mastercart_amex.png', array('alt' => 'credit-cards')); ?>
                    </label>
                  </div>
              </fieldset>
              <br/><br/>
            <?php } ?>
        <!-- *************************** -->


            <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'): ?>
              <fieldset>
                <div class="col-md-12 col-xs-12 col-sm-12 check text-left">
                  <p>
                    <?php echo $this->Form->checkbox('term', array('label' => false, 'data-bvalidator' => 'required')); ?>
                    I read <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'terms_conditions')); ?>"  target="_blank" class="color-green">Terms and Conditions</a>
                    <br>* Due to the nature of goods, orders once placed can't be cancelled.
                  </p>
                </div>
              </fieldset>
            <?php endif; ?>
            <br><br>
            <fieldset>
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <?php echo $this->Form->submit('Submit', array( 'id' =>'contact-submit')) ?>
                </div>
            </fieldset>


          <?php echo $this->Form->end(); ?>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
$(document).ready(function(e){
  var buyoption = localStorage.getItem('buyoption');
  localStorage.removeItem('buyoption');
  $('#EnquiryLicenceType'+buyoption).prop('checked', true);
});
</script>