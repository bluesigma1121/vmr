<style>
    .mtp{
        margin-top: 10px;
    }
    .search_mtp{
        margin-top: 3px;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
             <div class="pull-right">
                    <?php echo $this->Html->link(__('Add Testimonial'), array('action' => 'add'), array('class' => 'btn   btn-outline btn-sm btn-warning dim', 'title' => 'Add Testimonial')); ?>
                </div>
                <h5>Testimonial List</h5>
            </div>
            <div class="ibox-content"> 
                <div class="table-responsive mtp">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('user_id', 'User Name'); ?></th>
                                <th><?php echo $this->Paginator->sort('testimonial_title'); ?></th>
                                <th><?php echo $this->Paginator->sort('testimonial_description'); ?></th>
                                <th><?php echo $this->Paginator->sort('is_verified'); ?></th>
                                <th><?php echo $this->Paginator->sort('is_activated'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead>
                        <?php foreach ($testimonials as $testimonial): ?>
                            <tbody>
                            <td><?php echo h($testimonial['User']['name']); ?>&nbsp;</td>
                            <td><?php echo h($testimonial['Testimonial']['testimonial_title']); ?>&nbsp;</td>
                            <td><?php
                                echo $this->Text->truncate(
                                        $testimonial['Testimonial']['testimonial_description'], 100, array(
                                    'ellipsis' => '...',
                                    'exact' => false
                                        )
                                );
                                ?>&nbsp;</td>
                            <td><?php echo h($yes_no[$testimonial['Testimonial']['is_verified']]); ?>&nbsp;</td>
                            <td><?php echo h($yes_no[$testimonial['Testimonial']['is_verified']]); ?>&nbsp;</td>
                            <td class="actions">
                                <?php echo $this->Html->link(__('view'), array('action' => 'view', $testimonial['Testimonial']['id']), array('class' => 'btn btn-outline btn-sm btn-warning dim', 'title' => 'View')); ?>
                                <?php  echo $this->Html->link(__('Edit'), array('action' => 'edit', $testimonial['Testimonial']['id']), array('class' => 'btn btn-outline btn-sm btn-info dim', 'title' => 'Edit')); ?>                               
                                <?php
                                if (AuthComponent::user('role') == 1) {
                                    if ($testimonial['Testimonial']['is_active'] == 1) {
                                        echo $this->Html->link(__('Deactive'), array('action' => 'deactive', $testimonial['Testimonial']['id']), array('class' => 'btn btn-outline btn-sm btn-info', 'title' => 'DeActive'));
                                    } else {
                                        echo $this->Html->link(__('Active'), array('action' => 'active', $testimonial['Testimonial']['id']), array('class' => 'btn btn-outline btn-sm btn-info', 'title' => 'Active'));
                                    }
                                }
                                ?>
                                <?php
                                if (AuthComponent::user('role') == 1) {
                                    if ($testimonial['Testimonial']['is_verified'] == 1) {
                                        echo $this->Html->link(__('DeVerified'), array('action' => 'deverified', $testimonial['Testimonial']['id']), array('class' => 'btn btn-outline btn-sm btn-warning', 'title' => 'DeVerified'));
                                    } else {
                                        echo $this->Html->link(__('Verified'), array('action' => 'verified', $testimonial['Testimonial']['id']), array('class' => 'btn btn-outline btn-sm btn-warning', 'title' => 'Verified'));
                                    }
                                }
                                ?>
                                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $testimonial['Testimonial']['id']), array('class' => 'btn btn-outline btn-sm btn-danger dim', 'title' => 'Delete'), __('Are you sure you want to delete # %s?', $testimonial['Testimonial']['testimonial_title'])); ?>
                            </td>
                            </tbody>
                        <?php endforeach; ?>
                    </table>
                    <div class="row col-md-12">
                        <div class="dataTables_paginate paging_bootstrap">
                            <p>
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?> </p>
                            <ul class="pagination" style="visibility: visible;">
                                <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
                                <li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
                                <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
