<?php echo $this->Html->script('back/ckeditor/ckeditor.js'); ?>
    <script type="text/javascript">
            $(document).ready(function() {
                $('#add_testimonial').bValidator();
            });
        </script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5> Add Testimonial</h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('Testimonial', array('id' => 'add_testimonial', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-1">
            <div class="form-group"><label class="col-lg-3 control-label">Title</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('testimonial_title', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Testimonial Title', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"> Description</label>
                <div class="col-lg-6">
                    <?php echo $this->Form->input('testimonial_description', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Testimonial Description', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>

         
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>