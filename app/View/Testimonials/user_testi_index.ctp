<div class="breadcrumbs border_tp">
    <div class="container">
        <h1 class="pull-left">Account</h1>
        <ul class="pull-right breadcrumb"> 
            <span class="text_color">Your Are Here :</span>
            <li><a href="<?php echo Router::url('/') ?>">Home</a></li>
            <li><a href="#">Your Account</a></li>
        </ul>
    </div>
</div> 
<div class="container content">
    <div class="row">
        <div class="col-md-9">
            <!-- Tabs -->
            <div class="tab-v2 margin-bottom-60">
                <?php echo $this->element('my_account_panel'); ?>              
                <div class="tab-content">
                    <div id="enquiries" class="inside">
                        <p>
                            Please find below your Testimonial history with valuemarketresearch.com.<span class="pull_left">  <?php echo $this->Html->link(__('  Add Testimonial'), array('action' => 'add'), array('class' => 'btn btn-outline btn-sm mar_top_user_testi_index btn-success dim edit_testi_user fa fa-plus', 'title' => 'Add')); ?></span>
                        </p><hr> 

                        <?php if (!empty($user_testies)) { ?>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th> Date </th>
                                            <th>Subject </th>
                                            <th>Description </th>
                                            <th> Active </th>
                                            <th>Verified </th>
                                            <th>Action </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $cnt = 0;
                                        foreach ($user_testies as $key => $testi) {
                                            ?>
                                            <tr>
                                                <td><?php echo++$cnt; ?></td>
                                                <td><?php echo Date('d-M-Y h:i a', strtotime($testi['Testimonial']['created'])); ?></td>
                                                <td><?php echo $this->Text->truncate($testi['Testimonial']['testimonial_title'], 50); ?></td>
                                                <td><?php echo $this->Text->truncate($testi['Testimonial']['testimonial_description'], 50); ?></td>
                                                <td><?php echo $yes_no[$testi['Testimonial']['is_active']]; ?></td>
                                                <td><?php echo $yes_no[$testi['Testimonial']['is_verified']]; ?></td>
                                                <td class="actions">
                                                    <?php echo $this->Html->link(__('  Edit'), array('action' => 'edit', $testi['Testimonial']['id']), array('class' => 'btn btn-outline btn-sm mar_top btn-warning dim edit_testi_user fa fa-edit', 'title' => 'Edit')); ?>
                                                    <?php echo $this->Html->link(__(' view'), array('action' => 'user_view', $testi['Testimonial']['id']), array('class' => 'btn btn-outline mar_top btn-sm btn-info dim view_testi_user fa fa-eye', 'title' => 'View')); ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <hr>   
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        </div>
    </div>          
</div>