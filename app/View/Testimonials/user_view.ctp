<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>valuemarketresearch.com</title>
        <!-- Bootstrap core CSS -->
        <?php echo $this->Html->css(array('front/bootstrap/css/bootstrap.min.css', 'front/bvalidator.css')); ?>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .mar_top_hed{
                margin-top: -40px;
                color: #555;
                text-align: center;
                margin-bottom: 5px;
                border-bottom: solid 1px #eee;

            }
            .inver{
                color: red !important
            }
            .ver{
                color: green !important
            }
        </style>

        <?= $this->Html->script(array('front/plugins/jquery/jquery.min.js',  'front/plugins/bootstrap/js/bootstrap.min.js', 'front/jquery.bvalidator-yc.js')); ?>
    </head>
    <body>
        <div class="container content">
            <div class="row  funny-boxes-top-sea">
                <div class="col-md-3 funny-boxes-img">
                    <?php echo ucfirst($testi['User']['name']); ?>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-briefcase"></i>  <?php echo ucfirst($testi['User']['organisation']); ?></li>
                        <li><i class="fa fa-map-marker"></i> <?php echo ucfirst($testi['User']['city']); ?> <?php echo ucfirst($testi['User']['country']); ?></li>
                    </ul>
                </div>
                <div class="col-md-9">
                    <h2><?php echo ucfirst($testi['Testimonial']['testimonial_title']); ?> </h2>
                    <p>
                        <i class="fa fa-calendar"></i> <?php echo Date('d-M-Y h:i a', strtotime($testi['Testimonial']['created'])); ?>  <?php if ($testi['Testimonial']['is_active'] == 1) { ?>
                            <i class="fa fa-check ver"></i> Active
                        <?php } else { ?>
                            <i class="fa fa-times inver"></i> InActive
                        <?php }
                        ?>
                        <?php if ($testi['Testimonial']['is_verified'] == 1) { ?>
                            <i class="fa fa-check ver"></i> Verified
                        <?php } else { ?>
                            <i class="fa fa-times inver"></i> InVerified
                        <?php } ?>
                    </p>
                    <p>
                        <?php echo $testi['Testimonial']['testimonial_description'] ?>
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
