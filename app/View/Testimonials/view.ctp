
<div class="row">
    <div class="wrapper wrapper-content animated fadeInUp">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <div class="m-b-md text-center">
                                <h2 class="btn btn-primary btn-rounded btn-block">Testimonial Details</h2>
                            </div>
                        </div>
                        <div class="col-lg-4"> </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5">
                        <dl class="dl-horizontal">
                            <dt>User Name:</dt> <dd>  <?php echo h($testimonial['User']['name']); ?></dd>
                            <dt>Verified:</dt> <dd>  <?php echo h($yes_no[$testimonial['Testimonial']['is_verified']]); ?></dd>
                            <dt>Activated:</dt> <dd>  <?php echo h($yes_no[$testimonial['Testimonial']['is_active']]); ?></dd>
                        </dl>
                    </div>
                    <div class="col-lg-7" id="cluster_info">
                        <dl class="dl-horizontal" >
                            <dt>Title:</dt> <dd>  <?php echo h($testimonial['Testimonial']['testimonial_title']); ?></dd>
                            <dt>Activated:</dt> <dd>  <?php echo h($yes_no[$testimonial['Testimonial']['is_active']]); ?></dd>
                        </dl>
                    </div>
                </div>

                <div class="row">
                    <dl class="dl-horizontal">
                        <dt>Description:</dt> 
                        <dd><?php echo h($testimonial['Testimonial']['testimonial_description']); ?></dd>
                    </dl>
                </div>
            
                <div class="row">
                    <div class="text-center">
                        <button onclick="goBack();" class="btn btn-info btn-rounded btn-outline">Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
