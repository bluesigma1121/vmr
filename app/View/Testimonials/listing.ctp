<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>Testimonials</h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a>Testimonials</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->


<!-- TESTIMONIALS INNER SECTION START -->
<div class="testimonial-main">
  <div class="container">
    <div class="row">
      <?php foreach ($testimonials as $key => $testimonial) { ?>
        <div class="col-md-6">
          <section class="testimonial" aria-label="testimonal">
            <div class="testimonial-text">
                <div class="scrollDiv scroll_delayed">
                    <div class="scroll-content">
                        <p> <i class="fa fa-quote-left fa-lg" aria-hidden="true"></i> <?php echo " \" "; echo  $testimonial['Testimonial']['testimonial_description'] ;echo" \" "; ?></p>
                    </div>
                </div>
                <h5><i class="fa fa-user-circle fa-lg" aria-hidden="true"></i> <?php echo $testimonial['Testimonial']['testimonial_title'];?></h5>
                <!-- <h6>- Position</h6> -->
            </div>
          </section>
        </div>
      <?php } ?>
    </div>
    <div class="row text-center">
      <ul class="pagination">
        <?php if (isset($this->params['paging']['Testimonial']['pageCount'])) {
            $page_cnt = $this->params['paging']['Testimonial']['pageCount'];
            $current = $this->params['paging']['Testimonial']['page'];
        } elseif (isset($this->params['paging']['Product']['pageCount'])) {
            $page_cnt = $this->params['paging']['Product']['pageCount'];
            $current = $this->params['paging']['Testimonial']['page'];
        }
       ?>
        <?php if ($current > 2): ?>
            <a href="<?php echo Router::url("/", true); ?>industries/<?php echo $paginator_url ?>-page-<?php echo $current - 1 ?>">&lt; &nbsp; Previous</a>
        <?php elseif ($current == 2): ?>
            <a href="<?php echo Router::url("/", true); ?>industries/<?php echo $paginator_url ?>">&lt; &nbsp; Previous</a>
        <?php else: ?>
            &lt; &nbsp; Previous &nbsp;
        <?php endif; ?>
        &nbsp;&nbsp;|&nbsp;&nbsp;

        <?php if ($page_cnt != $current && $page_cnt > 0): ?>
            <a href="<?php echo Router::url("/", true); ?>industries/<?php echo $paginator_url ?>-page-<?php echo $current + 1 ?>">Next &nbsp; &gt;</a>
        <?php else: ?>
            Next &nbsp; &gt;
        <?php endif; ?>
        <br>
        <small>
          <?php echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
            )); ?>
        </small>

     </ul>
    </div>
  </div>
</div>
<!-- TESTIMONIALS INNER SECTION END -->

<!-- CLIENT LOGO SECTION START -->
<section id="services" class="services-section">
    <div class="container bgwhite">
        <div class="row">
            <div class="col-lg-12">
                <!--Item slider text-->
                <div class="">
                  <div class="row" id="slider-text">
                    <div class="header-section text-center">
                      <h1 class="">Our Clients</h1>
                      <hr class="bottom-line">
                    </div>
                  </div>
                </div>

                <!-- Item slider-->
                <div class="container-fluid row">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="carousel carousel-showmanymoveone slide" id="itemslider">
                        <div class="carousel-inner carousel-inner-pay">
                          <?php foreach ($ourclients as $key => $clients) { ?>

                            <div class="item <?= $key == 0?'active':''?> payotn">
                              <div class="col-sm-2 col-md-2">
                                <?php if (!empty($clients['OurClient']['logo'])) { ?>
                                    <!--<a href="#" target="blank">-->
                                      <?= $this->Html->image('client_images/' . $clients['OurClient']['logo'], array('class' => '','alt' => 'Client Logos')); ?>
                                    <!--</a>-->
                                <?php } else {
                                        echo $this->Html->image('noimage.png', array('class' => '','alt' => 'Client Logos'));
                                      } ?>
                              </div>
                            </div>
                          <?php } ?>
                        </div>

                        <div id="slider-control">
                        <a class="left carousel-control" href="#itemslider" data-slide="prev">
                          <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                        </a>
                        <a class="right carousel-control" href="#itemslider" data-slide="next">
                          <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
                        </a>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Item slider end-->
            </div>
        </div>
    </div>
</section>
<!-- CLIENT LOGO SECTION END -->