<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Production House</title>
        <!-- Bootstrap core CSS -->
        <?php echo $this->Html->css(array('front/bootstrap/css/bootstrap.min.css', 'front/bvalidator.css')); ?>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .mar_top_hed{
                margin-top: -40px;
                color: #555;
                text-align: center;
                margin-bottom: 5px;
                border-bottom: solid 1px #eee;

            } 
            .inver{
                color: red !important
            }
            .ver{
                color: green !important
            }
        </style> 
        <script type="text/javascript">
            $(document).ready(function() {
                $('#edit_user').bValidator();
            });
        </script>

        <?= $this->Html->script(array('front/plugins/jquery/jquery.min.js',  'front/plugins/bootstrap/js/bootstrap.min.js', 'front/jquery.bvalidator-yc.js')); ?>
    </head>
    <body> 
        <div class="container content"> 
            <div class="row">
                <!-- Begin Sidebar Menu -->
                <h3> Testimonial Add</h3>
                <div class="funny-boxes funny-boxes-top-sea">
                    <div class="ibox-content">
                        <?php echo $this->Form->create('Testimonial', array('id' => 'edit_user', 'class' => 'form-horizontal', 'type' => 'file')); ?>
                        <div class="row">
                            <div class="form-group"><label class="col-lg-2 control-label">Title</label>
                                <div class="col-lg-6">
                                    <?php echo $this->Form->input('testimonial_title', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Testimonial Title', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-lg-2 control-label">Description</label>
                                <div class="col-lg-6">
                                    <?php echo $this->Form->input('testimonial_description', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Testimonial Description', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>