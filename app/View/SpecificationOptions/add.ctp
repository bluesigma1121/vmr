<script type="text/javascript">
    $(document).ready(function() {
        $('#add_options').bValidator();
        var i = 1;
        $('#add_child_option1').click(function() {
            $('#add_more_childs1').append('<tr id="display_child' + i + '"><td><input name="data[option_name][' + i + ']" class="form-control opt_name' + i + '" data-bvalidator="required" placeholder="Enter Child Name " type="text" id="option_name' + i + '" required="required"></td><td><input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger ml  " value="Remove" onclick=" del_row(' + i + ');" </td> </tr>');
            i++;
        });
    });
    function del_row(i) {
        $("#display_child" + i).remove();
    }
</script>
<style>
    .ml{
        margin-left: 15px;
    }
</style>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Add Options To <?php echo $spec['Specification']['specification_name'] ?></h5>
    </div>
    <div class="ibox-content"> 


        <?php echo $this->Form->create('SpecificationOption', array('id' => 'add_options', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-2">
            <div class="form-group">
                <label class="col-md-3 control-label">Option Name :</label>
                <div class="form-group">
                    <table class="col-md-6" id="add_child_option">
                        <thead>
                            <tr role="row"><th> </th>
                                <th>  
                        <div id="plusminus_id" class="plusminus" style="
                             margin-left: 15px;
                             ">
                            <a href="#" id="add_child_option1" class="btn btn-success"><i class="fa fa-plus"> Add Options</i></a>
                        </div>
                        </th>
                        </tr>                      
                        </thead> 
                        <tbody role="alert" id="add_more_childs1" aria-live="polite" aria-relevant="all">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-primary btn-sm', 'div' => false)); ?>
            <a href="<?php echo Router::url(array('controller' => 'specifications', 'action' => 'index')) ?>" class="btn btn-sm btn-success">Back</a>      
        </div>
        <?php echo $this->Form->end(); ?>

        <table class="table table-bordered">
            <tr>
                <th>Specifications</th>
                <th>Options</th>
            </tr>

            <tr>
                <td><?= ucwords($spec['Specification']['specification_name']); ?>
                </td>
                <td>
                    <ul>
                        <? foreach ($spec['SpecificationOption'] as $op): ?>
                        <li>
                            <?= ucwords($op['option_name']); ?>

                        </li>
                        <br>
                        <? endforeach; ?>
                    </ul>
                </td>
            </tr>
        </table>
    </div>
</div>

