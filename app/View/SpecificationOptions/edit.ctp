<div class="specificationOptions form">
<?php echo $this->Form->create('SpecificationOption'); ?>
	<fieldset>
		<legend><?php echo __('Edit Specification Option'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('option_name');
		echo $this->Form->input('specification_id');
		echo $this->Form->input('is_active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SpecificationOption.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('SpecificationOption.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Specification Options'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Specifications'), array('controller' => 'specifications', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Specification'), array('controller' => 'specifications', 'action' => 'add')); ?> </li>
	</ul>
</div>
