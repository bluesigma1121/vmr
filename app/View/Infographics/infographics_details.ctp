<style>
* {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Float four columns side by side */
.column {
  float: left;
  width: 25%;
  padding: 0 10px;
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive columns */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
    display: block;
    margin-bottom: 20px;
  }
}

/* Style the counter cards */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  padding: 16px;
  margin-bottom:10px;
  text-align: center;
  background-color: #f1f1f1;
}
/* style for download sample buttons */
.ig-bottom-block .read-report-btn {
    color: #8E258B;
    border-color: #8E258B;
}

.ig-bottom-block .read-more-btn {
    padding: .7em 1em;
}
.read-more-btn {
    display: inline-block;
    font-size: .9em;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: 1px;
    padding: 0.75em 1.5em;
    border: 2px solid #0868ad;
    color: #fff;
    transition: all 0.33s ease;
    border-radius: 2px;
    margin: 1em 0 0;
    background: transparent;
}
.ig-bottom-block .read-report-btn:hover {
    background: #8E258B;
    color: #fff;
}
.ig-bottom-block .read-report-btn {
    color: #8E258B;
    border-color: #8E258B;
}
.ig-bottom-block .read-more-btn {
    padding: .7em 1em;
}
.ig-bottom-block .sample-request-btn {
    color: #0868ad;
}
.ig-bottom-block .read-more-btn {
    padding: .7em 1em;
}

.read-more-btn {
    display: inline-block;
    font-size: .9em;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: 1px;
    padding: 0.75em 1.5em;
    border: 2px solid #0868ad;
    color: #fff;
    transition: all 0.33s ease;
    border-radius: 2px;
    margin: 1em 0 0;
    background: transparent;
}
.ig-bottom-block .sample-request-btn:hover {
    background: #0868ad;
    color: #fff;
}
.ig-bottom-block .sample-request-btn {
    color: #0868ad;
}
.ig-bottom-block .read-more-btn {
    padding: .7em 1em;
}
.ig-bottom-block .download-ig-btn {
    color: #f54b28;
    border-color: #f54b28;
}
.ig-bottom-block .read-more-btn {
    padding: .7em 1em;
}
.read-more-btn {
    display: inline-block;
    font-size: .9em;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: 1px;
    padding: 0.75em 1.5em;
    border: 2px solid #0868ad;
    color: #fff;
    transition: all 0.33s ease;
    border-radius: 2px;
    margin: 1em 0 0;
    background: transparent;
}
.ig-bottom-block .download-ig-btn:hover {
    background: #f54b28;
    color: #fff;
}
.ig-bottom-block .download-ig-btn {
    color: #f54b28;
    border-color: #f54b28;
}
.ig-bottom-block .read-more-btn {
    padding: .7em 1em;
}
</style>
<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-kcolor bg-color-inner-main">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>About Us</h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a>Infographics</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->
<style>.page-heading{color: #666;font-size: 14px;text-transform: uppercase;font-weight: 700;}</style>

<div class="container content about-inner-page">
    <div class="row margin-bottom-40">
        <div class="col-md-12 md-margin-bottom-40">
            <!-- <div class="headline"><h1 class="page-heading">Infographics</h1></div> -->
         
           <br>
            <div class="row">
            <!---start-->
            <?php //print_r($ourinfographics);?>
            <?php 
            foreach ($ourinfographics as $key => $infographic) {
            //echo $infographic['Infographic']['id']
            
            ?>
            
              
      <input type="hidden" value="<?php echo $infographic['Infographic']['id'];?>">
        <div class="infographics-dtls-block">
        <div class="container-fluid">
											<div class="row">
                      <div class="col-xs-12 col-md-9">
                      <div class="col-xs-12 col-md-12" >
															<h1 style="margin-top: -14px;"><?php echo $infographic['Infographic']['info_title'];?></h1>
														</div>
                      
        <?php 
     echo $this->Html->image('infographics_images/' . $infographic['Infographic']['image_path'], array('class' => 'img-thumbnail image_size','style'=>'width:100%;height:auto;'));
    ?>
    <div class="ig-bottom-block" style="margin-bottom: 136px;">
															<div class="row">
																<div class="col-xs-12 col-md-4 text-center">
																	<!-- <a href="<?php //echo $infographic['Infographic']['linktoreport'];?>" class="read-report-btn read-more-btn" target="_blank">
																		<span>Download Sample</span>
																	</a>
                                                                    													 -->
               
               
               
                <?php 
                //  echo $this->Html->link(__('View Report').
            //     // Router::connect(
            //         '/report/'.$infographic['productsJoin']['slug'], array('controller' => 'products', 'action' => 'category_details'), $infographic['productsJoin']['slug'])
            // ;
                
                // echo $this->Html->link(__('View Report'), array('controller' => 'products', 'action' => 'category_details', $infographic['productsJoin']['slug'])); ?>

                                  <a href="<?php echo Router::url('/report/', true) ?><?php echo $infographic['productsJoin']['slug'];?>/" class="read-report-btn read-more-btn" target="_blank">
																		<span>View Report</span>
																	</a>
																</div>
																<div class="col-xs-12 col-md-4 text-center">
																	<a href="<?php echo Router::url('/contact/', true) ?><?php echo $infographic['productsJoin']['slug'];?>/download-sample" class="sample-request-btn read-more-btn">
																		<span>Download FREE Sample</span>
                                    </a>

																</div>
																<div class="col-xs-12 col-md-4 text-center">
																	<a href="<?php echo Router::url('/contact/', true) ?><?php echo $infographic['productsJoin']['slug'];?>/request-customization" class="download-ig-btn read-more-btn" id="infographics_image_id">
																		<span>Request Customization </span>
																	</a>
                                 
																</div>	
															</div>														
														</div>
        <!-- <p></p> -->
        </div>
        <div class="col-xs-12 col-md-3">
													<aside class="aside-block">
														<div class="ig-info-block">
															<div class="col-xs-12 col-md-12">
																<p><strong>Published : </strong> <span><?php
      $datetime = explode(" ",$infographic['Infographic']['created']);
					echo $date = $datetime[0];?></p></span></p>
															</div>
														</div>
														<div class="ig-info-block">
															<div class="col-xs-12 col-md-12">
																<p><strong>Description : </strong> <span><p><?php echo $infographic['Infographic']['info_desc']; ?></p>
</span></p>

															</div>
														</div>

														 <!-- <div class="related-ig-list-block">
															<div class="row">
																<div class="col-xs-12 col-md-12">
																	<p><strong>Trending Infographics</strong></p>
																</div>	
															</div>
															<div class="row"> -->
                              <?php 
           // foreach ($ourinfographicstrending as $key => $infographictrending) {
            // echo $infographictrending['Infographic']['id'];
            ?>

																																	<!-- <div class="col-xs-12 col-md-6">
																		<a href="#"> -->
																			<!-- <img src="infographics_images/<?php //echo $infographictrending['Infographic']['image_path']?>" class="img-responsive" alt="Trending Infographics" title="Trending Infographics"> -->
																	
                                   
                        <?php
                              //echo $this->Html->image('infographics_images/' . $infographic['Infographic']['image_path'], array('class' => 'img-responsive'));
                         
                         
                          ?>
															</a>			<!-- </div>
																																	<div class="col-xs-12 col-md-6">
																		<a href="https://www.futuremarketinsights.com/infographics/pharmaceutical-containers-infographic">
																			<img src="https://www.futuremarketinsights.com/uploads/infographics/thumb/pharmaceutical-containers-thumb.jpg" class="img-responsive" alt="Trending Infographics" title="Trending Infographics">
																		</a>
																	</div> -->
																		<?php //} ?>															
																																
																															<!-- </div>
														</div>  -->

													</aside>
												</div>

        </div>
        </div>
  <!---->
</a>

</div>
            <?php } ?>

          </div>
        </div>
    </div><!--/row-->
</div>

<!--/container-->


