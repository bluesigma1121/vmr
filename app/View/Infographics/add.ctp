<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Edit Product Description Image Details</title>
        <!-- Bootstrap core CSS -->
        <?php echo $this->Html->css(array('front/bootstrap/css/bootstrap.min.css', 'front/bvalidator.css')); ?>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .mar_top_hed{
                margin-top: -40px;
                color: #555;
                text-align: center;
                margin-bottom: 5px;
                border-bottom: solid 1px #eee;

            } 
            .inver{
                color: red !important
            }
            .ver{
                color: green !important
            }
        </style> 
        <script type="text/javascript">
            $(document).ready(function() {
                $('#add_rdi').bValidator();
            });
        </script>

        <?= $this->Html->script(array('front/plugins/jquery/jquery.min.js',  'front/plugins/bootstrap/js/bootstrap.min.js', 'front/jquery.bvalidator-yc.js')); ?>
    </head>
    <body> 
        <div class="container content"> 
            <div class="row">
                <!-- Begin Sidebar Menu -->
                <h3> Add Infographics</h3>
                <?php   $ss=$this->Html->url( null, true );
				    $endproductid = end((explode('/', $ss)));
		   ?> 
                <div class="funny-boxes funny-boxes-top-sea">
                    <div class="ibox-content">
                        <?php echo $this->Form->create('Infographic', array('id' => 'add_infographics', 'class' => 'form-horizontal', 'type' => 'file')); ?>
                        <div class="row">
                            <div class="form-group"><label class="col-lg-2 control-label">Infographic Title</label>
                                <div class="col-lg-6">
                                    <?php echo $this->Form->input('info_title', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Infographic Title', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                </div>
                            </div>
                            <?php echo $this->Form->input('product_id', array('type' => 'hidden','value'=>$endproductid)); ?>
                           
                             <div class="form-group"><label class="col-lg-2 control-label">Infographic Description</label>
                                <div class="col-lg-6">
                                   <?php echo $this->Form->input('info_desc', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Description', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                </div>
                            </div>
                           
                            <div class="form-group"><label class="col-lg-2 control-label">Meta Title</label>
                                <div class="col-lg-6">
                                   <?php echo $this->Form->input('meta_title', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Meta Title', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-lg-2 control-label">Meta Keywords</label>
                                <div class="col-lg-6">
                                   <?php echo $this->Form->input('meta_keyword', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Meta Keywords', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-lg-2 control-label">Meta Description</label>
                                <div class="col-lg-6">
                                   <?php echo $this->Form->input('meta_desc', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Meta Keywords', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                </div>
                            </div>
                            <!------------------start description image upload---------->
                        <div class="form-group"><label class="col-lg-2 control-label">Infographic Image</label>
                            <div class="col-lg-6" style="width:616px">
                                <?php echo $this->Form->input('image_path', array('class' => 'form-control', 'type' => 'file', 'placeholder' => 'Image', 'label' => false, 'data-bvalidator' => '')); ?>
                            </div>
                        </div>
                            <!------------------end description image upload---------->
                            <div class="form-group"><label class="col-lg-2 control-label">Infographic Slug</label>
                                <div class="col-lg-6">
                                   <?php echo $this->Form->input('slug', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Infographic Slug', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
                            <!-- <button onclick="javascript:history.back()" class="btn btn-sm btn-info">Back</button> -->
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script>
$(document).ready(function(){
//location.reload();
$('.form-control').val("");
});
</script>