<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
           
            <div class="ibox-title">
       	<div class="pull-right">
		   <?php   $ss=$this->Html->url( null, true );
				    $endproductid = end((explode('/', $ss)));
		   ?> 
		   <?php echo $this->Html->link(__('Add'), array('action' => 'add',$endproductid), array('class' => 'tn   btn-outline btn-sm btn-warning dim', 'title' => 'Add')); ?>
           <!-- <a href="<?php //echo Router::url('/infographics/add', true);?>"><input type="button" value="Add" class="tn   btn-outline btn-sm btn-warning dim"/></a> -->
		                
          <?php //echo $this->Html->link(__('New Slideshow'), array('action' => 'add'), array('class' => 'btn   btn-outline btn-sm btn-warning dim', 'title' => 'Add Slideshow')); ?>
        </div>
        <h5>Infographics List</h5> 
      </div>
         
                        <div class="ibox-content">
          <div class="table-responsive mtp">

						<table class="table table-bordered table-striped">
							<thead>
								<tr>
										<!-- <th><?php// echo $this->Paginator->sort('id'); ?></th> -->
                                        <th><?php echo $this->Paginator->sort('title'); ?></th>
										<th><?php echo $this->Paginator->sort('image'); ?></th>
										<th><?php echo $this->Paginator->sort('slug'); ?></th>
                                        <th><?php echo $this->Paginator->sort('created_on'); ?></th>
										<th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($infographics as $infographic): ?>
									<tr>
										<!-- <td><?php //echo h($infographic['Infographic']['id']); ?>&nbsp;</td> -->
                                        <td><?php echo h($infographic['Infographic']['info_title']); ?>&nbsp;</td>
										<td>
                      <?php if (!empty($infographic['Infographic']['image_path'])) { ?>
                        <?php
                              echo $this->Html->image('infographics_images/' . $infographic['Infographic']['image_path'], array('class' => 'img-thumbnail image_size','style'=>'hieght:90px;width:90px;'));
                          } else {
                              echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail image_size'));
                          }
                          ?>&nbsp;
                    </td>
					<td><?php echo h($infographic['Infographic']['slug']); ?>&nbsp;</td>
                    <td><?php   //echo h(date('d-m-Y', $infographic['Infographic']['created_on']));
					//$timestamp = "2012-04-02 02:57:54";
					$datetime = explode(" ",$infographic['Infographic']['created']);
					echo $date = $datetime[0];
					//$time = $datetime[1];
					//echo h($infographic['Infographic']['created_on']); ?>&nbsp;</td>
										<td class="actions">
											<?php //echo $this->Html->link(__('view'), array('action' => 'view', $slideshow['Slideshow']['id']), array('class' => 'btn btn-outline btn-sm btn-warning dim', 'title' => 'View')); ?>
											<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $infographic['Infographic']['id'],$endproductid), array('class' => 'btn btn-outline btn-sm btn-info dim', 'title' => 'Edit')); ?>
											<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $infographic['Infographic']['id'],$endproductid), array('class' => 'btn btn-outline btn-sm btn-danger dim', 'title' => 'Delete'), __('Are you sure you want to delete # %s?', $infographic['Infographic']['id'])); ?>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						 <div class="row col-md-12">
							<!-- <div class="dataTables_paginate paging_bootstrap">
								<p>
									<?php
									echo $this->Paginator->counter(array(
									'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
									));
									?>
								</p>
								<ul class="pagination" style="visibility: visible;">
										<li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
										<li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
										<li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
								</ul>
							</div> -->
						</div> 
					</div>
				</div>
                </div>
            </div>
            </div>


            
       