<style>
* {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Float four columns side by side */
.column {
  float: left;
  width: 25%;
  padding: 0 10px;
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive columns */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
    display: block;
    margin-bottom: 20px;
  }
}

/* Style the counter cards */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  padding: 16px;
  margin-bottom:10px;
  text-align: center;
  background-color: #f1f1f1;
}
/* style for download sample buttons */
.ig-bottom-block .read-report-btn {
    color: #8E258B;
    border-color: #8E258B;
}

.ig-bottom-block .read-more-btn {
    padding: .7em 1em;
}
.read-more-btn {
    display: inline-block;
    font-size: .9em;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: 1px;
    padding: 0.75em 1.5em;
    border: 2px solid #0868ad;
    color: #fff;
    transition: all 0.33s ease;
    border-radius: 2px;
    margin: 1em 0 0;
    background: transparent;
}
.ig-bottom-block .read-report-btn:hover {
    background: #8E258B;
    color: #fff;
}
.ig-bottom-block .read-report-btn {
    color: #8E258B;
    border-color: #8E258B;
}
.ig-bottom-block .read-more-btn {
    padding: .7em 1em;
}
.ig-bottom-block .sample-request-btn {
    color: #0868ad;
}
.ig-bottom-block .read-more-btn {
    padding: .7em 1em;
}

.read-more-btn {
    display: inline-block;
    font-size: .9em;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: 1px;
    padding: 0.75em 1.5em;
    border: 2px solid #0868ad;
    color: #fff;
    transition: all 0.33s ease;
    border-radius: 2px;
    margin: 1em 0 0;
    background: transparent;
}
.ig-bottom-block .sample-request-btn:hover {
    background: #0868ad;
    color: #fff;
}
.ig-bottom-block .sample-request-btn {
    color: #0868ad;
}
.ig-bottom-block .read-more-btn {
    padding: .7em 1em;
}
.ig-bottom-block .download-ig-btn {
    color: #f54b28;
    border-color: #f54b28;
}
.ig-bottom-block .read-more-btn {
    padding: .7em 1em;
}
.read-more-btn {
    display: inline-block;
    font-size: .9em;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: 1px;
    padding: 0.75em 1.5em;
    border: 2px solid #0868ad;
    color: #fff;
    transition: all 0.33s ease;
    border-radius: 2px;
    margin: 1em 0 0;
    background: transparent;
}
.ig-bottom-block .download-ig-btn:hover {
    background: #f54b28;
    color: #fff;
}
.ig-bottom-block .download-ig-btn {
    color: #f54b28;
    border-color: #f54b28;
}
.ig-bottom-block .read-more-btn {
    padding: .7em 1em;
}
</style>
<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-kcolor bg-color-inner-main">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3>About Us</h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a>Infographics</a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->
<style>.page-heading{color: #666;font-size: 14px;text-transform: uppercase;font-weight: 700;}</style>

<div class="container content about-inner-page">
    <div class="row margin-bottom-40">
        <div class="col-md-12 md-margin-bottom-40">
            <div class="headline"><h1 class="page-heading">Infographics</h1></div>
  
            <div class="row">
            <!---start-->
            <?php //print_r($ourinfographics);?>
            <?php 
            foreach ($ourinfographics as $key => $infographic) {
            //echo $infographic['Infographic']['id']
            ?>


 <a>
<!-- <a href="#myModal<?//= $infographic['Infographic']['id'] ?>"  data-target="#myModal<?= $infographic['Infographic']['id'] ?>"  data-toggle="modal" data-code="code" data-company="company name">s -->
  <!-- <img src="../images/edit.png" style="width:20px;"> -->

  <!---->
 <div class="column">
    <div class="card">

  
    <?php
  //echo $this->Html->link($this->Html->image('infographics_images/' . $infographic['Infographic']['image_path'],array('class' => 'img-thumbnail image_size','style'=>'width:342px;height:220px;')),array('controller' => 'infographics', 'action' => 'infographics_details',$infographic['Infographic']['slug']), array('escape' => false));
   ?>
    <a href="<?php echo Router::url('/infographics/', true),$infographic['Infographic']['slug']?>">
    <?php echo $this->Html->image('infographics_images/' . $infographic['Infographic']['image_path'], array('class' => 'img-thumbnail image_size','style'=>'width:342px;height:220px;'));?>
   </a>

  

   
    <?php
    
     //echo $this->Html->image('infographics_images/' . $infographic['Infographic']['image_path'], array('class' => 'img-thumbnail image_size','style'=>'width:342px;height:220px;'));
    ?>
    <!-- <p><img src="uploads/<?php //echo $infographic['Infographic']['image']; ?>"></p> -->
      <p style="font-size: 18px; padding-top:10px;"><?php echo $infographic['Infographic']['info_title'];?></p>
      <p><?php 
      
      
      $datetime = explode(" ",$infographic['Infographic']['created']);
					echo $date = $datetime[0];
      //echo $infographic['Infographic']['created_on']?></p>
     
    </div>
  </div>
  <!---->
</a>
<div class="modal fade bs-example-modal-sm" tabindex="-1" id="myModal<?= $infographic['Infographic']['id'] ?>">
  <div class="modal-dialog modal-sm" style="width: 90%;max-width: 90%;">
    <div class="modal-content">
      <div class="modal-header">
      
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" >×</span></button>
        <h4 class="modal-title" id="mySmallModalLabel"><?php //echo $infographic['Infographic']['title'];?></h4>
      </div>
      <div class="modal-body">
     
      
      <input type="hidden" value="<?php echo $infographic['Infographic']['id'];?>">
        <div class="infographics-dtls-block">
        <div class="container-fluid">
											<div class="row">
                      <div class="col-xs-12 col-md-9">
                      <div class="col-xs-12 col-md-12" >
															<h1 style="margin-top: -14px;"><?php echo $infographic['Infographic']['info_title'];?></h1>
														</div>
                      
        <?php 
     echo $this->Html->image('infographics_images/' . $infographic['Infographic']['image_path'], array('class' => 'img-thumbnail image_size','style'=>'width:100%;height:auto;'));
    ?>
    <div class="ig-bottom-block">
															<div class="row">
																<div class="col-xs-12 col-md-4 text-center">
																	<!-- <a href="<?php //echo $infographic['Infographic']['linktoreport'];?>" class="read-report-btn read-more-btn" target="_blank">
																		<span>Download Sample</span>
																	</a>														 -->
                                  <a href="report/<?php echo $infographic['productsJoin']['slug'];?>/" class="read-report-btn read-more-btn" target="_blank">
																		<span>View Report</span>
																	</a>
																</div>
																<div class="col-xs-12 col-md-4 text-center">
																	<a href="contact/<?php echo $infographic['productsJoin']['slug'];?>/download-sample" class="sample-request-btn read-more-btn">
																		<span>Download FREE Sample</span>
                                    </a>
                                  <?php
                        // echo $this->Html->link($this->Html->tag('i','',array('class' => 'fa fa-download')).' Download Sample', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'download-sample'), array("class" => "btn-u btn-u-green", "target" => "_blank",'escape' => false)
                        // );
                        ?>

																</div>
																<div class="col-xs-12 col-md-4 text-center">
                                <!-- <?php
                        //echo $this->Html->link("<span>Download Infographics</span>", array('controller' => 'enquiries', 'action' => 'get_lead_info_form_infographics','id' => $infographic['Infographic']['id'],'ref_page' => 'download-infographics'), array("class" => "download-ig-btn read-more-btn", "target" => "_blank",'escape' => false,'id'=>'infographics_image_id', "style"=>'')
                        //);
                        ?> -->
																	<a href="contact/<?php echo $infographic['productsJoin']['slug'];?>/request-customization" class="download-ig-btn read-more-btn" id="infographics_image_id">
																		<span>Request Customization </span>
																	</a>
                                 
																</div>	
															</div>														
														</div>
        <!-- <p></p> -->
        </div>
        <div class="col-xs-12 col-md-3">
													<aside class="aside-block">
														<div class="ig-info-block">
															<div class="col-xs-12 col-md-12">
																<p><strong>Published : </strong> <span><?php
      $datetime = explode(" ",$infographic['Infographic']['created']);
					echo $date = $datetime[0];?></p></span></p>
															</div>
														</div>
														<div class="ig-info-block">
															<div class="col-xs-12 col-md-12">
																<p><strong>Description : </strong> <span><p><?php echo $infographic['Infographic']['info_desc']; ?></p>
</span></p>

															</div>
														</div>

														 <!-- <div class="related-ig-list-block">
															<div class="row">
																<div class="col-xs-12 col-md-12">
																	<p><strong>Trending Infographics</strong></p>
																</div>	
															</div>
															<div class="row"> -->
                              <?php 
           // foreach ($ourinfographicstrending as $key => $infographictrending) {
            // echo $infographictrending['Infographic']['id'];
            ?>

																																	<!-- <div class="col-xs-12 col-md-6">
																		<a href="#"> -->
																			<!-- <img src="infographics_images/<?php //echo $infographictrending['Infographic']['image_path']?>" class="img-responsive" alt="Trending Infographics" title="Trending Infographics"> -->
																	
                                   
                        <?php
                              //echo $this->Html->image('infographics_images/' . $infographic['Infographic']['image_path'], array('class' => 'img-responsive'));
                         
                         
                          ?>
															</a>			<!-- </div>
																																	<div class="col-xs-12 col-md-6">
																		<a href="https://www.futuremarketinsights.com/infographics/pharmaceutical-containers-infographic">
																			<img src="https://www.futuremarketinsights.com/uploads/infographics/thumb/pharmaceutical-containers-thumb.jpg" class="img-responsive" alt="Trending Infographics" title="Trending Infographics">
																		</a>
																	</div> -->
																		<?php //} ?>															
																																
																															<!-- </div>
														</div>  -->

													</aside>
												</div>

        </div>
        </div>
        </div>


      </div>
    </div>
  </div>
</div>
            <?php } ?>

          </div>
        </div>
    </div><!--/row-->
</div>

<!--/container-->


<script>
$(function () {
  $('#myModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var code = button.data('code'); // Extract info from data-* attributes
    var company = button.data('company'); // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#code').val(code);
    modal.find('#company').val(company);
  });
});
</script>
