<script>
    $(document).ready(function() {
        var i = 1;
        var pro_id = "<?php echo $pro_id; ?>";
        $("#prnt_of_chld1").multiselect({
            multiple: false,
            noneSelectedText: "Select Specification",
            selectedList: 1}).multiselectfilter({});
        $('#prnt_of_chld1').change(function() {
            $("#div2").hide();
            $("#prnt_of_chld2").prop('disabled', true);
            var id = $('#prnt_of_chld1').val();
            $('#parent_cat_id_per').val(id);
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "specifications/dropdwn_fill";
                $.ajax({
                    url: urls,
                    type: "POST",
                    data: {spid: id, p_id: pro_id},
                    dataType: 'json'
                }).done(function(data) {
                    if (data.length != 0) {
                        $("#div2").show();
                        $("#prnt_of_chld2").prop('disabled', false);
                        $("#prnt_of_chld2").empty();
                        $.each(data, function(i, val) {
                            $("#prnt_of_chld2").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                        $('#prnt_of_chld2').multiselect({
                            selectedList: 10,
                        }).multiselectfilter();
                        $("#prnt_of_chld2").multiselect('refresh');
                    }
                    else {
                        $("#div2").hide();
                        $("#prnt_of_chld2").prop('disabled', true);
                        alert('No Option Available For Selection');
                    }
                });
            }
            else {
                $("#div2").hide();
                $("#prnt_of_chld2").prop('disabled', true);
                $("#prnt_of_chld2").empty();

            }
        });
    });

    function del_row(i) {
        $("#label" + i).remove();
        $("#cat_hidden" + i).remove();
    }
</script> 

<style>
    .bg { background: aquamarine ;
          margin-left: 5px;
    }
    .mtp {
        margin-top:10px;
    }
    .margin_left_muti_filter{
        margin-left: 10px;
    }
    .bread{
        margin-bottom: 10px;
    }
    .breadcrumb>li+li:before {
        padding: 0 5px;
        color: #ccc;
        content: ">>\00a0" !important;
    }
</style>

<div class="ibox float-e-margins"> 
    <div id="form" action="#" class="wizard-big wizard clearfix" role="application" novalidate="novalidate">
        <div class="bread">
            <ol class="breadcrumb">
                <li>
                    <a href="#">Products</a>
                </li>

                <?php if (!empty($prod_dt['Category']['category_name'])) : ?>
                    <li class="active">
                        <strong><?php echo $prod_dt['Category']['category_name']; ?></strong>
                    </li>
                <?php endif; ?>
            </ol>
        </div>
        <div class="steps clearfix"><ul role="tablist"> 

                <li  class="first current" aria-disabled="false" aria-selected="true">
                    <a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'edit', $pro_id)) ?>">
                         Edit Product</a></li>
                <li  class="first current" style="width:200px;"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'edit_category', $pro_id)) ?>" > Edit Categories</a></li>                         
                <?php if (!empty($prod_dt['Product']['product_faq'])) { ?>
                    <li  class="first current" title="Active" style="width:200px;"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'edit_faq', $pro_id)) ?>" > Edit FAQ</a></li>
                <?php } else { ?>
                    <li  class="first current" title="Active" style="width:200px;"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'add_faq', $pro_id)) ?>" > Add FAQ</a></li>
                <?php } ?>
                <?php if ($spres == 1) { ?>
                    <li  class="first current" style="width:200px;"><a  href="" > Edit Specification <span class="badge badge-warning">Active</span> </a></li> 
                <?php } else { ?>
                    <li  class="first current" style="width:200px;"><a  href="" > Add Specification  <span class="badge badge-warning">Active</span></a></li> 
                <?php } ?>
                <?php if ($res == 1) { ?>
                    <li  class="first current" style="width:200px;"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'add_product_countries', $pro_id)) ?>" > Edit Countries</a></li>
                <?php } else { ?>
                    <li  class="first current" style="width:200px;"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'add_product_countries', $pro_id)) ?>" > Add Countries</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <!--    <div class="ibox-title">
            <h5> Add Specifications</h5>
        </div> -->
    <div class="ibox-content">
        <?php echo $this->Form->create('Specification', array('id' => 'add_product_specification')) ?>
        <div class="row">
            <div class="form-group">   
                <div class="col-lg-3" id="main_div">
                    <?php
                    echo $this->Form->input('specification_id', array(
                        'options' => $all_spec,
                        'class' => 'form-control hastip', 'size' => '10', 'id' => 'prnt_of_chld1', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Specification Name'));
                    ?>
                </div>
                <div class="col-lg-3 margin_left_muti_filter" id="div2" hidden>
                    <?php
                    echo $this->Form->input('specification_option_id', array(
                        'multiple' => 'multiple', 'class' => 'form-control hastip', 'id' => 'prnt_of_chld2', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Option Name'));
                    ?>
                </div>
                <div class="col-lg-3 margin_left_muti_filter">
                    <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-primary btn-sm', 'div' => false)); ?>
                </div>
            </div>
        </div>    
        <?php echo $this->Form->end(); ?>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Selected Specification </h5>
        </div>
        <div class="ibox-content"> 
            <table class="table table-bordered">
                <tr>
                    <th>Specifications</th>
                    <th>Options</th>
                </tr> 
                <?php foreach ($final_sel_array as $dkey => $final_spc) {
                    ?>
                    <tr>
                        <td>
                            <?php echo ucwords($final_spc['Specification']['specification_name']); ?>
                        </td>
                        <td>
                            <ul>
                                <?php foreach ($final_spc['SpecificationOption'] as $op): ?>
                                    <li>
                                        <?php echo ucwords($op['SpecificationOption']['option_name']); ?>
                                        <div class="pull-right">
                                            <?php echo $this->Form->postLink(__(''), array('controller' => 'product_specifications', 'action' => 'delete', $op['ProductSpecification']['id']), array('class' => 'btn btn-sm btn-danger fa fa-trash-o color-black tooltips', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Delete Option'), __('Are you sure you want to delete # %s?', $op['SpecificationOption']['option_name'])); ?>
                                        </div>
                                    </li>
                                    <br>
                                <?php endforeach; ?>
                            </ul>
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <div class="row">
                <div class="text-center">
                    <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'add_product_countries', $pro_id)) ?>" class="btn btn-sm btn-success">Next</a>      
                    <?php
                    if ($prod_dt['Product']['is_active'] == 0) {
                        echo $this->Html->link(__('Activate Product'), array('controller' => 'products', 'action' => 'active', $prod_dt['Product']['id'], 'pending_status'), array('class' => 'btn btn-sm btn-success', 'title' => 'Active Product'));
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

