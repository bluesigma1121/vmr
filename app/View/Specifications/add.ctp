<script type="text/javascript">
    $(document).ready(function() {
        $('#add_specification').bValidator();
    });
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Add Specification</h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('Specification', array('id' => 'add_specification', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-1">
            <div class="form-group"><label class="col-lg-4 control-label">Specification Name</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('specification_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Specification Name', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
        </div>
        <div class="row col-lg-offset-1">
            <div class="form-group"><label class="col-lg-4 control-label">Is Searchable </label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('is_searchable', array('class' => 'form-control', 'type' => 'select', 'options' => $yesno, 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
            <button onclick="goBack();" class="btn btn-sm btn-info">Back</button>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
