<script type="text/javascript">
    $(document).ready(function() {
        $('#Setting').bValidator();
    });
</script>
<style>
    .ml{
        margin-left: 15px;
    }
    .margin_l{
        margin-left: 18px;
    }
</style> 
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Edit Setting</h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('Setting', array('id' => 'add_user', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-2">
            <div  class="form-group">
                <label class="col-md-3 control-label">Price:</label>
                <div class="col-md-4">
                    <?php echo $this->Form->input('dollar_rate', array('type' => 'text', 'data-bvalidator' => 'required', 'class' => 'form-control', 'label' => false)); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Mail to Email:</label>
                <div class="col-md-4">
                    <?php echo $this->Form->input('mail_to', array('type' => 'text', 'data-bvalidator' => 'required', 'class' => 'form-control', 'label' => false)); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Secret Payment code:</label>
                <div class="col-md-4">
                    <?php echo $this->Form->input('secret_payment', array('type' => 'text', 'data-bvalidator' => 'required', 'class' => 'form-control', 'label' => false)); ?>
                </div>
            </div> 
            <div class="form-group">
                <label class="col-md-3 control-label">Emails Active:</label>
                <div class="col-md-4 margin_l">
                    <?php echo $this->Form->input('is_email_active', array('type' => 'checkbox', 'class' => '', 'label' => false)); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Emails:</label>
                <div class="col-md-4">
                    <?php echo $this->Form->input('emails', array('type' => 'textarea', 'data-bvalidator' => '', 'class' => 'form-control', 'placeholder' => 'Enter email with comma seperated', 'label' => false)); ?>
                </div>
            </div>
            <?php
            echo $this->Form->input('id');
            ?>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-primary btn-sm', 'div' => false)); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>