<script type="text/javascript">
    $(document).ready(function() {
        $('#Setting').bValidator();
    });
</script>
<style>
    .ml{
        margin-left: 15px;
    }
</style> 

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Set Setting</h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('Setting', array('id' => 'add_user', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-2">
            <div class="form-group">
                <label class="col-md-3 control-label">Price:</label>
                <div class="col-md-4">
                    <?php echo $this->Form->input('dollar_rate', array('type' => 'text', 'data-bvalidator' => '', 'class' => 'form-control', 'label' => false)); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Mail to Email:</label>
                <div class="col-md-4">
                    <?php echo $this->Form->input('mail_to', array('type' => 'text', 'data-bvalidator' => '', 'class' => 'form-control', 'label' => false)); ?>
                </div>
            </div>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-primary btn-sm', 'div' => false)); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>

