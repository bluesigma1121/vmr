<!-- <form action="<?php echo Configure::read("RAZOR_PAYMENT_URL"); ?>" method="POST"> -->
<?php echo $this->Form->create('customerData',['url'=>['controller'=>'enquiries','action'=>'verifyRazorpay', $data['enq_id']],'type'=>'post']); ?>
<script>
  $(window).on('load', function() {
    $('.razorpay-payment-button').click();
  });
</script>
<style>
    .razorpay-payment-button {
    display: none;
}
</style>
<script

    src="https://checkout.razorpay.com/v1/checkout.js"

    data-key="<?php Configure::load('idata'); echo Configure::read("idata.RAZOR_API_KEY"); ?>" // Enter the Key ID generated from the Dashboard

    data-amount="TOTAL_AMOUNT" // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise

    data-currency="<?php echo $data['currency']; ?>"

    data-order_id="<?php echo $orderId; ?>"//This is a sample Order ID. Pass the `id` obtained in the response of the previous step.

    //data-buttontext="Pay with Razorpay"

    data-name="<?php echo $data['billing_name']; ?>"

    data-description=""

    //data-prefill.name=""

    data-prefill.email="<?php echo $data['billing_email']; ?>"

    data-prefill.contact="<?php echo $data['delivery_tel']; ?>"

></script>

<!-- </form> -->
<?php echo $this->Form->end(); ?>