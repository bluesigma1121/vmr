<div class="countryRegions index">
	<h2><?php echo __('Country Regions'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('country_id'); ?></th>
			<th><?php echo $this->Paginator->sort('region_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($countryRegions as $countryRegion): ?>
	<tr>
		<td><?php echo h($countryRegion['CountryRegion']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($countryRegion['Country']['id'], array('controller' => 'countries', 'action' => 'view', $countryRegion['Country']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($countryRegion['Region']['id'], array('controller' => 'regions', 'action' => 'view', $countryRegion['Region']['id'])); ?>
		</td>
		<td><?php echo h($countryRegion['CountryRegion']['created']); ?>&nbsp;</td>
		<td><?php echo h($countryRegion['CountryRegion']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $countryRegion['CountryRegion']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $countryRegion['CountryRegion']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $countryRegion['CountryRegion']['id']), null, __('Are you sure you want to delete # %s?', $countryRegion['CountryRegion']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Country Region'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Regions'), array('controller' => 'regions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Region'), array('controller' => 'regions', 'action' => 'add')); ?> </li>
	</ul>
</div>
