<div class="countryRegions view">
<h2><?php echo __('Country Region'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($countryRegion['CountryRegion']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo $this->Html->link($countryRegion['Country']['id'], array('controller' => 'countries', 'action' => 'view', $countryRegion['Country']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Region'); ?></dt>
		<dd>
			<?php echo $this->Html->link($countryRegion['Region']['id'], array('controller' => 'regions', 'action' => 'view', $countryRegion['Region']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($countryRegion['CountryRegion']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($countryRegion['CountryRegion']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Country Region'), array('action' => 'edit', $countryRegion['CountryRegion']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Country Region'), array('action' => 'delete', $countryRegion['CountryRegion']['id']), null, __('Are you sure you want to delete # %s?', $countryRegion['CountryRegion']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Country Regions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country Region'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Regions'), array('controller' => 'regions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Region'), array('controller' => 'regions', 'action' => 'add')); ?> </li>
	</ul>
</div>
