<table  border="0" cellpadding="0" cellspacing="0" align="left" width="100%"> 
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Total Ready to Active Reports - <?= $cnt_ready_to_active ?><br/><br/>           
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
          Today's Activated Reports - <?= $cnt_active_today ?><br/><br/>
        </td>
    </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" align="left" style="border-bottom:1px solid #ddd;padding-bottom:10px" width="100%">
    <tr>
        <td  class="center" style="font-size: 18px; font-weight:600; color: #333; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            List of Ready to Active Reprots 
        </td>
    </tr>
  </table>
  <div style="width:100%; display:inline-block; margin-bottom:20px ">
    <ol>
      <?php foreach ($ready_to_active as $key => $value) { ?>
            <li class="center" style="border-bottom:1px solid #ddd; font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 10px; ">
                <?php echo $value; ?> 
            </li>
        <?php } ?>
      
    </ol>
  </div>

  <table border="0" cellpadding="0" cellspacing="0" align="left" style="border-bottom:1px solid #ddd;padding-bottom:10px" width="100%">
    <tr>
        <td  class="center" style="font-size: 18px; font-weight:600; color: #333; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            List of Today's Activated Reprots 
        </td>
    </tr>
  </table>
  <div style="width:100%; display:inline-block">
    <ol>
      <?php foreach ($active_today as $key => $value) { ?>
          <li class="center" style="border-bottom:1px solid #ddd; font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 10px; ">
              <?php echo $value; ?> 
          </li>
      <?php } ?>
      
    </ol>
  </div>