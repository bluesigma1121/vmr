<table  border="0" cellpadding="0" cellspacing="0" align="left">
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Dear <?php echo ucfirst($data['User']['first_name']); ?>,<br/><br/>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Thank you for connecting with <strong> Value Market Research. </strong> <br/><br/>     
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
        We at Value Market Research facilitate clients with the syndicate and customized research reports on 25+ industries with global and regional coverage. We have a vast repository of more than 7000+ reports with 500+ happy and satisfied clients.<br/><br/>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            We received your inquiry for <span style="font-style:italic;font-weight:bold"><?= $pro_name['Product']['product_name'] ?></span><br/><br/>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            <strong>We have shared your request with our research team and will soon share a detailed blueprint of the standard report.</strong> <br/><br/>     
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            In case you are looking for something more specific, it would be helpful if you could answer the below questions for us –<br/>
            <ul>
                <li>What is your exact requirement for this market?</li>
                <li>Are you looking for a global report or a region/country-specific report?</li>
                <li>How many users plan to use this study?</li>
                <li>By when do you need this study?</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
        These details will help us share a more relevant sample report with you.<br/><br/>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
        We wish you good health.<br/><br/>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
        Warm Regards,<br/>
        Nisha Naroliya| Business Development <br/>
        Value Market Research - Adding value to Business<br/>
        Tel: +1-888-294-1147<br/>
        Email ID: <a href="mailto:sales@valuemarketresearch.com">sales@valuemarketresearch.com</a><br/>
        Website: <a href="https://www.valuemarketresearch.com" target="_blank">www.valuemarketresearch.com</a> <br/><br/>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 12px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Disclaimer:
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 12px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            This email and any files transmitted with it are confidential and contain privileged or copyright information. You must not present this message to another party without gaining permission from the sender. If you are not the intended recipient you must not copy, print, distribute, publish or use this email or the information contained in it for any purpose other than to notify the sender. If you have received this message in error, please notify the sender.immediately, and delete this email from your system.<br/><br/>
        </td>
    </tr>
    <!-- <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
          We received your inquiry for  <?= $pro_name['Product']['product_name'] ?><br/><br/>
        </td>
    </tr> -->
    <!-- <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
           We will share the sample report shortly.
        </td>
    </tr> -->
</table>
