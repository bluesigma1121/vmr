<table  border="0" cellpadding="0" cellspacing="0" align="left"> 
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            <strong>New Lead Received for   <?= $product_name ?><br/><br/>           
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Lead ID : <?php echo $enq_id; ?> 
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            <strong> Name : </strong><?php echo ucfirst($data['User']['first_name']); ?>   <?php echo ucfirst($data['User']['last_name']); ?>   
        </td>
    </tr>
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            <strong> Email : </strong><?= $data['User']['email'] ?>
        </td>
    </tr>
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            <strong> Number :</strong> <?= $data['User']['mobile'] ?>
        </td>
    </tr>
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            <strong> Organisation :</strong> <?= $data['User']['organisation']; ?>
        </td>
    </tr>
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            <strong> Job Title : </strong><?= $data['User']['job_title']; ?>
        </td>
    </tr>
    
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            <strong> Country : </strong><?= $enq_data['Enquiry']['country']; ?>
        </td>
    </tr>

    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            <strong> Subject :</strong> <?= $enq_data['Enquiry']['subject'] ?>
        </td>
    </tr>
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            <strong> Message : </strong><?= $enq_data['Enquiry']['message'] ?>
        </td>
    </tr>

    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            <strong> Visited Ip :</strong> <?= $enq_data['Enquiry']['visited_ip'] ?>
        </td>
    </tr>
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            <strong> Ref Page : </strong><?= $enq_data['Enquiry']['ref_page'] ?>
        </td>
    </tr>
</table>

