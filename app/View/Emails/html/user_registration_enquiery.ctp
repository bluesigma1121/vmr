<table  border="0" cellpadding="0" cellspacing="0" align="left">
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Dear <?php echo ucfirst($data['User']['first_name']); ?>,<br/>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Welcome to Value Market Research,       
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Many thanks for registering with Value Market Research ,<br/><br/>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Your User Name : <?= $data['User']['email'] ?>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Your Password : <?= $v_code ?>
        </td>
    </tr>
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            You can login to our site using these details at the following link:
        </td>
    </tr>
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            <a  target="_blank" href="<?php echo Router::url('/', true) . 'users' . '/login'; ?>"> <?php echo Router::url('/', true) . 'users' . '/login'; ?>
            </a>
        </td>
    </tr>
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Thanking You !
        </td>
    </tr>
</table>
