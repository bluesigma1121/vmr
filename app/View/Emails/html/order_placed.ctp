<table  border="0" cellpadding="0" cellspacing="0" align="left">
    <tr>
        <td  class="center" style="font-size: 14px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Dear <?php echo ucfirst($order_details['Order']['ship_user_name']); ?>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 14px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Welcome to Value Market Research,       
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 14px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Thank you for placing an order with Value Market Research. Please find attached an invoice for your order. <br>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 14px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Orders for Electronic Copy Products - one business day once payment has been processed<br>
            Orders for Physical Delivery Products - 7-10 Business Days once payment has been processed <br>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 14px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            To check the status of your order please click on the following link: <br>
            <a  target="_blank" href="<?php echo Router::url('/', true) . 'orders' . '/user_order_index'; ?>"> My Orders<br>
            </a>
            <br>
        </td>
    </tr>
    <tr>
        <td   class="center" style="font-size: 14px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Thanks for the inquiry. Our team will get back to you soon.
        </td>
    </tr>
</table>
