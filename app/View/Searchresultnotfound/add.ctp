<div class="searchResultNotFound form">
<?php echo $this->Form->create('Searchresultnotfound'); ?>
	<fieldset>
		<legend><?php echo __('Add Search Result Not Found'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('email');
		echo $this->Form->input('description');
		echo $this->Form->input('created_on');
		echo $this->Form->input('visited_ip');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Search Result Not Found'), array('action' => 'index')); ?></li>
	</ul>
</div>
