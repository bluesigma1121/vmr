
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
          <h5><?php echo __('Users Specification(Search Result Not Found)'); ?></h5>
      </div>
      <div class="ibox-content">
        <div class="table-responsive mtp">
          <table class="table table-bordered table-striped">
			<thead>
				<tr>
						<th><?php echo $this->Paginator->sort('id'); ?></th>
						<th><?php echo $this->Paginator->sort('Name'); ?></th>
						<th><?php echo $this->Paginator->sort('Email'); ?></th>
						<th><?php echo $this->Paginator->sort('Description'); ?></th>
						<th><?php echo $this->Paginator->sort('Visited IP'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($searchresultnotfounds as $searchresultnotfound): ?>
				<tr>
					<td><?php echo h($searchresultnotfound['Searchresultnotfound']['id']); ?>&nbsp;</td>
					<td><?php echo h($searchresultnotfound['Searchresultnotfound']['name']); ?>&nbsp;</td>
					<td><?php echo h($searchresultnotfound['Searchresultnotfound']['email']); ?>&nbsp;</td>
					<td><?php echo h($searchresultnotfound['Searchresultnotfound']['description']); ?>&nbsp;</td>
					<td><?php echo h($searchresultnotfound['Searchresultnotfound']['visited_ip']); ?>&nbsp;</td>
					<td class="actions">
						
						<?php echo $this->Html->link(__('View'), array('action' => 'view', $searchresultnotfound['Searchresultnotfound']['id']), array('class' => 'btn btn-outline btn-sm btn-info dim','title' => 'View') ); ?>
						<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $searchresultnotfound['Searchresultnotfound']['id']), array('class' => 'btn btn-outline btn-sm btn-danger dim', 'title' => 'Delete'), array(), __('Are you sure you want to delete # %s?', $searchresultnotfound['Searchresultnotfound']['id']) ); ?>
						
					</td>
				</tr>
				<?php endforeach; ?>
		  	</tbody>
		  </table>
		  <div class="row col-md-12">
		  <div class="dataTables_paginate paging_bootstrap">
			  <p>
					  <?php
					  echo $this->Paginator->counter(array(
							  'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					  ));
						  ?>
			  </p>
			  <ul class="pagination" style="visibility: visible;">
					  <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
					  <li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
					  <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
			  </ul>
		  </div>
	  </div>
  </div>
</div>
</div>
</div>
</div>