<div class="row">
	<div class="col-md-12">
		<h1> VMR Reports By Category </h1>
	</div>
</div>

<div class="row">

	<div class="col-md-8 col-md-offset-2">
		<table class="table table-bordered table-condensed">
			<thead>
				<tr>
					<th class="text-center"> Category </th>
					<th> Count </th>
				</tr>
			</thead>
			<tbody>
				<? foreach($vmr_reports_by_category as $index => $vmr_reports_by_category) : ?>
				    <tr> 
				    	<? if(empty($vmr_reports_by_category['Category']['category_name'])): ?>
						    <td class="text-center"> - Not Set - </td>
						<? else: ?>
						    <td class="text-center"> <?= $vmr_reports_by_category['Category']['category_name'] ?> </td>
						<? endif; ?>
					    <td> <?= $vmr_reports_by_category[0]['category_count'] ?> </td>    
				    <tr>
				<? endforeach; ?>
			</tbody>
		</table>
	</div>

</div>
