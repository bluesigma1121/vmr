<div class="row">
	<div class="col-md-12">
		<h1> Reports By Publisher </h1>
	</div>
</div>

<div class="row">

	<div class="col-md-8 col-md-offset-2">
		<table class="table table-bordered table-condensed">
			<thead>
				<tr>
					<th class="text-center"> Publisher Name </th>
					<th> Count </th>
				</tr>
			</thead>
			<tbody>
				<? foreach($reports_by_publisher as $index => $reports_by_publisher) : ?>
				    <tr> 
				    	<? if(empty($reports_by_publisher['Product']['publisher_name'])): ?>
						    <td class="text-center"> - Not Set - </td>
						<? else: ?>
						    <td class="text-center"> <?= $reports_by_publisher['Product']['publisher_name'] ?> </td>
						<? endif; ?>
					    <td> <?= $reports_by_publisher[0]['publisher_count'] ?> </td>    
				    <tr>
				<? endforeach; ?>
			</tbody>
		</table>
	</div>

</div>
