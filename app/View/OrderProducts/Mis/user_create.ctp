<style>
    .mtp{
        margin-top: 10px;
    }
    .search_mtp{
        margin-top: 3px;
    }
</style> 

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>User Create</h5>
            </div>
            <div class="ibox-content">
                <form method="post">

	                <div class="table-responsive mtp">
	                    <table class="table table-bordered table-striped">
                            <tr>
                                <td> First Name </td>
                                <td> 
                                	<input type="text" name="data[User][first_name]" 
                                		value="<?= @$user['User']['name'] ?>" required /> 
                            	</td>
                            </tr>
                            <tr>
                                <td> Email </td>
                                <td> <input type="email" name="data[User][email]" required /> </td>
                            </tr>
                            <tr>
                                <td> Role </td>
                                <td>
                                	<select name="data[User][role]">
                                		<option value=""> -- Select Role -- </option>
                                		<? foreach($roles as $role_id => $role_name): ?>

	                                		<? if(@$user['User']['role'] == $role_id): ?>
		                                		<option value="<?= $role_id ?>" selected> 
		                                			<?= $role_name ?>
		                                		</option> 
		                                	<? else: ?>
		                                		<option value="<?= $role_id ?>"> 
		                                			<?= $role_name ?> 
			                                	</option>
			                                <? endif; ?>
			                            <? endforeach; ?>
                                	</select>
                            	</td>
                            </tr>
                            <tr>
                                <td> Country </td>
                                <td>
                                	<select name="data[User][country_id]">
                                		<option value=""> -- Select Country -- </option>
                                		<? foreach($countries as $id => $country_name): ?>
                                			<? if(@$user['User']['country_id'] == $id): ?>
		                                		<option value="<?= $id ?>" selected> 
		                                			<?= $country_name ?>   
		                                		</option>
		                                	<? else: ?>
		                                		<option value="<?= $id ?>"> 
		                                			<?= $country_name ?>   
		                                		</option>
			                                <? endif; ?>
	                                	<? endforeach ?>
                                	</select>
                            	</td>
                            </tr>
	                    </table>
	                </div>
	                <div>
	                	<input type="submit" value="Submit" />
	                </div>
            </div>
        </div>
    </div>
</div> 
