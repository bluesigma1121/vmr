<div class="row">
	<div class="col-md-12">
		<h1> Leads By Report </h1>
	</div>
</div>

<div class="row">

	<div class="col-md-8 col-md-offset-2">
		<table class="table table-bordered table-condensed">
			<thead>
				<tr>
					<th class="text-center"> Reports </th>
					<th> Publisher </th>
					<th> Leads </th>
				</tr>
			</thead>
			<tbody>
				<? foreach($leads_by_report as $index => $leads_by_report) : ?>
				    <tr> 
				    	<? if(empty($leads_by_report['Product']['alias'])): ?>
						    <td class="text-center"> - Not Set - </td>
						<? else: ?>
						    <td class="text-center"> <?= $leads_by_report['Product']
						    ['alias'] ?> </td>
						<? endif; ?>
						<td class="text-center"> <?= $leads_by_report['Product']
						    ['publisher_name'] ?> </td> 
					    <td> <?= $leads_by_report[0]['lead_count'] ?> </td>    
				    <tr>
				<? endforeach; ?>
			</tbody>
		</table>
	</div>

</div>
