<?php App::uses('CakeTime', 'Utility'); ?>

<rss xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
    <channel>
        <title>Latest Press Release of Value Market Research</title>
        <link><?php echo $this->Html->url('/',true); ?></link>
        <description>Value Market Research XML Feeds of Press Release</description>
        <language>en-us</language>
        <pubDate><?php echo (date('D, d M Y H:i:s')); ?></pubDate>
        <dc:date><?php echo $this->Time->toAtom(date('Y-m-d H:i:s')); ?></dc:date>
        <dc:language>en-us</dc:language>

        <?php foreach ($article as $key => $value) { ?>
            <item>
                <title><?=htmlspecialchars($value['Article']['headline']) ?></title>
                <link><?php echo (Router::url('/', true) . 'pressreleases/' . $value['Article']['slug']); ?></link>
                <description><?= (htmlspecialchars($value['Article']['description'])) ?></description>
                <content:encoded><?= (htmlspecialchars($value['Article']['description'])) ?></content:encoded>
                <category><?= htmlspecialchars($value['Category']['category_name']) ?></category>
                <pubDate><?php echo date('D, d M Y H:i:s', strtotime($value['Article']['modified'])); ?></pubDate>
                <guid><?php echo (Router::url('/', true) . 'pressreleases/' . $value['Article']['slug']); ?></guid>
                <dc:date><?php echo $this->Time->toAtom(date('Y-m-d H:i:s',strtotime($value['Article']['modified']))); ?></dc:date>
            </item>

        <?php } ?>

    </channel>
</rss>