<div class="container content">		
    <div class="container mar_top_bot">
        <div class="row">
            <ul class="pull-left breadcrumb">
                <li>
                    <a href="<?php echo Router::url('/', true) ?>"> Home </a>               
                </li>

                <li> 
                    <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'article_listing', 'section' => $article['Article']['article_type'])); ?>"><?php echo $heading ?></a>
                </li>
                <li><a href="<?php Router::url(array('controller' => 'articles', 'action' => 'article_details', 'folder' => $article['Article']['article_type'], 'article_id' => $article['Article']['id'], 'url_slug' => $this->Link->cleanString($article['Article']['slug']))); ?>"><?php echo $article['Article']['headline'] ?></a></li>
            </ul>
        </div>
    </div>

    <div class="row blog-page blog-item left-inner section-block">
        <!-- Left Sidebar -->
        <div class="col-md-12 md-margin-bottom-60">
            <!--Blog Post-->        
            <div class="blog margin-bottom-40">
                <h2><?php echo $article['Article']['headline']; ?></h2>
                <ul class="details list-inline">
                    <li id="first"><?php echo $heading ?></li>
                    <li class="spart_pd">October 2015</li>
                </ul>
                <br/>
                <?php echo $article['Article']['description']; ?>
                <hr/>
                <ul class="details list-inline">
                    <li><strong>Title : </strong><?php echo $article['Article']['meta_title']; ?></li>
                    <li><strong>Meta Desc : </strong><?php echo $article['Article']['meta_desc']; ?></li>
                    <li><strong>Meta Keywords : </strong><?php echo $article['Article']['meta_keywords']; ?></li>
                </ul>
            </div>
            <!--End Blog Post-->
            <hr>
        </div>
    </div><!--/row-->        
</div>