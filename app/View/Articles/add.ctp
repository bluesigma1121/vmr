<script>
    $(document).ready(function(){
       $('#ArticleAddForm').bValidator();
    });
</script>
<?php echo $this->Html->script('back/ckeditor/ckeditor.js'); ?>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <?php echo $this->Form->create('Article', array('class' => 'form-horizontal')); ?>
        <fieldset>
            <legend><?php echo __('Add Article'); ?></legend>
            <div class="row">
                <div class="form-group">
                  <label class="col-lg-3 control-label">Headline</label>
                    <div class="col-lg-8">
                        <?php echo $this->Form->input('headline',array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Headline', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group"><label class="col-lg-3 control-label">Article Type</label>
                    <div class="col-lg-8">
                        <?php echo $this->Form->input('article_type',array('class' => 'form-control', 'options'=>$article_type, 'placeholder' => 'Headline', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group"><label class="col-lg-3 control-label">Description</label>
                    <div class="col-lg-8">
                        <?php echo $this->Form->input('description',array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Description', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group"><label class="col-lg-3 control-label">Category</label>
                    <div class="col-lg-4">
                        <?php echo $this->Form->input('category_id',array('class' => 'form-control','options'=>$categories, 'placeholder' => 'Category', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group"><label class="col-lg-3 control-label">Product</label>
                    <div class="col-lg-8">
                        <?php echo $this->Form->input('product_id',array('class' => 'form-control select2','options'=>$products, 'placeholder' => 'Select Product', 'label' => false, 'empty'=> 'Select Product')); ?>
                    </div>
                </div>
                
                <div class="form-group"><label class="col-lg-3 control-label">Slug</label>
                    <div class="col-lg-8">
                        <?php echo $this->Form->input('slug',array('class' => 'ckeditor form-control', 'type' => 'text', 'placeholder' => 'Slug', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group"><label class="col-lg-3 control-label">Title</label>
                    <div class="col-lg-8">
                        <?php echo $this->Form->input('meta_title',array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Meta Name', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group"><label class="col-lg-3 control-label">Meta Desc</label>
                    <div class="col-lg-4">
                        <?php echo $this->Form->input('meta_desc',array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Meta Description', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
                <div class="form-group"><label class="col-lg-3 control-label">Keywords</label>
                    <div class="col-lg-8">
                        <?php echo $this->Form->input('meta_keywords',array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Meta Keyword', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
            </div>
        </fieldset>
        <?php echo $this->Form->end(__('Submit')); ?>
    </div>
</div>
