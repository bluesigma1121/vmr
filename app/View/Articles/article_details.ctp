<style>

.blog-details-inner ul {
    font-weight: normal;
    font-size: 14px;
    line-height: 26px;
    text-align: justify;
    transition: all .5s ease-in-out;
    color: #363636 !important;
    list-style: square;
}
.blog-details-inner strong{
    font-size: 14px;
    line-height: 26px;
    text-align: justify;
    transition: all .5s ease-in-out;
    color: #363636 !important;
    list-style: square;
}
.blog-inner a{
  color:#0074BD;
}

.item {
  display:initial;
  margin-left: 60px;
  /* display: grid;
  grid-template-rows: 1fr min-content;
  align-items: center;
  justify-content: center;
   height: 50vh; 
  flex-wrap: wrap; */
  /* background: var(--bg-color); */
}
#covid-19 {
  background: transparent;
  color: #fff;
  border: none;
  /* border-radius: 50px; */
  padding: 5px 10px;
  font-size: 16px;
  outline: none;
  cursor: pointer;
  position: relative;
  transition: 0.2s ease-in-out;
  /* letter-spacing: 2px; */
}

.button-rainbow #covid-19 {
  animation: rainbow 3s infinite, heartbeat 0.6s infinite;
  background: #c0392b;
}
.button-rainbow #covid-19 .rainbow {
  width: 120px;
  height: 120px;
  /* border-radius: 50%; */
  position: absolute;
  left: 0;
  right: 0;
  margin: auto;
  top: -40px;
  transition: 0.5s ease;
  opacity: 0;
  transform: rotate(0);
}


@keyframes rotate-nonstop {
  50% {
    transform: rotate(92deg);
  }
}
@keyframes omg-yes {
  50% {
    opacity: 1;
  }
  100% {
    top: -50px;
  }
}
@keyframes rainbow {
  0% {
    background: #1abc9c;
  }
  10% {
    background: #2ecc71;
  }
  20% {
    background: #3498db;
  }
  30% {
    background: #9b59b6;
  }
  40% {
    background: #e74c3c;
  }
  50% {
    background: #e67e22;
  }
  60% {
    background: #f1c40f;
  }
  70% {
    background: #2c3e50;
  }
  80% {
    background: #9b59b6;
  }
}

@keyframes heartbeat {
  50% {
    transform: scale(1.1);
  }
}
</style>


  
<style>
/*404 Error Page v1 
------------------------------------*/
.error-v1 {
	padding-bottom: 30px;
	text-align: center;	
}

.error-v1 p {
	color: #555;
	font-size: 16px;
}

.error-v1 span {
	color: #555;
	display: block;
	font-size: 35px;
	font-weight: 200;
}

.error-v1 span.error-v1-title {
	color: #777;	
	font-size: 180px;
	line-height: 200px;
	padding-bottom: 20px;
}

/*For Mobile Devices*/
@media (max-width: 500px) { 
	.error-v1 p {
		font-size: 12px;
	}	

	.error-v1 span {
		font-size: 25px;
	}

	.error-v1 span.error-v1-title {
		font-size: 140px;
	}
}
</style>

<!--BANNER SECTION START -->
<?php

$url=Router::url($this->here, true);
$url_arr=explode("/",$url);
//print_r($url_arr);

$value1=in_array('pressreleases',$url_arr);
if($value1 && $article['Article']['article_type'] == 'press-releases')
{
?>
<div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3><?php echo $article['Article']['headline'] ?></h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a href="<?php echo Router::url(array('controller' => 'articles', 'action' => $action )); ?>"><?php echo $heading ?></a></li>
              <li><a><?php echo $article['Article']['headline'] ?></a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->


<!-- ABOUT INNER SECTION START -->
<section class="about-inner">

      <div class="container">
        <!-- <h4>Blog Details</h4> -->
        <div class="col-md-9">
          <div class="blog-inner blog-details-inner">
   
            <h1><?php echo $article['Article']['headline']; ?></h1>
              <div class="pull-left post-on">
                <h6 class="">Posted On <?php echo date('F d, Y', strtotime($article['Article']['modified'])) ?> &nbsp;&nbsp;&nbsp;&nbsp;
                  <?php if(empty($product['Product']['slug'])) { ?>
                    <div class="item button-rainbow" style="--bg-color: #ecf0f1">
                      <button id="covid-19" class="covid-19-popup">COVID-19 Impact <div class="rainbow"></div></button>
                    </div> 
                  <?php } else { ?>
                    <div class="item button-rainbow" style="--bg-color: #ecf0f1">
                      <?php echo $this->Html->link('COVID-19 Impact' . $this->Html->tag('div','',array('class' => 'rainbow')), array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'covid-19-impact'), array("id" => "covid-19", "target" => "_blank",'escape' => false)
                      );
                      ?>
                    </div>
                  <?php } ?>

              </h6>
              </div>
              <p><?php echo $article['Article']['description']; ?></p>
              
          </div>
        </div>

        <div class="col-md-3">
          <div class="user">
            <div class="">
              <div class="btns chooseopt">
                <h4>Why Choose Us</h4>
                <div class="why-us-content">
                  <?php foreach ($whyus as $key => $why) { ?>
                      <div class="col-md-2 col-xs-2 col-sm-2">
                          <?php switch ($key) {
                            case 0:
                              echo '<i class="fa fa-user" aria-hidden="true"></i>';
                              break;
                            case 1:
                              echo '<i class="fa fa-certificate" aria-hidden="true"></i>';
                              break;
                            case 2:
                              echo '<i class="fa fa-handshake-o" aria-hidden="true"></i>';
                              break;
                            case 3:
                              echo '<i class="fa fa-envelope" aria-hidden="true"></i>';
                              break;
                          }?>
                      </div>
                      
                      <div class="col-md-10 col-xs-10 col-sm-10 whychoosedata">
                        <h5><?=$why['Whyus']['title']?></h5>
                        <!--<p>Get your queries resolved from an industry expert.</p>-->
                      </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>

          <div class="user">
            <div class="">
              <div class="btns chooseopt">
                <h4>Buy Chapters or Sections</h4>
                <div class="why-us-content buy-chap-content">
                  <div class="col-md-12 col-xs-12 col-sm-12">
                    <p>Avail customized purchase options to meet your exact research needs:</p>                  
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 buy-chap">
                    <ul>
                      <li>Buy sections of this report</li>
                      <li>Buy country level reports</li>
                      <li>Request for historical data</li>
                      <li>Request discounts available for Start-Ups & Universities</li>
                    </ul>
                  </div>              
                </div>
              </div>
            </div>
          </div>
          
          <div class="user">
              <div class="">
                <div class="btns chooseopt">
                  <h4>Clients</h4>
                  <div class="why-us-content">
                  <div class="container-fluid row">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="carousel carousel-showmanymoveone1 slide" id="itemslider">
                        <div class="carousel-inner carousel-inner-pay">
                          <?php foreach ($ourclients as $key => $clients) { ?>

                            <div class="item <?= $key == 0?'active':''?> payotn">
                              <div class="col-xs-12 col-sm-12 col-md-12">
                                <?php if (!empty($clients['OurClient']['logo'])) { ?>
                                    <!--<a href="http://<?php echo $clients['OurClient']['link']; ?>" target="blank">-->
                                      <?= $this->Html->image('client_images/' . $clients['OurClient']['logo'], array('alt' => 'Client Logos')); ?>
                                    <!--</a>-->
                                <?php } else {
                                        echo $this->Html->image('noimage.png', array('alt' => 'Client Logos'));
                                      } ?>
                              </div>
                            </div>
                          <?php } ?>
                        </div>

                        <div id="slider-control">
                        <a class="left carousel-control" href="#itemslider" data-slide="prev">
                          <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                        </a>
                        <a class="right carousel-control" href="#itemslider" data-slide="next">
                          <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
                        </a>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="user">
                <div class="">
                <div class="btns chooseopt">
                  <h4>Testimonials</h4>
                  <div class="why-us-content">
                  <div class="pricing-head">
                    <div id="myCarousel" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel">
                      <!-- Carousel items -->
                      <div class="carousel-inner">
                        <?php foreach ($testimonial as $key => $testimonials) { ?>
                          <div class="item <?= $key == 0?'active':''?>">
                              <div class="row">
                                  <div class="col-xs-12 col-sm-12 col-md-12 testimonial-text">
                                      <p> <i class="fa fa-quote-left fa-lg" aria-hidden="true"></i><?php echo " \" "; echo  $testimonials['Testimonial']['testimonial_description'] ;echo" \" "; ?></p>
                                      <h5><i class="fa fa-user-circle fa-lg" aria-hidden="true"></i> <?php echo $testimonials['Testimonial']['testimonial_title'];?></h5>
                                      <!-- <h6>- Position</h6> -->
                                  </div>
                              </div>
                              <!--/row-fluid-->
                            </div>
                          <?php } ?>
                          <!--/item-->
                      </div>
                    </div>
                    <!-- <div class="view-more-btn">
                      <a href="<?php echo Router::url(array('controller' => 'testimonials', 'action' => 'listing' )); ?>">View More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    </div> -->
                  </div>
                  </div>
                </div>
              </div>
            </div>

        </div>

      </div>
</section>

              <?php
              }
              elseif(in_array('analysis',$url_arr) && $article['Article']['article_type'] == 'analysis')
              {
                
                ?>
                 <!-- <h1><?php //echo $article['Article']['headline']; ?></h1>
              <div class="pull-left post-on">
                <h6 class="">Posted On <?php //echo date('F d, Y', strtotime($article['Article']['modified'])) ?> &nbsp;&nbsp;&nbsp;&nbsp;
                  <?php //if(empty($product['Product']['slug'])) { ?>
                    <div class="item button-rainbow" style="--bg-color: #ecf0f1">
                      <button id="covid-19" class="covid-19-popup">COVID-19 Impact <div class="rainbow"></div></button>
                    </div> 
                  <?php //} else { ?>
                    <div class="item button-rainbow" style="--bg-color: #ecf0f1">
                      <?php //echo $this->Html->link('COVID-19 Impact' . $this->Html->tag('div','',array('class' => 'rainbow')), array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'covid-19-impact'), array("id" => "covid-19", "target" => "_blank",'escape' => false)
                     // );
                      ?>
                    </div>
                  <?php //} ?>

              </h6>
              </div>
                 <p><?php //echo $article['Article']['description']; ?></p> -->
                 <div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3><?php echo $article['Article']['headline'] ?></h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a href="<?php echo Router::url(array('controller' => 'articles', 'action' => $action )); ?>"><?php echo $heading ?></a></li>
              <li><a><?php echo $article['Article']['headline'] ?></a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->


<!-- ABOUT INNER SECTION START -->
<section class="about-inner">

      <div class="container">
        <!-- <h4>Blog Details</h4> -->
        <div class="col-md-9">
          <div class="blog-inner blog-details-inner">
   
            <h1><?php echo $article['Article']['headline']; ?></h1>
              <div class="pull-left post-on">
                <h6 class="">Posted On <?php echo date('F d, Y', strtotime($article['Article']['modified'])) ?> &nbsp;&nbsp;&nbsp;&nbsp;
                  <?php if(empty($product['Product']['slug'])) { ?>
                    <div class="item button-rainbow" style="--bg-color: #ecf0f1">
                      <button id="covid-19" class="covid-19-popup">COVID-19 Impact <div class="rainbow"></div></button>
                    </div> 
                  <?php } else { ?>
                    <div class="item button-rainbow" style="--bg-color: #ecf0f1">
                      <?php echo $this->Html->link('COVID-19 Impact' . $this->Html->tag('div','',array('class' => 'rainbow')), array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'covid-19-impact'), array("id" => "covid-19", "target" => "_blank",'escape' => false)
                      );
                      ?>
                    </div>
                  <?php } ?>

              </h6>
              </div>
              <p><?php echo $article['Article']['description']; ?></p>
              
          </div>
        </div>

        <div class="col-md-3">
          <div class="user">
            <div class="">
              <div class="btns chooseopt">
                <h4>Why Choose Us</h4>
                <div class="why-us-content">
                  <?php foreach ($whyus as $key => $why) { ?>
                      <div class="col-md-2 col-xs-2 col-sm-2">
                          <?php switch ($key) {
                            case 0:
                              echo '<i class="fa fa-user" aria-hidden="true"></i>';
                              break;
                            case 1:
                              echo '<i class="fa fa-certificate" aria-hidden="true"></i>';
                              break;
                            case 2:
                              echo '<i class="fa fa-handshake-o" aria-hidden="true"></i>';
                              break;
                            case 3:
                              echo '<i class="fa fa-envelope" aria-hidden="true"></i>';
                              break;
                          }?>
                      </div>
                      
                      <div class="col-md-10 col-xs-10 col-sm-10 whychoosedata">
                        <h5><?=$why['Whyus']['title']?></h5>
                        <!--<p>Get your queries resolved from an industry expert.</p>-->
                      </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>

          <div class="user">
            <div class="">
              <div class="btns chooseopt">
                <h4>Buy Chapters or Sections</h4>
                <div class="why-us-content buy-chap-content">
                  <div class="col-md-12 col-xs-12 col-sm-12">
                    <p>Avail customized purchase options to meet your exact research needs:</p>                  
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 buy-chap">
                    <ul>
                      <li>Buy sections of this report</li>
                      <li>Buy country level reports</li>
                      <li>Request for historical data</li>
                      <li>Request discounts available for Start-Ups & Universities</li>
                    </ul>
                  </div>              
                </div>
              </div>
            </div>
          </div>
          
          <div class="user">
              <div class="">
                <div class="btns chooseopt">
                  <h4>Clients</h4>
                  <div class="why-us-content">
                  <div class="container-fluid row">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="carousel carousel-showmanymoveone1 slide" id="itemslider">
                        <div class="carousel-inner carousel-inner-pay">
                          <?php foreach ($ourclients as $key => $clients) { ?>

                            <div class="item <?= $key == 0?'active':''?> payotn">
                              <div class="col-xs-12 col-sm-12 col-md-12">
                                <?php if (!empty($clients['OurClient']['logo'])) { ?>
                                    <!--<a href="http://<?php echo $clients['OurClient']['link']; ?>" target="blank">-->
                                      <?= $this->Html->image('client_images/' . $clients['OurClient']['logo'], array('alt' => 'Client Logos')); ?>
                                    <!--</a>-->
                                <?php } else {
                                        echo $this->Html->image('noimage.png', array('alt' => 'Client Logos'));
                                      } ?>
                              </div>
                            </div>
                          <?php } ?>
                        </div>

                        <div id="slider-control">
                        <a class="left carousel-control" href="#itemslider" data-slide="prev">
                          <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                        </a>
                        <a class="right carousel-control" href="#itemslider" data-slide="next">
                          <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
                        </a>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="user">
                <div class="">
                <div class="btns chooseopt">
                  <h4>Testimonials</h4>
                  <div class="why-us-content">
                  <div class="pricing-head">
                    <div id="myCarousel" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel">
                      <!-- Carousel items -->
                      <div class="carousel-inner">
                        <?php foreach ($testimonial as $key => $testimonials) { ?>
                          <div class="item <?= $key == 0?'active':''?>">
                              <div class="row">
                                  <div class="col-xs-12 col-sm-12 col-md-12 testimonial-text">
                                      <p> <i class="fa fa-quote-left fa-lg" aria-hidden="true"></i><?php echo " \" "; echo  $testimonials['Testimonial']['testimonial_description'] ;echo" \" "; ?></p>
                                      <h5><i class="fa fa-user-circle fa-lg" aria-hidden="true"></i> <?php echo $testimonials['Testimonial']['testimonial_title'];?></h5>
                                      <!-- <h6>- Position</h6> -->
                                  </div>
                              </div>
                              <!--/row-fluid-->
                            </div>
                          <?php } ?>
                          <!--/item-->
                      </div>
                    </div>
                    <!-- <div class="view-more-btn">
                      <a href="<?php echo Router::url(array('controller' => 'testimonials', 'action' => 'listing' )); ?>">View More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    </div> -->
                  </div>
                  </div>
                </div>
              </div>
            </div>

        </div>

      </div>
</section>
                <?php
              }
              elseif(in_array('article',$url_arr) && $article['Article']['article_type'] == 'article')
              {
                ?>
                 <!-- <h1><?php// echo $article['Article']['headline']; ?></h1>
              <div class="pull-left post-on">
                <h6 class="">Posted On <?php //echo date('F d, Y', strtotime($article['Article']['modified'])) ?> &nbsp;&nbsp;&nbsp;&nbsp;
                  <?php //if(empty($product['Product']['slug'])) { ?>
                    <div class="item button-rainbow" style="--bg-color: #ecf0f1">
                      <button id="covid-19" class="covid-19-popup">COVID-19 Impact <div class="rainbow"></div></button>
                    </div> 
                  <?php //} else { ?>
                    <div class="item button-rainbow" style="--bg-color: #ecf0f1">
                      <?php //echo $this->Html->link('COVID-19 Impact' . $this->Html->tag('div','',array('class' => 'rainbow')), array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'covid-19-impact'), array("id" => "covid-19", "target" => "_blank",'escape' => false)
                      // );
                      ?>
                    </div>
                  <?php //} ?>

              </h6>
              </div>
                 <p><?php //echo $article['Article']['description']; ?></p> -->
                 <div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3><?php echo $article['Article']['headline'] ?></h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a href="<?php echo Router::url(array('controller' => 'articles', 'action' => $action )); ?>"><?php echo $heading ?></a></li>
              <li><a><?php echo $article['Article']['headline'] ?></a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->


<!-- ABOUT INNER SECTION START -->
<section class="about-inner">

      <div class="container">
        <!-- <h4>Blog Details</h4> -->
        <div class="col-md-9">
          <div class="blog-inner blog-details-inner">
   
            <h1><?php echo $article['Article']['headline']; ?></h1>
              <div class="pull-left post-on">
                <h6 class="">Posted On <?php echo date('F d, Y', strtotime($article['Article']['modified'])) ?> &nbsp;&nbsp;&nbsp;&nbsp;
                  <?php if(empty($product['Product']['slug'])) { ?>
                    <div class="item button-rainbow" style="--bg-color: #ecf0f1">
                      <button id="covid-19" class="covid-19-popup">COVID-19 Impact <div class="rainbow"></div></button>
                    </div> 
                  <?php } else { ?>
                    <div class="item button-rainbow" style="--bg-color: #ecf0f1">
                      <?php echo $this->Html->link('COVID-19 Impact' . $this->Html->tag('div','',array('class' => 'rainbow')), array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'slug' => $product['Product']['slug'], 'ref_page' => 'covid-19-impact'), array("id" => "covid-19", "target" => "_blank",'escape' => false)
                      );
                      ?>
                    </div>
                  <?php } ?>

              </h6>
              </div>
              <p><?php echo $article['Article']['description']; ?></p>
              
          </div>
        </div>

        <div class="col-md-3">
          <div class="user">
            <div class="">
              <div class="btns chooseopt">
                <h4>Why Choose Us</h4>
                <div class="why-us-content">
                  <?php foreach ($whyus as $key => $why) { ?>
                      <div class="col-md-2 col-xs-2 col-sm-2">
                          <?php switch ($key) {
                            case 0:
                              echo '<i class="fa fa-user" aria-hidden="true"></i>';
                              break;
                            case 1:
                              echo '<i class="fa fa-certificate" aria-hidden="true"></i>';
                              break;
                            case 2:
                              echo '<i class="fa fa-handshake-o" aria-hidden="true"></i>';
                              break;
                            case 3:
                              echo '<i class="fa fa-envelope" aria-hidden="true"></i>';
                              break;
                          }?>
                      </div>
                      
                      <div class="col-md-10 col-xs-10 col-sm-10 whychoosedata">
                        <h5><?=$why['Whyus']['title']?></h5>
                        <!--<p>Get your queries resolved from an industry expert.</p>-->
                      </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>

          <div class="user">
            <div class="">
              <div class="btns chooseopt">
                <h4>Buy Chapters or Sections</h4>
                <div class="why-us-content buy-chap-content">
                  <div class="col-md-12 col-xs-12 col-sm-12">
                    <p>Avail customized purchase options to meet your exact research needs:</p>                  
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 buy-chap">
                    <ul>
                      <li>Buy sections of this report</li>
                      <li>Buy country level reports</li>
                      <li>Request for historical data</li>
                      <li>Request discounts available for Start-Ups & Universities</li>
                    </ul>
                  </div>              
                </div>
              </div>
            </div>
          </div>
          
          <div class="user">
              <div class="">
                <div class="btns chooseopt">
                  <h4>Clients</h4>
                  <div class="why-us-content">
                  <div class="container-fluid row">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="carousel carousel-showmanymoveone1 slide" id="itemslider">
                        <div class="carousel-inner carousel-inner-pay">
                          <?php foreach ($ourclients as $key => $clients) { ?>

                            <div class="item <?= $key == 0?'active':''?> payotn">
                              <div class="col-xs-12 col-sm-12 col-md-12">
                                <?php if (!empty($clients['OurClient']['logo'])) { ?>
                                    <!--<a href="http://<?php echo $clients['OurClient']['link']; ?>" target="blank">-->
                                      <?= $this->Html->image('client_images/' . $clients['OurClient']['logo'], array('alt' => 'Client Logos')); ?>
                                    <!--</a>-->
                                <?php } else {
                                        echo $this->Html->image('noimage.png', array('alt' => 'Client Logos'));
                                      } ?>
                              </div>
                            </div>
                          <?php } ?>
                        </div>

                        <div id="slider-control">
                        <a class="left carousel-control" href="#itemslider" data-slide="prev">
                          <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                        </a>
                        <a class="right carousel-control" href="#itemslider" data-slide="next">
                          <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
                        </a>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="user">
                <div class="">
                <div class="btns chooseopt">
                  <h4>Testimonials</h4>
                  <div class="why-us-content">
                  <div class="pricing-head">
                    <div id="myCarousel" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel">
                      <!-- Carousel items -->
                      <div class="carousel-inner">
                        <?php foreach ($testimonial as $key => $testimonials) { ?>
                          <div class="item <?= $key == 0?'active':''?>">
                              <div class="row">
                                  <div class="col-xs-12 col-sm-12 col-md-12 testimonial-text">
                                      <p> <i class="fa fa-quote-left fa-lg" aria-hidden="true"></i><?php echo " \" "; echo  $testimonials['Testimonial']['testimonial_description'] ;echo" \" "; ?></p>
                                      <h5><i class="fa fa-user-circle fa-lg" aria-hidden="true"></i> <?php echo $testimonials['Testimonial']['testimonial_title'];?></h5>
                                      <!-- <h6>- Position</h6> -->
                                  </div>
                              </div>
                              <!--/row-fluid-->
                            </div>
                          <?php } ?>
                          <!--/item-->
                      </div>
                    </div>
                    <!-- <div class="view-more-btn">
                      <a href="<?php echo Router::url(array('controller' => 'testimonials', 'action' => 'listing' )); ?>">View More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    </div> -->
                  </div>
                  </div>
                </div>
              </div>
            </div>

        </div>

      </div>
</section>
                 <?php
              }
              else
              {








                
                //echo "<h2>No Aricle</h2>";
                // header("HTTP/1.0 404 Not Found");
                // echo "<h1>404 Not Found</h1>";
                // echo "The page that you have requested could not be found.";
               // exit();
              ?>
                
<!--BANNER SECTION END -->


<!-- ABOUT INNER SECTION START -->
<!-- <section class="about-inner">

      <div class="container">
      </div>
</section> -->


<div class="container content">		
    <!--Error Block-->
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="error-v1 ">
                <span class="error-v1-title"></span>
                <span>Ooopss... </span>
                <p class="text-center">The content you are looking for seems to be moved away. Kindly write to us at <a href="mailto:sales@valuemarketresearch.com">sales@valuemarketresearch.com</a> or continue browsing...<a href="/vmr/">click here</a> </p>
				<br>
				<div class="enq-btn">
                <a class="btn-u btn-u-green" href="/vmr/"><i class="fa fa-home fa-lg" aria-hidden="true"></i> Visit Home Page</a>
				</div>
            </div>
        </div>
    </div>
    <!--End Error Block-->
</div> 
              <?php }
              ?>
<!-- ABOUT INNER SECTION END -->




<!-- New Code For Covid-19 Popup Start VG-22/04/2020 -->
<style>
  .blur-it {
    filter: blur(4px);
  }
  .modal-wrapper {
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0; 
    left: 0;
    background: rgba(0, 0, 0, 0.6);
    visibility: hidden;
    opacity: 0;
    transition: all 0.25s ease-in-out;
    z-index:9999;
  }

  .modal-wrapper.open {
    opacity: 1;
    visibility: visible;
  }

  .covid-popup .modal {
    width: 600px;
    height: auto;
    display: block;
    margin: 50% 0 0 -300px;
    position: relative;
    top: 42%; 
    left: 50%;
    background: #fff;
    opacity: 0;
    transition: all 0.5s ease-in-out;
    border-radius: 10px;
  }

  .modal-wrapper.open .modal {
    margin-top: -200px;
    opacity: 1;
  }

  .covid-popup .head { 
      width: 100%;
      height: 44px;
      padding: 0px 10px;
      overflow: hidden;
      background: #3498db;
  }
  .covid-popup .head h4 {
      float: left;
      color: #fff;
      font-size: 22px;
      letter-spacing: 1px;
      font-weight: 500;
  }

  .covid-popup .btn-close {
    font-size: 28px;
    display: block;
    float: right;
    color: #fff;
  }
  .covid-popup .btn-close:hover {
      color:#fff;
  }

  .modal-wrapper.covid-popup .modal .content {
      padding: 5%;
      /* background: url(../img/covid.png); */
      background-position: top left;
      background-repeat: no-repeat;
      background-size: cover;
      box-shadow: 10000px 0 10000px rgba(255,255,255,0.8) inset;
  }
  .covid-popup .good-job  {
    text-align: justify;
  }
  .covid-popup .good-job p {
      letter-spacing: 1px;
      line-height: 26px;
  }
</style>
<div class="modal-wrapper covid-popup">
  <div class="modal">
    <div class="head">
        <h4><i class="fa fa-info-circle" aria-hidden="true"></i> Important: Covid19 pandemic market impact</h4>
        <a class="btn-close trigger" href="#">
            <i class="fa fa-times" aria-hidden="true"></i>
        </a>
    </div>
    <div class="content">
        <div class="good-job">
          <p>
          The world is currently experiencing a huge challenge which has affected all the industries and markets. Few markets have experienced a high growth in their demand while others have faced a downfall. The extent of growth or drop in the market statistics of different industries by COVID 19 plays an important role in how the future of businesses will shape up in the near future. We at Value Market Research provide you with an impact analysis of COVID 19 on this market with its latest update in the purchased version of the report.
          </p>
        </div>
    </div>
  </div>
</div>


<script>
$( document ).ready(function() {
  $('.covid-19-popup').click(function(e) {
    $('.modal-wrapper').addClass('open');
    $('.page-wrapper').addClass('blur-it');
  });
  $('.btn-close').click(function(e) {
      $('.modal-wrapper').removeClass('open');
      $('.page-wrapper').removeClass('blur-it'); 
  });
});
</script>


<!-- New Code For Covid-19 Popup End VG-22/04/2020 -->