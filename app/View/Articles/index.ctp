<div class="articles index">
    <div class="row">
        <div class="col-md-10">
            <h2><?php echo __('Articles'); ?></h2>
        </div>
        <div class="col-md-2 pull-right">
            <?php echo $this->Html->link('Add Article',array('action'=>'add'),array('class'=>'btn btn-outline btn-sm btn-warning dim')) ?>
            <a href="<?= Router::url(array('controller' => 'pressreleases/feeds','ext'=>'xml')); ?>" class='btn btn-outline btn-sm btn-primary dim' target="_blank" download> RSS Feeds</a>
            <!-- <?php echo $this->Html->link('RSS Feeds',array('action'=>'rssfeeds'),array('class'=>'btn btn-outline btn-sm btn-primary dim')) ?> -->
        </div>
    </div>
    <input type="text" id="search" style="float:right;margin-bottom:10px;width:250px;" placeholder="Search" class="form-control">
    <table class="table table-bordered table-striped table-condensed table-hover dataTable js-basic-example" id="example">
        <tr>
            <th><?php echo $this->Paginator->sort('headline'); ?></th>
            <th><?php echo $this->Paginator->sort('article_type'); ?></th>
            <th><?php echo $this->Paginator->sort('category_id'); ?></th>            
            <th><?php echo $this->Paginator->sort('meta_title'); ?></th>
            <th><?php echo $this->Paginator->sort('meta_desc'); ?></th>
            <th><?php echo $this->Paginator->sort('meta_keywords'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        <?php foreach ($articles as $article): ?>
            <tr>
                <td><?php echo h($article['Article']['headline']); ?>&nbsp;</td>
                <td><?php echo h($article['Article']['article_type']); ?>&nbsp;</td>
                <td>
                    <?php echo $this->Html->link($article['Category']['category_name'], array('controller' => 'categories', 'action' => 'view', $article['Category']['id'])); ?>
                </td>
                <td><?php echo h($article['Article']['meta_title']); ?>&nbsp;</td>
                <td><?php echo h($article['Article']['meta_desc']); ?>&nbsp;</td>
                <td><?php echo h($article['Article']['meta_keywords']); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link(__('View'), array('action' => 'view', $article['Article']['id']), array('class' => 'btn btn-outline btn-sm btn-info dim tooltip-f')); ?>
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $article['Article']['id']), array('class' => 'btn btn-outline btn-sm btn-warning dim tooltip-f')); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $article['Article']['id']), array('class' => 'btn btn-outline btn-sm btn-danger dim tooltip-f'), __('Are you sure you want to delete # %s?', $article['Article']['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('New Article'), array('action' => 'add')); ?></li>
        <li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
    </ul>
</div>
<script>  
      $(document).ready(function(){  
           $('#search').keyup(function(){  
                search_table($(this).val());  
           });  
           function search_table(value){  
                $('#example tr').each(function(){  
                     var found = 'false';  
                     $(this).each(function(){  
                          if($(this).text().toLowerCase().indexOf(value.toLowerCase()) >= 0)  
                          {  
                               found = 'true';  
                          }  
                     });  
                     if(found == 'true')  
                     {  
                          $(this).show();  
                     }  
                     else  
                     {  
                          $(this).hide();  
                     }  
                });  
           }  
      });  
 </script>  
