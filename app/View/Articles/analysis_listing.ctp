<!--BANNER SECTION START -->
<div class="banner">
  <div class="bg-color bg-color-inner-main">
    <div class="container">
      <div class="row">
        <!--<div class="col-md-12 col-xs-12 col-lg-12 inner-title">-->
        <!--  <h3><?php echo $heading ?></h3>-->
        <!--</div>-->
        <div class="col-md-12 col-xs-12 col-lg-12">
           <ol class="breadcrumb">
              <li><a href="<?php echo Router::url('/', true) ?>">Home</a></li>
              <li><a><?php echo $heading ?></a></li>
           </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<!--BANNER SECTION END -->

<style>.blog-inner h2 {font-weight: normal;font-size: 18px;letter-spacing: 0;margin-top: 0;color: #337ab7;text-transform: capitalize;font-family: Roboto;}.page-heading{color: #666;font-size: 14px;text-transform: uppercase;font-weight: 700;}
</style>
<!-- ABOUT INNER SECTION START -->
<section class="about-inner">

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="page-heading">Analysis</h1>
        <div class="msg1">
            <p>This section highlights the latest development and identifies ongoing latest research in different industries.</p>
        </div>
      </div>
    </div>
    <div class="col-md-9">
      <?php foreach ($articles as $article): ?>
        <div class="blog-inner">
          <a href="<?php echo Router::url(array('controller'=>'articles','action'=>'article_details','type' => 'analysis','url_slug'=>$this->Link->cleanString($article['Article']['slug']))); ?>">
          <h2><?php echo $article['Article']['headline'] ?></h2>
            <?php
            echo $this->Text->truncate(
                    strip_tags($article['Article']['description']), 400, array(
                'ellipsis' => '...',
                'exact' => false
                    )
            );
            ?>
          </a>
          <div class="clearfix">
          </div>
          <div class="pull-left col-md-6 col-xs-12 col-sm-6 row">
              <h6 class="">Posted On <?php echo date('d-F-Y', strtotime($article['Article']['modified'])) ?></h6>
          </div>

        </div>
      <?php endforeach; ?>
      
      <?php if(empty($articles)) { ?>
        <h2>No Data Found!</h2>
      <?php } ?>
    </div>

    <div class="col-md-3">
      <div class="user">
        <div class="">
          <div class="btns chooseopt">
            <h4>Why Choose Us</h4>
            <div class="why-us-content">
              <?php foreach ($whyus as $key => $why) { ?>
                  <div class="col-md-2 col-xs-2 col-sm-2">
                      <?php switch ($key) {
                        case 0:
                          echo '<i class="fa fa-user" aria-hidden="true"></i>';
                          break;
                        case 1:
                          echo '<i class="fa fa-certificate" aria-hidden="true"></i>';
                          break;
                        case 2:
                          echo '<i class="fa fa-handshake-o" aria-hidden="true"></i>';
                          break;
                        case 3:
                          echo '<i class="fa fa-envelope" aria-hidden="true"></i>';
                          break;
                      }?>
                  </div>
                  
                  <div class="col-md-10 col-xs-10 col-sm-10 whychoosedata">
                    <h5><?=$why['Whyus']['title']?></h5>
                    <!--<p>Get your queries resolved from an industry expert.</p>-->
                  </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>

      <div class="user">
        <div class="">
          <div class="btns chooseopt">
            <h4>Buy Chapters or Sections</h4>
            <div class="why-us-content buy-chap-content">
              <div class="col-md-12 col-xs-12 col-sm-12">
                <p>Avail customized purchase options to meet your exact research needs:</p>                  
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 buy-chap">
                <ul>
                  <li>Buy sections of this report</li>
                  <li>Buy country level reports</li>
                  <li>Request for historical data</li>
                  <li>Request discounts available for Start-Ups & Universities</li>
                </ul>
              </div>              
            </div>
          </div>
        </div>
      </div>
      
      <div class="user">
              <div class="">
                <div class="btns chooseopt">
                  <h4>Clients</h4>
                  <div class="why-us-content">
                  <div class="container-fluid row">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="carousel carousel-showmanymoveone1 slide" id="itemslider">
                        <div class="carousel-inner carousel-inner-pay">
                          <?php foreach ($ourclients as $key => $clients) { ?>

                            <div class="item <?= $key == 0?'active':''?> payotn">
                              <div class="col-xs-12 col-sm-12 col-md-12">
                                <?php if (!empty($clients['OurClient']['logo'])) { ?>
                                    <!--<a href="http://<?php echo $clients['OurClient']['link']; ?>" target="blank">-->
                                      <?= $this->Html->image('client_images/' . $clients['OurClient']['logo'], array('alt' => 'Client Logos')); ?>
                                    <!--</a>-->
                                <?php } else {
                                        echo $this->Html->image('noimage.png', array('alt' => 'Client Logos'));
                                      } ?>
                              </div>
                            </div>
                          <?php } ?>
                        </div>

                        <div id="slider-control">
                        <a class="left carousel-control" href="#itemslider" data-slide="prev">
                          <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                        </a>
                        <a class="right carousel-control" href="#itemslider" data-slide="next">
                          <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
                        </a>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="user">
                <div class="">
                <div class="btns chooseopt">
                  <h4>Testimonials</h4>
                  <div class="why-us-content">
                  <div class="pricing-head">
                    <div id="myCarousel" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel">
                      <!-- Carousel items -->
                      <div class="carousel-inner">
                        <?php foreach ($testimonial as $key => $testimonials) { ?>
                          <div class="item <?= $key == 0?'active':''?>">
                              <div class="row">
                                  <div class="col-xs-12 col-sm-12 col-md-12 testimonial-text">
                                      <p> <i class="fa fa-quote-left fa-lg" aria-hidden="true"></i><?php echo " \" "; echo  $testimonials['Testimonial']['testimonial_description'] ;echo" \" "; ?></p>
                                      <h5><i class="fa fa-user-circle fa-lg" aria-hidden="true"></i> <?php echo $testimonials['Testimonial']['testimonial_title'];?></h5>
                                      <!-- <h6>- Position</h6> -->
                                  </div>
                              </div>
                              <!--/row-fluid-->
                            </div>
                          <?php } ?>
                          <!--/item-->
                      </div>
                    </div>
                    <!-- <div class="view-more-btn">
                      <a href="<?php echo Router::url(array('controller' => 'testimonials', 'action' => 'listing' )); ?>">View More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    </div> -->
                  </div>
                  </div>
                </div>
              </div>
            </div>

    </div>

  </div>
</section>

<!-- ABOUT INNER SECTION END -->
