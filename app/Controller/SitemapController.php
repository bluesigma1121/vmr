<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class SitemapController extends AppController {

    public $uses = array();

    /**
     * Components
     *
     * @var array
     */
    public $components = array('RequestHandler');

    /**
     * index method
     *
     * @return void
     */
    public function generate() {
        $this->uses = array('Category','Product', 'Article');

        $categories = $this->Category->find('all', array(
            'fields'        => array('id','slug', 'modified'),
            'conditions'    => array('Category.is_active' => 1,'Category.parent_category_id !=' => 0),           
            'recursive'     => -1 
           )
        );

        $products = $this->Product->find('all', array(
            'fields'        => array('id','slug', 'modified'),
            'conditions'    => array('Product.is_active' => 1),           
            'recursive'     => -1 
           )
        );
     
        $pressrelease = $this->Article->find('all', array(
            'fields'        => array('id','slug', 'modified'),  
            'conditions'    => array('Article.article_type' => 'press-releases'),  
            'recursive'     => -1 
           )
        );
        $analysis = $this->Article->find('all', array(
            'fields'        => array('id','slug', 'modified'),   
            'conditions'    => array('Article.article_type' => 'analysis'),   
            'recursive'     => -1 
           )
        );

        $data['categories'] = $categories;        
        $data['products'] = $products;
        $data['pressrelease'] = $pressrelease;
        $data['analysis'] = $analysis;
     
        $this->set($data);
        $this-> render();
    }

    public function beforeFilter() {
        $this->Auth->allow(array('generate'));
    }
}    