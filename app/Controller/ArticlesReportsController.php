<?php

App::uses('AppController', 'Controller');

/**
 * ArticlesReports Controller
 *
 * @property ArticlesReport $ArticlesReport
 * @property PaginatorComponent $Paginator
 */
class ArticlesReportsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->layout = 'admin_layout';
        $this->ArticlesReport->recursive = 0;
        $this->set('articlesReports', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->ArticlesReport->exists($id)) {
            throw new NotFoundException(__('Invalid articles report'));
        }
        $options = array('conditions' => array('ArticlesReport.' . $this->ArticlesReport->primaryKey => $id));
        $this->set('articlesReport', $this->ArticlesReport->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $this->ArticlesReport->create();
            if ($this->ArticlesReport->save($this->request->data)) {
                $this->Session->setFlash(__('The articles report has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The articles report could not be saved. Please, try again.'));
            }
        }
        $articles = $this->ArticlesReport->Article->find('list',array('fields'=>array('Article.id','Article.headline')));
        $products = $this->ArticlesReport->Product->find('list',array('fields'=>array('Product.id','Product.product_name')));
        $this->set(compact('articles', 'products'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->ArticlesReport->exists($id)) {
            throw new NotFoundException(__('Invalid articles report'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->ArticlesReport->save($this->request->data)) {
                $this->Session->setFlash(__('The articles report has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The articles report could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('ArticlesReport.' . $this->ArticlesReport->primaryKey => $id));
            $this->request->data = $this->ArticlesReport->find('first', $options);
        }
        $articles = $this->ArticlesReport->Article->find('list');
        $products = $this->ArticlesReport->Product->find('list');
        $this->set(compact('articles', 'products'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->ArticlesReport->id = $id;
        if (!$this->ArticlesReport->exists()) {
            throw new NotFoundException(__('Invalid articles report'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->ArticlesReport->delete()) {
            $this->Session->setFlash(__('The articles report has been deleted.'));
        } else {
            $this->Session->setFlash(__('The articles report could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
