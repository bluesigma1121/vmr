<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $helpers = array("Html", "Form", "Session");
    public $components = array(
        'Session',
        'Cookie',
        'Auth' => array(
            'authenticate' => array(
                'Form' => array(
                    'userModel' => 'User',
                    'fields' => array(
                        'username' => 'email',
                        'password' => 'password'
                    )
                )
            ),
            'authorize' => array('Controller'),
            'loginRedirect' => array('controller' => 'users', 'action' => 'index'),
            'logoutRedirect' => array('/'),
        //'loginAction' => array('controller' => 'users', 'action' => 'login'),
        )
    );

    const ROLE_SUPER_ADMIN = 1;
    const ROLE_USER = 11;
    const ROLE_OPERATOR = 2;
    const ROLE_SEO = 3;
    const ROLE_CONTENT = 4;
    const ROLE_JR_ANALYST = 5;
    const ROLE_SALES_TEAM = 12; //code added VG-13/03/2017

    public function isAuthorized() {
        if (!AuthComponent::user())
            return false;
        $role = AuthComponent::user('role');
        $url = $this->request->url;
        $controller = $this->request->controller;
        $action = $this->request->action;
        $is_authorized = false;
        if ($role == 1) {
            Configure::load('superadmin_roles');
            $role_data = Configure::read("roles");
        } elseif ($role == 2) {
            Configure::load('operator_roles');
            $role_data = Configure::read("roles");
        } elseif ($role == 3) {
            Configure::load('seo_roles');
            $role_data = Configure::read("roles");
        } elseif ($role == 4) {
            Configure::load('content_roles');
            $role_data = Configure::read("roles");
        } elseif ($role == 11) {
            Configure::load('user_roles');
            $role_data = Configure::read("roles");
        }elseif ($role == 5) {
            Configure::load('jr_analyst_role');
            $role_data = Configure::read("roles");
        }elseif ($role == 12) {  //code added VG-13/03/2017 start
          Configure::load('salesteam_roles');
          $role_data = Configure::read("roles"); //code added VG-13/03/2017 End
        }else {
            $this->Session->setFlash(__('You are not authorized to perform this actions. Please contact to the administrator'), 'error');
            return false;
        }
        switch (true) {
            case $url == false;
                $is_authorized = true;
                break;
            case $role == self::ROLE_SUPER_ADMIN && in_array($action, $role_data[$controller]):
                $is_authorized = true;
                break;
            case $role == self::ROLE_USER && in_array($action, $role_data[$controller]):
                $is_authorized = true;
                break;
            case $role == self::ROLE_OPERATOR && in_array($action, $role_data[$controller]):
                $is_authorized = true;
                break;
            case $role == self::ROLE_SEO && in_array($action, $role_data[$controller]):
                $is_authorized = true;
                break;
            case $role == self::ROLE_CONTENT && in_array($action, $role_data[$controller]):
                $is_authorized = true;
                break;
            case $role == self::ROLE_JR_ANALYST && in_array($action, $role_data[$controller]):
                $is_authorized = true;
                break;
            //code added VG-13/03/2017 Start
            case $role == self::ROLE_SALES_TEAM && in_array($action, $role_data[$controller]):
                $is_authorized = true;
                break;
            //code added VG-13/03/2017 End
    
            case $controller == 'pages';
                $is_authorized = true;
                break;
        }
        if ($is_authorized) {
            return true;
        } else {
            $this->Session->setFlash(__('You are not authorized to perform this actions.'), 'error');
            return false;
        }
    }

    public function beforeFilter() {
        parent::beforeFilter();
        $this->loadModel('User');
        $device = $this->Session->read('device');
        if (empty($device)) {
            $this->User->detect_device();
            $device = $this->Session->read('device');
        }
    }

    public function beforeRender() {

	if (APPLICATION_ENV == 'prod') {
            if ($this->name == 'CakeError') {

                CakeLog::write('activity', 'A special message for activity logging');
                $this->Session->setFlash("404: Opps we didn't find what you are looking for..", 'error');
                $this->redirect(array('controller' => 'users', 'action' => 'page_not_found'));
            }
        }

        $device = $this->Session->read('device');
        $this->loadModel('User');
        if (empty($device)) {
            $this->User->detect_device();
            $device = $this->Session->read('device');
        }
        $ip_data = array();
        //debug($device);
        if ($device == "desktop") {
            $ip_data['SiteVisit']['medium'] = 1;
        } else {
            $ip_data['SiteVisit']['medium'] = 2;
        }
        $role = AuthComponent::user('role');
        if ($role > 5 || $role == '') {
            //$ip = $_SERVER['REMOTE_ADDR'];
            //$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            @$ip_array = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip = trim($ip_array[count($ip_array) - 1]);
            //$ip = "49.248.181.70";
            //$details = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip));
            $ip_data['SiteVisit']['ip'] = $ip;
            //$ip_data['SiteVisit']['city'] = $details['geoplugin_city'];
            //$ip_data['SiteVisit']['state'] = iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $details['geoplugin_region']);
            //$ip_data['SiteVisit']['country'] = $details['geoplugin_countryName'];

            $this->loadModel('SiteVisit');
            $this->SiteVisit->save($ip_data);
        }

        $currency = Configure::read('idata.curr');
        $limits = Configure::read('idata.limits');
        $this->loadModel('Category');
        $main_category = $this->Category->find('list', array(
             /* Footer Main category List code Start VG-2/11/2017 | Removed Market Reports Category from list */
           // 'conditions' => array('Category.id' => array(2, 292, 560, 555), 'Category.is_active' => 1),
           
            'conditions' => array('Category.id' => array(292, 560, 555), 'Category.is_active' => 1),
            /* Footer Main category List code Ends VG-2/11/2017*/
            'fields' => array('Category.id', 'Category.category_name')));
        $this->set(compact('main_category'));
        $cart_count = 0;
        /*if (AuthComponent::user('id')) {
            $this->loadModel('CartItem');
            $cart_count = $this->CartItem->find('count', array('conditions' => array('CartItem.user_id' => AuthComponent::user('id'))));
        } else {*/
            if ($this->Session->check('cart_product')) {
                $session_data = $this->Session->read('cart_product');
                static $cnt = 0;
                if (!empty($session_data)) {
                    foreach ($session_data as $key => $prod) {
                        if (!empty($prod)) {
                            foreach ($prod as $key => $record) {
                                $cnt++;
                            }
                        }
                    }
                }
                $cart_count = $cnt;
            }
        //}
        $cur_curency = 1;
        if ($this->Session->check('currency')) {
            $cur_curency = $this->Session->read('currency');
        }else{
            $this->Session->write('currency',$cur_curency);
        }
        
        $dollar_rate = 1;
        if ($cur_curency == 0) {
            $this->loadModel('Setting');
            $dollar_rate = $this->Setting->find('first', array('conditions' => array('Setting.id' => 1), 'fields' => ('Setting.dollar_rate'), 'recursive' => -1));
            $this->Session->write('dollar_rate', $dollar_rate['Setting']['dollar_rate']);
            $dollar_rate = $dollar_rate['Setting']['dollar_rate'];
        }
        $this->loadModel('User');
        $list_operator = $this->User->find('list', array('conditions' => array('User.role' => 2), 'fields' => array('User.id', 'User.name')));
        $switch_roles = Configure::read('idata.switch_roles');
        $this->set(compact('cart_count', 'cur_curency', 'switch_roles', 'limits', 'currency', 'list_operator', 'dollar_rate'));
    }

    public function appError($error) {
	if (APPLICATION_ENV == 'prod') {
            $final_message = date('Y-m-d H:i:s') . ' - ' . $this->request->clientIp() . ' - ' . $error->getMessage();
            CakeLog::write('activity', $final_message);
            $this->Session->setFlash("404 : Sorry we didn't find what you are looking for..", 'error');
            $this->redirect(array('controller' => 'users', 'action' => 'page_not_found'));
        }
        else {

        	 $this->redirect(array('controller' => 'users', 'action' => 'page_not_found'));
             // parent::appError($error);
         }
     }

}
