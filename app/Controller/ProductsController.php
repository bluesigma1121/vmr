<?php

App::uses('AppController', 'Controller');
App::uses('Spreadsheet_Excel_Reader', 'Vendor');
App::uses('CakeEmail', 'Network/Email');

/**
 * Products Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 */
class ProductsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    public function transport_index() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $this->Session->write('product_filter', $this->request->data);
        } elseif ($this->Session->check('product_filter')) {
            $this->request->data = $this->Session->read('product_filter');
        } else {
            $this->request->data['Product']['search_text'] = '';
            $this->request->data['Product']['price_search'] = '';
            $this->request->data['Product']['product_status'] = '';
        }

        $conditions_price = array();
        $conditions_name = array();
        $conditions_status = array();
        $conditions_cat = array();
        if ($this->Session->check('product_filter')) {
            $data = $this->Session->read('product_filter');

            if (isset($data['Product']['price_search']) && !empty($data['Product']['price_search'])) {
                $conditions_price["Product.price"] = $data['Product']['price_search'];
            }
            if (isset($data['Product']['search_text']) && !empty($data['Product']['search_text'])) {
                $conditions_name["Product.product_name  LIKE"] = '%' . $data['Product']['search_text'] . '%';
            }
            if (isset($data['Product']['product_status']) && ($data['Product']['product_status'] != '')) {
                $conditions_status["Product.is_active"] = $data['Product']['product_status'];
            }
            if (isset($data['Product']['is_on_home']) && ($data['Product']['is_on_home'] != '')) {
                $conditions_status["Product.is_hide"] = $data['Product']['is_on_home'];
            }

            if (isset($data['Product']['parent_category_id6']) && ($data['Product']['parent_category_id6'] != '')) {
                $cat_id = $data['Product']['parent_category_id6'];
            } elseif (isset($data['Product']['parent_category_id5']) && ($data['Product']['parent_category_id5'] != '')) {
                $cat_id = $data['Product']['parent_category_id5'];
            } elseif (isset($data['Product']['parent_category_id4']) && ($data['Product']['parent_category_id4'] != '')) {
                $cat_id = $data['Product']['parent_category_id4'];
            } elseif (isset($data['Product']['parent_category_id3']) && ($data['Product']['parent_category_id3'] != '')) {
                $cat_id = $data['Product']['parent_category_id3'];
            } elseif (isset($data['Product']['parent_category_id2']) && ($data['Product']['parent_category_id2'] != '')) {
                $cat_id = $data['Product']['parent_category_id2'];
            } elseif (isset($data['Product']['parent_category_id1']) && ($data['Product']['parent_category_id1'] != '')) {
                $cat_id = $data['Product']['parent_category_id1'];
            }
            // debug($cat_id);
            if (!empty($cat_id)) {
                $this->loadModel('ProductCategory');
                $pro_id = $this->ProductCategory->find('list', array('conditions' => array('ProductCategory.category_id' => $cat_id), 'fields' => array('product_id', 'product_id'), 'recursive' => 0));
                $conditions_cat["Product.id"] = $pro_id;
            }
            //debug($pro_id);die;
        }
        $role = $this->Auth->user('role');
        $pro_ignore = '';
        if($role != '1'){
            $pro_ignore['Product.id !='] = 1989;
        }
        $this->paginate = array(
            'conditions' => array($conditions_price, $conditions_name, $conditions_status, $conditions_cat,$pro_ignore),
            'order' => 'Product.id DESC',
            'limit' => 50
        );
        //    $this->Product->recursive = 0;
        $this->loadModel('Category');
        $cat = $this->Category->find('list', array('conditions' => array('Category.level' => 1),
            'fields' => array('category_name'), 'recursive' => -1));
        //debug($cat);die;
        $products = $this->Paginator->paginate();
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $product_search = Configure::read('idata.product_search');
        $product_status = Configure::read('idata.product_status');
        Configure::load('idata');
        $toc = Configure::read('idata.toc');
        $this->set(compact('products', 'product_status', 'yes_no', 'toc', 'cat', 'product_search'));
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->layout = 'admin_layout';
        
        if ($this->request->is('post')) {
            $this->Session->write('product_filter', $this->request->data);
        } elseif ($this->Session->check('product_filter')) {
            $this->request->data = $this->Session->read('product_filter');
        } else {
            $this->request->data['Product']['search_text'] = '';
            $this->request->data['Product']['price_search'] = '';
            $this->request->data['Product']['product_status'] = '';
        }

        $conditions_price = array();
        $conditions_name = array();
        $conditions_status = array();
        $conditions_cat = array();
        if ($this->Session->check('product_filter')) {
            $data = $this->Session->read('product_filter');

            if (isset($data['Product']['price_search']) && !empty($data['Product']['price_search'])) {
                $conditions_price["Product.price"] = $data['Product']['price_search'];
            }
            if (isset($data['Product']['search_text']) && !empty($data['Product']['search_text'])) {
                $conditions_name["Product.product_name  LIKE"] = '%' . $data['Product']['search_text'] . '%';
            }
            if (isset($data['Product']['product_status']) && ($data['Product']['product_status'] != '')) {
                $conditions_status["Product.is_active"] = $data['Product']['product_status'];
            }
            if (isset($data['Product']['is_on_home']) && ($data['Product']['is_on_home'] != '')) {
                $conditions_status["Product.is_hide"] = $data['Product']['is_on_home'];
            }

            if (isset($data['Product']['parent_category_id6']) && ($data['Product']['parent_category_id6'] != '')) {
                $cat_id = $data['Product']['parent_category_id6'];
            } elseif (isset($data['Product']['parent_category_id5']) && ($data['Product']['parent_category_id5'] != '')) {
                $cat_id = $data['Product']['parent_category_id5'];
            } elseif (isset($data['Product']['parent_category_id4']) && ($data['Product']['parent_category_id4'] != '')) {
                $cat_id = $data['Product']['parent_category_id4'];
            } elseif (isset($data['Product']['parent_category_id3']) && ($data['Product']['parent_category_id3'] != '')) {
                $cat_id = $data['Product']['parent_category_id3'];
            } elseif (isset($data['Product']['parent_category_id2']) && ($data['Product']['parent_category_id2'] != '')) {
                $cat_id = $data['Product']['parent_category_id2'];
            } elseif (isset($data['Product']['parent_category_id1']) && ($data['Product']['parent_category_id1'] != '')) {
                $cat_id = $data['Product']['parent_category_id1'];
            }
            // debug($cat_id);
            if (!empty($cat_id)) {
                $this->loadModel('ProductCategory');
                $pro_id = $this->ProductCategory->find('list', array('conditions' => array('ProductCategory.category_id' => $cat_id), 'fields' => array('product_id', 'product_id'), 'recursive' => 0));
                $conditions_cat["Product.id"] = $pro_id;
            }
            //debug($pro_id);die;
        }
        $role = $this->Auth->user('role');
        $pro_ignore = '';
        if($role != '1'){
            $pro_ignore['Product.id !='] = 1989;
        }
        $this->paginate = array(
            'conditions' => array($conditions_price, $conditions_name, $conditions_status, $conditions_cat,$pro_ignore),
            'order' => 'Product.id DESC',
            'limit' => 50
        );
        //    $this->Product->recursive = 0;
        $this->loadModel('Category');
        $cat = $this->Category->find('list', array('conditions' => array('Category.level' => 1),
            'fields' => array('category_name'), 'recursive' => -1));
        //debug($cat);die;
        $products = $this->Paginator->paginate();
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $product_search = Configure::read('idata.product_search');
        $product_status = Configure::read('idata.product_status');
        Configure::load('idata');
        $toc = Configure::read('idata.toc');
        $this->set(compact('products', 'product_status', 'yes_no', 'toc', 'cat', 'product_search'));
    }

    public function jr_analyst_index() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $this->Session->write('product_filter', $this->request->data);
        } elseif ($this->Session->check('product_filter')) {
            $this->request->data = $this->Session->read('product_filter');
        } else {
            $this->request->data['Product']['search_text'] = '';
            $this->request->data['Product']['price_search'] = '';
            $this->request->data['Product']['product_status'] = '';
        }
        $conditions_price = array();
        $conditions_name = array();
        $conditions_status = array();
        $conditions_cat = array();
        if ($this->Session->check('product_filter')) {
            $data = $this->Session->read('product_filter');

            if (isset($data['Product']['price_search']) && !empty($data['Product']['price_search'])) {
                $conditions_price["Product.price"] = $data['Product']['price_search'];
            }
            if (isset($data['Product']['search_text']) && !empty($data['Product']['search_text'])) {
                $conditions_name["Product.product_name  LIKE"] = '%' . $data['Product']['search_text'] . '%';
            }
            if (isset($data['Product']['product_status']) && ($data['Product']['product_status'] != '')) {
                $conditions_status["Product.is_active"] = $data['Product']['product_status'];
            }

            if (isset($data['Product']['parent_category_id6']) && ($data['Product']['parent_category_id6'] != '')) {
                $cat_id = $data['Product']['parent_category_id6'];
            } elseif (isset($data['Product']['parent_category_id5']) && ($data['Product']['parent_category_id5'] != '')) {
                $cat_id = $data['Product']['parent_category_id5'];
            } elseif (isset($data['Product']['parent_category_id4']) && ($data['Product']['parent_category_id4'] != '')) {
                $cat_id = $data['Product']['parent_category_id4'];
            } elseif (isset($data['Product']['parent_category_id3']) && ($data['Product']['parent_category_id3'] != '')) {
                $cat_id = $data['Product']['parent_category_id3'];
            } elseif (isset($data['Product']['parent_category_id2']) && ($data['Product']['parent_category_id2'] != '')) {
                $cat_id = $data['Product']['parent_category_id2'];
            } elseif (isset($data['Product']['parent_category_id1']) && ($data['Product']['parent_category_id1'] != '')) {
                $cat_id = $data['Product']['parent_category_id1'];
            }
            // debug($cat_id);
            if (!empty($cat_id)) {
                $this->loadModel('ProductCategory');
                $pro_id = $this->ProductCategory->find('list', array(
                    'conditions' => array('ProductCategory.category_id' => $cat_id),
                    'fields' => array('product_id', 'product_id'), 'recursive' => 0));
                $conditions_cat["Product.id"] = $pro_id;
            }
            //debug($pro_id);die;
        }
        $conditions_cat["Product.is_active"] = array(0, 2);
        $role = $this->Auth->user('role');
        $pro_ignore = '';
        if($role != '1'){
            $pro_ignore['Product.id !='] = 1989;
        }
        $this->paginate = array(
            'conditions' => array($conditions_price, $conditions_name, $conditions_status, $conditions_cat,$pro_ignore),
            'order' => 'Product.id DESC',
            'limit' => 50
        );
        //    $this->Product->recursive = 0;
        $this->loadModel('Category');
        $cat = $this->Category->find('list', array('conditions' => array('Category.level' => 1),
            'fields' => array('category_name'), 'recursive' => -1));
        //debug($cat);die;
        $products = $this->Paginator->paginate();
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $product_search = Configure::read('idata.product_search');
        $product_status = Configure::read('idata.product_status');
        Configure::load('idata');
        $toc = Configure::read('idata.toc');
        $this->set(compact('products', 'product_status', 'yes_no', 'toc', 'cat', 'product_search'));
    }

    public function seo_index() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $this->Session->write('product_filter', $this->request->data);
        } elseif ($this->Session->check('product_filter')) {
            $this->request->data = $this->Session->read('product_filter');
        } else {
            $this->request->data['Product']['search_text'] = '';
            $this->request->data['Product']['price_search'] = '';
            $this->request->data['Product']['product_status'] = '';
        }
        $conditions_price = array();
        $conditions_name = array();
        $conditions_status = array();
        if ($this->Session->check('product_filter')) {
            $data = $this->Session->read('product_filter');

            if (isset($data['Product']['price_search']) && !empty($data['Product']['price_search'])) {
                $conditions_price["Product.price"] = $data['Product']['price_search'];
            }
            if (isset($data['Product']['search_text']) && !empty($data['Product']['search_text'])) {
                $conditions_name["Product.product_name  LIKE"] = '%' . $data['Product']['search_text'] . '%';
            }
            if (isset($data['Product']['product_status']) && ($data['Product']['product_status'] != '')) {
                $conditions_status["Product.is_active"] = $data['Product']['product_status'];
            }
        }
        $role = $this->Auth->user('role');
        $pro_ignore = '';
        if($role != '1'){
            $pro_ignore['Product.id !='] = 1989;
        }
        $this->paginate = array(
            'conditions' => array($conditions_price, $conditions_name, $conditions_status,$pro_ignore),
            'order' => 'Product.id DESC',
            'limit' => 50
        );
        //    $this->Product->recursive = 0;
        $products = $this->Paginator->paginate();
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $product_search = Configure::read('idata.product_search');
        $product_status = Configure::read('idata.product_status');
        $this->set(compact('products', 'product_status', 'yes_no', 'product_search'));
    }

    public function pending_status() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {

            $this->Session->write('product_filter_pending', $this->request->data);
        } elseif ($this->Session->check('product_filter_pending')) {
            $this->request->data = $this->Session->read('product_filter_pending');
        } else {
            $this->request->data['Product']['search_text'] = '';
            $this->request->data['Product']['price_search'] = '';
            $this->request->data['Product']['product_status'] = '';
            $this->request->data['Product']['parent_category_id1'] = '';
        }

        $conditions_price = array();
        $conditions_name = array();
        $conditions_status = array();
        $conditions_product = array();
        if ($this->Session->check('product_filter_pending')) {

            $data = $this->Session->read('product_filter_pending');
            // debug($data);
            if (isset($data['Product']['price_search']) && !empty($data['Product']['price_search'])) {
                $conditions_price["Product.price"] = $data['Product']['price_search'];
            }
            if (isset($data['Product']['search_text']) && !empty($data['Product']['search_text'])) {
                $conditions_name["Product.product_name  LIKE"] = '%' . $data['Product']['search_text'] . '%';
            }
            if (isset($data['Product']['product_status']) && ($data['Product']['product_status'] != '')) {
                $conditions_status["Product.is_active"] = $data['Product']['product_status'];
            }
            if (isset($data['Product']['parent_category_id1']) && ($data['Product']['parent_category_id1'] != '')) {
                $conditions_product["Product.category_id"] = $data['Product']['parent_category_id1'];
            }
        } else {
            $conditions_status["Product.is_active"] = 0;
        }
        $conditions_status["Product.is_active"] = 0;
        // debug($condition_product);die;
        $role = $this->Auth->user('role');
        $pro_ignore = '';
        if($role != '1'){
            $pro_ignore['Product.id !='] = 1989;
        }
        $this->paginate = array(
            'conditions' => array($conditions_price, $conditions_name, $conditions_status, $conditions_product,$pro_ignore),
            'order' => 'Product.created DESC',
            'limit' => 50
        );
        // debug($this->paginate);die;
        //    $this->Product->recursive = 0;
        $this->loadModel('Category');
        $cat = $this->Category->find('list', array('conditions' => array('Category.level' => 1),
            'fields' => array('category_name'), 'recursive' => -1));

        $products = $this->Paginator->paginate();
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $product_search = Configure::read('idata.product_search');
        $product_status = Configure::read('idata.product_status');
        $this->set(compact('products', 'product_status', 'yes_no', 'cat', 'product_search'));
    }

    //Newly added function for ready to active products.
    public function products_ready_active() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {

            $this->Session->write('product_filter_pending', $this->request->data);
        } elseif ($this->Session->check('product_filter_pending')) {
            $this->request->data = $this->Session->read('product_filter_pending');
        } else {
            $this->request->data['Product']['search_text'] = '';
            $this->request->data['Product']['price_search'] = '';
            $this->request->data['Product']['product_status'] = '';
            $this->request->data['Product']['parent_category_id1'] = '';
        }

        $conditions_price = array();
        $conditions_name = array();
        $conditions_status = array();
        $conditions_product = array();
        if ($this->Session->check('product_filter_pending')) {

            $data = $this->Session->read('product_filter_pending');
            // debug($data);
            if (isset($data['Product']['price_search']) && !empty($data['Product']['price_search'])) {
                $conditions_price["Product.price"] = $data['Product']['price_search'];
            }
            if (isset($data['Product']['search_text']) && !empty($data['Product']['search_text'])) {
                $conditions_name["Product.product_name  LIKE"] = '%' . $data['Product']['search_text'] . '%';
            }
            if (isset($data['Product']['product_status']) && ($data['Product']['product_status'] != '')) {
                $conditions_status["Product.is_active"] = $data['Product']['product_status'];
            }
            if (isset($data['Product']['parent_category_id1']) && ($data['Product']['parent_category_id1'] != '')) {
                $conditions_product["Product.category_id"] = $data['Product']['parent_category_id1'];
            }
        } else {
            $conditions_status["Product.is_active"] = 2;
            //$conditions_status["Product.is_active"] = 3;
        }
        $conditions_status["Product.is_active"] = 2;
        //$conditions_status["Product.is_active"] = 3;
        // debug($condition_product);die;
        $role = $this->Auth->user('role');
        $pro_ignore = '';
        if($role != '1'){
            $pro_ignore['Product.id !='] = 1989;
        }
        $this->paginate = array(
            'conditions' => array($conditions_price, $conditions_name, $conditions_status, $conditions_product,$pro_ignore),
            'fields' => array('Product.id', 'Product.product_name', 'Product.publisher_name','Product.price','Product.is_active', 'Product.slug', 'Product.product_description'),
            'order' => 'Product.created DESC',
            'limit' => 50
        );
        // debug($this->paginate);die;
        //    $this->Product->recursive = 0;
        $this->loadModel('Category');
        $cat = $this->Category->find('list', array('conditions' => array('Category.level' => 1),
            'fields' => array('category_name'), 'recursive' => -1));

        $products = $this->Paginator->paginate();
        //debug($products);
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $product_search = Configure::read('idata.product_search');
        $product_status = Configure::read('idata.product_status');
        $this->set(compact('products', 'product_status', 'yes_no', 'cat', 'product_search'));
    }

    public function clear_product_filter() {
        if ($this->Session->check('product_filter')) {
            $this->Session->delete('product_filter');
        }
        //return $this->redirect(array('controller' => 'users', 'action' => 'index'));
        return $this->redirect($this->referer());
    }

    public function clear_product_filter_pending() {
        if ($this->Session->check('product_filter_pending')) {
            $this->Session->delete('product_filter_pending');
        }
        //return $this->redirect(array('controller' => 'users', 'action' => 'index'));
        return $this->redirect($this->referer());
    }

    //    public function product_upload_fails($sts = null) {
//        if ($this->Session->check('product_fails')) {
//            $product_fails = $this->Session->read('product_fails');
//            $this->Session->delete('product_fails');
//            if ($sts == 1) {
//                $this->Session->setFlash(__('Product Added Successfully '), 'success');
//             //   return $this->redirect(array('action' => 'index'));
//            } elseif ($sts == 0) {
//                $this->Session->setFlash(__('Error in Product Upload'), 'error');
//              //  return $this->redirect(array('action' => 'index'));
//            } elseif ($sts == 2) {
//                $this->Session->setFlash(__('Error in  Product Id Generation '), 'error');
//               // return $this->redirect(array('action' => 'index'));
//            }
//            $this->set(compact('product_fails'));
//        }
//    }

    public function upload_products_excel() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($this->request->data['Product']['product_upload']['name'])) {
                $this->Session->setFlash(__('Please Select Excel File '), 'error');
                return $this->redirect(array('action' => 'upload_products_excel'));
            }
            $sts = $this->Product->upload_product_excel($data);
            $product_fails = $this->Session->read('product_fails');
            if ($this->Session->check('product_fails')) {
                $product_fails = $this->Session->read('product_fails');
                $this->Session->delete('product_fails');
                $this->set(compact('product_fails'));
                $this->render('product_upload_fails');
            }
            if ($sts == 0) {
                $this->Session->setFlash(__('Product Added Successfully '), 'success');
                return $this->redirect(array('action' => 'pending_status'));
            } elseif ($sts == 1) {
                $this->Session->setFlash(__('Error in Product Upload'), 'error');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Some Weird Error'), 'error');
                return $this->redirect(array('action' => 'index'));
            }
        }
//        Configure::load('idata');
//        $main_categories = Configure::read('idata.main_category');
        $this->loadModel('Category');
        $main_categories = $this->Category->find('list', array(
            'fields' => array('id', 'category_name'),
            'conditions' => array('Category.parent_category_id' => 0))
        );

        $this->set(compact('main_categories'));
    }

    public function upload_company_reports() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($this->request->data['Product']['product_upload']['name'])) {
                $this->Session->setFlash(__('Please Select Excel File '), 'error');
                return $this->redirect(array('action' => 'upload_products_excel'));
            }
            $sts = $this->Product->upload_company_reports($data);
            $this->redirect(array('controller' => 'products', 'action' => 'index'));
        }
    }

    public function add_to_cart() {

    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->Product->exists($id)) {
            throw new NotFoundException(__('Invalid product'));
        }

        $this->Product->bindModel(
                array('hasMany' => array(
                        'ProductCountry' => array(
                            'className' => 'ProductCountry'
                        )
                    )
                )
        );


        $options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
        $product = $this->Product->find('first', $options);
        //debug($product);
        $category_ids =  array();
        foreach ($product['ProductCategory'] as $key => $pc) {
            $category_ids[$key] = $pc['category_id'];
        }
        $product_categories = $this->Product->Category->find('list', array(
            'fields' => array('id', 'category_name'),
            'conditions' => array('Category.id' => $category_ids, 'Category.id !=' => $product['Product']['category_id'])
        ));
        //debug($product_categories);
        $country_ids = array();
        foreach ($product['ProductCountry'] as $key => $pro_country) {
            $country_ids[$key] = $pro_country['country_id'];
        }
        $this->loadModel('Country');
        $product_countries = $this->Country->find('list', array(
            'fields' => array('id', 'country_name'),
            'conditions' => array('Country.id' => $country_ids)
        ));

        $this->loadModel('CountryRegion');

        $this->loadModel('CountryRegion');
        $country_ids = array();
        $country_regions = $this->CountryRegion->find('list', array(
            'fields' => array('CountryRegion.region_id', 'CountryRegion.region_id'),
            'conditions' => array('CountryRegion.country_id' => $country_ids)));

        $this->loadModel('Region');
        $regions = $this->Region->find('list', array(
            'fields' => array('Region.id', 'Region.region_name'),
            'conditions' => array('Region.id' => $country_regions)));

        $product_specs = $this->Product->ProductSpecification->find('all', array(
            'conditions' => array('ProductSpecification.product_id' => $id),
            'fields' => array(
                'Specification.id', 'Specification.specification_name',
                'SpecificationOption.id', 'SpecificationOption.option_name',
                'Product.id'
            ),
            'order' => array('Specification.id')
        ));
        $product_specifications = array();
        $i = 0;

        foreach ($product_specs as $key => $ps) {
            $product_specifications[$ps['Specification']['specification_name']][$i++] = $ps['SpecificationOption']['option_name'];
        }
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $product_status = Configure::read('idata.product_status');

        $this->set(compact('product', 'product_categories', 'yes_no', 'product_countries', 'regions', 'product_specifications', 'product_status'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $res = $this->Product->save_product_info($this->request->data);            
            if ($res == 999) {
                $this->Session->setFlash(__('The product title exist. Please, try again.'), 'error');
            } else if ($res != 0 && $res != 999) {
                //debug($this->request->data);die;                
                $this->Session->setFlash(__('The product has been saved.'), 'success');
                // return $this->redirect(array('action' => 'add_faq', $res));  
                return $this->redirect(array('action' => 'add_category', $res));                             
            } else {
                $this->Session->setFlash(__('The product could not be saved. Please, try again.'));
            }
        }
        $this->loadModel('Category');
        $category = $this->Category->find('list', array('conditions' => array('Category.parent_category_id' => 0), 'fields' => array('Category.id', 'Category.category_name')));
        Configure::load('idata');
        $months = Configure::read('idata.months');
        Configure::load('idata');
        $toc = Configure::read('idata.toc');
        $this->set(compact('category', 'months', 'toc'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $pro_id = $id;
        $this->layout = 'admin_layout';
        if (!$this->Product->exists($id)) {
            throw new NotFoundException(__('Invalid product'));
        }
        if ($this->request->is(array('post', 'put'))) {

            $res = $this->Product->edit_product_info($this->request->data);

            if ($res == 1) {
                $this->Session->setFlash(__('The product has been saved.'), 'success');
                //return $this->redirect(array('action' => 'index'));
                return $this->redirect($this->referer());
            } else {
                $this->Session->setFlash(__('The product could not be saved. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
            $this->request->data = $this->Product->find('first', $options);
        }
        $res = $this->Product->avl_country($id);
        $this->loadModel('Category');
        $category = $this->Category->find('list', array('conditions' => array('Category.parent_category_id' => 0), 'fields' => array('Category.id', 'Category.category_name')));
        $product = $this->Product->find('first', array('conditions' => array('Product.id' => $pro_id)));
        Configure::load('idata');
        $months = Configure::read('idata.months');
        $schema_data = (!empty($product['Product']['schema_data'])) ? json_decode($product['Product']['schema_data'],TRUE) : NULL;                
        $this->set(compact('category', 'pro_id', 'months', 'res','product','schema_data'));
    }
    public function temp_edit_proddescription($id = null) {
        $pro_id = $id;
        $this->layout = 'admin_layout';
        if (!$this->Product->exists($id)) {
            throw new NotFoundException(__('Invalid product'));
        }
        if ($this->request->is(array('post', 'put'))) {

            $res = $this->Product->edit_product_info($this->request->data);

            if ($res == 1) {
                $this->Session->setFlash(__('The product has been saved.'), 'success');
                //return $this->redirect(array('action' => 'index'));
                return $this->redirect($this->referer());
            } else {
                $this->Session->setFlash(__('The product could not be saved. Please, try again.'), 'error');
            }
        } 
        else {
            $options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
            $this->request->data = $this->Product->find('first', $options);
        }
        $res = $this->Product->avl_country($id);
        $this->loadModel('Category');
        $category = $this->Category->find('list', array('conditions' => array('Category.parent_category_id' => 0), 'fields' => array('Category.id', 'Category.category_name')));
        Configure::load('idata');
        $months = Configure::read('idata.months');
        $this->set(compact('category', 'pro_id', 'months', 'res'));
    }


    public function seo_edit($id = null) {
        $pro_id = $id;
        $this->layout = 'admin_layout';
        if (!$this->Product->exists($id)) {
            throw new NotFoundException(__('Invalid product'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Product->save($this->request->data)) {
                $this->Session->setFlash(__('The product information  has been saved.'), 'success');
                return $this->redirect(array('action' => 'seo_index'));
            } else {
                $this->Session->setFlash(__('The product could not be saved. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
            $this->request->data = $this->Product->find('first', $options);
        }
        $this->Product->bindModel(
                array('hasMany' => array(
                        'ProductCountry' => array(
                            'className' => 'ProductCountry'
                        )
                    )
                )
        );
        $options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
        $product = $this->Product->find('first', $options);
        //debug($product);

        foreach ($product['ProductCategory'] as $key => $pc) {
            $category_ids[$key] = $pc['category_id'];
        }
        $product_categories = $this->Product->Category->find('list', array(
            'fields' => array('id', 'category_name'),
            'conditions' => array('Category.id' => $category_ids, 'Category.id !=' => $product['Product']['category_id'])
        ));
        //debug($product_categories);
        $country_ids = array();
        foreach ($product['ProductCountry'] as $key => $pro_country) {
            $country_ids[$key] = $pro_country['country_id'];
        }
        $this->loadModel('Country');
        $product_countries = $this->Country->find('list', array(
            'fields' => array('id', 'country_name'),
            'conditions' => array('Country.id' => $country_ids)
        ));

        $this->loadModel('CountryRegion');

        $this->loadModel('CountryRegion');
        $country_ids = array();
        $country_regions = $this->CountryRegion->find('list', array(
            'fields' => array('CountryRegion.region_id', 'CountryRegion.region_id'),
            'conditions' => array('CountryRegion.country_id' => $country_ids)));

        $this->loadModel('Region');
        $regions = $this->Region->find('list', array(
            'fields' => array('Region.id', 'Region.region_name'),
            'conditions' => array('Region.id' => $country_regions)));

        $product_specs = $this->Product->ProductSpecification->find('all', array(
            'conditions' => array('ProductSpecification.product_id' => $id),
            'fields' => array(
                'Specification.id', 'Specification.specification_name',
                'SpecificationOption.id', 'SpecificationOption.option_name',
                'Product.id'
            ),
            'order' => array('Specification.id')
        ));
        $product_specifications = array();
        $i = 0;

        foreach ($product_specs as $key => $ps) {
            $product_specifications[$ps['Specification']['specification_name']][$i++] = $ps['SpecificationOption']['option_name'];
        }
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');

        $this->set(compact('product', 'product_categories', 'yes_no', 'product_countries', 'regions', 'product_specifications'));
    }

    public function add_category($id = null) {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $this->request->data['id'] = $id;
            $res = $this->Product->add_prod_category($this->request->data);
            if ($res == 1) {
                $this->Product->update_prod_count_admin();
                $this->Session->setFlash(__('The product Category has been saved.'), 'success');
                return $this->redirect(array('action' => 'add_faq', $id)); 
                return $this->redirect(array('controller' => 'specifications', 'action' => 'add_product_spc', $id));
            } else {
                $this->Session->setFlash(__('The product Category could not be saved. Please, try again.'), 'error');
            }
        }
        $this->loadModel('Category');
        $category = $this->Category->find('list', array('conditions' => array('Category.parent_category_id' => 0), 'fields' => array('Category.id', 'Category.category_name')));
        $prod_name = $this->Product->find('first', array('conditions' => array('Product.id' => $id), 'fields' => array('Product.product_name', 'Category.category_name','Product.product_faq'), 'recursive' => 0));
        $pro_id = $id;

        $this->set(compact('category', 'pro_id', 'prod_name'));
    }

    public function edit_category($id = null) {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $this->request->data['id'] = $id;
            //debug($this->request->data);
            //die;
            $res = $this->Product->edit_prod_category($this->request->data);
            if ($res != 2) {
                if ($res == 1) {
                    $this->Session->setFlash(__('The product Category has been saved.'), 'success');
      //            return $this->redirect(array('controller' => 'products', 'action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The product Category could not be saved. Please, try again.'), 'error');
                }
            } else {
                $this->Session->setFlash(__('Select Category.'), 'error');
            }
        }
        $pro_id = $id;
        $this->loadModel('Category');
        $this->loadModel('ProductCategory');
        $res = $this->Product->avl_country($id);
        $select_prod_list = $this->ProductCategory->find('list', array('conditions' => array('ProductCategory.product_id' => $id), 'fields' => array('ProductCategory.category_id', 'ProductCategory.category_id')));
        $prod_name = $this->Product->find('first', array('conditions' => array('Product.id' => $id), 'fields' => array('Product.product_name', 'Product.is_active', 'Category.category_name','Product.product_faq'), 'recursive' => 0));
        $category = $this->Category->find('list', array('conditions' => array('Category.parent_category_id' => 0), 'fields' => array('Category.id', 'Category.category_name')));
        $select_prod_category = $this->ProductCategory->find('all', array('conditions' => array('ProductCategory.product_id' => $id), 'fields' => array('ProductCategory.*', 'Category.category_name')));
        $this->loadModel('Category');
        foreach ($select_prod_category as $key => $cat) {
            $select_prod_category[$key]['Bread'] = $this->Category->bread_cum_function($cat['ProductCategory']['category_id']);
        }

        $this->set(compact('category', 'select_prod_category', 'pro_id', 'res', 'prod_name'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null, $ref = null) {
        $this->Product->id = $id;
        if (!$this->Product->exists()) {
            throw new NotFoundException(__('Invalid product'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Product->delete()) {
            $this->Product->ProductCategory->deleteAll(array('ProductCategory.product_id' => $id));
            //$this->Product->update_prod_count_admin();
            $this->Session->setFlash(__('The product has been deleted.'), 'success');
            if (!empty($ref)) {
                return $this->redirect(array('action' => $ref));
            }
        } else {
            $this->Session->setFlash(__('The product could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function latest_market_reports() {
        $this->loadModel('Category');
        $this->loadModel('ProductCategory');
        $all_cate = $this->Category->find_sub_cat_ids(2);
        //debug($all_cate);
        $products = array();
        $product_ids = $this->ProductCategory->find('list', array(
            'conditions' => array('ProductCategory.category_id' => $all_cate),
            'fields' => array('ProductCategory.product_id', 'ProductCategory.product_id'),
            //'limit' => 10,
            'order' => array('ProductCategory.product_id DESC')
        ));

        $products = $this->Product->find('all', array(
            'conditions' => array('Product.id' => $product_ids, 'Product.is_active' => 1),
            'fields' => array('Product.id', 'Product.product_name', 'Product.alias', 'Product.slug',
                'Product.category_id', 'Product.product_description', 'Product.price'),
            'order' => array('Product.id DESC'),
            'recursive' => -1,
            'limit' => 80
        ));
        //debug($products);
        foreach ($products as $pr_key => $pro) {
            $main_cat = $this->Product->find_main_cat($pro['Product']['category_id']);
            $products[$pr_key]['main_cat'] = $main_cat['Category'];
        }
         $meta_name="Latest and Upcoming Market Research Reports: valuemarketresearch.com";
         $meta_desc="Explore our trending and most popular market research reports. We also include here our best selling and latest updated market research report.";
        $meta_keyword="Latest Market Research Reports, Upcoming Market Report, valuemarketresearch";
        $this->set(compact('products','meta_name','meta_desc','meta_keyword'));
        //die;
    }

    public function rss() {
        $this->layout = null;
        $this->loadModel('Category');
        $this->loadModel('ProductCategory');
        $all_cate = $this->Category->find_sub_cat_ids(2);
        //debug($all_cate);
        $products = array();
        $product_ids = $this->ProductCategory->find('list', array(
            'conditions' => array('ProductCategory.category_id' => $all_cate),
            'fields' => array('ProductCategory.product_id', 'ProductCategory.product_id'),
            //'limit' => 10,
            'order' => array('ProductCategory.product_id DESC')
        ));

        $products = $this->Product->find('all', array(
            'conditions' => array('Product.id' => $product_ids, 'Product.is_active' => 1),
            'fields' => array('Product.id', 'Product.product_name', 'Product.alias', 'Product.slug',
                'Product.category_id', 'Product.product_description', 'Product.price'),
            'order' => array('Product.id DESC'),
            'recursive' => -1,
            'limit' => 80
        ));
        //debug($products);
        foreach ($products as $pr_key => $pro) {
            $main_cat = $this->Product->find_main_cat($pro['Product']['category_id']);
            $products[$pr_key]['main_cat'] = $main_cat['Category'];
        }

        $this->set(compact('products'));
        //die;
    }

    /*For home page RSS FEEDS code Start VG-16/12/2016*/
     public function home_rss() {
        $this->layout = null;
        $this->loadModel('Category');
        $this->loadModel('ProductCategory');
        $all_cate = $this->Category->find_sub_cat_ids(2);
        //debug($all_cate);
        $products = array();
        $product_ids = $this->ProductCategory->find('list', array(
            'conditions' => array('ProductCategory.category_id' => $all_cate),
            'fields' => array('ProductCategory.product_id', 'ProductCategory.product_id'),
            'limit' => 10,
            'order' => array('ProductCategory.product_id DESC')
        ));

        $products = $this->Product->find('all', array(
            'conditions' => array('Product.id' => $product_ids, 'Product.is_active' => 1),
            'fields' => array('Product.id', 'Product.product_name', 'Product.alias', 'Product.slug',
                'Product.category_id', 'Product.product_description', 'Product.price'),
            'order' => array('Product.id DESC'),
            'recursive' => -1,
            'limit' => 10
        ));
        //debug($products);
        foreach ($products as $pr_key => $pro) {
            $main_cat = $this->Product->find_main_cat($pro['Product']['category_id']);
            $products[$pr_key]['main_cat'] = $main_cat['Category'];
        }
        //$this->set(compact('products'));
        return $products;
        //die;
    }
   /*For home page RSS FEEDS code End VG-16/12/2016*/
    public function category_listing($slug = null, $main=null) {
        
       // $this->request->params['named']['page'] = '2';
        $this->loadModel('Category');
        
        $catid = $this->Category->find('first',array('conditions' => array('Category.slug' => $slug)));

        if(empty($catid)){
            throw new MissingControllerException(__('Invalid Category'));
        }
        $id = $catid['Category']['id'];
            
        $paginator_url =  $slug;

        $this->set(compact('paginator_url'));
        $category = $this->Product->Category->find('first', array('conditions' => array('Category.id' => $id),
            'fields' => array('Category.id', 'Category.category_name', 'Category.is_active'), 'recursive' => 0));

        if (empty($category) || $category['Category']['is_active'] == 0) {
            return $this->redirect(array('controller' => 'users', 'action' => 'page_not_found'));
        }

        $res = $this->Product->Category->check_cat_url($id, $slug);

        $slug_name = $this->Product->cleanString($category['Category']['category_name']);
        if ($res == 0) {
            $this->redirect(array('controller' => 'products', 'action' => 'category_listing', 'id' => $id, 'slug' => $slug_name), 301);
        }
        if (isset($this->request->params['page']) && !empty($this->request->params['page'])) {
            $this->request->params['named']['page'] = $this->request->params['page'];
        }

        $this->loadModel('Category');
        $this->loadModel('Region');
        $this->loadModel('Country');
        $this->loadModel('ProductCategory');
        $this->loadModel('ProductCountry');
        $this->loadModel('CountryRegion');
        $this->loadModel('ProductCategory');
        $this->loadModel('ProductSpecification');
        $cat_data = array();
        if (isset($id)) {
            $prev_data = $this->Cookie->read('cat_view');
            if (!empty($prev_data)) {
                if (!in_array($id, $prev_data)) {
                    $recent_view_array = $this->Cookie->read('cat_view');
                    array_push($prev_data, $id);
                    $this->Cookie->write('cat_view', $prev_data, false, '+2 weeks');
                    $counter_data = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'fields' => array('Category.no_of_view'), 'recursive' => -1));
                    $cnt = $counter_data['Category']['no_of_view'] + 1;
                    $this->Category->updateAll(array('Category.no_of_view' => $cnt), array('Category.id' => $id));
                }
            } else {
                $recent_view_array = array();
                array_push($recent_view_array, $id);
                $this->Cookie->write('cat_view', $recent_view_array, false, '+2 weeks');
                $counter_data = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'fields' => array('Category.no_of_view'), 'recursive' => -1));
                $cnt = $counter_data['Category']['no_of_view'] + 1;
                $this->Category->updateAll(array('Category.no_of_view' => $cnt), array('Category.id' => $id));
            }
        }
        if (!empty($id)) {
            $main_cat = $this->Category->find('first', array('conditions' => array('Category.id' => $id, 'Category.is_active' => 1), 'recursive' => -1));
             $research_ind = array();
             $cat_data = array();
            $cat_root = array();
            if ($main_cat['Category']['level'] != 1) {
                $cat_root = $this->Category->get_front_category_tree($main_cat['Category']['level'], $main_cat['Category']['parent_category_id'], $main_cat['Category']['level']);
            } else {
                $cat_root = $main_cat['Category']['id'];
            }

            // $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
            // foreach ($research_ind_cat as $key1 => $cat) {
            //     $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
            //     $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
            //     $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
            //     $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
                $level_two = $this->Category->find('all', array('conditions' =>
                    array(
                        'Category.parent_category_id' => 2,
                        'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_two as $key2 => $level2) {
                    $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                    $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                    $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                    $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_three as $key3 => $level3) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                        $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_four as $key4 => $level4) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                            $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_five as $key5 => $level5) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                                $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                                foreach ($level_six as $key6 => $level6) {
                                    $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                    $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                    $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                    $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                                }
                            }
                        }
                    }
                }
            // }

            // echo "<pre>";
            // print_r($this->Category->bread_cum_function($id));exit;
            // $parent_cat_id = $this->Category->bread_cum_function($id);
            // $cat_data = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1, 'Category.id' => $parent_cat_id[1]['Category']['id']), 'recursive' => -1));
            // foreach ($cat_data as $key1 => $cat) {
            //     $cat_data['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
            //     $cat_data['Level1'][$key1]['id'] = $cat['Category']['id'];
            //     $cat_data['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
            //     $cat_data['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
                $level_two = $this->Category->find('all', array('conditions' =>
                    array(
                        'Category.parent_category_id' => 2,
                        'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_two as $key2 => $level2) {
                    $cat_data['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                    $cat_data['Level2'][$key2]['id'] = $level2['Category']['id'];
                    $cat_data['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                    $cat_data['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                    $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_three as $key3 => $level3) {
                        $cat_data['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                        $cat_data['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                        $cat_data['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                        $cat_data['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                        $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_four as $key4 => $level4) {
                            $cat_data['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                            $cat_data['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                            $cat_data['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                            $cat_data['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                            $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_five as $key5 => $level5) {
                                $cat_data['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                                $cat_data['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                                $cat_data['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                                $cat_data['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                                $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                                foreach ($level_six as $key6 => $level6) {
                                    $cat_data['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                    $cat_data['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                    $cat_data['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                    $cat_data['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                                }
                            }
                        }
                    }
                }
            }

        // }

        $cook_data = $this->Cookie->read('recent_cook');
        $recent_view = array();
        if (!empty($cook_data)) {
            $recent_view = $this->Category->recent_view_function($cook_data);
        }
        $bredcum_array = array();
        $bredcum_array = $this->Category->bread_cum_function($id);
        // echo "<pre>";
        // print_r($bredcum_array);exit;
        $this->set(compact('research_ind', 'cat_data', 'bredcum_array', 'id', 'main_cat', 'recent_view'));
        $all_cate = $this->Category->find_sub_cat_ids($id);

        //$selected_cat = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'recursive' => -1));

          // New Code added to prevent  market reports links start VG-27-01-2017
        if($id !=2)
        {
                 $selected_cat = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'recursive' => -1));
        }
        else {
            $this->redirect('/custom404');
        //echo "No records";
        }
        // New Code added to prevent  market reports links end VG-27-01-2017
        // echo "<pre>";
        // print_r($selected_cat);exit;
        $meta_name = $selected_cat['Category']['meta_name'];
        // $meta_keyword = $selected_cat['Category']['meta_keywords'];
        $meta_desc = $selected_cat['Category']['meta_desc'];
        $meta_keyword = $selected_cat['Category']['long_desc'];

        $this->set(compact('selected_cat','meta_name','meta_desc','meta_keyword'));
        $select_category_product = array();

        $select_category_product = $this->ProductCategory->find('list', array(
            'conditions' => array('ProductCategory.category_id' => $all_cate),
            'fields' => array('ProductCategory.product_id', 'ProductCategory.product_id')));
        //print_r($select_category_product); exit();
        $limit_per_page = 10;
        $condi_date = '';
        $condi_price = '';
        if ($this->Session->check('publishd_date')) {
            $p_data = $this->Session->read('publishd_date');
            if ($p_data['sort_by'] == 'ASC') {
                $condi_date = 'Product.pub_date ASC';
            } else {
                $condi_date = 'Product.pub_date DESC';
            }
        }
        if ($this->Session->check('pro_price')) {
            $price_data = $this->Session->read('pro_price');
            if ($price_data['sort_by'] == 'ASC') {
                $condi_date = 'Product.price ASC';
            } else {
                $condi_date = 'Product.price DESC';
            }
        }

        if ($this->request->is('post')) {
            $f = 0;

            if (isset($this->request->data['Product']['limit_of_records'])) {
                //  $this->Session->write('limit_per_page', $this->request->data['Product']['limit_of_records']);
                $limit_per_page = $this->request->data['Product']['limit_of_records'];
                $this->Session->write('limit_filter', $this->request->data);
            } else {
                $this->Session->write('specification_session', $this->request->data);
            }
            $final_out = array();
            if (isset($this->request->data['SpecificationOption'])) {
                $f = 1;
                foreach ($this->request->data['SpecificationOption'] as $okey => $spc_op) {

                    $res = $this->ProductSpecification->find('list', array('conditions' => array('ProductSpecification.specification_option_id' => $spc_op, 'ProductSpecification.product_id' => $select_category_product), 'fields' => array('ProductSpecification.product_id', 'ProductSpecification.product_id')));
                    if (empty($final_out)) {
                        $final_out = $res;
                    } else {
                        $interset = array_intersect($final_out, $res);
                        $final_out = $interset;
                    }
                }
            }
            $reg_prod_id = array();
            if (isset($this->request->data['Region'])) {
                $f = 1;
                $rg_id = $this->CountryRegion->find('list', array('conditions' => array('CountryRegion.region_id' => $this->request->data['Region']), 'fields' => array('CountryRegion.country_id', 'CountryRegion.country_id')));
                $reg_prod_id = $this->ProductCountry->find('list', array('conditions' => array('ProductCountry.country_id' => $rg_id), 'fields' => array('ProductCountry.product_id', 'ProductCountry.product_id')));
            }
            $country_prod_id = array();
            if (isset($this->request->data['Country'])) {
                $f = 1;
                $country_prod_id = $this->ProductCountry->find('list', array('conditions' => array('ProductCountry.country_id' => $this->request->data['Country']), 'fields' => array('ProductCountry.product_id', 'ProductCountry.product_id')));
            }
            if (empty($reg_prod_id)) {
                if ((!empty($country_prod_id)) && (!empty($final_out))) {
                    $fin_reg_cou_spec = array_intersect($country_prod_id, $final_out);
                } elseif (empty($final_out)) {
                    $fin_reg_cou_spec = $country_prod_id;
                } else {
                    $fin_reg_cou_spec = $final_out;
                }
            } elseif (empty($country_prod_id)) {
                if ((!empty($reg_prod_id)) && (!empty($final_out))) {
                    $fin_reg_cou_spec = array_intersect($reg_prod_id, $final_out);
                } elseif (empty($final_out)) {
                    $fin_reg_cou_spec = $reg_prod_id;
                } else {
                    $fin_reg_cou_spec = $final_out;
                }
            } elseif (empty($final_out)) {
                if ((!empty($reg_prod_id)) && (!empty($country_prod_id))) {
                    $fin_reg_cou_spec = array_intersect($reg_prod_id, $country_prod_id);
                } elseif (empty($country_prod_id)) {
                    $fin_reg_cou_spec = $reg_prod_id;
                } else {
                    $fin_reg_cou_spec = $country_prod_id;
                }
            } else {
                $fin_reg_cou_spec = array_intersect($reg_prod_id, $country_prod_id, $final_out);
            }
            $fin_reg_cou_spec = array_intersect($fin_reg_cou_spec, $select_category_product);
            if (!empty($this->request->data['SpecificationOption'])) {
                if (empty($final_out)) {
                    $fin_reg_cou_spec = array();
                }
            }
            if (empty($this->request->data)) {
                if ($this->Session->check('limit_filter')) {
                    $data = $this->Session->read('limit_filter');
                    $limit_per_page = $data['Product']['limit_of_records'];
                    // $this->Session->delete('limit_filter');
                }
                $this->paginate = array(
                    'limit' => $limit_per_page,
                    'group' => array('Product.id'),
                    'order' => $condi_date
                );
                $products = $this->Paginator->paginate(
                    'ProductCategory', array(
                    'ProductCategory.category_id' => $all_cate,
                    'Product.is_active' => 1,
                    'Product.is_upcoming' => 0)
                );
            } else {
                if ($this->Session->check('limit_filter')) {
                    $data = $this->Session->read('limit_filter');
                    $limit_per_page = $data['Product']['limit_of_records'];
                    // $this->Session->delete('limit_filter');
                }

                if ($f == 1) {
                    $this->paginate = array(
                        'limit' => $limit_per_page,
                        'group' => array('Product.id'),
                        'order' => $condi_date
                    );
                    $products = $this->Paginator->paginate(
                            array('Product.id' => $fin_reg_cou_spec,
                                'Product.is_active' => 1,'Product.is_upcoming' => 0)
                    );
                } else {
                    $this->paginate = array(
                        'limit' => $limit_per_page,
                        'group' => array('ProductCategory.product_id'),
                        'order' => $condi_date
                    );
                    $products = $this->Paginator->paginate(
                            'ProductCategory', array(
                        'ProductCategory.category_id' => $all_cate,
                        'Product.is_active' => 1,'Product.is_upcoming' => 0)
                    );
                }
            }
        } else {

            if ($this->Session->check('limit_filter')) {
                $data = $this->Session->read('limit_filter');
                $limit_per_page = $data['Product']['limit_of_records'];
                // $this->Session->delete('limit_filter');
            }

            $this->paginate = array(
                'limit' => $limit_per_page,
                'group' => array('ProductCategory.product_id'),
                'order' =>array($condi_date=>'desc'),
                //'order' =>$condi_date,
                'order' => array('ProductCategory.product_id'=>'desc')
                
                        );
            $products = $this->Paginator->paginate(
                'ProductCategory', array(
                'ProductCategory.category_id' => $all_cate,
                'Product.is_active' => 1,'Product.is_upcoming' => 0, )
            );
        }
        $product_ids = $this->Product->ProductCategory->find('list', array('conditions' => array('ProductCategory.category_id' => $all_cate), 'fields' => array('ProductCategory.product_id', 'ProductCategory.product_id')));
        $product_country = $this->ProductCountry->find('list', array('conditions' => array('ProductCountry.product_id' => $product_ids), 'fields' => array('ProductCountry.country_id', 'ProductCountry.country_id')));
        $product_region = $this->CountryRegion->find('list', array('conditions' => array('CountryRegion.country_id' => $product_country), 'fields' => array('CountryRegion.region_id', 'CountryRegion.region_id')));
        $regions = $this->Region->find('list', array('conditions' => array('Region.id' => $product_region), 'fields' => array('Region.id', 'Region.region_name')));
        $countries = $this->Country->find('list', array('conditions' => array('Country.id' => $product_country), 'fields' => array('Country.id', 'Country.country_name')));
        $this->loadModel('ProductSpecification');
        $product_specification = $this->ProductSpecification->find('all', array('conditions' => array('ProductSpecification.product_id' => $product_ids)));

        $specification_options = array();
        foreach ($product_specification as $spkey => $spec) {
            $specification_options[$spec['Specification']['id']]['specification_name'] = $spec['Specification']['specification_name'];
            if (isset($spec['SpecificationOption']) && !empty($spec['SpecificationOption'])) {

                $specification_options[$spec['Specification']['id']]['specification_options'][$spec['SpecificationOption']['id']] = $spec['SpecificationOption']['option_name'];
            }
        }
        $bredcum_array = array();
        $bredcum_array = $this->Category->bread_cum_function($id);
       // echo "<pre>";
        foreach ($products as $pr_key => $pro) {
            $main_cat = $this->Product->find_main_cat($pro['Product']['category_id']);            
            $products[$pr_key]['main_cat'] = ($main_cat) ? $main_cat['Category'] : '';
        }

        // print_r($products);exit;
        Configure::load('idata');
        $limit = Configure::read('idata.limit');
        $this->set(compact('products', 'bredcum_array', 'limits', 'limit_per_page', 'countries', 'regions', 'specification_options'));
        $device = $this->Session->read('device');

        
        
        if (isset($device) && !empty($device)) {
            // $this->render('category_listing_' . $device);
            $this->render('category_listing_desktop');
        } else {
            $this->loadModel('User');
            $this->User->detect_device();
            $device = $this->Session->read('device');
            // $this->render('category_listing_' . $device);
            $this->render('category_listing_desktop');
        }
    }

    public function category_listing_preview($id = NULL) {
        // $this->request->params['named']['page'] = '2';
        $category = $this->Product->Category->find('first', array('conditions' => array('Category.id' => $id), 'fields' => array('Category.id', 'Category.category_name', 'Category.is_active'), 'recursive' => 0));

        // $res = $this->Product->Category->check_cat_url($id, $slug);
        $slug_name = $this->Product->cleanString($category['Category']['category_name']);
//        if ($res == 0) {
//            $this->redirect(array('controller' => 'products', 'action' => 'category_listing', 'id' => $id, 'slug' => $slug_name), 301);
//        }
        if (isset($this->request->params['page']) && !empty($this->request->params['page'])) {
            $this->request->params['named']['page'] = $this->request->params['page'];
        }
        $this->loadModel('Category');
        $this->loadModel('Region');
        $this->loadModel('Country');
        $this->loadModel('ProductCategory');
        $this->loadModel('ProductCountry');
        $this->loadModel('CountryRegion');
        $this->loadModel('ProductCategory');
        $this->loadModel('ProductSpecification');
        $cat_data = array();

        if (!empty($id)) {
            $main_cat = $this->Category->find('all', array('conditions' => array('Category.id' => $id), 'recursive' => -1));

            $cat_data = array();
            foreach ($main_cat as $key1 => $cat) {
                $cat_data['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
                $cat_data['Level1'][$key1]['id'] = $cat['Category']['id'];
                $cat_data['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
                $cat_data['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
                $level_two = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $cat['Category']['id']), 'recursive' => -1));
                foreach ($level_two as $key2 => $level2) {
                    $cat_data['Level1'][$key1]['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                    $cat_data['Level1'][$key1]['Level2'][$key2]['id'] = $level2['Category']['id'];
                    $cat_data['Level1'][$key1]['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                    $cat_data['Level1'][$key1]['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                    $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id']), 'recursive' => -1));
                    foreach ($level_three as $key3 => $level3) {
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                        $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id']), 'recursive' => -1));
                        foreach ($level_four as $key4 => $level4) {
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                            $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id']), 'recursive' => -1));
                            foreach ($level_five as $key5 => $level5) {
                                $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                                $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                                $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                                $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                                $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id']), 'recursive' => -1));
                                foreach ($level_six as $key6 => $level6) {
                                    $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                    $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                    $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                    $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                                }
                            }
                        }
                    }
                }
            }
        }

        /* $this->loadModel('Product');

          $relatet_top_product = $this->Product->find('all', array(
          'conditions' => array('Product.category_id' => $id),
          'limit' => 6,
          'order' => 'rand()',
          'recursive' => -1,
          'fields' => array('Product.id', 'Product.product_image', 'Product.product_name', 'Product.slug'),
          )); */
        $cook_data = $this->Cookie->read('recent_cook');
        $recent_view = array();
        if (!empty($cook_data)) {
            $recent_view = $this->Category->recent_view_function($cook_data);
        }
        $bredcum_array = array();
        $bredcum_array = $this->Category->bread_cum_function($id);
        $this->set(compact('cat_data', 'bredcum_array', 'id', 'main_cat', 'recent_view'));
        $all_cate = $this->Category->find_sub_cat_ids($id);

        $selected_cat = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'recursive' => -1));
        $this->set(compact('selected_cat'));
        $select_category_product = array();
        $select_category_product = $this->ProductCategory->find('list', array('conditions' => array('ProductCategory.category_id' => $all_cate), 'fields' => array('ProductCategory.product_id', 'ProductCategory.product_id')));
        $limit_per_page = 10;
        $condi_date = '';
        $condi_price = '';
        if ($this->Session->check('publishd_date')) {
            $p_data = $this->Session->read('publishd_date');
            if ($p_data['sort_by'] == 'ASC') {
                $condi_date = 'Product.pub_date ASC';
            } else {
                $condi_date = 'Product.pub_date DESC';
            }
        }
        if ($this->Session->check('pro_price')) {
            $price_data = $this->Session->read('pro_price');
            if ($price_data['sort_by'] == 'ASC') {
                $condi_date = 'Product.price ASC';
            } else {
                $condi_date = 'Product.price DESC';
            }
        }
        if ($this->request->is('post')) {
            $f = 0;

            if (isset($this->request->data['Product']['limit_of_records'])) {
                //  $this->Session->write('limit_per_page', $this->request->data['Product']['limit_of_records']);
                $limit_per_page = $this->request->data['Product']['limit_of_records'];
                $this->Session->write('limit_filter', $this->request->data);
            } else {
                $this->Session->write('specification_session', $this->request->data);
            }
            $final_out = array();
            if (isset($this->request->data['SpecificationOption'])) {
                $f = 1;
                foreach ($this->request->data['SpecificationOption'] as $okey => $spc_op) {

                    $res = $this->ProductSpecification->find('list', array('conditions' => array('ProductSpecification.specification_option_id' => $spc_op, 'ProductSpecification.product_id' => $select_category_product), 'fields' => array('ProductSpecification.product_id', 'ProductSpecification.product_id')));
                    if (empty($final_out)) {
                        $final_out = $res;
                    } else {
                        $interset = array_intersect($final_out, $res);
                        $final_out = $interset;
                    }
                }
            }
            $reg_prod_id = array();
            if (isset($this->request->data['Region'])) {
                $f = 1;
                $rg_id = $this->CountryRegion->find('list', array('conditions' => array('CountryRegion.region_id' => $this->request->data['Region']), 'fields' => array('CountryRegion.country_id', 'CountryRegion.country_id')));
                $reg_prod_id = $this->ProductCountry->find('list', array('conditions' => array('ProductCountry.country_id' => $rg_id), 'fields' => array('ProductCountry.product_id', 'ProductCountry.product_id')));
            }
            $country_prod_id = array();
            if (isset($this->request->data['Country'])) {
                $f = 1;
                $country_prod_id = $this->ProductCountry->find('list', array('conditions' => array('ProductCountry.country_id' => $this->request->data['Country']), 'fields' => array('ProductCountry.product_id', 'ProductCountry.product_id')));
            }
            if (empty($reg_prod_id)) {
                if ((!empty($country_prod_id)) && (!empty($final_out))) {
                    $fin_reg_cou_spec = array_intersect($country_prod_id, $final_out);
                } elseif (empty($final_out)) {
                    $fin_reg_cou_spec = $country_prod_id;
                } else {
                    $fin_reg_cou_spec = $final_out;
                }
            } elseif (empty($country_prod_id)) {
                if ((!empty($reg_prod_id)) && (!empty($final_out))) {
                    $fin_reg_cou_spec = array_intersect($reg_prod_id, $final_out);
                } elseif (empty($final_out)) {
                    $fin_reg_cou_spec = $reg_prod_id;
                } else {
                    $fin_reg_cou_spec = $final_out;
                }
            } elseif (empty($final_out)) {
                if ((!empty($reg_prod_id)) && (!empty($country_prod_id))) {
                    $fin_reg_cou_spec = array_intersect($reg_prod_id, $country_prod_id);
                } elseif (empty($country_prod_id)) {
                    $fin_reg_cou_spec = $reg_prod_id;
                } else {
                    $fin_reg_cou_spec = $country_prod_id;
                }
            } else {
                $fin_reg_cou_spec = array_intersect($reg_prod_id, $country_prod_id, $final_out);
            }
            $fin_reg_cou_spec = array_intersect($fin_reg_cou_spec, $select_category_product);
            if (!empty($this->request->data['SpecificationOption'])) {
                if (empty($final_out)) {
                    $fin_reg_cou_spec = array();
                }
            }
            if (empty($this->request->data)) {
                if ($this->Session->check('limit_filter')) {
                    $data = $this->Session->read('limit_filter');
                    $limit_per_page = $data['Product']['limit_of_records'];
                    // $this->Session->delete('limit_filter');
                }
                $this->paginate = array(
                    'limit' => $limit_per_page,
                    'group' => array('Product.id'),
                    'order' => $condi_date
                );
                $products = $this->Paginator->paginate(
                        'ProductCategory', array(
                    'ProductCategory.category_id' => $all_cate,
                    'Product.is_active' => 1)
                );
            } else {
                if ($this->Session->check('limit_filter')) {
                    $data = $this->Session->read('limit_filter');
                    $limit_per_page = $data['Product']['limit_of_records'];
                    // $this->Session->delete('limit_filter');
                }

                if ($f == 1) {
                    $this->paginate = array(
                        'limit' => $limit_per_page,
                        'group' => array('Product.id'),
                        'order' => $condi_date
                    );
                    $products = $this->Paginator->paginate(
                            array('Product.id' => $fin_reg_cou_spec,
                            )
                    );
                } else {
                    $this->paginate = array(
                        'limit' => $limit_per_page,
                        'group' => array('ProductCategory.product_id'),
                        'order' => $condi_date
                    );
                    $products = $this->Paginator->paginate(
                            'ProductCategory', array(
                        'ProductCategory.category_id' => $all_cate,
                            )
                    );
                }
            }
        } else {
            if ($this->Session->check('limit_filter')) {
                $data = $this->Session->read('limit_filter');
                $limit_per_page = $data['Product']['limit_of_records'];
                // $this->Session->delete('limit_filter');
            }

            $this->paginate = array(
                'limit' => $limit_per_page,
                'group' => array('ProductCategory.product_id'),
                'order' => $condi_date
            );
            $products = $this->Paginator->paginate(
                    'ProductCategory', array(
                'ProductCategory.category_id' => $all_cate,
                    )
            );
        }
        $product_ids = $this->Product->ProductCategory->find('list', array('conditions' => array('ProductCategory.category_id' => $all_cate), 'fields' => array('ProductCategory.product_id', 'ProductCategory.product_id')));
        $product_country = $this->ProductCountry->find('list', array('conditions' => array('ProductCountry.product_id' => $product_ids), 'fields' => array('ProductCountry.country_id', 'ProductCountry.country_id')));
        $product_region = $this->CountryRegion->find('list', array('conditions' => array('CountryRegion.country_id' => $product_country), 'fields' => array('CountryRegion.region_id', 'CountryRegion.region_id')));
        $regions = $this->Region->find('list', array('conditions' => array('Region.id' => $product_region), 'fields' => array('Region.id', 'Region.region_name')));
        $countries = $this->Country->find('list', array('conditions' => array('Country.id' => $product_country), 'fields' => array('Country.id', 'Country.country_name')));
        $this->loadModel('ProductSpecification');
        $product_specification = $this->ProductSpecification->find('all', array('conditions' => array('ProductSpecification.product_id' => $product_ids)));

        $specification_options = array();
        foreach ($product_specification as $spkey => $spec) {
            $specification_options[$spec['Specification']['id']]['specification_name'] = $spec['Specification']['specification_name'];
            if (isset($spec['SpecificationOption']) && !empty($spec['SpecificationOption'])) {

                $specification_options[$spec['Specification']['id']]['specification_options'][$spec['SpecificationOption']['id']] = $spec['SpecificationOption']['option_name'];
            }
        }
        $bredcum_array = array();
        $bredcum_array = $this->Category->bread_cum_function($id);

        foreach ($products as $pr_key => $pro) {
            $main_cat = $this->Product->find_main_cat($pro['Product']['category_id']);
            $products[$pr_key]['main_cat'] = $main_cat['Category'];
        }
        Configure::load('idata');
        $limit = Configure::read('idata.limit');
        $this->set(compact('products', 'bredcum_array', 'limits', 'limit_per_page', 'countries', 'regions', 'specification_options'));
        $device = $this->Session->read('device');
        if (isset($device) && !empty($device)) {
            $this->render('category_listing_' . $device);
            //$this->render('category_listing_mobile');
        } else {
            $this->loadModel('User');
            $this->User->detect_device();
            $device = $this->Session->read('device');
            $this->render('category_listing_' . $device);
            //$this->render('category_listing_mobile');
        }
    }

    public function publish_session() {
        if ($this->Session->check('publishd_date')) {
            $data = $this->Session->read('publishd_date');
            if ($data['sort_by'] == 'ASC') {
                $data['sort_by'] = 'DSC';
            } else {
                $data['sort_by'] = 'ASC';
            }
            $this->Session->write('publishd_date', $data);
        } else {
            $data['sort_by'] = 'ASC';
            $this->Session->write('publishd_date', $data);
        }
        $this->Session->delete('pro_price');
        return $this->redirect($this->referer());
    }

    public function price_session() {
        if ($this->Session->check('pro_price')) {
            $data = $this->Session->read('pro_price');
            if ($data['sort_by'] == 'ASC') {
                $data['sort_by'] = 'DSC';
            } else {
                $data['sort_by'] = 'ASC';
            }
            $this->Session->write('pro_price', $data);
        } else {
            $data['sort_by'] = 'ASC';
            $this->Session->write('pro_price', $data);
        }
        $this->Session->delete('publishd_date');
        return $this->redirect($this->referer());
    }

    public function category_details($slug = null) {

        $this->loadModel('Testimonial');
        /*Testimonial code Start VG-15/12/2016*/
        $testimonial=$this->Testimonial->find('all',array(
            'conditions'=>array('Testimonial.is_verified'=>1,'Testimonial.is_active'=>1,'Testimonial.is_deleted'=>0),
            'fields'=>array('id','testimonial_title','testimonial_description','testimonial_image'),
            'recursive'=>-1,
            'order'=>array('Testimonial.id DESC'),
            'limit'=>3,
            'cache' => 'testimonialList', 
            'cacheConfig' => 'short'
        ));
        /*Testimonial code End VG-15/12/2016        */

        $this->loadModel('Category');
        $ourclients = $this->Category->our_client_fun();
        $this->set(compact('ourclients','testimonial'));
        
        $this->loadModel('Whyus');
        $whyus = $this->Whyus->find('all',array(
            'fields'=>array('title','description'),
            'limit' => 4,
            'cache' => 'whyUsList', 
            'cacheConfig' => 'short'
        ));

        $prdid = $this->Product->find('first',array('conditions' => array('Product.slug' => $slug)));
        if(empty($prdid)){
            throw new MissingControllerException(__('Invalid Product'));
        }
        $id = $prdid['Product']['id'];

        $product = $this->Product->find('first', array('conditions' => array('Product.id' => $id)));

        if (empty($product)) {
            return $this->redirect(array('controller' => 'users', 'action' => 'page_not_found'));
        }

        if ($product['Product']['is_active'] == 0) {
            return $this->redirect(array('controller' => 'categories', 'action' => 'home'));
        }

        $this->loadModel('Category');
        $this->loadModel('ProductCategory');
        $this->loadModel('ProductCountry');

        $prev_data = $this->Cookie->read('recent_cook');
        if (!empty($prev_data)) {
            $recent_view_array = $this->Cookie->read('recent_cook');
            array_push($recent_view_array, $id);
            $result = array_unique($recent_view_array);
            $rev_res = array_reverse($result);
            if (count($result) > 5) {
                $rev_res = array_slice($rev_res, 0, 5, true);
            }
            $this->Cookie->write('recent_cook', $rev_res, false, '+2 weeks');
        } else {
            $recent_view_array = array();
            array_push($recent_view_array, $id);
            $this->Cookie->write('recent_cook', $recent_view_array, false, '+2 weeks');
        }

        $category_names = $this->ProductCategory->find('all', array(
            'fields' => array('Category.id', 'Category.category_name', 'ProductCategory.category_id', 'ProductCategory.product_id'),
            'conditions' => array('ProductCategory.product_id' => $id, 'Category.is_active' => 1),
            'cache' => 'categoryList', 
            'cacheConfig' => 'short'
        ));

        $category_ids = $this->ProductCategory->find('first', array(
            'conditions' => array(
                'ProductCategory.product_id' => $id,
                'ProductCategory.category_id NOT' => $product['Product']['category_id']
            ), 'fields' => array('category_id')));

        $rand_cat_ids = array();
        foreach ($category_names as $cat_list) {
            array_push($rand_cat_ids, $cat_list['ProductCategory']['category_id']);
        }

        $cat_array = array();
        foreach ($category_names as $key => $cat) {
            $cat_array[$cat['Category']['id']] = $cat['Category']['category_name'];
        }

        $product_country = $this->ProductCountry->find('all', array('conditions' => array('ProductCountry.product_id' => $id)));

        foreach ($product_country as $key => $country) {
            $pro_country[$country['Country']['id']] = $country['Country']['country_name'];
            $c_arry[$country['Country']['id']] = $country['Country']['id'];
        }

        $meta_name = $product['Product']['meta_name'];
        $meta_desc = $product['Product']['meta_desc'];
        $meta_keyword = $product['Product']['meta_keywords'];

        $bredcum_array = array();
        $bredcum_array = array();

        if (empty($category_ids['ProductCategory']['category_id'])) {
            $category_ids['ProductCategory']['category_id'] = $product['Product']['category_id'];
        }
        // debug($category_ids);die;
        $bredcum_array = $this->Category->bread_cum_function($category_ids['ProductCategory']['category_id']);


        //Related produts section start
        $related_product_ids = $this->ProductCategory->find('list', array(
            'fields' => array('ProductCategory.product_id', 'ProductCategory.product_id'),
            'conditions' => array(
                'ProductCategory.category_id' => $rand_cat_ids,
                'ProductCategory.product_id !=' => $product['Product']['id']
            ),
            'limit' => 15,
            'cache' => 'productList', 
            'cacheConfig' => 'short'
        ));

        $related_products = $this->Product->find('all', array(
            'fields' => array(
                'Product.id', 'Product.product_name', 'Product.slug', 'Product.category_id',
                'Product.meta_name', 'Product.alias', 'Product.product_description'
            ),
            'conditions' => array(
                'Product.id' => $related_product_ids,
                'Product.is_active' => 1
            ),
            'order' => array('Product.id DESC'),
            'limit' => 10,
            'cache' => 'relatedProductList', 
            'cacheConfig' => 'short'
        ));
        //debug($related_products);
        //Get main category of product for URL generation in view
        foreach ($related_products as $pr_key => $pro) {
            $main_cat = $this->Product->find_main_cat($pro['Product']['category_id']);
            if (!empty($main_cat)) {
                $related_products[$pr_key]['main_cat'] = $main_cat['Category'];
            } else {
                $related_products[$pr_key]['main_cat'] = 'products';
            }
        }

        Configure::load('idata');
        $title = Configure::read('idata.title');
        $ref_page = Configure::read('idata.ref_page');
        $licence_type = Configure::read('idata.licence_type');
        $licence_type_tooltip = Configure::read('idata.licence_type_tooltip');
        $lic_type = Configure::read('idata.licence_type');

        if ($this->Session->check('currency')) {
            $cur_curency = $this->Session->read('currency');
        }

        $licence_type[1] = $licence_type[1] . ': $ ' . $product['Product']['price'];
        $licence_type[2] = $licence_type[2] . ': $ ' . $product['Product']['corporate_price'];
        $licence_type[3] = $licence_type[3] . ': $ ' . $product['Product']['upto10'];
        $licence_type[4] = $licence_type[4] . ': $ ' . $product['Product']['data_pack_price'];
        $cat_main = '';

        $this->set(compact('title', 'ref_page', 'licence_type', 'licence_type_tooltip','lic_type','whyus'));
        $this->set(compact('product', 'bredcum_array', 'meta_name', 'meta_desc', 'meta_keyword', 'related_products', 'cat_array', 'pro_country'));
        // Code Ends Here VG- 11/08/2016
    }

    public function billing_info() {

    }

    public function payment() {

    }

    public function active($id = NULL, $ref = null) {
        if ($this->Product->updateAll(array('Product.is_active' => 1, 'Product.is_verified' => 1, 'Product.modified' => 'now()', ), array('Product.id' => $id))) {
            $this->Product->update_prod_count_admin();
            $this->Session->setFlash(__('The Product has been Activated.'), 'success');
            if (!empty($ref)) {
                return $this->redirect(array('action' => $ref));
            }
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Product could not be Activated. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    //change status function for jr. analysts and Sr. Analyst
    public function ready_to_active($id = NULL, $ref = null) {
        if ($this->Product->updateAll(array('Product.is_active' => 2, 'Product.is_verified' => 1), array('Product.id' => $id))) {
            $this->Product->update_prod_count_admin();
            $this->Session->setFlash(__('The Product has been Activated.'), 'success');
            if (!empty($ref)) {
                return $this->redirect(array('action' => $ref));
            }
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Product could not be Activated. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function deactive($id = NULL, $ref = null) {
        if ($this->Product->updateAll(array('Product.is_active' => 0), array('Product.id' => $id))) {
            $this->Product->update_prod_count_admin();
            $this->Session->setFlash(__('The Product has been DeActivated.'), 'success');
            if (!empty($ref)) {
                return $this->redirect(array('action' => $ref));
            }
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Product could not be DeActivated. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function hide($id = NULL) {
        if ($this->Product->updateAll(array('Product.is_hide' => 1, 'Product.is_verified' => 1), array('Product.id' => $id))) {
            $this->Session->setFlash(__('The Product has been Hide.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Product could not be Hided. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function unhide($id = NULL) {
        if ($this->Product->updateAll(array('Product.is_hide' => 0), array('Product.id' => $id))) {
            $this->Session->setFlash(__('The Product has been Unhide.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Product could not be Unhided. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function verified($id = NULL) {
        if ($this->Product->updateAll(array('Product.is_verified' => 1), array('Product.id' => $id))) {
            $this->Session->setFlash(__('The Product has been verified.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Product could not be verified. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function deverified($id = NULL) {
        if ($this->Product->updateAll(array('Product.is_verified' => 0), array('Product.id' => $id))) {
            $this->Session->setFlash(__('The Product has been Deverified.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Product could not be Deverified. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function add_product_countries($id = null) {
        $pro_id = $id;
        $this->layout = 'admin_layout';
        $this->loadModel('ProductCountry');
        if ($this->request->is('post')) {
            unset($this->request->data['Product']['product_name']);
            $f = 0;
            $this->ProductCountry->deleteAll(array('ProductCountry.product_id' => $id));
            if (isset($this->request->data['Product']['country'])) {
                foreach ($this->request->data['Product']['country'] as $pkey => $prod) {
                    $data['ProductCountry']['product_id'] = $id;
                    $data['ProductCountry']['country_id'] = $prod;
                    $this->ProductCountry->create();
                    if ($this->ProductCountry->save($data)) {
                        $f = 1;
                    } else {
                        $f = 0;
                    }
                }
            }
            if ($f == 1) {
                $this->Session->setFlash(__('The Countries Added Successfuly.'), 'success');
                return $this->redirect(array('controller' => 'products', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Countries could not be Added. Please, try again.'), 'error');
                return $this->redirect(array('controller' => 'products', 'action' => 'index'));
            }
        }
        $res = $this->Product->avl_country($id);

        $this->loadModel('Country');
        $this->loadModel('ProductCountry');
        $product = $this->Product->find('first', array('conditions' => array('Product.id' => $id), 'fields' => array('Product.product_name', 'Product.id', 'Product.is_active', 'Category.category_name','Product.product_faq'), 'recursive' => 0));
        $select_country = $this->ProductCountry->find('list', array('conditions' => array('ProductCountry.product_id' => $id), 'fields' => array('ProductCountry.country_id', 'ProductCountry.country_id')));
        $country = $this->Country->find('list', array('fields' => array('Country.id', 'Country.country_name')));
        $this->set(compact('country', 'product', 'select_country', 'pro_id', 'res'));
    }

    public function home_search_autofill($data = null) {
        $this->layout = false;
        if ($this->request->is('post')) {
            $final = $this->Product->search_autofill_function($this->request->data);
            $this->set(compact('final'));
        }
    }

    public function download_sample_file() {
        $this->viewClass = 'Media';
        $params = array(
            'id' => 'products_sample.xls',
            'name' => 'products_sample',
            'download' => true,
            'extension' => 'xls',
            'path' => APP . WEBROOT_DIR . DS . 'files' . DS
        );
        $this->set($params);
    }

    public function change_status_selected() {
        if ($this->request->is('post')) {
            if (isset($this->request->data['activate']) && !empty($this->request->data['Product']['selected_products'])) {
                $id = explode(',', $this->request->data['Product']['selected_products']);
                if ($this->Product->updateAll(array('Product.is_active' => 1), array('Product.id' => $id))) {
                    $this->Session->setFlash(__('The Products has been Activated.'), 'success');
                    return $this->redirect($this->referer());
                } else {
                    $this->Session->setFlash(__('The Products could not be Activated. Please, try again.'), 'error');
                    return $this->redirect($this->referer());
                }
            } elseif (isset($this->request->data['deactivate']) && !empty($this->request->data['Product']['selected_products'])) {
                $id = explode(',', $this->request->data['Product']['selected_products']);

                if ($this->Product->updateAll(array('Product.is_active' => 0), array('Product.id' => $id))) {
                    $this->Session->setFlash(__('The Products has been DeActivated.'), 'success');
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The Products could not be DeActivated. Please, try again.'), 'error');
                    return $this->redirect($this->referer(array('action' => 'index')));
                }
            } elseif (isset($this->request->data['delete']) && !empty($this->request->data['Product']['selected_products'])) {
                $id = explode(',', $this->request->data['Product']['selected_products']);
                if ($this->Product->deleteAll(array('Product.id' => $id))) {
                    $this->Session->setFlash(__('The Products has been Deleted.'), 'success');
                    return $this->redirect($this->referer());
                } else {
                    $this->Session->setFlash(__('The Product could not be Deleted. Please, try again.'), 'error');
                    return $this->redirect($this->referer());
                }
            }
        }
    }

    public function product_preview($id = null) {
        $product = $this->Product->find('first', array('conditions' => array('Product.id' => $id)));
        if (empty($product)) {
            return $this->redirect(array('controller' => 'users', 'action' => 'page_not_found'));
        }













        if ($this->request->is('post')) {

            if ($this->Session->read('captcha_code') != $this->request->data['Enquiry']['captcha']) {
                $this->Session->setFlash(__('Please enter correct captcha code.'), 'error');
                return $this->redirect($this->referer());
            }
            $cat_id = $this->Product->find('first', array('conditions' => array('Product.id' => $id), 'fields' => array('Product.category_id'), 'recursive' => -1));
            $pro_name = $this->Product->find('first', array('conditions' => array('Product.id' => $id), 'fields' => array('Product.product_name'), 'recursive' => -1));

            //debug($this->request->data);
            if (!$this->Auth->user('id')) {
                if (empty($this->request->data['Enquiry']['mobile']) || empty($this->request->data['Enquiry']['email']) || empty($this->request->data['Enquiry']['first_name']) || empty($this->request->data['Enquiry']['last_name'])) {
                    $this->redirect($this->referer());
                    $this->Session->setFlash(__('Please Fill Form '), 'error');
                }
                $this->loadModel('Enquiry');
                $this->loadModel('User');
                $user_details = $this->Enquiry->User->find('first', array('conditions' => array('User.email' => $this->request->data['Enquiry']['email']), 'recursive' => -1));
                // debug($user_details);//die;
                // $cat_id = $this->Product->find('first', array('conditions' => array('Product.id' => $id), 'fields' => array('Product.category_id'), 'recursive' => -1));

                if (!empty($user_details)) {

                    $enq_data['Enquiry']['product_id'] = $id;
                    $enq_data['Enquiry']['category_id'] = $cat_id['Product']['category_id'];
                    $enq_data['Enquiry']['user_id'] = $user_details['User']['id'];
                    $enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];
                    $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                    $enq_data['Enquiry']['mobile'] = $this->request->data['Enquiry']['mobile'];
                    $enq_data['Enquiry']['ref_page'] = $this->request->data['Enquiry']['ref_page'];
                    $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                    $data['User']['first_name'] = $user_details['User']['first_name'];
                    $data['User']['last_name'] = $user_details['User']['last_name'];
                    $data['User']['id'] = $user_details['User']['id'];
                    $data['User']['email'] = $user_details['User']['email'];
                    $data['User']['job_title'] = $user_details['User']['job_title'];
                    $this->loadModel('Category');
                    $category = $this->Category->find('first', array(
                        'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
                    ));


                    $this->set(compact('category', 'pro_name'));
                    $this->Enquiry->create();
                    if ($this->Enquiry->save($enq_data)) {
                        $enq_id = $this->Enquiry->id;
                        $email = new CakeEmail();
                        $email->config('user_enquiery_toc');
                        $email->to($data['User']['email']);
                        $email->viewVars(compact('data', 'pro_name', 'category'));
                        $email->subject('TOC Enquiry Recieved ');
                        $email->send();

                        $this->loadModel('Setting');
                        $mail_to = $this->Setting->find_setting(1);
                        if (!empty($mail_to['Setting']['mail_to'])) {
                            $email_admin = new CakeEmail();
                            $email_admin->config('user_enquiery_received');
                            $email_admin->to($mail_to['Setting']['mail_to']);
                            $email_admin->viewVars(compact('data', 'pro_name', 'enq_id', 'category', 'enq_data'));
                            $email_admin->subject('New TOC enquiry Recieved');
                            $email_admin->send();
                        }


                        $this->Session->setFlash(__('Enquiry Received Successfully'), 'success');
                        return $this->redirect(array('controller' => 'enquiries', 'action' => 'user_enquiry_index', $data['User']['id']));
                    }
                } else {
                    $this->loadModel('Enquiry');
                    $this->loadModel('User');

                    $v_code = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz123456789', 5)), 0, 5);
                    $data['User']['role'] = 11;
                    $data['User']['first_name'] = $this->request->data['Enquiry']['first_name'];
                    $data['User']['last_name'] = $this->request->data['Enquiry']['last_name'];
                    $data['User']['email'] = $this->request->data['Enquiry']['email'];
                    $data['User']['mobile'] = $this->request->data['Enquiry']['mobile'];
                    $data['User']['organisation'] = $this->request->data['Enquiry']['organisation'];
                    $data['User']['visited_ip'] = $this->request->clientIp();
                    $data['User']['job_title'] = $this->request->data['Enquiry']['job_title'];
                    $data['User']['password'] = $v_code;
                    $data['User']['role'] = 11;
                    // debug($data);
                    if ($this->Enquiry->User->save($data)) {
                        $data['User']['id'] = $this->Enquiry->User->id;
                        $enq_data['Enquiry']['product_id'] = $id;
                        $enq_data['Enquiry']['category_id'] = $cat_id['Product']['category_id'];
                        $enq_data['Enquiry']['user_id'] = $data['User']['id'];
                        $enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];
                        $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                        $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                        $enq_data['Enquiry']['ref_page'] = $this->request->data['Enquiry']['ref_page'];
                        $enq_data['Enquiry']['mobile'] = $this->request->data['Enquiry']['mobile'];
                        // debug($enq_data);die;
                        $this->Enquiry->create();
                        $this->Enquiry->save($enq_data);
                        $enq_id = $this->Enquiry->id;
                        $this->loadModel('Category');
                        $category = $this->Category->find('all', array(
                            'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
                        ));
                        $this->set(compact('category'));

                        $email = new CakeEmail();
                        $email->config('user_registration_enquiery');
                        $email->to($data['User']['email']);
                        $email->viewVars(compact('data', 'pro_name', 'v_code', 'category'));
                        $email->subject('Online Registration on Value Market Research');
                        $email->send();

                        $this->loadModel('Setting');
                        $mail_to = $this->Setting->find_setting(1);
                        if (!empty($mail_to['Setting']['mail_to'])) {
                            $email_admin = new CakeEmail();
                            $email_admin->config('user_enquiery_received');
                            $email_admin->to($mail_to['Setting']['mail_to']);
                            $email_admin->viewVars(compact('data', 'pro_name', 'enq_id', 'category', 'enq_data'));
                            $email_admin->subject('New TOC Lead Recieved');
                            $email_admin->send();
                        }

                        if ($this->Auth->login($data['User'])) {
                            if (AuthComponent::user('role') == 11) {
                                $this->Session->setFlash(__('A mail has been sent to your mailbox . Please use Username And Password For Further Login.'), 'success');
                                return $this->redirect(array('controller' => 'enquiries', 'action' => 'user_enquiry_index', $data['User']['id']));
                            } else {
                                $this->Session->setFlash(__('Your account has been successfully verified.'), 'success');
                                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                            }
                        } else {
                            $this->Session->setFlash(__('Unauthorised user details, verification failed'), 'error');
                            return $this->redirect(array('action' => 'login'));
                        }
                    } else {
                        $this->Session->setFlash(__('Invalid information please try again'), 'success');
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                    }
                }
            } else {
                $this->loadModel('Enquiry');
                $this->loadModel('User');
                //debug($this->request->data);die;
                $enq_data['Enquiry']['product_id'] = $id;
                $enq_data['Enquiry']['category_id'] = $cat_id['Product']['category_id'];
                $enq_data['Enquiry']['user_id'] = $this->Auth->user('id');
                $enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];
                $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                $enq_data['Enquiry']['mobile'] = $this->request->data['Enquiry']['mobile'];
                $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                $enq_data['Enquiry']['ref_page'] = $this->request->data['Enquiry']['ref_page'];
                if ($this->Auth->user('mobile') != $this->request->data['Enquiry']['mobile']) {
                    $user_temp['User']['id'] = $this->Auth->user('id');
                    $user_temp['User']['mobile'] = $this->request->data['Enquiry']['mobile'];
                    $this->Enquiry->User->save($user_temp);
                }
                $data['User']['first_name'] = $this->Auth->user('first_name');
                $data['User']['last_name'] = $this->Auth->user('last_name');
                $data['User']['id'] = $this->Auth->user('id');
                $data['User']['email'] = $this->Auth->user('email');
                $data['User']['job_title'] = $this->Auth->user('job_title');
                $data['User']['visited_ip'] = $this->request->clientIp();
                $this->loadModel('Category');
                $category = $this->Category->find('all', array(
                    'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
                ));
                $this->set(compact('category'));
                //debug($enq_data);die;
                $this->Enquiry->create();
                if ($this->Enquiry->save($enq_data)) {
                    $enq_id = $this->Enquiry->id;
                    $email = new CakeEmail();
                    $email->config('user_enquiery_toc');
                    $email->to($data['User']['email']);
                    $email->viewVars(compact('data', 'pro_name', 'category'));
                    $email->subject('Enquiry Recieved ');
                    $email->send();
                    $this->loadModel('Setting');
                    $mail_to = $this->Setting->find_setting(1);
                    if (!empty($mail_to['Setting']['mail_to'])) {
                        $email_admin = new CakeEmail();
                        $email_admin->config('user_enquiery_received');
                        $email_admin->to($mail_to['Setting']['mail_to']);
                        $email_admin->viewVars(compact('data', 'pro_name', 'enq_id', 'category', 'enq_data'));
                        $email_admin->subject('New TOC Lead Recieved');
                        $email_admin->send();
                    }
                    $this->Session->setFlash(__('Enquiry Received Successfully'), 'success');
                    return $this->redirect(array('controller' => 'enquiries', 'action' => 'user_enquiry_index', $data['User']['id']));
                } else {
                    $this->Session->setFlash(__('The enquiry could not be saved. Please, try again.'), 'error');
                    return $this->redirect($this->referer());
                }
            }
        }










        //  $res = $this->Product->check_url($id, $main, $slug);
        $this->loadModel('Category');
        $this->loadModel('ProductCategory');
        $this->loadModel('ProductCountry');
        $category_names = $this->ProductCategory->find('all', array('conditions' => array('ProductCategory.product_id' => $id, 'Category.is_active' => 1)));

        $rand_cat_ids = array();
        foreach ($category_names as $cat_list) {
            array_push($rand_cat_ids, $cat_list['ProductCategory']['category_id']);
        }

        $cat_array = array();
        foreach ($category_names as $key => $cat) {
            $cat_array[$cat['Category']['id']] = $cat['Category']['category_name'];
        }
        $product_country = $this->ProductCountry->find('all', array('conditions' => array('ProductCountry.product_id' => $id)));
        foreach ($product_country as $key => $country) {
            $pro_country[$country['Country']['id']] = $country['Country']['country_name'];
            $c_arry[$country['Country']['id']] = $country['Country']['id'];
        }

        $related_product = $this->ProductCategory->find('all', array(
            'conditions' => array('ProductCategory.category_id' => $rand_cat_ids, 'ProductCategory.product_id NOT' => $id, 'Product.is_active' => 1),
            'limit' => 10,
            'group' => array('ProductCategory.product_id'),
            'order' => 'rand()'));
        $meta_name = $product['Product']['meta_name'];
        $meta_desc = $product['Product']['meta_desc'];
        $meta_keyword = $product['Product']['meta_keywords'];

        $bredcum_array = array();
        $bredcum_array = array();

        $bredcum_array = $this->Category->bread_cum_function($product['Product']['category_id']);
        foreach ($related_product as $pr_key => $pro) {
            $main_cat = $this->Product->find_main_cat($pro['Product']['category_id']);
            if (!empty($main_cat)) {
                $related_product[$pr_key]['main_cat'] = $main_cat['Category'];
            } else {
                $related_product[$pr_key]['main_cat'] = 'products';
            }
        }
        //  debug($product);die;
        $this->set(compact('product', 'bredcum_array', 'meta_name', 'meta_desc', 'meta_keyword', 'related_product', 'cat_array', 'pro_country'));
    }

//update all slug with main category
    public function set_cat_slug() {
        $all_prod = $this->Product->find('all', array('recursive' => 0));
        foreach ($all_prod as $key => $product) {
            $slug = $product['Product']['product_name'];
            $slug = $slug . ' ' . $product['Category']['category_name'];
            $prodcut_slug = $this->Product->cleanString($slug);
            $this->Product->updateAll(array('Product.slug' => "'$prodcut_slug'"), array('Product.id' => $product['Product']['id']));
        }
        die;
    }

    public function update_product_seo() {
        $all_product = $this->Product->find('list', array('fields' => array('Product.id', 'Product.id')));
        foreach ($all_product as $pkey => $id) {
            $this->Product->update_seo($id);
        }
        echo 'Done';
        die;
    }

    public function activate_product($prod_id = null) {
        if ($prod_id == null) {
            $this->redirect($this->referer());
        }
        $this->Product->updateAll(array('Product.is_active' => 1), array('Product.id' => $prod_id));
        $this->Session->setFlash(__('The product has been activated.'), 'success');
        $this->redirect($this->referer());
    }

    public function ck_img_up() {
        //         $this->layout = null;
        // debug($this->request->params['form']['upload']);
        // debug($_GET);
        // die;
        
            //code starts here-VG-11/08/2016

            $funcNum = $_GET['CKEditorFuncNum'] ;

            // Optional: instance name (might be used to load a specific configuration file or anything else).

            $CKEditor = $_GET['CKEditor'] ;

            // Optional: might be used to provide localized messages.

            $langCode = $_GET['langCode'] ;

            // Check the $_FILES array and save the file. Assign the correct path to a variable ($url).

            $ext = pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);

            $upload_path = APP.WEBROOT_DIR.'/images';

            $filename = $upload_path.'\\'.$_FILES['upload']['name'];
            
            $success = move_uploaded_file($_FILES['upload']['tmp_name'], $upload_path . DS . $_FILES['upload']['name']);
            
            $url = Router::url('/', true).'images/'.$_FILES['upload']['name'];

            // Usually you will only assign something here if the file could not be uploaded.

            $message = '';

            // debug($url);die;
            echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
            die;

         //code ends here-VG-11/08/2016
    }

    public function elfinder_browser() {
        $this->layout = null;
    }
    /* public function add_alias() {
      $this->Product->recursive = -1;
      $products = $this->Product->find('list', array('fields' => array('id', 'meta_name')));
      //$filename = substr(strrchr($products['54'], "-"), 0);
      foreach ($products as $key => $val) {
      $filename = str_replace(" - valuemarketresearch.com", "", $val);
      $this->Product->updateAll(array('Product.alias'=>"'".$filename."'"),array('Product.id'=>$key));
      }
      die;
      } */

    public function update_company_reports(){
        $this->layout = null;
        $this->Product->update_company_reports();
        die;
    }


    public function reportlist() {
        $page = 1;
        if(!empty($this->params['page'])){
            $page = $this->params['page'];
        }
        $this->paginate = array(
            'conditions' => array('Product.is_active' => 1, 'Product.is_upcoming' => 0),
            'order' => 'Product.id DESC',
            'page' => $page,
            'limit' => 25
        );
        //    $this->Product->recursive = 0;
        $this->loadModel('Category');
        $cat = $this->Category->find('list', array('conditions' => array('Category.level' => 1),
            'fields' => array('category_name'), 'recursive' => -1));
        //debug($cat);die;
        $products = $this->Paginator->paginate();

    // Research Industries (Category and subCategory) code start here

                // $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.is_active' => 1, 'Category.display_home' => 1), 'recursive' => -1));
                // $research_ind = array();
                // foreach ($research_ind_cat as $key1 => $cat) {
                //     $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
                //     $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
                //     $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
                //     $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
                    $level_two = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => 2, 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_two as $key2 => $level2) {
                        $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                        $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                        $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                        $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_three as $key3 => $level3) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                            $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_four as $key4 => $level4) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                                $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                                foreach ($level_five as $key5 => $level5) {
                                    $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                                    $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                                    $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                                    $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                                    $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                                    foreach ($level_six as $key6 => $level6) {
                                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                                    }
                                }
                            }
                        }
                    }
                // }

        // Research Industries (Category and subCategory) code end here

        
        
        $meta_name = "Market Research Reports - Top and Trending";
        $meta_keyword="Market Research Reports, Top Market Research Reports, Trending Market Research Reports, Browse Research Reports, Industrial Research Reports, VMR Reports, Best Market Research Reports";
        $meta_desc = "Browse our list of trending market research reports from 16+ top global industries. Get accurate and precise data at the best value.";

        $this->set(compact('research_ind','products', 'cat','meta_desc','meta_keyword','meta_name'));
    }

    public function upcomingreport() {
        $page = 1;
        if(!empty($this->params['page'])){
            $page = $this->params['page'];
        }
        $this->paginate = array(
            'conditions' => array('Product.is_active' => 1, 'Product.is_upcoming' => 1),
            'order' => 'Product.id DESC',
            'page' => $page,
            'limit' => 25
        );
        //    $this->Product->recursive = 0;
        $this->loadModel('Category');
        $cat = $this->Category->find('list', array('conditions' => array('Category.level' => 1),
            'fields' => array('category_name'), 'recursive' => -1));
        //debug($cat);die;
        $products = $this->Paginator->paginate();

    // Research Industries (Category and subCategory) code start here

        $level_two = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => 2, 'Category.is_active' => 1), 'recursive' => -1));
        foreach ($level_two as $key2 => $level2) {
            $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
            $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
            $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
            $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
            $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
            foreach ($level_three as $key3 => $level3) {
                $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_four as $key4 => $level4) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                    $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_five as $key5 => $level5) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                        $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_six as $key6 => $level6) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                        }
                    }
                }
            }
        }

        // Research Industries (Category and subCategory) code end here
        
        $meta_name = "Upcoming Market Research Reports - Top and Trending";
        $meta_keyword = "Market Research Reports, Top Market Research Reports, Upcoming Market Research Reports, Browse Research Reports, Industrial Research Reports, VMR Reports, Best Market Research Reports";
        $meta_desc = "Browse our list of upcoming market research reports from 16+ top global industries. Get accurate and precise data at the best value.";
        // $meta_name = "Market Research Reports - Top and Trending";
        // $meta_keyword="Market Research Reports, Top Market Research Reports, Trending Market Research Reports, Browse Research Reports, Industrial Research Reports, VMR Reports, Best Market Research Reports";
        // $meta_desc = "Browse our list of trending market research reports from 16+ top global industries. Get accurate and precise data at the best value.";

        $this->set(compact('research_ind','products', 'cat','meta_desc','meta_keyword','meta_name'));
    }


    public function cron() {
        $this->layout = false;
        $conditions_status["Product.is_active"] = 2;
        $ready_to_active = $this->Product->find('list', array(
            'conditions' => array($conditions_status),
            'fields' => array('Product.id', 'Product.product_name'),
        ));
        $cnt_ready_to_active = count($ready_to_active);
        $conditions_status["Product.is_active"] = 1;
        $conditions_dt["date(Product.modified)"] = date('Y-m-d');
        $active_today = $this->Product->find('list', array(
            'conditions' => array($conditions_status, $conditions_dt),
            'fields' => array('Product.id', 'Product.product_name'),
        ));
        $cnt_active_today = count($active_today);
        $this->loadModel('Setting');
        $mail_to = $this->Setting->find_setting(1);
        if (!empty($mail_to['Setting']['mail_to'])) {
            $email = new CakeEmail();
            $email->config('cron_job');
            $email->to($mail_to['Setting']['mail_to']);
            $email->viewVars(compact('ready_to_active', 'cnt_ready_to_active','active_today', 'cnt_active_today'));
            $email->subject(date('d M y') . ' RD Active Status Report');
            $email->send();
            echo "True";
        }
        // $this->set(compact('ready_to_active', 'cnt_ready_to_active','active_today', 'cnt_active_today'));
    }
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(array('elfinder_browser', 'cron',  'reportlist','upcomingreport','ck_img_up', 'category_listing', 'clear_product_filter_pending','rss',
            'update_product_seo', 'category_listing_preview', 'price_session', 'publish_session', 'set_cat_slug', 'clear_product_filter',
            'change_status_selected', 'category_details','category_details_new', 'payment', 'add_to_cart', 'billing_info', 'home_search_autofill', 'latest_market_reports'));
            if (isset($this->request->params['page'])) {

                $this->request->params['named']['page'] = $this->request->params['page'];
        
            }elseif( isset($this->request->query['page']) ){
        
                $this->request->params['named']['page'] = $this->request->query['page'];
        
            } 
        }


    public function add_faq($id=null) {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {              
            $this->request->data['id'] = $id;
            $res = $this->Product->add_prod_faq($this->request->data);              
            if ($res == 1) {
                $this->Product->update_prod_count_admin();
                $this->Session->setFlash(__('The Product FAQ has been saved.'), 'success');
                // return $this->redirect(array('action' => 'add_category', $id));  
                return $this->redirect(array('controller' => 'specifications', 'action' => 'add_product_spc', $id));
            } else {
                $this->Session->setFlash(__('The Product FAQ could not be saved. Please, try again.'), 'error');
            }
        }  
        $res = $this->Product->avl_country($id);      
        $prod_data = $this->Product->find('first', array('conditions' => array('Product.id' => $id), 'recursive' => 0));
        $prod_name = $this->Product->find('first', array('conditions' => array('Product.id' => $id), 'fields' => array('Product.product_name', 'Category.category_name'), 'recursive' => 0));
        $pro_id = $id;        
        $this->set(compact('prod_data', 'pro_id', 'prod_name','res'));
    }

    public function edit_faq($id=null) {
        $this->layout = 'admin_layout';
        $this->layout = 'admin_layout';
        if ($this->request->is('post','put')) {            
            $this->request->data['id'] = $id;
            $res = $this->Product->add_prod_faq($this->request->data);            
            if ($res == 1) {
                $this->Product->update_prod_count_admin();                                
                $this->Session->setFlash(__('The Product FAQ has been saved.'), 'success');                           
            } else {
                $this->Session->setFlash(__('The Product FAQ could not be saved. Please, try again.'), 'error');
            }
        }    
        $res = $this->Product->avl_country($id);    
        $prod_data = $this->Product->find('first', array('conditions' => array('Product.id' => $id), 'recursive' => 0));
        $prod_name = $this->Product->find('first', array('conditions' => array('Product.id' => $id), 'fields' => array('Product.product_name', 'Category.category_name'), 'recursive' => 0));
        $pro_id = $id; 
        $questions_answers_array = array();          
        $questions_answers_array = (!empty($prod_data['Product']['product_faq'])) ? json_decode($prod_data['Product']['product_faq'],TRUE) : NULL;       
        $this->set(compact('prod_data', 'pro_id', 'prod_name','res','questions_answers_array'));
    }

}
