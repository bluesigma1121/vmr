<?php

App::uses('AppController', 'Controller');

/**
 * SiteVisits Controller
 *
 * @property SiteVisit $SiteVisit
 * @property PaginatorComponent $Paginator
 */
class SiteVisitsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    /* public function index() {
      $this->SiteVisit->recursive = 0;
      $this->set('siteVisits', $this->Paginator->paginate());
      } */

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    /* public function view($id = null) {
      if (!$this->SiteVisit->exists($id)) {
      throw new NotFoundException(__('Invalid site visit'));
      }
      $options = array('conditions' => array('SiteVisit.' . $this->SiteVisit->primaryKey => $id));
      $this->set('siteVisit', $this->SiteVisit->find('first', $options));
      } */

    /**
     * add method
     *
     * @return void
     */
    /* public function add() {
      if ($this->request->is('post')) {
      $this->SiteVisit->create();
      if ($this->SiteVisit->save($this->request->data)) {
      $this->Session->setFlash(__('The site visit has been saved.'));
      return $this->redirect(array('action' => 'index'));
      } else {
      $this->Session->setFlash(__('The site visit could not be saved. Please, try again.'));
      }
      }
      } */

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    /* public function edit($id = null) {
      if (!$this->SiteVisit->exists($id)) {
      throw new NotFoundException(__('Invalid site visit'));
      }
      if ($this->request->is(array('post', 'put'))) {
      if ($this->SiteVisit->save($this->request->data)) {
      $this->Session->setFlash(__('The site visit has been saved.'));
      return $this->redirect(array('action' => 'index'));
      } else {
      $this->Session->setFlash(__('The site visit could not be saved. Please, try again.'));
      }
      } else {
      $options = array('conditions' => array('SiteVisit.' . $this->SiteVisit->primaryKey => $id));
      $this->request->data = $this->SiteVisit->find('first', $options);
      }
      } */

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    /* public function delete($id = null) {
      $this->SiteVisit->id = $id;
      if (!$this->SiteVisit->exists()) {
      throw new NotFoundException(__('Invalid site visit'));
      }
      $this->request->onlyAllow('post', 'delete');
      if ($this->SiteVisit->delete()) {
      $this->Session->setFlash(__('The site visit has been deleted.'));
      } else {
      $this->Session->setFlash(__('The site visit could not be deleted. Please, try again.'));
      }
      return $this->redirect(array('action' => 'index'));
      } */

    public function visit_report() {
        $this->layout = 'admin_layout';
        $this->SiteVisit->recursive = 0;
        $visit_count = $this->SiteVisit->find('count');
        //debug($visit_count);
        $limit = 1000;
        $offset = 0;
        $stats = array();
        $loop_limit = $visit_count / $limit;
        for ($i = 0; $i <= $loop_limit; $i++) {
            if ($i != 0) {
                $offset = $limit * $i;
            }
            $visits = $this->SiteVisit->find('all', array('limit' => $limit, 'offset' => $offset, 'order' => 'SiteVisit.id DESC'));
            //debug($visits);
            foreach ($visits as $visit) {
                if (!isset($stats[$visit['SiteVisit']['ip']]['count'])) {
                    $stats[$visit['SiteVisit']['ip']]['count'] = 1;
                    $stats[$visit['SiteVisit']['ip']]['id'] = $visit['SiteVisit']['id'];
                    $stats[$visit['SiteVisit']['ip']]['city'] = $visit['SiteVisit']['city'];
                    $stats[$visit['SiteVisit']['ip']]['state'] = $visit['SiteVisit']['state'];
                    $stats[$visit['SiteVisit']['ip']]['country'] = $visit['SiteVisit']['country'];
                    $stats[$visit['SiteVisit']['ip']]['date'] = $visit['SiteVisit']['created'];
                } else {
                    $stats[$visit['SiteVisit']['ip']]['count'] += 1;
                }
            }
        }
        $unique_visits = count($stats);
        $this->set(compact('visit_count', 'unique_visits', 'stats'));
    }
}
