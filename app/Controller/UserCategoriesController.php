<?php
App::uses('AppController', 'Controller');
/**
 * UserCategories Controller
 *
 * @property UserCategory $UserCategory
 * @property PaginatorComponent $Paginator
 */
class UserCategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->UserCategory->recursive = 0;
		$this->set('userCategories', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->UserCategory->exists($id)) {
			throw new NotFoundException(__('Invalid user category'));
		}
		$options = array('conditions' => array('UserCategory.' . $this->UserCategory->primaryKey => $id));
		$this->set('userCategory', $this->UserCategory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->UserCategory->create();
			if ($this->UserCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The user category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user category could not be saved. Please, try again.'));
			}
		}
		$users = $this->UserCategory->User->find('list');
		$categories = $this->UserCategory->Category->find('list');
		$this->set(compact('users', 'categories'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->UserCategory->exists($id)) {
			throw new NotFoundException(__('Invalid user category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UserCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The user category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user category could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UserCategory.' . $this->UserCategory->primaryKey => $id));
			$this->request->data = $this->UserCategory->find('first', $options);
		}
		$users = $this->UserCategory->User->find('list');
		$categories = $this->UserCategory->Category->find('list');
		$this->set(compact('users', 'categories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->UserCategory->id = $id;
		if (!$this->UserCategory->exists()) {
			throw new NotFoundException(__('Invalid user category'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->UserCategory->delete()) {
			$this->Session->setFlash(__('The user category has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
