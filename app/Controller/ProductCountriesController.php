<?php
App::uses('AppController', 'Controller');
/**
 * ProductCountries Controller
 *
 * @property ProductCountry $ProductCountry
 * @property PaginatorComponent $Paginator
 */
class ProductCountriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProductCountry->recursive = 0;
		$this->set('productCountries', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProductCountry->exists($id)) {
			throw new NotFoundException(__('Invalid product country'));
		}
		$options = array('conditions' => array('ProductCountry.' . $this->ProductCountry->primaryKey => $id));
		$this->set('productCountry', $this->ProductCountry->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProductCountry->create();
			if ($this->ProductCountry->save($this->request->data)) {
				$this->Session->setFlash(__('The product country has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product country could not be saved. Please, try again.'));
			}
		}
		$products = $this->ProductCountry->Product->find('list');
		$countries = $this->ProductCountry->Country->find('list');
		$this->set(compact('products', 'countries'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProductCountry->exists($id)) {
			throw new NotFoundException(__('Invalid product country'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProductCountry->save($this->request->data)) {
				$this->Session->setFlash(__('The product country has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product country could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProductCountry.' . $this->ProductCountry->primaryKey => $id));
			$this->request->data = $this->ProductCountry->find('first', $options);
		}
		$products = $this->ProductCountry->Product->find('list');
		$countries = $this->ProductCountry->Country->find('list');
		$this->set(compact('products', 'countries'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProductCountry->id = $id;
		if (!$this->ProductCountry->exists()) {
			throw new NotFoundException(__('Invalid product country'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ProductCountry->delete()) {
			$this->Session->setFlash(__('The product country has been deleted.'));
		} else {
			$this->Session->setFlash(__('The product country could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
