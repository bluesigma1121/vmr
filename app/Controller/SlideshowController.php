<?php
App::uses('AppController', 'Controller');
/**
 * Slideshow Controller
 *
 * @property Slideshow $Slideshow
 * @property PaginatorComponent $Paginator
 */
class SlideshowController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'admin_layout';
		$this->Slideshow->recursive = 0;
		$this->set('slideshows', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'admin_layout';
		if (!$this->Slideshow->exists($id)) {
			throw new NotFoundException(__('Invalid slideshow'));
		}
		$options = array('conditions' => array('Slideshow.' . $this->Slideshow->primaryKey => $id));
		$this->set('slideshow', $this->Slideshow->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'admin_layout';
		if ($this->request->is('post')) {
			// echo "<pre>";
			// print_r($this->request->data);exit;
			$this->Slideshow->create();
			$res = $this->Slideshow->save_image($this->request->data);
			if ($res == 1) {
				$this->Session->setFlash(__('The slideshow has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The slideshow could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'admin_layout';
		if (!$this->Slideshow->exists($id)) {
			throw new NotFoundException(__('Invalid slideshow'));
		}
		if ($this->request->is(array('post', 'put'))) {

			if ($this->Slideshow->edit_image($this->request->data)) {
				$this->Session->setFlash(__('The slideshow has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The slideshow could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Slideshow.' . $this->Slideshow->primaryKey => $id));
			$this->request->data = $this->Slideshow->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'admin_layout';
		$this->Slideshow->id = $id;
		if (!$this->Slideshow->exists()) {
			throw new NotFoundException(__('Invalid slideshow'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Slideshow->delete()) {
			$this->Session->setFlash(__('The slideshow has been deleted.'));
		} else {
			$this->Session->setFlash(__('The slideshow could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
