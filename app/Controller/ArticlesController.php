<?php

App::uses('AppController', 'Controller');

/**
 * Articles Controller
 *
 * @property Article $Article
 * @property PaginatorComponent $Paginator
 */
class ArticlesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator','RequestHandler');
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->layout = 'admin_layout';
        $this->Article->recursive = 0;
        $this->paginate = array(
            'order' => 'Article.id DESC',
            'limit' => 20
        );
        $this->set('articles', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Article->exists($id)) {
            throw new NotFoundException(__('Invalid article'));
        }
        $options = array('conditions' => array('Article.' . $this->Article->primaryKey => $id));
        $article = $this->Article->find('first', $options);
        Configure::load('idata');
        $ar_type = Configure::read('idata.article_type');
        $heading = $ar_type[$article['Article']['article_type']];
        $this->set(compact('article','heading'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->layout = 'admin_layout';
        Configure::load('idata');
        $article_type = Configure::read('idata.article_type');
        $this->set(compact('article_type'));
        if ($this->request->is('post')) {
            $this->Article->create();
            if ($this->Article->save($this->request->data)) {
                $this->Session->setFlash(__('The article has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
            }
        }
        $categories = $this->Article->Category->find('list', array(
            'fields' => array('id', 'category_name'),
            'conditions' => array('Category.parent_category_id' => 0)));

        $this->loadModel('Product');
        $products = $this->Product->find('list', array('conditions' => array('Product.is_active' => 1, 'Product.is_deleted' => 0, 'Product.is_upcoming' => 0), 'fields'=>array('id','alias'), 'recursive' => 0,'order'=>array('Product.product_name ASC'),));
        
        $this->set(compact('categories', 'products'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->layout = 'admin_layout';
        Configure::load('idata');
        $article_type = Configure::read('idata.article_type');
        $this->set(compact('article_type'));
        if (!$this->Article->exists($id)) {
            throw new NotFoundException(__('Invalid article'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Article->id = $id;
            $this->request->data['Article']['id'] = $id;
            if ($this->Article->save($this->request->data)) {
                $this->Session->setFlash(__('The article has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Article.' . $this->Article->primaryKey => $id));
            $this->request->data = $this->Article->find('first', $options);
        }
        $categories = $this->Article->Category->find('list', array(
            'fields' => array('id', 'category_name'),
            'conditions' => array('Category.parent_category_id' => 0)));

        $this->loadModel('Product');
        $products = $this->Product->find('list', array('conditions' => array('Product.is_active' => 1, 'Product.is_deleted' => 0, 'Product.is_upcoming' => 0), 'fields'=>array('id','alias'), 'recursive' => 0,'order'=>array('Product.product_name ASC'),));

        $this->set(compact('categories', 'products'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Article->delete()) {
            $this->Session->setFlash(__('The article has been deleted.'));
        } else {
            $this->Session->setFlash(__('The article could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function generate(){
        
        $this->layout = '';
        $article = $this->Article->find('all');
        $data['article'] = $article;
        $this->set($data);
        $this->render();
    }

    public function article_listing() {
        $type = "press-releases";
        $this->Article->recursive = 0;
        $this->paginate = array(
            'conditions' => array('Article.article_type' => $type),
            'order' => 'Article.id DESC',
            'limit' => 1000
        );
        Configure::load('idata');
        $ar_type = Configure::read('idata.article_type');
        $heading = @$ar_type[$type];
        /* code Start VG-31/01/2017*/
         if($heading == 'Analysis')
        {
            $meta_name = $heading . ' - Value Market Research';
            $meta_keyword = "Free Industry Analysis, Current Market Analysis, Industry Analysis and Expert's review.";
            $meta_desc ="Read our free analysis of trending markets and industries.";
        }
        else
        {
            $meta_name = $heading . ' - Value Market Research';
            $meta_keyword = "Press Releases, Latest PR, Latest Market PR, VMR Press Release";
            $meta_desc = "Press Releases - Empower yourself with our Press Releases with latest Global Market Data and Industry Analysis.";
        }
        /* code End VG-31/01/2017*/
        /*$meta_name = $heading;
        $meta_keyword = $heading;
        $meta_desc = $heading;*/

// Menu-Research Industries (Category and subCategory) code start here
        $this->loadModel('Category');
        // $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
        // foreach ($research_ind_cat as $key1 => $cat) {
        //     $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
        //     $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
        //     $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
        //     $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
            $level_two = $this->Category->find('all', array('conditions' =>
                array(
                    'Category.parent_category_id' => 2,
                    'Category.is_active' => 1), 'recursive' => -1));
            foreach ($level_two as $key2 => $level2) {
                $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
        // }

// Menu-Research Industries (Category and subCategory) code end here



        $articles = $this->Paginator->paginate();
        if(empty($articles)){
            // return $this->redirect(
            //     array('controller' => 'users', 'action' => 'page_not_found')
            // );
            throw new NotFoundException();
            die;
        }
        $this->loadModel('Whyus');
        $whyus=$this->Whyus->find('all',array(
                                  'fields'=>array('title','description'),
                                  'limit' => 4,
                                  ));

        $this->loadModel('Testimonial');
        /*Testimonial code Start VG-15/12/2016*/
        $testimonial=$this->Testimonial->find('all',array(
            'conditions'=>array('Testimonial.is_verified'=>1,'Testimonial.is_active'=>1,'Testimonial.is_deleted'=>0),
            'fields'=>array('id','testimonial_title','testimonial_description','testimonial_image'),
            'recursive'=>-1,
            'order'=>array('Testimonial.id DESC'),
            'limit'=>3,
            ));
        /*Testimonial code End VG-15/12/2016        */

        $this->loadModel('Category');
        $ourclients = $this->Category->our_client_fun();
        $this->set(compact('ourclients','testimonial'));

        $this->set(compact('research_ind','whyus', 'articles', 'heading', 'meta_name', 'meta_keyword', 'meta_desc', 'type'));
    }


    public function analysis_listing() {
        $type = "analysis";
        $this->Article->recursive = 0;
        $this->paginate = array(
            'conditions' => array('Article.article_type' => $type),
            'order' => 'Article.id DESC',
            'limit' => 1000
        );
        Configure::load('idata');
        $ar_type = Configure::read('idata.article_type');
        $heading = @$ar_type[$type];
        
        $meta_name = $heading . ' - Value Market Research';
        $meta_keyword = "Free Industry Analysis, Current Market Analysis, Industry Analysis and Expert's review.";
        $meta_desc ="Read our free analysis of trending markets and industries.";
    
        $articles = $this->Paginator->paginate();
        // if(empty($articles)){
        //     throw new NotFoundException();
        //     die;
        // }
        $this->loadModel('Whyus');
        $whyus=$this->Whyus->find('all',array(
                                  'fields'=>array('title','description'),
                                  'limit' => 4,
                                  ));

        $this->loadModel('Testimonial');
        /*Testimonial code Start VG-15/12/2016*/
        $testimonial=$this->Testimonial->find('all',array(
            'conditions'=>array('Testimonial.is_verified'=>1,'Testimonial.is_active'=>1,'Testimonial.is_deleted'=>0),
            'fields'=>array('id','testimonial_title','testimonial_description','testimonial_image'),
            'recursive'=>-1,
            'order'=>array('Testimonial.id DESC'),
            'limit'=>3,
            ));
        /*Testimonial code End VG-15/12/2016        */

        $this->loadModel('Category');
        $ourclients = $this->Category->our_client_fun();
        $this->set(compact('ourclients','testimonial'));

        $this->set(compact('research_ind','whyus', 'articles', 'heading', 'meta_name', 'meta_keyword', 'meta_desc', 'type'));
    }

    public function article_details($url_slug = null) {
        
        $article = $this->Article->find('first', array('conditions' => array('Article.slug' => $url_slug)));
        $action = '';
        if(!empty($article)) {
            $action = $article['Article']['article_type'] == 'press-releases' ? 'article_listing' : 'analysis_listing';
        }

        if(empty($article)){
            throw new MissingControllerException(__('Invalid article'));
            die;
        }
        Configure::load('idata');
        $ar_type = Configure::read('idata.article_type');
        
        $heading = $article['Article']['article_type'] == 'press-releases' ? "Press Releases" : "Analysis";
        
        $meta_name = $article['Article']['meta_title'];
        $meta_keyword = $article['Article']['meta_keywords'];
        $meta_desc = $article['Article']['meta_desc'];


        $this->loadModel('Whyus');
        $whyus=$this->Whyus->find('all',array(
                                'fields'=>array('title','description'),
                                'limit' => 4,
                                ));

        $this->loadModel('Testimonial');
        /*Testimonial code Start VG-15/12/2016*/
        $testimonial=$this->Testimonial->find('all',array(
            'conditions'=>array('Testimonial.is_verified'=>1,'Testimonial.is_active'=>1,'Testimonial.is_deleted'=>0),
            'fields'=>array('id','testimonial_title','testimonial_description','testimonial_image'),
            'recursive'=>-1,
            'order'=>array('Testimonial.id DESC'),
            'limit'=>3,
            ));
        /*Testimonial code End VG-15/12/2016        */

        $this->loadModel('Category');
        $ourclients = $this->Category->our_client_fun();
        $this->set(compact('ourclients','testimonial'));

        $this->loadModel('Product');
        $product = $this->Product->find('first', array(
            'conditions' => array('Product.id' => $article['Article']['product_id']), 
            'fields'=>array('id','slug'), 
            'recursive' => 0,
        ));
        $this->set(compact('research_ind', 'article', 'whyus', 'heading', 'meta_name', 'meta_keyword', 'meta_desc', 'product', 'action'));
    }

    public function beforeFilter() {
        $this->Auth->allow(array('article_listing', 'article_details','generate', 'analysis_listing'));
    }

}
