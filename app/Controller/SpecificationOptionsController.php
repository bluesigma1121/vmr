<?php

App::uses('AppController', 'Controller');

/**
 * SpecificationOptions Controller
 *
 * @property SpecificationOption $SpecificationOption
 * @property PaginatorComponent $Paginator
 */
class SpecificationOptionsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->SpecificationOption->recursive = 0;
        $this->set('specificationOptions', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->layout = 'admin_layout';
        $options = array('conditions' => array('SpecificationOption.' . $this->SpecificationOption->primaryKey => $id));
        $this->set('specificationOption', $this->SpecificationOption->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add($id = null) {
        $this->layout = 'admin_layout';
        if (empty($id) || $id == NULL) {
            $this->Session->setFlash(__('Invalid Specification ...'), 'error');
            return $this->redirect(array('controller' => 'specifications', 'action' => 'index'));
        }
        if ($this->request->is('post')) {
            $this->request->data['Specification']['spec_id'] = $id;
            $res = $this->SpecificationOption->fun_add_options($this->request->data);
            if ($res == 1) {
                $this->Session->setFlash(__('The specification option has been saved.'), 'success');
                return $this->redirect(array('controller' => 'specifications', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('The specification option could not be saved. Please, try again.'), 'error');
            }
        }
        $spec = $this->SpecificationOption->Specification->find('first', array('conditions' => array('Specification.id' => $id)));
        $this->set(compact('spec'));
    }

    public function add_modal_option() {
        if ($this->request->is('post')) {
            $this->request->data['SpecificationOption']['option_name'] = ucfirst($this->request->data['SpecificationOption']['option_name']);
            if ($this->SpecificationOption->find('first', array('conditions' => array('SpecificationOption.specification_id' => $this->request->data['SpecificationOption']['specification_id'], 'SpecificationOption.option_name' => $this->request->data['SpecificationOption']['option_name'])))) {
                $this->Session->setFlash(__('The specification option could not be saved. Please, try again.'), 'error');
                return $this->redirect(array('controller' => 'specification_options', 'action' => 'view_options', $this->request->data['SpecificationOption']['specification_id']));
            } else {
                $this->SpecificationOption->create();
                if ($this->SpecificationOption->save($this->request->data)) {
                    $this->Session->setFlash(__('The specification option has been saved.'), 'success');
                    return $this->redirect(array('controller' => 'specification_options', 'action' => 'view_options', $this->request->data['SpecificationOption']['specification_id']));
                } else {
                    $this->Session->setFlash(__('The specification option could not be saved. Please, try again.'), 'error');
                    return $this->redirect(array('controller' => 'specification_options', 'action' => 'view_options', $this->request->data['SpecificationOption']['specification_id']));
                }
            }
        }
    }

    public function edit_op() {
        $this->layout = 'admin_layout';
        if ($this->request->is(array('post', 'put'))) {
            if ($this->SpecificationOption->save($this->request->data)) {
                $this->Session->setFlash(__('The specification option has been updated successfully.'), 'success');
                return $this->redirect($this->referer());
            } else {
                $this->Session->setFlash(__('The specification option could not be updated. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('SpecificationOption.' . $this->SpecificationOption->primaryKey => $id));
            $this->request->data = $this->SpecificationOption->find('first', $options);
        }
    }

    public function view_options($id = null) {
        $this->layout = 'admin_layout';
        if (empty($id) || $id == NULL) {
            $this->Session->setFlash(__('Invalid Specification ...'), 'error');
            return $this->redirect(array('controller' => 'specifications', 'action' => 'index'));
        }
        $this->loadModel('Specification');
        if (!$this->Specification->exists($id)) {
            $this->Session->setFlash(__('Invalid Specification ...'), 'error');
            return $this->redirect(array('controller' => 'specifications', 'action' => 'index'));
        }
        if ($this->request->is('post')) {
            $this->request->data['Specification']['spec_id'] = $id;
            $res = $this->SpecificationOption->fun_add_options($this->request->data);
            if ($res == 1) {
                $this->Session->setFlash(__('The specification option has been saved.', 'success'));
                return $this->redirect(array('controller' => 'specifications', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('The specification option could not be saved. Please, try again.'), 'error');
            }
        }
        $spec = $this->SpecificationOption->Specification->find('first', array('conditions' => array('Specification.id' => $id)));
        $this->set(compact('spec'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->SpecificationOption->exists($id)) {
            throw new NotFoundException(__('Invalid specification option'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->SpecificationOption->save($this->request->data)) {
                $this->Session->setFlash(__('The specification option has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The specification option could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('SpecificationOption.' . $this->SpecificationOption->primaryKey => $id));
            $this->request->data = $this->SpecificationOption->find('first', $options);
        }
        $specifications = $this->SpecificationOption->Specification->find('list');
        $this->set(compact('specifications'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->SpecificationOption->id = $id;
        if (!$this->SpecificationOption->exists()) {
            throw new NotFoundException(__('Invalid specification option'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->SpecificationOption->delete()) {
            $this->Session->setFlash(__('The specification option has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The specification option could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect($this->referer());
    }

}
