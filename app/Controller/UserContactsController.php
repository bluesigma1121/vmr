<?php

App::uses('AppController', 'Controller');

/**
 * UserContacts Controller
 *
 * @property UserContact $UserContact
 * @property PaginatorComponent $Paginator
 */
class UserContactsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->UserContact->recursive = 0;
        $this->set('userContacts', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->UserContact->exists($id)) {
            throw new NotFoundException(__('Invalid user contact'));
        }
        $options = array('conditions' => array('UserContact.' . $this->UserContact->primaryKey => $id));
        $this->set('userContact', $this->UserContact->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->UserContact->create();
            if ($this->UserContact->save($this->request->data)) {
                $this->Session->setFlash(__('The user contact has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user contact could not be saved. Please, try again.'));
            }
        }
        $users = $this->UserContact->User->find('list');
        $this->set(compact('users'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->UserContact->exists($id)) {
            throw new NotFoundException(__('Invalid user contact'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->UserContact->save($this->request->data)) {
                $this->Session->setFlash(__('The user contact has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user contact could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('UserContact.' . $this->UserContact->primaryKey => $id));
            $this->request->data = $this->UserContact->find('first', $options);
        }
        $users = $this->UserContact->User->find('list');
        $this->set(compact('users'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->UserContact->id = $id;
        if (!$this->UserContact->exists()) {
            throw new NotFoundException(__('Invalid user contact'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->UserContact->delete()) {
            $this->Session->setFlash(__('The user contact has been deleted.'));
        } else {
            $this->Session->setFlash(__('The user contact could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
