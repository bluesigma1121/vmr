<?php

App::uses('AppController', 'Controller');

/**
 * ProductCategories Controller
 *
 * @property ProductCategory $ProductCategory
 * @property PaginatorComponent $Paginator
 */
class ProductCategoriesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->ProductCategory->recursive = 0;
        $this->set('productCategories', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->ProductCategory->exists($id)) {
            throw new NotFoundException(__('Invalid product category'));
        }
        $options = array('conditions' => array('ProductCategory.' . $this->ProductCategory->primaryKey => $id));
        $this->set('productCategory', $this->ProductCategory->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->ProductCategory->create();
            if ($this->ProductCategory->save($this->request->data)) {
                $this->Session->setFlash(__('The product category has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The product category could not be saved. Please, try again.'));
            }
        }
        $categories = $this->ProductCategory->Category->find('list');
        $products = $this->ProductCategory->Product->find('list');
        $this->set(compact('categories', 'products'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->ProductCategory->exists($id)) {
            throw new NotFoundException(__('Invalid product category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->ProductCategory->save($this->request->data)) {
                $this->Session->setFlash(__('The product category has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The product category could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('ProductCategory.' . $this->ProductCategory->primaryKey => $id));
            $this->request->data = $this->ProductCategory->find('first', $options);
        }
        $categories = $this->ProductCategory->Category->find('list');
        $products = $this->ProductCategory->Product->find('list');
        $this->set(compact('categories', 'products'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->ProductCategory->id = $id;
        if (!$this->ProductCategory->exists()) {
            throw new NotFoundException(__('Invalid product category'));
        }
        $this->request->onlyAllow('post', 'delete');
        $level4 = $this->ProductCategory->find('first', array('conditions' => array('ProductCategory.id' => $id)));
        $this->loadModel('Category');
        if ($this->ProductCategory->delete()) {
            //  $res = $this->ProductCategory->auto_update_product_cat($level4['Category']['id']);
            $child_product_count = $this->ProductCategory->get_child_count($level4['Category']['id']);
            $this->ProductCategory->set_count_product($level4['Category']['id'], $child_product_count);
            $this->Session->setFlash(__('The product category has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The product category could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect($this->referer());
    }

    public function edit_main_cat() {
        if ($this->request->is('post')) {
            $del = $this->ProductCategory->find('first', array('conditions' => array('ProductCategory.id' => $this->request->data['cid']), 'recursive' => -1));
            $this->ProductCategory->updateAll(array('ProductCategory.is_main_cat' => 0), array('ProductCategory.product_id' => $del['ProductCategory']['product_id']));
            $this->ProductCategory->updateAll(array('ProductCategory.is_main_cat' => 1), array('ProductCategory.id' => $this->request->data['cid']));
            $this->ProductCategory->Product->updateAll(array('Product.category_id' => $del['ProductCategory']['category_id']), array('Product.id' => $del['ProductCategory']['product_id']));
        }
    }

    public function auto_count_cat() {
        $this->loadModel('Category');
        $cat_list = $this->ProductCategory->find('all', array('fields' => array('ProductCategory.category_id', 'ProductCategory.product_id')));
        foreach ($cat_list as $key => $cat) {
            $prod_main_cat = $this->ProductCategory->Product->find('first', array('conditions' => array('Product.id' => $cat['ProductCategory']['product_id']), 'fields' => array('Product.category_id'), 'recursive' => -1));
            $level4 = $this->Category->find('first', array('conditions' => array('Category.id' => $cat['ProductCategory']['category_id']), 'recursive' => -1));
            if (!empty($prod_main_cat)) {
                if (!empty($level4)) {

                    if ($level4['Category']['parent_category_id'] != 0) {
                        if ($prod_main_cat['Product']['category_id'] != $level4['Category']['id']) {
                            $cnt = $level4['Category']['no_of_product'] + 1;
                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level4['Category']['id']));
                        }

                        $level3 = $this->Category->find('first', array('conditions' => array('Category.id' => $level4['Category']['parent_category_id']), 'recursive' => -1));
                        if (!empty($level3)) {
                            if ($level3['Category']['parent_category_id'] != 0) {

                                if ($prod_main_cat['Product']['category_id'] != $level3['Category']['id']) {
                                    $cnt = $level3['Category']['no_of_product'] + 1;
                                    $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level3['Category']['id']));
                                }

                                $level2 = $this->Category->find('first', array('conditions' => array('Category.id' => $level3['Category']['parent_category_id']), 'recursive' => -1));
                                if (!empty($level2)) {
                                    if ($level2['Category']['parent_category_id'] != 0) {

                                        if ($prod_main_cat['Product']['category_id'] != $level2['Category']['id']) {
                                            $cnt = $level2['Category']['no_of_product'] + 1;
                                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level2['Category']['id']));
                                        }
                                        $level1 = $this->Category->find('first', array('conditions' => array('Category.id' => $level2['Category']['parent_category_id']), 'recursive' => -1));
                                        if (!empty($level1)) {
                                            if ($level1['Category']['parent_category_id'] != 0) {
                                                if ($prod_main_cat['Product']['category_id'] != $level1['Category']['id']) {
                                                    $cnt = $level1['Category']['no_of_product'] + 1;
                                                    $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level1['Category']['id']));
                                                }
                                                $level0 = $this->Category->find('first', array('conditions' => array('Category.id' => $level1['Category']['parent_category_id']), 'recursive' => -1));
                                                if (!empty($level0)) {
                                                    if ($level0['Category']['parent_category_id'] != 0) {
                                                        if ($prod_main_cat['Product']['category_id'] != $level0['Category']['id']) {
                                                            $cnt = $level0['Category']['no_of_product'] + 1;
                                                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level0['Category']['parent_category_id']));
                                                        }
                                                    } else {
                                                        if ($prod_main_cat['Product']['category_id'] != $level0['Category']['id']) {
                                                            $cnt = $level0['Category']['no_of_product'] + 1;
                                                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level0['Category']['parent_category_id']));
                                                        }
                                                    }
                                                }
                                            } else {
                                                if ($prod_main_cat['Product']['category_id'] != $level1['Category']['id']) {
                                                    $cnt = $level1['Category']['no_of_product'] + 1;
                                                    $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level1['Category']['id']));
                                                }
                                            }
                                        }
                                    } else {
                                        if ($prod_main_cat['Product']['category_id'] != $level2['Category']['id']) {
                                            $cnt = $level2['Category']['no_of_product'] + 1;
                                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level2['Category']['id']));
                                        }
                                    }
                                }
                            } else {

                                if ($prod_main_cat['Product']['category_id'] != $level3['Category']['id']) {
                                    $cnt = $level3['Category']['no_of_product'] + 1;
                                    $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level3['Category']['id']));
                                }
                            }
                        }
                    } else {

                        if ($prod_main_cat['Product']['category_id'] != $level4['Category']['id']) {
                            $cnt = $level4['Category']['no_of_product'] + 1;
                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level4['Category']['id']));
                        }
                    }
                }
            }
        }
        die;
    }

    public function update_category_count() {
        $this->loadModel('Category');
        $final_array = array();
        $level1 = $this->Category->find('all', array('conditions' => array('Category.level' => 1), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id', 'ParentCategory.id', 'ParentCategory.category_name', 'ParentCategory.parent_category_id', 'ParentCategory.level'), 'recursive' => 0));
        if (!empty($level1)) {
            foreach ($level1 as $key => $catlevel1) {
                if (!empty($catlevel1)) {
                    $final_array['level1'][$catlevel1['Category']['id']]['cat_id'] = $catlevel1['Category']['id'];
                    $final_array['level1'][$catlevel1['Category']['id']]['level'] = $catlevel1['Category']['level'];
                    $dist_product1 = array();
                    $dist_product1 = $this->ProductCategory->find('list', array('Conditions' => array('ProductCategory.category_id' => $catlevel1['Category']['id']), 'fields' => array('ProductCategory.product_id', 'ProductCategory.product_id'), 'group' => 'ProductCategory.product_id'));
                    $final_array['level1'][$catlevel1['Category']['id']]['product_id'] = $dist_product1;
                    $level2 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel1['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));
                    if (!empty($level2)) {
                        foreach ($level2 as $catlevel2) {
                            if (!empty($catlevel2)) {

                                $final_array['level1'][$catlevel1['Category']['id']]['level2'][$catlevel2['Category']['id']]['cat_id'] = $catlevel2['Category']['id'];

                                $final_array['level1'][$catlevel1['Category']['id']]['level2'][$catlevel2['Category']['id']]['level'] = $catlevel2['Category']['level'];
                                $dist_product2 = array();
                                $dist_product2 = $this->ProductCategory->find('list', array('Conditions' => array('ProductCategory.category_id' => $catlevel2['Category']['id']), 'fields' => array('ProductCategory.product_id', 'ProductCategory.product_id'), 'group' => 'ProductCategory.product_id'));
                                $final_array['level1'][$catlevel1['Category']['id']]['level2'][$catlevel2['Category']['id']]['product_id'] = $dist_product2;

                                $level3 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel2['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));
                                if (!empty($level3)) {
                                    foreach ($level3 as $catlevel3) {
                                        if (!empty($catlevel3)) {

                                            $final_array['level1'][$catlevel1['Category']['id']]['level2'][$catlevel2['Category']['id']]['level3'][$catlevel3['Category']['id']]['cat_id'] = $catlevel3['Category']['id'];
                                            $final_array['level1'][$catlevel1['Category']['id']]['level2'][$catlevel2['Category']['id']]['level3'][$catlevel3['Category']['id']]['level'] = $catlevel3['Category']['level'];
                                            $dist_product3 = array();
                                            $dist_product3 = $this->ProductCategory->find('list', array('Conditions' => array('ProductCategory.category_id' => $catlevel2['Category']['id']), 'fields' => array('ProductCategory.product_id', 'ProductCategory.product_id'), 'group' => 'ProductCategory.product_id'));
                                            $final_array['level1'][$catlevel1['Category']['id']]['level2'][$catlevel2['Category']['id']]['level3'][$catlevel3['Category']['id']]['product_id'] = $dist_product3;

                                            $level4 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel3['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));
                                            if (!empty($level4)) {
                                                foreach ($level4 as $catlevel4) {
                                                    if (!empty($catlevel4)) {
                                                        $final_array['level1'][$catlevel1['Category']['id']]['level2'][$catlevel2['Category']['id']]['level3'][$catlevel3['Category']['id']]['level4'][$catlevel4['Category']['id']]['cat_id'] = $catlevel4['Category']['id'];
                                                        $final_array['level1'][$catlevel1['Category']['id']]['level2'][$catlevel2['Category']['id']]['level3'][$catlevel3['Category']['id']]['level4'][$catlevel4['Category']['id']]['level'] = $catlevel4['Category']['level'];
                                                        $dist_product4 = array();
                                                        $dist_product4 = $this->ProductCategory->find('list', array('Conditions' => array('ProductCategory.category_id' => $catlevel3['Category']['id']), 'fields' => array('ProductCategory.product_id', 'ProductCategory.product_id'), 'group' => 'ProductCategory.product_id'));
                                                        $final_array['level1'][$catlevel1['Category']['id']]['level2'][$catlevel2['Category']['id']]['level3'][$catlevel3['Category']['id']]['level4'][$catlevel4['Category']['id']]['product_id'] = $dist_product4;

                                                        //  $level5 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel4['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        debug($final_array);
        die;
    }

    public function update_all_level_category_count() {
        $this->loadModel('Category');
        $this->ProductCategory->clean_extra_relation();
        $level1 = $this->Category->find('list', array('conditions' => array('Category.level' => 1), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level1)) {
            foreach ($level1 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }
        $level2 = $this->Category->find('list', array('conditions' => array('Category.level' => 2), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level2)) {
            foreach ($level2 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }
        $level3 = $this->Category->find('list', array('conditions' => array('Category.level' => 3), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level3)) {
            foreach ($level3 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }
        $level4 = $this->Category->find('list', array('conditions' => array('Category.level' => 4), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level4)) {
            foreach ($level4 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }
        debug('updated');
        die;
    }

    public function test_count($cat_id=null){
         $child_product_count = $this->ProductCategory->get_child_count($cat_id);
         debug($child_product_count);
         die;
    }
    public function update_prod_count_admin() {
        $this->loadModel('Category');
        $this->ProductCategory->clean_extra_relation();
        $level1 = $this->Category->find('list', array('conditions' => array('Category.level' => 1), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level1)) {
            foreach ($level1 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }
        $level2 = $this->Category->find('list', array('conditions' => array('Category.level' => 2), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level2)) {
            foreach ($level2 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }
        $level3 = $this->Category->find('list', array('conditions' => array('Category.level' => 3), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level3)) {
            foreach ($level3 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }
        $level4 = $this->Category->find('list', array('conditions' => array('Category.level' => 4), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level4)) {
            foreach ($level4 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }
        $level5 = $this->Category->find('list', array('conditions' => array('Category.level' => 5), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level5)) {
            foreach ($level5 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }
        $level6 = $this->Category->find('list', array('conditions' => array('Category.level' => 6), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level6)) {
            foreach ($level6 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }
        $this->Session->setFlash(__('The product count has been updated.'), 'success');
        return $this->redirect($this->referer());
    }

    public function update_level_one_count() {
        $this->loadModel('Category');
        $level1 = $this->Category->find('list', array('conditions' => array('Category.level' => 1), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level1)) {
            foreach ($level1 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                if ($this->ProductCategory->set_count_product($cat_id, $child_product_count)) {
                    debug('success');
                } else {
                    debug('error');
                }
            }
        }
        die;
    }

    public function update_level_two_count() {
        $this->loadModel('Category');
        $level1 = $this->Category->find('list', array('conditions' => array('Category.level' => 2), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level1)) {
            foreach ($level1 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                if ($this->ProductCategory->set_count_product($cat_id, $child_product_count)) {
                    debug('success');
                } else {
                    debug('error');
                }
            }
        }
        die;
    }

    public function update_level_three_count() {
        $this->loadModel('Category');
        $level1 = $this->Category->find('list', array('conditions' => array('Category.level' => 3), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level1)) {
            foreach ($level1 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);

                if ($this->ProductCategory->set_count_product($cat_id, $child_product_count)) {
                    debug('success');
                } else {
                    debug('error');
                }
            }
        }
        die;
    }

    public function update_level_four_count() {
        $this->loadModel('Category');
        $level1 = $this->Category->find('list', array('conditions' => array('Category.level' => 4), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level1)) {
            foreach ($level1 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                if ($this->ProductCategory->set_count_product($cat_id, $child_product_count)) {
                    debug('success');
                } else {
                    debug('error');
                }
            }
        }
        die;
    }

    public function update_product_cat_count($cat_id) {
        $child_product_count = $this->ProductCategory->get_child_count($cat_id);
        $this->ProductCategory->set_count_product($cat_id, $child_product_count);
        return 1;
    }

    public function clean_product_category() {
        $this->ProductCategory->clean_extra_relation();
    }

    public function beforeFilter() {
        $this->Auth->allow(array('edit_main_cat','test_count', 'update_all_level_category_count', 'clean_product_category', 'auto_count_cat', 'update_category_count', 'update_level_one_count', 'update_level_two_count', 'update_level_three_count', 'update_level_four_count'));
    }

}
