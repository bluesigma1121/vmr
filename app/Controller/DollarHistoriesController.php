<?php
App::uses('AppController', 'Controller');
/**
 * DollarHistories Controller
 *
 * @property DollarHistory $DollarHistory
 * @property PaginatorComponent $Paginator
 */
class DollarHistoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->DollarHistory->recursive = 0;
		$this->set('dollarHistories', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->DollarHistory->exists($id)) {
			throw new NotFoundException(__('Invalid dollar history'));
		}
		$options = array('conditions' => array('DollarHistory.' . $this->DollarHistory->primaryKey => $id));
		$this->set('dollarHistory', $this->DollarHistory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->DollarHistory->create();
			if ($this->DollarHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The dollar history has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The dollar history could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->DollarHistory->exists($id)) {
			throw new NotFoundException(__('Invalid dollar history'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->DollarHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The dollar history has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The dollar history could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('DollarHistory.' . $this->DollarHistory->primaryKey => $id));
			$this->request->data = $this->DollarHistory->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->DollarHistory->id = $id;
		if (!$this->DollarHistory->exists()) {
			throw new NotFoundException(__('Invalid dollar history'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->DollarHistory->delete()) {
			$this->Session->setFlash(__('The dollar history has been deleted.'));
		} else {
			$this->Session->setFlash(__('The dollar history could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
