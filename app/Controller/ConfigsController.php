<?php

App::uses('AppController', 'Controller');

/**
 * Configs Controller
 *
 * @property Config $Config
 */
class ConfigsController extends AppController
{

    /**
     * index method
     *
     * @return void
     */
    public function index()
    {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $configs = array();
            foreach ($this->request->data['Config'] as $key => $value) {
                $config = array();
                $config_data = $this->Config->findByConfigKey($key);
                if (!empty($config_data)) {
                    $config['Config']['id'] = $config_data['Config']['id'];
                }
                $config['Config']['config_key'] = $key;
                $config['Config']['config_value'] = $value;
                $configs[] = $config;
            }
            if ($this->Config->saveAll($configs)) {
                $this->Session->setFlash(__('The configuration has been saved.'), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The configuration could not be saved. Please, try again.'), 'error');
            }
        }

    }

    /**
     * get_config_settings method
     *
     * @return void
     */
    public function get_config_settings($key)
    {
        $config = $this->Config->findByConfigKey($key);            
        if ($key == "smtp_password") {            
            return Security::decrypt($config['Config']['config_value'],$this->Config->salt_key);
        }else{            
            return $config['Config']['config_value'];
        }        
    }

}
