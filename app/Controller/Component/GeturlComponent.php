<?php

App::uses('Component', 'Controller');

class GeturlComponent extends Component {

    function newurl($location = null, $action = null, $id = null, $slug = null) {
        
        $slug = str_replace(' ', '-', $slug);
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $slug);
        $slug = preg_replace('/-+/', '-', $slug);
        
        if ($location == 'products' && $action == 'product_details') {
            $url = '/product-description-' . $id . '-' . strtolower($slug);
            return $url;
        }
        if ($location == 'requirements' && $action == 'requirement_details') {
            $url = '/buy-Lead-' . $id . '-' . strtolower($slug);
            return $url;
        }
        if ($location == 'requirements' && $action == 'seller_offer_details') {
            $url = '/sell-offer-' . $id . '-' . strtolower($slug);
            return $url;
        }
        if ($location == 'companies' && $action == 'company_details') {
            $url = '/company-description-' . $id . '-' . strtolower($slug);
            return $url;
        }
        if($location=='company_reviews' && $action=='company_ratings'){
            $url = '/reviews-&-ratings-'. $id . '-' .strtolower($slug);
            return $url;
        }
    }

}
