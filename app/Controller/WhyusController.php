<?php
App::uses('AppController', 'Controller');
/**
 * Whyus Controller
 *
 * @property Whyus $Whyus
 * @property PaginatorComponent $Paginator
 */
class WhyusController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout= "admin_layout";
		$this->Whyus->recursive = 0;
		$this->set('whyuses', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Whyus->exists($id)) {
			throw new NotFoundException(__('Invalid whyus'));
		}
		$options = array('conditions' => array('Whyus.' . $this->Whyus->primaryKey => $id));
		$this->set('whyus', $this->Whyus->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout= "admin_layout";
		if ($this->request->is('post')) {
			$this->Whyus->create();
			if ($this->Whyus->save($this->request->data)) {
				$this->Session->setFlash(__('The whyus has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The whyus could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout= "admin_layout";
		if (!$this->Whyus->exists($id)) {
			throw new NotFoundException(__('Invalid whyus'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Whyus->save($this->request->data)) {
				$this->Session->setFlash(__('The whyus has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The whyus could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Whyus.' . $this->Whyus->primaryKey => $id));
			$this->request->data = $this->Whyus->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Whyus->id = $id;
		if (!$this->Whyus->exists()) {
			throw new NotFoundException(__('Invalid whyus'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Whyus->delete()) {
			$this->Session->setFlash(__('The whyus has been deleted.'));
		} else {
			$this->Session->setFlash(__('The whyus could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
