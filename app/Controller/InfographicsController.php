<?php

App::uses('AppController', 'Controller');

class InfographicsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->layout = 'admin_layout';
        //$this->Infographics->recursive = 0;
        //$this->set('infographics', $this->Paginator->paginate());

        $this->loadModel('Infographic'); 
       
            //if (empty($_POST['searchrdimage'])) {
             //
               $this->paginate = array(
               // 'conditions' => array('Rdupdateimagedata.id >=' => $ssid['id']),
                'fields' => array('id','info_title','info_desc','meta_title','meta_keyword','meta_desc','slug','image_path','created'),
                'limit' => 11,
                'order'=>'id ASC'
            );
            $tarcounselect = $this->Paginator->paginate();
            $this->set('infographics', $tarcounselect);
           
        
    }
    
    public function add($endproductid) {
        $this->layout = 'admin_layout';
        $this->loadModel('Infographic');
        if ($this->request->is('post')) {
            $this->Infographic->create();
            $res = $this->Infographic->save_client_fun($this->request->data);
            if ($res == 1) {
                $this->Session->setFlash(__('The Infographics has been saved.'), 'success');
                
                //return Router::url('/', true);
                $this->redirect(array('action' => 'index',$endproductid));
                
            } else {
                $this->Session->setFlash(__('The Infographics could not be saved. Please, try again.'), 'error');
            }
           
        }
    }
    /**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null,$endproductid) {
		$this->layout = 'admin_layout';
        $this->loadModel('Infographic');
		if (!$this->Infographic->exists($id)) {
			throw new NotFoundException(__('Invalid Infographic'));
		}
		if ($this->request->is(array('post', 'put'))) {
            if ($this->Infographic->edit_client_info($this->request->data)) {
                $this->Session->setFlash(__('The our client has been saved.'), 'success');
                 return $this->redirect(array('action' => 'index',$endproductid));
                //return Router::url('/', true);
            } else {
                $this->Session->setFlash(__('The Infographic could not be saved. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('Infographic.' . $this->Infographic->primaryKey => $id));
            $this->request->data = $this->Infographic->find('first', $options);
        }
	}
/**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null,$endproductid) {
        $this->loadModel('Infographic'); 
        $this->Infographic->id = $id;
        if (!$this->Infographic->exists()) {
            throw new NotFoundException(__('Invalid infographic'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Infographic->delete()) {
            $this->Session->setFlash(__('The infographic has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The infographic could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'index',$endproductid));
    }

    public function infographicsview() {
        $this->loadModel('Infographic');
        $ourinfographics = $this->Infographic->our_infographics_fun();
        // $ourinfographicstrending = $this->Infographic->our_infographics_trending();
        // echo "<pre>";
		// print_r($ourinfographics);
		// exit;
        // echo "<pre>";
		// print_r($ourinfographicstrending);
		// exit;
        $this->set(compact('ourinfographics','ourinfographicstrending'));
    }
    
    public function infographic_enquiry() {
        $this->layout = 'admin_layout';
        $this->loadModel('Infographic_Download'); 
        $Infographic_Downloads=$this->Infographic_Download->find('all',array(
            'fields'=>array('id','firstname','last_name','organization','designation','mobile','corporate_emailid','country','created_on'),
            // 'limit' => 4,
            ));

        $this->set(compact('Infographic_Downloads'));
            //    $this->paginate = array(
            //    // 'conditions' => array('Rdupdateimagedata.id >=' => $ssid['id']),
            //     'fields' => array('id','firstname','last_name','organization','designation','mobile','corporate_emailid','country','created_on'),
            //     'limit' => 11,
            //     'order'=>'id ASC'
            // );
            // $tarcounselect = $this->Paginator->paginate();
            // $this->set('infographics_enquiry', $tarcounselect);
           
        
    }
    public function infographics_details($slug) {
        $this->loadModel('Infographic');
        $ourinfographics = $this->Infographic->our_infographics_funslug($slug);
        //$ourinfographicsmetatitle = $this->Infographic->our_infographics_funmetatitle($slug);
        // $ourinfographicstrending = $this->Infographic->our_infographics_trending();
        //echo "<pre>";
		// print_r($ourinfographicsmetatitle);
		// exit;
        // echo "<pre>";
		// print_r($ourinfographicstrending);
		// exit;
        $this->set(compact('ourinfographics','ourinfographicstrending'));
    }
   

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(array('infographicsview','infographics_details'));
    }



}


