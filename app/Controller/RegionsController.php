<?php

App::uses('AppController', 'Controller');

/**
 * Regions Controller
 *
 * @property Region $Region
 * @property PaginatorComponent $Paginator
 */
class RegionsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->layout = 'admin_layout';
        $this->Region->recursive = 0;
        $this->set('regions', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->Region->exists($id)) {
            throw new NotFoundException(__('Invalid region'));
        }
        $options = array('conditions' => array('Region.' . $this->Region->primaryKey => $id));
        $this->set('region', $this->Region->find('first', $options));
        $this->loadModel('CountryRegion');
        $country_list = $this->CountryRegion->find('all', array('conditions' => array('CountryRegion.region_id' => $id)));
        $this->set(compact('country_list'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $this->Region->create();
            $res = $this->Region->fun_add_countries($this->request->data);
            if ($res == 1) {
                $this->Session->setFlash(__('The region has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The region could not be saved. Please, try again.'));
            }
        }
        $this->loadModel('Country');
        $country = $this->Country->find('list', array('fields' => array('Country.id', 'Country.country_name')));
        $this->set(compact('country'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->Region->exists($id)) {
            throw new NotFoundException(__('Invalid region'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $res = $this->Region->edit_region_country($this->request->data);
            if ($res == 1) {
                $this->Session->setFlash(__('The region has been saved.'), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The region could not be saved. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('Region.' . $this->Region->primaryKey => $id));
            $this->request->data = $this->Region->find('first', $options);
        }
        $this->loadModel('Country');
        $this->loadModel('CountryRegion');
        $select_country = $this->CountryRegion->find('list', array('conditions' => array('CountryRegion.region_id' => $id), 'fields' => array('CountryRegion.country_id', 'CountryRegion.country_id')));
        $country = $this->Country->find('list', array('fields' => array('Country.id', 'Country.country_name')));
        $this->set(compact('country', 'select_country'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Region->id = $id;
        if (!$this->Region->exists()) {
            throw new NotFoundException(__('Invalid region'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Region->delete()) {
            $this->Session->setFlash(__('The region has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The region could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'index'));
    }

}
