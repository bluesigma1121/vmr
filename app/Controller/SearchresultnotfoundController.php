<?php
App::uses('AppController', 'Controller');
/**
 * Searchresultnotfound Controller
 *
 * @property Searchresultnotfound $Searchresultnotfound
 * @property PaginatorComponent $Paginator
 */
class SearchresultnotfoundController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout= "admin_layout";
		$this->Searchresultnotfound->recursive = 0;
		$this->set('searchresultnotfounds', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout= "admin_layout";
		if (!$this->Searchresultnotfound->exists($id)) {
			throw new NotFoundException(__('Invalid searchresultnotfound'));
		}
		$options = array('conditions' => array('Searchresultnotfound.' . $this->Searchresultnotfound->primaryKey => $id));
		$this->set('searchresultnotfound', $this->Searchresultnotfound->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Searchresultnotfound->create();
			if ($this->Searchresultnotfound->save($this->request->data)) {
				$this->Session->setFlash(__('The searchresultnotfound has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The searchresultnotfound could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Searchresultnotfound->exists($id)) {
			throw new NotFoundException(__('Invalid searchresultnotfound'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Searchresultnotfound->save($this->request->data)) {
				$this->Session->setFlash(__('The searchresultnotfound has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The searchresultnotfound could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Searchresultnotfound.' . $this->Searchresultnotfound->primaryKey => $id));
			$this->request->data = $this->Searchresultnotfound->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Searchresultnotfound->id = $id;
		if (!$this->Searchresultnotfound->exists()) {
			throw new NotFoundException(__('Invalid searchresultnotfound'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Searchresultnotfound->delete()) {
			$this->Session->setFlash(__('The searchresultnotfound has been deleted.'));
		} else {
			$this->Session->setFlash(__('The searchresultnotfound could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
