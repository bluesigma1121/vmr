<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Captcha');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($data['User']['search_text'])) {
                $this->Session->setFlash(__('Please Fill Search Box.'), 'error');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->write('user_filter', $data['User']);
        }
        if ($this->Session->check('user_filter')) {
            $user_filt = $this->Session->read('user_filter');
        }
        $conditions = array();
        if (isset($user_filt['search_by'])) {
            if ($user_filt['search_by'] == 'name') {
                $conditions["User." . $user_filt['search_by'] . " LIKE"] = '%' . $user_filt['search_text'] . '%';
            }
            if ($user_filt['search_by'] == 'organisation') {
                $conditions["User." . $user_filt['search_by'] . " LIKE"] = '%' . $user_filt['search_text'] . '%';
            }
        }
        $this->Session->delete('user_filter');
        $this->paginate = array(
            // Added Sals Team Role Users VG starts here 2/11/2017
           // 'conditions' => array($conditions, 'User.role' => array(1, 2, 3, 4, 5,12)),
            'conditions' => array($conditions, 'User.role' => array(1, 2, 3, 4, 5,12)),
             // Added Sals Team Role Users VG ends here 2/11/2017
            'order' => 'User.id DESC',
            'limit' => 50
        );
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
        Configure::load('idata');
        $roles = Configure::read('idata.roles');
        $user_search = Configure::read('idata.user_search');
        $this->set(compact('roles', 'user_search'));
    }

    public function lead_user_index() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($data['User']['search_text'])) {
                $this->Session->setFlash(__('Please Fill Search Box.'), 'error');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->write('user_filter', $data['User']);
        }
        if ($this->Session->check('user_filter')) {
            $user_filt = $this->Session->read('user_filter');
        }
        $conditions = array();
        if (isset($user_filt['search_by'])) {
            if ($user_filt['search_by'] == 'name') {
                $conditions["User." . $user_filt['search_by'] . " LIKE"] = '%' . $user_filt['search_text'] . '%';
            }
            if ($user_filt['search_by'] == 'organisation') {
                $conditions["User." . $user_filt['search_by'] . " LIKE"] = '%' . $user_filt['search_text'] . '%';
            }
        }
        $this->Session->delete('user_filter');
        $this->paginate = array(
            'conditions' => array($conditions, 'User.role' => 11),
            'order' => 'User.id DESC',
            'limit' => 50
        );
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
        Configure::load('idata');
        $roles = Configure::read('idata.roles');
        $user_search = Configure::read('idata.user_search');
        $this->set(compact('roles', 'user_search'));
    }

    public function dashbord() {
        $this->layout = "admin_layout";
    }

    public function test() {
        $this->layout = null;
        $this->loadModel('Category');
        $category = $this->Category->find('all', array(
            'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
        ));
        $this->set(compact('category'));
    }

    public function my_account($id) {
        if (AuthComponent::user('id') != $id) {
            $this->Session->setFlash(__('The user has Not Found .'), 'error');
            return $this->redirect(array('action' => 'registration'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->request->data = $this->User->find('first', $options);
        Configure::load('idata');
        $title = Configure::read('idata.title');
        $this->set(compact('title'));
    }

    public function details($id = null) {
        if (AuthComponent::user('id') != $id) {
            $this->Session->setFlash(__('The user has Not Found .'), 'error');
            return $this->redirect(array('action' => 'registration'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->request->data = $this->User->find('first', $options);
        Configure::load('idata');
        $title = Configure::read('idata.title');
        $this->set(compact('title'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
        Configure::load('idata');
        $roles = Configure::read('idata.roles');
        $this->loadModel('Enquiry');
        $enqueries = $this->Enquiry->find('all', array('conditions' => array('Enquiry.user_id' => $id), 'fields' => array('Enquiry.*', 'Product.product_name', 'Category.category_name')));
        $this->loadModel('CartItem');
        $this->loadModel('User');
        $cartItems = $this->CartItem->find('all', array('conditions' => array('CartItem.user_id' => $id), 'fields' => array('CartItem.*', 'Product.product_name')));

        $this->set(compact('roles', 'enqueries', 'cartItems'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->layout = "admin_layout";
        Configure::load('idata');
        $roles = Configure::read('idata.roles');
        $title = Configure::read('idata.title');
        $this->set(compact('roles', 'title'));
        if ($this->request->is('post')) {
            $this->set(compact('roles', 'title'));
            if ($this->User->find('first', array('conditions' => array('User.email' => $this->request->data['User']['email'])))) {
                $this->Session->setFlash(__('This Email Already Registered .'), 'error');
                return false;
            }
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved.'), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'error');
            }
        }
    }
    public function resetpassword() {
       }
       public function resetadd() {
      //$this->loadModel('User');
      $password=$_POST['password'];
      $confirm_password=$_POST['confirm_password'];
      $userid=$_POST['userid'];
      if($password == $confirm_password)
      {
    //     $sqlurl1 = "UPDATE users 
    //     SET password='".$password."' 
    //     WHERE id ='".$userid."'";
    //    $queryurl1 = $this->User->query($sqlurl1);
    //    $this->Session->setFlash('Password has been reseted', 'success');
  
    if ($this->request->is('post')) {
        $data = $this->request->data;
       
        $user = $this->User->find('first', array('conditions' => array('User.id' => $userid), 'recursive' => -1));
        $user['User']['password'] = $password;
       
        if ($this->User->save($user)) {
            $this->Session->setFlash('Password has been reseted', 'success');
        } else {
            $this->Session->setFlash('Oops something went wrong. Please try again.', 'error');
        }
    }   
    
      }
      else
      {
        $this->Session->setFlash('Fail to Reset', 'error');
        return $this->redirect(array('action' => 'resetpassword'));
      }
     
    }

    public function user_reset_password($id = null) {
        $this->layout = 'admin_layout';
        //----- 
                $uniqidStr = md5(uniqid(mt_rand()));
       $resetPassLink = Router::url('/resetpassword',true)."?fp_code=".$uniqidStr."&userid=".$id; 
      $resetlink = '(You can reset your password -'.'<br>'.'<a href="'.$resetPassLink.'">'.$resetPassLink.'</a>)';
        //-----

        $user = $this->User->find('first', array('conditions' => array('User.id' => $id), 'recursive' => -1));
        $new_pwd = substr(str_shuffle(str_repeat('ABCDEFGHJKLMNPQRSTUVWXYZ23456789', 5)), 0, 5);
        $user['User']['password'] = $new_pwd;
        $resetlinkuser['User']['resetlink'] = $resetlink;
        $this->loadModel('Category');
        $category = $this->Category->find('all', array(
            'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
        ));
        
        $this->set(compact('category'));
        if ($this->User->save($user)) {
            $email = new CakeEmail();
            $email->config('reset_password');
            $email->viewVars(compact('user', 'new_pwd', 'category','resetlinkuser'));
            $email->subject('Password Reset By Admin On Value Market Research');
            $email->to($user['User']['email']);
            $email->send();
            $this->Session->setFlash('Password has been reseted', 'success');
            $this->redirect($this->referer());
        } else {
            $this->Session->setFlash('Oops something went wrong. Please try again.', 'error');
        }
    }

    public function active($id = NULL) {
        if ($this->User->updateAll(array('User.is_active' => 1, 'User.is_verified' => 1), array('User.id' => $id))) {
            $this->Session->setFlash(__('The User has been Activated.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The User could not be Activated. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function deactive($id = NULL) {
        if ($this->User->updateAll(array('User.is_active' => 0), array('User.id' => $id))) {
            $this->Session->setFlash(__('The User has been DeActivated.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The User could not be DeActivated. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function verified($id = NULL) {
        if ($this->User->updateAll(array('User.is_verified' => 1), array('User.id' => $id))) {
            $this->Session->setFlash(__('The User has been verified.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The User could not be verified. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function deverified($id = NULL) {
        if ($this->User->updateAll(array('User.is_verified' => 0), array('User.id' => $id))) {
            $this->Session->setFlash(__('The User has been Deverified.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The User could not be Deverified. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->layout = "admin_layout";
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved.'), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
        Configure::load('idata');
        $roles = Configure::read('idata.roles');
        $title = Configure::read('idata.title');
        $this->set(compact('roles', 'title'));
    }

    public function edit_details($id) {
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved.'), 'success');
                return $this->redirect($this->referer());
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'error');
            }
        }
    }

    public function change_password($id = null) {
        $this->layout = 'admin_layout';
        if ($id != AuthComponent::user('id')) {
            $this->redirect($this->referer());
        }
        $this->User->id = $id;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($data['User']['cuurent_pwd']) || empty($data['User']['new_password']) || empty($data['User']['Confirm_password'])) {
                $this->Session->setFlash('Operation failed. All fields mandatory. Please try again.', 'error');
                $this->redirect($this->referer());
            }
            $res = $this->User->change_user_password($data);

//  return 5 current password mitchmatch
//  return 4 newpassword and  Confirm_password mitchmatch
//  return 1 Success
//  return 0 Fail
            if ($res == 1) {
                $this->Session->setFlash('Password has been changed successfully.', 'success');
                $this->redirect($this->referer());
            } elseif ($res == 4) {
                $this->Session->setFlash('New Password And Confirm Password Mis-matched.', 'error');
                $this->redirect($this->referer());
            } elseif ($res == 5) {
                $this->Session->setFlash('Please Enter Currunt Password Correctly.', 'error');
                $this->redirect($this->referer());
            } else {
                $this->Session->setFlash('Operation failed. Please try again.', 'error');
                $this->redirect($this->referer());
            }
        }
    }

    public function set_user_password($id = null) {
        $this->layout = 'login_layout';
        $this->User->id = $id;

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $data['User']['id'] = $id;
            if (empty($data['User']['password']) || empty($data['User']['Confirm_password'])) {
                $this->Session->setFlash('Operation failed. All fields mandatory. Please try again.', 'error');
                $this->redirect($this->referer());
            }

//  return 4 newpassword and  Confirm_password mitchmatch
//  return 1 Success
//  return 0 Fail
            $res = $this->User->set_user_password($data);
            if ($res == 1) {
                $this->Session->setFlash('Password has been set successfully.', 'success');
                $this->redirect(array('action' => 'my_account', AuthComponent::user('id')));
            } elseif ($res == 4) {
                $this->Session->setFlash('New Password And Confirm Password Mis-matched.', 'error');
                $this->redirect($this->referer());
            } else {
                $this->Session->setFlash('Operation failed. Please try again.', 'error');
                $this->redirect($this->referer());
            }
        }
    }

    public function change_user_passwaord($id = null) {

        if ($this->request->is(array('post', 'put'))) {
            $data = $this->request->data;
            $data['User']['id'] = AuthComponent::user('id');
            if (empty($data['User']['cuurent_pwd']) || empty($data['User']['password']) || empty($data['User']['Confirm_password'])) {
                $this->Session->setFlash('Operation failed. All fields mandatory. Please try again.', 'error');
                $this->redirect($this->referer());
            }
            $res = $this->User->fun_change_user_password($data);

//  return 5 current password mitchmatch
//  return 4 newpassword and  Confirm_password mitchmatch
//  return 1 Success
//  return 0 Fail

            if ($res == 1) {
                $this->Session->setFlash('Password has been changed successfully.', 'success');
                $this->redirect(array('action' => 'my_account', AuthComponent::user('id')));
            } elseif ($res == 4) {
                $this->Session->setFlash('New Password And Confirm Password Mis-matched.', 'error');
                $this->redirect(array('action' => 'my_account', AuthComponent::user('id')));
            } elseif ($res == 5) {
                $this->Session->setFlash('Please Enter Currunt Password Correctly.', 'error');
                $this->redirect(array('action' => 'my_account', AuthComponent::user('id')));
            } else {
                $this->Session->setFlash('Operation failed. Please try again.', 'error');
                $this->redirect(array('action' => 'my_account', AuthComponent::user('id')));
            }
        }
    }

    public function login_front_forgot_password() {
        $this->layout = 'login_layout';
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($data['User']['email'])) {
                $this->Session->setFlash('Please enter correct email id.', 'error');
                $this->redirect($this->referer());
            }
            $user = $this->User->find('first', array('conditions' => array('User.email' => $data['User']['email']), 'recursive' => -1));
            if (empty($user)) {
                $this->Session->setFlash('This Email  is not associated with our site.', 'error');
                $this->redirect($this->referer());
            }
            
            $new_pwd = substr(str_shuffle(str_repeat('ABCDEFGHJKLMNPQRSTUVWXYZ23456789', 5)), 0, 5);
            $user['User']['password'] = $new_pwd;
            $this->loadModel('Category');
            $category = $this->Category->find('all', array(
                'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
            ));
            $this->set(compact('category'));
            if ($this->User->save($user)) {
                $email = new CakeEmail();
                $email->config('forgot_password');
                $email->viewVars(compact('user', 'category'));
                $email->subject('Password Recovery On Value Market Research');
                $email->to($user['User']['email']);
                $email->send();
                $this->Session->setFlash('Your password has been reseted. Please check your mail.', 'success');
                $this->redirect(array('action' => 'login'));
            } else {
                $this->Session->setFlash('Oops something went wrong. Please try again.', 'error');
            }
        }
    }
    public function forgotresetpassword() {
    }
    public function forgotresetadd() {
        $this->loadModel('User');
        // $password=sha1($_POST['password']);
        // $confirm_password=sha1($_POST['confirm_password']);
        $password=$_POST['password'];
        $confirm_password=$_POST['confirm_password'];
        $userid=$_POST['userid'];
        if($password == $confirm_password)
        {
        if ($this->request->is('post')) {
            $data = $this->request->data;
           
            $user = $this->User->find('first', array('conditions' => array('User.email' => $userid), 'recursive' => -1));
            $user['User']['password'] = $password;
           
            if ($this->User->save($user)) {
                $this->Session->setFlash('Password has been reseted', 'success');
            } else {
                $this->Session->setFlash('Oops something went wrong. Please try again.', 'error');
            }
        }   
    }  
    else
      {
        $this->Session->setFlash('Fail to Reset', 'error');
        return $this->redirect(array('action' => 'forgotresetpassword'));
      }
      }
    public function forgot_password() {

        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($data['User']['email'])) {
                $this->Session->setFlash('Please enter correct email id.', 'error');
                $this->redirect($this->referer());
            }
            $user = $this->User->find('first', array('conditions' => array('User.email' => $data['User']['email']), 'recursive' => -1));
            if (empty($user)) {
                $this->Session->setFlash('This Email  is not associated with our site.', 'error');
                $this->redirect($this->referer());
            }
             //----- 
           
             $uniqidStr = md5(uniqid(mt_rand()));
             $resetPassLink = Router::url('/forgotresetpassword',true)."?fp_code=".$uniqidStr."&userid=".$data['User']['email']; 
            $resetlink = '(You can reset your password -'.'<br>'.'<a href="'.$resetPassLink.'">'.$resetPassLink.'</a>)';
              //-----
            $new_pwd = substr(str_shuffle(str_repeat('ABCDEFGHJKLMNPQRSTUVWXYZ23456789', 5)), 0, 5);
             $user['User']['password'] = $new_pwd;
            $resetlinkuser['User']['resetlink'] = $resetlink;
            $this->loadModel('Category');
            $category = $this->Category->find('all', array(
                'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
            ));
            
            $this->set(compact('category'));
            if ($this->User->save($user)) {
                $email = new CakeEmail();
                $email->config('forgot_password');
                $email->viewVars(compact('user','category', 'resetlinkuser'));
                $email->subject('Password Recovery On Value Market Research');
                $email->to($user['User']['email']);
                $email->send();
                $this->Session->setFlash('Your password has been reseted. Please check your mail.', 'success');
                $this->redirect(array('action' => 'login'));
            } else {
                $this->Session->setFlash('Oops something went wrong. Please try again.', 'error');
            }
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function login() {
        if (AuthComponent::user('id')) {
            
            if (AuthComponent::user('role') == 1) {
                return $this->redirect(array('controller' => 'products', 'action' => 'products_ready_active'));
            } if (AuthComponent::user('role') == 2) {
                return $this->redirect(array('controller' => 'products', 'action' => 'index'));
            }if (AuthComponent::user('role') == 5) {
                return $this->redirect(array('controller' => 'products', 'action' => 'jr_analyst_index'));
            } if (AuthComponent::user('role') == 11) {
                return $this->redirect(array('controller' => 'users', 'action' => 'my_account', AuthComponent::user('id')));
            }if (AuthComponent::user('role') == 3) {
                return $this->redirect(array('controller' => 'products', 'action' => 'seo_index'));
            }if (AuthComponent::user('role') == 4) {
                return $this->redirect(array('controller' => 'articles', 'action' => 'index'));
            }if (AuthComponent::user('role') == 12) { //code added VG-13/03/2017 Start
                return $this->redirect(array('controller' => 'enquiries'));
            }//code added VG-13/03/2017 End
            else {
                return $this->redirect(array('controller' => 'products', 'action' => 'index'));
            }
        }
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $last_login = Date('Y-m-d H:i:s');
                $login_ip = $this->request->clientIp();
                $this->User->updateAll(array('User.last_login' => "'$last_login'", 'User.visited_ip' => "'$login_ip'",), array('User.id' => AuthComponent::user('id')));
                if ($this->Session->check('bill_log')) {
                    $cart_data = $this->Session->read('bill_log');
                    if ($cart_data == 'bill_info') {
                        if (AuthComponent::user('role') == 11) {
                            return $this->redirect(array('controller' => 'cart_items', 'action' => 'billing_info'));
                        } else {
                            $this->Session->setFlash(__('Please Login as User'), 'success');
                            return $this->redirect('/');
                        }
                    }
                }

                if ($this->Session->check('user_order')) {
                    $page_data = $this->Session->read('user_order');

                    if ($page_data == 'user_order_index') {
                        if (AuthComponent::user('role') == 11) {
                            return $this->redirect(array('controller' => 'orders', 'action' => 'user_order_index'));
                        }
                    }
                }
                // print_r(AuthComponent::user('role'));exit;
                if (AuthComponent::user('role') == 1) {
                    return $this->redirect(array('controller' => 'products', 'action' => 'products_ready_active'));
                } if (AuthComponent::user('role') == 2) {
                    return $this->redirect(array('controller' => 'products', 'action' => 'index'));
                } if (AuthComponent::user('role') == 4) {
                    return $this->redirect(array('controller' => 'articles', 'action' => 'index'));
                }if (AuthComponent::user('role') == 5) {
                    return $this->redirect(array('controller' => 'products', 'action' => 'jr_analyst_index'));
                }if (AuthComponent::user('role') == 11) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'my_account', AuthComponent::user('id')));
                } if (AuthComponent::user('role') == 3) {
                    return $this->redirect(array('controller' => 'products', 'action' => 'seo_index'));
                }if (AuthComponent::user('role') == 12) { //code added VG-13/03/2017 Start
                    return $this->redirect(array('controller' => 'enquiries', 'action' => 'index'));
                }//code added VG-13/03/2017 End
                else {
                    return $this->redirect(array('controller' => 'products', 'action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('Invalid username or password, try again'), 'error');
            }
        }

// Menu-Research Industries (Category and subCategory) code start here
      $this->loadModel('Category');
    //   $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
    //   foreach ($research_ind_cat as $key1 => $cat) {
    //       $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
    //       $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
    //       $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
    //       $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
          $level_two = $this->Category->find('all', array('conditions' =>
              array(
                  'Category.parent_category_id' => 2,
                  'Category.is_active' => 1), 'recursive' => -1));
          foreach ($level_two as $key2 => $level2) {
                $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
    //   }

// Menu-Research Industries (Category and subCategory) code end here
  $this->set(compact('research_ind'));

    }

    public function login_front_user() {
        $this->layout = 'login_layout';
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {

                if ($this->Session->check('bill_log')) {
                    $cart_data = $this->Session->read('bill_log');
                    if ($cart_data == 'bill_info') {
                        if (AuthComponent::user('role') == 11) {
                            return $this->redirect(array('controller' => 'cart_items', 'action' => 'billing_info'));
                        }
                    }
                }

                if (AuthComponent::user('role') == 1) {
                    return $this->redirect(array('controller' => 'products', 'action' => 'index'));
                } if (AuthComponent::user('role') == 11) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'my_account', AuthComponent::user('id')));
                } else {
                    return $this->redirect(array('controller' => 'users', 'action' => 'dashbord'));
                }
            } else {
                $this->Session->setFlash(__('Invalid username or password, try again'), 'error');
            }
        }
    }

    public function user_verification($url_id = null, $url_vcode = null, $ref = null) {
        $conditions = array(
            'conditions' => array(
                'User.id' => $url_id,
                'User.veri_code' => $url_vcode,
        ));
        $result = $this->User->find('first', $conditions);
        if (isset($result['User']) && !empty($result)) {
            if ($result['User']['is_active']) {
                $this->Session->setFlash(__('Your account is already verified.'), 'front_success');
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->User->updateAll(array('User.is_active' => '1', 'User.is_verified' => '1'), array('User.id' => $url_id));
            if ($this->Auth->login($result['User'])) {
                if (AuthComponent::user('role') == 11) {
                    if ($ref == 'set_password') {
                        $this->Session->setFlash(__('Your account has been successfully verified.Please Set Your Password .'), 'success');
                        return $this->redirect(array('action' => 'set_user_password', $url_id));
                    } else {
                        $this->Session->setFlash(__('Your account has been successfully verified.'), 'success');
                        return $this->redirect(array('controller' => 'users', 'action' => 'my_account', $url_id));
                    }
                } else {
                    $this->Session->setFlash(__('Your account has been successfully verified.'), 'success');
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
            } else {
                $this->Session->setFlash(__('Unauthorised user details, verification failed'), 'error');
                return $this->redirect(array('action' => 'login'));
            }
        }
    }

    public function registration_ver() {
        if ($this->request->is('post')) {
            if ($this->request->data['User']['term'] == 0) {
                $this->Session->setFlash(__('Please Accept Term And Conditions'), 'error');
                return false;
            }

            if (empty($this->request->data['User']['password']) || empty($this->request->data['User']['confirm_password']) || empty($this->request->data['User']['mobile']) || empty($this->request->data['User']['email']) || empty($this->request->data['User']['first_name']) || empty($this->request->data['User']['last_name'])) {
                $this->Session->setFlash(__('Please Fill Form '), 'error');
                return false;
            }



            if ($this->Session->read('captcha_code') != $this->request->data['User']['captcha']) {
                $this->Session->setFlash(__('Please enter correct captcha code.'), 'error');
                return false;
            }
            if ($this->User->find('first', array('conditions' => array('User.email' => $this->request->data['User']['email'])))) {
                $this->Session->setFlash(__('This Email Already Used .'), 'error');
                return false;
            }
            $v_code = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz123456789', 15)), 0, 15);
            $this->request->data['User']['role'] = 11;
            $this->request->data['User']['veri_code'] = $v_code;
            $data = $this->request->data;
            $this->loadModel('Category');
            $category = $this->Category->find('all', array(
                'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
            ));
            $this->set(compact('category'));

            if ($this->User->save($this->request->data)) {
                $data['User']['id'] = $this->User->id;
                $email = new CakeEmail();
                $email->config('user_registration');
                $email->to($this->request->data['User']['email']);
                $email->viewVars(compact('data', 'category'));
                $email->subject('Online Registration on Value Market Research');
                $email->send();
                $this->Session->setFlash(__('A mail has been sent to your mailbox . Please open and click on the link to verify your account, its mandatory. Unless you verify your account you will not be allowed to login.'), 'success');
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            } else {
                $this->Session->setFlash(__('Invalid information please try again'), 'success');
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
        }
    }

    public function registration() {

        if (AuthComponent::user('id')) {
            if (AuthComponent::user('role') == 1) {
                return $this->redirect(array('controller' => 'products', 'action' => 'pending_status'));
            } if (AuthComponent::user('role') == 2) {
                return $this->redirect(array('controller' => 'products', 'action' => 'index'));
            } if (AuthComponent::user('role') == 11) {
                return $this->redirect(array('controller' => 'users', 'action' => 'my_account', AuthComponent::user('id')));
            } else {
                return $this->redirect(array('controller' => 'users', 'action' => 'dashbord'));
            }
        }

        if ($this->request->is('post')) {

            if ($this->request->data['User']['term'] == 0) {
                $this->Session->setFlash(__('Please Accept Term And Conditions'), 'error');
                return false;
            }

            if (empty($this->request->data['User']['password']) || empty($this->request->data['User']['confirm_password']) || empty($this->request->data['User']['mobile']) || empty($this->request->data['User']['email']) || empty($this->request->data['User']['first_name']) || empty($this->request->data['User']['last_name'])) {
                $this->Session->setFlash(__('Please Fill Form '), 'error');
                return false;
            }
            $pwd = $this->request->data['User']['password'];
            if ($this->Session->read('captcha_code') != $this->request->data['User']['captcha']) {
                $this->Session->setFlash(__('Please enter correct captcha code.'), 'error');
                return false;
            }
            if ($this->User->find('first', array('conditions' => array('User.email' => $this->request->data['User']['email'])))) {
                $this->Session->setFlash(__('This Email Already Registered .'), 'error');
                return false;
            }
//            if ($this->User->find('first', array('conditions' => array('User.mobile' => $this->request->data['User']['mobile'])))) {
//                $this->Session->setFlash(__('This Mobile Already Registered .'), 'error');
//                return false;
//            }
//  $v_code = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz123456789', 15)), 0, 15);
            $this->request->data['User']['role'] = 11;
//    $this->request->data['User']['veri_code'] = $v_code;
            $data = $this->request->data;
            $this->loadModel('Category');
            $category = $this->Category->find('all', array(
                'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
            ));
            $this->set(compact('category'));

            $this->request->data['User']['visited_ip'] = $this->request->clientIp();

            if ($this->User->save($this->request->data)) {
                $data['User']['id'] = $this->User->id;
                $email = new CakeEmail();
                $email->config('user_registration');
                $email->to($this->request->data['User']['email']);
                $email->viewVars(compact('data', 'pwd', 'category'));
                $email->subject('Online Registration on Value Market Research');
                $email->send();
                if ($this->Auth->login($data['User'])) {

                    $last_login = Date('Y-m-d H:i:s');
                    $this->User->updateAll(array('User.last_login' => "'$last_login'"), array('User.id' => AuthComponent::user('id')));
                    if (AuthComponent::user('role') == 11) {

                        if ($this->Session->check('bill_log')) {
                            $cart_data = $this->Session->read('bill_log');
                            if ($cart_data == 'bill_info') {
                                if (AuthComponent::user('role') == 11) {
                                    return $this->redirect(array('controller' => 'cart_items', 'action' => 'billing_info'));
                                }
                            }
                        }
                        $this->Session->setFlash(__('Your account has been successfully verified.'), 'success');
                        return $this->redirect(array('controller' => 'users', 'action' => 'my_account', $data['User']['id']));
                    } else {
                        $this->Session->setFlash(__('Your account has been successfully verified.'), 'success');
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                    }
                } else {
                    $this->Session->setFlash(__('Unauthorised user details, verification failed'), 'error');
                    return $this->redirect(array('action' => 'login'));
                }
            } else {
                $this->Session->setFlash(__(
                                'Invalid information please try again'), 'success');
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
        }

// Menu-Research Industries (Category and subCategory) code start here
      $this->loadModel('Category');
    //   $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
    //   foreach ($research_ind_cat as $key1 => $cat) {
    //       $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
    //       $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
    //       $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
    //       $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
          $level_two = $this->Category->find('all', array('conditions' =>
              array(
                  'Category.parent_category_id' => 2,
                  'Category.is_active' => 1), 'recursive' => -1));
foreach ($level_two as $key2 => $level2) {
                $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
    // }

// Menu-Research Industries (Category and subCategory) code end here

      $this->set(compact('research_ind'));


    }

    public function change_login() {
        if ($this->request->is('post')) {
            if (empty($this->request->data['User']['id'])) {
                $this->Session->setFlash(__(
                                'Invalid user please try again'), 'error');
                return $this->redirect(array('action' => 'login'));
            }
            if (!$this->Session->check('admin_data')) {
                $this->Session->write('admin_data', AuthComponent::user());
            }
//            if ($this->Session->check('admin_data')) {
//                $admin_dtl = $this->Session->read('admin_data');
//            }

            $data = $this->User->find('first', array('conditions' => array('User.id' => $this->request->data['User']['id']), 'recursive' => -1));
            if (!empty($data)) {
                if ($this->Auth->login($data['User'])) {
                    return $this->redirect(array('action' => 'login'));
                }
            }
        }
    }

    public function login_as_admin() {
        if ($this->Session->check('admin_data')) {
            $admin_dtl = $this->Session->read('admin_data');
            $data = $this->User->find('first', array('conditions' => array('User.id' => $admin_dtl['id']), 'recursive' => -1));
            if (!empty($data)) {
                if ($this->Auth->login($data['User'])) {
                    return $this->redirect(array('action' => 'login'));
                }
            } else {
                return $this->redirect('/');
            }
        }
    }

    public function logout() {
        $this->Auth->logout();
        $this->Session->destroy();
        $this->Session->setFlash('You are logged out!', 'success');
        return $this->redirect('/');
    }

    public function delete($id = null, $ref = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }

        $this->request->onlyAllow('post', 'delete');
        if ($this->User->delete()) {
            $this->Session->setFlash(__('The user has been deleted.'), 'success');
        } else {
            $this->
            Session->setFlash(__('The user could not be deleted. Please, try again.'), 'error');
        }
        if ($ref == 'lead_user_index') {
            return $this->redirect(array('action' => 'lead_user_index'));
        } else {
            return $this->redirect(array('action' => 'index'));
        }
    }

    function captcha() {
        $this->autoRender = false;
// $this->layout = 'ajax';
        if (!isset($this->Captcha)) {
//if Component was not loaded throug $components array()
            App::import('Component', 'Captcha'); //load it
            $this->Captcha = new CaptchaComponent(); //make instance
            $this->Captcha->startup($this); //and do some manually calling
        }
        $this->Captcha->create();
    }

    public function contactus() {
      $this->loadModel('Category');

        if ($this->request->is('post')) {
            
            // New Google Captcha code VG-26-2-2018
            // $secretKey = "6LekeEYUAAAAAD7XUUSIByNUfH2dGBJZMjfWBJnn";
            $secretKey = "6LfRUFIUAAAAAGEzHagig3JaXO1D5OQ7M1WHjjBp";
            
            $responseKey = $_POST['g-recaptcha-response'];
            $userIP = $_SERVER['REMOTE_ADDR'];
            
            $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$responseKey&remoteip=$userIP";
            $response = file_get_contents($url);
            $response = json_decode($response);
            
            if (!$response->success){
                $this->Session->setFlash(__('Please enter correct captcha code.'), 'error');
                return $this->redirect($this->referer());
            }
            // New Google Captcha code VG 26-2-2018
            
            // if ($this->Session->read('captcha_code') != $this->request->data['User']['captcha']) {
            //     $this->Session->setFlash(__('Please enter correct captcha code.'), 'error');
            //     return false;
            // }
            if (empty($this->request->data['User']['name']) || empty($this->request->data['User']['email']) || empty($this->request->data['User']['mobile']) || empty($this->request->data['User']['message'])) {
                $this->Session->setFlash(__('Please Fill all Fields'), 'error');
                return false;
            }

            $validate = true;
            // New Code To Filter the Textarea from badwords VG 07-07-2020
                
            if(!empty($this->request->data['User']['message'])) {
                $str = $this->request->data['User']['message'];
                $bad_words = array('adult', 'porn', 'ass', 'sex');
                $reg = '~\b' . implode('\b|\b', $bad_words) . '\b~';

                preg_match_all($reg, preg_replace('~[.,?!]~', '', $str), $matches);

                if(count($matches[0]) > 0) {
                    $this->Session->setFlash(__('Bad Words in Message '), 'error');
                    $validate = false;
                    // $this->redirect($this->referer());
                }
                $string = $this->request->data['User']['message'];
                if(preg_match('/(http|ftp|mailto|https|WWW|www)/', $string, $matches)) {
                    $this->Session->setFlash(__('URL not allowed in Message '), 'error');
                    $validate = false;
                    // $this->redirect($this->referer());
                }
            }

            // New Code To Filter the Textarea from badwords VG 07-07-2020
            if($validate == true) {

                $data = $this->request->data;
                $data['User']['visited_ip'] = $this->request->clientIp();

                $this->loadModel('Setting');
                $category = $this->Category->main_cat_for_email();



                $this->set(compact('category'));
                $email = new CakeEmail();
                $email->config('thanking_contact_us');
                $email->to($data['User']['email']);
                $email->viewVars(compact('data', 'category'));
                $email->subject('Thanking For Connect With Us');
                $email->send();
                $mail_to = $this->Setting->find_setting(1);
                $eml = new CakeEmail();
                $eml->config('new_contact_message');
                $eml->to($mail_to['Setting']['mail_to']);
                $eml->viewVars(compact('data', 'category'));
                $eml->subject('New Message Received ');
                if ($eml->send()) {
                    $this->Session->setFlash(__('Thanking You .. Your Message Received'), 'success');
                } else {
                    $this->Session->setFlash(__('The user could not be Send. Please, try again.'), 'error');
                }
                return $this->redirect(array('action' => 'contactus'));
            }
        }

// Menu-Research Industries (Category and subCategory) code start here
        // $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
        // foreach ($research_ind_cat as $key1 => $cat) {
        //     $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
        //     $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
        //     $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
        //     $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
            $level_two = $this->Category->find('all', array('conditions' =>
                array(
                    'Category.parent_category_id' => 2,
                    'Category.is_active' => 1), 'recursive' => -1));
foreach ($level_two as $key2 => $level2) {
                $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
        // }

// Menu-Research Industries (Category and subCategory) code end here

        $meta_name="Contact us - Value Market Research";
        $meta_desc="We would be glad to hear your questions, comments and concerns. Mail us at sales@valuemarketresearch.com";
        $meta_keyword="Contact Value Market Research, Contact VMR";
        $this->set(compact('meta_name','meta_desc','meta_keyword','research_ind'));
    }

    public function aboutus() {

        $meta_name="About us: Value Market Research";
        $meta_desc="Value Market Research was incepted with the idea to add value to the businesses worldwide. Our Market Research Reports and Industry Analysis covers detailed information for decision makers to take an informed decision.";
        $meta_keyword="Value Market Research, Market Research Reports, About Value Market Research, VMR Reports";
        $this->loadModel('Category');
        $ourclients = $this->Category->our_client_fun();

// Menu-Research Industries (Category and subCategory) code start here
        // $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
        // foreach ($research_ind_cat as $key1 => $cat) {
        //     $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
        //     $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
        //     $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
        //     $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
            $level_two = $this->Category->find('all', array('conditions' =>
                array(
                    'Category.parent_category_id' => 2,
                    'Category.is_active' => 1), 'recursive' => -1));
            foreach ($level_two as $key2 => $level2) {
                $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
        // }

// Menu-Research Industries (Category and subCategory) code end here


         $this->set(compact('meta_name', 'research_ind', 'meta_desc','meta_keyword','ourclients'));
    }

    public function privacy_policy() {

        $meta_name="Privacy Policy - Value Market Research";
        $meta_keyword="Privacy Policy, Value Market Research, VMR Reports";
        $meta_desc = "Value Market Research understands that you care how information about you is used and shared. This notice describes our privacy policy.";
// Menu-Research Industries (Category and subCategory) code start here
        $this->loadModel('Category');
        // $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
        // foreach ($research_ind_cat as $key1 => $cat) {
        //     $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
        //     $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
        //     $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
        //     $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
            $level_two = $this->Category->find('all', array('conditions' =>
                array(
                    'Category.parent_category_id' => 2,
                    'Category.is_active' => 1), 'recursive' => -1));
            foreach ($level_two as $key2 => $level2) {
                $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
        // }

// Menu-Research Industries (Category and subCategory) code end here


        $this->set(compact('research_ind', 'meta_name','meta_keyword', 'meta_desc'));


    }

    public function terms_conditions() {
// Menu-Research Industries (Category and subCategory) code start here
      $this->loadModel('Category');
    //   $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
    //   foreach ($research_ind_cat as $key1 => $cat) {
    //       $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
    //       $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
    //       $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
    //       $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
          $level_two = $this->Category->find('all', array('conditions' =>
              array(
                  'Category.parent_category_id' => 2,
                  'Category.is_active' => 1), 'recursive' => -1));
          foreach ($level_two as $key2 => $level2) {
                $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
    //   }

// Menu-Research Industries (Category and subCategory) code end here
    $meta_name="Terms & Conditions - Value Market Research";
    $meta_desc="Read Value Market Research's detailed terms and conditions with respect to our services and products here.";
    $meta_keyword="Terms & Conditions,  VMR Reports, Value Market Research";

      $this->set(compact('research_ind','meta_name','meta_desc','meta_keyword'));
    }


    public function accept_terms_conditions() {
        $this->layout = false;
    }

    public function page_not_found() {
// Menu-Research Industries (Category and subCategory) code start here
        $this->loadModel('Category');
        // $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
        // foreach ($research_ind_cat as $key1 => $cat) {
        //     $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
        //     $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
        //     $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
        //     $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
            $level_two = $this->Category->find('all', array('conditions' =>
                array(
                    'Category.parent_category_id' => 2,
                    'Category.is_active' => 1), 'recursive' => -1));
            foreach ($level_two as $key2 => $level2) {
                $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
        // }

// Menu-Research Industries (Category and subCategory) code end here


        $this->set(compact('research_ind'));
    }

    public function role_users($role = null) {
        $this->layout = null;
        $result = $this->User->find('list', array('conditions' => array('User.role' => $role), 'fields' => array('User.id', 'User.name'), 'recursive' => -1));
        $this->set(compact('result'));
        $this->render('filter');
    }

    public function offer_price($id = null, $user_id = null) {
        $this->layout = NULL;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            // debug($this->request->data);die;
            $data['CartItem']['is_offer_set'] = 1;
            $data['CartItem']['id'] = $id;
            //s debug($this->request->data);die;
            $this->loadModel('CartItem');
            if ($this->CartItem->save($data)) {
                $this->Session->setFlash(__('The Offer Price has been saved.'));
                return $this->redirect(array('action' => 'view', $user_id));
            } else {
                $this->Session->setFlash(__('The Offer Price could not be saved. Please, try again.'));
            }
        }
        $this->loadModel('CartItem');
        $curr_price = $this->CartItem->find('first', array('conditions' => array('CartItem.id' => $id), 'fields' => array('CartItem.current_price'), 'recursive' => 1));
        $this->set(compact('curr_price'));
    }

    public function careers() {
        $meta_name="Career opportunities with Valuemarketresearch.com";
        $meta_desc="Explore the latest job opportunities in varoius domain with valuemarketresearch.com.";
        $this->set(compact('meta_name','meta_desc'));

    }

    public function thanks($slug = null) { //echo "Id: ".$id."<br>Slug: ".$slug; exit();
        /*$line3=$this->Session->read('id');
        $line4=$this->Session->read('slug');*/
       // echo "id:".$line3."<br>"."slug:".$line4;exit;
        // if (!$this->Session->check('thanks')) {
        //     $this->redirect(array('controller' => 'orders', 'action' => 'user_order_index'));
        // }
        // print_r($slug);exit;
    
        // code starts here-VG-10/08/2016
         $this->loadModel('Category');
         $this->loadModel('Product');
         $this->loadModel('ProductCategory');

        //  $mapped_category_id=$this->ProductCategory->find('all',array('conditions'=>array('ProductCategory.product_id'=>$id))); // fetched category_id from ProductCategory model from product_id
         
        //  if(count($mapped_category_id)==1 && $mapped_category_id[0]['Category']['parent_category_id']==2){
        //     $related_category_name = $mapped_category_id; // Here condition checked that if fetched result count is one and parent_category_id is equals 2 then required data is copied in $related_category_name
        //  }
        //  else{

            $related_category_name=$this->Category->find('first',array('conditions'=>array('Category.slug'=>$slug))); // Here required data is fetched from Cateogry model with category id.
        //  }
         
         
         $this->set(compact('related_category_name'));
        // code ends here-VG-10/08/2016

        $line1 = "Thank You for contacting Value Market Research and sharing your details.";
        $line2 = "Our Sales team representative will get in touch with you soon. In case you wish to contact us, kindly write to us at <a href='mailto:sales@valuemarketresearch.com'>sales@valuemarketresearch.com</a>"; //sales@decisiondatabases.com

            /*<a href="mailto:sales@decisiondatabases.com">sales@decisiondatabases.com</a>*/

        //code start here-VG-23/08/2016
        // $id = $id;
        $slug = $slug;  //retrieving  querystring value here
        // $line3=$id;//$this->Session->read('id');  //echo $line3."<br>";
        $line4=$slug;//$this->Session->read('slug'); //echo $line4."<br>";exit();

// Menu-Research Industries (Category and subCategory) code start here
      $this->loadModel('Category');
    //   $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
    //   foreach ($research_ind_cat as $key1 => $cat) {
    //       $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
    //       $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
    //       $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
    //       $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
          $level_two = $this->Category->find('all', array('conditions' =>
              array(
                  'Category.parent_category_id' => 2,
                  'Category.is_active' => 1), 'recursive' => -1));
          foreach ($level_two as $key2 => $level2) {
                $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
    //   }

// Menu-Research Industries (Category and subCategory) code end here


        $this->set(compact('research_ind', 'line1', 'line2','line4'));

        //code end here-VG-23/08/2016
    }

    public function payment_success() {
        //debug($_POST);
        //debug('here success');die;
    }

    public function payment_failed() {
        //debug($_POST);
        //debug('here success');die;
    }
    
    public function faq(){
        $meta_name="FAQ - Value Market Research";
        $meta_desc="Read answers to the Frequently Asked Questions related to Value Market Research's market reports and industry analysis, delivery process, payment methods, refund etc.";
        $meta_keyword="FAQ, Frequently Asked Questions, FAQ to VMR";
        $this->loadModel('Category');

// Menu-Research Industries (Category and subCategory) code start here
        // $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
        // foreach ($research_ind_cat as $key1 => $cat) {
        //     $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
        //     $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
        //     $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
        //     $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
            $level_two = $this->Category->find('all', array('conditions' =>
                array(
                    'Category.parent_category_id' => 2,
                    'Category.is_active' => 1), 'recursive' => -1));
            foreach ($level_two as $key2 => $level2) {
                $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
        // }

    // Menu-Research Industries (Category and subCategory) code end here

         $this->set(compact('meta_name', 'research_ind', 'meta_desc','meta_keyword'));
    }

    public function refundpolicy(){
        $meta_name="Refund Policy - Value Market Research";
        $meta_desc="The below mentioned Refund policy is applicable to all the products - Market Research Reports, Industry Analysis, Customized Reports sold through our site - www.valuemarketresearch.com";
        $meta_keyword="Refund policy, Market Research Reports, Industry Analysis, Customized Reports, VMR Reports, Value Market Research";
        $this->loadModel('Category');

// Menu-Research Industries (Category and subCategory) code start here
        // $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
        // foreach ($research_ind_cat as $key1 => $cat) {
        //     $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
        //     $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
        //     $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
        //     $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
            $level_two = $this->Category->find('all', array('conditions' =>
                array(
                    'Category.parent_category_id' => 2,
                    'Category.is_active' => 1), 'recursive' => -1));
            foreach ($level_two as $key2 => $level2) {
                $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
        // }

    // Menu-Research Industries (Category and subCategory) code end here

         $this->set(compact('meta_name', 'research_ind', 'meta_desc','meta_keyword'));
    }


    public function howtoorder(){
        $meta_name="How to Order - Value Market Research";
        $meta_desc="Buying from Value Market Research for the first time! Please follow the below instructions.";
        $meta_keyword="Buy Market Research Reports, Order Market research Reports, Buy VMR Reports";
        $this->loadModel('Category');

// Menu-Research Industries (Category and subCategory) code start here
        // $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
        // foreach ($research_ind_cat as $key1 => $cat) {
        //     $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
        //     $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
        //     $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
        //     $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
            $level_two = $this->Category->find('all', array('conditions' =>
                array(
                    'Category.parent_category_id' => 2,
                    'Category.is_active' => 1), 'recursive' => -1));
            foreach ($level_two as $key2 => $level2) {
                $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
        // }

    // Menu-Research Industries (Category and subCategory) code end here

         $this->set(compact('meta_name', 'research_ind', 'meta_desc','meta_keyword'));
    }

    public function researchmethodology(){
        $meta_name="Research Methodology - Value Market Research";
        $meta_desc="Value Market Research ensures reliable and accurate data reaches to the strategists. Our reports are based on in-depth interviews and discussions with a wide range of key industry participants and opinion leaders.";
        $meta_keyword="Research Methodology, Value Market Research, VMR Methodology, Value Market Research Methodology";
        $this->loadModel('Category');

// Menu-Research Industries (Category and subCategory) code start here
        // $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
        // foreach ($research_ind_cat as $key1 => $cat) {
        //     $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
        //     $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
        //     $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
        //     $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
            $level_two = $this->Category->find('all', array('conditions' =>
                array(
                    'Category.parent_category_id' => 2,
                    'Category.is_active' => 1), 'recursive' => -1));
            foreach ($level_two as $key2 => $level2) {
                $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
        // }

    // Menu-Research Industries (Category and subCategory) code end here

         $this->set(compact('meta_name', 'research_ind', 'meta_desc','meta_keyword'));
    }

    public function disclaimers(){
        $meta_name="Disclaimer: Value Market Research";
        $meta_desc="Value Market Research is not responsible for any inaccurate information provided in our reports. As this information is collected based on interviews fluctuations and diversion of data is possible, though we ensure regular checks and data validation through secondary sources.";
        $meta_keyword="Value Market Research, VMR Disclaimer, VMR Reports";
        $this->loadModel('Category');

// Menu-Research Industries (Category and subCategory) code start here
        // $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
        // foreach ($research_ind_cat as $key1 => $cat) {
        //     $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
        //     $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
        //     $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
        //     $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
            $level_two = $this->Category->find('all', array('conditions' =>
                array(
                    'Category.parent_category_id' => 2,
                    'Category.is_active' => 1), 'recursive' => -1));
            foreach ($level_two as $key2 => $level2) {
                $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
        // }

    // Menu-Research Industries (Category and subCategory) code end here

         $this->set(compact('meta_name', 'research_ind', 'meta_desc','meta_keyword'));
    }
    

    public function beforeFilter() {
        $this->Auth->allow(array('resetpassword', 'resetadd','forgotresetpassword','forgotresetadd', 'logout', 'disclaimers','privacy_policy', 'aboutus', 'contactus', 'careers',
            'login_front_forgot_password', 'test', 'captcha', 'set_user_password',
            'user_verification', 'registration', 'my_account', 'forgot_password',
            'login', 'login_front_user', 'login_as_admin', 'terms_conditions', 'role_users',
            'page_not_found', 'thanks', 'payment_success', 'payment_failed','faq','refundpolicy','howtoorder','researchmethodology', 'accept_terms_conditions'));
    }

}
