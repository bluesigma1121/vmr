<?php

App::uses('AppController', 'Controller');

/**
 * CountryRegions Controller
 *
 * @property CountryRegion $CountryRegion
 * @property PaginatorComponent $Paginator
 */
class CountryRegionsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->CountryRegion->recursive = 0;
        $this->set('countryRegions', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->CountryRegion->exists($id)) {
            throw new NotFoundException(__('Invalid country region'));
        }
        $options = array('conditions' => array('CountryRegion.' . $this->CountryRegion->primaryKey => $id));
        $this->set('countryRegion', $this->CountryRegion->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->CountryRegion->create();
            if ($this->CountryRegion->save($this->request->data)) {
                $this->Session->setFlash(__('The country region has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The country region could not be saved. Please, try again.'));
            }
        }
        $countries = $this->CountryRegion->Country->find('list');
        $regions = $this->CountryRegion->Region->find('list');
        $this->set(compact('countries', 'regions'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->CountryRegion->exists($id)) {
            throw new NotFoundException(__('Invalid country region'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->CountryRegion->save($this->request->data)) {
                $this->Session->setFlash(__('The country region has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The country region could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('CountryRegion.' . $this->CountryRegion->primaryKey => $id));
            $this->request->data = $this->CountryRegion->find('first', $options);
        }
        $countries = $this->CountryRegion->Country->find('list');
        $regions = $this->CountryRegion->Region->find('list');
        $this->set(compact('countries', 'regions'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->CountryRegion->id = $id;
        if (!$this->CountryRegion->exists()) {
            throw new NotFoundException(__('Invalid country region'), 'error');
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->CountryRegion->delete()) {
            $this->Session->setFlash(__('The country region has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The country region could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect($this->referer());
    }

}
