<?php

App::uses('AppController', 'Controller');

/**
 * Reportdescriptionimages Controller
 *
 * @property Reportdescriptionimage $Reportdescriptionimage
 * @property PaginatorComponent $Paginator
 */
class ReportdescriptionimagesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
   
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->layout = 'admin_layoutfooter';
        //$this->Rreportdescriptionimage->recursive = 0;
       //$this->set('reportdescriptionimages');
        // Configure::load('idata');
        // $yes_no = Configure::read('idata.yes_no');
        // $this->set(compact('yes_no'));
        $countries = $this->Reportdescriptionimage->find('all');
        $this->set('countries', $countries); 

        if (empty($_POST['searchrdimage'])) {
            $this->paginate = array(
                //'conditions' => array('Reportdescriptionimage.id >=' => $ssid['id']),
                'fields' => array('id','product_description','product_no','slug','meta_desc'),
                'limit' => 11,
                'order'=>'id ASC'
            );
            $tarcounselect = $this->Paginator->paginate();
            $this->set('tarcounselect', $tarcounselect); 


        } else {
           // $countryselect = $_POST['countryselected'];
            $countryselect = $_POST['searchrdimage'];

            $sqlselect=$this->Reportdescriptionimage->query("SELECT id FROM products WHERE slug like '%$countryselect%'");
        
            $sid=$sqlselect[0];
            foreach($sid as $ssid)
            {
            //    print_r($ssid['id']);
            //    exit;
            }
          

           /* $tarcounselect = $this->Reportdescriptionimage->find('all',  array('conditions' => array('Reportdescriptionimage.id >=' => $ssid['id']
                                                                //, 'OR' => array(
                                                                //'Rdupdateimagedatas.slug' => $countryselect
                                                                //'Rdupdateimagedatas.slug LIKE' => "%".$countryselect."%"
                                                                //'Rdupdateimagedatas.slug' => $countryselect
                                                                    //)
                                                                   // 'Rdupdateimagedatas.product_description LIKE' => "%".'/<strong>Europe([a-z0-9]*)\?</strong>/i'."%"
                                                                ),
                                                                   //'recursive' => -1,
                                                                   //'order' => 'Rdupdateimagedatas.id DESC',
                                                                    'fields'=>array('id','product_description','product_no','slug','meta_desc')                                                                   //'limit'=>10
                                                                   //'recursive' => -1
                                                                  ));

                    $this->set('tarcounselect', $tarcounselect); */
                    $this->paginate = array(
                        'conditions' => array('Reportdescriptionimage.id >=' => $ssid['id']),
                        'fields' => array('id','product_description','product_no','slug','meta_desc'),
                        'limit' => 11,
                        'order'=>'id ASC'
                    );
                    $tarcounselect = $this->Paginator->paginate();
                    $this->set('tarcounselect', $tarcounselect); 
                   
        }

    }

    
    public function add()
    {
        $this->layout = 'admin_layout';
        for($i=0;$i<count($_POST['productdescription']);$i++)
            //    for($i=100;$i<=999;$i++)
				{
                       $productdescription = $_POST['productdescription'][$i];
                       $idss =$_POST['ids'][$i];

                      
                       $descriptiont = $_POST['description'][$i];
                       $metadescription = $_POST['metadescription'][$i];

                       
                         $sqlurl1 = "UPDATE products SET product_description ='".$descriptiont."' WHERE id ='".$idss."'";                        
                         $queryurl1 = $this->Reportdescriptionimage->query($sqlurl1);
                        
                        
                        $sqlurl2 = "UPDATE products SET meta_desc ='".$metadescription."' WHERE id ='".$idss."'";   
                         $queryurl2 = $this->Reportdescriptionimage->query($sqlurl2);
                        //-----description replace----
                    }
                  
                   
                    $this->Session->setFlash(__('The Product Description has been Updated.'), 'success');
                    $this->redirect($this->referer());
                    //$this->redirect(array('action' => 'index')); 
                  

                    
                      
                       
    }

 
}