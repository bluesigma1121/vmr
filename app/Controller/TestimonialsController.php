<?php

App::uses('AppController', 'Controller');

/**
 * Testimonials Controller
 *
 * @property Testimonial $Testimonial
 * @property PaginatorComponent $Paginator
 */
class TestimonialsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->layout = 'admin_layout';
        $this->Testimonial->recursive = 0;
        $this->set('testimonials', $this->Paginator->paginate());
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no'));
    }

    public function user_testi_index($id = null) {
        if (AuthComponent::user('id') != $id) {
            $this->Session->setFlash(__('The user has Not Found .'), 'error');
            return $this->redirect(array('action' => 'login'));
        }
        $this->Testimonial->recursive = 0;
        $this->paginate = array(
            'conditions' => array('Testimonial.user_id' => $id),
            'order' => 'Testimonial.created DESC',
            'limit' => 50
        );
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set('user_testies', $this->Paginator->paginate());
        $this->set(compact('yes_no'));
    }

    public function user_view($id = null) {
        $this->layout = null;
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $testi = $this->Testimonial->find('first', array('conditions' => array('Testimonial.id' => $id)));
        $this->set(compact('yes_no', 'testi'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->Testimonial->exists($id)) {
            throw new NotFoundException(__('Invalid testimonial'));
        }
        $options = array('conditions' => array('Testimonial.' . $this->Testimonial->primaryKey => $id));
        $this->set('testimonial', $this->Testimonial->find('first', $options));
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $this->request->data['Testimonial']['user_id'] = $this->Auth->user('id');
            $this->Testimonial->create();
            if ($this->Testimonial->save($this->request->data)) {
                $this->Session->setFlash(__('The testimonial has been saved.'), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The testimonial could not be saved. Please, try again.'), 'error');
            }
        }
        $users = $this->Testimonial->User->find('list');
        $this->set(compact('users'));
    }
    /*New add method start for super_admin VG-20/12/2016*/
    public function admin_add(){
        $this->layout = 'admin_layout';

        if ($this->request->is('post')) {
            $data= $this->request->data;
            $data['Testimonial']['user_id'] = $this->Auth->user('id');

            $this->Testimonial->create();
            if ($this->Testimonial->save($data)) {
                $this->Session->setFlash(__('The testimonial has been saved.'), 'success');
                return $this->redirect(array('action' => 'index', $this->Auth->user('id')));
            } else {
                $this->Session->setFlash(__('The testimonial could not be saved. Please, try again.'), 'error');
            }
        }
        $users = $this->Testimonial->User->find('list');
        $this->set(compact('users'));
    }
/*New add method End for super_admin VG-20/12/2016*/

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->Testimonial->exists($id)) {
            throw new NotFoundException(__('Invalid testimonial'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Testimonial']['is_active'] = 0;
            $this->request->data['Testimonial']['is_verified'] = 0;

            if ($this->Testimonial->save($this->request->data)) {
                $this->Session->setFlash(__('The testimonial has been saved.'), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The testimonial could not be saved. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('Testimonial.' . $this->Testimonial->primaryKey => $id));
            $this->request->data = $this->Testimonial->find('first', $options);
        }
        $users = $this->Testimonial->User->find('list');
        $this->set(compact('users'));
    }

    /*New edit method start for super_admin VG-20/12/2016*/
    public function admin_edit($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->Testimonial->exists($id)) {
            throw new NotFoundException(__('Invalid testimonial'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $data= $this->request->data;
            $data['Testimonial']['is_active'] = 0;
            $data['Testimonial']['is_verified'] = 0;

            if ($this->Testimonial->save($data)) {
                $this->Session->setFlash(__('The testimonial has been saved.'), 'success');
                return $this->redirect(array('action' => 'index', $this->Auth->user('id')));
            } else {
                $this->Session->setFlash(__('The testimonial could not be saved. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('Testimonial.' . $this->Testimonial->primaryKey => $id));
            $this->request->data = $this->Testimonial->find('first', $options);
        }
        $users = $this->Testimonial->User->find('list');
        $this->set(compact('users'));
    }
/*New edit method End for super_admin VG-20/12/2016*/
    public function active($id = NULL) {
        if ($this->Testimonial->updateAll(array('Testimonial.is_active' => 1, 'Testimonial.is_verified' => 1), array('Testimonial.id' => $id))) {
            $this->Session->setFlash(__('The Testimonial has been Activated.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Testimonial could not be Activated. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function deactive($id = NULL) {
        if ($this->Testimonial->updateAll(array('Testimonial.is_active' => 0), array('Testimonial.id' => $id))) {
            $this->Session->setFlash(__('The Testimonial has been DeActivated.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Testimonial could not be DeActivated. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function verified($id = NULL) {
        if ($this->Testimonial->updateAll(array('Testimonial.is_verified' => 1), array('Testimonial.id' => $id))) {
            $this->Session->setFlash(__('The Testimonial has been verified.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Testimonial could not be verified. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function deverified($id = NULL) {
        if ($this->Testimonial->updateAll(array('Testimonial.is_verified' => 0), array('Testimonial.id' => $id))) {
            $this->Session->setFlash(__('The Testimonial has been Deverified.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Testimonial could not be Deverified. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Testimonial->id = $id;
        if (!$this->Testimonial->exists()) {
            throw new NotFoundException(__('Invalid testimonial'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Testimonial->delete()) {
            $this->Session->setFlash(__('The testimonial has been deleted.'),'success');
        } else {
            $this->Session->setFlash(__('The testimonial could not be deleted. Please, try again.'),'error');
        }
        return $this->redirect(array('action' => 'index'));
    }


    public function listing() {
        $this->Testimonial->recursive = 0;
        $this->paginate = array(
            'order' => 'Testimonial.id DESC',
            'limit' => 20
        );

// Menu-Research Industries (Category and subCategory) code start here
        $this->loadModel('Category');
        // $research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
        // foreach ($research_ind_cat as $key1 => $cat) {
        //     $research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
        //     $research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
        //     $research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
        //     $research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
            $level_two = $this->Category->find('all', array('conditions' =>
                array(
                    'Category.parent_category_id' => 2,
                    'Category.is_active' => 1), 'recursive' => -1));
            foreach ($level_two as $key2 => $level2) {
                $research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
                $research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
        // }

// Menu-Research Industries (Category and subCategory) code end here
        $this->loadModel('Category');
        $ourclients = $this->Category->our_client_fun();
        $this->set(compact('ourclients'));

        $meta_name="Testimonials - Value Market Research";
        $meta_desc="Read what our esteemed clients have to say about us.";
        $meta_keyword="Testimonials, VMR Reports, Industry Reports";

        $testimonials = $this->Paginator->paginate();
        $this->set(compact('research_ind','testimonials', 'meta_name', 'meta_desc', 'meta_keyword'));
    }

    public function beforeFilter() {
        $this->Auth->allow(array('listing'));
    }

}
