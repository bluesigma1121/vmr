<?php
App::uses('AppController', 'Controller');
App::uses('Spreadsheet_Excel_Reader', 'Vendor');
App::uses('CakeEmail', 'Network/Email');

class CronjobController extends AppController {
    public function cron_details() {
        $this->layout = false;
        $this->loadModel('Crons');
        $producttotal_active = $this->Crons->find('list', array(
            'conditions' => array('DATE(Crons.created)' => date('Y-m-d')),
            'fields' => array('Crons.id', 'Crons.created'),
        ));
        $cnt_ready_to_active = count($producttotal_active);

        $producttotal_modified = $this->Crons->find('list', array(
            'conditions' => array('DATE(Crons.modified)' => date('Y-m-d')),
            'fields' => array('Crons.id', 'Crons.modified'),
        ));
        $cnt_producttotal_modified_active = count($producttotal_modified);


        $this->loadModel('Enquiry');
        $tleads_created = $this->Enquiry->find('list', array(
            'conditions' => array('DATE(Enquiry.created)' => date('Y-m-d')),
            'fields' => array('Enquiry.id', 'Enquiry.created'),
        ));
        $cnt_tleads_created_active = count($tleads_created);


        $this->loadModel('User');
        $tleadsllogin_created = $this->User->find('list', array(
            'conditions' => array('DATE(User.last_login)' => date('Y-m-d')),
            'fields' => array('User.id', 'User.last_login'),
        ));
        $llogin = count($tleadsllogin_created);

        $tleadslf_created = $this->User->find('list', array(
            'conditions' => array('DATE(User.last_login)' => date('Y-m-d')),
            'fields' => array('User.id', 'User.first_name'),
        ));
        $tleadsll_created = $this->User->find('list', array(
            'conditions' => array('DATE(User.last_login)' => date('Y-m-d')),
            'fields' => array('User.id', 'User.last_name'),
        ));
       
      
        //$mail_to = $this->Setting->find_setting(1);
        //if (!empty($mail_to['Setting']['mail_to'])) {
            $email = new CakeEmail();
            $email->config('cron_jobdetails');
            $email->to("sales@valuemarketresearch.com");
            $email->viewVars(compact('cnt_ready_to_active','cnt_producttotal_modified_active','cnt_tleads_created_active','llogin','tleadsllogin_created','tleadslf_created','tleadsll_created'));
             $email->subject(date('d M y') . ' Daily Progress');
            $email->send();
            // echo "True";
        //}
        // $this->set(compact('ready_to_active', 'cnt_ready_to_active','active_today', 'cnt_active_today'));
    }
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(array('cron_details'));
    }
}