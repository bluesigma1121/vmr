<?php

App::uses('AppController', 'Controller');

/**
 * Candidates Controller
 *
 * @property Candidate $Candidate
 * @property PaginatorComponent $Paginator
 */
class CandidatesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Candidate->recursive = 0;
        $this->set('candidates', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Candidate->exists($id)) {
            throw new NotFoundException(__('Invalid candidate'));
        }
        $options = array('conditions' => array('Candidate.' . $this->Candidate->primaryKey => $id));
        $this->set('candidate', $this->Candidate->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Candidate->create();
            if ($this->Candidate->save($this->request->data)) {
                $this->Session->setFlash(__('The candidate has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The candidate could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Candidate->exists($id)) {
            throw new NotFoundException(__('Invalid candidate'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Candidate->save($this->request->data)) {
                $this->Session->setFlash(__('The candidate has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The candidate could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Candidate.' . $this->Candidate->primaryKey => $id));
            $this->request->data = $this->Candidate->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Candidate->id = $id;
        if (!$this->Candidate->exists()) {
            throw new NotFoundException(__('Invalid candidate'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Candidate->delete()) {
            $this->Session->setFlash(__('The candidate has been deleted.'));
        } else {
            $this->Session->setFlash(__('The candidate could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function apply_online() {
        Configure::load('idata');
        $positions = Configure::read('idata.position_type');
        $this->set(compact('positions'));
        if ($this->request->is('post')) {
            $this->Candidate->create();
            $data = $this->request->data;
            
            $resume = $data['Candidate']['resume'];
            $resume_name = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 5)), 0, 7);
            $upload_path = APP.WEBROOT_DIR.'/files/resumes';

            if ($resume['error'] == UPLOAD_ERR_OK) {
                $ext = pathinfo($resume["name"], PATHINFO_EXTENSION);
                if (strtolower($ext) == 'docx' || strtolower($ext) == 'doc' || strtolower($ext) == 'pdf') {
                    if (move_uploaded_file($resume['tmp_name'], $upload_path . DS . $resume_name . "." . $ext)) {
                        $alias_name = $resume_name . "." . $ext;
                    }
                }
            }
            $data['Candidate']['resume'] = $alias_name;
            debug($data);
            debug(WEBROOT_DIR.'/files/resumes');
            //die;
            
            if ($this->Candidate->save($data)) {
                $this->Session->setFlash(__('The candidate has been saved.'));
                debug($this->Candidate->validationErrors);
                die;
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The candidate could not be saved. Please, try again.'));
                debug($this->Candidate->validationErrors);
            }
        }
    }

    public function beforeFilter() {
        $this->Auth->allow(array('apply_online'));
    }

}
