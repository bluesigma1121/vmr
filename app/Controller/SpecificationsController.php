<?php

App::uses('AppController', 'Controller');

/**
 * Specifications Controller
 *
 * @property Specification $Specification
 * @property PaginatorComponent $Paginator
 */
class SpecificationsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->layout = 'admin_layout';
        $this->Specification->recursive = 0;
        $this->set('specifications', $this->Paginator->paginate());
        Configure::load('idata');
        $yesno = Configure::read('idata.yes_no');
        $this->set(compact('yesno'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->Specification->exists($id)) {
            throw new NotFoundException(__('Invalid specification'));
        }
        $options = array('conditions' => array('Specification.' . $this->Specification->primaryKey => $id));
        $this->set('specification', $this->Specification->find('first', $options));
        Configure::load('idata');
        $yesno = Configure::read('idata.yes_no');
        $this->set(compact('yesno'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $this->Specification->create();
            if ($this->Specification->save($this->request->data)) {
                $this->Session->setFlash(__('The specification has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The specification could not be saved. Please, try again.'));
            }
        }
        Configure::load('idata');
        $yesno = Configure::read('idata.yes_no');
        $this->set(compact('yesno'));
    }

    public function add_product_spc($id = null) {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $this->request->data['Specification']['prod_id'] = $id;
            $res = $this->Specification->prod_specification($this->request->data);
            if ($res == 1) {
                $this->Session->setFlash(__('The specification  has been saved.'), 'success');
                return $this->redirect(array('action' => 'add_product_spc', $id));
            } else {
                $this->Session->setFlash(__('The specification could not be saved. Please, try again.'), 'error');
                return $this->redirect(array('action' => 'add_product_spc', $id));
            }
        }
        $this->loadModel('ProductSpecification');
        $this->loadModel('Product');
        $sele_spe = $this->ProductSpecification->find('all', array('conditions' => array('ProductSpecification.product_id' => $id)));
        $final_sel_array = array();
        foreach ($sele_spe as $spkey => $speci) {
            $s_id = $speci['Specification']['id'];
            $final_sel_array[$s_id]['Specification'] = $speci['Specification'];
            $final_sel_array[$s_id]['SpecificationOption'][$spkey] = $speci;
        }
        $res = $this->Product->avl_country($id);
        $pro_id = $id;
        $all_spec = $this->Specification->find('list', array('fields' => array('Specification.id', 'Specification.specification_name')));
        $spres = $this->Specification->check_speci($id);
        $prod_dt = $this->Product->find('first', array('conditions' => array('Product.id' => $id), 'recursive' => 0, 'fields' => array('Product.id', 'Product.product_name', 'Product.is_active', 'Category.category_name','Product.product_faq')));
        $this->set(compact('all_spec', 'final_sel_array', 'prod_dt', 'pro_id', 'spres', 'res'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->Specification->exists($id)) {
            throw new NotFoundException(__('Invalid specification'));
        }
        if ($this->request->is(array('post', 'put'))) {

            if ($this->Specification->save($this->request->data)) {
                $this->Session->setFlash(__('The specification has been saved.'), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The specification could not be saved. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('Specification.' . $this->Specification->primaryKey => $id));
            $this->request->data = $this->Specification->find('first', $options);
        }
        Configure::load('idata');
        $yesno = Configure::read('idata.yes_no');
        $this->set(compact('yesno'));
    }

    public function edit_using_modal() {
        $this->layout = 'admin_layout';
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Specification->save($this->request->data)) {
                $this->Session->setFlash(__('The specification Name has been saved.'), 'success');
                return $this->redirect(array('controller' => 'specification_options', 'action' => 'view_options', $this->request->data['Specification']['id']));
            } else {
                $this->Session->setFlash(__('The specification Name could not be saved. Please, try again.'), 'error');
                return $this->redirect(array('controller' => 'specification_options', 'action' => 'view_options', $this->request->data['Specification']['id']));
            }
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Specification->id = $id;
        if (!$this->Specification->exists()) {
            throw new NotFoundException(__('Invalid specification'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Specification->delete()) {
            $this->Session->setFlash(__('The specification has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The specification could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function dropdwn_fill() {
        $this->layout = null;
        if ($this->request->is('post')) {
            $this->loadModel('SpecificationOption');
            $this->loadModel('ProductSpecification');
            $already_sel_options = $this->ProductSpecification->find('list', array('conditions' => array('ProductSpecification.product_id' => $this->request->data['p_id'], 'ProductSpecification.specification_id' => $this->request->data['spid']), 'fields' => array('ProductSpecification.specification_option_id', 'ProductSpecification.specification_option_id')));
            $result = $this->SpecificationOption->find('list', array('conditions' => array('SpecificationOption.specification_id' => $this->request->data['spid'], 'SpecificationOption.id NOT' => $already_sel_options), 'fields' => array('SpecificationOption.id', 'SpecificationOption.option_name'), 'recursive' => -1));
            $this->set(compact('result'));
            $this->render('ajax_result');
        }
    }

    public function beforeFilter() {
        $this->Auth->allow(array('dropdwn_fill', 'ajax_result'));
    }

}
