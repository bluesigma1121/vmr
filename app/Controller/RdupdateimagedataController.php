<?php

App::uses('AppController', 'Controller');
/**
 * Rdupdateimagedata Controller
 *
 * @property Rdupdateimagedata $Rdupdateimagedata
 * @property PaginatorComponent $Paginator
 */
class RdupdateimagedataController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    /**
     * index method
     *
     * @return void
     */
    
    public function index() {
        $this->layout = 'admin_layoutfooter';
       $this->loadModel('Rdupdateimagedata'); 
    //     $countries = $this->Rdupdateimagedata->find('all');
    //     $this->set('countries', $countries); 
        
        
        if (empty($_POST['searchrdimage'])) {
         //  $this->loadModel('Rdupdateimagedatas'); 
      
           // $this->set('tarcounselect', $this->paginate()); 
 
           $this->paginate = array(
           // 'conditions' => array('Rdupdateimagedata.id >=' => $ssid['id']),
            'fields' => array('id','product_description','product_no','slug'),
            'limit' => 11,
            'order'=>'id ASC'
        );
        $tarcounselect = $this->Paginator->paginate();
        $this->set('tarcounselect', $tarcounselect);
       
            // $this->paginate = $this->Rdupdateimagedatas->find('all',
            //     array('fields' => array('id','product_description','product_no','slug'),
            //     'limit' => 2,
            //     'order'=>'id ASC')
            // );
           
           // echo "<pre>";
            //print_r($this->paginate);
             // exit;
           // $tarcounselect = $this->Paginator->paginate($this->paginate);
           
           
           //  $this->set('tarcounselect', $this->paginate()); 


        } 
        else {
           // $countryselect = $_POST['countryselected'];
            $countryselect = $_POST['searchrdimage'];

            $sqlselect=$this->Rdupdateimagedata->query("SELECT id FROM products WHERE slug like '%$countryselect%'");
        
            $sid=$sqlselect[0];
            foreach($sid as $ssid)
            {
              //  print_r($ssid['id']);
            }
           

            // $tarcounselect = $this->Rdupdateimagedata->find('all',  array('conditions' => array('Rdupdateimagedata.id >=' => $ssid['id']),
            //                                                         'fields'=>array('id','product_description','product_no','slug'),
            //                                                         'limit'=>2
            //                                                       ));

            //         $this->set('tarcounselect', $tarcounselect); 
                    $this->paginate = array(
                        'conditions' => array('Rdupdateimagedata.id >=' => $ssid['id']),
                        'fields' => array('id','product_description','product_no','slug'),
                        'limit' => 11,
                        'order'=>'id ASC'
                    );
                    $tarcounselect = $this->Paginator->paginate();
                    $this->set('tarcounselect', $tarcounselect); 
                   
        }
    }
    public function add()
    {
        $this->layout = 'admin_layout';
         for($i=0;$i<count($_POST['productdescription']);$i++)
               // for($i=0;$i<1;$i++)
				{
                       $productdescription = $_POST['productdescription'][$i];
                       $idss =$_POST['ids'][$i];

                      
                        //---------image url start------
						$imageurlold1 = $_POST['image_urlold1'][$i];
                        $imageurl1 = $_POST['image_url1'][$i];

                        $imageurlold2 = $_POST['image_urlold2'][$i];
                        $imageurl2 = $_POST['image_url2'][$i];

                        $imageurlold3 = $_POST['image_urlold3'][$i];
                        $imageurl3 = $_POST['image_url3'][$i];

                        //---------image url end------
                        
                        //---------image title start------
						
                       /* $imagetitleold2 = $_POST['image_titleold2'][$i];
                        $imagetitle2 = $_POST['image_title2'][$i];

                        $imagetitleold3 = $_POST['image_titleold3'][$i];
                        $imagetitle3 = $_POST['image_title3'][$i];

                        $imagetitleold4 = $_POST['image_titleold4'][$i];
                        $imagetitle4 = $_POST['image_title4'][$i];

                        $imagetitleold5 = $_POST['image_titleold5'][$i];
                        $imagetitle5 = $_POST['image_title5'][$i];

                        $imagetitleold6 = $_POST['image_titleold6'][$i];
                        $imagetitle6 = $_POST['image_title6'][$i];

                        $imagetitleold7 = $_POST['image_titleold7'][$i];
                        $imagetitle7 = $_POST['image_title7'][$i];

                        $imagetitleold8 = $_POST['image_titleold8'][$i];
                        $imagetitle8 = $_POST['image_title8'][$i];*/
                        
                        //---------image title end------
                        //---------image alt start------
                        $imagealtold1 = '"'.$_POST['image_altold1'][$i].'"';
                        $imagealt1 = '"'.$_POST['image_alt1'][$i].'"';

                        $imagealtold2 = '"'.$_POST['image_altold2'][$i].'"';
                        $imagealt2 = '"'.$_POST['image_alt2'][$i].'"';

                        $imagealtold3 = '"'.$_POST['image_altold3'][$i].'"';
                        $imagealt3 = '"'.$_POST['image_alt3'][$i].'"';
                        //---------image alt end------
                        // $healthy = [$imageurlold, $imagealtold, $imagetitleold];
                        // $yummy   = [$imageurl, $imagealt, $imagetitle];
                        // $productd = str_replace($healthy, $yummy, $productdescription);

                        $this->loadModel('Rdupdateimagedata');
                        // $sql = "UPDATE products
                        // SET product_description='".$productd."'
                        // WHERE id='".$idss."'";
                       
                        //------start img url replace-----
                        $sqlurl1 = "UPDATE products 
                        SET 
                        product_description = REPLACE(product_description, '".$imageurlold1."', '".$imageurl1."') 
                        WHERE id = '".$idss."'";
                        $queryurl1 = $this->Rdupdateimagedata->query($sqlurl1);

                        $sqlurl2 = "UPDATE products 
                        SET 
                        product_description = REPLACE(product_description, '".$imageurlold2."', '".$imageurl2."') 
                        WHERE id = '".$idss."'";
                        $queryurl2 = $this->Rdupdateimagedata->query($sqlurl2);

                        $sqlurl3 = "UPDATE products 
                        SET 
                        product_description = REPLACE(product_description, '".$imageurlold3."', '".$imageurl3."') 
                        WHERE id = '".$idss."'";
                        $queryurl3 = $this->Rdupdateimagedata->query($sqlurl3);



                        //------end img url replace-----
                         //------start img alt replace-----
                         $sqlalt = "UPDATE products 
                         SET 
                         product_description = REPLACE(product_description, '".$imagealtold1."', '".$imagealt1."') 
                         WHERE id = '".$idss."'";
                         $queryalt1 = $this->Rdupdateimagedata->query($sqlalt);

                         $sqlalt2 = "UPDATE products 
                         SET 
                         product_description = REPLACE(product_description, '".$imagealtold2."', '".$imagealt2."') 
                         WHERE id = '".$idss."'";
                         $queryalt2 = $this->Rdupdateimagedata->query($sqlalt2);

                         $sqlalt3 = "UPDATE products 
                         SET 
                         product_description = REPLACE(product_description, '".$imagealtold3."', '".$imagealt3."') 
                         WHERE id = '".$idss."'";
                         $queryalt3 = $this->Rdupdateimagedata->query($sqlalt3);
                          //------end img alt replace-----

                          //------start img title headline replace-----
                            // if(isset($_POST['image_titleold'][$i])=="" && isset($_POST['image_title'][$i])=="")
                            if(empty($_POST['image_titleold'][$i]) && empty($_POST['image_title'][$i]))
                            {
                                
                            }
                            else{

                          $imagetitleold1 = $_POST['image_titleold'][$i];
                          $imagetitle1 = $_POST['image_title'][$i];
  

                         $sqltitle1 = "UPDATE products 
                         SET 
                         product_description = REPLACE(product_description, '".$imagetitleold1."', '".$imagetitle1."') 
                         WHERE id = '".$_POST['showtextid'][$i]."'";
                         $querytitle1 = $this->Rdupdateimagedata->query($sqltitle1);
                        }
                        /* $sqltitle2 = "UPDATE products 
                         SET 
                         product_description = REPLACE(product_description, '".$imagetitleold2."', '".$imagetitle2."') 
                         WHERE id = '".$idss."'";
                         $querytitle2 = $this->Rdupdateimagedatas->query($sqltitle2);

                         $sqltitle3 = "UPDATE products 
                         SET 
                         product_description = REPLACE(product_description, '".$imagetitleold3."', '".$imagetitle3."') 
                         WHERE id = '".$idss."'";
                         $querytitle3 = $this->Rdupdateimagedatas->query($sqltitle3);

                         $sqltitle4 = "UPDATE products 
                         SET 
                         product_description = REPLACE(product_description, '".$imagetitleold4."', '".$imagetitle4."') 
                         WHERE id = '".$idss."'";
                         $querytitle4 = $this->Rdupdateimagedatas->query($sqltitle4);

                         $sqltitle5 = "UPDATE products 
                         SET 
                         product_description = REPLACE(product_description, '".$imagetitleold5."', '".$imagetitle5."') 
                         WHERE id = '".$idss."'";
                         $querytitle5 = $this->Rdupdateimagedatas->query($sqltitle5);

                         $sqltitle6 = "UPDATE products 
                         SET 
                         product_description = REPLACE(product_description, '".$imagetitleold6."', '".$imagetitle6."') 
                         WHERE id = '".$idss."'";
                         $querytitle6 = $this->Rdupdateimagedatas->query($sqltitle6);

                        
                        //  $sqltitle6 = 'UPDATE products 
                        //  SET 
                        //  product_description = REPLACE(product_description, "'.$imagetitleold6.'", "'.$imagetitle6.'") 
                        //  WHERE id = "'.$idss.'"';
                        //  $querytitle6 = $this->Rdupdateimagedatas->query($sqltitle6);

                        //  $sqltitle6 = "UPDATE products 
                        //  SET 
                        //  product_description = REPLACE(product_description, '$imagetitleold6', '$imagetitle6') 
                        //  WHERE id = '$idss'";
                        //  $querytitle6 = $this->Rdupdateimagedatas->query($sqltitle6);



                         $sqltitle7 = "UPDATE products 
                         SET 
                         product_description = REPLACE(product_description, '".$imagetitleold7."', '".$imagetitle7."') 
                         WHERE id = '".$idss."'";
                         $querytitle7 = $this->Rdupdateimagedatas->query($sqltitle7);

                         $sqltitle8 = "UPDATE products 
                         SET 
                         product_description = REPLACE(product_description, '$imagetitleold8', '$imagetitle8') 
                         WHERE id = '$idss'";
                         $querytitle8 = $this->Rdupdateimagedatas->query($sqltitle8);*/

                        }

                          //------end img title headline replace-----
                   
                    $this->Session->setFlash(__('The Image Details has been Updated.'), 'success');
                   //$this->redirect(array('action' => 'index')); 
                   $this->redirect($this->referer());
               
                   // if ($query) {  
                        //    $this->Session->setFlash(__('The Image Details could not be updated. Please, try again.'), 'error');
                        //  } else { 
                        //     $this->Session->setFlash(__('The Image Details has been Updated.'), 'success');
                        //     $this->redirect(array('action' => 'index')); 
                        //  } 
                        //  if ($query1) {  
                        //        $this->Session->setFlash(__('The Image Details could not be updated. Please, try again.'), 'error');
                        //      } else {  
                        //         $this->Session->setFlash(__('The Image Details has been Updated.'), 'success');
                        //         $this->redirect(array('action' => 'index')); 
                        //      } 
                        //      if ($query2) { 
                        //            $this->Session->setFlash(__('The Image Details could not be updated. Please, try again.'), 'error');
                        //          } else {  
                        //             $this->Session->setFlash(__('The Image Details has been Updated.'), 'success');
                        //             $this->redirect(array('action' => 'index')); 
                        //          } 
                       
    }
}