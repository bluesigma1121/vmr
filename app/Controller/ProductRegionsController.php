<?php
App::uses('AppController', 'Controller');
/**
 * ProductRegions Controller
 *
 * @property ProductRegion $ProductRegion
 * @property PaginatorComponent $Paginator
 */
class ProductRegionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProductRegion->recursive = 0;
		$this->set('productRegions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProductRegion->exists($id)) {
			throw new NotFoundException(__('Invalid product region'));
		}
		$options = array('conditions' => array('ProductRegion.' . $this->ProductRegion->primaryKey => $id));
		$this->set('productRegion', $this->ProductRegion->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProductRegion->create();
			if ($this->ProductRegion->save($this->request->data)) {
				$this->Session->setFlash(__('The product region has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product region could not be saved. Please, try again.'));
			}
		}
		$products = $this->ProductRegion->Product->find('list');
		$countries = $this->ProductRegion->Country->find('list');
		$regions = $this->ProductRegion->Region->find('list');
		$this->set(compact('products', 'countries', 'regions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProductRegion->exists($id)) {
			throw new NotFoundException(__('Invalid product region'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProductRegion->save($this->request->data)) {
				$this->Session->setFlash(__('The product region has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product region could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProductRegion.' . $this->ProductRegion->primaryKey => $id));
			$this->request->data = $this->ProductRegion->find('first', $options);
		}
		$products = $this->ProductRegion->Product->find('list');
		$countries = $this->ProductRegion->Country->find('list');
		$regions = $this->ProductRegion->Region->find('list');
		$this->set(compact('products', 'countries', 'regions'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProductRegion->id = $id;
		if (!$this->ProductRegion->exists()) {
			throw new NotFoundException(__('Invalid product region'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ProductRegion->delete()) {
			$this->Session->setFlash(__('The product region has been deleted.'));
		} else {
			$this->Session->setFlash(__('The product region could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
