<?php

App::uses('AppController', 'Controller');
App::uses('Paypal', 'Paypal.Lib');
App::uses('CakeEmail', 'Network/Email');
//App::uses('tcpdf/xtcpdf', 'Vendor');
//require_once('tcpdf_include.php');
App::uses('xtcpdf', 'Vendor');
App::uses('Payu', 'Vendor');

/**
 * Orders Controller
 *
 * @property Order $Order
 * @property PaginatorComponent $Paginator
 */
class OrdersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($data['Order']['search_text'])) {
                $this->Session->setFlash(__('Please Fill Search Box.'), 'error');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->write('user_filter', $data['Order']);
        }

        if ($this->Session->check('user_filter')) {
            $user_filt = $this->Session->read('user_filter');
        }
        $conditions = array();
        if (isset($user_filt['search_by'])) {
            if ($user_filt['search_by'] == 'name') {
                $conditions["CONCAT(User.first_name,' ',User.last_name) LIKE"] = '%' . $user_filt['search_text'] . '%';
                // $conditions2["User.last_name LIKE"] = '%' . $enquiry_filt['search_text'] . '%';
            }
        }
        if (isset($user_filt['search_by'])) {
            if ($user_filt['search_by'] == 'token') {
                $conditions["Order." . $user_filt['search_by'] . " LIKE"] = '%' . $user_filt['search_text'] . '%';
            }
        }
        if (isset($user_filt['search_by'])) {
            if ($user_filt['search_by'] == 'payer_code') {
                $conditions["Order." . $user_filt['search_by'] . " LIKE"] = '%' . $user_filt['search_text'] . '%';
                // $conditions2["User.last_name LIKE"] = '%' . $enquiry_filt['search_text'] . '%';
            }
        }

        $this->Session->delete('user_filter');
        $this->paginate = array(
            'conditions' => array($conditions),
            'order' => 'Order.id DESC',
            'limit' => 50
        );
        $this->Order->recursive = 0;
        $this->set('orders', $this->Paginator->paginate());
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $order_search = Configure::read('idata.order_search');
        $transaction_status = Configure::read('idata.transaction_status');
        $payment_status = Configure::read('idata.payment_status');
        $order_ststus = Configure::read('idata.order_ststus');
        $this->set(compact('yes_no', 'order_search', 'order_ststus', 'payment_status', 'transaction_status'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->Order->exists($id)) {
            throw new NotFoundException(__('Invalid order'));
        }

        if ($this->request->is(array('post', 'put'))) {


            $prev_data = $this->Order->find('first', array('conditions' => array('Order.id' => $id), 'fields' => array('Order.remark'), 'recursive' => 1));
            $data['Order']['id'] = $id;
            $cnt = 1;
            $temp_array = array();
            if (!empty($prev_data['Order']['remark'])) {
                $temp_array = json_decode($prev_data['Order']['remark'], true);
                $cnt = count($temp_array);
                $cnt++;
                $date = Date('d-m-Y H:i:s');
                $new_array['date'] = $date;
                $new_array['message'] = $this->request->data['Order']['new_remark'];
                $new_array['transaction_status'] = $this->request->data['Order']['transaction_status'];
                array_push($temp_array, $new_array);
                $data['Order']['remark'] = json_encode($temp_array);
            } else {
                $date = Date('d-m-Y H:i:s');
                $new_array['date'] = $date;
                $new_array['message'] = $this->request->data['Order']['new_remark'];
                $new_array['transaction_status'] = $this->request->data['Order']['transaction_status'];
                array_push($temp_array, $new_array);
                $data['Order']['remark'] = json_encode($temp_array);
                //  debug(Date('D-M-Y h:i a', strtotime($date)));
            }
            $data['Order']['transaction_status'] = $this->request->data['Order']['transaction_status'];
            if ($this->Order->save($data)) {
                $this->Session->setFlash(__('The Status and remark has been saved.'), 'success');
                return $this->redirect(array('action' => 'view', $id));
            } else {
                $this->Session->setFlash(__('The Order Status could not be saved. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('Order.' . $this->Order->primaryKey => $id));
            $this->request->data = $this->Order->find('first', $options);
            $prev_rem = array();
            $prev_rem = json_decode($this->request->data['Order']['remark'], true);
        }

        $options = array('conditions' => array('Order.' . $this->Order->primaryKey => $id));

        $this->set('orders', $this->Order->find('first', $options));
        $this->loadModel('OrderProduct');
        $products = $this->OrderProduct->find('all', array('conditions' => array('OrderProduct.order_id' => $id)));
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $order_search = Configure::read('idata.order_search');
        $transaction_status = Configure::read('idata.transaction_status');
        $payment_status = Configure::read('idata.payment_status');
        $order_ststus = Configure::read('idata.order_ststus');
        $this->set(compact('products', 'order_ststus', 'payment_status', 'prev_rem', 'transaction_status'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Order->create();
            if ($this->Order->save($this->request->data)) {
                $this->Session->setFlash(__('The order has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The order could not be saved. Please, try again.'));
            }
        }
        $users = $this->Order->User->find('list');
        $this->set(compact('users'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Order->exists($id)) {
            throw new NotFoundException(__('Invalid order'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Order->save($this->request->data)) {
                $this->Session->setFlash(__('The order has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The order could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Order.' . $this->Order->primaryKey => $id));
            $this->request->data = $this->Order->find('first', $options);
        }
        $users = $this->Order->User->find('list');
        $this->set(compact('users'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Order->id = $id;
        if (!$this->Order->exists()) {
            throw new NotFoundException(__('Invalid order'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Order->delete()) {
            $this->Session->setFlash(__('The order has been deleted.'));
        } else {
            $this->Session->setFlash(__('The order could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function del_order($id = null) {
        $this->loadModel('OrderProduct');
        $this->OrderProduct->delete(array('OrderProduct.order_id' => $id));
        if ($this->Order->delete(array('Order.id' => $id))) {
            echo 'Order deleted';
        } else {
            echo 'Error in order deletation';
        }
        die;
    }

    public function user_order_index() {
        $user = $this->Auth->user();
        $id = $user['id'];
        if (empty($id)) {
            $this->Session->write('user_order', 'user_order_index');
            $this->Session->setFlash(__('Please login.'), 'success');
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        //$this->Order->recursive = 0;
        $this->paginate = array(
            'conditions' => array('Order.user_id' => AuthComponent::user('id')),
            'order' => 'Order.created DESC',
            'limit' => 50
        );
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $txn_status = Configure::read('idata.transaction_status_hdfc');
        //$yes_no = Configure::read('idata.yes_no');
        $user_orders = $this->Paginator->paginate();
        //debug($user_orders);
        $this->loadModel('Transaction');
        $transactions = $this->Transaction->find('all', array('conditions' => array('user_id' => $this->Auth->user('id')),
            'fields' => array('Transaction.id', 'txnid', 'Product.product_name', 'User.id', 'Transaction.amount', 'Transaction.status', 'Transaction.created'),
        ));
        $this->set(compact('user_orders', 'yes_no', 'transactions', 'txn_status'));
    }

    public function user_order_view($id = null) {
        $this->loadModel('OrderProduct');
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $licence_type = Configure::read('idata.licence_type');
        $orders = $this->Order->find('first', array('conditions' => array('Order.id' => $id)));
        $products = $this->OrderProduct->find('all', array('conditions' => array('OrderProduct.order_id' => $id)));
        $this->set(compact('yes_no', 'orders', 'products', 'licence_type'));
    }

//    public function download_pdf() {
//        $id = $this->Session->read('download_id');
//        if (empty($id)) {
//            $this->redirect('/');
//        }
//        $member = $this->Member->read(null, $id);
//        if (empty($member['ApplicationForm']['id'])) {
//            $this->redirect('/');
//        }
//        $this->viewClass = 'Media';
//        $params = array(
//            'id' => $member['ApplicationForm']['id'] . '.pdf',
//            'name' => 'Application Form ' . str_pad($member['ApplicationForm']['id'], 6, '0', STR_PAD_LEFT),
//            'download' => true,
//            'extension' => 'pdf',
//            'path' => 'webroot/pdf/',
//        );
//        $this->set($params);
//    }

    function generate_pdf($order_id) {
        $this->layout = null;
        $this->loadModel('OrderProduct');
        if (empty($order_id)) {
            $this->Session->setFlash('Sorry, there was no PDF selected.');
            $this->redirect(array('action' => 'add'));
        }
        if (isset($order_id)) {
            $order_details = $this->Order->find('first', array('conditions' => array('Order.id' => $order_id)));
        }
        $order_product = $this->OrderProduct->find('all', array('conditions' => array('OrderProduct.order_id' => $order_id)));

        $this->set(compact('order_id', 'order_details', 'order_product'));
        $this->layout = null;
        $this->render('view_pdf');
    }

    /* function view_pdf_spouse($id = null) {
      if (!$id) {
      $this->Session->setFlash('Sorry, there was no PDF selected.');
      $this->redirect(array('action'=>'add'));
      }
      $member = $this->Member->read(null, $id);
      if(isset($member['Spouse']))
      $spouse = $this->Member->read(null, $member['Spouse']['id']);
      //debug($spouse);

      $this->set(compact('member','spouse'));
      $codes = Configure::read('kpa');
      $this->set($codes);

      $this->layout = 'pdf'; //this will use the pdf.ctp layout
      $this->render();
      } */

    public function secrate_payment($prod_id = null, $sec_code = null) {
        $this->loadModel('Setting');
        $setting_data = $this->Setting->find_setting(1);

        $id = AuthComponent::user('id');
        //debug($prod_id);die;

        if (!empty($id) && $setting_data['Setting']['secret_payment'] == $sec_code) {
            $this->loadModel('Product');
            $prod = $this->Product->find('first', array('conditions' => array('Product.id' => $prod_id), 'recursive' => -1));
            if (!empty($prod)) {
                $this->set(compact('prod'));
            } else {
                $this->Session->setFlash(__('Invalid Product ID.'), 'error');
                return $this->redirect('/');
            }
        } else {
            $this->Session->setFlash(__('Please login .'), 'error');
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
    }

    /* This Function supports paypal
     * public function process_order() {
     *
      $this->Paypal = new Paypal(array(
      'sandboxMode' => false,
      'nvpUsername' => 'admin_api1.decisiondatabases.com',
      'nvpPassword' => '95NQFYL48UGNYUJV',
      'nvpSignature' => 'Ajwath3vWTxZhV-dji.0VNOXCfYMAPmQ-pzAeAM72MLSRQaFx4.3-jHm'
      ));

      //$this->Paypal = new Paypal(array(
      //  'sandboxMode' => true,
      //  'nvpUsername' => 'mytest.b_api1.gmail.com',
      //  'nvpPassword' => 'QA72NS8GJMVKK5FB',
      //  'nvpSignature' => 'AWCJppxiinMrB040MY3Rvn2AXOqXAnrt-5SDVjcgdy9UqAR22ivEXV.O'
      //  ));

      $base_url = Router::url('/', true);
      $cancel_url = $base_url . 'cart_items/add_to_cart';
      $return_url = $base_url . 'orders/my_orders';

      if (!$this->Session->check('cart_item')) {
      $this->redirect($this->referer());
      }
      $cart_item = $this->Session->read('cart_item');
      $this->loadModel('Product');
      $product = $this->Product->find('first', array(
      'conditions' => array('Product.id' => $cart_item['CartItem']['product_id']),
      'fields' => array('Product.product_name', 'Product.id', 'Product.price', 'Product.corporate_price'),
      'recursive' => -1
      ));

      Configure::load('idata');
      $licence_type = Configure::read('idata.licence_type');
      $this->set(compact('product', 'cart_item', 'licence_type'));
      if ($this->request->is('post')) {
      $data = $this->request->data;
      $data['User']['product_name'] = $product['Product']['product_name'];
      $data['User']['licence'] = $cart_item['CartItem']['licence_type'];

      if ($cart_item['CartItem']['licence_type'] == 1) {
      $data['User']['price'] = $product['Product']['price'];
      } elseif ($cart_item['CartItem']['licence_type'] == 2) {
      $data['User']['price'] = $product['Product']['corporate_price'];
      }

      $user = $this->Order->User->find('first', array(
      'conditions' => array('User.email' => $data['User']['email']),
      'recursive' => -1
      ));

      if (empty($user)) {
      $data['User']['password'] = substr(str_shuffle("23456789abcdefghjkmnopqrstuvwxyz"), 0, 6);
      $data['User']['role'] = 11;
      $pwd = $data['User']['password'];

      $this->Order->User->create();
      if ($this->Order->User->save($data)) {
      $email = new CakeEmail();
      $email->config('user_registration');
      $email->to($this->request->data['User']['email']);
      $email->viewVars(compact('data', 'pwd'));
      $email->subject('Online Registration on Value Market Research');
      $email->send();

      $data['User']['id'] = $this->Order->User->id;
      $this->Auth->login($data['User']);
      }
      } else {
      $this->Auth->login($user['User']);
      }

      $this->Session->write('user_bill_detail', $data);
      $order['description'] = 'Your purchase with Value Market Research';
      $order['currency'] = 'USD';
      $order['return'] = $return_url;
      $order['cancel'] = $cancel_url;
      $order['custom'] = 'test';
      $order['shipping'] = 0.00;
      $order['items'][0]['name'] = $data['User']['product_name'];
      $order['items'][0]['description'] = $data['User']['product_name'];
      $order['items'][0]['tax'] = 0.00;
      $order['items'][0]['subtotal'] = round($data['User']['price'], 2);
      $order['items'][0]['qty'] = 1;
      $order['items'][0]['product_id'] = $cart_item['CartItem']['product_id'];
      $order['items'][0]['licence_type'] = $cart_item['CartItem']['licence_type'];

      $this->Session->write('order_session', $order);
      $res = $this->Paypal->setExpressCheckout($order);
      //debug($order);
      //debug($res);
      //die;
      $this->redirect($res);
      }
      } */


    /*
      Merchant Name	BLUESIGMA BUSINESS CONSULTING SOLUTIONS LLP
      Key	UCcgQy
      Salt	QAX6IZ0Q
      Merchant Domain	www.DECISIONDATABASES.com
      SETUP TYPE	HOSTED

      Merchant Console URL	test.payu.in/front/hdfc
      Username	super
      Alias	UCcgQy
      Password	Password123


      [key] => 71tFEF
      [txnid] => animesh_566abf38ac14b
      [amount] => 22
      [firstname] => animesh
      [email] => animesh.kundu@payu.in
      [phone] => 1234567890
      [productinfo] => This is shit
      [surl] => payment_success
      [furl] => payment_failure
     */

    //This Function supports payu money HDFC gateway
    public function process_order() {
        $this->redirect($this->referer());
        die;
        $base_url = Router::url('/', true);
        $fail_url = $base_url . 'cart_items/add_to_cart';
        $success_url = $base_url . 'users/payment_success';

        if ($this->request->is('post')) {
            if (isset($_POST['field9']) && $_POST['field9'] == "SUCCESS") {

                $transactions['Transaction']['user_id'] = $this->Auth->user('id');
                $cart_item = $this->Session->read('cart_item');
                //debug($cart_item);
                //$transactions['Transaction']['user_id'] = 90;
                $transactions['Transaction']['first_name'] = $_POST['firstname'];
                $transactions['Transaction']['last_name'] = $_POST['lastname'];
                $transactions['Transaction']['txnid'] = $_POST['txnid'];
                $transactions['Transaction']['address'] = $_POST['address1'];
                $transactions['Transaction']['city'] = $_POST['city'];
                $transactions['Transaction']['state'] = $_POST['state'];
                $transactions['Transaction']['country'] = $_POST['country'];
                $transactions['Transaction']['pincode'] = $_POST['zipcode'];
                $transactions['Transaction']['email'] = $_POST['email'];
                $transactions['Transaction']['productinfo'] = $_POST['productinfo'];
                $transactions['Transaction']['product_id'] = $cart_item['CartItem']['product_id'];
                $transactions['Transaction']['organisation'] = $_POST['udf2'];
                $transactions['Transaction']['job_title'] = $_POST['udf3'];
                $transactions['Transaction']['licence_type'] = $_POST['udf1'];
                $transactions['Transaction']['amount'] = $_POST['amount'];

                $this->loadModel('Transaction');

                if ($this->Transaction->save($transactions)) {
                    $email = new CakeEmail();
                    $email->config('order_placed_hdfc');
                    $email->to($transactions['Transaction']['email']);
                    $email->viewVars(compact('transactions'));
                    $email->subject('Order Update Value Market Research');
                    //$email->attachments(array($form_name => $form_path));
                    $email->send();
                }


                $enq_data['Enquiry']['product_id'] = $cart_item['CartItem']['product_id'];
                $enq_data['Enquiry']['user_id'] = $this->Auth->user('id');
                $enq_data['Enquiry']['subject'] = "Transaction Successful";
                $enq_data['Enquiry']['message'] = "REASON FOR TRANSACTION SUCCESSFUL: " . $_POST['field9'] . " PRODUCT NAME: " . $_POST['productinfo'];
                $enq_data['Enquiry']['mobile'] = $_POST['phone'];
                $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                $enq_data['Enquiry']['ref_page'] = "Buy Now Transaction Successful";
                $product_name = $_POST['productinfo'];

                $this->loadModel('Enquiry');
                $this->Enquiry->create();
                if ($this->Enquiry->save($enq_data)) {
                    $enq_id = $this->Enquiry->id;
                    $this->loadModel('Setting');
                    $mail_to = $this->Setting->find_setting(1);

                    if (!empty($mail_to['Setting']['mail_to'])) {
                        $email_admin = new CakeEmail();
                        $email_admin->config('user_enquiery_txn_success');
                        $email_admin->to($mail_to['Setting']['mail_to']);
                        $email_admin->cc('mmdgreat@decisiondatabases.com');
                        $email_admin->viewVars(compact('enq_data', 'data', 'product_name', 'enq_id'));
                        $email_admin->subject('New Lead Recieved : Transaction Success');
                        $email_admin->send();
                    }
                }

                $this->redirect(array('controller' => 'users', 'action' => 'payment_success'));
            } else if (isset($_POST['status']) && $_POST['status'] == 'failure') { // IF Transaction fail
                //debug($_POST);die;
                $cart_item = $this->Session->read('cart_item');

                $user = $this->Order->User->find('first', array(
                    'conditions' => array('User.email' => $_POST['email']),
                    'recursive' => -1
                ));


                $data['User']['first_name'] = $_POST['firstname'];
                $data['User']['last_name'] = $_POST['lastname'];
                $data['User']['email'] = $_POST['email'];
                $data['User']['organisation'] = $_POST['udf2'];
                $data['User']['job_title'] = $_POST['udf3'];

                $data['User']['city'] = $_POST['city'];
                $data['User']['pin_code'] = $_POST['zipcode'];
                $data['User']['state'] = $_POST['state'];
                $data['User']['country'] = $_POST['country'];
                $data['User']['mobile'] = $_POST['phone'];
                $data['User']['address'] = $_POST['address1'];

                $data['User']['password'] = substr(str_shuffle("23456789abcdefghjkmnopqrstuvwxyz"), 0, 6);
                $data['User']['role'] = 11;
                $pwd = $data['User']['password'];

                if (empty($user)) {
                    $this->Order->User->create();
                    if ($this->Order->User->save($data)) {
                        $email = new CakeEmail();
                        $email->config('user_registration');
                        $email->to($_POST['email']);
                        $email->viewVars(compact('data', 'pwd'));
                        $email->subject('Online Registration on Value Market Research');
                        $email->send();

                        $data['User']['id'] = $this->Order->User->id;
                        $this->Auth->login($data['User']);
                    }
                } else {
                    $this->Auth->login($user['User']);
                }


                //Failed mail and capture

                $enq_data['Enquiry']['product_id'] = $cart_item['CartItem']['product_id'];
                $enq_data['Enquiry']['user_id'] = $this->Auth->user('id');
                $enq_data['Enquiry']['subject'] = "Transaction failed";
                $enq_data['Enquiry']['message'] = "REASON FOR TRANSACTION FAIL: " . $_POST['field9'] . "<br/>Product name: " . $_POST['productinfo'];
                $enq_data['Enquiry']['mobile'] = $_POST['phone'];
                $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                $enq_data['Enquiry']['ref_page'] = "Buy Now Transaction Fail";
                $product_name = $_POST['productinfo'];

                $this->loadModel('Enquiry');
                $this->Enquiry->create();
                if ($this->Enquiry->save($enq_data)) {
                    $enq_id = $this->Enquiry->id;
                    $this->loadModel('Setting');
                    $mail_to = $this->Setting->find_setting(1);

                    if (!empty($mail_to['Setting']['mail_to'])) {
                        $email_admin = new CakeEmail();
                        $email_admin->config('user_enquiery_txn_fail');
                        $email_admin->to($mail_to['Setting']['mail_to']);
                        $email_admin->viewVars(compact('enq_data', 'data', 'product_name', 'enq_id'));
                        $email_admin->subject('New Lead Recieved : Transaction failed');
                        $email_admin->send();
                    }
                }
                $this->redirect(array('controller' => 'users', 'action' => 'payment_failed'));
            } else {
                if (!$this->Session->check('cart_item')) {
                    $this->redirect($this->referer());
                }
                $cart_item = $this->Session->read('cart_item');

                $this->loadModel('Product');
                $product = $this->Product->find('first', array(
                    'conditions' => array('Product.id' => $cart_item['CartItem']['product_id']),
                    'fields' => array('Product.product_name', 'Product.id', 'Product.price', 'Product.corporate_price'),
                    'recursive' => -1
                ));
                //debug($product);
                //debug($this->request->data);
                $data = $this->request->data;


                if ($_SERVER['HTTP_HOST'] == "www.decisiondatabases.com") {
                    $data['User']['key'] = 'lFmqOb';
                    $data['User']['salt'] = 'UKG7x5qd';
                } else {
                    $data['User']['key'] = 'UCcgQy';
                    $data['User']['salt'] = 'QAX6IZ0Q';
                }

                $data['User']['txnid'] = substr(str_shuffle('abcefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789'), 5, 11);
                $data['User']['firstname'] = $data['User']['first_name'] . " " . $data['User']['last_name'];
                $data['User']['email'] = $data['User']['email'];
                $data['User']['phone'] = $data['User']['mobile'];
                //$data['User']['surl'] = $success_url;
                //$data['User']['furl'] = $fail_url;

                $data['User']['surl'] = $success_url;
                $data['User']['furl'] = $fail_url;

                $data['User']['productinfo'] = $product['Product']['product_name'];

                if ($cart_item['CartItem']['licence_type'] == 1) {
                    $data['User']['amount'] = $product['Product']['price'];
                } elseif ($cart_item['CartItem']['licence_type'] == 2) {
                    $data['User']['amount'] = $product['Product']['corporate_price'];
                }

                $data_required['key'] = $data['User']['key'];
                $data_required['txnid'] = $data['User']['txnid'];
                $data_required['amount'] = $data['User']['amount'];
                $data_required['firstname'] = $data['User']['first_name'];
                $data_required['lastname'] = $data['User']['last_name'];

                $data_required['email'] = $data['User']['email'];
                $data_required['phone'] = $data['User']['phone'];
                $data_required['productinfo'] = $data['User']['productinfo'];
                $data_required['surl'] = $data['User']['surl'];
                $data_required['furl'] = $data['User']['furl'];

                $data_required['udf1'] = $cart_item['CartItem']['licence_type'];
                $data_required['udf2'] = $data['User']['organisation'];
                $data_required['udf3'] = $data['User']['job_title'];
                $data_required['udf4'] = $product['Product']['id'];

                $data_required['city'] = $data['User']['city'];
                $data_required['state'] = $data['User']['state'];
                $data_required['zipcode'] = $data['User']['pin_code'];
                $data_required['country'] = $data['User']['country'];

                $user = $this->Order->User->find('first', array(
                    'conditions' => array('User.email' => $data_required['email']),
                    'recursive' => -1
                ));

                //START USER ENTRY
                if (empty($user)) {
                    $data['User']['password'] = substr(str_shuffle("23456789abcdefghjkmnopqrstuvwxyz"), 0, 6);
                    $data['User']['role'] = 11;
                    $pwd = $data['User']['password'];

                    $this->Order->User->create();
                    if ($this->Order->User->save($data)) {
                        $email = new CakeEmail();
                        $email->config('user_registration');
                        $email->to($data_required['email']);
                        $email->viewVars(compact('data', 'pwd'));
                        $email->subject('Online Registration on Value Market Research');
                        $email->send();
                        $data['User']['id'] = $this->Order->User->id;
                        $this->Auth->login($data['User']);
                    }
                } else {
                    $this->Auth->login($user['User']);
                }

                //Attempt to email notification
                $enq_data['Enquiry']['product_id'] = $cart_item['CartItem']['product_id'];
                $enq_data['Enquiry']['user_id'] = $this->Auth->user('id');
                $enq_data['Enquiry']['subject'] = "Transaction Attempted";
                $enq_data['Enquiry']['message'] = "This User tried to buy " . $product['Product']['product_name']. " this product.";
                $enq_data['Enquiry']['mobile'] = $data_required['phone'];
                $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                $enq_data['Enquiry']['ref_page'] = "Buy Now Attempt Has Been Done.";
                $product_name = $product['Product']['product_name'];

                $this->loadModel('Enquiry');
                $this->Enquiry->create();
                if ($this->Enquiry->save($enq_data)) {
                    $enq_id = $this->Enquiry->id;
                    $this->loadModel('Setting');
                    $mail_to = $this->Setting->find_setting(1);

                    if (!empty($mail_to['Setting']['mail_to'])) {
                        $email_admin = new CakeEmail();
                        $email_admin->config('user_enquiery_txn_success');
                        $email_admin->to($mail_to['Setting']['mail_to']);
                        $email_admin->viewVars(compact('enq_data', 'data', 'product_name', 'enq_id'));
                        $email_admin->subject('New Lead Recieved : Transaction Attempted');
                        $email_admin->send();
                    }
                }
                //END ENQUIRY DATA
                //debug($data_required);
                $this->Payu = new Payu;
                $this->Payu->pay_page($data_required, $data['User']['salt']);
                die;
            }
        }

        if (!$this->Session->check('cart_item')) {
            $this->redirect($this->referer());
        }
        $cart_item = $this->Session->read('cart_item');
        $this->loadModel('Product');
        $product = $this->Product->find('first', array(
            'conditions' => array('Product.id' => $cart_item['CartItem']['product_id']),
            'fields' => array('Product.product_name', 'Product.id', 'Product.price', 'Product.corporate_price'),
            'recursive' => -1
        ));

        Configure::load('idata');
        $licence_type = Configure::read('idata.licence_type');
        $this->set(compact('product', 'cart_item', 'licence_type'));
    }

    public function my_orders() {
        /* $this->Paypal = new Paypal(array(
          'sandboxMode' => true,
          'nvpUsername' => 'mytest.b_api1.gmail.com',
          'nvpPassword' => 'QA72NS8GJMVKK5FB',
          'nvpSignature' => 'AWCJppxiinMrB040MY3Rvn2AXOqXAnrt-5SDVjcgdy9UqAR22ivEXV.O'
          )); */


        /* $this->Paypal = new Paypal(array(
          'sandboxMode' => false,
          'nvpUsername' => 'admin_api1.decisiondatabases.com',
          'nvpPassword' => '95NQFYL48UGNYUJV',
          'nvpSignature' => 'Ajwath3vWTxZhV-dji.0VNOXCfYMAPmQ-pzAeAM72MLSRQaFx4.3-jHm'
          )); */


        $token = $this->request->query['token'];
        $PayerID = $this->request->query['PayerID'];
        $res = $this->Paypal->getExpressCheckoutDetails($token);
        //debug($res);
        $user = $this->Auth->user();
        $user_id = $user['id'];
        $order = $this->Session->read('order_session');
        $final_res = $this->Paypal->doExpressCheckoutPayment($order, $token, $PayerID);

        $this->loadModel('Category');

        $category = $this->Category->find('all', array(
            'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
        ));
        $order_details = array();
        if ($res['ACK'] == 'Success' && ($final_res['ACK'] == 'Success' || $final_res['ACK'] == 'SuccessWithWarning' )) {
            if ($this->Session->check('user_bill_detail')) {
                $users = $this->Session->read('user_bill_detail');
                if (!empty($users)) {
                    $name = $users['User']['first_name'] . ' ' . $users['User']['last_name'];
                    $order_details['Order']['ship_user_name'] = $name;
                    $order_details['Order']['ship_address'] = $users['User']['address'];
                    $order_details['Order']['city'] = $users['User']['city'];
                    $order_details['Order']['state'] = $users['User']['state'];
                    $order_details['Order']['country'] = $users['User']['country'];
                    $order_details['Order']['ship_email'] = $users['User']['email'];
                    $order_details['Order']['pincode'] = $users['User']['pin_code'];
                    $order_details['Order']['mobile'] = $users['User']['mobile'];
                }
            } else {
                $order_details['Order']['ship_user_name'] = $res['SHIPTONAME'];
                $order_details['Order']['ship_address'] = $res['SHIPTOSTREET'];
                $order_details['Order']['city'] = $res['SHIPTOCITY'];
                $order_details['Order']['state'] = $res['SHIPTOSTATE'];
                $order_details['Order']['country'] = $res['SHIPTOCOUNTRYNAME'];
                $order_details['Order']['ship_email'] = $res['EMAIL'];
            }
            $order_details['Order']['token'] = $res['TOKEN'];
            $order_details['Order']['payer_code'] = $res['PAYERID'];
            $order_details['Order']['transaction_status'] = 1;
            $order_details['Order']['total_cost'] = $res['AMT'];
            $order_details['Order']['user_id'] = $user_id;
            $order_details['Order']['currency'] = $res['CURRENCYCODE'];
            $order_details['Order']['transaction_no'] = $final_res['PAYMENTINFO_0_TRANSACTIONID'];
            //$order_details['Order']['transaction_no'] = '111';

            $this->loadModel('OrderProduct');
            $this->Order->create();
            $res = $this->Order->find('first', array(
                'conditions' => array('Order.token' => $order_details['Order']['token']),
                'fields' => array('Order.token'), 'recursive' => -1));
            if (empty($res)) {
                if ($this->Order->save($order_details)) {
                    $order_id = $this->Order->id;
                    $invoice_num = 'DD' . str_pad($order_id, 10, '0', STR_PAD_LEFT);
                    $pdf_name = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 5)), 0, 7);
                    $this->Order->updateAll(array('Order.invoice_no' => "'$invoice_num'", 'Order.invoice_pdf_name' => "'$pdf_name'"), array('Order.id' => $order_id));
                    $this->loadModel('Product');
                    foreach ($order['items'] as $ckey => $cart_list) {
                        $cart_item = $this->Product->find('first', array(
                            'conditions' => array('Product.id' => $cart_list['product_id']),
                            'recursive' => -1
                        ));
                        $order_pro = array();
                        $order_pro['OrderProduct']['order_id'] = $order_id;
                        $order_pro['OrderProduct']['product_id'] = $cart_item['Product']['id'];
                        $order_pro['OrderProduct']['product_name'] = $cart_item['Product']['product_name'];
                        $order_pro['OrderProduct']['product_no'] = $cart_item['Product']['product_no'];
                        $order_pro['OrderProduct']['licence_type'] = $cart_list['licence_type'];
                        $order_pro['OrderProduct']['category_id'] = $cart_item['Product']['category_id'];
                        $order_pro['OrderProduct']['product_description'] = $cart_item['Product']['product_description'];
                        $order_pro['OrderProduct']['price'] = $cart_list['subtotal'];
                        $order_pro['OrderProduct']['is_offer_set'] = 0;
                        $order_pro['OrderProduct']['offer_price'] = 0;

                        $this->loadModel('OrderProduct');
                        $this->OrderProduct->create();
                        if ($this->OrderProduct->save($order_pro)) {
                            //die('hereIn');
                        } else {
                            //debug($this->OrderProduct->validationErrors);
                            //die('here1');
                        }
                    }
                    //die('here2');
                    $this->generate_pdf($order_id);
                    $form_name = $invoice_num . '.pdf';
                    $form_path = APP . WEBROOT_DIR . DS . 'pdf' . DS . $pdf_name . '.pdf';

                    //old mail code with pdf as attachment
                    /* $email = new CakeEmail();
                      $email->config('order_placed');
                      $email->to($order_details['Order']['ship_email']);
                      $email->viewVars(compact('order_details', 'category'));
                      $email->subject('Pending Order with Value Market Research');
                      $email->attachments(array($form_name => $form_path));
                      $email->send(); */

                    //new code with out pdf with thank you msg
                    $email = new CakeEmail();
                    $email->config('order_placed_thankyou');
                    $email->to($order_details['Order']['ship_email']);
                    $email->viewVars(compact('order_details', 'category'));
                    $email->subject('Order Update Value Market Research');
                    //$email->attachments(array($form_name => $form_path));
                    $email->send();

                    //Order Update to DD admin email id set in email config
                    Configure::load('idata');
                    $license_type = Configure::read('idata.licence_type');

                    $admin_email = new CakeEmail();
                    $admin_email->config('admin_order_notification');
                    $admin_email->viewVars(compact('order_details', 'order_pro', 'license_type'));
                    $admin_email->subject('New Sale Received');
                    //$email->attachments(array($form_name => $form_path));
                    $admin_email->send();


                    /* $this->Session->setFlash(__('Thank you for placing an order with Value Market Research. 
                      Our Sales team representative will get in touch with you soon.In case you wish to contact
                      us, kindly write to us at sales@decisiondatabases.com'), 'success'); */
                    $this->Session->write('thanks', true);
                    return $this->redirect(array('controller' => 'users', 'action' => 'thanks'));
                }
            }
        } else {
            $this->Session->setFlash(__('The order has been Failled.'), 'error');
            return $this->redirect('/');
        }
    }

    public function beforeFilter() {
        $this->Auth->allow(array('my_orders', 'user_order_index', 'generate_pdf', 'secrate_payment', 'process_order'));
    }

}
