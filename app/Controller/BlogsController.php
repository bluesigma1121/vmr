<?php
App::uses('AppController', 'Controller');
/**
 * Blogs Controller
 *
 * @property Blog $Blog
 * @property PaginatorComponent $Paginator
 */
class BlogsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'admin_layout';
		$this->Blog->recursive = 0;
		$this->paginate = array(
				'order' => 'Blog.id DESC',
				'limit' => 20
		);
		
		$this->set('blogs', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		// $this->layout = 'admin_layout';
		if (!$this->Blog->exists($id)) {
			throw new NotFoundException(__('Invalid blog'));
		}
		$options = array('conditions' => array('Blog.' . $this->Blog->primaryKey => $id));
		$this->set('blog', $this->Blog->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'admin_layout';
		if ($this->request->is('post')) {
			$this->Blog->create();
			if ($this->Blog->save($this->request->data)) {
				$this->Session->setFlash(__('The blog has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The blog could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'admin_layout';
		if (!$this->Blog->exists($id)) {
			throw new NotFoundException(__('Invalid blog'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Blog->save($this->request->data)) {
				$this->Session->setFlash(__('The blog has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The blog could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Blog.' . $this->Blog->primaryKey => $id));
			$this->request->data = $this->Blog->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Blog->id = $id;
		if (!$this->Blog->exists()) {
			throw new NotFoundException(__('Invalid blog'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Blog->delete()) {
			$this->Session->setFlash(__('The blog has been deleted.'));
		} else {
			$this->Session->setFlash(__('The blog could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Blog->recursive = 0;
		$this->set('blogs', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Blog->exists($id)) {
			throw new NotFoundException(__('Invalid blog'));
		}
		$options = array('conditions' => array('Blog.' . $this->Blog->primaryKey => $id));
		$this->set('blog', $this->Blog->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Blog->create();
			if ($this->Blog->save($this->request->data)) {
				$this->Session->setFlash(__('The blog has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The blog could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Blog->exists($id)) {
			throw new NotFoundException(__('Invalid blog'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Blog->save($this->request->data)) {
				$this->Session->setFlash(__('The blog has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The blog could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Blog.' . $this->Blog->primaryKey => $id));
			$this->request->data = $this->Blog->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Blog->id = $id;
		if (!$this->Blog->exists()) {
			throw new NotFoundException(__('Invalid blog'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Blog->delete()) {
			$this->Session->setFlash(__('The blog has been deleted.'));
		} else {
			$this->Session->setFlash(__('The blog could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


	public function blog_listing() {
        
			$this->Blog->recursive = 0;
			$this->paginate = array(
					'order' => 'Blog.id DESC',
					'limit' => 1000
			);

	// Menu-Research Industries (Category and subCategory) code start here
			$this->loadModel('Category');
// 			$research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
// 			foreach ($research_ind_cat as $key1 => $cat) {
// 					$research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
// 					$research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
// 					$research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
// 					$research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
					$level_two = $this->Category->find('all', array('conditions' =>
							array(
									'Category.parent_category_id' => 2,
									'Category.is_active' => 1), 'recursive' => -1));
					foreach ($level_two as $key2 => $level2) {
							$research_ind['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
							$research_ind['Level2'][$key2]['id'] = $level2['Category']['id'];
							$research_ind['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
							$research_ind['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
							$level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
							foreach ($level_three as $key3 => $level3) {
									$research_ind['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
									$research_ind['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
									$research_ind['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
									$research_ind['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
									$level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
									foreach ($level_four as $key4 => $level4) {
											$research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
											$research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
											$research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
											$research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
											$level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
											foreach ($level_five as $key5 => $level5) {
													$research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
													$research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
													$research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
													$research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
													$level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
													foreach ($level_six as $key6 => $level6) {
															$research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
															$research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
															$research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
															$research_ind['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
													}
											}
									}
							}
					}
// 			}

	// Menu-Research Industries (Category and subCategory) code end here


			$blog = $this->Paginator->paginate();
		    	
			$this->set(compact('research_ind','blog'));
	}

	public function blog_details($blog_id = null, $url_slug = null) {
		

			$blog = $this->Blog->find('first', array('conditions' => array('Blog.id' => $blog_id)));

			$meta_name = $blog['Blog']['meta_title'];
			$meta_keyword = $blog['Blog']['meta_keywords'];
			$meta_desc = $blog['Blog']['meta_desc'];


	// Menu-Research Industries (Category and subCategory) code start here
			$this->loadModel('Category');
			$research_ind_cat = $this->Category->find('all', array('conditions' => array('Category.display_home' => 1, 'Category.is_active' => 1), 'recursive' => -1));
			foreach ($research_ind_cat as $key1 => $cat) {
					$research_ind['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
					$research_ind['Level1'][$key1]['id'] = $cat['Category']['id'];
					$research_ind['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
					$research_ind['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
					$level_two = $this->Category->find('all', array('conditions' =>
							array(
									'Category.parent_category_id' => $cat['Category']['id'],
									'Category.is_active' => 1), 'recursive' => -1));
					foreach ($level_two as $key2 => $level2) {
							$research_ind['Level1'][$key1]['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
							$research_ind['Level1'][$key1]['Level2'][$key2]['id'] = $level2['Category']['id'];
							$research_ind['Level1'][$key1]['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
							$research_ind['Level1'][$key1]['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
							$level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
							foreach ($level_three as $key3 => $level3) {
									$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
									$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
									$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
									$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
									$level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
									foreach ($level_four as $key4 => $level4) {
											$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
											$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
											$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
											$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
											$level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
											foreach ($level_five as $key5 => $level5) {
													$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
													$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
													$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
													$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
													$level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
													foreach ($level_six as $key6 => $level6) {
															$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
															$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
															$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
															$research_ind['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
													}
											}
									}
							}
					}
			}

	// Menu-Research Industries (Category and subCategory) code end here



			$this->set(compact('research_ind', 'blog', 'meta_name', 'meta_keyword', 'meta_desc'));
	}

	public function beforeFilter() {
			$this->Auth->allow(array('blog_listing', 'blog_details'));
	}


}
