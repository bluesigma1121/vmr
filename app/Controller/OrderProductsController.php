<?php
App::uses('AppController', 'Controller');
/**
 * OrderProducts Controller
 *
 * @property OrderProduct $OrderProduct
 * @property PaginatorComponent $Paginator
 */
class OrderProductsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->OrderProduct->recursive = 0;
		$this->set('orderProducts', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->OrderProduct->exists($id)) {
			throw new NotFoundException(__('Invalid order product'));
		}
		$options = array('conditions' => array('OrderProduct.' . $this->OrderProduct->primaryKey => $id));
		$this->set('orderProduct', $this->OrderProduct->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->OrderProduct->create();
			if ($this->OrderProduct->save($this->request->data)) {
				$this->Session->setFlash(__('The order product has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The order product could not be saved. Please, try again.'));
			}
		}
		$orders = $this->OrderProduct->Order->find('list');
		$products = $this->OrderProduct->Product->find('list');
		$categories = $this->OrderProduct->Category->find('list');
		$this->set(compact('orders', 'products', 'categories'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->OrderProduct->exists($id)) {
			throw new NotFoundException(__('Invalid order product'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->OrderProduct->save($this->request->data)) {
				$this->Session->setFlash(__('The order product has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The order product could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('OrderProduct.' . $this->OrderProduct->primaryKey => $id));
			$this->request->data = $this->OrderProduct->find('first', $options);
		}
		$orders = $this->OrderProduct->Order->find('list');
		$products = $this->OrderProduct->Product->find('list');
		$categories = $this->OrderProduct->Category->find('list');
		$this->set(compact('orders', 'products', 'categories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->OrderProduct->id = $id;
		if (!$this->OrderProduct->exists()) {
			throw new NotFoundException(__('Invalid order product'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->OrderProduct->delete()) {
			$this->Session->setFlash(__('The order product has been deleted.'));
		} else {
			$this->Session->setFlash(__('The order product could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
