<?php

App::uses('AppModel', 'Model');

/**
 * Product Model
 *
 * @property Intrest $Intrest
 * @property ProductCategory $ProductCategory
 * @property ProductSpecification $ProductSpecification
 * @property RecentView $RecentView
 */
class Product extends AppModel {

    public $actsAs = array(
        'Sitemap.Sitemap' => array(
            'primaryKey' => 'id', //Default primary key field
            'loc' => 'buildUrlProducts', //Default function called that builds a url, passes parameters (Model $Model, $primaryKey)
            'lastmod' => 'modified', //Default last modified field, can be set to FALSE if no field for this
            'changefreq' => 'weekly', //Default change frequency applied to all model items of this type, can be set to FALSE to pass no value
            'priority' => '0.9', //Default priority applied to all model items of this type, can be set to FALSE to pass no value
            'conditions' => array('Product.is_active' => 1,'Product.id >' => 4529), //Conditions to limit or control the returned results for the sitemap
        )
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'product_name' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
//'allowEmpty' => false,
//'required' => false,
//'last' => false, // Stop validation after this rule
//'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'price' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
//'allowEmpty' => false,
//'required' => false,
//'last' => false, // Stop validation after this rule
//'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

//The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'category_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
    public $hasMany = array(
        'ProductCategory' => array(
            'className' => 'ProductCategory',
            'foreignKey' => 'product_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'ProductSpecification' => array(
            'className' => 'ProductSpecification',
            'foreignKey' => 'product_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'RecentView' => array(
            'className' => 'RecentView',
            'foreignKey' => 'product_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'ProductCountry' => array(
            'className' => 'ProductCountry',
            'foreignKey' => 'product_id',
            'dependent' => false
        )
    );

    public function update_company_reports() {
        $url = Router::url('/', true);
        if ($url == 'http://localhost/idata/') {
            $temp_id = 301;
        } else {
            $temp_id = 1989;
        }

        //debug('here');
        //die;
        $test_pro = $this->find('first', array('conditions' => array('Product.id' => $temp_id),
            'fields' => array('product_description', 'product_specification', 'publisher_name', 'price', 'corporate_price','upto10', 'pub_date'),
            'recursive' => -1
        ));

        $all_products = $this->find('all', array(
            'conditions' => array('Product.category_id' => 3, 'Product.id !=' => $temp_id),
            'fields' => array('Product.id', 'Product.product_name'),
            'recursive' => -1));

        foreach ($all_products as $ap) {
            $temp = explode('-', $ap['Product']['product_name']);

            $company_name = trim($temp[0]);
            $report_name = $company_name . " - Strategy and SWOT Analysis Report";

            $records_success['category_id'] = 3;
            $records_success['product_name'] = $report_name;
            $records_success['publisher_name'] = $test_pro['Product']['publisher_name'];
            $records_success['alias'] = $report_name;

            //$records_success['product_description'] = str_replace("(Company Name)", $company_name, $test_pro['Product']['product_description']);
            //$records_success['product_specification'] = str_replace("(Company Name)", $company_name, $test_pro['Product']['product_specification']);

            $records_success['meta_name'] = "$company_name Company Profile – SWOT Analysis Report : valuemarketresearch.com";
            $records_success['meta_desc'] = "The $report_name by valuemarketresearch.com offers an insightful study of the company's recent developments, SWOT analysis, and its financial & operational strategies.";
            $records_success['meta_keywords'] = "$company_name Profile, $company_name SWOT Analysis, $company_name Product Details, $company_name Financial Performance, $company_name Business Profile, $company_name Growth Analysis, $company_name Developments";
            $records_success['slug'] = $this->cleanString($company_name . "-strategy-SWOT-analysis-report");

            $record_to_be_saved = '';
            $record_to_be_saved['Product']['id'] = $ap['Product']['id'];
            $record_to_be_saved['Product'] = $records_success;
            $records_success = '';
            $this->id = $ap['Product']['id'];
            if ($this->save($record_to_be_saved)) {

            }
        }
        debug('success');
        die;
    }

    public function upload_company_reports($data) {
        $import_file = $data['Product']['product_upload'];
        $sub = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789', 5)), 0, 5);
        if ($import_file['error'] == UPLOAD_ERR_OK) {
            $destination_file = APP . WEBROOT_DIR . DS . 'files' . DS . 'uploads' . DS . $sub . $import_file['name'];

            if (move_uploaded_file($import_file['tmp_name'], $destination_file)) {
                chmod($destination_file, 0777);
                $reader = new Spreadsheet_Excel_Reader();
                $reader->read($destination_file, true);
                $records_success = array();
                $err_arr = array();
                $err_sts_flash = 0;
                set_time_limit(600);

                $url = Router::url('/', true);

                if ($url == 'http://localhost/idata/') {
                    $temp_id = 301;
                } else {
                    $temp_id = 1989;
                }

                $test_pro = $this->find('first', array('conditions' => array('Product.id' => $temp_id),
                    'fields' => array('product_description', 'product_specification', 'publisher_name', 'price', 'corporate_price','upto10', 'pub_date'),
                    'recursive' => -1
                ));


                if (($reader->sheets[0]['numRows']) <= 100) {
                    for ($i = 2; $i <= $reader->sheets[0]['numRows']; $i++) {
                        $j = 1;

                        //echo "<pre>";
                        //print_r($data['Product']['category_id']);
                        //print_r($reader->sheets[0]);

                        $company_name = trim(@$reader->sheets[0]['cells'][$i][$j++]);
                        $report_name = $company_name . " - Strategy and SWOT Analysis Report";

                        $records_success[$i]['category_id'] = $data['Product']['category_id'];
                        $records_success[$i]['product_name'] = $report_name;
                        $records_success[$i]['publisher_name'] = $test_pro['Product']['publisher_name'];
                        $records_success[$i]['alias'] = $report_name;
                        $records_success[$i]['alias'] = $report_name;
                        $records_success[$i]['product_description'] = str_replace("(Company Name)", $company_name, $test_pro['Product']['product_description']);
                        $records_success[$i]['product_specification'] = str_replace("(Company Name)", $company_name, $test_pro['Product']['product_specification']);
                        $records_success[$i]['price'] = $test_pro['Product']['price'];
                        $records_success[$i]['corporate_price'] = $test_pro['Product']['corporate_price'];
                        $records_success[$i]['upto10'] = $test_pro['Product']['upto10'];
                        $records_success[$i]['is_active'] = 0;

                        $records_success[$i]['pub_date'] = $test_pro['Product']['pub_date'];

                        $records_success[$i]['meta_name'] = "$company_name Company Profile – SWOT Analysis Report : valuemarketresearch.com";
                        $records_success[$i]['meta_desc'] = "The $report_name by valuemarketresearch.com offers an insightful study of the company's recent developments, SWOT analysis, and its financial & operational strategies.";
                        $records_success[$i]['meta_keywords'] = "$company_name Profile, $company_name SWOT Analysis, $company_name Product Details, $company_name Financial Performance, $company_name Business Profile, $company_name Growth Analysis, $company_name Developments";
                        $records_success[$i]['slug'] = $this->cleanString($company_name . "-strategy-SWOT-analysis-report");

                        $record_to_be_saved = '';
                        $record_to_be_saved = $records_success[$i];

                        $duplicate = $this->find('first', array(
                            'conditions' => array('Product.product_name' => $report_name),
                            'fields' => array('Product.id'),
                            'recursive' => -1));

                        if (!empty($duplicate)) {

                        } else {
                            $this->create();
                            if ($this->save($record_to_be_saved)) {
                                if ($this->update_category_counts($record_to_be_saved['category_id'], $this->id)) {
                                    $err_sts_flash = 1;
                                } else {
                                    $err_sts_flash = 2;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function save_product_info($data) {

        if (!empty($data['Product']['pub_month']) && !empty($data['Product']['pub_year'])) {
            $dt = $data['Product']['pub_year'] . '-' . $data['Product']['pub_month'] . '-' . '1';
            $data['Product']['pub_date'] = date('Y-m-d', strtotime($dt));
        } else {
            return 0;
        }


        App::uses('ProductCategory', 'Model');
        $this->ProductCategory = new ProductCategory();
        App::uses('Category', 'Model');
        $this->Category = new Category();
        $duplicate_product = $this->find('first', array(
            'recursive' => -1,
            'fields' => array('Product.id', 'Product.product_name'),
            'conditions' => array('Product.product_name' => $data['Product']['product_name'])));
        if (!empty($duplicate_product)) {
            return 999;
        }

        if (isset($data['Product']['product_image']) && !empty($data['Product']['product_image'])) {
            $image = $data['Product']['product_image'];
            $alias_name = $this->uploadMainImage($image);
            $data['Product']['product_image'] = $alias_name;
        } else {
            $data['Product']['product_image'] = NULL;
        }
        $data['Product']['is_active'] = 0;
        $data['Product']['is_verified'] = 0;
        $cat_name = $this->Category->find('first', array(
            'conditions' => array('Category.id' => 2),
            'fields' => array('Category.cat_slug'),
            'recursive' => -1
        ));
        $slug = $data['Product']['product_name'];
        if (!empty($cat_name)) {
            $cat_split = explode(' ', $cat_name['Category']['cat_slug']);
            end($cat_split);
            $key = key($cat_split);
            $cat_split[$key + 1] = Inflector::singularize($cat_split[$key]);
            // $cat_split[$key] = "Research";
            $final_cat_slug = implode(" ", $cat_split);
            $slug = $slug . ' ' . $final_cat_slug;
        }
        $data['Product']['slug'] = $this->cleanString($slug);
        // code started LB 14/3/2022    
        $schema_data_arr = $rating = $aggregate_rating = $reviewCount = array();    
        $rating = $data['schema_data']['rating'];             
        $aggregate_rating = $data['schema_data']['aggregate_rating']; 
        $reviewCount = $data['schema_data']['reviewCount']; 
        foreach($rating as $val)
        {
            $schema_data_arr['rating'] = $val; 
        }  
        foreach($aggregate_rating as $val)
        {
            $schema_data_arr['aggregate_rating'] = $val; 
        } 
        foreach($reviewCount as $val)
        {
            $schema_data_arr['reviewCount'] = $val; 
        }            
        $schema_data_json = json_encode($schema_data_arr);                  
        $data['Product']['schema_data'] = $schema_data_json;              
        // code ended LB 14/3/2022
        $this->create();
        if ($this->save($data)) {
            return $this->update_category_counts(2, $this->id);
        }
        return 0;
    }

    public function update_category_counts($category_id, $product_id) {

//        $counter_data = $this->Category->find('first', array(
//            'conditions' => array('Category.id' => $category_id),
//            'fields' => array('Category.no_of_product'),
//            'recursive' => -1
//        ));
//        $cnt = $counter_data['Category']['no_of_product'] + 1;
//        $this->Category->updateAll(
//                array('Category.no_of_product' => $cnt), array('Category.id' => $category_id));

        $input_id = $product_id;
        $tkt = str_pad($input_id, 4, "0", STR_PAD_LEFT);
        $product_no = 'VMR1121' . $tkt;
        $this->updateAll(array('Product.product_no' => "'$product_no'"), array('Product.id' => $input_id));
        $prod_cat['ProductCategory']['category_id'] = $category_id;
        $prod_cat['ProductCategory']['product_id'] = $input_id;
        $this->ProductCategory->create();
        //$this->ProductCategory->save($prod_cat);
        $child_product_count = $this->ProductCategory->get_child_count($category_id);
        $this->ProductCategory->set_count_product($category_id, $child_product_count);
        return $input_id;
    }

    public function edit_product_info($data) {

        if (!empty($data['Product']['pub_month']) && !empty($data['Product']['pub_year'])) {
            $dt = $data['Product']['pub_year'] . '-' . $data['Product']['pub_month'] . '-' . '1';
            $data['Product']['pub_date'] = date('Y-m-d', strtotime($dt));
        } else {
            return 0;
        }
        if (isset($data['Product']['product_image']['name']) && !empty($data['Product']['product_image']['name'])) {
            $image = $data['Product']['product_image'];
            $alias_name = $this->uploadMainImage($image);
            unlink(IMAGES . 'product_images' . DS . $data['Product']['prev_photo']);
            $data['Product']['product_image'] = $alias_name;
        } else {
            if (isset($data['Product']['prev_photo'])) {
                $data['Product']['product_image'] = $data['Product']['prev_photo'];
            } else {
                $data['Product']['product_image'] = NULL;
            }
        }
        unset($data['Product']['prev_photo']);
        $data['Product']['slug'] = $this->cleanString($data['Product']['slug']);
        // code started LB 14/3/2022    
        $schema_data_arr = $rating = $aggregate_rating = $reviewCount = array();    
        $rating = $data['schema_data']['rating'];             
        $aggregate_rating = $data['schema_data']['aggregate_rating'];
        $reviewCount = $data['schema_data']['reviewCount'];         
        foreach($rating as $val)
        {
            $schema_data_arr['rating'] = $val; 
        }  
        foreach($aggregate_rating as $val)
        {
            $schema_data_arr['aggregate_rating'] = $val; 
        } 
        foreach($reviewCount as $val)
        {
            $schema_data_arr['reviewCount'] = $val; 
        }           
        $schema_data_json = json_encode($schema_data_arr);                  
        $data['Product']['schema_data'] = $schema_data_json;               
        // code ended LB 14/3/2022
        if ($this->save($data)) {
            return 1;
        }
        return 0;
    }

    public function uploadMainImage($image = null) {
        $alias_name = '';
        $img_name = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 5)), 0, 7);
        $upload_path = IMAGES . 'product_images';

        if ($image['error'] == UPLOAD_ERR_OK) {
            $ext = pathinfo($image["name"], PATHINFO_EXTENSION);
            if (strtolower($ext) == 'png' || strtolower($ext) == 'jpg' || strtolower($ext) == 'gif' || strtolower($ext) == 'ico') {
                if (move_uploaded_file($image['tmp_name'], $upload_path . DS . $img_name . "." . $ext)) {
                    $alias_name = $img_name . "." . $ext;
                }
            }
        }
        return $alias_name;
    }

    public function add_prod_category($data) {

        $final_a = array_unique($data['cat_hidden']);
        App::uses('ProductCategory', 'Model');
        $this->ProductCategory = new ProductCategory();
        App::uses('Category', 'Model');
        $this->Category = new Category();
        $f = 0;
        if (!empty($final_a)) {
            foreach ($final_a as $key => $cat_id) {
                $prod_cat['ProductCategory']['category_id'] = $cat_id;
                $prod_cat['ProductCategory']['product_id'] = $data['id'];
                $this->ProductCategory->create();
                if ($this->ProductCategory->save($prod_cat)) {
                    $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                    $this->ProductCategory->set_count_product($cat_id, $child_product_count);
//    $level4 = $this->Category->find('first', array('conditions' => array('Category.id' => $cat_id), 'recursive' => -1));
//                    if (!empty($level4)) {
//                        if ($level4['Category']['parent_category_id'] != 0) {
//                            $cnt = $level4['Category']['no_of_product'] + 1;
//                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level4['Category']['id']));
//                            $level3 = $this->Category->find('first', array('conditions' => array('Category.id' => $level4['Category']['parent_category_id']), 'recursive' => -1));
//                            if (!empty($level3)) {
//                                if ($level3['Category']['parent_category_id'] != 0) {
//                                    $cnt = $level3['Category']['no_of_product'] + 1;
//                                    $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level3['Category']['id']));
//                                    $level2 = $this->Category->find('first', array('conditions' => array('Category.id' => $level3['Category']['parent_category_id']), 'recursive' => -1));
//                                    if (!empty($level2)) {
//                                        if ($level2['Category']['parent_category_id'] != 0) {
//                                            $cnt = $level2['Category']['no_of_product'] + 1;
//                                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level2['Category']['id']));
//                                            $level1 = $this->Category->find('first', array('conditions' => array('Category.id' => $level2['Category']['parent_category_id']), 'recursive' => -1));
//                                            if (!empty($level1)) {
//                                                if ($level1['Category']['parent_category_id'] != 0) {
//                                                    $cnt = $level1['Category']['no_of_product'] + 1;
//                                                    $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level1['Category']['id']));
//                                                    $level0 = $this->Category->find('first', array('conditions' => array('Category.id' => $level1['Category']['parent_category_id']), 'recursive' => -1));
//                                                    if (!empty($level0)) {
//                                                        if ($level0['Category']['parent_category_id'] != 0) {
//                                                            $cnt = $level0['Category']['no_of_product'] + 1;
//                                                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level0['Category']['parent_category_id']));
//                                                        } else {
//                                                            $cnt = $level0['Category']['no_of_product'] + 1;
//                                                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level0['Category']['parent_category_id']));
//                                                        }
//                                                    }
//                                                } else {
//                                                    $cnt = $level1['Category']['no_of_product'] + 1;
//                                                    $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level1['Category']['id']));
//                                                }
//                                            }
//                                        } else {
//                                            $cnt = $level2['Category']['no_of_product'] + 1;
//                                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level2['Category']['id']));
//                                        }
//                                    }
//                                } else {
//                                    $cnt = $level3['Category']['no_of_product'] + 1;
//                                    $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level3['Category']['id']));
//                                }
//                            }
//                        } else {
//                            $cnt = $level4['Category']['no_of_product'] + 1;
//                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level4['Category']['id']));
//                        }
//                    }
                    $f = 1;
                } else {
                    $f = 0;
                }
            }
        }
        return $f;
    }

    public function edit_prod_category($data) {
        $f = 0;
        if (!empty($data['cat_hidden'])) {
            $final_a = array_unique($data['cat_hidden']);
            //debug($final_a);die;
            App::uses('ProductCategory', 'Model');
            $this->ProductCategory = new ProductCategory();
            App::uses('Category', 'Model');
            $this->Category = new Category();
            $f = 0;
            if (!empty($final_a)) {
                foreach ($final_a as $key => $cat_id) {
                    $prod_main_cat = $this->find('first', array('conditions' => array('Product.id' => $data['id']), 'fields' => array('Product.category_id'), 'recursive' => -1));
//   $this->update_counter_add($cat_id);
                    $result = $this->ProductCategory->find('first', array('conditions' =>
                        array('ProductCategory.product_id' => $data['id'],
                            'ProductCategory.category_id' => $cat_id)));


                    if (empty($result)) {
                        $prod_cat['ProductCategory']['category_id'] = $cat_id;
                        $prod_cat['ProductCategory']['product_id'] = $data['id'];
                        if (!empty($prod_cat)) {
                            $this->ProductCategory->create();
                            if ($this->ProductCategory->save($prod_cat)) {
                                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                                $this->ProductCategory->set_count_product($cat_id, $child_product_count);


                                $f = 1;
                            } else {
                                $f = 0;
                            }
                        }
                    }
                }
            }

            return $f;
        } else {
            return $er = 2;
        }
    }

    public function update_counter_add($id = null) {
        App::uses('Category', 'Model');
        $this->Category = new Category();
        $level4 = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'recursive' => -1));

        if (!empty($level4)) {
            if ($level4['Category']['parent_category_id'] != 0) {
                $cnt = $level4['Category']['no_of_product'] + 1;
                $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level4['Category']['id']));
                $level3 = $this->Category->find('first', array('conditions' => array('Category.id' => $level4['Category']['parent_category_id']), 'recursive' => -1));
                if (!empty($level3)) {
                    if ($level3['Category']['parent_category_id'] != 0) {
                        $cnt = $level3['Category']['no_of_product'] + 1;
                        $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level3['Category']['id']));
                        $level2 = $this->Category->find('first', array('conditions' => array('Category.id' => $level3['Category']['parent_category_id']), 'recursive' => -1));
                        if (!empty($level2)) {
                            if ($level2['Category']['parent_category_id'] != 0) {
                                $cnt = $level2['Category']['no_of_product'] + 1;
                                $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level2['Category']['id']));
                                $level1 = $this->Category->find('first', array('conditions' => array('Category.id' => $level2['Category']['parent_category_id']), 'recursive' => -1));
                                if (!empty($level1)) {
                                    if ($level1['Category']['parent_category_id'] != 0) {
                                        $cnt = $level1['Category']['no_of_product'] + 1;
                                        $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level1['Category']['id']));
                                        $level0 = $this->Category->find('first', array('conditions' => array('Category.id' => $level1['Category']['parent_category_id']), 'recursive' => -1));
                                        if (!empty($level0)) {
                                            if ($level0['Category']['parent_category_id'] != 0) {
                                                $cnt = $level0['Category']['no_of_product'] + 1;
                                                $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level0['Category']['parent_category_id']));
                                            } else {
                                                $cnt = $level0['Category']['no_of_product'] + 1;
                                                $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level0['Category']['parent_category_id']));
                                            }
                                        }
                                    } else {

                                        $cnt = $level1['Category']['no_of_product'] + 1;
                                        $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level1['Category']['id']));
                                    }
                                }
                            } else {
                                $cnt = $level2['Category']['no_of_product'] + 1;
                                $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level2['Category']['id']));
                            }
                        }
                    } else {
                        $cnt = $level3['Category']['no_of_product'] + 1;
                        $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level3['Category']['id']));
                    }
                }
            } else {
                $cnt = $level4['Category']['no_of_product'] + 1;
                $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level4['Category']['id']));
            }
        }
        return 1;
    }

    public function cleanString($string) {
        $string = str_replace(' ', '-', $string);
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $string = preg_replace('/-+/', '-', $string);
        return strtolower($string);
    }

    public function search_autofill_function($data = null) {
        $conditions = array();
        $conditions["Product.product_name LIKE"] = '%' . $data['id'] . '%';
        $result_data = $this->find('list', array(
            'conditions' => array($conditions, 'Product.is_active' => 1, 'Product.product_disp_type' => 1, 'Product.is_deleted' => 0),
            'fields' => array('Product.id', 'Product.product_name'),
            'order' => 'Product.product_name ASC',
            'recursive' => -1,
            'limit' => 10
        ));
        $final = array();
        $i = 0;
        foreach ($result_data as $key => $val) {
            $final[$i]['id'] = $key;
            $final[$i]['name'] = $val;
            $i++;
        }
        return $final;
    }

    public function avl_country($id = NULL) {
        App::uses('ProductCountry', 'Model');
        $this->ProductCountry = new ProductCountry();
        $res = $this->ProductCountry->find('list', array('conditions' => array('ProductCountry.product_id' => $id), 'fields' => array('ProductCountry.product_id'), 'recursive' => -1));
        if (!empty($res)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function find_main_cat($cat_id = null) {
        $main_cat = array();
        $main_cat = $this->Category->find('first', array('conditions' => array('Category.id' => $cat_id)));
        return $main_cat;
    }

    public function check_url($id, $main, $slug) {
        $product = $this->find('first', array('conditions' => array('Product.id' => $id), 'fields' => array('Product.id', 'Product.slug', 'Category.slug'), 'recursive' => 0));

        if (($id == $product['Product']['id']) && ($main == $product['Category']['slug']) && (trim($slug) == trim($product['Product']['slug']))) {
            return 1;
        } else {
            return 0;
        }
    }

    public function update_seo($id = null) {
        $pro_dtl = $this->find('first', array('conditions' => array('Product.id' => $id), 'recursive' => -1));
        $prod['Product']['id'] = $pro_dtl['Product']['id'];
        if (empty($pro_dtl['Product']['meta_name'])) {
            $prod['Product']['meta_name'] = $pro_dtl['Product']['product_name'];
        }
        if (empty($pro_dtl['Product']['meta_desc']) && !empty($pro_dtl['Product']['product_description'])) {
            $temp_str = substr(strip_tags($pro_dtl['Product']['product_description']), 0, 160);
            $last = strrpos($temp_str, '.');
            $prod['Product']['meta_desc'] = $last;
        }
        $this->save($prod);
        return 1;
    }

    public function update_prod_count_admin() {
        App::uses('Category', 'Model');
        $this->Category = new Category();
        App::uses('ProductCategory', 'Model');
        $this->ProductCategory = new ProductCategory();
        $this->ProductCategory->clean_extra_relation();
        $level1 = $this->Category->find('list', array('conditions' => array('Category.level' => 1), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));

        if (!empty($level1)) {
            foreach ($level1 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }
        $level2 = $this->Category->find('list', array('conditions' => array('Category.level' => 2), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level2)) {
            foreach ($level2 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }
        $level3 = $this->Category->find('list', array('conditions' => array('Category.level' => 3), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level3)) {
            foreach ($level3 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }
        $level4 = $this->Category->find('list', array('conditions' => array('Category.level' => 4), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level4)) {
            foreach ($level4 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }
        $level5 = $this->Category->find('list', array('conditions' => array('Category.level' => 5), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level5)) {
            foreach ($level5 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }
        $level6 = $this->Category->find('list', array('conditions' => array('Category.level' => 6), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level6)) {
            foreach ($level6 as $key => $cat_id) {
                $child_product_count = $this->ProductCategory->get_child_count($cat_id);
                $this->ProductCategory->set_count_product($cat_id, $child_product_count);
            }
        }

        return 1;
    }

    public function upload_product_excel($data) {

        App::import('Vendor', 'PHPExcel/Classes/PHPExcel');

        if (!class_exists('PHPExcel')) {
            throw new CakeException('Vendor class PHPExcel not found!');
        }

        $this->import_map = array(
            0  => 'product_name',
            3  => 'price',
            4  => 'corporate_price',
            5  => 'upto10',
            6  => 'pub_date',
            7  => 'product_description',
            9  => 'product_specification',
            12  => 'meta_name',
            13  => 'meta_keywords',
            14  => 'meta_desc',
            15  => 'slug',
            16  => 'publisher_name'
        );

        Configure::load('idata');
        $import_file = $data['Product']['product_upload'];
        if ($import_file['error'] == UPLOAD_ERR_OK) {
            $destination_file = APP . WEBROOT_DIR . DS . 'files' . DS . $import_file['name'];

            if (move_uploaded_file($import_file['tmp_name'], $destination_file)) {
                chmod($destination_file, 0777);

                $xls = PHPExcel_IOFactory::load($destination_file);
                $sheet = $xls->getSheet(0);
                $row_count = $sheet->getHighestRow();
                $last_column = $sheet->getHighestDataColumn();
                $col_count = PHPExcel_Cell::columnIndexFromString($sheet->getHighestDataColumn());
                for($i = 2; $i <= $row_count; $i++) {
                    // $row = $sheet->rangeToArray("A$i:$last_column$i", null, true, false)[0];
                    $rows = $sheet->rangeToArray("A$i:$last_column$i", null, true, false);
                    // $row = $row[0];
                    $row = $rows[0];
                    $product = array();
                    foreach($this->import_map as $col => $col_name) {
                        if ($col == 7 or $col == 9){
                            $product[$col_name] =  nl2br($row[$col]);
                        } else {
                            $product[$col_name] =  ($row[$col]);
                        }
                    }
                    $product['category_id'] = '2';
                    $product_alias = '';
                    $product_alias = $product['product_name'];
                    $product['alias'] = $product_alias;
                    $excelDate = $product['pub_date'];
                    $excelDatetemp1 = PHPExcel_Shared_Date::ExcelToPHPObject($excelDate);
                    $excelDatetemp2 = $excelDatetemp1->format('y/m/d');
                    $product['pub_date'] = $excelDatetemp2;
                    $this->create();
                    if ($this->save($product)){
                        $sts = 0;
                    } else{
                        $sts = 1;
                    };
                    $input_id = $this->id;
                    $tkt = str_pad($input_id, 5, "0", STR_PAD_LEFT);
                    $product_no = 'VMR1121' . $tkt;
                    $this->updateAll(array('Product.product_no' => "'$product_no'"), array('Product.id' => $input_id));
                }
                return $sts;
            }
        }
    }

   public function add_prod_faq($data)
    {                     
        $f = 0; 
        $question = $answer = [];    
        // echo "<pre>"; print_r(array_values($data['faq']['Question']));   
        // foreach ($data['faq']['Question'] as $qkey => $qvalue) {
        //     $qvalue = trim($qvalue);
        //     if (!empty($qvalue))
        //         $question[] = $qvalue;             
        // }
        // foreach ($data['faq']['Answer'] as $akey => $avalue) {
        //     $avalue = trim($avalue);
        //     if (!empty($avalue))
        //         $answer[] = $avalue;                    
        // }
        // echo "<pre>"; print_r($question); print_r("\n"); print_r($answer); 
        // exit;  
        $question = $answer = [];   
        if (!empty($data['id'])) {            
            $question = $data['faq']['Question'];    
            $answer = $data['faq']['Answer'];      
            $faq = array();
            foreach ($question as $key => $val1) {
            $val2 = $answer[$key];
            if(!empty($val1) && !empty($val2))
                $faq[$val1] = $val2;
            }            
            // $faq = array_combine($question,$answer);            
            $faq_json = json_encode($faq);                         
            $this->id = $this->field('id', array('id' => $data['id']));            
            if ($this->id) {
                $this->saveField('product_faq', $faq_json);
                $f = 1 ;
            }  
            else
            {
                $f = 0;
            }            
           
        }
        return $f;
    }
}
