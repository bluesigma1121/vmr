<?php

App::uses('AppModel', 'Model');

/**
 * Specification Model
 *
 * @property Category $Category
 * @property SpecificationOption $SpecificationOption
 */
class Specification extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'specification_name' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'SpecificationOption' => array(
            'className' => 'SpecificationOption',
            'foreignKey' => 'specification_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function prod_specification($data) {
        $f = 0;
        App::uses('ProductSpecification', 'Model');
        $this->ProductSpecification = new ProductSpecification();
        if (!empty($data['Specification']['specification_option_id'])) {
            foreach ($data['Specification']['specification_option_id'] as $key => $op_id) {
                $prod_option['ProductSpecification']['product_id'] = $data['Specification']['prod_id'];
                $prod_option['ProductSpecification']['specification_id'] = $data['Specification']['specification_id'];
                $prod_option['ProductSpecification']['specification_option_id'] = $op_id;
                $this->ProductSpecification->create();
                if ($this->ProductSpecification->save($prod_option)) {
                    $f = 1;
                } else {
                    $f = 0;
                }
            }
        }
        return $f;
    }

    public function check_speci($id = NULL) {
        App::uses('ProductSpecification', 'Model');
        $this->ProductSpecification = new ProductSpecification();
        $res = $this->ProductSpecification->find('list', array('conditions' => array('ProductSpecification.product_id' => $id), 'fields' => array('ProductSpecification.product_id'), 'recursive' => -1));
        if (!empty($res)) {
            return 1;
        } else {
            return 0;
        }
    }

}
