<?php

App::uses('AppModel', 'Model');

/**
 * OurClient Model
 *
 */
class OurClient extends AppModel {

    public function save_client_fun($data) {

        if (isset($data['OurClient']['logo']) && !empty($data['OurClient']['logo'])) {
            $image = $data['OurClient']['logo'];
            $alias_name = $this->uploadMainImage($image);
            $data['OurClient']['logo'] = $alias_name;
        } else {
            $data['OurClient']['logo'] = NULL;
        }
        if ($this->save($data)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function edit_client_info($data) {
        if (isset($data['OurClient']['logo']['name']) && !empty($data['OurClient']['logo']['name'])) {
            $image = $data['OurClient']['logo'];
            $alias_name = $this->uploadMainImage($image);
            if(!empty($data['OurClient']['prev_photo'])) {
                unlink(IMAGES . 'client_images' . DS . $data['OurClient']['prev_photo']);
            }
            $data['OurClient']['logo'] = $alias_name;
        } else {
            if (isset($data['OurClient']['prev_photo'])) {
                $data['OurClient']['logo'] = $data['OurClient']['prev_photo'];
            } else {
                $data['OurClient']['logo'] = NULL;
            }
        }
        unset($data['OurClient']['prev_photo']);
        if ($this->save($data)) {
            return 1;
        }
        return 0;
    }

    public function uploadMainImage($image = null) {
        $alias_name = '';
        $img_name = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 5)), 0, 7);
        $upload_path = IMAGES . 'client_images';
        if ($image['error'] == UPLOAD_ERR_OK) {
            $ext = pathinfo($image["name"], PATHINFO_EXTENSION);
            if (strtolower($ext) == 'webp' || strtolower($ext) == 'png' || strtolower($ext) == 'jpg' || strtolower($ext) == 'gif' || strtolower($ext) == 'ico') {
                if (move_uploaded_file($image['tmp_name'], $upload_path . DS . $img_name . "." . $ext)) {
                    $alias_name = $img_name . "." . $ext;
                }
            }
        }
        return $alias_name;
    }

}
