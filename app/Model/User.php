<?php

App::uses('AppModel', 'Model');

/**
 * User Model
 *
 * @property Enquiry $Enquiry
 * @property Intrest $Intrest
 * @property RecentView $RecentView
 * @property Testimonial $Testimonial
 * @property Transaction $Transaction
 * @property UserCategory $UserCategory
 * @property UserContact $UserContact
 */
class User extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $virtualFields = array(
        'name' => 'CONCAT(User.first_name, " ",User.last_name)'
    );
    public $validate = array(
        'first_name' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'last_name' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'email' => array(
            'email' => array(
                'rule' => array('email'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'password' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'role' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Enquiry' => array(
            'className' => 'Enquiry',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'RecentView' => array(
            'className' => 'RecentView',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Testimonial' => array(
            'className' => 'Testimonial',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Transaction' => array(
            'className' => 'Transaction',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'UserCategory' => array(
            'className' => 'UserCategory',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'UserContact' => array(
            'className' => 'UserContact',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function beforeSave($options = Array()) {
        if (isset($this->data['User']['password'])) {
            $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
        }
        return true;
    }

    public function fun_change_user_password($data) {
        $pass = $this->find('first', array('conditions' => array('User.id' => AuthComponent::user('id')), 'fields' => array('User.password'), 'recursive' => -1));

        //  return 5 current password mitchmatch
        //  return 4 newpassword and  Confirm_password mitchmatch
        //  return 1 Success
        //  return 0 Fail  
        if (AuthComponent::password($data['User']['cuurent_pwd']) == $pass['User']['password']) {
            if ($data['User']['password'] == $data['User']['Confirm_password']) {
                if ($this->save($data)) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 4;
            }
        } else {
            return 5;
        }
    }

    public function change_user_password($data) {
        $this->read(null, $this->id);
        //  return 5 current password mitchmatch
        //  return 4 newpassword and  Confirm_password mitchmatch
        //  return 1 Success
        //  return 0 Fail  
        if (AuthComponent::password($data['User']['cuurent_pwd']) == $this->data['User']['password']) {
            if ($data['User']['new_password'] == $data['User']['Confirm_password']) {
                $this->set('password', $data['User']['new_password']);
                if ($this->save()) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 4;
            }
        } else {
            return 5;
        }
    }

    public function set_user_password($data) {

        // $this->read(null, $this->id);
        //  return 4 newpassword and  Confirm_password mitchmatch
        //  return 1 Success
        //  return 0 Fail  
        if ($data['User']['password'] == $data['User']['Confirm_password']) {
            // $this->set('password', $data['User']['new_password']);
            if ($this->save($data)) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 4;
        }
    }

    public function update_user_address($data) {
        unset($data['User']['email']);
        $this->save($data);
        return 1;
    }

    public function detect_device() {
        $tablet_browser = 0;
        $mobile_browser = 0;

        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $tablet_browser++;
        }

        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $mobile_browser++;
        }

        if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
            $mobile_browser++;
        }

        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird', 'blac',
            'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno',
            'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-',
            'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-',
            'newt', 'noki', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox',
            'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar',
            'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
            'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp',
            'wapr', 'webc', 'winw', 'winw', 'xda ', 'xda-');

        if (in_array($mobile_ua, $mobile_agents)) {
            $mobile_browser++;
        }

        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'opera mini') > 0) {
            $mobile_browser++;
            //Check for tablets on opera mini alternative headers
            $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA']) ? $_SERVER['HTTP_X_OPERAMINI_PHONE_UA'] : (isset($_SERVER['HTTP_DEVICE_STOCK_UA']) ? $_SERVER['HTTP_DEVICE_STOCK_UA'] : ''));
            if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
                $tablet_browser++;
            }
        }

        if ($tablet_browser > 0) {
            // do something for tablet devices
            CakeSession::write('device', 'mobile');
        } else if ($mobile_browser > 0) {
            // do something for mobile devices
            CakeSession::write('device', 'mobile');
        } else {
            // do something for everything else
            CakeSession::write('device', 'desktop');
        }
    }

    public function create_user($user) {
        $user['User']['role'] = (int) $user['User']['role'] ;

        if($user['User']['role'] == 3) {
            $response['status'] = false;
            $response['message'] = 'I dont like Role 3';
            return $response;
        }

        $return_code = $this->save($user);

        if($return_code === false) {
            $response['status'] = false;
            $response['message'] = 'Save unsuccessful';
            return $response;
        }

        $response['status'] = true;
        $response['message'] = 'Save successful';
        return $response;
    }

}
