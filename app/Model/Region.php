<?php

App::uses('AppModel', 'Model');

/**
 * Region Model
 *
 */
class Region extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'region_name' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    public function fun_add_countries($data) {
        App::uses('CountryRegion', 'Model');
        $this->CountryRegion = new CountryRegion();
        $reg['Region']['region_name'] = $data['Region']['region_name'];
        $this->create();
        if ($this->save($reg)) {
            $id = $this->id;
            foreach ($data['Region']['country'] as $ckey => $cou) {
                $country_data['CountryRegion']['region_id'] = $id;
                $country_data['CountryRegion']['country_id'] = $cou;
                $this->CountryRegion->create();
                $this->CountryRegion->save($country_data);
            }
            $f = 1;
        } else {
            $f = 0;
        }
        return $f;
    }

    public function edit_region_country($data) {

        App::uses('CountryRegion', 'Model');
        $this->CountryRegion = new CountryRegion();
        $reg['Region']['region_name'] = $data['Region']['region_name'];
        $reg['Region']['id'] = $data['Region']['id'];
        $this->CountryRegion->deleteAll(array('CountryRegion.region_id' => $data['Region']['id']));

        if ($this->save($reg)) {
            foreach ($data['Region']['country'] as $ckey => $cou) {
                $country_data['CountryRegion']['region_id'] = $data['Region']['id'];
                $country_data['CountryRegion']['country_id'] = $cou;
                $this->CountryRegion->create();
                $this->CountryRegion->save($country_data);
            }
            $f = 1;
        } else {
            $f = 0;
        }
        return $f;
    }

}
