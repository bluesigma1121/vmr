<?php

App::uses('AppModel', 'Model');

/**
 * SpecificationOption Model
 *
 * @property Specification $Specification
 * @property ProductSpecification $ProductSpecification
 */
class SpecificationOption extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'option_name' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'specification_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Specification' => array(
            'className' => 'Specification',
            'foreignKey' => 'specification_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'ProductSpecification' => array(
            'className' => 'ProductSpecification',
            'foreignKey' => 'specification_option_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function fun_add_options($data) {
        $flg = 0;
        foreach ($data['option_name'] as $key => $option) {
            $opt_name = ucfirst($option);
            if ($this->find('first', array('conditions' => array('SpecificationOption.specification_id' => $data['Specification']['spec_id'], 'SpecificationOption.option_name' => $option)))) {
                
            } else {
                $opt_data['SpecificationOption']['option_name'] = $option;
                $opt_data['SpecificationOption']['specification_id'] = $data['Specification']['spec_id'];
                $this->create();
                if ($this->save($opt_data)) {
                    $flg = 1;
                } else {
                    $flg = 0;
                }
            }
        }
        return $flg;
    }

}
