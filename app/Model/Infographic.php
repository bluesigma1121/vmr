<?php
App::uses('AppModel', 'Model');
// App::uses('AmazonS3', 'AmazonS3.Lib');
/**
 *
 * Rdupdateimagedata Model
 *
 * //* @property User $User
 */
//@property User $User
 
class Infographic extends AppModel {

    // public $name = 'Infographics';
    // public $useTable = 'infographics';

    public function save_client_fun($data) {

        if (isset($data['Infographic']['image_path']) && !empty($data['Infographic']['image_path'])) {
            $image = $data['Infographic']['image_path'];
            $alias_name = $this->uploadMainImage($image);
            // $alias_name = $this->uploadMainImage();
            $data['Infographic']['image_path'] = $alias_name;
        } else {
            $data['Infographic']['image_path'] = NULL;
        }
        if ($this->save($data)) {
            return 1;
        } else {
            return 0;
        }
    }
    //--------------
    public function edit_client_info($data) {
        if (isset($data['Infographic']['image_path']['name']) && !empty($data['Infographic']['image_path']['name'])) {
            $image = $data['Infographic']['image_path'];
            $alias_name = $this->uploadMainImage($image);
            if(!empty($data['Infographic']['prev_photo'])) {
                unlink(IMAGES . 'infographics_images' . DS . $data['Infographic']['prev_photo']);
            }
            $data['Infographic']['image_path'] = $alias_name;
        } else {
            if (isset($data['Infographic']['prev_photo'])) {
                $data['Infographic']['image_path'] = $data['Infographic']['prev_photo'];
            } else {
                $data['Infographic']['image_path'] = NULL;
            }
        }
        unset($data['Infographic']['prev_photo']);
        if ($this->save($data)) {
            return 1;
        }
        return 0;
    }
    public function uploadMainImage($image = null) {
        $alias_name = '';
        $img_name = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 5)), 0, 7);
        $upload_path = IMAGES . 'infographics_images';
        if ($image['error'] == UPLOAD_ERR_OK) {
            $ext = pathinfo($image["name"], PATHINFO_EXTENSION);
            if (strtolower($ext) == 'webp' || strtolower($ext) == 'png' || strtolower($ext) == 'jpg' || strtolower($ext) == 'gif' || strtolower($ext) == 'ico') {
                if (move_uploaded_file($image['tmp_name'], $upload_path . DS . $img_name . "." . $ext)) {
                    $alias_name = $img_name . "." . $ext;
                }
            }
        }
        return $alias_name;
    }

   
    public $useTable = 'infographics';
    
    public $validate = array(
        'info_title' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'slug' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'info_desc' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    public function our_infographics_fun() {
        App::uses('Infographic', 'Model');
        $this->Infographic = new Infographic();
        // $ourinfographics = $this->Infographic->find('all', array(
        //     //'conditions' => array('Infographic.is_active' => 1),
        //     //'limit' => 12,
        //     'order' => 'rand()',
        //     'recursive' => -1,
        //     'fields' => array('Infographic.id', 'Infographic.info_title','Infographic.image_path','Infographic.info_desc','Infographic.created','Infographic.product_id','Infographic.slug'),
        // ));
        // return $ourinfographics;


            $ourinfographics = $this->Infographic->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'products',
                        'alias' => 'productsJoin',
                        'type' => 'INNER',
                        'conditions' => array(
                            'productsJoin.id = Infographic.product_id'
                        )
                    )
                ),
                // 'conditions' => array(
                //     'Message.to' => 4
                // ),
                'fields' => array('productsJoin.slug', 'Infographic.*'),
                'order' => 'rand()',
            ));
           return $ourinfographics;
		// echo "<pre>";
		// print_r($ourinfographics);
		// exit;
    }
    
    public function our_infographics_funslug($slug) {
        App::uses('Infographic', 'Model');
        $this->Infographic = new Infographic();
        // $ourinfographics = $this->Infographic->find('all', array(
        //     //'conditions' => array('Infographic.is_active' => 1),
        //     //'limit' => 12,
        //     'order' => 'rand()',
        //     'recursive' => -1,
        //     'fields' => array('Infographic.id', 'Infographic.info_title','Infographic.image_path','Infographic.info_desc','Infographic.created','Infographic.product_id','Infographic.slug'),
        // ));
        // return $ourinfographics;


            $ourinfographics = $this->Infographic->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'products',
                        'alias' => 'productsJoin',
                        'type' => 'INNER',
                        'conditions' => array(
                            'productsJoin.id = Infographic.product_id'
                        )
                    )
                ),
                'conditions' => array(
                    'Infographic.slug' => $slug
                ),
                'fields' => array('productsJoin.slug', 'Infographic.*')
                //'order' => 'rand()',
            ));
           return $ourinfographics;
		// echo "<pre>";
		// print_r($ourinfographics);
		// exit;
    }
    
    // public function our_infographics_funmetatitle($slug) {
    //     App::uses('Infographic', 'Model');
    //     $this->Infographic = new Infographic();
    //     $ourinfographicsmetatitle = $this->Infographic->find('all', array(
    //         'conditions' => array('Infographic.slug' => $slug),
    //         //'limit' => 12,
    //         'recursive' => -1,
    //         'fields' => array('Infographic.meta_title','Infographic.meta_desc','Infographic.meta_keyword','Infographic.info_title','Infographic.info_desc'),
    //     ));
    //     return $ourinfographicsmetatitle;
    //     }
    // public function our_infographics_trending() {
    //     App::uses('Infographic', 'Model');
    //     $this->Infographic = new Infographic();
    //     $ourinfographicstrending = $this->Infographic->find('all', array(
    //         //'conditions' => array('Infographic.is_active' => 1),
    //         'limit' => 4,
    //         //'order' => 'rand()',
    //         //'recursive' => -1,
    //         'fields' => array('Infographic.id', 'Infographic.title','Infographic.image_path','Infographic.description','Infographic.created_on','Infographic.linktoreport'),
    //     ));
    //     return $ourinfographicstrending;
	// 	// echo "<pre>";
	// 	// print_r($ourclients);
	// 	// exit;
    // }
    

}

?>

