<?php

App::uses('AppModel', 'Model');

/**
 * Config Model
 *
 */
class Config extends AppModel {
 
    public $salt_key = "qt1U2MACDVITXGenFoZoiLwQGjLadbHA";

    public function beforeSave($options = Array()) {
        if ($this->data['Config']['config_key'] == "smtp_password") {                        
            $this->data['Config']['config_value'] = Security::encrypt($this->data['Config']['config_value'],$this->salt_key);            
        }
        return true;
    }

    public function afterFind($results, $primary = false) {
        foreach ($results as $key => $val) {                        
            if ($val['Config']['config_key'] == "smtp_password") {            
                $results['Config']['config_value'] = Security::decrypt($val['Config']['config_value'],$this->salt_key);
            }
            return $results;
        }
    }   

}
