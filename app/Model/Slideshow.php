<?php
App::uses('AppModel', 'Model');
/**
 * Slideshow Model
 *
 */
class Slideshow extends AppModel {

	public function save_image($data) {

			if (isset($data['Slideshow']['image']) && !empty($data['Slideshow']['image'])) {
					$image = $data['Slideshow']['image'];
					$alias_name = $this->uploadMainImage($image);
					$data['Slideshow']['image'] = $alias_name;
			} else {
					$data['Slideshow']['image'] = NULL;
			}
			if ($this->save($data)) {
					return 1;
			} else {
					return 0;
			}
	}
	public function uploadMainImage($image = null) {
			$alias_name = '';
			$img_name = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 5)), 0, 7);
			$upload_path = IMAGES . 'slideshow';
			if ($image['error'] == UPLOAD_ERR_OK) {
					$ext = pathinfo($image["name"], PATHINFO_EXTENSION);
					if (strtolower($ext) == 'png' || strtolower($ext) == 'jpg' || strtolower($ext) == 'gif' || strtolower($ext) == 'ico') {
							if (move_uploaded_file($image['tmp_name'], $upload_path . DS . $img_name . "." . $ext)) {
									$alias_name = $img_name . "." . $ext;
							}
					}
			}
			return $alias_name;
	}

	public function edit_image($data) {
			if (isset($data['Slideshow']['image']['name']) && !empty($data['Slideshow']['image']['name'])) {
					$image = $data['Slideshow']['image'];
					$alias_name = $this->uploadMainImage($image);
					unlink(IMAGES . 'slideshow' . DS . $data['Slideshow']['prev_photo']);
					$data['Slideshow']['image'] = $alias_name;
			} else {
					if (isset($data['Slideshow']['prev_photo'])) {
							$data['Slideshow']['image'] = $data['Slideshow']['prev_photo'];
					} else {
							$data['Slideshow']['image'] = NULL;
					}
			}
			unset($data['Slideshow']['prev_photo']);
			if ($this->save($data)) {
					return 1;
			}
			return 0;
	}

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'slideshow';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'image';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'image' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
