<style>
    .nav .open>a, .nav .open>a:hover, .nav .open>a:focus {
        background-color: black !important;
        border-color: #428bca;
    }
</style>
<?php
$users = '';
$cat = '';
$order = '';
$region = '';
$country = '';
$speci = '';
$product = '';
$enquiry = '';
$mis = '';
$sett = '';
$clients = '';
$lead_user = '';
$pending = '';
$site_visit = '';
$articles = '';
$article_reports = '';
$ready = '';
$testimonials = '';
$slideshow = '';
$reportdescriptionimages='';
$rdupdateimagedata = '';

$blog = '';
$whyus = '';
$searchresultnotfound = '';
$controller = $this->request->controller;
$action = $this->request->action;

if ($controller == 'site_visits' && $action == 'visit_report') {
    $site_visit = 'active open';
}

if ($controller == 'products' && $action == 'pending_status') {
    $pending = 'active open';
} elseif ($controller == 'products' && $action == 'products_ready_active') {
    $ready = 'active open';
} elseif ($controller == 'users' && $action == 'lead_user_index') {
    $lead_user = 'active open';
} elseif (($controller == 'users')) {
    $users = 'active open';
} elseif ($controller == 'categories') {
    $cat = 'active open';
} elseif ($controller == 'regions') {
    $region = 'active open';
} elseif ($controller == 'countries') {
    $country = 'active open';
} elseif ($controller == 'products') {
    $product = 'active open';
} elseif ($controller == 'specifications' || $controller == 'specification_options') {
    $speci = 'active open';
} elseif ($controller == 'mis') {
    $mis = 'active open';
} elseif ($controller == 'enquiries') {
    $enquiry = 'active open';
} elseif ($controller == 'our_clients') {
    $clients = 'active open';
} elseif ($controller == 'settings') {
    $sett = 'active open';
} elseif ($controller == 'orders') {
    $order = 'active open';
} elseif ($controller == 'articles') {
    $articles = 'active open';
} elseif ($controller == 'articles_reports') {
    $article_reports = 'active open';
} elseif ($controller == 'testimonials') {
    $testimonials = 'active open';
} elseif ($controller == 'slideshow') {
    $slideshow = 'active open';
}elseif ($controller == 'blog') {
    $slideshow = 'active open';
}elseif ($controller == 'reportdescriptionimages') {
    $reportdescriptionimages = 'active open';
}elseif ($controller == 'rdupdateimagedata') {
    $rdupdateimagedata = 'active open';
}  elseif ($controller == 'whyus') {
    $whyus = 'active open';
} elseif ($controller == 'searchresultnotfound') {
    $whyus = 'active open';
} else {
    $users = '';
    $cat = '';
    $region = '';
    $country = '';
    $speci = '';
    $product = '';
    $enquiry = '';
    $mis = '';
    $sett = '';
    $clients = '';
    $order = '';
    $pending = '';
    $lead_user = '';
    $article_reports = '';
    $ready = '';
    $testimonials = '';
    $slideshow = '';
    $rdupdateimagedata = '';
    $reportdescriptionimages='';
    $blog = '';
    $whyus = '';
    $searchresultnotfound = '';
}
?>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">  Welcome <?php echo ucfirst(AuthComponent::user('first_name')); ?></strong>
                                <b class="caret"></b> </span>  </span></a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">

                        <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'change_password', AuthComponent::user('id'))) ?>">Change Password</a></li>
                        <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'logout')) ?>">Logout</a></li>
                    </ul>
                </div>
            </li>
            <?php if (in_array(AuthComponent::user('role'), array(3))) { ?>
                <li class="<?php echo $pending ?>">
                    <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'seo_index')); ?>"><i class="fa fa-envelope"></i> <span class="nav-label">SEO Product Index</span></a>
                </li>
                <li class="<?php echo $pending ?>">
                    <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'index')); ?>"><i class="fa fa-laptop"></i> <span class="nav-label">All Products</span></a>
                </li>
            <?php } ?>
            <?php if (in_array(AuthComponent::user('role'), array(1,2))) { ?>
                <li class="<?php echo $pending ?>">
                    <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'pending_status')); ?>"><i class="fa fa-envelope"></i> <span class="nav-label">Pending Status </span></a>
                </li>
                <?php } ?>
                <?php if (in_array(AuthComponent::user('role'), array(1,2,3))) { ?>
                <li class="<?php echo $ready ?>">
                    <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'products_ready_active')); ?>"><i class="fa fa-laptop"></i> <span class="nav-label">Ready to active </span></a>
                </li>
            <?php } ?>
            <?php if (in_array(AuthComponent::user('role'), array(1,12))) { ?>
                <li class="<?php echo $enquiry ?>">
                    <a href="<?php echo Router::url(array('controller' => 'enquiries', 'action' => 'index')); ?>"><i class="fa fa-envelope"></i> <span class="nav-label">Inquiries</span></a>
                </li>
            <?php } ?>
            <?php /*if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $lead_user ?>">
                    <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'lead_user_index')); ?>"><i class="fa fa-envelope-square"></i> <span class="nav-label">Leads</span></a>
                </li>
            <?php } */?>
            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $users; ?>">
                    <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'index')); ?>"><i class="fa fa-users"></i> <span class="nav-label">Manage User</span></a>
                </li>
            <?php } ?>
            <?php if (in_array(AuthComponent::user('role'), array(1, 2, 3, 5))) { ?>
                <li class="<?php echo $cat ?>">
                    <a href="#"><i class="fa fa-flask"></i> <span class="nav-label">Categories </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <?php if (in_array(AuthComponent::user('role'), array(1, 2, 5))) { ?>
                            <!--<li class="<?php
                            if ($action == 'level_one' && $controller == 'categories') {
                                echo 'active';
                            }
                            ?>">
                                <a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'level_one')); ?>"> Level-1 Categories  </a></li>-->


                            <li class="<?php
                            if ($action == 'level_two' && $controller == 'categories') {
                                echo 'active';
                            }
                            ?>"><a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'level_two')); ?>"> Level-1 Categories </a></li>

                            <li class="<?php
                            if ($action == 'level_three' && $controller == 'categories') {
                                echo 'active';
                            }
                            ?>" ><a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'level_three')); ?>"> Level-2 Categories  </a></li>
                            <li class="<?php
                            if ($action == 'level_four' && $controller == 'categories') {
                                echo 'active';
                            }
                            ?>" ><a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'level_four')); ?>"> Level-3 Categories  </a></li>

                            <li class="<?php
                            if ($action == 'level_five' && $controller == 'categories') {
                                echo 'active';
                            }
                            ?>" ><a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'level_five')); ?>"> Level-4 Categories  </a></li>
                            <li class="<?php
                            if ($action == 'level_six' && $controller == 'categories') {
                                echo 'active';
                            }
                            ?>" ><a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'level_six')); ?>"> Level-5 Categories  </a></li>

                        <?php } ?>
                        <li class="<?php
                        if ($action == 'tree_view_categories' && $controller == 'categories') {
                            echo 'active';
                        }
                        ?>"><a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'tree_view_categories')); ?>"> Categories Tree View  </a></li>
                    </ul>
                </li>
            <?php } ?>
            <?php if (in_array(AuthComponent::user('role'), array(1, 2))) { ?>
                <li class="<?php echo $product ?>">
                    <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'index')); ?>"><i class="fa fa-laptop"></i> <span class="nav-label">Products</span></a>
                </li>
            <?php } ?>

            <?php if (in_array(AuthComponent::user('role'), array(5))) { ?>
                <li class="<?php echo $product ?>">
                    <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'jr_analyst_index')); ?>"><i class="fa fa-laptop"></i> <span class="nav-label">Products</span></a>
                </li>
            <?php } ?>




            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $order ?>">
                    <a href="<?php echo Router::url(array('controller' => 'orders', 'action' => 'index')); ?>"><i class="fa fa-laptop"></i> <span class="nav-label">Orders</span></a>
                </li>
            <?php } ?>
            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $speci ?>">
                    <a href="<?php echo Router::url(array('controller' => 'specifications', 'action' => 'index')); ?>"><i class="fa fa-list-alt"></i> <span class="nav-label">Specifications</span></a>
                </li>
            <?php } ?>
            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $region ?>">
                    <a href="<?php echo Router::url(array('controller' => 'regions', 'action' => 'index')); ?>"><i class="fa fa-list-alt"></i> <span class="nav-label">Regions</span></a>
                </li>
            <?php } ?>
            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $country ?>">
                    <a href="<?php echo Router::url(array('controller' => 'countries', 'action' => 'index')); ?>"><i class="fa fa-list-alt"></i> <span class="nav-label">Countries</span></a>
                </li>
            <?php } ?>
            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $mis ?>">
                    <a href="<?php echo Router::url(array('controller' => 'mis', 'action' => 'reports_by_publisher')); ?>"><i class="fa fa-certificate"></i> <span class="nav-label">Mis-Reports By Pub.</span></a>
                </li>
            <?php } ?>
            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $mis ?>">
                    <a href="<?php echo Router::url(array('controller' => 'mis', 'action' => 'vmr_reports_by_category')); ?>"><i class="fa fa-certificate"></i> <span class="nav-label">Mis-VMR Reports By Categ.</span></a>
                </li>
            <?php } ?>
            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $mis ?>">
                    <a href="<?php echo Router::url(array('controller' => 'mis', 'action' => 'leads_by_report')); ?>"><i class="fa fa-certificate"></i> <span class="nav-label">Mis-Leads By Report</span></a>
                </li>
            <?php } ?>
            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $clients ?>">
                    <a href="<?php echo Router::url(array('controller' => 'our_clients', 'action' => 'index')); ?>"><i class="fa fa-building-o"></i> <span class="nav-label">Our Clients</span></a>
                </li>
            <?php } ?>
            <!-- Testimonials code Start VG-15/12/2016 -->
            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $testimonials ?>">
            <a href="<?php echo Router::url(array('controller' => 'testimonials', 'action' => 'index')); ?>"><i class="fa fa-building-o"></i> <span class="nav-label">Testimonials</span></a>
                </li>
            <?php } ?>
            <!-- Testimonials code End VG-15/12/2016 -->

            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $slideshow ?>">
            <a href="<?php echo Router::url(array('controller' => 'slideshow', 'action' => 'index')); ?>"><i class="fa fa-image"></i> <span class="nav-label">Slideshow</span></a>
                </li>
            <?php } ?>

            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $blog ?>">
                  <a href="<?php echo Router::url(array('controller' => 'blogs', 'action' => 'index')); ?>"><i class="fa fa-list-alt"></i> <span class="nav-label">Blogs</span></a>
                </li>
            <?php } ?>

            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $whyus ?>">
            <a href="<?php echo Router::url(array('controller' => 'whyus', 'action' => 'index')); ?>"><i class="fa fa-building-o"></i> <span class="nav-label">Why Choose Us</span></a>
                </li>
            <?php } ?>

            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $searchresultnotfound ?>">
                    <a href="<?php echo Router::url(array('controller' => 'searchresultnotfound', 'action' => 'index')); ?>"><i class="fa fa-building-o"></i> <span class="nav-label">Users Specifications<br><small>(Search Result Not Found)</small></span></a>
                </li>
            <?php } ?>


            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $sett ?>">
                    <a href="<?php echo Router::url(array('controller' => 'settings', 'action' => 'index')); ?>"><i class="fa fa-cogs"></i> <span class="nav-label">Setting</span></a>
                </li>
            <?php } ?>

            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $site_visit ?>">
                    <a href="<?php echo Router::url(array('controller' => 'site_visits', 'action' => 'visit_report')); ?>"><i class="fa fa-star"></i> <span class="nav-label">Visit Report</span></a>
                </li>
            <?php } ?>

            <?php if (in_array(AuthComponent::user('role'), array(1, 4, 3))) { ?>
                <li class="<?php echo $articles ?>">
                    <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'index')); ?>"><i class="fa fa-star"></i> <span class="nav-label">Articles</span></a>
                </li>
            <?php } ?>

            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $article_reports ?>">
                    <a href="<?php echo Router::url(array('controller' => 'articles_reports', 'action' => 'index')); ?>"><i class="fa fa-star"></i> <span class="nav-label">Article Reports</span></a>
                </li>
            <?php } ?>

            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php //echo $article_reports ?>">
                    <a href="<?php //echo Router::url(array('controller' => 'articles_reports', 'action' => 'index')); ?>"><i class="fa fa-star"></i> <span class="nav-label">Report Image Upload</span></a>
                </li>
            <?php } ?>

            
            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $reportdescriptionimages ?>">
            <a href="<?php echo Router::url(array('controller' => 'reportdescriptionimages', 'action' => 'index')); ?>"><i class="fa fa-building-o"></i> <span class="nav-label">Report Image Description</span></a>
                </li>
            <?php } ?>

            <?php if (in_array(AuthComponent::user('role'), array(1))) { ?>
                <li class="<?php echo $rdupdateimagedata ?>">
            <a href="<?php echo Router::url(array('controller' => 'rdupdateimagedata', 'action' => 'index')); ?>"><i class="fa fa-building-o"></i> <span class="nav-label">RDUpdate Image</span></a>
                </li>
            <?php } ?>
            
        </ul>
    </div>
</nav>
